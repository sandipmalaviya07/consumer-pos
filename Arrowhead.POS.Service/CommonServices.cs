﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace Arrowhead.POS.Service
{
    public class CommonServices
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<QuoteSourceModel> GetQuoteSource()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.APPLICANTSOURCESTATUS.ToString()
                            select new QuoteSourceModel() { ID = gt.ID, Source = gt.Name }).ToList();
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<State> GetAllState(long QuoteId)
        {
            GeneralServices _generalServices = new GeneralServices();
            var stateId = _generalServices.GetStateID(QuoteId);
            var stateCode = GeneralServices.GetStateCode(stateId);
            using (var db = new ArrowheadPOSEntities())
            {
                var _excludedDriverState = db.ProjectTerms.Where(x => x.Code == stateCode + Enums.ExcludedLicense.EXCLUDEDLICENSESTATE.ToString()).Select(x => x.TermValue).FirstOrDefault();
                if (_excludedDriverState != null && !string.IsNullOrEmpty(_excludedDriverState))
                {
                    var _excludedStateList = _excludedDriverState.Split(',');
                    var item = db.States.Where(x => !_excludedStateList.Contains(x.Code)).OrderBy(f => f.Name).ToList();
                    return item;
                }
                else
                {
                    var item = db.States.OrderBy(f => f.Name).ToList();
                    return item;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Agency> GetAllAgency()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = db.Agencies.OrderBy(f => f.Name).ToList();
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<InsuranceCompany> GetAllCarrier()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = db.InsuranceCompanies.OrderBy(o => o.Name).ToList();
                return item;
            }
        }


        public List<User> GetSoldBy()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = db.Users.OrderBy(f => f.FirstName + " " + f.LastName).ToList();
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetDriverLicenseStatus()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.LICENSESTATUS.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        public List<DropdownModel> GetNSDPlans()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from np in db.NSDPlans
                            join npd in db.NSD_PlanDetails on np.ID equals npd.FKNsdPlansId
                            where np.Code == "TOWBUSTERS"
                            select new DropdownModel() { ID = npd.ID, Name = npd.Description }).ToList();

                return item;
            }
        }

        public List<DropdownModel> GetHomeTypesList()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            orderby gt.OrderNo
                            where q.Code == Enums.GeneralStatus.HOMETYPE.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        /// <summary>
        /// Get List of All Military Types
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetMilitaryTypes()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.MILITARY.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        public List<DropdownModel> GetWarehouseTypes()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.WAREHOUSE.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetPriorCarrier()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.BIPRIORVALUE.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetPriorInsuranceLapse()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.PRIORINSURANCELAPSE.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetAccountType()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from gs in db.GeneralStatus
                            join at in db.GeneralStatusTypes on gs.ID equals at.FKGeneralStatusID
                            where gs.Code == Enums.GeneralStatus.ACCOUNTTYPE.ToString()
                            select new DropdownModel()
                            {
                                ID = at.ID,
                                Name = at.Name,
                                Code = at.Code
                            }).ToList();
                return item;
            }
        }

        /// <summary>
        /// Get State Id By Code
        /// </summary>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        public int GetStateIdByCode(string stateCode)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (db.States.Where(x => x.Code == stateCode).Select(x => x.ID).FirstOrDefault());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DriverRelationModel> GetDriverRelationShip()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.DRIVERRELATION.ToString() && gt.Code != ConstantVariables.FirstDriverRelationCode
                            select new DriverRelationModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<AutoDriverViolationModel> GetAllViolation()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from v in db.Auto_Violation
                            select new AutoDriverViolationModel() { AutoDriverViolationId = v.ID, ViolationName = v.Name, ViolationCode = v.Code }).ToList();

                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetDriverOccupation()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == ConstantVariables.LicenseOccupationCode
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetAllOwnership()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == Enums.GeneralStatus.OWNERTYPE.ToString()
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<LienHolder> GetAllLienHolders()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = db.LienHolders.OrderBy(f => f.CommercialName).ToList();
                item.Add(new LienHolder() { ID = 10000, CommercialName = "Other" });
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetOfficeID()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = db.Offices.Where(f => f.Code == "ARROWHEAD").FirstOrDefault().ID;
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetAutoInsuranceID()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = db.InsuranceTypes.Where(f => f.Code == "AUTOINSURANCE").FirstOrDefault().Id;
                return item;
            }
        }

        public string GetCustomerName(long Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                string fullname = db.Customers.Where(f => f.ID == Id).Select(s => s.FirstName + " " + s.LastName).FirstOrDefault();
                return fullname;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string state(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var quoteDetails = db.Quotes.Where(w => w.ID == quoteId).FirstOrDefault();
                string stateAbr = db.States.Where(w => w.ID == quoteDetails.FkStateId).Select(s => s.Code).FirstOrDefault();
                return stateAbr;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public long VehicleByQuoteId(long vehicleId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var vehicles = db.Auto_Vehicle.Where(w => w.ID == vehicleId).FirstOrDefault();
                long quoteId = db.Quotes.Where(w => w.ID == vehicles.FkQuoteID).Select(s => s.ID).FirstOrDefault();
                return quoteId;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public long GetAgencyIDByQuote(long quoteId)
        {
            long i = 0;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    i = (from q in db.Quotes
                         let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                         let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                         where q.ID == quoteId
                         select a.ID).FirstOrDefault();
                }
            }
            catch (Exception ex) { }
            return i;
        }

        public int getProducerCodeByAgencyId(long agencyID, long stateID)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long raterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault();
                    var AgencyByContractNo = (from rpc in db.RaterProviderCredentials
                                              join rpm in db.RaterProviderModes on rpc.FKRaterProviderModeID equals rpm.ID
                                              where rpm.FKAgencyID == agencyID && rpm.FKStateID == stateID && rpc.FKRaterProviderFormId == raterProviderFormId
                                              select rpc.ValueText).FirstOrDefault();
                    return Convert.ToInt32(AgencyByContractNo);
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return 0;
        }


        public int getProducerCodeByAgencyId(long agencyID)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long raterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault();
                    var AgencyByContractNo = (from rpc in db.RaterProviderCredentials
                                              join rpm in db.RaterProviderModes on rpc.FKRaterProviderModeID equals rpm.ID
                                              where rpm.FKAgencyID == agencyID && rpc.FKRaterProviderFormId == raterProviderFormId
                                              select rpc.ValueText).FirstOrDefault();
                    return Convert.ToInt32(AgencyByContractNo);
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StateId"></param>
        /// <returns></returns>
        public List<StateMinimumCoverageModel> GetCoveragesByStateId(int StateId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var stateCoverages = (from sc in db.StateMinimumCoverages
                                          let c = db.Coverages.Where(w => w.ID == sc.FkCoverageID).FirstOrDefault()
                                          let s = db.States.Where(w => w.ID == sc.FKStateId).FirstOrDefault()
                                          where sc.FKStateId == StateId
                                          select new StateMinimumCoverageModel
                                          {
                                              CoverageValue1 = sc.Value1,
                                              CoverageValue2 = sc.Value2,
                                              CoverageCode = c.Code,
                                              StateCode = s.Code,
                                              StateId = sc.ID,
                                              CoverageId = sc.FkCoverageID
                                          }).ToList();
                    return stateCoverages;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipcode"></param>
        /// <returns></returns>
        public ZipCodeModel IsZipCodeCheck(string zipcode)
        {
            using (var db = new ArrowheadPOSEntities())
            {

                try
                {
                    HttpWebRequest request = WebRequest.Create("https://ziptasticapi.com/" + zipcode) as HttpWebRequest;

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    WebHeaderCollection header = response.Headers;

                    var encoding = ASCIIEncoding.ASCII;
                    var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding);
                    string responseText = reader.ReadToEnd();

                    ZipCodeModel zipCodeModel = JsonConvert.DeserializeObject<ZipCodeModel>(responseText);
                    return zipCodeModel;
                }
                catch (Exception ex)
                {
                    return null;
                }

            }



        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public byte[] GetTemplateByte(string template)
        {
           WebClient client = new WebClient();
           byte[] templateByte = client.DownloadData(template);
           return templateByte;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string CheckHost(string url)
        {
            String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            return strUrl;
        }

        /// <summary>
        /// Get General Status Type by General Status  Name
        /// </summary>
        /// <returns></returns>
        public List<DropdownModel> GetGeneralStatusByType(string param)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from q in db.GeneralStatus
                            join gt in db.GeneralStatusTypes on q.ID equals gt.FKGeneralStatusID
                            where q.Code == param
                            select new DropdownModel() { ID = gt.ID, Name = gt.Name, Code = gt.Code }).ToList();

                return item;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public AgencyModel GetAgencyDetailById(long agencyId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var agencyDetails = (from a in db.Agencies
                                         let o = db.Offices.FirstOrDefault(f => f.FKAgencyID == a.ID)
                                         where a.ID == agencyId
                                         select new AgencyModel
                                         {
                                             AgencyCode = a.Code,
                                             AgencyEmailId = a.EmailID,
                                             AgencyName = a.CommercialName,
                                             AgencyId = a.ID,
                                             OfficeId = o.ID

                                         }).FirstOrDefault();
                    return agencyDetails;
                }
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public PrintQuoteModel GetPrintQuoteDetailsById(long quoteId, long rateId, long agencyId)
        {
            var printQuoteModel = new PrintQuoteModel();
            AutoCoverageServices _autoCoverageServices = new AutoCoverageServices();
            AutoQuoteCovrageDetailsModel autoQuoteCovrageDetailsModel = new AutoQuoteCovrageDetailsModel();
            DocusignServices docusignServices = new DocusignServices();

            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var quoteDetails = (from q in db.Quotes
                                        let c = db.Customers.Where(w => w.ID == q.FkCustomerID).FirstOrDefault()
                                        let s = db.States.Where(w => w.ID == q.FkStateId).FirstOrDefault()
                                        where q.ID == quoteId
                                        select new
                                        {
                                            q,
                                            c,
                                            s
                                        }).FirstOrDefault();

                    var rateHistoryDetails = db.Auto_RateHistory.Where(w => w.FKQuoteID == quoteId).ToList();
                    var agency = db.Agencies.Where(w => w.ID == agencyId).FirstOrDefault();

                    DateTime currentDate = ConvertTo.GetEstTimeNow();
                    printQuoteModel.QuoteId = quoteId;

                    printQuoteModel.CurrentDate = currentDate;
                    printQuoteModel.AgentName = agency.CommercialName;
                    printQuoteModel.AgentNumber = agency.ContactNo != null ? ConvertTo.PhoneNumberFormatting(agency.ContactNo) : null;
                    printQuoteModel.InsuredName = quoteDetails.c.FirstName + " " + quoteDetails.c.LastName;
                    printQuoteModel.AgentAddress1 = agency.Address1;
                    printQuoteModel.AgentAddress2 = agency.Address2;
                    printQuoteModel.AgentZipCode = agency.Zipcode;
                    printQuoteModel.AgentCity = agency.City;
                    printQuoteModel.AgentStateCode = db.States.Where(w => w.ID == agency.FKStateId).Select(s => s.Code).FirstOrDefault();
                    printQuoteModel.Address1 = quoteDetails.q.Address1;
                    printQuoteModel.Address2 = quoteDetails.q.Address2;
                    printQuoteModel.CustomerContactNo = !string.IsNullOrEmpty(quoteDetails.c.ContactNo) ? ConvertTo.PhoneNumberFormatting(quoteDetails.c.ContactNo) : "";
                    printQuoteModel.CustomerEmailId = quoteDetails.c.EmailID;
                    printQuoteModel.City = quoteDetails.q.City;
                    printQuoteModel.ZipCode = quoteDetails.q.ZipCode;
                    printQuoteModel.StateCode = quoteDetails.s.Code;
                    printQuoteModel.RateHistoryDetails = new List<RateHistoryPlanModel>();
                    foreach (var item in rateHistoryDetails)
                    {
                        string plan = item.Description.Split('(').First();
                        string _premium = GetValuesWithDollerSign(item.Premium.ToString());
                        decimal _downpayment = item.DownPayment + item.NSDFee + item.PGProcessingFees;
                        string _dp = GetValuesWithDollerSign(_downpayment.ToString());
                        string monthlyAmount = GetValuesWithDollerSign(item.MonthllyPayment.ToString());
                        RateHistoryPlanModel rateHistoryPlanModel = new RateHistoryPlanModel
                        {
                            ApplicationFee = item.ApplicationFee,
                            Description = plan,
                            NumberOfPayment = item.NumberOfPayment ?? 0,
                            PGProcessingFees = item.PGProcessingFees,
                            InstallmenetFee = item.InstallmenetFee ?? 0,
                            NSDFee = item.NSDFee,
                            MonthlyPayment = monthlyAmount,
                            DownPayment = _dp,
                            InstallmentDueDate = Convert.ToDateTime(item.InstallmentDueDate),
                            Premium = _premium,
                            PolicyTerm = quoteDetails.q.PolicyTerm
                        };

                        printQuoteModel.RateHistoryDetails.Add(rateHistoryPlanModel);

                    }
                    var _vehicleList = (db.Auto_Vehicle.Where(w => w.FkQuoteID == quoteId && w.IsDeleted == false)).ToList();

                    List<DisplayCoverageModel> _withVeh = new List<DisplayCoverageModel>();
                    List<DisplayCoverageModel> _withOutVeh = new List<DisplayCoverageModel>();

                    long colllevelId = db.Coverages.Where(w => w.Code == Enums.Coverages.COLLLEVEL.ToString()).Select(s => s.ID).FirstOrDefault();

                    var _stateCoverage = (from sc in db.StateCoverages
                                          join c in db.Coverages on sc.FkCoverageID equals c.ID
                                          where sc.FkStateID == quoteDetails.q.FkStateId && sc.IsDeleted == false
                                          && c.ID != colllevelId
                                          select new StateCoverageModel
                                          {
                                              CoverageName = c.Name,
                                              CoverageCode = c.Code,
                                              IsVehicleCoverage = c.IsVehicleCoverage
                                          }).Distinct().ToList();

                    var _quoteCoverage = (from aqc in db.Auto_QuoteCoverage
                                          join c in db.Coverages on aqc.FkCoverageID equals c.ID
                                          where aqc.FKQuoteID == quoteId && aqc.FkCoverageID != 27
                                          select new StateCoverageValues()
                                          {
                                              Code = c.Code,
                                              Value1 = aqc.Value1 ?? 0,
                                              Value2 = aqc.Value2 ?? 0,
                                              VehicleID = aqc.FKVehicleID ?? 0,
                                              CoverageId = c.ID
                                          }).ToList();

                    foreach (var cov in _stateCoverage)
                    {
                        try
                        {
                            var _coverage = _quoteCoverage.FindAll(f => f.Code == cov.CoverageCode);
                            string _displayVal = "No Coverage";
                            string _displayVal2 = "";
                            if (cov.IsVehicleCoverage)
                            {
                                foreach (var _veh in _vehicleList)
                                {
                                    var _Ncoverage = _coverage.Where(w => w.VehicleID == _veh.ID && w.Code == cov.CoverageCode).FirstOrDefault();
                                    if (_coverage != null && _coverage.Count > 0 && _Ncoverage != null)
                                    {
                                        _displayVal = db.StateCoverages.Where(w => w.FkCoverageID == _Ncoverage.CoverageId && w.Value1 == _Ncoverage.Value1 && w.FkStateID == quoteDetails.q.FkStateId && w.IsDeleted == false).Select(s => s.DisplayTextValue1).FirstOrDefault();
                                        var _fkcov = db.Auto_RateHistoryDetail.Where(w => w.FkCoverageID == _Ncoverage.CoverageId && w.FKQuoteID == quoteId && w.VehicleID == _veh.ID)
                                                                                                 .Select(s => s.Amount)
                                                                                                 .FirstOrDefault();
                                        if (_fkcov != null)
                                        {
                                            _displayVal2 = GetValuesWithDollerSign(_fkcov.ToString()).ToString();
                                        }
                                        else
                                        {
                                            _displayVal2 = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        _displayVal = "No Coverage";
                                        _displayVal2 = "";
                                    }
                                    DisplayCoverageModel _withvehCov = new DisplayCoverageModel()
                                    {
                                        DisplayCoverageCode = cov.CoverageCode,
                                        DisplayCoverageName = cov.CoverageName,
                                        Value1 = _displayVal,
                                        Value2 = _displayVal2,
                                        VehicleName = _veh.Year.ToString() + " " + _veh.Make + " " + _veh.Model + " (" + _veh.VIN + ")",
                                        VehicleId = _veh.ID
                                    };
                                    _withVeh.Add(_withvehCov);
                                }
                            }
                            else
                            {
                                var _Ncoverage = _coverage.FirstOrDefault();
                                if (_coverage != null && _coverage.Count > 0 && _Ncoverage != null)
                                {
                                    _displayVal = db.StateCoverages.Where(w => w.FkCoverageID == _Ncoverage.CoverageId && w.Value1 == _Ncoverage.Value1 && w.FkStateID == quoteDetails.q.FkStateId && w.IsDeleted == false).Select(s => s.DisplayTextValue1).FirstOrDefault();
                                    _displayVal2 = GetValuesWithDollerSign((db.Auto_RateHistoryDetail.Where(w => w.FkCoverageID == _Ncoverage.CoverageId && w.FKQuoteID == quoteId)
                                                                                             .Select(s => s.Amount)
                                                                                             .FirstOrDefault() ?? 0).ToString());
                                }
                                else
                                {
                                    _displayVal = "No Coverage";
                                    _displayVal2 = "";
                                }
                                DisplayCoverageModel _withOutvehCov = new DisplayCoverageModel()
                                {
                                    DisplayCoverageCode = cov.CoverageCode,
                                    DisplayCoverageName = cov.CoverageName,
                                    Value1 = _displayVal,
                                    Value2 = _displayVal2,
                                    VehicleId = 0
                                };
                                _withOutVeh.Add(_withOutvehCov);
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                    }
                    printQuoteModel.DisplayCoverageDetailsWithoutVehicle = _withOutVeh;
                    printQuoteModel.DisplayCoverageDetailsWithVehicle = _withVeh.OrderBy(o => o.VehicleId).ToList();
                    List<AutoVehicleDetailModel> _autoVehicleDetailModel = new List<AutoVehicleDetailModel>();
                    foreach (var _vh in _vehicleList)
                    {
                        AutoVehicleDetailModel _veh = new AutoVehicleDetailModel()
                        {
                            VehFullName = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID).Select(s => s.VehicleName).FirstOrDefault(),
                            CollisionDed = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID && w.DisplayCoverageCode == Enums.Coverages.COLL.ToString()).Select(s => s.Value1).FirstOrDefault(),
                            CollisionDedVal2 = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID && w.DisplayCoverageCode == Enums.Coverages.COLL.ToString()).Select(s => s.Value2).FirstOrDefault(),
                            ComprehensiveDed = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID && w.DisplayCoverageCode == Enums.Coverages.COMP.ToString()).Select(s => s.Value1).FirstOrDefault(),
                            ComprehensiveDedVal2 = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID && w.DisplayCoverageCode == Enums.Coverages.COMP.ToString()).Select(s => s.Value2).FirstOrDefault(),
                            LoseOfUse = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID && w.DisplayCoverageCode == Enums.Coverages.LUSE.ToString()).Select(s => s.Value1).FirstOrDefault(),
                            LoseOfUseVal2 = printQuoteModel.DisplayCoverageDetailsWithVehicle.Where(w => w.VehicleId == _vh.ID && w.DisplayCoverageCode == Enums.Coverages.LUSE.ToString()).Select(s => s.Value2).FirstOrDefault()
                        };
                        _autoVehicleDetailModel.Add(_veh);
                    }
                    printQuoteModel.AutoVehicleDetailModels = _autoVehicleDetailModel;
                    var driverList = db.Auto_Driver.Where(w => w.FkQuoteID == quoteId && w.IsDeleted == false).ToList();
                    string _driverList = "";
                    foreach (var driver in driverList)
                    {

                        //_driverList += driver.FirstName + " " + driver.LastName + ", ";
                        var sr22Name = driver.IsSR22 == true ? "(SR22)" : "";
                        _driverList += driver.FirstName + " " + driver.LastName + sr22Name + ", ";
                    }
                    _driverList = _driverList.TrimEnd(',');

                    printQuoteModel.ExcludeDriver = _driverList;


                    printQuoteModel.DriverDefensiveDiscount = driverList.Where(w => w.IsDefensiveDriverCourse == true).Any() ? "YES" : "NO";
                    printQuoteModel.WorkLossBenefitsDiscount = driverList.Where(w => w.IsWorkLossBenefit == true).Any() ? "YES" : "NO";
                    printQuoteModel.MultiVehicleDisocunt = _vehicleList.Count() > 1 ? "YES" : "NO";


                    var _isNotexculdedDriver = db.Auto_Driver.Where(w => w.IsExcluded == false && w.FkQuoteID == quoteId).ToList();

                    for (int i = 0; i < _isNotexculdedDriver.Count; i++)
                    {
                        bool _isDSeniorDriver = ((ConvertTo.GetEstTimeNow() - _isNotexculdedDriver[i].BirthDate.Value).Days / 365.25) < 65 ? false : true;
                        printQuoteModel.SeniorDriverDiscount = (_isDSeniorDriver == true) ? "YES" : "NO";
                    }
                    if (quoteDetails.q.FkHomeTypeId != null && quoteDetails.q.FkHomeTypeId > 0)
                    {
                        var hometypeCod = GetGeneralStatusCodeById(quoteDetails.q.FkHomeTypeId.Value);
                        printQuoteModel.HomeownerDiscount = (hometypeCod != "NO") ? "YES" : "NO";
                    }


                    var quote = db.Auto_PriorInsuranceDetails.Where(w => w.FKQuoteId == quoteId).FirstOrDefault();
                    printQuoteModel.ProofofPriorDiscount = (quote != null) ? "YES" : "NO";

                    var _rateHistory = db.Auto_RateHistory.Where(w => w.ID == rateId).FirstOrDefault();
                    bool ispaidFull = _rateHistory.Description.Contains("PIF (0 Payments, 100% Down)") ? true : false;
                    printQuoteModel.PaidInFullDiscount = (ispaidFull == true) ? "YES" : "NO";

                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return printQuoteModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_input"></param>
        /// <returns></returns>
        private string GetValuesWithDollerSign(string _input)
        {
            try
            {
                if (!string.IsNullOrEmpty(_input))
                {
                    return string.Format("{0:C}", Convert.ToDecimal(_input));
                }
            }
            catch (Exception ex)
            {
                return _input;
            }
            return _input;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseModel"></param>
        /// <param name="fileName"></param>
        public void UploadBridgeRequestXML(BridgerXMLModel responseModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var RequestXMLByte = System.Text.Encoding.UTF8.GetBytes(responseModel.RequestFile);
                var _RequestFileUri = blobFileUtility.Save(ConstantVariables.RequestXMLContainer, fileName + "_reqXML" + extension, RequestXMLByte);
            }
            catch (Exception ex) { }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseModel"></param>
        /// <param name="fileName"></param>
        public void UploadBridgeResponseXML(string responseFile, string requestId)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var ResponseXMLByte = System.Text.Encoding.UTF8.GetBytes(responseFile);
                string fileName = requestId + extension;
                var _ResponseFileUri = blobFileUtility.Save(ConstantVariables.QuoteBridgeXMLContainer, fileName, ResponseXMLByte);
            }
            catch (Exception ex) { }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public void SendErrorMail(string error, string host)
        {
            string hostAddr = string.Empty;
            try
            {
                if (host.Contains("arrowhead-pos-qa") || host.Contains("localhost"))
                    hostAddr = "[QA]";
                else if (host.Contains("arrowhead-pos-prod"))
                    hostAddr = "[Production]";

                string EmailSubject = "Arrowhead - Application Error :" + hostAddr;
                bool isSuccess = MailingUtility.SendEmail(ConstantVariables.EmailFrom, ConstantVariables.EmailTo, "", "", EmailSubject, error,
                    false, ConstantVariables.Priortiy, "", "", ConstantVariables.Host, ConstantVariables.UserName, ConstantVariables.Password, ConstantVariables.Port,
                    true, false, true, null, null);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetGeneralStatusCodeById(long id)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    return db.GeneralStatusTypes.FirstOrDefault(f => f.ID == id).Code;
                }
            }
            catch (Exception ex)
            {
                return null;
            }


        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="id_CardTemplate"></param>
        public void IDCardFields(long quoteId, int id, dynamic _accrofield)
        {

            AutoQuoteServices quotesServices = new AutoQuoteServices();
            string agency_Phone = string.Empty;
            var policyHolderDetails = quotesServices.GetQuoteInfo(quoteId);
            var agent_Details = quotesServices.GetAgencyDetailsByQuoteId(quoteId);
            var _vehicle = policyHolderDetails.AutoQuoteVehicles.Where(x => x.AutoVehicleId == id).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(agent_Details.ContactNo))
            {
                agency_Phone = ConvertTo.PhoneNumberFormatting(agent_Details.ContactNo);
            }
            try
            {
                if (policyHolderDetails.AutoQuoteVehicles != null && policyHolderDetails.AutoQuoteVehicles.Count > 0)
                {

                    string excludedDriver = string.Empty;
                    if (policyHolderDetails.AutoQuoteDrivers != null && policyHolderDetails.AutoQuoteDrivers.Count > 0)
                    {

                        foreach (var _driver in policyHolderDetails.AutoQuoteDrivers)
                        {
                            if (_driver.IsExcluded == true)
                            {
                                if (string.IsNullOrWhiteSpace(excludedDriver))
                                    excludedDriver = _driver.FirstName + " " + _driver.LastName;
                                else
                                    excludedDriver = excludedDriver + " , " + _driver.FirstName + " " + _driver.LastName;
                            }
                        }

                    }


                    _accrofield.SetField("Issue_State", policyHolderDetails.QuoteModel.StateAbbr);
                    _accrofield.SetField("Policy_Number", policyHolderDetails.QuoteModel.PolicyNumber);
                    _accrofield.SetField("Insured_Name", policyHolderDetails.QuoteModel.FirstName + " " + policyHolderDetails.QuoteModel.LastName);
                    try
                    {
                        _accrofield.SetField("Insured_Address1", policyHolderDetails.QuoteModel.Address1);
                    }
                    catch (Exception ex) { }

                    try
                    {
                        _accrofield.SetField("Agent_Address", agent_Details.AgencyAddress);
                    }
                    catch (Exception ex) { }

                    _accrofield.SetField("Insured_Address1", policyHolderDetails.QuoteModel.Address1);
                    _accrofield.SetField("Insured_Address2", policyHolderDetails.QuoteModel.Address2);
                    _accrofield.SetField("Insured_CityStateZip", policyHolderDetails.QuoteModel.City + ", " + policyHolderDetails.QuoteModel.StateAbbr + " " + policyHolderDetails.QuoteModel.ZipCode);
                    _accrofield.SetField("Excluded_Drivers", excludedDriver);
                    _accrofield.SetField("Effective_From", Convert.ToString(Convert.ToDateTime(policyHolderDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt")));
                    _accrofield.SetField("Effective_To", Convert.ToString(Convert.ToDateTime(policyHolderDetails.QuoteModel.ExpirationDate).ToString("MM/dd/yyyy hh:mm tt")));
                    _accrofield.SetField("Year", Convert.ToString(_vehicle.Year));
                    _accrofield.SetField("Make", _vehicle.Make);
                    _accrofield.SetField("Model", _vehicle.Model);
                    _accrofield.SetField("VIN", _vehicle.Vin);
                    _accrofield.SetField("Agents_Name", StringExtension.FirstCharToUpper(agent_Details.AgencyName));
                    _accrofield.SetField("Agent_Phone", agency_Phone);
                    _accrofield.SetField("CarrierName", "Arrowhead");
                    _accrofield.SetField("ID_Card_Bottom_Info", "The current status of the actual motor vehicle liability insurance coverage is maintained by the Department of Motor Vehicle Safety and is accessible to law enforcement upon a check of the vehicle registration.");


                }
            }
            catch (Exception ex)
            {

            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="_accrofield"></param>
        public void BillingField(long quoteId, dynamic _accrofield)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
                RaterServices raterServices = new RaterServices();
                AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                int agencyId = autoPolicyServices.BillingAgencyId(quoteId);
                var billingDetails = autoQuoteServices.GetQuoteInfo(quoteId);
                var agencyDetails = autoQuoteServices.GetAgencyDetails(agencyId);
                var _rateHistory = autoQuoteServices.GetRateHistoryDetailsByQuoteID(quoteId);
                var _projectTerm = (from pt in db.ProjectTerms select pt).ToList();
                var _raterProviderForm = raterServices.GetArrowheadCredentials(billingDetails.QuoteModel.StateAbbr, agencyId);
                try
                {
                    _accrofield.SetField("producer_name", agencyDetails.AgencyName);
                    _accrofield.SetField("agency_address1", agencyDetails.Address1);
                    _accrofield.SetField("agency_address2", agencyDetails.Address2);
                    _accrofield.SetField("statement_date1", Convert.ToString(Convert.ToDateTime(billingDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt")));
                    _accrofield.SetField("due_date1", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy hh:mm tt")));
                    _accrofield.SetField("minimum_due1", "$" + _rateHistory.MonthlyPayment.ToString());
                    decimal payFullamount = _rateHistory.TotalPremium - _rateHistory.PayableDownPayment + _rateHistory.InstallmentFee + _rateHistory.InstallmentFee ?? 0;

                    _accrofield.SetField("pay_InFull", "$" + payFullamount.ToString());
                    _accrofield.SetField("policy_number", billingDetails.QuoteModel.PolicyNumber);

                    if (!string.IsNullOrWhiteSpace(_raterProviderForm.ContractNumber))
                    {
                        _accrofield.SetField("agent_number", _raterProviderForm.ContractNumber);
                        _accrofield.SetField("producer_code_1", "Producer #:" + _raterProviderForm.ContractNumber);

                    }

                    string weburl = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYWEBADDRESS.ToString()).Select(f => f.TermValue).FirstOrDefault();
                    string local_Phone = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYLOCALPHONENUMBER.ToString()).Select(f => f.TermValue).FirstOrDefault();
                    string tollFreeNumber = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYTOLLFREENUMBER.ToString()).Select(f => f.TermValue).FirstOrDefault();
                    string fax_phone = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYFAXNUMBER.ToString()).Select(f => f.TermValue).FirstOrDefault();

                    _accrofield.SetField("web_address", weburl);
                    string localPhone = ConvertTo.PhoneNumberFormatting(local_Phone);
                    _accrofield.SetField("local_Phone", localPhone);
                    string _tollfreePhone = ConvertTo.PhoneNumberFormatting(tollFreeNumber);
                    _accrofield.SetField("toll_free_phone", _tollfreePhone);
                    string _faxphpne = ConvertTo.PhoneNumberFormatting(fax_phone);
                    _accrofield.SetField("fax_phone", _faxphpne);

                    _accrofield.SetField("amount_due1", "$" + _rateHistory.MonthlyPayment.ToString());

                    _accrofield.SetField("insurer_address1", agencyDetails.Address1);
                    _accrofield.SetField("insurer_address2", agencyDetails.Address2);

                    _accrofield.SetField("Insured_Name", billingDetails.QuoteModel.FullName.Truncate(16));
                    _accrofield.SetField("insured_name_2", billingDetails.QuoteModel.FullName.Truncate(16));
                    _accrofield.SetField("Phone_1", billingDetails.QuoteModel.ContactNo);
                    _accrofield.SetField("insured_address_1", billingDetails.QuoteModel.Address1);
                    _accrofield.SetField("insured_address_2", billingDetails.QuoteModel.City + " " + billingDetails.QuoteModel.StateAbbr + " " + billingDetails.QuoteModel.ZipCode);
                    _accrofield.SetField("expiration_date_from", Convert.ToString(Convert.ToDateTime(billingDetails.QuoteModel.ExpirationDate).ToString("MM/dd/yyyy")));
                    _accrofield.SetField("effectivedate_to", Convert.ToString(Convert.ToDateTime(billingDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt")));
                    _accrofield.SetField("policy_number_2", billingDetails.QuoteModel.PolicyNumber);

                    _accrofield.SetField("Installment_notice_date1", Convert.ToString(Convert.ToDateTime(ConvertTo.GetEstTimeNow()).ToString("MM/dd/yyyy")));
                    _accrofield.SetField("Installment_notice_date2", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy")));
                    _accrofield.SetField("Installment_notice_date3", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy hh:mm tt")));

                    _accrofield.SetField("Installment_notice_amount1", "$" + _rateHistory.PayableDownPayment.ToString());
                    string _installmenetfees = "0";
                    _installmenetfees = _rateHistory.Description.Contains("PIF (0 Payments, 100% Down)") ? "0" : _rateHistory.InstallmentFee.ToString();
                    _accrofield.SetField("Installment_notice_amount2", "$" + (_rateHistory.MonthlyPayment - Convert.ToDecimal(_installmenetfees)).ToString());
                    _accrofield.SetField("Installment_notice_fee", "$" + _installmenetfees);
                    _accrofield.SetField("Installment_notice_minimum_due", "$" + _rateHistory.MonthlyPayment.ToString());
                    if (_rateHistory.NoOfInstallement == 1)
                    {
                        _accrofield.SetField("monthly_installment_no1", (_rateHistory.NoOfInstallement - 1).ToString());
                        _accrofield.SetField("monthly_installment_no2", (_rateHistory.NoOfInstallement - 1).ToString());
                    }
                    else
                    {
                        _accrofield.SetField("monthly_installment_no1", "1");
                        _accrofield.SetField("monthly_installment_no2", (_rateHistory.NoOfInstallement - 1).ToString());
                    }

                    _accrofield.SetField("producerdetail_name1", agencyDetails.AgencyName);
                    _accrofield.SetField("producerdetail_address1", agencyDetails.Address1);
                    _accrofield.SetField("producerdetail_address_2", agencyDetails.Address2);

                    if (!string.IsNullOrWhiteSpace(agencyDetails.ContactNo))
                    {
                        string phone = ConvertTo.PhoneNumberFormatting(agencyDetails.ContactNo);
                        _accrofield.SetField("producerdetail_phone1", phone);

                    }
                    else
                    {
                        _accrofield.SetField("producerdetail_phone1", "");

                    }
                }
                catch (Exception ex)
                {

                }
            }
        }


        /// <summary>
        /// Validate License No Based on State Wise 
        /// </summary>
        /// <param name="stateCode"></param>
        /// <param name="licenseNo"></param>
        /// <returns></returns>
        public AutoDriverLicenseModel IsValidLicenseNo(string stateCode, string licenseNo)
        {
            AutoDriverLicenseModel autoDriverLicenseModel = new AutoDriverLicenseModel();
            string oneToSevenNumeric = "^[0-9]{1,7}$";
            string oneAlpha = "(.*[A-Za-z]){1}";
            string oneAlphaPlusSeven = "^.[0-9]{7}$";
            string twoAlpha = "(.*[A-Za-z]){2}";
            string alphaPlusSixNumeric = "(.*[0-9]){6}$";
            string threeToFiveNumeric = "(.*[0-9]){3,5}$";
            string fiveToNineNumeric = "(.*[0-9]){5,9}";
            string sixNumeric = "^[0-9]{6}$";
            string sevenNumeric = "^[0-9]{7}$";
            string eightNumeric = "^[0-9]{7,8}$"; // AL State Allow 7-Digit Driver License
            string sevenToNineNumeric = "^[0-9]{7,9}$";
            string eightAreNumbers = "(.*[0-9]){8}";
            string nineNumeric = "^[0-9]{9}$";
            string nineAlphaChars = "^[A-Za-z0-9]{9}$";
            string tenNumeric = "^[0-9]{10}$";
            string elevenNumeric = "^.[0-9]{11}$";
            string twelveNumeric = "^.[0-9]{12}$";
            string tenNumericMI = "^.[0-9]{10}$";
            string hPlusEight = "([H][0-9]{8})$";
            string sevenPlusX = "([H][0-9]{7}X)$";

            try
            {
                if (stateCode == null || licenseNo == string.Empty)
                {
                    autoDriverLicenseModel.ErrorMessage = "Invalid";
                    autoDriverLicenseModel.IsSuccess = false;
                    return autoDriverLicenseModel;
                }
                else if (stateCode == "AK")
                {
                    return IsValidLicenseNo(oneToSevenNumeric, licenseNo, "Must be 1-7 numeric");
                }
                else if (stateCode == "AL")
                {
                    return IsValidLicenseNo(eightNumeric, licenseNo, "Must be 8 numeric");
                }
                else if (stateCode == "AR" || stateCode == "MS")
                {
                    return IsValidLicenseNo(nineNumeric, licenseNo, "Must be 9 numeric");
                }
                else if (stateCode == "AZ")
                {
                    if (IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else if (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(eightAreNumbers, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else if (IsValidLicenseNo(twoAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(threeToFiveNumeric, licenseNo, "").IsSuccess && licenseNo.Length < 8)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {

                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 1 Alphabetic, 8 Numeric; or 2 Alphabetic, 3 - 6 Numeric; or 9 Numeric";
                    }

                }
                else if (stateCode == "CA")
                {
                    if (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(oneAlphaPlusSeven, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {

                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 1 alpha and 7 numeric";
                    }
                }
                else if (stateCode == "CO" || stateCode == "CN" || stateCode == "CT")
                {
                    return IsValidLicenseNo(nineNumeric, licenseNo, "Must be 9 numeric");
                }
                else if (stateCode == "DC")
                {
                    if (IsValidLicenseNo(sevenNumeric, licenseNo, "").IsSuccess || IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 7 - 9 numeric";
                    }
                }
                else if (stateCode == "DE")
                {
                    if (IsValidLicenseNo(oneToSevenNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 1 - 7 numeric";
                    }
                }
                else if (stateCode == "FL")
                {
                    if (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(twelveNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;

                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 1 alpha, 12 numeric";
                    }
                }
                else if (stateCode == "GA")
                {
                    if (IsValidLicenseNo(sevenToNineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 7 - 9 numeric";
                    }
                }
                else if (stateCode == "HI")
                {
                    if (IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess || IsValidLicenseNo(hPlusEight, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }

                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must 'H' + 8 numeric; 9 numeric";
                    }
                }
                else if (stateCode == "ID")
                {
                    if (IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess || IsValidLicenseNo(sixNumeric, licenseNo, "").IsSuccess || ((IsValidLicenseNo(twoAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(alphaPlusSixNumeric, licenseNo, "").IsSuccess)))
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must 9 numbers or 6 numbers; or 2 char, 6 numbers ";
                    }
                }
                else if (stateCode == "IL")
                {
                    if (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(elevenNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must 1 character 11 numbers";
                    }
                }
                else if (stateCode == "IN")
                {
                    if (IsValidLicenseNo(tenNumeric, licenseNo, "").IsSuccess || IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 9-10 numbers";
                    }
                }
                else if (stateCode == "IA")
                {
                    if (IsValidLicenseNo(nineAlphaChars, licenseNo, "").IsSuccess || IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 9 alpha numbers";
                    }
                }
                else if (stateCode == "KS" || stateCode == "KY")
                {
                    if (IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess || (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(eightAreNumbers, licenseNo, "").IsSuccess && licenseNo.Length == 9))
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 1 alpha and 8 numeric";
                    }
                }
                else if (stateCode == "LA")
                {
                    if (IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 9 numeric";
                    }
                }
                else if (stateCode == "ME")
                {
                    if (IsValidLicenseNo(sevenNumeric, licenseNo, "").IsSuccess || IsValidLicenseNo(sevenPlusX, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 7 numeric";
                    }
                }
                else if (stateCode == "MD" || stateCode == "MN")
                {
                    if (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(twelveNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "1 Alphabetic, 12 Numeric";
                    }
                }
                else if (stateCode == "MI")
                {
                    if ((IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(twelveNumeric, licenseNo, "").IsSuccess) || (IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(tenNumericMI, licenseNo, "").IsSuccess))
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "1 Alphabetic, 10 or 12 Numeric";
                    }
                }
                else if (stateCode == "MA")
                {
                    if ((IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(eightAreNumbers, licenseNo, "").IsSuccess && licenseNo.Length == 9) || IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "Must be 1 alpha, 8 numeric; 9 numeric";
                    }
                }
                else if (stateCode == "MO")
                {
                    if ((IsValidLicenseNo(oneAlpha, licenseNo, "").IsSuccess && IsValidLicenseNo(fiveToNineNumeric, licenseNo, "").IsSuccess && licenseNo.Length < 11) || IsValidLicenseNo(nineNumeric, licenseNo, "").IsSuccess)
                    {
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.IsSuccess = true;
                    }
                    else
                    {
                        autoDriverLicenseModel.IsSuccess = false;
                        autoDriverLicenseModel.LicenseNo = licenseNo;
                        autoDriverLicenseModel.ErrorMessage = "1 alpha - 5-9 Numeric or 9 numeric";

                    }
                }
                else
                {
                    autoDriverLicenseModel.IsSuccess = true;
                    autoDriverLicenseModel.LicenseNo = licenseNo;
                }
                return autoDriverLicenseModel;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Validate Regex Expression
        /// </summary>
        /// <param name="regexExpr"></param>
        /// <param name="licenseNo"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public AutoDriverLicenseModel IsValidLicenseNo(string regexExpr, string licenseNo, string errorMessage)
        {
            AutoDriverLicenseModel _autoDriverLicenseModel = new AutoDriverLicenseModel();
            Regex re = new Regex(regexExpr);
            if (regexExpr != null && regexExpr != " ")
            {
                if (re.IsMatch(licenseNo))
                {
                    _autoDriverLicenseModel.LicenseNo = licenseNo;
                    _autoDriverLicenseModel.IsSuccess = true;
                }
                else
                {
                    _autoDriverLicenseModel.LicenseNo = licenseNo;
                    _autoDriverLicenseModel.IsSuccess = false;
                    _autoDriverLicenseModel.ErrorMessage = errorMessage;
                }
            }
            return _autoDriverLicenseModel;
        }


        /// <summary>
        /// Get Payment Method based on Payment Method Coe
        /// </summary>
        /// <param name="paymentMethodCd"></param>
        /// <returns></returns>
        public string GetPaymentMethod(string paymentMethodCd)
        {
            string paymentMethod = string.Empty;
            return "CASH";
            //switch (paymentMethodCd.ToUpper())
            //{
            //    case "SWEEP":
            //        paymentMethod = "CASH";
            //        break;

            //    case "CREDITCARD":
            //        paymentMethod = "CASH";
            //        break;

            //    case "ECHECK":
            //        paymentMethod = "CHECK";
            //        break;
            //}
            //return paymentMethod;
        }





    }
}
