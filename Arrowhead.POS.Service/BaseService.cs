﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Arrowhead.POS.Service
{
   public class BaseService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="module"></param>
        /// <param name="operation"></param>
        /// <param name="remarkResourceText"></param>
        /// <param name="groupTransactionId"></param>
        /// <param name="entityId"></param>
        /// <param name="quoteId"></param>
        /// <param name="customerId"></param>
        /// <param name="entityName"></param>
        /// <param name="deviceId"></param>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        /// <param name="parentId"></param>
        public static void InsertAuditHistory(Enums.AuditHistoryModule module, long agencyId,string operation, string remarkResourceText, Guid groupTransactionId, string entityId, long quoteId = 0, long customerId = 0, string entityName = null, string deviceId = null, string oldValue = null, string newValue = null, long parentId = 0)
        {
            try
            {
                var moduleName = Core.ConstantVariables.dict.FirstOrDefault(x => x.Key.ToString() == module.ToString()).Value;
                string IpAddress = HttpContext.Current.Request.UserHostAddress;
                CommonServices commonServices = new CommonServices();
                string customername = commonServices.GetCustomerName(customerId);
                string _remark = remarkResourceText.Replace("#module#", Convert.ToString(moduleName)).Replace("#entityname#", entityName).Replace("#customername#", customername).Replace("#ip#", IpAddress).Replace("#datetime#", ConvertTo.GetEstTimeNow().ToString());
                AuditHistoryService auditHistoryService = new AuditHistoryService();
                AuditHistoryModel auditHistoryModel = new AuditHistoryModel();
                auditHistoryModel.AgencyID =Convert.ToInt16(agencyId);
                auditHistoryModel.EntityID = entityId;
                auditHistoryModel.IsError = false;
                auditHistoryModel.ModuleID = module.ToString();
                auditHistoryModel.QuoteID = quoteId;
                auditHistoryModel.CustomerId = customerId;
                auditHistoryModel.Remarks = _remark;
                auditHistoryModel.Operation = Convert.ToChar(operation);
                auditHistoryService.InsertAuditHistory(auditHistoryModel);

            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="module"></param>
        /// <param name="operationType"></param>
        /// <param name="quoteId"></param>
        /// <param name="errormessage"></param>
        public static void InsertErrorLogHistory(int agencyId,string module,string operationType,long quoteId,string errormessage)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    AuditHistoryService auditHistoryService = new AuditHistoryService();
                    AuditHistoryModel auditHistoryModel = new AuditHistoryModel();

                    AuditHistoryModel _historyModel = new AuditHistoryModel()
                    {
                        AgencyID = agencyId,
                        IsError = true,
                        ModuleID = module,
                        Operation = Convert.ToChar(operationType),
                        Remarks = "Got error in Module : " + module + " is " + errormessage,
                        QuoteID = quoteId,
                        CustomerId = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.FkCustomerID).FirstOrDefault()
                    };
                    auditHistoryService.InsertAuditHistory(_historyModel);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
