﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Core.Paging;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Arrowhead.POS.Core.Resources;
using log4net;

namespace Arrowhead.POS.Service
{
    public class AutoQuoteServices : BaseService
    {
        GeneralServices generalServices = new GeneralServices();
        private static readonly ILog Log = LogManager.GetLogger(typeof(AutoQuoteServices));
        public ApplicantModel GetPolicyHolderDetailById(long Id)
        {
            Log.Debug("Get Policy Details :" + Id);
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    var quote = (from q in db.Quotes
                                 let s = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                 let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                 let ca = db.CustomerAddresses.FirstOrDefault(f => f.FKCustomerId == q.FkCustomerID)
                                 let aqd = db.Auto_QuoteDetail.FirstOrDefault(f => f.FKQuoteID == q.ID)
                                 let ac = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FKAssumeCreditId)
                                 let so = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkSourceID)
                                 let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkPolicyStatusId)
                                 let ap = db.Auto_PriorInsuranceDetails.FirstOrDefault(f => f.FKQuoteId == Id)
                                 where q.ID == Id
                                 select new ApplicantModel()
                                 {
                                     QuoteId = q.ID,
                                     CustomerId = q.FkCustomerID,
                                     StateId = q.FkStateId,
                                     City = q.City,
                                     ZipCode = q.ZipCode,
                                     Address1 = q.Address1,
                                     Address2 = q.Address2,
                                     StateAbbr = s.Code,
                                     EmailId = c.EmailID.ToLower(),
                                     ContactNo = c.ContactNo,
                                     FirstName = c.FirstName,
                                     EffectiveDate = q.EffectiveDate,
                                     PolicyTerm = q.PolicyTerm,
                                     LastName = c.LastName,
                                     SourceId = q.FkSourceID,
                                     SourceName = s.Name,
                                     //OtherPhoneNo = q.OtherPhoneNo,
                                     //AssumeCreditId = q.FKAssumeCreditId ?? 0,
                                     //AssumeCreditCardName = ac.Name,
                                     MailingAddress1 = (ca != null ? ca.Address1 : string.Empty),
                                     IsDuplicate = (ca != null ? ca.IsDuplicate : true),
                                     MailingAddress2 = (ca != null ? ca.Address2 : string.Empty),
                                     MailingCity = (ca != null ? ca.City : string.Empty),
                                     MailingState = s.Code,
                                     MailingZipCode = (ca != null ? ca.ZipCode : string.Empty),
                                     PolicyStatusId = ps.ID,
                                     IspriorPolicy = (ap != null ? true : false),
                                     BIPriorCoverage = ap.BIPriorCoverage,
                                     InsuranceCompanyId = ap.FKInsuranceCompanyId,
                                     DaysLapes = ap.DaysLapes,
                                     PolicyNumber = ap.PolicyNumber,
                                     PriorExpirationDate = ap.PriorExpirationDate,
                                     PolicyStatusCd = ps.Code,
                                     FkHomeTypeId = q.FkHomeTypeId.Value
                                 }).FirstOrDefault();
                    return quote;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quote"></param>
        /// <returns></returns>
        public ApplicantModel InsertQuote(ApplicantModel quote)
        {
            AuditHistoryService auditHistoryService = new AuditHistoryService();
            BaseService baseService = new BaseService();
            AuditHistoryModel auditHistoryModel = new AuditHistoryModel();
            TimeSpan effectiveDateTime = ConvertTo.GetEstTimeNow().TimeOfDay;
            try
            {

                using (var db = new ArrowheadPOSEntities())
                {
                    var _createDate = ConvertTo.GetEstTimeNow();
                    int stateID = db.States.Where(f => f.Code == quote.StateAbbr).FirstOrDefault().ID;

                    var quoteStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.NEW.ToString());
                    var quoteTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTETYPE.ToString(), Enums.QuoteType.QUOTE.ToString());
                    long fkcurrentstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QuoteOrderStatus.APPLICANT.ToString());
                    var groupTransactionId = Guid.NewGuid();

                    Customer customer = new Customer
                    {
                        FKOfficeID = quote.OfficeId,
                        FKStateID = stateID,
                        FirstName = StringExtension.FirstCharToUpper(quote.FirstName),
                        LastName = StringExtension.FirstCharToUpper(quote.LastName),
                        EmailID = quote.EmailId,
                        ContactNo = quote.ContactNo.ToPhoneNumber(),
                        CreatedDate = _createDate,
                        IsDeleted = false
                    };
                    customer.EmailID = quote.EmailId;
                    db.Customers.Add(customer);
                    db.SaveChanges();
                    quote.CustomerId = customer.ID;

                    InsertAuditHistory(Enums.AuditHistoryModule.C, quote.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, quote.CustomerId.ToString(), 0, quote.CustomerId, quote.FirstName + " " + quote.LastName);

                    Quote quoteDetail = new Quote
                    {
                        FkQuoteStatusID = quoteStatusID,
                        FkPolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.NEW.ToString()),
                        FkInsuranceTypeID = db.InsuranceTypes.Where(w => w.Code == "AUTOINSURANCE").Select(s => s.Id).FirstOrDefault(),
                        FKInsuranceCompanyId = db.InsuranceCompanies.Where(w => w.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
                        FkSourceID = quote.SourceId,
                        Address1 = StringExtension.FirstCharToUpper(quote.Address1),
                        Address2 = StringExtension.FirstCharToUpper(quote.Address2),
                        ZipCode = quote.ZipCode,
                        FkStateId = stateID,
                        City = quote.City,
                        AssignByID = quote.UserId,
                        CreatedDate = _createDate,
                        CreatedById = quote.UserId,
                        FkOfficeId = quote.OfficeId,
                        FKCurrentStepId = fkcurrentstatusId,
                        FkQuoteTypeId = quoteTypeID,
                        EffectiveDate = quote.EffectiveDate.Value.Add(effectiveDateTime),
                        ExpirationDate = quote.EffectiveDate.Value.AddMonths(ConstantVariables.PolicyTerm),
                        PolicyStatusChangeDate = _createDate,
                        FkCustomerID = quote.CustomerId,
                        PolicyTerm = ConstantVariables.PolicyTerm,
                        FkHomeTypeId = quote.FkHomeTypeId

                    };
                    db.Quotes.Add(quoteDetail);
                    db.SaveChanges();
                    quote.QuoteId = quoteDetail.ID;


                    InsertAuditHistory(Enums.AuditHistoryModule.AQ, quote.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, quote.QuoteId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);


                    Auto_QuoteDetail quoteDetails = new Auto_QuoteDetail
                    {
                        FKQuoteID = quote.QuoteId,
                        IsHomeownersDiscount = false,
                        IsGoodStudentDiscount = false,
                        IsPaperLessDiscount = false,
                        ResidenceType = 0,
                        MonthsInResidence = 0,
                        RateTransactionID = Guid.NewGuid()
                    };
                    db.Auto_QuoteDetail.Add(quoteDetails);
                    db.SaveChanges();
                    InsertAuditHistory(Enums.AuditHistoryModule.AQD, quote.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, quote.QuoteId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);


                    AddCustomerAddress(quote);

                    if (quote.isAddressVerified == true)
                    {
                        auditHistoryService.InsertAuditHistory(quote.QuoteId, (int)quote.AgencyId, quote.UserId);
                    }


                    if (quote.IspriorPolicy)
                    {
                        ManagePriorInsurance(quote, quote.QuoteId);
                    }
                    else
                    {
                        db.Auto_PriorInsuranceDetails.Where(w => w.FKQuoteId == quote.QuoteId).Delete();
                        InsertAuditHistory(Enums.AuditHistoryModule.API, quote.AgencyId, Enums.AuditHistoryOperation.D.ToString(), AuditHistoryRemarks.Delete.ToString(), groupTransactionId, quote.QuoteId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);

                    }
                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory((int)quote.AgencyId, Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.S.ToString(), quote.QuoteId, ex.ToString());

                throw new Exception();
            }
            return quote;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="quoteId"></param>
        public void ManagePriorInsurance(ApplicantModel model, long quoteId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    bool alreadyexists = db.Auto_PriorInsuranceDetails.Where(w => w.FKQuoteId == quoteId).Any();
                    var groupTransactionId = Guid.NewGuid();
                    if (alreadyexists)
                    {
                        db.Auto_PriorInsuranceDetails.Where(w => w.FKQuoteId == quoteId).Update(u => new Auto_PriorInsuranceDetails
                        {
                            //BIPriorCoverage = model.BIPriorCoverage,
                            ClaimFree = model.ClaimFree,
                            ContinuousCoverage = model.ContinuousCoverage,
                            DaysLapes = model.DaysLapes,
                            FKInsuranceCompanyId = model.InsuranceCompanyId,
                            IsAgentOfRecord = false,
                            MonthsInPriorPolicy = model.MonthsInPriorPolicy,
                            NumberOfRenewal = model.NumberOfRenewal,
                            PolicyNumber = model.PolicyNumber,
                            PriorExpirationDate = model.PriorExpirationDate

                        });

                        InsertAuditHistory(Enums.AuditHistoryModule.API, model.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quoteId.ToString(), quoteId, 0, model.FirstName + " " + model.LastName);

                    }
                    else
                    {
                        Auto_PriorInsuranceDetails auto_PriorInsuranceDetails = new Auto_PriorInsuranceDetails
                        {
                            FKQuoteId = quoteId,
                            //BIPriorCoverage = model.BIPriorCoverage,
                            ClaimFree = model.ClaimFree,
                            ContinuousCoverage = model.ContinuousCoverage,
                            DaysLapes = model.DaysLapes,
                            FKInsuranceCompanyId = model.InsuranceCompanyId,
                            IsAgentOfRecord = false,
                            MonthsInPriorPolicy = model.MonthsInPriorPolicy,
                            NumberOfRenewal = model.NumberOfRenewal,
                            PolicyNumber = model.PolicyNumber,
                            PriorExpirationDate = model.PriorExpirationDate
                        };
                        db.Auto_PriorInsuranceDetails.Add(auto_PriorInsuranceDetails);
                        db.SaveChanges();

                        InsertAuditHistory(Enums.AuditHistoryModule.API, model.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, quoteId.ToString(), quoteId, 0, model.FirstName + " " + model.LastName);

                    }
                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory((int)model.AgencyId, Enums.AuditHistoryModule.API.ToString(), Enums.AuditHistoryOperation.I.ToString(), quoteId, ex.ToString());

            }
        }

        public void AddCustomerAddress(ApplicantModel quote)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _createDate = ConvertTo.GetEstTimeNow();
                    int stateID = db.States.Where(f => f.Code == quote.StateAbbr).FirstOrDefault().ID;
                    long addressType = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ADDRESSTYPE.ToString(), Enums.AddressType.PERMANENT.ToString());
                    var groupTransactionId = Guid.NewGuid();

                    CustomerAddress customerAddress = new CustomerAddress();
                    AuditHistoryService auditHistoryService = new AuditHistoryService();
                    if ((string.IsNullOrWhiteSpace(quote.MailingAddress1)))
                    {
                        customerAddress.Address1 = StringExtension.FirstCharToUpper(quote.Address1);
                        customerAddress.Address2 = StringExtension.FirstCharToUpper(quote.Address2);
                        customerAddress.City = quote.City;
                        customerAddress.ZipCode = quote.ZipCode;
                        customerAddress.State = quote.StateAbbr;
                        customerAddress.CreatedDate = _createDate;
                        customerAddress.FKOfficeID = quote.OfficeId;
                        customerAddress.FKCustomerId = quote.CustomerId;
                        customerAddress.FKAddressTypeId = addressType;
                        customerAddress.IsDuplicate = true;
                        db.CustomerAddresses.Add(customerAddress);
                        db.SaveChanges();

                        InsertAuditHistory(Enums.AuditHistoryModule.CA, quote.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, customerAddress.ID.ToString(), quote.QuoteId, 0, null);

                    }
                    else
                    {
                        customerAddress.Address1 = StringExtension.FirstCharToUpper(quote.MailingAddress1);
                        customerAddress.Address2 = StringExtension.FirstCharToUpper(quote.MailingAddress2);
                        customerAddress.City = quote.MailingCity;
                        customerAddress.ZipCode = quote.MailingZipCode;
                        customerAddress.State = quote.MailingState;
                        customerAddress.IsDuplicate = false;
                        customerAddress.CreatedDate = _createDate;
                        customerAddress.FKOfficeID = quote.OfficeId;
                        customerAddress.FKCustomerId = quote.CustomerId;
                        customerAddress.FKAddressTypeId = addressType;
                        db.CustomerAddresses.Add(customerAddress);
                        db.SaveChanges();
                        InsertAuditHistory(Enums.AuditHistoryModule.CA, quote.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, customerAddress.ID.ToString(), quote.QuoteId, 0, null);

                    }




                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory((int)quote.AgencyId, Enums.AuditHistoryModule.CA.ToString(), Enums.AuditHistoryOperation.I.ToString(), quote.QuoteId, ex.ToString());

            }
        }

        public AutoModel GetQuoteInfo(long Id)
        {
            GeneralServices generalServices = new GeneralServices();
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {

                try
                {
                    var fkpolicystatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.NEW.ToString());
                    long fkPaymentSuccessStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());

                    int coverageId = db.Coverages.Where(x => x.Code == "SR22Fee").Select(s => s.ID).FirstOrDefault();
                    decimal srfee = db.Auto_RateHistoryDetail.Where(w => w.FKQuoteID == Id && w.FkCoverageID == coverageId).Select(s => s.Amount).FirstOrDefault() ?? 0;


                    var quoteInfo = (from q in db.Quotes
                                     let s = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                     let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                     let ca = db.CustomerAddresses.FirstOrDefault(f => f.FKCustomerId == c.ID)
                                     // let u = db.Users.FirstOrDefault(f => f.ID == UserId)
                                     let ac = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FKAssumeCreditId)
                                     let so = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkSourceID)
                                     let aqd = db.Auto_QuoteDetail.FirstOrDefault(f => f.FKQuoteID == q.ID)
                                     let rt = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == aqd.ResidenceType)
                                     let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkPolicyStatusId)
                                     let ap = db.Auto_PriorInsuranceDetails.FirstOrDefault(f => f.FKQuoteId == Id)

                                     let _companyDetails = (from rh in db.Auto_RateHistory
                                                            join ic in db.InsuranceCompanies on rh.FKInsuranceCompanyID equals ic.ID
                                                            where rh.ID == aqd.FKRateHistoryID
                                                            select new AutoRateDetailModel
                                                            {
                                                                InsuranceCompanyId = ic.ID,
                                                                InsuranceCompany = (ic.Name == null ? "" : ic.Name),
                                                                CarierDownpaymentAmount = (rh == null ? 0 : rh.DownPayment),
                                                                TotalPremium = (rh == null ? 0 : rh.Premium),
                                                                MonthlyPremium = (rh == null ? 0 : rh.MonthllyPayment),
                                                                BridgeUrl = rh.BridgeURL,
                                                                Description = rh.Description,

                                                                InstallmentDueDate = rh.InstallmentDueDate,
                                                                OtherFeesAmount = (rh == null ? 0 : rh.OtherFee),
                                                                DownPaymentAmount = (rh == null ? 0 : (rh.DownPayment + rh.NSDFee + rh.PGProcessingFees)),
                                                                NsdAmount = rh.NSDFee,
                                                                CCCharge = rh.PGProcessingFees
                                                            }).FirstOrDefault()

                                     let _autoQuoteDetails = (from aq in db.Auto_QuoteDetail
                                                              where aq.FKQuoteID == Id
                                                              select new AutoQuoteDetailModel
                                                              {
                                                                  IsGoodStudentDiscount = aq.IsGoodStudentDiscount,
                                                                  IsHomeOwnersDiscount = aq.IsHomeownersDiscount,
                                                                  IsPaperLessDiscount = aq.IsPaperLessDiscount,
                                                                  RateDate = aq.RateDate,
                                                                  RateTransactionID = aq.RateTransactionID,
                                                                  MonthsInResidence = aq.MonthsInResidence,
                                                                  IsNonOwner = aq.IsNonOwner,
                                                                  ResidenceType = aq.ResidenceType,
                                                                  RateHistoryId = aq.FKRateHistoryID ?? 0
                                                              }).ToList()


                                     let _autoQuoteDriver = (from aqd in db.Auto_Driver
                                                             join gs in db.GeneralStatusTypes on aqd.FkMaritalStatusID equals gs.ID
                                                             let licenseStatsName = db.GeneralStatusTypes.Where(x => x.ID == aqd.LicenseStatus).Select(x => x.Name).FirstOrDefault()
                                                             let OccupasionName = db.GeneralStatusTypes.Where(x => x.ID == aqd.FkDriverOccupationID).Select(x => x.Name).FirstOrDefault()
                                                             let licenseState = db.States.Where(w => w.ID == aqd.FkStateID).Select(w => w.Code).FirstOrDefault()
                                                             where aqd.FkQuoteID == Id && aqd.IsDeleted == false
                                                             select new AutoDriverModel
                                                             {
                                                                 AutoDriverId = aqd.ID,
                                                                 LicenseNo = aqd.LicenseNo,
                                                                 MaritalStatusName = gs.Code,
                                                                 DriverOrderID = aqd.DriverOrderID,
                                                                 MaritalStatusID = aqd.FkMaritalStatusID,
                                                                 LicenseState = licenseState,
                                                                 Dob = aqd.BirthDate,
                                                                 FirstName = aqd.FirstName,
                                                                 Sr22 = aqd.IsSR22,
                                                                 LastName = aqd.LastName,
                                                                 DriverName = aqd.FirstName + " " + aqd.LastName,
                                                                 LicenseStatusName = licenseStatsName,
                                                                 DriverOccupationName = OccupasionName,
                                                                 Gender = aqd.Gender,
                                                                 IsExcluded = aqd.IsExcluded,
                                                                 IsWorkLossBenefit = aqd.IsWorkLossBenefit,
                                                                 RelationName = db.GeneralStatusTypes.Where(w => w.ID == aqd.FkRelationCdID).Select(w => w.Name).FirstOrDefault(),
                                                                 // ViolationPoints = (ad != null && ad ?? 0),
                                                                 ViolationPoints = aqd.ViolationPoints,
                                                                 IsNoHit = aqd.IsNoHit

                                                             })

                                     let _autoVehicle = (from v in db.Auto_Vehicle
                                                         let l = db.LienHolders.FirstOrDefault(f => f.ID == v.FkLienHolderID)
                                                         let s = db.States.FirstOrDefault(f => f.ID == l.FkStateID)
                                                         where v.FkQuoteID == Id && v.IsDeleted == false
                                                         select new AutoVehicleModel
                                                         {
                                                             AutoVehicleId = v.ID,
                                                             VehicleOrderId = v.VehicleOrderID,
                                                             QuoteId = v.FkQuoteID,
                                                             Vin = v.VIN,
                                                             Year = v.Year,
                                                             Model = v.Model,
                                                             Make = v.Make,
                                                             VehicleDetails = v.Year + " " + v.Make + " " + v.Model,
                                                             LienHolderName = l.CommercialName,
                                                             LienHolderAddress1 = l.Address1,
                                                             LienHolderAddress2 = l.Address2,
                                                             LienHolderCity = l.City,
                                                             LienHolderStateID = l.FkStateID,
                                                             LienHolderZipCode = l.ZipCode,
                                                             LienHolderCommercialName = (v.LienHolderName != null ? v.LienHolderName + ", " + l.Address1.Trim() + (l.Address2.Trim() != "" ? " " + l.Address2.Trim() + ", " : ", ") + l.City.Trim() + ", " + s.Code.Trim() + " " + l.ZipCode.Trim() : ""),
                                                             StateAbbr = v.State.Name,
                                                             IsLienHolder = (v.FkLienHolderID != null ? "Yes" : "No")
                                                         })



                                     //let nsdplan = (from n in db.NSDPlans
                                     //               join qn in db.QuoteNSDPlans on n.ID equals qn.FKNsdId
                                     //               join nsp in db.NSD_PlanDetails on n.ID  equals nsp.FKNsdPlansId
                                     //               where qn.FKQuoteId == Id && nsp.PlanNo == qn.PlanId
                                     //               select new AutoQuoteNsdDetailModel
                                     //               {
                                     //                   AutoQuoteNsdPlanId = (qn.ID != null ? qn.ID : 0),
                                     //                   IsEnable = (qn.PlanId != null ? true : false),
                                     //                   PlanId = (qn.PlanId == null ? "" : qn.PlanId),
                                     //                   Price = (qn.Price != null ? qn.Price : 0),
                                     //                   QuoteId = qn.FKQuoteId,
                                     //                   NsdPlanCode = n.Code,
                                     //                   NsdPlanDesription = n.Description,
                                     //                   NsdPlanName = n.Name,
                                     //                   NsdPlanId = n.ID,
                                     //                   NSDPlanJsonDescription = Newtonsoft.Json.Linq.JObject.Parse(nsp.JsonDescription).ToString().Replace(Environment.NewLine, " ").Replace(@"""", @"\""").Replace(@"\", "").Replace("{", "").Replace("}", ""),
                                     //               })


                                     where q.ID == Id
                                     select new
                                     {
                                         q,
                                         q.FKCurrentStepId,
                                         s,
                                         rt,
                                         so,
                                         ac,
                                         c,
                                         ca,
                                         aqd,
                                         q.FkCustomerID,
                                         q.FkSourceID,
                                         q.FkStateId,
                                         _autoQuoteDriver,
                                         _autoVehicle,
                                         _companyDetails,
                                         _autoQuoteDetails,
                                         ps,
                                         //   u,
                                         ap
                                         //_producerCode
                                     }).FirstOrDefault();
                    AutoModel model = new AutoModel();

                    QuoteModel quoteModel = new QuoteModel()
                    {
                        QuoteId = quoteInfo.q.ID,
                        SR22Fee = srfee,
                        FullName = quoteInfo.c.FirstName + " " + quoteInfo.c.LastName,
                        CustomerId = quoteInfo.q.FkCustomerID,
                        PolicyTerm = quoteInfo.q.PolicyTerm,
                        PolicyNumber = quoteInfo.q.PolicyNumber,
                        Address1 = quoteInfo.q.Address1,
                        Address2 = quoteInfo.q.Address2,
                        IsDuplicate = quoteInfo.ca.IsDuplicate,
                        MailingAddress1 = quoteInfo.ca.Address1,
                        MailingAddress2 = quoteInfo.ca.Address2,
                        MailingCity = quoteInfo.ca.City,
                        MailingState = quoteInfo.ca.State,
                        MailingZipCode = quoteInfo.ca.ZipCode,
                        ZipCode = quoteInfo.q.ZipCode,
                        StateAbbr = quoteInfo.s.Code,
                        StateName = quoteInfo.s.Name,
                        StateId = quoteInfo.q.FkStateId,
                        City = quoteInfo.q.City,
                        IsProofOfPrior = (quoteInfo.ap != null ? true : false),
                        // ResidenceTypeName = quoteInfo.rt.Name,
                        SourceName = quoteInfo.so.Name,
                        //AssumeCreditCardName = quoteInfo.ac.Name,
                        EmailId = quoteInfo.c.EmailID.ToLower(),
                        ContactNo = !string.IsNullOrEmpty(quoteInfo.c.ContactNo) ? ConvertTo.PhoneNumberFormatting(quoteInfo.c.ContactNo) : "",
                        FirstName = quoteInfo.c.FirstName,
                        LastName = quoteInfo.c.LastName,
                        OfficeId = quoteInfo.c.FKOfficeID,
                        //AgentName = quoteInfo.u.FirstName,
                        NSDPayAmount = quoteInfo._companyDetails == null ? 0 : quoteInfo._companyDetails.NsdAmount,
                        FaxNumber = quoteInfo.q.FaxNumber,
                        OtherPhoneNo = quoteInfo.q.OtherPhoneNo,
                        EffectiveDate = quoteInfo.q.EffectiveDate,
                        ExpirationDate = Convert.ToDateTime(quoteInfo.q.ExpirationDate),
                        AssumeCreditCardName = quoteInfo.ac != null ? quoteInfo.ac.Name : string.Empty,
                        TotalDueAmount = quoteInfo.c.TotalBalanceDueAmount,
                        InitiatedById = quoteInfo.q.InitiatedById,
                        InsuranceCompanyId = quoteInfo.q.FKInsuranceCompanyId ?? 0,
                        PolicyStatusCd = quoteInfo.ps.Code,
                        PolicyMsgDesc = quoteInfo.q.PolicyMsgDesc,
                        IsPaid = (db.QuotePayments.Where(w => w.FKQuoteID == Id && w.FKPaymentStatusID == fkPaymentSuccessStatusId).Count() > 0 ? true : false),
                    };

                    model.QuoteModel = quoteModel;
                    model.AutoQuoteDetail = quoteInfo._autoQuoteDetails.FirstOrDefault();
                    model.AutoQuoteVehicles = quoteInfo._autoVehicle.ToList();
                    model.AutoQuoteDrivers = quoteInfo._autoQuoteDriver.ToList();
                    model.AutoRateDetail = quoteInfo._companyDetails;
                    try
                    {
                        model.IsQuoteCompleted = IsQuoteCompleted(Id);
                        model.IsQuoteInitiated = IsQuoteInitiated(Id);
                    }
                    catch (Exception ex) { }

                    return model;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public long UserIdByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    int OfficeId = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.FkOfficeId).FirstOrDefault();
                    long userId = db.Users.Where(w => w.FKOfficeID == OfficeId).Select(s => s.ID).FirstOrDefault();
                    return userId;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CurrentStepModel GetCurrentStepDetails(long Id)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    int vehicleCount = db.Auto_Vehicle.Where(w => w.FkQuoteID == Id && w.IsDeleted == false).Count();
                    int driverCount = db.Auto_Driver.Where(x => x.FkQuoteID == Id && x.IsDeleted == false).Count();

                    var CurrentStepDetails = (from q in db.Quotes
                                              let cs = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FKCurrentStepId)
                                              where q.ID == Id
                                              select new CurrentStepModel
                                              {
                                                  VehicleCount = vehicleCount,
                                                  DriverCount = driverCount,
                                                  QuoteId = q.ID,
                                                  CusrrentStepId = q.FKCurrentStepId ?? 0,
                                                  CurrentStepCode = cs.Code,
                                                  CurrentStepName = cs.Name,
                                                  CurrentStepOrderId = cs.OrderNo
                                              }).FirstOrDefault();
                    return CurrentStepDetails;

                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quote"></param>
        /// <returns></returns>
        public ApplicantModel UpdateQuoteDetails(ApplicantModel quote)
        {
            try
            {
                AuditHistoryService auditHistoryService = new AuditHistoryService();
                using (var db = new ArrowheadPOSEntities())
                {
                    var _currentDate = ConvertTo.GetEstTimeNow();
                    int stateID = db.States.Where(f => f.Code == quote.StateAbbr).FirstOrDefault().ID;
                    long addressType = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ADDRESSTYPE.ToString(), Enums.AddressType.PERMANENT.ToString());
                    var groupTransactionId = Guid.NewGuid();
                    db.Customers.Where(f => f.ID == quote.CustomerId).Update(u => new Customer()
                    {
                        FirstName = quote.FirstName,
                        LastName = quote.LastName,
                        MiddleName = quote.MiddleName,
                        ContactNo = quote.ContactNo.ToPhoneNumber(),
                        EmailID = quote.EmailId.ToLower(),
                        ModifiedDate = _currentDate,
                        FKStateID = stateID,
                        ModifiedByID = quote.UserId
                    });

                    InsertAuditHistory(Enums.AuditHistoryModule.C, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quote.CustomerId.ToString(), 0, quote.CustomerId, quote.FirstName + " " + quote.LastName);


                    db.Quotes.Where(f => f.ID == quote.QuoteId).Update(u => new Quote()
                    {
                        Address1 = quote.Address1,
                        Address2 = quote.Address2,
                        City = quote.City,
                        ZipCode = quote.ZipCode,
                        ModifiedDate = _currentDate,
                        FkSourceID = quote.SourceId,
                        OtherPhoneNo = quote.ContactNo.ToPhoneNumber(),
                        ExpirationDate = quote.EffectiveDate.Value.AddMonths(ConstantVariables.PolicyTerm),
                        FaxNumber = string.Empty,
                        FkStateId = stateID,
                        EffectiveDate = quote.EffectiveDate,
                        FkHomeTypeId = quote.FkHomeTypeId
                    });

                    InsertAuditHistory(Enums.AuditHistoryModule.AQ, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quote.QuoteId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);


                    //db.QuoteNSDPlans.Where(w => w.FKQuoteId == quote.QuoteId).Update(u => new QuoteNSDPlan
                    //{
                    //    EffectiveDate = quote.EffectiveDate,
                    //    ExpiryDate = quote.EffectiveDate.Value.AddMonths(ConstantVariables.PolicyTerm)
                    //});

                    if (quote.isAddressVerified == true)
                    {
                        auditHistoryService.InsertAuditHistory(quote.QuoteId, (int)quote.AgencyId, quote.UserId);
                    }

                    bool checkDriverExist = checkFirstDriver(quote.QuoteId);
                    if (checkDriverExist == true)
                    {
                        db.Auto_Driver.Where(w => w.FkQuoteID == quote.QuoteId && w.DriverOrderID == 1).Update(u => new Auto_Driver
                        {
                            FirstName = quote.FirstName,
                            LastName = quote.LastName,
                            ModifiedByID = quote.UserId,
                            ModifiedDate = ConvertTo.GetEstTimeNow()
                        });

                        InsertAuditHistory(Enums.AuditHistoryModule.AD, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quote.QuoteId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);

                    }



                    if ((string.IsNullOrWhiteSpace(quote.MailingAddress1)))
                        db.CustomerAddresses.Where(w => w.FKCustomerId == quote.CustomerId).Update(u => new CustomerAddress()
                        {
                            Address1 = quote.Address1,
                            Address2 = quote.Address2,
                            FKAddressTypeId = addressType,
                            IsDuplicate = quote.IsDuplicate,
                            ZipCode = quote.ZipCode,
                            State = quote.StateAbbr,
                            City = quote.City,
                            ModifiedDate = _currentDate,
                            ModifiedById = quote.UserId
                        });


                    else
                    {
                        db.CustomerAddresses.Where(w => w.FKCustomerId == quote.CustomerId).Update(u => new CustomerAddress()
                        {
                            Address1 = quote.MailingAddress1,
                            Address2 = quote.MailingAddress2,
                            FKAddressTypeId = addressType,
                            IsDuplicate = quote.IsDuplicate,
                            ZipCode = quote.MailingZipCode,
                            State = quote.MailingState,
                            City = quote.MailingCity,
                            ModifiedDate = _currentDate,
                            ModifiedById = quote.UserId
                        });

                        InsertAuditHistory(Enums.AuditHistoryModule.CA, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quote.CustomerId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);

                    }

                    if (quote.IspriorPolicy)
                    {
                        ManagePriorInsurance(quote, quote.QuoteId);
                    }
                    else
                    {
                        db.Auto_PriorInsuranceDetails.Where(w => w.FKQuoteId == quote.QuoteId).Delete();

                        InsertAuditHistory(Enums.AuditHistoryModule.API, quote.AgencyId, Enums.AuditHistoryOperation.D.ToString(), AuditHistoryRemarks.Delete.ToString(), groupTransactionId, quote.QuoteId.ToString(), quote.QuoteId, quote.CustomerId, quote.FirstName + " " + quote.LastName);


                    }
                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory((int)quote.AgencyId, Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.S.ToString(), quote.QuoteId, ex.ToString());

            }

            return quote;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public bool checkFirstDriver(long quoteId)
        {
            bool CheckFirstDriver = false;


            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var driversList = db.Auto_Driver.Where(x => x.FkQuoteID == quoteId && x.IsDeleted == false).ToList();
                    if (driversList.Count != 0)
                    {
                        bool firstDriver = driversList.Where(w => w.FkQuoteID == quoteId && w.DriverOrderID == 1).Any();
                        if (firstDriver == true)
                        {
                            CheckFirstDriver = true;
                        }
                        else
                        {
                            CheckFirstDriver = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return CheckFirstDriver;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public QuoteRateResponseModel GetRateHistoryDetailsByQuoteID(long Id)
        {
            var rateDetails = new QuoteRateResponseModel();
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var quoteDetails = db.Auto_QuoteDetail.Where(f => f.FKQuoteID == Id).FirstOrDefault();
                    bool isErrorInNSD = db.QuoteNSDPlans.Where(x => x.FKQuoteId == Id && x.IsSuccess == false).Any();
                    if (((quoteDetails) != null && quoteDetails.FKRateHistoryID > 0))
                        rateDetails = (from rh in db.Auto_RateHistory
                                       join i in db.InsuranceCompanies on rh.FKInsuranceCompanyID equals i.ID
                                       where rh.ID == quoteDetails.FKRateHistoryID
                                       select new QuoteRateResponseModel()
                                       {
                                           BridgeURL = rh.BridgeURL,
                                           CompanyId = rh.FKInsuranceCompanyID,
                                           CarrierQuoteID = rh.CarrierQuoteID,
                                           Description = rh.Description,
                                           CompanyName = i.Name,
                                           DownPayment = rh.DownPayment + rh.NSDFee + rh.PGProcessingFees,
                                           MonthlyPayment = rh.MonthllyPayment,
                                           IsError = false,
                                           NoOfInstallement = rh.NumberOfPayment ?? 0,
                                           TotalPremium = rh.Premium,
                                           PayableDownPayment = rh.DownPayment,
                                           InstallmentFee = rh.InstallmenetFee,
                                           InstallmentDueDt = rh.InstallmentDueDate,
                                           PayPlanId = rh.PayPlanID,
                                           NSDFees = rh.NSDFee,
                                           IsErrorInNSD = isErrorInNSD,
                                           CCCharge = rh.PGProcessingFees

                                       }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }
            }
            return rateDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        public bool IsQuoteCompleted(long quoteID)
        {
            bool IsQuoteCompleted = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    IsQuoteCompleted = (from q in db.Quotes
                                        join gt in db.GeneralStatusTypes on q.FkQuoteStatusID equals gt.ID
                                        where q.ID == quoteID && gt.Code == Enums.QuoteStatus.COMPLETED.ToString()
                                        orderby q.ID descending
                                        select new { gt.Code }).Any();
                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }
            return IsQuoteCompleted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public bool IsQuoteInitiated(long quoteId)
        {
            bool IsQuoteInititated = false;
            long FkInitiatedPaymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    IsQuoteInititated = (from qp in db.QuotePayments
                                         join pgp in db.PaymentGatewayProviders on qp.FKPaymentGateWayID equals pgp.Id
                                         where qp.FKQuoteID == quoteId &&
                                         qp.FKPaymentStatusID == FkInitiatedPaymentStatusId &&
                                         pgp.Code == Enums.PaymentGateway.AUTHORIZENET.ToString()
                                         select new { pgp.Code }).Any();
                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }
            return IsQuoteInititated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rateHistoryID"></param>
        /// <param name="quoteID"></param>
        /// <param name="nsdPrice"></param>
        public void UpdatePurchaseInitiated(long rateHistoryID, long quoteID, decimal nsdPrice, long userId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _currentDate = ConvertTo.GetEstTimeNow();
                    var groupTransactionId = Guid.NewGuid();
                    var ratehistorydetails = db.Auto_RateHistory.Where(w => w.ID == rateHistoryID).FirstOrDefault();
                    db.Auto_RateHistory.Where(f => f.FKQuoteID == quoteID).Update(u => new Auto_RateHistory
                    {
                        NSDFee = nsdPrice
                    });
                    // InsertAuditHistory(Enums.AuditHistoryModule.ARH, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quoteID.ToString(), quoteID, 0, null);

                    long fkpolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.MVR.ToString());
                    db.Quotes.Where(f => f.ID == quoteID).Update(u => new Quote()
                    {
                        FkPolicyStatusId = fkpolicyStatusId,
                        FKInsuranceCompanyId = ratehistorydetails.FKInsuranceCompanyID,
                        ModifiedDate = _currentDate,
                        InitiatedById = userId,
                        InitiatedDate = _currentDate
                    });

                    //   InsertAuditHistory(Enums.AuditHistoryModule.AQ, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quoteID.ToString(), quoteID, 0, null);

                    db.Auto_QuoteDetail.Where(f => f.FKQuoteID == quoteID).Update(f => new Auto_QuoteDetail() { FKRateHistoryID = rateHistoryID });

                    //    InsertAuditHistory(Enums.AuditHistoryModule.AQD, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quoteID.ToString(), quoteID,0,null);


                }

            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<QuoteModel> GetLatestQuoteDetails(long agencyId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    long paymentSuccessStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    long quoteEnvelopeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ENVELOPESTATUS.ToString(), Enums.EnvelopeStatus.ESIGNCOMPLETED.ToString());
                    long paymentInitiatedStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                    long bridgeQuoteStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTETYPE.ToString(), Enums.QuoteType.BRIDGEQUOTE.ToString());

                    var quote = (from q in db.Quotes
                                 let s = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                 let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                 let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                                 let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                 let gs = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FKCurrentStepId)
                                 let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkPolicyStatusId)
                                 let qp = db.QuotePayments.FirstOrDefault(f => f.FKQuoteID == q.ID && (f.FKPaymentStatusID == paymentSuccessStatusId || f.FKPaymentStatusID == paymentInitiatedStatusId))
                                 let pg = db.PaymentGatewayProviders.FirstOrDefault(f => f.Id == qp.FKPaymentGateWayID)
                                 let qe = db.QuoteEnvelopes.FirstOrDefault(f => f.FKQuoteId == q.ID && f.FKEnvelopeStatusId == quoteEnvelopeId)
                                 where a.ID == agencyId
                                 orderby q.ID descending
                                 select new QuoteModel
                                 {
                                     QuoteId = q.ID,
                                     CustomerId = c.ID,
                                     StateId = q.FkStateId,
                                     City = q.City,
                                     ZipCode = q.ZipCode,
                                     Address1 = q.Address1,
                                     Address2 = q.Address2,
                                     StateAbbr = s.Code,
                                     EmailId = c.EmailID.ToLower(),
                                     ContactNo = c.ContactNo,
                                     FirstName = c.FirstName,
                                     PolicyTerm = q.PolicyTerm,
                                     LastName = c.LastName,
                                     SourceId = q.FkSourceID,
                                     OtherPhoneNo = q.OtherPhoneNo,
                                     PaymentGatewayCd = pg.Code,
                                     AssumeCreditId = q.FKAssumeCreditId ?? 0,
                                     CreatedDate = q.CreatedDate,
                                     CurrentStepOrderNo = gs.OrderNo,
                                     PolicyStatusCd = ps.Code,
                                     PolicyStatusId = q.FkPolicyStatusId,
                                     IsPaymentComplete = (qp != null && qp.FKPaymentStatusID == paymentSuccessStatusId) ? true : false,
                                     IsDocusignComplete = (qe != null) ? true : false,
                                     IsBridgeQuote = (q != null && q.FkQuoteTypeId == bridgeQuoteStatusId) ? true : false

                                 }).Take(20).ToList();

                    return quote;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public List<QuoteModel> SearchLatestQuoteDetails(string searchString, long agencyId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    long paymentSuccessStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    long quoteEnvelopeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ENVELOPESTATUS.ToString(), Enums.EnvelopeStatus.ESIGNCOMPLETED.ToString());
                    long paymentInitiatedStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());

                    var query = (from q in db.Quotes
                                 let s = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                 let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                 let gs = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FKCurrentStepId)
                                 let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkPolicyStatusId)
                                 let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                                 let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                 let qp = db.QuotePayments.FirstOrDefault(f => f.FKQuoteID == q.ID && (f.FKPaymentStatusID == paymentSuccessStatusId || f.FKPaymentStatusID == paymentInitiatedStatusId))
                                 let qe = db.QuoteEnvelopes.FirstOrDefault(f => f.FKQuoteId == q.ID && f.FKEnvelopeStatusId == quoteEnvelopeId)
                                 where a.ID == agencyId
                                 orderby q.ID descending
                                 select new { q, c, s, gs, ps, qe, qp }).AsQueryable();

                    if (!(string.IsNullOrWhiteSpace(searchString)))
                        query = query.Where(f => f.c.FirstName.ToLower().Contains(searchString) || f.c.LastName.ToLower().Contains(searchString) || f.q.ID.ToString().ToLower().Contains(searchString) || (f.c.FirstName + " " + f.c.LastName).ToLower().Contains(searchString) || f.q.PolicyNumber.ToLower().Contains(searchString) || f.c.ContactNo.ToLower().Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Trim().Contains(searchString) || f.c.EmailID.ToLower().Contains(searchString));

                    var quoteList = query.Select(f => new QuoteModel()
                    {
                        QuoteId = f.q.ID,
                        CustomerId = f.q.FkCustomerID,
                        StateId = f.q.FkStateId,
                        City = f.q.City,
                        ZipCode = f.q.ZipCode,
                        Address1 = f.q.Address1,
                        Address2 = f.q.Address2,
                        StateAbbr = f.s.Code,
                        EmailId = f.c.EmailID.ToLower(),
                        ContactNo = f.c.ContactNo,
                        FirstName = f.c.FirstName,
                        EffectiveDate = f.q.EffectiveDate,
                        PolicyTerm = f.q.PolicyTerm,
                        LastName = f.c.LastName,
                        SourceId = f.q.FkSourceID,
                        OtherPhoneNo = f.q.OtherPhoneNo,
                        //  AssumeCreditId = f.q.FKAssumeCreditId ?? 0,
                        CreatedDate = f.q.CreatedDate,
                        CurrentStepOrderNo = f.gs.OrderNo,
                        PolicyStatusCd = f.ps.Code,
                        PolicyStatusId = f.q.FkPolicyStatusId,
                        IsPaymentComplete = (f.qp != null) ? true : false,
                        IsDocusignComplete = (f.qe != null) ? true : false
                    }).Take(20).ToList();

                    return quoteList;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public User GetAgentByID(int Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    return db.Users.Where(f => f.ID == Id).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Get Agency Details By User ID 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public AgencyModel GetAgencyDetailsByUserID(int Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var agency = (from u in db.Users
                                  join o in db.Offices on u.FKOfficeID equals o.ID
                                  join a in db.Agencies on o.FKAgencyID equals a.ID
                                  let s = db.States.FirstOrDefault(f => f.ID == a.FKStateId)
                                  where u.ID == Id
                                  select new AgencyModel()
                                  {
                                      AgencyCode = a.Code,
                                      AgencyName = a.CommercialName,
                                      //AgencyFirstName = u.FirstName,
                                      //AgencyLastName = u.LastName,
                                      ContactNo = u.ContactNo,
                                      AgencyEmailId = a.EmailID,
                                      AgencyAddress = a.Address1 + " " + a.Address2 + " " + a.City + " " + s.Code + " " + a.Zipcode,
                                      AgencyId = a.ID,
                                      OfficeId = o.ID
                                  }).FirstOrDefault();
                    return agency;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Get Agency Details By User ID 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public AgencyModel GetAgencyDetailsByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var agency = (from q in db.Quotes
                                  join o in db.Offices on q.FkOfficeId equals o.ID
                                  join a in db.Agencies on o.FKAgencyID equals a.ID
                                  let s = db.States.FirstOrDefault(f => f.ID == a.FKStateId)
                                  where q.ID == quoteId
                                  select new AgencyModel()
                                  {
                                      AgencyCode = a.Code,
                                      AgencyName = a.CommercialName,
                                      AgencyEmailId = a.EmailID,
                                      AgencyAddress = a.Address1 + " " + a.Address2 + " " + a.City + " " + s.Code + " " + a.Zipcode,
                                      AgencyId = a.ID,
                                      OfficeId = o.ID,
                                      ContactNo = a.ContactNo
                                  }).FirstOrDefault();
                    return agency;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public Quote GetQuoteById(long Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Quotes.Where(w => w.ID == Id).FirstOrDefault();
            }
        }


        public TabModel GetQuoteDetailForTab(long Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var quote = (from q in db.Quotes
                                 let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                 where q.ID == Id
                                 select new TabModel()
                                 {
                                     QuoteID = q.ID,
                                     FullName = c.FirstName + " " + c.LastName
                                 }).FirstOrDefault();
                    return quote;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Get Agency Details by Agency ID
        /// </summary>
        /// <returns></returns>
        public AgencyModel GetAgencyDetails(long agencyID)
        {
            CommonServices _commonServices = new CommonServices();
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var agency = (from a in db.Agencies
                                  let o = db.Offices.FirstOrDefault(f => f.FKAgencyID == a.ID)
                                  let s = db.States.FirstOrDefault(f => f.ID == a.FKStateId)
                                  let u = db.Users.Where(f => f.FKOfficeID == o.ID).FirstOrDefault()
                                  where a.ID == agencyID
                                  select new AgencyModel()
                                  {
                                      AgencyCode = a.Code,
                                      AgencyName = a.CommercialName,
                                      ContactNo = a.ContactNo,
                                      ZipCode = a.Zipcode,
                                      City = a.City,
                                      Address1 = a.Address1,
                                      Address2 = a.City + " " + s.Code + " " + a.Zipcode,
                                      Address3 = a.Address2,
                                      AgencyEmailId = a.EmailID,
                                      AgencyId = a.ID,
                                      StateId = a.FKStateId ?? 0,
                                      OfficeId = o.ID,
                                      FaxNo = a.Fax,
                                      StateAbbr = s.Code,
                                      UserId = (u != null ? u.ID : ConstantVariables.UserID),
                                      NSDPlanId = a.FKDefaultNSDPlanId
                                  }).FirstOrDefault();

                    if (agency != null)
                    {
                        var producerCode = _commonServices.getProducerCodeByAgencyId(agencyID);
                        agency.ProducerCode = producerCode;
                    }
                    return agency;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="contractNo"></param>
        /// <returns></returns>
        public AgencyModel GetAgencyDetailsByContratNo(string contractNo)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    int _agencyID = (from d in db.RaterProviderCredentials
                                     join m in db.RaterProviderModes on d.FKRaterProviderModeID equals m.ID
                                     where d.ValueText == contractNo && m.FKAgencyID != null
                                     select m.FKAgencyID ?? 0
                                     ).FirstOrDefault();

                    var agencyDetails = (from a in db.Agencies
                                         let o = db.Offices.FirstOrDefault(f => f.FKAgencyID == a.ID)
                                         let u = db.Users.Where(f => f.FKOfficeID == o.ID).FirstOrDefault()
                                         where a.ID == _agencyID
                                         select new AgencyModel
                                         {
                                             AgencyCode = a.Code,
                                             AgencyEmailId = a.EmailID,
                                             AgencyName = a.CommercialName,
                                             AgencyId = a.ID,
                                             StateId = (int)a.FKStateId,
                                             OfficeId = o.ID,
                                             ContactNo = a.ContactNo,
                                             //ProducerCode = Convert.ToInt32(contractNo),
                                             IsSetupCompleted = a.IsSetupCompleted,
                                             UserId = (u != null ? u.ID : ConstantVariables.UserID)
                                         }).FirstOrDefault();

                    if (agencyDetails != null && agencyDetails.ProducerCode == 0)
                    {
                        agencyDetails.ProducerCode = Convert.ToInt32(contractNo);
                    }

                    return agencyDetails;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="contractNo"></param>
        /// <returns></returns>
        public AgencyModel GetUserDetailByAgencyId(int agencyId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {

                    var agencyDetails = (from a in db.Agencies
                                         let o = db.Offices.FirstOrDefault(f => f.FKAgencyID == a.ID)
                                         let u = db.Users.Where(f => f.FKOfficeID == o.ID).FirstOrDefault()
                                         where a.ID == agencyId
                                         select new AgencyModel
                                         {
                                             AgencyCode = a.Code,
                                             AgencyEmailId = a.EmailID,
                                             AgencyName = a.CommercialName,
                                             AgencyId = a.ID,
                                             StateId = (int)a.FKStateId,
                                             UserId = (u != null ? u.ID : ConstantVariables.UserID),
                                             OfficeId = o.ID,
                                             IsSetupCompleted = a.IsSetupCompleted

                                         }).FirstOrDefault();
                    return agencyDetails;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentStepCode"></param>
        /// <returns></returns>
        public long QuoteStepId(string currentStepCode)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                long quoteStepId = generalServices.GetGeneralStatusOrderIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), currentStepCode.ToString());
                return quoteStepId;
            }
        }

        /// <summary>
        /// Get Bank Details by Agency Id
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public BankDetailModel GetBankDetailsByAgencyId(long agencyId)
        {
            string secretKey = ConstantVariables.AgencyEncryptionKey;
            BankDetailModel _bankDetailModel = new BankDetailModel(); // fill decrypted bank detail 
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _agencyDetails = (from ag in db.Agencies
                                          join agd in db.AgencyBankDetails on ag.ID equals agd.FKAgencyID
                                          where ag.ID == agencyId
                                          select new BankDetailModel()
                                          {
                                              BankName = agd.BankName,
                                              AccountHolderName = agd.ACHolderName,
                                              AccountType = agd.FKACHolderType,
                                              AccountNumber = agd.ACNumber,
                                              RoutingNumber = agd.BankRoutingNumber
                                          }).FirstOrDefault();

                    if (_agencyDetails != null)
                    {
                        _bankDetailModel.BankName = CryptoGrapher.DecryptText(_agencyDetails.BankName, secretKey);
                        _bankDetailModel.AccountHolderName = CryptoGrapher.DecryptText(_agencyDetails.AccountHolderName, secretKey);
                        _bankDetailModel.AccountTypeId = Convert.ToInt32(CryptoGrapher.DecryptText(_agencyDetails.AccountType, secretKey));
                        _bankDetailModel.AccountNumber = CryptoGrapher.DecryptText(_agencyDetails.AccountNumber, secretKey);
                        _bankDetailModel.RoutingNumber = CryptoGrapher.DecryptText(_agencyDetails.RoutingNumber, secretKey);
                    }
                    return _bankDetailModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        public bool UpdateEffectiveAndExpireDateByQuoteId(long quoteId)
        {
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            bool isEffectivedDateUpdate = false;
            using (var db = new ArrowheadPOSEntities())
            {
                DateTime effectiveDate = ConvertTo.GetEstTimeNow();
                DateTime expirationDate = ConvertTo.GetEstTimeNow().AddMonths(ConstantVariables.PolicyTerm);
                var _paymentDetail = _autoPolicyServices.GetPaymentDetailByQuoteId(quoteId);
                var groupTransactionId = Guid.NewGuid();

                if (_paymentDetail != null && ((_paymentDetail.PaymentStatus == Enums.PAYMENTSTATUS.SUCCESS.ToString() && _paymentDetail.PaymentMethodCd == Enums.PaymentMethod.SWEEP.ToString()) ||
                   ((_paymentDetail.PaymentStatus == Enums.PAYMENTSTATUS.INITIATED.ToString() || _paymentDetail.PaymentStatus == Enums.PAYMENTSTATUS.SUCCESS.ToString()) && _paymentDetail.PaymentMethodCd != Enums.PaymentMethod.SWEEP.ToString())))
                {
                    // do nothing
                }
                else
                {
                    db.Quotes.Where(w => w.ID == quoteId).Update(u => new Quote
                    {
                        EffectiveDate = effectiveDate,
                        ExpirationDate = expirationDate
                    });

                    isEffectivedDateUpdate = true;
                }
                // InsertAuditHistory(Enums.AuditHistoryModule.API, quote.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, quoteId.ToString(), quoteId, 0, null);
            }
            return isEffectivedDateUpdate;
        }


        public long SaveQuickQuoteDetails(long agencyId, List<QuickQuoteDriverModel> quickQuoteDriverModels, List<QuickQuoteVehicleModel> quickQuoteVehicleModels, List<StateCoveragesModel> quickQuoteCoverageModel)
        {

            try
            {
                AgencyModel _agencyModel = new AgencyModel();
                AutoVehicleServices _autoVehicleServices = new AutoVehicleServices();
                AutoCoverageServices _autoCoverageServices = new AutoCoverageServices();
                CommonServices _commonServices = new CommonServices();
                GeneralServices _generalServices = new GeneralServices();
                long quoteStatusID, quoteTypeID, fkCurrentStatusId, fkInsuredDriverStatusId, addressType, insuranceTypeId = 0;
                long QuoteId = 0;
                long CustomerID = 0;
                var lstGeneralStatus = generalServices.GetGeneralStatusList(); // Common Method for General Status
                quoteStatusID = lstGeneralStatus.Where(g => g.Type == Enums.GeneralStatus.QUOTESTATUS.ToString() && g.Code == Enums.QuoteStatus.NEW.ToString()).Select(x => x.FkGeneralStatusId).FirstOrDefault();
                quoteTypeID = lstGeneralStatus.Where(g => g.Type == Enums.GeneralStatus.QUOTETYPE.ToString() && g.Code == Enums.QuoteType.QUICKQUOTE.ToString()).Select(x => x.FkGeneralStatusId).FirstOrDefault();
                fkCurrentStatusId = lstGeneralStatus.Where(g => g.Type == Enums.GeneralStatus.QUOTEORDERSTATUS.ToString() && g.Code == Enums.QuoteOrderStatus.APPLICANT.ToString()).Select(x => x.FkGeneralStatusId).FirstOrDefault();
                fkInsuredDriverStatusId = lstGeneralStatus.Where(g => g.Type == Enums.GeneralStatus.DRIVERRELATION.ToString() && g.Code == Enums.DRIVERRELATION.INSURED.ToString()).Select(x => x.FkGeneralStatusId).FirstOrDefault();
                addressType = lstGeneralStatus.Where(g => g.Type == Enums.GeneralStatus.ADDRESSTYPE.ToString() && g.Code == Enums.AddressType.PERMANENT.ToString()).Select(x => x.FkGeneralStatusId).FirstOrDefault();
                insuranceTypeId = _commonServices.GetAutoInsuranceID();

                var _createDate = ConvertTo.GetEstTimeNow();

                // declare policy term 
                int policyTerm = Core.ConstantVariables.PolicyTerm;


                // Fetch Agency Details
                _agencyModel = _commonServices.GetAgencyDetailById(agencyId);

                var applicantModel = (quickQuoteDriverModels != null && quickQuoteDriverModels.Count > 0) ? quickQuoteDriverModels.FirstOrDefault() : null; // consider first driver as applicant

                using (var db = new ArrowheadPOSEntities())
                {
                    //Save Customer,Quote
                    if (applicantModel != null)
                    {
                        TimeSpan effectiveDateTime = ConvertTo.GetEstTimeNow().TimeOfDay;
                        var effectiveDate = ConvertTo.GetEstTimeNow().Add(effectiveDateTime);
                        var zipCodeDetails = !string.IsNullOrEmpty(applicantModel.ZipCode) ? _commonServices.IsZipCodeCheck(applicantModel.ZipCode) : null;
                        var stateId = _commonServices.GetStateIdByCode(zipCodeDetails.state.ToUpper());

                        Customer customer = new Customer
                        {
                            FirstName = applicantModel.FirstName,
                            LastName = applicantModel.LastName,
                            MiddleName = string.Empty,
                            Gender = applicantModel.Gender.Substring(0, 1).ToUpper(),
                            FKStateID = stateId,
                            FKQuoteId = QuoteId,
                            FKOfficeID = _agencyModel.OfficeId,
                            EmailID = ConstantVariables.EmailId,
                            //ContactNo = ConstantVariables.PhoneNumber,
                            ContactNo = null,
                            CreatedDate = _createDate,
                            IsDeleted = false
                        };
                        db.Customers.Add(customer);
                        db.SaveChanges();
                        CustomerID = customer.ID;

                        CustomerAddress customerAddress = new CustomerAddress
                        {
                            Address1 = string.Empty,
                            Address2 = string.Empty,
                            City = zipCodeDetails.city,
                            ZipCode = applicantModel.ZipCode,
                            FKOfficeID = _agencyModel.OfficeId,
                            State = zipCodeDetails.state,
                            FKCustomerId = CustomerID,
                            FKAddressTypeId = addressType,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            IsDuplicate = true
                        };
                        db.CustomerAddresses.Add(customerAddress);
                        db.SaveChanges();


                        Quote quoteDetail = new Quote
                        {
                            FkQuoteStatusID = quoteStatusID,
                            FkPolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.NEW.ToString()),
                            FkInsuranceTypeID = db.InsuranceTypes.Where(w => w.Code == "AUTOINSURANCE").Select(s => s.Id).FirstOrDefault(),
                            FKInsuranceCompanyId = db.InsuranceCompanies.Where(w => w.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
                            FkSourceID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.APPLICANTSOURCESTATUS.ToString(), Enums.ApplicationSource.OTHER.ToString()),
                            Address1 = string.Empty,
                            Address2 = string.Empty,
                            ZipCode = applicantModel.ZipCode,
                            FkStateId = stateId,
                            City = zipCodeDetails.city,
                            AssignByID = ConstantVariables.UserID,
                            CreatedDate = _createDate,
                            CreatedById = ConstantVariables.UserID,
                            FkOfficeId = _agencyModel.OfficeId,
                            FKCurrentStepId = fkCurrentStatusId,
                            FkQuoteTypeId = quoteTypeID,
                            EffectiveDate = effectiveDate.Add(effectiveDateTime),
                            ExpirationDate = effectiveDate.AddMonths(ConstantVariables.PolicyTerm),
                            PolicyStatusChangeDate = _createDate,
                            FkCustomerID = CustomerID,
                            PolicyTerm = ConstantVariables.PolicyTerm,

                        };
                        db.Quotes.Add(quoteDetail);
                        db.SaveChanges();
                        QuoteId = quoteDetail.ID;

                        // Update QuoteId in Customer 
                        db.Customers.Where(x => x.ID == CustomerID).Update(c => new Customer
                        {
                            FKQuoteId = QuoteId
                        });
                    }

                    if (QuoteId > 0) // Save Vehicle / Driver Details / Coverage
                    {
                        var driverCount = 0;
                        foreach (var items in quickQuoteDriverModels)
                        {
                            if (items.ZipCode == null)
                            {
                                var zipCode = db.Quotes.Where(x => x.ID == QuoteId).Select(x => x.ZipCode).FirstOrDefault();
                                items.ZipCode = zipCode;
                            }

                            var zipCodeDetails = !string.IsNullOrEmpty(items.ZipCode) ? _commonServices.IsZipCodeCheck(items.ZipCode) : null;
                            var stateId = _commonServices.GetStateIdByCode(zipCodeDetails.state.ToUpper());
                            int _driverOrderId = driverCount + 1;
                            long fkmaritalstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MARITALSTATUS.ToString(), items.MaritalStatusCode.ToString());

                            Auto_Driver _autoDriverModel = new Auto_Driver()
                            {
                                FkQuoteID = QuoteId,
                                FkStateID = stateId,
                                BirthDate = items.Dob,
                                FirstName = items.FirstName,
                                LastName = items.LastName,
                                Gender = items.Gender.Substring(0, 1),
                                LicenseNo = string.Empty,
                                FkMaritalStatusID = fkmaritalstatusId,
                                FkHomeTypeID = null,
                                IsExcluded = items.IsExcluded,
                                FkDriverEducationID = null,
                                FkCreditScoreID = null,
                                DriverOrderID = _driverOrderId,
                                FkRelationCdID = (items.RelationCDId == 0 ? fkInsuredDriverStatusId : items.RelationCDId),
                                SocialSecurity = string.Empty,
                                IsDeleted = false,
                                IsSR22 = false,
                                IsSR22A = false,
                                SR22Reason = string.Empty,
                                FKStateSR22AId = null,
                                FkStateSR22Id = null,
                                CreatedDate = _createDate,
                                DateFirstLicense = DateTime.Now,
                                Employer = string.Empty,
                                EmployerYears = 0,
                                MilesToWork = 0,
                                YearsLicensed = 0,
                                LicenseStatus = 0,
                                IsDefensiveDriverCourse = false,
                                FkDriverOccupationID = 0,
                                DrivertrainingDate = DateTime.Now,
                                IsWorkLossBenefit = false
                            };
                            db.Auto_Driver.Add(_autoDriverModel);
                            db.SaveChanges();
                            driverCount++;
                        }

                        int vehCount = 0;
                        foreach (var items in quickQuoteVehicleModels)
                        {
                            long fkOwnerTypeOwnedId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.OWNERTYPE.ToString(), Enums.OwnerType.OWNED.ToString());
                            long stateId = db.Quotes.Where(x => x.ID == QuoteId).Select(x => x.FkStateId).FirstOrDefault();
                            long lienHolderId = (db.LienHolders.Where(x => x.FkStateID == stateId).Select(x => x.ID).FirstOrDefault());
                            Auto_Vehicle autoVehicle = new Auto_Vehicle()
                            {
                                VehicleOrderID = (vehCount + 1),
                                FkQuoteID = QuoteId,
                                Year = items.Year,
                                Make = items.Make,
                                Model = items.Model,
                                VIN = items.Vin,
                                FkOwnerTypeID = items.FkOwnerTypeID,
                                FkLienHolderID = (items.FkOwnerTypeID != fkOwnerTypeOwnedId) ? Convert.ToInt32(lienHolderId) : 0,
                                LienHolderName = string.Empty,
                                LienHoldersAddress = string.Empty,
                                CreatedDate = _createDate,
                                CreatedByID = ConstantVariables.UserID,
                                IsDeleted = false
                            };
                            db.Auto_Vehicle.Add(autoVehicle);
                            db.SaveChanges();
                            var vehId = autoVehicle.ID;
                            items.AutoVehicleId = vehId;
                            items.VehicleOrderId = (vehCount + 1);
                            vehCount++;
                        }

                        // Prepare Coverages 
                        foreach (var items in quickQuoteVehicleModels)
                        {
                            int stateId = db.Quotes.Where(x => x.ID == QuoteId).Select(x => x.FkStateId).FirstOrDefault();
                            List<CoveragesFormControlValues> lstCoverageFormControlValues = new List<CoveragesFormControlValues>();
                            var coverageCOLLDetails = _autoCoverageServices.GetCoverageDetailsByValue(items.COMPCOLLValue, Enums.Coverages.COLL.ToString(), stateId);
                            var splitCOMPCollValue = items.COMPCOLLValue.Split('/');

                            // Check COMP COLL Value && Set COLL-LEVEL (Standard)
                            if (Convert.ToDecimal(splitCOMPCollValue[0]) > 0)
                            {
                                // ### COLL 
                                CoveragesFormControlValues covFormCOLLControlValues = new CoveragesFormControlValues()
                                {
                                    IsSelected = true,
                                    Text = coverageCOLLDetails.DisplayText,
                                    Value = items.COMPCOLLValue
                                };
                                lstCoverageFormControlValues.Add(covFormCOLLControlValues); // add coverage into list of Form Control Values

                                StateCoveragesModel stateCOLLCoverageModel = new StateCoveragesModel()
                                {
                                    Code = coverageCOLLDetails.CoverageCode,
                                    CoverageName = coverageCOLLDetails.CoverageName,
                                    CoverageFormControl = coverageCOLLDetails.ControlType,
                                    CoverageId = coverageCOLLDetails.CoverageId,
                                    CoveragesFormControlValues = lstCoverageFormControlValues,
                                    GroupName = coverageCOLLDetails.GroupName,
                                    IsEdit = false,
                                    IsDuplicate = true,
                                    ShortName = coverageCOLLDetails.CoverageCode.ToUpper(),
                                    IsVehicleCoverage = coverageCOLLDetails.IsVehicleCoverage,
                                    StateId = stateId,
                                    VehicleID = items.AutoVehicleId,
                                    VehicleOrderID = items.VehicleOrderId,
                                    VehicleTitle = items.Year + " " + items.Make + " " + items.Model,
                                    Selected = items.COMPCOLLValue,
                                    QuoteSelectedValue = items.COMPCOLLValue
                                };
                                quickQuoteCoverageModel.Add(stateCOLLCoverageModel);

                                var coverageCOMPDetails = _autoCoverageServices.GetCoverageDetailsByValue(items.COMPCOLLValue, Enums.Coverages.COMP.ToString(), stateId);

                                // ### COMP
                                lstCoverageFormControlValues = new List<CoveragesFormControlValues>();
                                CoveragesFormControlValues covFormCOMPControlValues = new CoveragesFormControlValues()
                                {
                                    IsSelected = true,
                                    Text = coverageCOMPDetails.DisplayText,
                                    Value = items.COMPCOLLValue
                                };
                                lstCoverageFormControlValues.Add(covFormCOMPControlValues); // add coverage into list of Form Control Values

                                StateCoveragesModel stateCOMPCoverageModel = new StateCoveragesModel()
                                {
                                    Code = coverageCOMPDetails.CoverageCode,
                                    CoverageFormControl = coverageCOMPDetails.ControlType,
                                    CoverageId = coverageCOMPDetails.CoverageId,
                                    CoveragesFormControlValues = lstCoverageFormControlValues,
                                    GroupName = coverageCOMPDetails.GroupName,
                                    IsEdit = false,
                                    IsDuplicate = true,
                                    ShortName = coverageCOMPDetails.CoverageCode.ToUpper(),
                                    IsVehicleCoverage = coverageCOMPDetails.IsVehicleCoverage,
                                    StateId = stateId,
                                    VehicleID = items.AutoVehicleId,
                                    VehicleOrderID = items.VehicleOrderId,
                                    VehicleTitle = items.Year + " " + items.Make + " " + items.Model,
                                    Selected = items.COMPCOLLValue,
                                    QuoteSelectedValue = items.COMPCOLLValue
                                };
                                quickQuoteCoverageModel.Add(stateCOMPCoverageModel);

                                // ### Coll-Level
                                var CoverageDetail = _autoCoverageServices.GetCoverageDetailByCode(Enums.Coverages.COLLLEVEL.ToString(), Enums.ColLevel.Standard.ToString(), stateId);
                                lstCoverageFormControlValues = new List<CoveragesFormControlValues>();
                                CoveragesFormControlValues coverage_COLLLEVELFormControlValues = new CoveragesFormControlValues()
                                {
                                    IsSelected = true,
                                    Text = CoverageDetail.DisplayText,
                                    Value = (CoverageDetail.Value1 + "/" + CoverageDetail.Value2)
                                };
                                lstCoverageFormControlValues.Add(coverage_COLLLEVELFormControlValues); // add coverage into list of Form Control Values

                                StateCoveragesModel stateCollLevelCoveragesModel = new StateCoveragesModel()
                                {
                                    Code = CoverageDetail.CoverageCode,
                                    CoverageFormControl = CoverageDetail.ControlType,
                                    CoverageId = CoverageDetail.CoverageId,
                                    CoveragesFormControlValues = lstCoverageFormControlValues,
                                    GroupName = CoverageDetail.GroupName,
                                    IsEdit = false,
                                    IsDuplicate = true,
                                    ShortName = CoverageDetail.CoverageCode,
                                    IsVehicleCoverage = CoverageDetail.IsVehicleCoverage,
                                    StateId = stateId,
                                    VehicleID = items.AutoVehicleId,
                                    VehicleOrderID = items.VehicleOrderId,
                                    VehicleTitle = items.Year + " " + items.Make + " " + items.Model,
                                    Selected = (CoverageDetail.Value1 + "/" + CoverageDetail.Value2),
                                    QuoteSelectedValue = (CoverageDetail.Value1 + "/" + CoverageDetail.Value2)
                                };
                                quickQuoteCoverageModel.Add(stateCollLevelCoveragesModel);
                            }
                            else // Set COLL-LEVEL (NONE)
                            {
                                var CoverageDetail = _autoCoverageServices.GetCoverageDetailByCode(Enums.Coverages.COLLLEVEL.ToString(), Enums.ColLevel.None.ToString(), stateId);
                                lstCoverageFormControlValues = new List<CoveragesFormControlValues>();
                                CoveragesFormControlValues coverage_COLLLEVELFormControlValues = new CoveragesFormControlValues()
                                {
                                    IsSelected = true,
                                    Text = CoverageDetail.DisplayText,
                                    Value = (CoverageDetail.Value1 + "/" + CoverageDetail.Value2)
                                };
                                lstCoverageFormControlValues.Add(coverage_COLLLEVELFormControlValues);
                                StateCoveragesModel stateCollLevelCoveragesModel = new StateCoveragesModel()
                                {
                                    Code = CoverageDetail.CoverageCode,
                                    CoverageFormControl = CoverageDetail.ControlType,
                                    CoverageId = CoverageDetail.CoverageId,
                                    CoveragesFormControlValues = lstCoverageFormControlValues,
                                    GroupName = CoverageDetail.GroupName,
                                    IsEdit = false,
                                    IsDuplicate = true,
                                    ShortName = CoverageDetail.CoverageCode,
                                    IsVehicleCoverage = CoverageDetail.IsVehicleCoverage,
                                    StateId = stateId,
                                    VehicleID = items.AutoVehicleId,
                                    VehicleOrderID = items.VehicleOrderId,
                                    VehicleTitle = items.Year + " " + items.Make + " " + items.Model,
                                    Selected = items.COMPCOLLValue,
                                    QuoteSelectedValue = items.COMPCOLLValue
                                };
                                quickQuoteCoverageModel.Add(stateCollLevelCoveragesModel);
                            }
                        }
                        // Insert Quote Coverage
                        if (quickQuoteCoverageModel != null && quickQuoteCoverageModel.Count > 0)
                        {
                            _autoCoverageServices.InsertQuoteCoverage(quickQuoteCoverageModel, QuoteId);
                        }

                        return QuoteId;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        /// <summary>
        /// Quote Rates Exist or not by Quote Id
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public bool IsQuoteRatesExist(long QuoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (db.Auto_RateHistory.Where(x => x.FKQuoteID == QuoteId).Count() > 0 ? true : false);
            }
        }

        public void UpdateQuoteTypeByQuoteId(long QuoteId, long? QuoteTypeId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.Quotes.Where(x => x.ID == QuoteId).Update(x => new Quote()
                {
                    FkQuoteTypeId = QuoteTypeId
                });
            }
        }

        public bool IsQuickQuoteByQuoteId(long QuoteId)
        {
            GeneralServices _generalServices = new GeneralServices();
            long quickQuoteStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTETYPE.ToString(), Enums.QuoteType.QUICKQUOTE.ToString());

            using (var db = new ArrowheadPOSEntities())
            {
                return (db.Quotes.Where(x => x.ID == QuoteId && x.FkQuoteTypeId == quickQuoteStatusId).Count() > 0 ? true : false);
            }
        }

        public bool UpdateQuoteContactDetails(long QuoteId, string contactNo)
        {
            bool isContactNoUpdated = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var custId = db.Quotes.Where(x => x.ID == QuoteId).Select(x => x.FkCustomerID).FirstOrDefault();
                    if (custId > 0)
                    {
                        db.Customers.Where(x => x.ID == custId).Update(x => new Customer()
                        {
                            ContactNo = contactNo
                        });
                        isContactNoUpdated = true;
                    }
                }
                return isContactNoUpdated;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        #region "Report Functions"

        /// <summary>
        /// Get Policy Report
        /// </summary>
        /// <returns></returns>
        //public PolicyReportModel GetPolicyReport(DateTime startDate, DateTime endDate, long agencyId, int pageNumber = 1, int pageRows = 20, string orderBy = "CompletedDate descending", string searchtext = " ")
        //{
        //    using (var db = new ArrowheadPOSEntities())
        //    {
        //        PolicyReportModel policyViewModel = new PolicyReportModel();
        //        PagerList<PolicyViewModel> paginationObject = null;
        //        GeneralServices generalServices = new GeneralServices();
        //        try
        //        {

        //            var startDateFilter = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
        //            var endDateFilter = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
        //            var currentWeekStartDate = Arrowhead.POS.Process.CommonFun.StartOfWeek(ConvertTo.GetEstTimeNow(), DayOfWeek.Monday);
        //            var currentWeekStartDateFilter = new DateTime(currentWeekStartDate.Year, currentWeekStartDate.Month, currentWeekStartDate.Day, 0, 0, 0);
        //            var currentWeekLastDate  = currentWeekStartDate.AddDays(6);
        //            var currentWeekLastDateFilter = new DateTime(currentWeekLastDate.Year, currentWeekLastDate.Month, currentWeekLastDate.Day, 23, 59, 59);


        //            int _processFilterByOrderNo = Enums.PolicyStatusFilter.PROCESS.GetHashCode();
        //            int _policyFilterByOrderNo = Enums.PolicyStatusFilter.POLICY.GetHashCode();


        //            long fkpolicystatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
        //            long paymentSuccessStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
        //            long paymentInitiatedStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
        //            long paymentNSDId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.NSD.ToString());
        //            long paymentCarrierId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CARRIER.ToString());
        //            long paymentCCTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CREDITCARDFEES.ToString());
        //            long paymentSweepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.SWEEP.ToString());
        //            long paymentEcheckId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.ECHECK.ToString());
        //            long paymentCcId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.CREDITCARD.ToString());
        //            long raterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault();
        //            long raterProviderModeId = db.RaterProviderModes.Where(w => w.FKAgencyID == agencyId).Select(s => s.ID).FirstOrDefault();
        //            string producerCode = db.RaterProviderCredentials.Where(w => w.FKRaterProviderFormId == raterProviderFormId && w.FKRaterProviderModeID == raterProviderModeId).Select(s => s.ValueText).FirstOrDefault();

        //            var policyView = (from q in db.Quotes
        //                              let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
        //                              let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
        //                              let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
        //                              let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
        //                              let s = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkSourceID)
        //                              let p = db.QuotePayments.Where(f => f.FKQuoteID == q.ID).OrderByDescending(x => x.ID).Select(s => new { s.FKPaymentMethodID, s.CreatedDate, s.FKPaymentStatusID }).FirstOrDefault()
        //                              let pm = db.QuotePayments.Where(f => f.FKQuoteID == q.ID && (f.FKPaymentStatusID == paymentSuccessStatusId || f.FKPaymentStatusID == paymentInitiatedStatusId)).Count() > 0 ? true : false
        //                              let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == p.FKPaymentMethodID)
        //                              let aq = db.Auto_QuoteDetail.FirstOrDefault(f => f.FKQuoteID == q.ID)
        //                              let rh = db.Auto_RateHistory.FirstOrDefault(f => f.ID == aq.FKRateHistoryID)
        //                              let pst = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
        //                              let ic = db.InsuranceCompanies.FirstOrDefault(f => f.ID == q.FKInsuranceCompanyId)
        //                              let qe = db.QuoteEnvelopes.FirstOrDefault(f => f.FKQuoteId == q.ID)
        //                              let es = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == qe.FKEnvelopeStatusId)
        //                              where fkpolicystatusId == q.FkPolicyStatusId
        //                              select new
        //                              {
        //                                  q,
        //                                  c,
        //                                  rh,
        //                                  s,
        //                                  st,
        //                                  pst,
        //                                  ic,
        //                                  es,
        //                                  ps,
        //                                  a,
        //                                  o,
        //                                  pm,
        //                                  p
        //                              }).AsQueryable();




        //            if (!string.IsNullOrWhiteSpace(searchtext))
        //            {
        //                string searchView = searchtext.Trim();
        //                policyView = policyView.Where(w => w.q.Customer.FirstName.Contains(searchView) || w.q.Customer.LastName.Contains(searchView) || w.q.PolicyNumber.Contains(searchView) || w.rh.DownPayment.ToString().Contains(searchView)).AsQueryable();
        //            }

        //            policyView = policyView.Where(f => f.a.ID == agencyId).AsQueryable();
        //            policyView = policyView.Where(f => f.p.FKPaymentStatusID == paymentSuccessStatusId).AsQueryable();
        //            if (string.IsNullOrWhiteSpace(searchtext))
        //            {
        //                policyView = policyView.Where(f => f.p.CreatedDate >= startDateFilter && f.p.CreatedDate <= endDateFilter).AsQueryable();
        //            }
        //            policyView = policyView.Where(w => w.pst.OrderNo > _processFilterByOrderNo && w.pst.OrderNo <= _policyFilterByOrderNo).AsQueryable();

        //            var _qp = (from q in db.Quotes
        //                       let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
        //                       let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
        //                       let gt = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
        //                       let qns = db.QuoteNSDPlans.Where(w => w.FKQuoteId == q.ID && w.IsSuccess == true).FirstOrDefault()
        //                       join qp in db.QuotePayments on q.ID equals qp.FKQuoteID
        //                       where qp.FKPaymentStatusID == paymentSuccessStatusId && qp.CreatedDate >= startDateFilter && q.CreatedDate <= endDateFilter
        //                       && a.ID == agencyId && fkpolicystatusId == q.FkPolicyStatusId && gt.OrderNo > _processFilterByOrderNo && gt.OrderNo <= _policyFilterByOrderNo
        //                       select new { qp, qns }).ToList();

        //            var _weeklyACHAmt = (from q in db.Quotes
        //                                 let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
        //                                 let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
        //                                 let gt = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
        //                                 let qns = db.QuoteNSDPlans.Where(w => w.FKQuoteId == q.ID && w.IsSuccess == true).FirstOrDefault()
        //                                 join qp in db.QuotePayments on q.ID equals qp.FKQuoteID
        //                                 where qp.FKPaymentStatusID == paymentSuccessStatusId && qp.FKPaymentMethodID == paymentSweepId && qp.CreatedDate >= currentWeekStartDate && q.CreatedDate <= currentWeekLastDate
        //                                 && qp.FKPaymentForID == paymentCarrierId
        //                                 && a.ID == agencyId && fkpolicystatusId == q.FkPolicyStatusId && gt.OrderNo > _processFilterByOrderNo && gt.OrderNo <= _policyFilterByOrderNo
        //                                 select new { qp,qns }).ToList();

        //            int creditCardcount = (_qp != null ? _qp.Where(w => w.qp.FKPaymentMethodID == paymentCcId && w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
        //            int eCheckCount = (_qp != null ? _qp.Where(w => w.qp.FKPaymentMethodID == paymentEcheckId && w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
        //            int sweepCount = (_qp != null ? _qp.Where(w => w.qp.FKPaymentMethodID == paymentSweepId && w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
        //            decimal downPayment = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            decimal nsdFee = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentNSDId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            decimal weeklyNSDFee = (_weeklyACHAmt != null ? _weeklyACHAmt.Where(f => f.qp.FKPaymentForID == paymentNSDId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            decimal weeklyDownPayment = (_weeklyACHAmt != null ? _weeklyACHAmt.Where(f => f.qp.FKPaymentForID == paymentCarrierId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            decimal ccFee = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCCTypeId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            decimal echekPremium = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId && f.qp.FKPaymentMethodID == paymentEcheckId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            decimal ccPremium = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId && f.qp.FKPaymentMethodID == paymentCcId).Select(s => s.qp.PaidAmount).Sum() : 0);

        //            PolicySummaryModel policySummaryModel = new PolicySummaryModel();
        //            policySummaryModel.Premium = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId && f.qp.FKPaymentMethodID == paymentSweepId).Select(s => s.qp.PaidAmount).Sum() : 0);
        //            policySummaryModel.NsdFee = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentNSDId && f.qp.FKPaymentMethodID == paymentSweepId && f.qns != null).Select(s => s.qns.RFCost).Sum() : 0);
        //            policySummaryModel.TotalPolicySold = (_qp != null ? _qp.Where(w => w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
        //            policySummaryModel.TotalCCEchekSold = creditCardcount + eCheckCount;
        //            policySummaryModel.TotalSweepCount = sweepCount;
        //            policySummaryModel.SummaryPremium = downPayment + nsdFee + ccFee;
        //            policySummaryModel.TotalMotorClubSales = nsdFee;
        //            policySummaryModel.TotalMotorClubNet = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentNSDId && f.qns != null).Select(s => s.qns.AgentGross).Sum() : 0);
        //            policySummaryModel.EcheckCCPremium = echekPremium + ccPremium;
        //            policySummaryModel.UpcomingACHDraftDate = currentWeekStartDate.AddDays(7);
        //            policySummaryModel.WeeklyACHDraftAmt = weeklyNSDFee + weeklyDownPayment;

        //            paginationObject = (from q in policyView
        //                                select new PolicyViewModel
        //                                {
        //                                    QuoteId = q.q.ID,
        //                                    FullName = q.c.FirstName + " " + q.c.LastName,
        //                                    PolicyNumber = q.q.PolicyNumber,
        //                                    DownPayment = (q.rh != null ? q.rh.DownPayment + q.rh.NSDFee + q.rh.PGProcessingFees : 0),
        //                                    SourceName = q.s.Name,
        //                                    PolicyStatus = q.pst.Name,
        //                                    InsuranceCompanyName = q.ic.Name,
        //                                    State = q.st.Code,
        //                                    CompletedDate = q.q.EffectiveDate,
        //                                    StateId = q.q.FkStateId,
        //                                    CarrierId = q.q.FKInsuranceCompanyId ?? 0,
        //                                    EnvelopeStatus = q.es.Code,
        //                                    UserId = q.q.InitiatedById ?? 0,
        //                                    Payment = q.ps.Name,
        //                                    IsPaymentCompleted = q.pm,
        //                                    AgencyName = q.a.Name,
        //                                    ProducerPhone = q.a.ContactNo,
        //                                    ProducerCode = producerCode,
        //                                    Nsd = (q.rh != null ? q.rh.NSDFee : 0),
        //                                    Premium = (q.rh != null ? q.rh.DownPayment : 0),
        //                                    CCFee = (q.rh != null ? q.rh.PGProcessingFees : 0),

        //                                }).ToPagerListOrderBy(pageNumber, pageRows, orderBy);

        //            policyViewModel.PolicyView = paginationObject;
        //            policyViewModel.PolicySummary = policySummaryModel;

        //            return policyViewModel;
        //        }
        //        catch (Exception ex)
        //        {
        //            return null;
        //        }
        //    }
        //}
        public PolicyReportModel GetPolicyReport(DateTime startDate, DateTime endDate, long agencyId, int pageNumber = 1, int pageRows = 20, string orderBy = "CompletedDate descending", string searchtext = " ")
        {
            using (var db = new ArrowheadPOSEntities())
            {
                PolicyReportModel policyViewModel = new PolicyReportModel();
                PagerList<PolicyViewModel> paginationObject = null;
                GeneralServices generalServices = new GeneralServices();
                try
                {

                    var startDateFilter = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
                    var endDateFilter = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                    var currentWeekStartDate = Arrowhead.POS.Process.CommonFun.StartOfWeek(ConvertTo.GetEstTimeNow(), DayOfWeek.Monday);
                    var currentWeekStartDateFilter = new DateTime(currentWeekStartDate.Year, currentWeekStartDate.Month, currentWeekStartDate.Day, 0, 0, 0);
                    var currentWeekLastDate = currentWeekStartDate.AddDays(6);
                    var currentWeekLastDateFilter = new DateTime(currentWeekLastDate.Year, currentWeekLastDate.Month, currentWeekLastDate.Day, 23, 59, 59);


                    int _processFilterByOrderNo = Enums.PolicyStatusFilter.PROCESS.GetHashCode();
                    int _policyFilterByOrderNo = Enums.PolicyStatusFilter.POLICY.GetHashCode();


                    long fkpolicystatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                    long paymentSuccessStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    long paymentInitiatedStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                    long paymentNSDId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.NSD.ToString());
                    long paymentCarrierId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CARRIER.ToString());
                    long paymentCCTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CREDITCARDFEES.ToString());
                    long paymentSweepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.SWEEP.ToString());
                    long paymentEcheckId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.ECHECK.ToString());
                    long paymentCcId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.CREDITCARD.ToString());
                    long raterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault();
                    long raterProviderModeId = db.RaterProviderModes.Where(w => w.FKAgencyID == agencyId).Select(s => s.ID).FirstOrDefault();
                    string producerCode = db.RaterProviderCredentials.Where(w => w.FKRaterProviderFormId == raterProviderFormId && w.FKRaterProviderModeID == raterProviderModeId).Select(s => s.ValueText).FirstOrDefault();

                    var policyView = (from q in db.Quotes
                                      let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                      let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                                      let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                      let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                      let s = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkSourceID)
                                      let p = db.QuotePayments.Where(f => f.FKQuoteID == q.ID).OrderByDescending(x => x.ID).Select(s => new { s.FKPaymentMethodID, s.CreatedDate, s.FKPaymentStatusID }).FirstOrDefault()
                                      let pm = db.QuotePayments.Where(f => f.FKQuoteID == q.ID && (f.FKPaymentStatusID == paymentSuccessStatusId || f.FKPaymentStatusID == paymentInitiatedStatusId)).Count() > 0 ? true : false
                                      let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == p.FKPaymentMethodID)
                                      let aq = db.Auto_QuoteDetail.FirstOrDefault(f => f.FKQuoteID == q.ID)
                                      let rh = db.Auto_RateHistory.FirstOrDefault(f => f.ID == aq.FKRateHistoryID)
                                      let pst = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
                                      let ic = db.InsuranceCompanies.FirstOrDefault(f => f.ID == q.FKInsuranceCompanyId)
                                      let qe = db.QuoteEnvelopes.FirstOrDefault(f => f.FKQuoteId == q.ID)
                                      let es = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == qe.FKEnvelopeStatusId)
                                      where fkpolicystatusId == q.FkPolicyStatusId
                                      select new
                                      {
                                          q,
                                          c,
                                          rh,
                                          s,
                                          st,
                                          pst,
                                          ic,
                                          es,
                                          ps,
                                          a,
                                          o,
                                          pm,
                                          p
                                      }).AsQueryable();




                    if (!string.IsNullOrWhiteSpace(searchtext))
                    {
                        string searchView = searchtext.Trim();
                        policyView = policyView.Where(w => w.q.Customer.FirstName.Contains(searchView) || w.q.Customer.LastName.Contains(searchView) || w.q.PolicyNumber.Contains(searchView) || w.rh.DownPayment.ToString().Contains(searchView)).AsQueryable();
                    }

                    policyView = policyView.Where(f => f.a.ID == agencyId).AsQueryable();
                    policyView = policyView.Where(f => f.p.FKPaymentStatusID == paymentSuccessStatusId).AsQueryable();
                    if (string.IsNullOrWhiteSpace(searchtext))
                    {
                        policyView = policyView.Where(f => f.p.CreatedDate >= startDateFilter && f.p.CreatedDate <= endDateFilter).AsQueryable();
                    }
                    policyView = policyView.Where(w => w.pst.OrderNo > _processFilterByOrderNo && w.pst.OrderNo <= _policyFilterByOrderNo).AsQueryable();

                    var _qp = (from q in db.Quotes
                               let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                               let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                               let gt = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
                               let qns = db.QuoteNSDPlans.Where(w => w.FKQuoteId == q.ID && w.IsSuccess == true).FirstOrDefault()
                               join qp in db.QuotePayments on q.ID equals qp.FKQuoteID
                               where qp.FKPaymentStatusID == paymentSuccessStatusId && qp.CreatedDate >= startDateFilter && qp.CreatedDate <= endDateFilter
                               && a.ID == agencyId && fkpolicystatusId == q.FkPolicyStatusId && gt.OrderNo > _processFilterByOrderNo && gt.OrderNo <= _policyFilterByOrderNo
                               select new { qp, qns }).ToList();

                    var _weeklyACHAmt = (from q in db.Quotes
                                         let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                                         let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                         let gt = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
                                         let qns = db.QuoteNSDPlans.Where(w => w.FKQuoteId == q.ID && w.IsSuccess == true).FirstOrDefault()
                                         join qp in db.QuotePayments on q.ID equals qp.FKQuoteID
                                         where qp.FKPaymentStatusID == paymentSuccessStatusId && qp.FKPaymentMethodID == paymentSweepId && qp.CreatedDate >= currentWeekStartDateFilter && qp.CreatedDate <= currentWeekLastDateFilter
                                         && qp.FKPaymentForID == paymentNSDId
                                         && a.ID == agencyId && fkpolicystatusId == q.FkPolicyStatusId && gt.OrderNo > _processFilterByOrderNo && gt.OrderNo <= _policyFilterByOrderNo
                                         select new { qp, qns }).ToList();

                    int creditCardcount = (_qp != null ? _qp.Where(w => w.qp.FKPaymentMethodID == paymentCcId && w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
                    int eCheckCount = (_qp != null ? _qp.Where(w => w.qp.FKPaymentMethodID == paymentEcheckId && w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
                    int sweepCount = (_qp != null ? _qp.Where(w => w.qp.FKPaymentMethodID == paymentSweepId && w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
                    decimal downPayment = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId).Select(s => s.qp.PaidAmount).Sum() : 0);
                    decimal nsdFee = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentNSDId).Select(s => s.qp.PaidAmount).Sum() : 0);
                    decimal ccFee = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCCTypeId).Select(s => s.qp.PaidAmount).Sum() : 0);
                    decimal echekPremium = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId && f.qp.FKPaymentMethodID == paymentEcheckId).Select(s => s.qp.PaidAmount).Sum() : 0);
                    decimal ccPremium = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId && f.qp.FKPaymentMethodID == paymentCcId).Select(s => s.qp.PaidAmount).Sum() : 0);

                    PolicySummaryModel policySummaryModel = new PolicySummaryModel();
                    policySummaryModel.Premium = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentCarrierId && f.qp.FKPaymentMethodID == paymentSweepId).Select(s => s.qp.PaidAmount).Sum() : 0);
                    policySummaryModel.NsdFee = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentNSDId && f.qp.FKPaymentMethodID == paymentSweepId && f.qns != null).Select(s => s.qns.RFCost).Sum() : 0);
                    policySummaryModel.TotalPolicySold = (_qp != null ? _qp.Where(w => w.qp.FKPaymentForID == paymentCarrierId).Count() : 0);
                    policySummaryModel.TotalCCEchekSold = creditCardcount + eCheckCount;
                    policySummaryModel.TotalSweepCount = sweepCount;
                    policySummaryModel.SummaryPremium = downPayment + nsdFee + ccFee;
                    policySummaryModel.TotalMotorClubSales = nsdFee;
                    policySummaryModel.TotalMotorClubNet = (_qp != null ? _qp.Where(f => f.qp.FKPaymentForID == paymentNSDId && f.qns != null).Select(s => s.qns.AgentGross).Sum() : 0);
                    policySummaryModel.EcheckCCPremium = echekPremium + ccPremium;
                    policySummaryModel.UpcomingACHDraftDate = currentWeekStartDate.AddDays(7);
                    policySummaryModel.WeeklyACHDraftAmt = _weeklyACHAmt != null ? _weeklyACHAmt.Where(f => f.qns != null).Select(s => s.qns.RFCost).Sum() : 0;

                    paginationObject = (from q in policyView
                                        select new PolicyViewModel
                                        {
                                            QuoteId = q.q.ID,
                                            FullName = q.c.FirstName + " " + q.c.LastName,
                                            PolicyNumber = q.q.PolicyNumber,
                                            DownPayment = (q.rh != null ? q.rh.DownPayment + q.rh.NSDFee + q.rh.PGProcessingFees : 0),
                                            SourceName = q.s.Name,
                                            PolicyStatus = q.pst.Name,
                                            InsuranceCompanyName = q.ic.Name,
                                            State = q.st.Code,
                                            CompletedDate = q.q.EffectiveDate,
                                            StateId = q.q.FkStateId,
                                            CarrierId = q.q.FKInsuranceCompanyId ?? 0,
                                            EnvelopeStatus = q.es.Code,
                                            UserId = q.q.InitiatedById ?? 0,
                                            Payment = q.ps.Name,
                                            IsPaymentCompleted = q.pm,
                                            AgencyName = q.a.Name,
                                            ProducerPhone = q.a.ContactNo,
                                            ProducerCode = producerCode,
                                            Nsd = (q.rh != null ? q.rh.NSDFee : 0),
                                            Premium = (q.rh != null ? q.rh.DownPayment : 0),
                                            CCFee = (q.rh != null ? q.rh.PGProcessingFees : 0),

                                        }).ToPagerListOrderBy(pageNumber, pageRows, orderBy);

                    policyViewModel.PolicyView = paginationObject;
                    policyViewModel.PolicySummary = policySummaryModel;

                    return policyViewModel;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Get Quote Report 
        /// </summary>
        /// <returns></returns>
        public PagerList<PolicyViewModel> GetQuoteReport(DateTime startDate, DateTime endDate, long agencyId, int pageNumber = 1, int pageRows = 20, string orderBy = "CompletedDate descending", string searchtext = " ")
        {
            using (var db = new ArrowheadPOSEntities())
            {
                PagerList<PolicyViewModel> paginationObject = null;
                GeneralServices generalServices = new GeneralServices();
                try
                {
                    var startDateFilter = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
                    var endDateFilter = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                    long fkpolicystatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.NEW.ToString());
                    var policyView = (from q in db.Quotes
                                      let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                      let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                                      let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                      let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                                      let s = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FkSourceID)
                                      let p = db.QuotePayments.FirstOrDefault(f => f.FKQuoteID == q.ID)
                                      let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == p.FKPaymentMethodID)
                                      let aq = db.Auto_QuoteDetail.FirstOrDefault(f => f.FKQuoteID == q.ID)
                                      // let rh = db.Auto_RateHistory.FirstOrDefault(f => f.ID == aq.FKRateHistoryID)
                                      let pst = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
                                      let ic = db.InsuranceCompanies.FirstOrDefault(f => f.ID == q.FKInsuranceCompanyId)
                                      let qe = db.QuoteEnvelopes.FirstOrDefault(f => f.FKQuoteId == q.ID)
                                      let es = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == qe.FKEnvelopeStatusId)
                                      where fkpolicystatusId == q.FkPolicyStatusId
                                      select new
                                      { q, st, c, ic, pst, es, o, a }).AsQueryable();

                    if (!string.IsNullOrWhiteSpace(searchtext))
                    {
                        policyView = policyView.Where(w => w.q.Customer.FirstName.Contains(searchtext) || w.q.Customer.LastName.Contains(searchtext)).AsQueryable();

                    }

                    policyView = policyView.Where(w => w.a.ID == agencyId).AsQueryable();

                    policyView = policyView.Where(f => f.q.EffectiveDate >= startDateFilter && f.q.EffectiveDate <= endDateFilter).AsQueryable();

                    paginationObject = (from q in policyView
                                        select new PolicyViewModel
                                        {
                                            QuoteId = q.q.ID,
                                            StateId = q.q.FkStateId,
                                            State = q.st.Code,
                                            CompletedDate = q.q.EffectiveDate,
                                            FullName = q.c.FirstName + " " + q.c.LastName,
                                            Phone = q.c.ContactNo,
                                            InsuranceCompanyName = q.ic.Name,
                                            PolicyStatus = q.pst.Name,
                                            CarrierId = q.q.FKInsuranceCompanyId ?? 0,
                                            EnvelopeStatus = q.es.Code,
                                            UserId = q.q.InitiatedById ?? 0,
                                        }).ToPagerListOrderBy(pageNumber, pageRows, orderBy);

                    return paginationObject;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public bool IsGarrageAddressMissing(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var _add = db.Quotes.Where(w => w.ID == quoteId).FirstOrDefault();
                return _add != null && !string.IsNullOrEmpty(_add.City) && !string.IsNullOrEmpty(_add.Address1) && !string.IsNullOrEmpty(_add.ZipCode)
                    ? true
                    : false;
            }
        }
        #endregion
    }
}
