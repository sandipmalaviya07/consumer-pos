﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Core;
using Arrowhead.POS.Core.Paging;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using Newtonsoft.Json;
using Stripe;

namespace Arrowhead.POS.Service
{
    public class SchedularService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<PolicyListModel> GetVoidPolicy()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                List<PolicyListModel> _policyList = new List<PolicyListModel>();
                try
                {
                    GeneralServices generalServices = new GeneralServices();
                    DateTime yesterdayDate = ConvertTo.GetEstTimeNow().AddDays(-1);


                    var startDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 0, 0, 0);
                    var endDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 23, 59, 59);

                    long completedSignId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ENVELOPESTATUS.ToString(), Enums.EnvelopeStatus.ESIGNCOMPLETED.ToString());
                    _policyList = (from aq in db.QuoteEnvelopes
                                   let c = db.Customers.Where(w => w.ID == aq.FKCustomerId).FirstOrDefault()
                                   let q = db.Quotes.Where(w => w.ID == aq.FKQuoteId).FirstOrDefault()
                                   join qd in db.Auto_QuoteDetail on q.ID equals qd.FKQuoteID
                                   let rh = db.Auto_RateHistory.Where(f => f.ID == qd.FKRateHistoryID).FirstOrDefault()
                                   let o = db.Offices.Where(f => f.ID == q.FkOfficeId).FirstOrDefault()
                                   let a = db.Agencies.Where(f => f.ID == o.FKAgencyID).FirstOrDefault()
                                   let p = db.QuotePayments.Where(f => f.FKQuoteID == q.ID).OrderByDescending(x => x.ID).Select(s => s.FKPaymentMethodID).FirstOrDefault()
                                   let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == p.Value)
                                   where aq.CreatedDate >= startDateFilter && aq.CreatedDate <= endDateFilter && aq.FKEnvelopeStatusId != completedSignId
                                   select new PolicyListModel
                                   {
                                       QuoteEnvelopeId = aq.Id,
                                       QuoteId = aq.FKQuoteId ?? 0,
                                       FkEnvelopeStatusId = aq.FKEnvelopeStatusId,
                                       CustomerId = aq.FKCustomerId,
                                       EnvelopeId = aq.EnvelopeId,
                                       CreatedDate = aq.CreatedDate,
                                       PolicyNumber = q.PolicyNumber,
                                       CustomerName = c.FirstName + " " + c.LastName,
                                       AgencyName = a.CommercialName,
                                       AgencyEmailId = a.EmailID,
                                       EmailId=c.EmailID,
                                       Payment = rh.DownPayment + rh.NSDFee + rh.PGProcessingFees,
                                       PaymentType = ps.Name
                                   }).ToList();

                }
                catch (Exception ex)
                {
                    return null;
                }

                return _policyList;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public PolicyListModel GetVoidPolicyDetailsByQuoteId(long quoteId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                PolicyListModel voidPolicy = new PolicyListModel();
                try
                {
                     voidPolicy = (from q in db.Quotes
                                      let c = db.Customers.Where(w => w.ID == q.FkCustomerID).FirstOrDefault()
                                      join qd in db.Auto_QuoteDetail on q.ID equals qd.FKQuoteID
                                      let rh = db.Auto_RateHistory.Where(f => f.ID == qd.FKRateHistoryID).FirstOrDefault()
                                      let o = db.Offices.Where(f => f.ID == q.FkOfficeId).FirstOrDefault()
                                      let a = db.Agencies.Where(f => f.ID == o.FKAgencyID).FirstOrDefault()
                                      let p = db.QuotePayments.Where(f => f.FKQuoteID == q.ID).OrderByDescending(x => x.ID).Select(s => s.FKPaymentMethodID).FirstOrDefault()
                                      let ps = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == p.Value)
                                      where q.ID == quoteId
                                      select new PolicyListModel
                                      {
                                          CustomerId = q.FkCustomerID,
                                          QuoteId = quoteId,
                                          CustomerName = c.FirstName + " " + c.LastName,
                                          PolicyNumber = q.PolicyNumber,
                                          AgencyName = a.Name,
                                          Payment = rh.DownPayment + rh.NSDFee + rh.PGProcessingFees,
                                          PaymentType = ps.Name
                                      }).FirstOrDefault();
                }
                catch(Exception ex)
                {
                    return null;
                }

                return voidPolicy;


            }
        }
    }
}
