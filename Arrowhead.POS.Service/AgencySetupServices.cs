﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using EntityFramework.Extensions;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Service
{
    public class AgencySetupServices
    {
        /// <summary>
        /// Get Current Agency Step Details By Agency Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CurrentStepModel GetCurrentStepDetails(long Id)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var CurrentStepDetails = (from a in db.Agencies
                                              let cs = db.GeneralStatusTypes.Where(f => f.ID == a.FKAgencyCurrentStepID).FirstOrDefault()
                                              where a.ID == Id
                                              select new CurrentStepModel
                                              {
                                                  AgencyId = a.ID,
                                                  CusrrentStepId = a.FKAgencyCurrentStepID ?? 0,
                                                  CurrentStepCode = cs != null ? cs.Code : "",
                                                  CurrentStepName = cs != null ? cs.Name : "",
                                                  CurrentStepOrderId = cs != null ? cs.OrderNo : 0
                                              }).FirstOrDefault();
                    return CurrentStepDetails;

                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Agency Step By Current Step Code
        /// </summary>
        /// <param name="currentStepCode"></param>
        /// <returns></returns>
        public long AgencyStepId(string currentStepCode)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                long quoteStepId = generalServices.GetGeneralStatusOrderIdByType(Enums.GeneralStatus.AGENCYSETUP.ToString(), currentStepCode.ToString());
                return quoteStepId;
            }
        }

        /// <summary>
        /// Update Agency Details and send notification email to arrowhead
        /// </summary>
        /// <param name="_agencyModel"></param>
        /// <returns></returns>
        public bool UpdateAgencyDetails(AgencyModel _agencyModel)
        {
            GeneralServices _generalServices = new GeneralServices();
            AutoQuoteServices _autoQuoteServices = new AutoQuoteServices();
            Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
            TemplateModel template = templateServices.GetTemplateDetails(Enums.TemplateType.SENDARROWHEADAGENCYINFOUPDATENOTIFICATION.ToString());
            OnBoardAgencyInfoModel _onBoardAgencyInfoModel = new OnBoardAgencyInfoModel();
            List<AgencyInfoModel> _lstAgencyInfoModel = new List<AgencyInfoModel>();

            bool isUpdate = false;
            string oldText = string.Empty;
            string newText = string.Empty;

            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    if (_agencyModel != null)
                    {
                        //Prepare On Board Model 
                        var _agencyInfoModel = _autoQuoteServices.GetAgencyDetails(_agencyModel.AgencyId);

                        try
                        {
                            if (_agencyInfoModel != null)
                            {
                                _onBoardAgencyInfoModel.AgencyName = _agencyInfoModel.AgencyName;

                                if (!string.IsNullOrEmpty(_agencyInfoModel.ContactNo) && _agencyInfoModel.ContactNo != _agencyModel.ContactNo.Replace("-", ""))
                                {
                                    oldText = _agencyInfoModel.ContactNo;
                                    newText = _agencyModel.ContactNo;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "Contact No", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.FaxNo) && _agencyInfoModel.FaxNo != _agencyModel.FaxNo.Replace("-", ""))
                                {
                                    oldText = _agencyInfoModel.FaxNo;
                                    newText = _agencyModel.FaxNo;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "Fax No", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.AgencyEmailId) && _agencyInfoModel.AgencyEmailId != _agencyModel.AgencyEmailId)
                                {
                                    oldText = _agencyInfoModel.AgencyEmailId;
                                    newText = _agencyModel.AgencyEmailId;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "Email ID", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.Address1) && _agencyInfoModel.Address1 != _agencyModel.Address1)
                                {
                                    oldText = _agencyInfoModel.Address1;
                                    newText = _agencyModel.Address1;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "Address 1", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.Address3) && _agencyInfoModel.Address3 != _agencyModel.Address3)
                                {
                                    oldText = _agencyInfoModel.Address3;
                                    newText = _agencyModel.Address3;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "Address 2", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.ZipCode) && _agencyInfoModel.ZipCode != _agencyModel.ZipCode)
                                {
                                    oldText = _agencyInfoModel.ZipCode;
                                    newText = _agencyModel.ZipCode;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "Zip Code", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.City) && _agencyInfoModel.City != _agencyModel.City)
                                {
                                    oldText = _agencyInfoModel.City;
                                    newText = _agencyModel.City;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "City", newTextVal = newText, oldTextVal = oldText });
                                }

                                if (!string.IsNullOrEmpty(_agencyInfoModel.StateAbbr) && _agencyInfoModel.StateAbbr != _agencyModel.StateAbbr)
                                {
                                    oldText = _agencyInfoModel.StateAbbr;
                                    newText = _agencyModel.StateAbbr;
                                    _lstAgencyInfoModel.Add(new AgencyInfoModel { labelText = "State", newTextVal = newText, oldTextVal = oldText });
                                }
                            }

                            if (_lstAgencyInfoModel != null && _lstAgencyInfoModel.Count > 0)
                            {
                                _onBoardAgencyInfoModel.Heading = _agencyInfoModel.AgencyName + " has Updated Its Info";
                                _onBoardAgencyInfoModel.ProducerCode = _agencyInfoModel.ProducerCode.ToString();
                                _onBoardAgencyInfoModel.AgencyInfoModel = _lstAgencyInfoModel;
                            }
                        }
                        catch (Exception)
                        {

                        }


                        db.Agencies.Where(x => x.ID == _agencyModel.AgencyId).Update(f => new Agency
                        {
                            ContactNo = _agencyModel.ContactNo.ToPhoneNumber(),
                            Fax = !string.IsNullOrEmpty(_agencyModel.FaxNo) ? _agencyModel.FaxNo.ToPhoneNumber() : "",
                            EmailID = _agencyModel.AgencyEmailId,
                            Address1 = _agencyModel.Address1,
                            Address2 = _agencyModel.Address3,
                            City = _agencyModel.City,
                            FKStateId = _agencyModel.StateId,
                            Zipcode = _agencyModel.ZipCode,
                            Password = CryptoGrapher.MD5Hash(ConstantVariables.AgencyPassCode) // default pass code for agency
                        });

                        // Notify Arrowhead If Update 
                        if (_onBoardAgencyInfoModel != null && _onBoardAgencyInfoModel.AgencyInfoModel != null && _onBoardAgencyInfoModel.AgencyInfoModel.Count > 0)
                        {
                            var _emailBody = GetAgencyInfoUpdateTemplate(template, _onBoardAgencyInfoModel);
                            var _subject = _agencyInfoModel.AgencyName + "- " + template.EmailSubject;
                            if (!string.IsNullOrEmpty(_emailBody))
                            {
                                var officeSMTPlDetail = _generalServices.GetSmtpDetails();
                                MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, ConstantVariables.OnBoardNotificationArrowhead, "", "", _subject, _emailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, null, null);
                                MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, ConstantVariables.OnBoardNotificationArrowheadGroup, "", "", _subject, _emailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, null, null);
                            }
                        }
                        isUpdate = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isUpdate = false;
                throw ex;
            }
            return isUpdate;
        }

        public bool SetupAgencyPIN(AgencyModel _agencyModel)
        {
            if (_agencyModel == null)
            {
                return false;
            }

            bool isUpdate = true;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    db.Agencies.Where(x => x.ID == _agencyModel.AgencyId).Update(f => new Agency
                    {
                        Password = CryptoGrapher.MD5Hash(_agencyModel.PIN)
                    });
                }
            }
            catch (Exception ex)
            {
                isUpdate = false;
            }
            return isUpdate;
        }

        public bool SetupAgencyNSDACH(AgencyModel _agencyModel)
        {
            if (_agencyModel == null)
            {
                throw new ArgumentNullException(nameof(_agencyModel));
            }

            bool isUpdate = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    if (_agencyModel != null)
                    {
                        db.Agencies.Where(x => x.ID == _agencyModel.AgencyId).Update(f => new Agency
                        {
                            IsNSDAvailable = _agencyModel.IsNSDAvailable,
                            IsSetupCompleted = true
                        });
                        isUpdate = true;
                    }
                }

            }
            catch (Exception ex)
            {
                isUpdate = false;
            }
            return isUpdate;
        }

        /// <summary>
        /// Save On Board Agency Bank Details
        /// </summary>
        /// <param name="onBoardBankDetailModel"></param>
        /// <param name="agencyID"></param>
        /// <returns></returns>
        public PaymentProcessResponseModel SaveBankDetails(OnBoardBankDetailModel onBoardBankDetailModel, long agencyID)
        {
            string secretKey = ConstantVariables.AgencyEncryptionKey.ToString();
            PaymentServices _paymentServices = new PaymentServices();
            PaymentProcessResponseModel _paymentRespModel = new PaymentProcessResponseModel();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    int _agencyId = Convert.ToInt32(agencyID);
                    bool isBankDetailExist = db.AgencyBankDetails.Any(c => c.FKAgencyID == _agencyId);
                    if (isBankDetailExist)
                    {
                        db.AgencyBankDetails.Where(w => w.FKAgencyID == _agencyId).Update(u => new AgencyBankDetail
                        {
                            ACHolderName = CryptoGrapher.EncryptText(onBoardBankDetailModel.AccountHolderName, secretKey),
                            FKACHolderType = CryptoGrapher.EncryptText(onBoardBankDetailModel.AccountTypeId.ToString(), secretKey),
                            ACNumber = CryptoGrapher.EncryptText(onBoardBankDetailModel.AccountNumber, secretKey),
                            BankRoutingNumber = CryptoGrapher.EncryptText(onBoardBankDetailModel.RoutingNumber, secretKey),
                            BankName = CryptoGrapher.EncryptText(onBoardBankDetailModel.BankName, secretKey)
                        });
                    }
                    else
                    {
                        AgencyBankDetail agencyBankDetail = new AgencyBankDetail
                        {
                            FKAgencyID = _agencyId,
                            ACHolderName = CryptoGrapher.EncryptText(onBoardBankDetailModel.AccountHolderName, secretKey),
                            FKACHolderType = CryptoGrapher.EncryptText(onBoardBankDetailModel.AccountTypeId.ToString(), secretKey),
                            ACNumber = CryptoGrapher.EncryptText(onBoardBankDetailModel.AccountNumber, secretKey),
                            BankRoutingNumber = CryptoGrapher.EncryptText(onBoardBankDetailModel.RoutingNumber, secretKey),
                            BankName = CryptoGrapher.EncryptText(onBoardBankDetailModel.BankName, secretKey)
                        };
                        db.AgencyBankDetails.Add(agencyBankDetail);
                        db.SaveChanges();                
                    }
                    try
                    {
                        PaymentProcessRequestModel _paymentReqModel = new PaymentProcessRequestModel()
                        {
                            CustomerName = onBoardBankDetailModel.AccountHolderName,
                            BankAccountType = "individual",
                            BankAccountNumber = onBoardBankDetailModel.AccountNumber,
                            BankName = onBoardBankDetailModel.BankName,
                            CurrencyCode = "US",
                            BankRoutingNumber = onBoardBankDetailModel.RoutingNumber,
                            Description = "AgencyID : " + agencyID + "  Bank " + onBoardBankDetailModel.BankName + " details"
                        };

                        _paymentRespModel = _paymentServices.CreateStripeCustomer(_paymentReqModel);
                        if (_paymentRespModel != null && _paymentRespModel.StatusCode == "200")
                        {
                            // update Is NSD Available flag 
                            db.Agencies.Where(x => x.ID == agencyID).Update(f => new Agency
                            {
                                IsNSDAvailable = true,
                                StripeCustID = _paymentRespModel.CustomerId,
                                StripeBankAccountID = _paymentRespModel.BankAccountId
                            });
                        }
                        else
                        {
                            return _paymentRespModel;
                        }
                    }
                    catch (Exception ex)
                    {
                        _paymentRespModel.StatusCode = "400";
                        _paymentRespModel.Status = false;
                        _paymentRespModel.StatusMessage = ex.Message.ToString();
                        _paymentRespModel.CustomerId = "0";
                        _paymentRespModel.BankAccountId = "0";
                    }
                    // Expire Token After Bank Details Saved
                    db.Tokenizations.Where(x => x.FKAgencyID == agencyID && x.IsExpired == false).Update(f => new Tokenization
                    {
                        IsExpired = true
                    });
                    return _paymentRespModel;
                }
            }
            catch (Exception ex)
            {
                _paymentRespModel.StatusCode = "400";
                _paymentRespModel.Status = false;
                _paymentRespModel.StatusMessage = ex.Message.ToString();
                _paymentRespModel.CustomerId = "0";
                _paymentRespModel.BankAccountId = "0";
                //_paymentRespModel = null;
            }
            return _paymentRespModel;
        }

        /// <summary>
        /// Save Token and Get activation token for prepare redirect URL
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public string GetActivationToken(long agencyId)
        {
            GeneralServices _generalServices = new GeneralServices();
            string authToken = GeneralServices.GenerateAuthToken(20);
            string redirectURL = "nsd/ach/{" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(agencyId.ToString())) + "}";
            long nsdActivationStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.TOKENIZE.ToString(), Enums.TokenizeType.NSDACTIVATIONLINK.ToString());
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    Tokenization tokenization = new Tokenization()
                    {
                        TokenValue = authToken,
                        FKAgencyID = Convert.ToInt32(agencyId),
                        RedirectionURL = redirectURL,
                        CreatedDate = ConvertTo.GetEstTimeNow(),
                        FKTokenTypeID = nsdActivationStatusId,
                        ExpiryDate = ConvertTo.GetEstTimeNow().AddMonths(1),
                        IsExpired = false,
                        TotalClickCounts = 0
                    };
                    db.Tokenizations.Add(tokenization);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return authToken;
        }

        /// <summary>
        /// Get NSD Activation Template
        /// </summary>
        /// <param name="templateModel"></param>
        /// <param name="activationURL"></param>
        /// <returns></returns>
        public string GetNSDActivationTemplate(TemplateModel templateModel, AgencyOnBoardActivationModel model)
        {
            TemplateModel _template = templateModel;
            try
            {
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, model);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return string.Empty;
        }

        /// <summary>
        /// Get Redirect URL By Token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string GetRedirectURLByTokenVal(string token)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    return (db.Tokenizations.Where(x => x.TokenValue == token && x.IsExpired == false).Select(x => x.RedirectionURL).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Get Agency Info Update Template
        /// </summary>
        /// <param name="templateModel"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string GetAgencyInfoUpdateTemplate(TemplateModel templateModel, OnBoardAgencyInfoModel model)
        {
            TemplateModel _template = templateModel;
            try
            {
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, model);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return string.Empty;
        }

        public List<AgencyModel> GetAgencyListForNSDActivation()
        {
            try
            {
                List<AgencyModel> agencyModels = new List<AgencyModel>();
                CommonServices commonServices = new CommonServices();
                using (var db = new ArrowheadPOSEntities())
                {
                    var lstAgency = db.Agencies.Where(x => x.IsSetupCompleted == true && x.IsNSDAvailable == false).ToList();
                    Parallel.ForEach(lstAgency, (agency) =>
                    {
                        agencyModels.Add(new AgencyModel
                        {
                            AgencyId = agency.ID,
                            ProducerCode = commonServices.getProducerCodeByAgencyId(agency.ID),
                            AgencyName = agency.Name,
                            AgencyEmailId = agency.EmailID,
                        });
                    });
                    return agencyModels;
                }
            }
            catch(Exception ex)
            {
                return null;
            }
        }


        public bool IsAgencyNSDAvailable(long agencyId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    return (db.Agencies.Where(x => x.ID == agencyId).Select(x => x.IsNSDAvailable).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
