﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Arrowhead.POS.Service
{
    public class AutoCoverageServices
    {
        /// <summary>
        /// Get Coverage OF Quote 
        /// </summary>
        /// <param name="quoteId">QuoteID</param>
        /// <returns></returns>
        public List<StateCoveragesModel> GetQuoteCoverages(long quoteId)
        {
            List<StateCoveragesModel> lstStateCoverageModel = new List<StateCoveragesModel>();
            DataTable dtCoverageValues = new DataTable();
            DataTable dtCoverage = new DataTable();

            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    string connectionString = ConstantVariables.ConnectionString;
                    // Dim connectionString As String = "data source=DESKTOP-9SOA1QP\SQLEXPRESS2014;initial catalog=ArrowheadPOS;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;"

                    if (connectionString.ToLower().StartsWith("metadata="))
                    {
                        System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder efBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(connectionString);
                        connectionString = efBuilder.ProviderConnectionString;
                    }

                    DataSet dbset = new DataSet();
                    string strConnString = connectionString;
                    SqlConnection con1 = new SqlConnection(strConnString);
                    SqlCommand cmd2 = new SqlCommand();
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.CommandText = "spGetQuoteCoverages";
                    cmd2.Parameters.Add("@quote_id", SqlDbType.Int).Value = quoteId;
                    cmd2.Connection = con1;
                    try
                    {
                        con1.Open();

                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd2;
                        da.Fill(dbset);
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        con1.Close();
                        con1.Dispose();
                    }

                    if (dbset != null && dbset.Tables.Count != 0)
                    {
                        dtCoverageValues = dbset.Tables[0];
                        dtCoverage = dbset.Tables[1];
                    }
                }

                foreach (DataRow dr in dtCoverage.Rows)
                    lstStateCoverageModel.Add(new StateCoveragesModel()
                    {
                        CoverageId = Convert.ToInt32(dr["CoverageId"]),
                        Code = Convert.ToString(dr["CoverageCode"]),
                        ShortName = Convert.ToString(dr["ShortName"]),
                        CoverageName = Convert.ToString(dr["CoverageName"]),
                        CoverageFormControl = Convert.ToString(dr["ControlType"]),
                        Selected = Convert.ToString(dr["SelectedValue"]),
                        IsVehicleCoverage = Convert.ToBoolean(dr["IsVehicleCoverage"]),
                        VehicleID = Convert.ToInt64(dr["VehicleId"]),
                        VehicleTitle = Convert.ToString(dr["VehicleTitle"]),
                        VehicleOrderID = Convert.ToInt32(dr["VehicleOrderId"]),
                        IsDuplicate = Convert.ToBoolean(dr["IsDuplicate"]),
                        StateId = Convert.ToInt32(dr["StateId"]),
                        IsEnable = Convert.ToBoolean(dr["IsEnable"]),
                        ParentId = (dr["ParentId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ParentId"])),
                        QuoteSelectedValue = Convert.ToString(dr["QuoteVal"]),
                        GroupName = Convert.ToString(dr["GroupName"]),
                        IsEdit = Convert.ToString(dr["QuoteVal"]) == "" ? false : true
                    });

                foreach (var item in lstStateCoverageModel)
                {
                    var _dataReturn = dtCoverageValues.Select("fkCoverageId = '" + item.CoverageId + "'");
                    List<CoveragesFormControlValues> _coverageFormControl = new List<CoveragesFormControlValues>();

                    foreach (var _dr in _dataReturn)
                    {
                        CoveragesFormControlValues _cnt = new CoveragesFormControlValues();
                        _cnt.Text = Convert.ToString(_dr["DisplayTextValue"]);
                        _cnt.Value = Convert.ToString(_dr["DdlVal"]);
                        if (item.Selected == Convert.ToString(_dr["DisplayTextValue"]))
                            _cnt.IsSelected = true;
                        else
                            _cnt.IsSelected = false;

                        _coverageFormControl.Add(_cnt);

                        if (item.Selected == _cnt.Text)
                            item.Selected = _cnt.Value;
                    }

                    item.CoveragesFormControlValues = _coverageFormControl;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return lstStateCoverageModel;
        }

        public List<StateCoveragesModel> GetQuickQuoteCoverages(long stateId)
        {
            List<StateCoveragesModel> lstStateCoverageModel = new List<StateCoveragesModel>();
            DataTable dtCoverageValues = new DataTable();
            DataTable dtCoverage = new DataTable();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    string connectionString = ConstantVariables.ConnectionString;
                    if (connectionString.ToLower().StartsWith("metadata="))
                    {
                        System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder efBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(connectionString);
                        connectionString = efBuilder.ProviderConnectionString;
                    }

                    DataSet dbset = new DataSet();
                    string strConnString = connectionString;
                    SqlConnection con1 = new SqlConnection(strConnString);
                    SqlCommand cmd2 = new SqlCommand();
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.CommandText = "spGetQuickQuoteCoverages";
                    cmd2.Parameters.Add("@state_id", SqlDbType.Int).Value = stateId;
                    cmd2.Connection = con1;
                    try
                    {
                        con1.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd2;
                        da.Fill(dbset);
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        con1.Close();
                        con1.Dispose();
                    }

                    if (dbset != null && dbset.Tables.Count != 0)
                    {
                        dtCoverage = dbset.Tables[0];
                        dtCoverageValues = dbset.Tables[1];
                    }
                }

                foreach (DataRow dr in dtCoverage.Rows)
                {
                    lstStateCoverageModel.Add(new StateCoveragesModel()
                    {
                        CoverageId = Convert.ToInt32(dr["CoverageId"]),
                        Code = Convert.ToString(dr["CoverageCode"]),
                        ShortName = Convert.ToString(dr["ShortName"]),
                        CoverageName = Convert.ToString(dr["CoverageName"]),
                        CoverageFormControl = Convert.ToString(dr["ControlType"]),
                        Selected = Convert.ToString(dr["SelectedValue"]),
                        IsVehicleCoverage = Convert.ToBoolean(dr["IsVehicleCoverage"]),
                        VehicleID = Convert.ToInt64(dr["VehicleId"]),
                        VehicleTitle = Convert.ToString(dr["VehicleTitle"]),
                        VehicleOrderID = Convert.ToInt32(dr["VehicleOrderId"]),
                        IsEnable = Convert.ToBoolean(dr["IsEnable"]),
                        ParentId = (dr["ParentId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ParentId"])),
                        //QuoteSelectedValue = Convert.ToString(dr["QuoteVal"]),
                        GroupName = Convert.ToString(dr["GroupName"])
                    });
                }

                foreach (var item in lstStateCoverageModel)
                {
                    var _dataReturn = dtCoverageValues.Select("fkCoverageId = '" + item.CoverageId + "'");
                    List<CoveragesFormControlValues> _coverageFormControl = new List<CoveragesFormControlValues>();

                    foreach (var _dr in _dataReturn)
                    {
                        CoveragesFormControlValues _cnt = new CoveragesFormControlValues();
                        _cnt.Text = Convert.ToString(_dr["DisplayTextValue"]);
                        _cnt.Value = Convert.ToString(_dr["DdlVal"]);
                        if (item.Selected == Convert.ToString(_dr["DisplayTextValue"]))
                            _cnt.IsSelected = true;
                        else
                            _cnt.IsSelected = false;

                        _coverageFormControl.Add(_cnt);

                        if (item.Selected == _cnt.Text)
                            item.Selected = _cnt.Value;
                    }

                    item.CoveragesFormControlValues = _coverageFormControl;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return lstStateCoverageModel;
        }

        /// <summary>
        /// Insert In Quote Coverage
        /// </summary>
        /// <param name="coveragesModel">State Coverages Model</param>
        /// <param name="QuoteID">QuoteID</param>
        /// <returns></returns>
        public bool InsertQuoteCoverage(List<StateCoveragesModel> coveragesModel, long QuoteID)
        {
            GeneralServices generalServices = new GeneralServices();

            using (var db = new ArrowheadPOSEntities())
            {
                var _createDate = ConvertTo.GetEstTimeNow();

                db.Auto_QuoteCoverage.Where(f => f.FKQuoteID == QuoteID).Delete();

                foreach (var item in coveragesModel)
                {
                    try
                    {
                        decimal value1 = 0;
                        decimal value2 = 0;

                        if (item.Selected != null)
                        {
                            string[] arr = item.Selected.Split('/');

                            if (arr != null)
                            {
                                value1 = Convert.ToDecimal(arr[0]);
                                if (arr.Count() > 1)
                                    value2 = Convert.ToDecimal(arr[1]);
                            }
                        }

                        //if (!(value1 == 0 && value2 == 0))
                        //{
                            Auto_QuoteCoverage quoteCoverage = new Auto_QuoteCoverage
                            {
                                FKQuoteID = QuoteID,
                                FkCoverageID = item.CoverageId,
                                Value1 = value1,
                                Value2 = value2,
                                CreatedDate = _createDate
                            };

                            if (item.VehicleID != null && item.VehicleID != 0)
                                quoteCoverage.FKVehicleID = item.VehicleID;
                            db.Auto_QuoteCoverage.Add(quoteCoverage);
                            db.SaveChanges();
                        //}



                        var fkcurrentstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.COVERAGE.ToString());

                        db.Quotes.Where(w => w.ID == QuoteID).Update(U => new Quote
                        {
                            FKCurrentStepId = fkcurrentstatusId,
                        });
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Verify Lienholder exist of Quote 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public List<VerifyLienholderModel> isLienHolderExist(long quoteId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    List<VerifyLienholderModel> lstVerifyLienholdermodel = new List<VerifyLienholderModel>();


                    // Get Vehicle List Who have Lienholders

                    var vehicleList = (from av in db.Auto_Vehicle
                                       let coverage = (from aqc in db.Auto_QuoteCoverage join c in db.Coverages on aqc.FkCoverageID equals c.ID where aqc.FKVehicleID == av.ID select new { aqc, c }).ToList()
                                       where av.FkQuoteID == quoteId && av.IsDeleted == false &&
                                             av.FkLienHolderID != null && av.FkLienHolderID > 0
                                       select new VerifyLienholderModel()
                                       {
                                           QuoteId = quoteId,
                                           VehicleId = av.ID,
                                           VIN = av.VIN,
                                           Year = av.Year,
                                           Make = av.Make,
                                           Model = av.Model,
                                           IsLeinComCollVerified = coverage.Any(a => a.c.Code == Enums.Coverages.COMP.ToString() && a.aqc.Value1 > 0)
                                       }).ToList();

                    return vehicleList;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return null;
        }

        /// <summary>
        /// Get collsion level type
        /// </summary>
        /// <param name="collType"></param>
        /// <returns></returns>
        public string GetCollType(string collType)
        {
            switch (collType)
            {
                case "B":
                    collType = "Broad";
                    break;

                case "S":
                    collType = "Standard";
                    break;

                case "L":
                    collType = "Limited";
                    break;

                default:
                    collType = string.Empty;
                    break;
            }
            return collType;
        }

        /// <summary>
        /// Get Coverage Details By Value
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public CoverageDetailModel GetCoverageDetailsByValue(string value, string Code, int stateId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var splitValue = value.Split('/');
                decimal value1 = Convert.ToDecimal(splitValue[0]);
                decimal value2 = Convert.ToDecimal(splitValue[1]);
                return (from sc in db.StateCoverages
                        join c in db.Coverages on sc.FkCoverageID equals c.ID
                        where sc.Value1 == value1 && sc.Value2 == value2 && sc.FkStateID == stateId && c.Code == Code
                        select new CoverageDetailModel()
                        {
                            CoverageId = c.ID,
                            CoverageCode = c.Code,
                            CoverageName = c.Name,
                            IsVehicleCoverage = c.IsVehicleCoverage,
                            GroupName = c.GroupName,
                            ControlType = c.ControlType,
                            DisplayText = sc.DisplayTextValue1
                        }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get Coverage Detail By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public CoverageDetailModel GetCoverageDetailByCode(string code, string paramCovValue, int stateId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (from sc in db.StateCoverages
                        join c in db.Coverages on sc.FkCoverageID equals c.ID
                        where (code != string.Empty && paramCovValue != string.Empty ? c.Code == code && sc.DisplayTextValue1 == paramCovValue && sc.FkStateID == stateId : c.Code == code && sc.FkStateID == stateId)
                        select new CoverageDetailModel()
                        {
                            CoverageId = c.ID,
                            CoverageCode = c.Code,
                            CoverageFormControl = c.ControlType,
                            CoverageName = c.Name,
                            IsVehicleCoverage = c.IsVehicleCoverage,
                            GroupName = c.GroupName,
                            ControlType = c.ControlType,
                            DisplayText = sc.DisplayTextValue1,
                            Value1 = sc.Value1,
                            Value2 = sc.Value2
                        }).FirstOrDefault();

            }
        }
    }
}
