﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Model;
using Arrowhead.POS.Core;

namespace Arrowhead.POS.Service
{
    public class LoginService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CheckLoginDetails(LoginModel model)
        {
            bool checkPassword = false;
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
                    AgencyModel agencyModel = new AgencyModel();
                    agencyModel = autoQuoteServices.GetAgencyDetailsByContratNo(model.ProducerCode);

                    if (agencyModel != null)
                    {
                        string password = CryptoGrapher.MD5Hash(model.Password);
                        bool agencyPassword = db.Agencies.Where(w => w.ID == agencyModel.AgencyId && w.Password == password).Any();
                        if (agencyPassword == true)
                        {
                            checkPassword = true;
                        }
                    }
                    return checkPassword;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SuperAdminPassword()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                string superadmin = db.ProjectTerms.Where(w => w.Code == Enums.ProjectTerm.SUPERADMINPASSWORD.ToString()).Select(s => s.TermValue).FirstOrDefault();
                return superadmin;
            }
        }
    }
}
