﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using Arrowhead.POS.Model;

using Arrowhead.POS.Process.ProcessXMLModel;

using Arrowhead.POS.Core;
using System.IO;
using Newtonsoft.Json;
using log4net;
using System.Globalization;

namespace Arrowhead.POS.Service
{

    public class PolicyXmlServices
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PolicyXmlServices));
        //public const string url = "https://ratingqa.itcdataservices.com/Webservices/imp/api/vehicles/";
        //public const string VehicleDetailUrl = "https://ratingqa.itcdataservices.com/Webservices/imp/api/vehicles/detail/";
        //public const string impAccountId = "4A69E926-E440-48C8-A41D-BCDB1ED5BFF3";

        public const string url = "https://www.itcratingservices.com/Webservices/imp/api/vehicles/";
        public const string VehicleDetailUrl = "https://www.itcratingservices.com/Webservices/imp/api/vehicles/detail/";
        public const string impAccountId = "4a69e926-e440-48c8-a41d-bcdb1ed5bff3";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vin"></param>
        /// <returns></returns>
        public VehicleInfoModel DecodeVIN(string vin)
        {
            VehicleInfoModel VINDetailModel = new VehicleInfoModel();

            if (!(string.IsNullOrWhiteSpace(vin)))
            {
                string response = GetResponse(VehicleDetailUrl + vin, impAccountId);
                response = response.Replace("[", "").Replace("]", "");
                VINDetailModel = JsonConvert.DeserializeObject<VehicleInfoModel>(response);
            }

            return VINDetailModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="make"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string GetVinByVehicleDetail(string year, string make, string model)
        {
            List<VehicleInfoModel> VINDetailModel = new List<VehicleInfoModel>();
            string vin = string.Empty;

            if (!(string.IsNullOrWhiteSpace(make) && string.IsNullOrWhiteSpace(year) && string.IsNullOrWhiteSpace(model)))
            {
                string response = GetResponse(url + year + "/" + make + "/" + model, impAccountId);

                VINDetailModel = JsonConvert.DeserializeObject<List<VehicleInfoModel>>(response);
                if (VINDetailModel != null)
                    vin = VINDetailModel[0].VIN;
            }
            return vin;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="impAccountId"></param>
        /// <returns></returns>
        public static string GetResponse(string url, string impAccountId)
        {
            string response = string.Empty;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(impAccountId.ToString() + ":" + impAccountId.ToString())));
                    response = client.DownloadString(url.Replace("*", ""));
                }
            }
            catch (Exception ex)
            {
            }

            return response;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AgencyModel InsertXmlPolicyProcess(ACORD model)
        {
            Log.Debug("Insert xml process start " + JsonConvert.SerializeObject(model));

            AgencyModel agencyModel = new AgencyModel();
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    agencyModel.IsError = true;
                    CommonServices commonServices = new CommonServices();
                    GeneralServices generalServices = new GeneralServices();
                    var getGeneralStatusType = generalServices.GetGeneralStatusList();
                    long quoteStatusID = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.QUOTESTATUS.ToString() && w.Code == Enums.QuoteStatus.NEW.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                    long quoteTypeID = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.QUOTETYPE.ToString() && w.Code == Enums.QuoteType.BRIDGEQUOTE.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                    long addressType = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.ADDRESSTYPE.ToString() && w.Code == Enums.AddressType.PERMANENT.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                    int insuranceTypeId = commonServices.GetAutoInsuranceID();
                    int stateId = 0;

                    var stateCd = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.StateProvCd).FirstOrDefault();
                    var postalCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.PostalCode).FirstOrDefault();
                    var city = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.City).FirstOrDefault();

                    var zipCodeDetails = commonServices.IsZipCodeCheck(postalCode);
                    if (zipCodeDetails != null)
                    {
                        Log.Debug("Insert xml process zipcode details " + JsonConvert.SerializeObject(zipCodeDetails));

                        stateCd = zipCodeDetails.state.ToUpper();
                        stateId = commonServices.GetStateIdByCode(zipCodeDetails.state.ToUpper());
                        city = zipCodeDetails.city;
                    }
                    else
                    {
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                    }

                    string phoneNumber = string.Empty;


                    if (!string.IsNullOrWhiteSpace(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.ProducerInfo.ContractNumber))
                    {
                        var _resp = InsertAgency(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.ProducerInfo.ContractNumber, model);
                        agencyModel = _resp;
                    }
                    else
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = "ProducerInfo ContractNumber is not coming";
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                    }

                    foreach (var phone in model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Communications.PhoneInfo)
                    {
                        if (phone.PhoneTypeCd == Enums.PhoneTypeCode.Phone.ToString() && phone.CommunicationUseCd == Enums.CommunicationUseCd.Home.ToString())
                        {
                            phoneNumber = phone.PhoneNumber;
                        }
                    }

                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = "Applicant PhoneNumber is not coming";
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        phoneNumber = ConstantVariables.PhoneNumber;
                    }

                    long customerId = 0;
                    long? homeTypeId = null;
                    try
                    {
                        string emailAddress = string.Empty;
                        string firstName = string.Empty;
                        string lastName = string.Empty;
                        string hometypeCode = string.Empty;

                        if (!string.IsNullOrWhiteSpace(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.ResidenceTypeCd))
                        {
                            hometypeCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.ResidenceTypeCd;
                            switch (hometypeCode)
                            {
                                case "Single Family Home":
                                    hometypeCode = "HOMEOWNER";
                                    break;
                                case "Mobile Home":
                                    hometypeCode = "MOBILEHOMEOWNER";
                                    break;
                                case "Apartment1":
                                    hometypeCode = "RENTAL";
                                    break;
                                case "Apartment":
                                    hometypeCode = "APARTMENT";
                                    break;
                            }
                            homeTypeId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.HOMETYPE.ToString() && w.Code == hometypeCode).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                        }
                        if (!string.IsNullOrWhiteSpace(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Communications.EmailInfo.EmailAddr))
                        {
                            emailAddress = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Communications.EmailInfo.EmailAddr;
                        }
                        if (!string.IsNullOrWhiteSpace(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName.GivenName))
                        {
                            firstName = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName.GivenName;
                        }
                        else
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Applicant FirstName is not coming";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                        }
                        if (!string.IsNullOrWhiteSpace(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName.Surname))
                        {
                            lastName = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName.Surname;
                        }
                        else
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Applicant LastName is not coming";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                        }



                        Customer customer = new Customer
                        {

                            FirstName = firstName,
                            LastName = lastName,
                            MiddleName = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName.OtherGivenName,
                            FKStateID = stateId,
                            FKOfficeID = agencyModel.OfficeId,
                            EmailID = (emailAddress != "") ? emailAddress : ConstantVariables.EmailId,
                            ContactNo = phoneNumber,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            IsDeleted = false
                        };
                        db.Customers.Add(customer);
                        db.SaveChanges();

                        customerId = customer.ID;
                    }
                    catch (Exception ex)
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = ex.ToString();
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        Log.Error("Customer exception : " + ex.Message.ToString());

                    }

                    var garazingaddressAddress1 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.Addr1).FirstOrDefault();
                    var garazingaddressAddress2 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.Addr2).FirstOrDefault();
                    try
                    {
                        Log.Debug("Insert xml process garazingaddressAddress1 " + garazingaddressAddress1);
                        int policyTerm = 0;
                        policyTerm = Core.ConstantVariables.PolicyTerm;

                        if (string.IsNullOrWhiteSpace(garazingaddressAddress1))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Address 1 is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }
                        if (string.IsNullOrWhiteSpace(postalCode))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Postal Code is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }

                        if (string.IsNullOrWhiteSpace(city))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Garazing City is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }

                        if (string.IsNullOrWhiteSpace(stateCd))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "State Code is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }
                        if (garazingaddressAddress1.ToUpper().Contains("QUOTE") || city.Contains("QUOTE"))
                        {
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                            agencyModel.IsError = false;
                        }
                        Quote quote = new Quote
                        {
                            Address1 = (garazingaddressAddress1.ToUpper().Contains("QUOTE") ? string.Empty : garazingaddressAddress1),
                            Address2 = (!string.IsNullOrWhiteSpace(garazingaddressAddress2) && garazingaddressAddress2.ToUpper().Contains("QUOTE")) ? string.Empty : garazingaddressAddress2,
                            City = (city.ToUpper().Contains("QUOTE") ? string.Empty : city),
                            OtherPhoneNo = ConstantVariables.PhoneNumber,
                            PolicyTerm = policyTerm,
                            ZipCode = postalCode,
                            FkStateId = stateId,
                            FkQuoteStatusID = quoteStatusID,
                            FkOfficeId = agencyModel.OfficeId,
                            FkCustomerID = customerId,
                            FkQuoteTypeId = quoteTypeID,
                            FkPolicyStatusId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.POLICYSTATUS.ToString() && w.Code == Enums.PolicyStatus.PROGRESS.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                            FKCurrentStepId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.QUOTEORDERSTATUS.ToString() && w.Code == Enums.QuoteOrderStatus.COVERAGE.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                            AssignByID = agencyModel.UserId,
                            CreatedById = agencyModel.UserId,
                            PolicyStatusChangeDate = ConvertTo.GetEstTimeNow(),
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            EffectiveDate = Convert.ToDateTime(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.ContractTerm.EffectiveDt) == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : Convert.ToDateTime(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.ContractTerm.EffectiveDt),
                            ExpirationDate = Convert.ToDateTime(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.ContractTerm.EffectiveDt) == DateTime.MinValue ? ConvertTo.GetEstTimeNow().AddMonths(ConstantVariables.PolicyTerm) : Convert.ToDateTime(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.ContractTerm.EffectiveDt).AddMonths(ConstantVariables.PolicyTerm),
                            IsHide = false,
                            IsVoid = false,
                            FkSourceID = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.APPLICANTSOURCESTATUS.ToString() && w.Code == Enums.ApplicationSource.GOOGLE.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                            IsOnline = false,
                            IsOfferedBetterRate = false,
                            FkInsuranceTypeID = insuranceTypeId,
                            FKInsuranceCompanyId = db.InsuranceCompanies.Where(w => w.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
                            FkHomeTypeId = homeTypeId != 0 ? homeTypeId : null
                        };
                        db.Quotes.Add(quote);
                        db.SaveChanges();


                        agencyModel.QuoteId = quote.ID;
                    }
                    catch (Exception ex)
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = ex.ToString();
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        Log.Error("Quote exception : " + ex.Message.ToString());

                    }

                    if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr != null
                        && model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "MailingAddress").FirstOrDefault() != null)
                    {

                        var mailingaddressAddress1 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "MailingAddress").Select(s => s.Addr1).FirstOrDefault();
                        var mailingaddressAddress2 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "MailingAddress").Select(s => s.Addr2).FirstOrDefault();
                        var mailingstateCd = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "MailingAddress").Select(s => s.StateProvCd).FirstOrDefault();
                        var mailingpostalCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "MailingAddress").Select(s => s.PostalCode).FirstOrDefault();
                        var mailingcity = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.InsuredOrPrincipal.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "MailingAddress").Select(s => s.City).FirstOrDefault();


                        var _zipCodeDetails = commonServices.IsZipCodeCheck(mailingpostalCode);
                        if (_zipCodeDetails != null)
                        {
                            Log.Debug("Insert xml process zipcode details " + JsonConvert.SerializeObject(_zipCodeDetails));

                            mailingstateCd = _zipCodeDetails.state.ToUpper();
                            stateId = commonServices.GetStateIdByCode(_zipCodeDetails.state.ToUpper());
                            mailingcity = _zipCodeDetails.city;
                        }

                        Log.Debug("Insert xml process mailingaddressAddress1 " + mailingaddressAddress1);

                        if (string.IsNullOrWhiteSpace(mailingaddressAddress1))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Mailing Address 1 is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }
                        if (string.IsNullOrWhiteSpace(mailingcity))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "City is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }
                        if (string.IsNullOrWhiteSpace(mailingstateCd))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "State is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }
                        if (string.IsNullOrWhiteSpace(mailingpostalCode))
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = "Postal Code is missing";
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }


                        if (!string.IsNullOrWhiteSpace(mailingaddressAddress1) && (mailingaddressAddress1.ToUpper().Contains("QUOTE") ||
                            mailingaddressAddress2.ToUpper().Contains("QUOTE") ||
                            city.ToUpper().Contains("QUOTE") ||
                            stateCd.ToUpper().Contains("QUOTE")))
                        {
                            agencyModel.IsError = false;
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                        }
                        if (mailingaddressAddress1 == garazingaddressAddress1)
                        {
                            CustomerAddress customerAddress = new CustomerAddress
                            {
                                Address1 = (mailingaddressAddress1.ToUpper().Contains("QUOTE") ? string.Empty : mailingaddressAddress1),
                                Address2 = (!string.IsNullOrWhiteSpace(mailingaddressAddress2) && mailingaddressAddress2.ToUpper().Contains("QUOTE") ? string.Empty : mailingaddressAddress2),
                                City = (mailingcity.ToUpper().Contains("QUOTE") ? string.Empty : mailingcity),
                                ZipCode = mailingpostalCode,
                                FKOfficeID = agencyModel.OfficeId,
                                State = (mailingstateCd.ToUpper().Contains("QUOTE") ? string.Empty : mailingstateCd),
                                FKCustomerId = customerId,
                                FKAddressTypeId = addressType,
                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                IsDuplicate = true,
                            };
                            db.CustomerAddresses.Add(customerAddress);
                            db.SaveChanges();
                        }
                        else
                        {
                            CustomerAddress customerAddress = new CustomerAddress
                            {
                                Address1 = (mailingaddressAddress1.ToUpper().Contains("QUOTE") ? string.Empty : mailingaddressAddress1),
                                Address2 = (!string.IsNullOrWhiteSpace(mailingaddressAddress2) && mailingaddressAddress2.ToUpper().Contains("QUOTE") ? string.Empty : mailingaddressAddress2),
                                City = (mailingcity.ToUpper().Contains("QUOTE") ? string.Empty : mailingcity),
                                ZipCode = mailingpostalCode,
                                FKOfficeID = agencyModel.OfficeId,
                                State = (mailingstateCd.ToUpper().Contains("QUOTE") ? string.Empty : mailingstateCd),
                                FKCustomerId = customerId,
                                FKAddressTypeId = addressType,
                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                IsDuplicate = false
                            };
                            db.CustomerAddresses.Add(customerAddress);
                            db.SaveChanges();
                        }

                    }

                    else
                    {
                        //If mailing address is not available, enter same value of gargage address.
                        CustomerAddress customerAddress = new CustomerAddress
                        {
                            Address1 = (garazingaddressAddress1.ToUpper().Contains("QUOTE") ? string.Empty : garazingaddressAddress1),
                            Address2 = (!string.IsNullOrWhiteSpace(garazingaddressAddress2) && garazingaddressAddress2.ToUpper().Contains("QUOTE")) ? string.Empty : garazingaddressAddress2,
                            City = (city.ToUpper().Contains("QUOTE") ? string.Empty : city),
                            ZipCode = postalCode,
                            FKOfficeID = agencyModel.OfficeId,
                            State = (stateCd.ToUpper().Contains("QUOTE") ? string.Empty : stateCd),
                            FKCustomerId = customerId,
                            FKAddressTypeId = addressType,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            IsDuplicate = true
                        };
                        db.CustomerAddresses.Add(customerAddress);
                        db.SaveChanges();

                    }

                    try
                    {

                        if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy != null)
                        {
                            long insuranceCompanyId = 0;
                            long dayLapse = 0;
                            insuranceCompanyId = !string.IsNullOrEmpty(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.InsurerName) ? db.InsuranceCompanies.Where(x => x.Code == model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.InsurerName).Select(x => x.ID).FirstOrDefault() : db.InsuranceCompanies.Where(f => f.Code == "Other Insurance Company (Not Listed)").Select(s => s.ID).FirstOrDefault();

                            // Check Proof Of prior No Coverage 
                            if ((!string.IsNullOrEmpty(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse) &&
                                !string.IsNullOrEmpty(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.ExpirationDt)) &&
                               (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse != "No Prior Cov" ||
                                model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.ExpirationDt != "1/1/0001"))
                            {
                                string dayLapseVal = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse.Contains("Cov") ? model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse.Replace("Cov", "Coverage") : model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse;
                                if (dayLapseVal == "6 Months Prior Coverage and lapse >= 1 day")
                                {
                                    dayLapse = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.PRIORINSURANCELAPSE.ToString() && w.Code == Enums.PriorInsuranceLapseType.SIXMONTHSPRIORCOVANDLAPSEGREATERTHANONEDAY.ToString().Replace("SIX", "6").Replace("GREATERTHANONE", ">=1")).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                                }
                                else if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse == "Less than 6 Months Prior Cov")
                                {
                                    dayLapse = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.PRIORINSURANCELAPSE.ToString() && w.Code == Enums.PriorInsuranceLapseType.LESSTHESIXMONTHSPRIORCOV.ToString().Replace("SIX", "6")).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                                }
                                else if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.Lapse == "Full 6 Months Prior Cov - no lapse")
                                {
                                    dayLapse = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.PRIORINSURANCELAPSE.ToString() && w.Code == Enums.PriorInsuranceLapseType.FULLSIXMONTHSPRIORCOVNOLAPSE.ToString().Replace("SIX", "6")).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                                }
                                else
                                {
                                    agencyModel.IsError = false;
                                    agencyModel.IsErrorMessage = "No day Lapse match with response";
                                    agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                                }

                                Auto_PriorInsuranceDetails auto_PriorInsuranceDetails = new Auto_PriorInsuranceDetails
                                {

                                    FKQuoteId = agencyModel.QuoteId,
                                    IsAgentOfRecord = false,
                                    FKInsuranceCompanyId = Convert.ToInt32(insuranceCompanyId),
                                    PriorExpirationDate = Convert.ToDateTime(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.ExpirationDt) == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : Convert.ToDateTime(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.ContractTerm.ExpirationDt), //EffectiveDt
                                    DaysLapes = Convert.ToInt32(dayLapse),
                                    PolicyNumber = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.OtherOrPriorPolicy.PolicyNumber,
                                };
                                db.Auto_PriorInsuranceDetails.Add(auto_PriorInsuranceDetails);
                                db.SaveChanges();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = ex.ToString();
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                    }



                    foreach (var vehicle in model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersAutoLineBusiness.PersVeh)
                    {
                        int lienHolderId = 0;
                        try
                        {
                            if (vehicle.AdditionalInterest != null)
                            {
                                var policyState = vehicle.AdditionalInterest.GeneralPartyInfo.Addr[0].StateProv.ToString();
                                var _stateId = db.States.Where(w => w.Code == policyState).Select(s => s.ID).FirstOrDefault();
                                if (!db.LienHolders.Any(x => x.CommercialName.Contains(vehicle.AdditionalInterest.GeneralPartyInfo.NameInfo.CommlName.CommercialName) && x.FkStateID == _stateId))
                                {
                                    LienHolder lienHolder = new LienHolder
                                    {
                                        CommercialName = vehicle.AdditionalInterest.GeneralPartyInfo.NameInfo.CommlName.CommercialName,
                                        Address1 = vehicle.AdditionalInterest.GeneralPartyInfo.Addr[0].Addr1,
                                        Address2 = vehicle.AdditionalInterest.GeneralPartyInfo.Addr[0].Addr2,
                                        City = vehicle.AdditionalInterest.GeneralPartyInfo.Addr[0].City,
                                        ZipCode = vehicle.AdditionalInterest.GeneralPartyInfo.Addr[0].PostalCode,
                                        IsDeleted = false,
                                        CreatedByID = agencyModel.UserId,
                                        FkStateID = _stateId,
                                        CreatedDate = ConvertTo.GetEstTimeNow()
                                    };
                                    db.LienHolders.Add(lienHolder);
                                    db.SaveChanges();
                                    lienHolderId = lienHolder.ID;
                                }
                                else
                                {
                                    lienHolderId = db.LienHolders.Where(x => x.CommercialName.Contains(vehicle.AdditionalInterest.GeneralPartyInfo.NameInfo.CommlName.CommercialName) && x.FkStateID == _stateId).Select(x => x.ID).FirstOrDefault();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = ex.ToString();
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                        }

                        long autoVehicleId = 0;
                        try
                        {
                            var vehicleInformation = DecodeVIN(vehicle.VehIdentificationNumber);
                            if (vehicleInformation != null)
                            {
                                Auto_Vehicle autoVehicle = new Auto_Vehicle
                                {
                                    IsDeleted = false,
                                    FkLienHolderID = (lienHolderId != 0) ? lienHolderId : (int?)null,
                                    Make = vehicleInformation.Maker,
                                    Model = vehicleInformation.Model,
                                    PurchaseDate = Convert.ToDateTime(vehicle.PurchaseDt) == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : Convert.ToDateTime(vehicle.PurchaseDt),
                                    FkQuoteID = agencyModel.QuoteId,
                                    VIN = vehicle.VehIdentificationNumber,
                                    Year = Convert.ToInt32(vehicleInformation.Year),
                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                    VehicleOrderID = db.Auto_Vehicle.Count(w => w.FkQuoteID == agencyModel.QuoteId && w.IsDeleted == false) + 1,
                                    CreatedByID = agencyModel.UserId,
                                    FkOwnerTypeID = (lienHolderId != 0) ? getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.OWNERTYPE.ToString() && w.Code == Enums.OwnerType.LEASED.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault() : (long?)null
                                };
                                db.Auto_Vehicle.Add(autoVehicle);
                                db.SaveChanges();
                                autoVehicleId = autoVehicle.ID;
                            }
                            else
                            {
                                Auto_Vehicle autoVehicle = new Auto_Vehicle
                                {
                                    IsDeleted = false,
                                    FkLienHolderID = (lienHolderId != 0) ? lienHolderId : (int?)null,
                                    Make = vehicle.Manufacturer,
                                    Model = vehicle.Model.Text,
                                    PurchaseDate = Convert.ToDateTime(vehicle.PurchaseDt) == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : Convert.ToDateTime(vehicle.PurchaseDt),
                                    FkQuoteID = agencyModel.QuoteId,
                                    VIN = vehicle.VehIdentificationNumber,
                                    Year = Convert.ToInt32(vehicle.ModelYear),
                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                    VehicleOrderID = db.Auto_Vehicle.Count(w => w.FkQuoteID == agencyModel.QuoteId && w.IsDeleted == false) + 1,
                                    CreatedByID = agencyModel.UserId
                                };
                                db.Auto_Vehicle.Add(autoVehicle);
                                db.SaveChanges();
                                autoVehicleId = autoVehicle.ID;
                            }
                        }
                        catch (Exception ex)
                        {
                            agencyModel.IsError = false;
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                            agencyModel.IsErrorMessage = ex.ToString();
                            Log.Error("Vehicle exception : " + ex.Message.ToString());

                        }

                        var stateCoverages = commonServices.GetCoveragesByStateId(stateId);

                        if (vehicle.Coverage.Count > 0 && vehicle.Id == "Veh1")
                        {
                            try
                            {

                                foreach (var _coverage in vehicle.Coverage)
                                {

                                    if (_coverage.CoverageCd == Enums.Coverages.BI.ToString())
                                    {
                                        var BICoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.BI.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        var BICoverageValue2 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.BI.ToString()).Select(s => s.CoverageValue2).FirstOrDefault();
                                        string coverage = _coverage.Limit.FormatInteger;
                                        if (string.IsNullOrEmpty(coverage))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        var value1 = coverage.Split('/').First();
                                        var value2 = coverage.Split('/').Last();
                                        if (Convert.ToDecimal(value1) < BICoverageValue1)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (Convert.ToDecimal(value2) < BICoverageValue2)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(value1),
                                            Value2 = Convert.ToDecimal(value2)
                                        };

                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();

                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.PD.ToString())
                                    {
                                        var PDCoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.PD.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        if (string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (Convert.ToDecimal(_coverage.Limit.FormatInteger) < PDCoverageValue1)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        decimal value2 = (decimal)0.00;
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                            Value2 = value2
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.PIP.ToString())
                                    {
                                        var PIPCoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.PIP.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        if (string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            if (Convert.ToDecimal(_coverage.Limit.FormatInteger) < PIPCoverageValue1)
                                            {
                                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                            }

                                            decimal value2 = (decimal)0.00;
                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                Value2 = value2
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.PPI.ToString())
                                    {
                                        var PPICoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.PPI.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        if (string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();

                                        }
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            if (Convert.ToDecimal(_coverage.Limit.FormatInteger) < PPICoverageValue1)
                                            {
                                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                            }
                                            decimal value2 = (decimal)0.00;
                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                Value2 = value2
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UMBI.ToString())
                                    {
                                        var UMBICoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UMBI.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        var UMBICoverageValue2 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UMBI.ToString()).Select(s => s.CoverageValue2).FirstOrDefault();
                                        string coverage = _coverage.Limit.FormatInteger;
                                        if (string.IsNullOrEmpty(coverage))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        var value1 = coverage.Split('/').First();
                                        var value2 = coverage.Split('/').Last();
                                        if (Convert.ToDecimal(value1) < UMBICoverageValue1)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (Convert.ToDecimal(value2) < UMBICoverageValue2)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(value1),
                                            Value2 = Convert.ToDecimal(value2)
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UMPD.ToString())
                                    {
                                        var UMPDCoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UMPD.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        if (string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            if (Convert.ToDecimal(_coverage.Limit.FormatInteger) < UMPDCoverageValue1)
                                            {
                                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                            }

                                            decimal value2 = (decimal)0.00;
                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                Value2 = Convert.ToDecimal(value2)
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.LPD.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                            Value2 = value2
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.SAF.ToString())
                                    {
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger)
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UM.ToString())
                                    {
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            if (_coverage.Option != null && _coverage.Option.OptionTypeCd == "R")
                                            {
                                                decimal value1 = (decimal)2.00;

                                                Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = value1,
                                                    Value2 = (decimal)0.00
                                                };
                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                                db.SaveChanges();

                                                Auto_QuoteCoverage _auto_QuoteCoverage = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == Enums.Coverages.UMBI.ToString()).Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                    Value2 = (decimal)0.00
                                                };
                                                db.Auto_QuoteCoverage.Add(_auto_QuoteCoverage);
                                                db.SaveChanges();

                                            }
                                            else
                                            {
                                                decimal value1 = (decimal)1.00;
                                                Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = value1,
                                                    Value2 = (decimal)0.00
                                                };
                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                                db.SaveChanges();

                                                Auto_QuoteCoverage _auto_QuoteCoverage = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == Enums.Coverages.UMBI.ToString()).Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                    Value2 = (decimal)0.00
                                                };
                                                db.Auto_QuoteCoverage.Add(_auto_QuoteCoverage);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.MCCA.ToString())
                                    {

                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger)
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UMPDD.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        var UMPDDCoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UMPDD.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        if (string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            if (Convert.ToDecimal(_coverage.Limit.FormatInteger) < UMPDDCoverageValue1)
                                            {
                                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                            }
                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                Value2 = value2
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.MEDP.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            if (stateCd == "GA")
                                            {
                                                if (_coverage.Option == null)
                                                {
                                                    Auto_QuoteCoverage _auto_QuoteCoverage = new Auto_QuoteCoverage
                                                    {
                                                        FKQuoteID = agencyModel.QuoteId,
                                                        FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                        CreatedDate = ConvertTo.GetEstTimeNow(),
                                                        Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                        Value2 = value2
                                                    };
                                                    db.Auto_QuoteCoverage.Add(_auto_QuoteCoverage);
                                                    db.SaveChanges();
                                                }
                                                if (_coverage.Option != null)
                                                {
                                                    if (_coverage.Option.OptionTypeCd == "NC")
                                                    {
                                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                                        {
                                                            FKQuoteID = agencyModel.QuoteId,
                                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                                            Value1 = Convert.ToDecimal(Convert.ToDouble(_coverage.Limit.FormatInteger) + 0.01),
                                                            Value2 = value2
                                                        };
                                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                                        db.SaveChanges();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                    Value2 = value2
                                                };
                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.MEDPM.ToString()) // Additional Coverage MEDPM
                                    {
                                        decimal value2 = (decimal)0.00;
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {
                                            string coverageCode = _coverage.CoverageCd.Contains("MEDPM") ? _coverage.CoverageCd.Replace("MEDPM", "MEDP") : _coverage.CoverageCd;

                                            if (stateCd == "GA")
                                            {
                                                if (_coverage.Option == null)
                                                {
                                                    Auto_QuoteCoverage _auto_QuoteCoverage = new Auto_QuoteCoverage
                                                    {
                                                        FKQuoteID = agencyModel.QuoteId,
                                                        FkCoverageID = db.Coverages.Where(w => w.Code == coverageCode).Select(s => s.ID).FirstOrDefault(),
                                                        CreatedDate = ConvertTo.GetEstTimeNow(),
                                                        Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                        Value2 = value2
                                                    };
                                                    db.Auto_QuoteCoverage.Add(_auto_QuoteCoverage);
                                                    db.SaveChanges();
                                                }
                                                if (_coverage.Option != null)
                                                {
                                                    if (_coverage.Option.OptionTypeCd == "NC")
                                                    {
                                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                                        {
                                                            FKQuoteID = agencyModel.QuoteId,
                                                            FkCoverageID = db.Coverages.Where(w => w.Code == coverageCode).Select(s => s.ID).FirstOrDefault(),
                                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                                            Value1 = Convert.ToDecimal(Convert.ToDouble(_coverage.Limit.FormatInteger) + 0.01),
                                                            Value2 = value2
                                                        };
                                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                                        db.SaveChanges();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == coverageCode).Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                    Value2 = value2
                                                };
                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UIMBI.ToString())
                                    {
                                        var UIMBICoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UIMBI.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        var UIMBICoverageValue2 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UIMBI.ToString()).Select(s => s.CoverageValue2).FirstOrDefault();
                                        string coverage = _coverage.Limit.FormatInteger;
                                        if (string.IsNullOrEmpty(coverage))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        var value1 = coverage.Split('/').First();
                                        var value2 = coverage.Split('/').Last();
                                        if (Convert.ToDecimal(value1) < UIMBICoverageValue1)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (Convert.ToDecimal(value2) < UIMBICoverageValue2)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(value1),
                                            Value2 = Convert.ToDecimal(value2)
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UNDUM.ToString())
                                    {
                                        string coverageCode = _coverage.CoverageCd.Contains("UNDUM") ? _coverage.CoverageCd.Replace("UNDUM", "UIMBI") : _coverage.CoverageCd;

                                        var UIMBICoverageValue1 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UIMBI.ToString()).Select(s => s.CoverageValue1).FirstOrDefault();
                                        var UIMBICoverageValue2 = stateCoverages.Where(w => w.CoverageCode == Enums.Coverages.UIMBI.ToString()).Select(s => s.CoverageValue2).FirstOrDefault();
                                        string coverage = _coverage.Limit.FormatInteger;
                                        if (string.IsNullOrEmpty(coverage))
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        var value1 = coverage.Split('/').First();
                                        var value2 = coverage.Split('/').Last();
                                        if (Convert.ToDecimal(value1) < UIMBICoverageValue1)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        if (Convert.ToDecimal(value2) < UIMBICoverageValue2)
                                        {
                                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                        }
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == coverageCode).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(value1),
                                            Value2 = Convert.ToDecimal(value2)
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UIMPD.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {

                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                Value2 = value2
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.UNDPD.ToString()) // Additional Coverage UIMPD
                                    {
                                        string coverageCode = _coverage.CoverageCd.Contains("UNDPD") ? _coverage.CoverageCd.Replace("UNDPD", "UIMPD") : _coverage.CoverageCd;

                                        decimal value2 = (decimal)0.00;
                                        if (!string.IsNullOrEmpty(_coverage.Limit.FormatInteger))
                                        {

                                            Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                            {
                                                FKQuoteID = agencyModel.QuoteId,
                                                FkCoverageID = db.Coverages.Where(w => w.Code == coverageCode).Select(s => s.ID).FirstOrDefault(),
                                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                                Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                                Value2 = value2
                                            };
                                            db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                            db.SaveChanges();
                                        }
                                    }
                                    else if (string.IsNullOrWhiteSpace(_coverage.CoverageCd))
                                    {
                                        agencyModel.IsError = false;
                                        agencyModel.IsErrorMessage = "Coverage is not available.";
                                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                    }

                                }

                                List<string> coevargeCode = new List<string>(stateCoverages.Select(s => s.CoverageCode));
                                var results = vehicle.Coverage.Where(m => coevargeCode.Contains(m.CoverageCd)).Count();
                                if (coevargeCode.Count() != results)
                                {
                                    agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = ex.ToString();
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                Log.Error("Coverage exception : " + ex.Message.ToString());
                            }
                        }

                        if (vehicle.Coverage.Count > 0)
                        {
                            try
                            {
                                foreach (var _coverage in vehicle.Coverage)
                                {
                                    string vehicleOrderNo = vehicle.Id.Replace("Veh", "");
                                    long vehicleOrder = Convert.ToInt64(vehicleOrderNo);
                                    long vehicleId = db.Auto_Vehicle.Where(w => w.VehicleOrderID == vehicleOrder && w.FkQuoteID == agencyModel.QuoteId).Select(s => s.ID).FirstOrDefault();


                                    if (_coverage.CoverageCd == Enums.Coverages.COLL.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {

                                            FKQuoteID = agencyModel.QuoteId,
                                            FKVehicleID = autoVehicleId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                            Value2 = value2
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();

                                        if (_coverage.Option != null && !string.IsNullOrWhiteSpace(_coverage.Option.OptionTypeCd))
                                        {
                                            if (_coverage.Option.OptionTypeCd == "B")
                                            {

                                                decimal broadValue1 = (decimal)1.00;
                                                decimal broadValue2 = (decimal)0.00;
                                                Auto_QuoteCoverage auto_QuoteCoveragebroad = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FKVehicleID = autoVehicleId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == "COLLLEVEL").Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = broadValue1,
                                                    Value2 = broadValue2
                                                };

                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoveragebroad);
                                                db.SaveChanges();
                                            }

                                            if (_coverage.Option.OptionTypeCd == "S")
                                            {

                                                decimal LimitedValue1 = (decimal)2.00;
                                                decimal LimitedValue2 = (decimal)0.00;
                                                Auto_QuoteCoverage auto_QuoteCoverageLimited = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FKVehicleID = autoVehicleId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == "COLLLEVEL").Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = LimitedValue1,
                                                    Value2 = LimitedValue2
                                                };

                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoverageLimited);
                                                db.SaveChanges();
                                            }

                                            if (_coverage.Option != null && _coverage.Option.OptionTypeCd == "L")
                                            {
                                                decimal standardValue1 = (decimal)3.00;
                                                decimal standardValue2 = (decimal)0.00;
                                                Auto_QuoteCoverage auto_QuoteCoverageStandard = new Auto_QuoteCoverage
                                                {
                                                    FKQuoteID = agencyModel.QuoteId,
                                                    FKVehicleID = autoVehicleId,
                                                    FkCoverageID = db.Coverages.Where(w => w.Code == "COLLLEVEL").Select(s => s.ID).FirstOrDefault(),
                                                    CreatedDate = ConvertTo.GetEstTimeNow(),
                                                    Value1 = standardValue1,
                                                    Value2 = standardValue2
                                                };

                                                db.Auto_QuoteCoverage.Add(auto_QuoteCoverageStandard);
                                                db.SaveChanges();
                                            }
                                        }
                                    }

                                    if (_coverage.CoverageCd == Enums.Coverages.COMP.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FKVehicleID = autoVehicleId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                            Value2 = value2
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }

                                    if (_coverage.CoverageCd == Enums.Coverages.LUSE.ToString())
                                    {
                                        decimal value2 = (decimal)0.00;
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FKVehicleID = autoVehicleId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == _coverage.CoverageCd).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                            Value2 = value2
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }
                                    else if (_coverage.CoverageCd == Enums.Coverages.RREIM.ToString())
                                    {
                                        string coverageCode = _coverage.CoverageCd.Contains("RREIM") ? _coverage.CoverageCd.Replace("RREIM", "LUSE") : _coverage.CoverageCd;
                                        decimal value2 = (decimal)0.00;
                                        Auto_QuoteCoverage auto_QuoteCoverage = new Auto_QuoteCoverage
                                        {
                                            FKQuoteID = agencyModel.QuoteId,
                                            FKVehicleID = autoVehicleId,
                                            FkCoverageID = db.Coverages.Where(w => w.Code == coverageCode).Select(s => s.ID).FirstOrDefault(),
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            Value1 = Convert.ToDecimal(_coverage.Limit.FormatInteger),
                                            Value2 = value2
                                        };
                                        db.Auto_QuoteCoverage.Add(auto_QuoteCoverage);
                                        db.SaveChanges();
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = ex.ToString();
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.COVERAGE.ToString();
                                Log.Error("Coverage exception : " + ex.Message.ToString());

                            }
                        }



                    }



                    foreach (var driver in model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersAutoLineBusiness.PersDriver)
                    {
                        try
                        {
                            string maritalStatuscd = string.Empty;
                            string gender = string.Empty;
                            string RelationShipCd = string.Empty;
                            string birthDate = string.Empty;
                            if (!string.IsNullOrWhiteSpace(driver.DriverInfo.PersonInfo.MaritalStatusCd))
                            {
                                maritalStatuscd = driver.DriverInfo.PersonInfo.MaritalStatusCd;
                            }
                            else
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = "MaritalStatusCd is not coming";
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                            }
                            if (!string.IsNullOrWhiteSpace(driver.DriverInfo.PersonInfo.GenderCd))
                            {
                                gender = driver.DriverInfo.PersonInfo.GenderCd;
                            }
                            else
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = "GenderCd is not coming";
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                            }
                            if (!string.IsNullOrWhiteSpace(driver.PersDriverInfo.DriverRelationshipToApplicantCd))
                            {
                                RelationShipCd = driver.PersDriverInfo.DriverRelationshipToApplicantCd;
                            }
                            else
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = "DriverRelationshipToApplicantCd is not coming";
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                            }
                            if (Convert.ToDateTime(driver.DriverInfo.PersonInfo.BirthDt) != DateTime.MinValue)
                            {
                                birthDate = driver.DriverInfo.PersonInfo.BirthDt;
                            }
                            else
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = "BirthDate is not coming";
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                            }
                            long marritialStatusId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.MARITALSTATUS.ToString() && w.Code == maritalStatuscd.ToUpper()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                            long licenseStatusId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.LICENSESTATUS.ToString() && w.Code == driver.DriverInfo.License.LicenseStatusCd.ToUpper()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                            //int licensestateId = db.States.Where(w => w.Code == driver.DriverInfo.License.StateProvCd).Select(s => s.ID).FirstOrDefault();
                            int licensestateId;
                            if (driver.DriverInfo.License.StateProvCd == "ZI")
                            {
                                if (driver.DriverInfo.License.LicenseStatusCd == "IT")
                                    licensestateId = db.States.Where(w => w.Code == "IT").Select(s => s.ID).FirstOrDefault();
                                else
                                    licensestateId = db.States.Where(w => w.Code == "FR").Select(s => s.ID).FirstOrDefault();
                            }
                            else
                                licensestateId = db.States.Where(w => w.Code == driver.DriverInfo.License.StateProvCd).Select(s => s.ID).FirstOrDefault();

                            long relationid = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.DRIVERRELATION.ToString() && w.Code == RelationShipCd.ToUpper()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                            long driverOccupationId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.DRIVEROCCUPATION.ToString() && w.Code == driver.DriverInfo.PersonInfo.OccupationClassCd.ToUpper()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                            long milatrytypeId = 0;
                            long warehouseTypeId = 0;
                            if (driver.DriverInfo.License.StateProvCd == "SC")
                            {
                                if (driver.PersDriverInfo.Military == Enums.IsDriverDefensive.Y.ToString())
                                    milatrytypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.ARMY.ToString());
                                else
                                    milatrytypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.NONE.ToString());

                                if (driver.PersDriverInfo.MembershipClub == Enums.IsDriverDefensive.Y.ToString())
                                    warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.SAMSCLUB.ToString());
                                else
                                    warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.NONE.ToString());
                            }

                            Auto_Driver auto_Driver = new Auto_Driver
                            {
                                BirthDate = Convert.ToDateTime(birthDate),
                                Gender = gender,
                                FirstName = driver.GeneralPartyInfo.NameInfo.PersonName.GivenName,
                                LastName = driver.GeneralPartyInfo.NameInfo.PersonName.Surname,
                                FkMaritalStatusID = (marritialStatusId != 0) ? marritialStatusId : getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.MARITALSTATUS.ToString() && w.Code == Enums.MaritalStatus.SINGLE.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                                LicenseStatus = (licenseStatusId != 0) ? licenseStatusId : getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.LICENSESTATUS.ToString() && w.Code == Enums.LicenseStatus.VALID.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                                LicenseNo = (driver.DriverInfo.License != null && !string.IsNullOrEmpty(driver.DriverInfo.License.LicensePermitNumber) ? (driver.DriverInfo.License.LicensePermitNumber.ToUpper().StartsWith("X") ? string.Empty : driver.DriverInfo.License.LicensePermitNumber) : string.Empty),
                                FkStateID = (licensestateId != 0) ? licensestateId : db.States.Where(w => w.Code == "MI").Select(s => s.ID).FirstOrDefault(),
                                FkRelationCdID = (relationid != 0) ? relationid : getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.DRIVERRELATION.ToString() && w.Code == Enums.DRIVERRELATION.OTHER.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                                IsDefensiveDriverCourse = driver.PersDriverInfo.DefensiveDriverCd == Enums.IsDriverDefensive.N.ToString() ? false : true,
                                FkDriverOccupationID = (driverOccupationId != 0) ? driverOccupationId : getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.DRIVEROCCUPATION.ToString() && w.Code == Enums.DriverOccupation.OTHER.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault(),
                                FkQuoteID = agencyModel.QuoteId,
                                IsDeleted = false,
                                DriverOrderID = db.Auto_Driver.Count(w => w.FkQuoteID == agencyModel.QuoteId && w.IsDeleted == false) + 1,
                                IsExcluded = driver.PersDriverInfo.DriverTypeCd == Enums.ExcludedDriver.E.ToString() ? true : false,
                                IsSeniorDriver = false,
                                IsWorkLossBenefit = driver.PersDriverInfo.IsWorkLoss == "Opt2" || driver.PersDriverInfo.IsWorkLoss == "Opt3" ? true : false,
                                IsSR22 = driver.PersDriverInfo.FinancialResponsibilityFiling.FilingCd == "SR22" ? true : false,
                                ViolationPoints = 0,
                                CreatedDate = ConvertTo.GetEstTimeNow(),
                                CreatedByID = agencyModel.UserId
                            };
                            if (driver.DriverInfo.License.StateProvCd == "SC")
                            {
                                auto_Driver.FkMilitaryTypeId = milatrytypeId;
                                auto_Driver.FkWarehouseTypeId = warehouseTypeId;
                            }
                            db.Auto_Driver.Add(auto_Driver);
                            db.SaveChanges();

                            if (driver.PersDriverInfo.FinancialResponsibilityFiling.FilingFeeAmt.Amt != "0")
                            {
                                Auto_RateHistoryDetail _rateHistoryDetail = new Auto_RateHistoryDetail()
                                {
                                    FkCoverageID = db.Coverages.Where(x => x.Code == Enums.Coverages.SR22Fee.ToString()).Select(s => s.ID).FirstOrDefault(),
                                    FKQuoteID = agencyModel.QuoteId,
                                    FKInsuranceCompanyID = db.InsuranceCompanies.Where(f => f.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
                                    Amount = Convert.ToDecimal(driver.PersDriverInfo.FinancialResponsibilityFiling.FilingFeeAmt.Amt),
                                    Name = "SR22Fee",
                                    VehicleID = 0,
                                    RateTransactionID = Guid.NewGuid()
                                };
                                db.Auto_RateHistoryDetail.Add(_rateHistoryDetail);
                                db.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            agencyModel.IsError = false;
                            agencyModel.IsErrorMessage = ex.ToString();
                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                            Log.Error("Driver exception : " + ex.Message.ToString());

                        }
                    }
                    if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.AccidentViolation.Count != 0)
                    {

                        foreach (var accident in model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.AccidentViolation)
                        {
                            try
                            {
                                string driverOrderNo = accident.DriverRef.Replace("Drv", "");
                                long driverOrder = Convert.ToInt64(driverOrderNo);
                                long driverId = db.Auto_Driver.Where(w => w.DriverOrderID == driverOrder && w.FkQuoteID == agencyModel.QuoteId).Select(s => s.ID).FirstOrDefault();
                                int fkautoviolationId = db.Auto_Violation.Where(w => w.Code == accident.AccidentViolationCd).Select(s => s.ID).FirstOrDefault();
                                if (fkautoviolationId != 0)
                                {
                                    if (accident.AccidentViolationDesc != "NO HIT/NO MATCH/ERROR")
                                    {
                                        Auto_DriverViolation auto_DriverViolation = new Auto_DriverViolation
                                        {
                                            FkDriverId = driverId,
                                            FKAutoViolationId = db.Auto_Violation.Where(w => w.Code == accident.AccidentViolationCd).Select(s => s.ID).FirstOrDefault(),
                                            ViolationDate = Convert.ToDateTime(accident.AccidentViolationDt) == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : Convert.ToDateTime(accident.AccidentViolationDt),
                                            Points = accident.NumSurchargePoints,
                                            CreatedDate = ConvertTo.GetEstTimeNow(),
                                            CreatedById = agencyModel.UserId,
                                            IsMVR = false
                                        };
                                        db.Auto_DriverViolation.Add(auto_DriverViolation);
                                        db.SaveChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                agencyModel.IsError = false;
                                agencyModel.IsErrorMessage = ex.ToString();
                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                            }


                        }
                    }

                    if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersAutoLineBusiness.PersDriver.Count == 0)
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = "Driver is missing";
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                    }
                    if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersAutoLineBusiness.PersVeh.Count == 0)
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = "Vehicle is missing";
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                    }
                    try
                    {

                        Auto_QuoteDetail auto_QuoteDetail = new Auto_QuoteDetail()
                        {
                            FKQuoteID = agencyModel.QuoteId,
                            RateTransactionID = Guid.NewGuid(),
                            RateDate = ConvertTo.GetEstTimeNow(),
                            IsChange = false,
                            IsGoodStudentDiscount = false,
                            IsHomeownersDiscount = false,
                            IsNonOwner = false,
                            ResidenceType = 0,
                            MonthsInResidence = 0,
                            IsPaperLessDiscount = false,

                        };
                        db.Auto_QuoteDetail.Add(auto_QuoteDetail);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        agencyModel.IsError = false;
                        agencyModel.IsErrorMessage = ex.ToString();
                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

                    }

                }
                catch (Exception ex)
                {
                    agencyModel.IsError = false;
                    agencyModel.IsErrorMessage = ex.ToString();
                    agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
                    Log.Error("Policy XML exception : " + ex.Message.ToString());

                }


                return agencyModel;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public List<QuoteRateResponseModel> GetExistsQuoteRatesList(long quoteId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                if (db.Auto_QuoteDetail.Where(w => w.FKQuoteID == quoteId && w.RateDate == null).Any())
                {
                    return null;
                }

                int _PolicyTerm = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.PolicyTerm).FirstOrDefault();

                var _StateCoverageList = (from aqc in db.Auto_QuoteCoverage
                                          join c in db.Coverages on aqc.FkCoverageID equals c.ID
                                          let sc = (from sc in db.StateCoverages
                                                    where c.ID == sc.FkCoverageID
                                                    select sc).FirstOrDefault()
                                          where aqc.FKQuoteID == quoteId
                                          select new StateCoverageValues
                                          {
                                              Code = (sc == null ? string.Empty : c.Name),
                                              StateId = (sc == null ? 0 : sc.FkStateID),
                                              StateCoverageId = sc == null ? 0 : sc.ID,
                                              CoverageId = aqc.FkCoverageID,
                                              OrderId = sc == null ? 0 : sc.OrderID,
                                              Value1 = aqc.Value1 ?? 0,
                                              Value2 = aqc.Value2 ?? 0,
                                              DisplayTextValue1 = sc == null ? "0.00" : sc.DisplayTextValue1,
                                              DisplayTextValue2 = sc == null ? "0.00" : sc.DisplayTextValue2,
                                              IsSelected = (sc == null ? false : sc.IsSelected)
                                          }).ToList();

                var _resp = (from aq in db.Auto_RateHistory
                             join ic in db.InsuranceCompanies on aq.FKInsuranceCompanyID equals ic.ID

                             where aq.FKQuoteID == quoteId

                             select new QuoteRateResponseModel
                             {

                                 CarrierQuoteID = aq.CarrierQuoteID,

                                 CompanyId = aq.FKInsuranceCompanyID,
                                 CompanyName = ic.Name,
                                 Description = aq.Description,
                                 DownPayment = aq.DownPayment,
                                 ErrorResponse = string.Empty,
                                 IsError = false,
                                 MonthlyPayment = aq.MonthllyPayment,
                                 NoOfInstallement = aq.NumberOfPayment ?? 0,
                                 NSDFees = aq.NSDFee,
                                 PayableDownPayment = aq.DownPayment + aq.NSDFee,
                                 PolicyTerm = _PolicyTerm,
                                 RateId = aq.ID,
                                 TotalPremium = aq.Premium,
                                 //    StateCoverage = _StateCoverageList,

                             }).ToList();

                List<QuoteRateResponseModel> _returnQuoteResponeModel = new List<QuoteRateResponseModel>();

                _returnQuoteResponeModel = _resp.OrderBy(o => o.DownPayment).ThenBy(o => o.MonthlyPayment).ToList();

                foreach (var childRateHistory in _returnQuoteResponeModel)
                {
                    childRateHistory._quoteDetailsResponseAllDetails = _resp.Where(w => w.CompanyId == childRateHistory.CompanyId).Select(s => new QuoteRateResponseDetailsModel
                    {
                        RateId = s.RateId,
                        DownPayment = s.DownPayment,
                        MonthlyPayment = s.MonthlyPayment,
                        NSDFees = s.NSDFees,
                        IsError = s.IsError,
                        ErrorResponse = s.ErrorResponse,
                        BridgeURL = s.BridgeURL,
                        Description = s.Description,
                        PayableDownPayment = s.PayableDownPayment,
                        TotalPremium = s.TotalPremium,

                    }).ToList();
                }
                return _returnQuoteResponeModel;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="contractNo"></param>
        /// <param name="model"></param>
        public AgencyModel InsertAgency(string contractNo, ACORD model)
        {
            AgencyModel agencyModel = new AgencyModel();
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    AutoQuoteServices autoQuoteServices = new AutoQuoteServices();

                    long raterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault();
                    var AgencyByContractNo = db.RaterProviderCredentials.Where(w => w.FKRaterProviderFormId == raterProviderFormId && w.ValueText == contractNo).FirstOrDefault();
                    string agencyAddress1 = string.Empty;
                    string agencyAddress2 = string.Empty;
                    string city = string.Empty;
                    string stateCode = string.Empty;
                    string postalCode = string.Empty;
                    if (AgencyByContractNo == null)
                    {
                        var nameCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.NameInfo.CommlName.CommercialName;
                        var agencyCode = nameCode.Substring(0, nameCode.IndexOf(" ")).ToUpper();
                        if (model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr != null
                        && model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").FirstOrDefault() != null)
                        {
                            agencyAddress1 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.Addr1).FirstOrDefault();
                            agencyAddress2 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.Addr2).FirstOrDefault();
                            city = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.City).FirstOrDefault();
                            stateCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.StateProvCd).FirstOrDefault();
                            postalCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Addr.Where(w => w.AddrTypeCd == "StreetAddress").Select(s => s.PostalCode).FirstOrDefault();
                        }
                        Agency agency = new Agency
                        {
                            CommercialName = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.NameInfo.CommlName.CommercialName,
                            EmailID = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Communications.EmailInfo != null ? model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.Communications.EmailInfo.EmailAddr : string.Empty,
                            Name = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.NameInfo.CommlName.CommercialName,
                            Address1 = agencyAddress1,
                            Address2 = agencyAddress2,
                            City = city,
                            Zipcode = postalCode,
                            FKStateId = db.States.Where(w => w.Code == stateCode).Select(s => s.ID).FirstOrDefault(),
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            Code = agencyCode,
                            IsDeleted = false,
                            CreatedByID = 0
                        };

                        db.Agencies.Add(agency);
                        db.SaveChanges();

                        OfficeGroup officeGroup = new OfficeGroup
                        {
                            FKAgencyID = agency.ID,
                            Code = agency.Code,
                            IsActive = true,
                            IsDeleted = false,
                            CreatedByID = 0,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            CommercialName = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.NameInfo.CommlName.CommercialName,
                            Name = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.NameInfo.CommlName.CommercialName
                        };

                        db.OfficeGroups.Add(officeGroup);
                        db.SaveChanges();

                        Office office = new Office
                        {
                            FKOfficeGroupID = officeGroup.ID,
                            FKAgencyID = agency.ID,
                            FKStateID = (int)agency.FKStateId,
                            IsDeleted = false,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            CreatedByID = 0,
                            EmailID = officeGroup.Code + "@email.com",
                            Name = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Producer.GeneralPartyInfo.NameInfo.CommlName.CommercialName,
                            Code = officeGroup.Code
                        };


                        db.Offices.Add(office);
                        db.SaveChanges();

                        long roleId = db.Roles.Where(w => w.Code == Enums.Role.ADMINISTRATOR.ToString()).Select(f => f.ID).FirstOrDefault();
                        User user = new User
                        {
                            FKOfficeID = office.ID,
                            FKRoleID = roleId,
                            FirstName = agency.Code.ToLower(),
                            LastName = agency.Code.ToLower(),
                            EmailID = agency.EmailID,
                            Gender = Enums.Gender.M.ToString(),
                            DOB = ConvertTo.GetEstTimeNow(),
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            UserName = agency.Code.ToLower(),
                            Password = Arrowhead.POS.Core.StringExtension.CreatePassword(8),
                            IsActive = true,
                            IsDeleted = false
                        };
                        db.Users.Add(user);
                        db.SaveChanges();

                        int raterFkstateId = db.States.Where(w => w.Code == stateCode).Select(s => s.ID).FirstOrDefault();
                        RaterProviderMode raterProviderMode = new RaterProviderMode
                        {
                            FKAgencyID = agency.ID,
                            FKStateID = raterFkstateId,
                            IsLiveMode = true,
                            CreatedByID = user.ID,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            FKRaterServiceProviderID = db.RaterServicesProviders.Where(w => w.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
                            ModifiedByID = user.ID,
                            ModifiedDate = ConvertTo.GetEstTimeNow()
                        };
                        db.RaterProviderModes.Add(raterProviderMode);
                        db.SaveChanges();

                        RaterProviderCredential raterProviderCredential = new RaterProviderCredential
                        {
                            CreatedByID = user.ID,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            FKRaterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault(),
                            FKRaterProviderModeID = raterProviderMode.ID,
                            IsLiveMode = true,
                            ValueText = contractNo
                        };
                        db.RaterProviderCredentials.Add(raterProviderCredential);
                        db.SaveChanges();

                        agencyModel.AgencyCode = agency.Code;
                        agencyModel.AgencyName = agency.Name;
                        agencyModel.AgencyEmailId = agency.EmailID;
                        agencyModel.AgencyId = agency.ID;
                        agencyModel.OfficeId = office.ID;
                        agencyModel.ProducerCode = Convert.ToInt32(contractNo); // set producer code
                        agencyModel.UserId = user.ID;

                    }
                    else
                    {
                        agencyModel = autoQuoteServices.GetAgencyDetailsByContratNo(contractNo);

                    }



                }
                catch (Exception ex)

                {
                    agencyModel.IsError = false;
                    Log.Error("Agency exception : " + ex.Message.ToString());
                }

                return agencyModel;



            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AgencyModel InserAgencyByProducerCode(AgencyProducerCodeModel model)
        {
            AgencyModel agencyModel = new AgencyModel();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            try
            {
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {

                    long raterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault();
                    var AgencyByContractNo = db.RaterProviderCredentials.Where(w => w.FKRaterProviderFormId == raterProviderFormId && w.ValueText == model.ProducerCode).FirstOrDefault();


                    if (AgencyByContractNo == null)
                    {
                        var nameCode = model.AgencyName;
                        var agencyCode = nameCode.Substring(0, nameCode.IndexOf(" ")).ToUpper();
                        Agency agency = new Agency
                        {
                            CommercialName = model.AgencyName,
                            Name = model.AgencyName,
                            Address1 = model.Address,
                            Address2 = model.Address2,
                            City = model.City,
                            Zipcode = model.Zip,
                            FKStateId = db.States.Where(w => w.Code == model.State).Select(s => s.ID).FirstOrDefault(),
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            Code = agencyCode,
                            IsDeleted = false,
                            IsSetupCompleted = false,
                            CreatedByID = 0
                        };

                        db.Agencies.Add(agency);
                        db.SaveChanges();

                        OfficeGroup officeGroup = new OfficeGroup
                        {
                            FKAgencyID = agency.ID,
                            Code = agency.Code,
                            IsActive = true,
                            IsDeleted = false,
                            CreatedByID = 0,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            CommercialName = model.AgencyName,
                            Name = model.AgencyName
                        };

                        db.OfficeGroups.Add(officeGroup);
                        db.SaveChanges();

                        Office office = new Office
                        {
                            FKOfficeGroupID = officeGroup.ID,
                            FKAgencyID = agency.ID,
                            FKStateID = (int)agency.FKStateId,
                            IsDeleted = false,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            CreatedByID = 0,
                            EmailID = officeGroup.Code + "@email.com",
                            Name = model.AgencyName,
                            Code = officeGroup.Code
                        };


                        db.Offices.Add(office);
                        db.SaveChanges();

                        long roleId = db.Roles.Where(w => w.Code == Enums.Role.ADMINISTRATOR.ToString()).Select(f => f.ID).FirstOrDefault();
                        User user = new User
                        {
                            FKOfficeID = office.ID,
                            FKRoleID = roleId,
                            FirstName = agency.Code.ToLower(),
                            LastName = agency.Code.ToLower(),
                            EmailID = agency.EmailID,
                            Gender = Enums.Gender.M.ToString(),
                            DOB = ConvertTo.GetEstTimeNow(),
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            UserName = agency.Code.ToLower(),
                            Password = Arrowhead.POS.Core.StringExtension.CreatePassword(8),
                            IsActive = true,
                            IsDeleted = false
                        };
                        db.Users.Add(user);
                        db.SaveChanges();

                        int raterFkstateId = db.States.Where(w => w.Code == model.State).Select(s => s.ID).FirstOrDefault();
                        RaterProviderMode raterProviderMode = new RaterProviderMode
                        {
                            FKAgencyID = agency.ID,
                            FKStateID = raterFkstateId,
                            IsLiveMode = true,
                            CreatedByID = user.ID,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            FKRaterServiceProviderID = db.RaterServicesProviders.Where(w => w.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
                            ModifiedByID = user.ID,
                            ModifiedDate = ConvertTo.GetEstTimeNow()
                        };
                        db.RaterProviderModes.Add(raterProviderMode);
                        db.SaveChanges();

                        RaterProviderCredential raterProviderCredential = new RaterProviderCredential
                        {
                            CreatedByID = user.ID,
                            CreatedDate = ConvertTo.GetEstTimeNow(),
                            FKRaterProviderFormId = db.RaterProviderForms.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.ID).FirstOrDefault(),
                            FKRaterProviderModeID = raterProviderMode.ID,
                            IsLiveMode = true,
                            ValueText = model.ProducerCode
                        };
                        db.RaterProviderCredentials.Add(raterProviderCredential);
                        db.SaveChanges();

                        agencyModel.AgencyCode = agency.Code;
                        agencyModel.AgencyName = agency.Name;
                        agencyModel.AgencyEmailId = agency.EmailID;
                        agencyModel.AgencyId = agency.ID;
                        agencyModel.OfficeId = office.ID;
                        agencyModel.ZipCode = agency.Zipcode;
                        agencyModel.StateId = (int)agency.FKStateId;
                        agencyModel.UserId = user.ID;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return agencyModel;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AgencyModel CheckProducerCode(string producerCode)
        {
            AgencyModel agencyModel = new AgencyModel();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();

            agencyModel = autoQuoteServices.GetAgencyDetailsByContratNo(producerCode);

            return agencyModel;
        }

        //use for paymen section

        //  Guid RateTransId = 
        //foreach (var payment in model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PaymentOption)
        //{
        //    DateTime _installmentDueDate = Convert.ToDateTime(payment.InstallmentInfo.Where(w => w.InstallmentNumber == "1").Select(s => s.InstallmentDueDt).FirstOrDefault());
        //    Auto_RateHistory auto_RateHistory = new Auto_RateHistory
        //    {
        //        Description = payment.Description,
        //        PayPlanID = payment.PayplanId,
        //        NumberOfPayment = Convert.ToInt32(payment.NumPayments),
        //        FKInsuranceCompanyID = db.InsuranceCompanies.Where(f => f.Code == "ARROWHEAD").Select(s => s.ID).FirstOrDefault(),
        //        DownPayment = Convert.ToDecimal(payment.InstallmentInfo[0].InstallmentAmt.Amt),
        //        FKQuoteID = quote.ID,
        //        PGProcessingFees = 0,
        //        AgentFee = 0,
        //        NSDFee = 0,
        //        ApplicationFee = 0,
        //        OtherFee = 0,
        //        InstallmenetFee = Convert.ToDecimal(payment.InstallmentFeeAmt),
        //        RateTransactionID = RateTransId,
        //        Premium = Convert.ToDecimal(model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PolicySummaryInfo.FullTermAmt.Amt),
        //        InstallmentDueDate = _installmentDueDate.Date == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : _installmentDueDate.Date,
        //        BridgeURL = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersAutoLineBusiness.CompanyURL,
        //        MonthllyPayment = Convert.ToDecimal(payment.InstallmentInfo.Where(w => w.InstallmentNumber == "1").Select(s => s.InstallmentAmt.Amt).FirstOrDefault()),
        //        CarrierQuoteID = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.QuoteInfo.CompanysQuoteNumber,
        //        CreatedDate = ConvertTo.GetEstTimeNow()
        //    };
        //    db.Auto_RateHistory.Add(auto_RateHistory);
        //    db.SaveChanges();

        //}

        //use for address



        //            try
        //                    {
        //                        var garageingAddress = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.Location.Addr;
        //                        if (garageingAddress == null)
        //                        {
        //                            agencyModel.IsError = false;
        //                            agencyModel.IsErrorMessage = "Garage Address is missing";
        //                            agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                        }
        //                        //Validate Mailing Address If Exist then Check Garaging Address
        //                        else
        //                        {
        //                            if (string.IsNullOrWhiteSpace(garageingAddress.Addr1))
        //                            {
        //                                agencyModel.IsError = false;
        //                                agencyModel.IsErrorMessage = "Garage Address 1 is missing";
        //                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                            }

        //                            if (string.IsNullOrWhiteSpace(garageingAddress.City))
        //                            {

        //                                agencyModel.IsError = false;
        //                                agencyModel.IsErrorMessage = "Garaging City is missing";
        //                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                            }

        //                            if (string.IsNullOrWhiteSpace(garageingAddress.PostalCode))
        //                            {

        //                                agencyModel.IsError = false;
        //                                agencyModel.IsErrorMessage = "Postal Code is missing";
        //                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                            }

        //                            if (string.IsNullOrWhiteSpace(garageingAddress.StateProvCd))
        //                            {
        //                                agencyModel.IsError = false;
        //                                agencyModel.IsErrorMessage = "State Code is missing";
        //                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                            }
        //                        }

        //                        if ((string.IsNullOrWhiteSpace(garageingAddress.Addr1)))
        //                        {

        //                            CustomerAddress customerAddress = new CustomerAddress
        //                            {
        //                                Address1 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr.Addr1,
        //                                Address2 = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr.Addr2,
        //                                City = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr.City,
        //                                ZipCode = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr.PostalCode,
        //                                FKOfficeID = agencyModel.OfficeId,
        //                                State = model.InsuranceSvcRs.PersAutoPolicyQuoteInqRs.PersPolicy.PersApplicationInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr.StateProvCd,
        //                                FKCustomerId = customerId,
        //                                FKAddressTypeId = addressType,
        //                                CreatedDate = ConvertTo.GetEstTimeNow(),
        //                                IsDuplicate = true
        //                            };
        //db.CustomerAddresses.Add(customerAddress);
        //                            db.SaveChanges();
        //                        }
        //                        else
        //                        {
        //                            var zipCodeDetails = (!string.IsNullOrWhiteSpace(garageingAddress.PostalCode) ? commonServices.IsZipCodeCheck(garageingAddress.PostalCode) : null);
        //                            if (zipCodeDetails == null)
        //                            {
        //                                agencyModel.IsError = false;
        //                                agencyModel.IsErrorMessage = "ZipCode Details missing";
        //                                agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                            }
        //                            else
        //                            {
        //                                if (string.IsNullOrWhiteSpace(zipCodeDetails.city))
        //                                {
        //                                    agencyModel.IsError = false;
        //                                    agencyModel.IsErrorMessage = "City is missing";
        //                                    agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                                }

        //                                if (string.IsNullOrWhiteSpace(zipCodeDetails.state))
        //                                {
        //                                    agencyModel.IsError = false;
        //                                    agencyModel.IsErrorMessage = "State is missing";
        //                                    agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();
        //                                }
        //                            }

        //                            CustomerAddress customerAddress = new CustomerAddress
        //                            {
        //                                Address1 = garageingAddress.Addr1.ToUpper().Contains("QUOTE") ? string.Empty : garageingAddress.Addr1,
        //                                Address2 = garageingAddress.Addr2.ToUpper().Contains("QUOTE") ? string.Empty : garageingAddress.Addr2,
        //                                ZipCode = garageingAddress.PostalCode,
        //                                City = (!string.IsNullOrWhiteSpace(garageingAddress.City)) ? (garageingAddress.City.ToUpper().Contains("QUOTE") ? string.Empty : (zipCodeDetails != null ? zipCodeDetails.city : string.Empty)) : string.Empty,
        //                                FKCustomerId = customerId,
        //                                FKOfficeID = agencyModel.OfficeId,
        //                                State = (!string.IsNullOrWhiteSpace(garageingAddress.StateProvCd)) ? (garageingAddress.StateProvCd.ToUpper().Contains("QUOTE") ? string.Empty : (zipCodeDetails != null ? zipCodeDetails.state : string.Empty)) : string.Empty,
        //                                FKAddressTypeId = addressType,
        //                                CreatedDate = ConvertTo.GetEstTimeNow(),
        //                                IsDuplicate = false
        //                            };
        //db.CustomerAddresses.Add(customerAddress);
        //                            db.SaveChanges();
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        agencyModel.IsError = false;
        //                        agencyModel.IsErrorMessage = ex.ToString();
        //                        agencyModel.PolicyXMLStep = Enums.PolicyXMLStep.APPLICANT.ToString();

        //                    }

    }
}
