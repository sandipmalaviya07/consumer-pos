﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using EntityFramework.Extensions;


namespace Arrowhead.POS.Service
{
    public class DashboardService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyID"></param>
        /// <param name="startDateFilter"></param>
        /// <param name="endDateFilter"></param>
        /// <returns></returns>
        public List<CityCountModel> GetCityCounts(int agencyID, DateTime startDateFilter, DateTime endDateFilter)
        {
            using (var db = new ArrowheadPOSEntities())
            {


                var query = (from q in db.Quotes
                             let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                             let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                             let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                             where q.City != null
                             select new { q, a }).AsQueryable();

                if (agencyID > 0)
                {
                    query = query.Where(f => f.a.ID == agencyID).AsQueryable();

                }

                var startDate = new DateTime(startDateFilter.Year, startDateFilter.Month, startDateFilter.Day, 0, 0, 0);
                var endDate = new DateTime(endDateFilter.Year, endDateFilter.Month, endDateFilter.Day, 23, 59, 59);
                query = query.Where(f => f.q.EffectiveDate >= startDate && f.q.EffectiveDate <= endDate).AsQueryable();



                var query1 = query.GroupBy(f => f.q.City)
                        .Select(f => new CityCountModel()
                        {
                            ColumnName = f.Key,
                            Count = f.Count()
                        });

                var lst = query1.OrderByDescending(f => f.Count).ToList();

                return lst;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyID"></param>
        /// <param name="startDateFilter"></param>
        /// <param name="endDateFilter"></param>
        /// <returns></returns>
        public List<TotalSoldPolicy> GetSoldPolicyCount(int agencyID, int year)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {

                int _processFilterByOrderNo = Enums.PolicyStatusFilter.PROCESS.GetHashCode();
                int _policyFilterByOrderNo = Enums.PolicyStatusFilter.POLICY.GetHashCode();
                long fkpolicystatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                var query = (from q in db.Quotes
                             let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                             let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                             let pst = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == fkpolicystatusId)
                             let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                             where q.PolicyNumber != null && q.FkPolicyStatusId == fkpolicystatusId
                             select new { q, a, pst }).AsQueryable();


                if (agencyID > 0)
                {
                    query = query.Where(f => f.a.ID == agencyID).AsQueryable();

                }
                var startDate = new DateTime(year, 1, 1, 0, 0, 0);
                var endDate = new DateTime(year, 12, 31, 23, 59, 59);



                query = query.Where(f => f.q.EffectiveDate >= startDate && f.q.EffectiveDate <= endDate).AsQueryable();

                query = query.Where(w => w.pst.OrderNo > _processFilterByOrderNo && w.pst.OrderNo <= _policyFilterByOrderNo).AsQueryable();

                var query1 = query.GroupBy(f => f.q.EffectiveDate.Value.Month)
                      .Select(f => new TotalSoldPolicy()
                      {

                          PolicyCount = f.Count(),
                          MonthName = f.Select(s => s.q.EffectiveDate.Value.Month).FirstOrDefault().ToString()

                      });

                var lst = query1.OrderBy(f => f.MonthName).ToList();


                List<TotalSoldPolicy> _soldPolicyList = new List<TotalSoldPolicy>();

                for (int i = 1; i <= 12; i++)
                {
                    TotalSoldPolicy policy = new TotalSoldPolicy
                    {
                        PolicyCount = 0,
                        MonthName = i.ToString()
                    };
                    _soldPolicyList.Add(policy);
                }
                List<TotalSoldPolicy> _RespList = new List<TotalSoldPolicy>();
                foreach (var item in _soldPolicyList)
                {
                    TotalSoldPolicy policy = new TotalSoldPolicy
                    {
                        PolicyCount = lst.Where(w => w.MonthName == item.MonthName).Select(s => s.PolicyCount).FirstOrDefault(),
                        MonthName = item.MonthName
                    };
                    _RespList.Add(policy);
                }

                _RespList = _RespList.Select(c => { c.MonthName = new DateTime(2015, Convert.ToInt32(c.MonthName), 1).ToString("MMM", CultureInfo.CreateSpecificCulture("en")); return c; }).ToList();

                return _RespList;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyID"></param>
        /// <param name="startDateFilter"></param>
        /// <param name="endDateFilter"></param>
        /// <returns></returns>
        public List<QuoteZipCodeModel> GetQuoteZipCodes(int agencyID, DateTime startDateFilter, DateTime endDateFilter)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {

                var query = (from q in db.Quotes
                             let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                             let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                             let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                             let z = db.ZipCodeLatLngs.FirstOrDefault(f => f.ZipCode.ToString() == q.ZipCode)
                             where q.ZipCode != null
                             select new { q, a, z }).AsQueryable();
                if (agencyID > 0)
                {
                    query = query.Where(f => f.a.ID == agencyID).AsQueryable();

                }

                var startDate = new DateTime(startDateFilter.Year, startDateFilter.Month, startDateFilter.Day, 0, 0, 0);
                var endDate = new DateTime(endDateFilter.Year, endDateFilter.Month, endDateFilter.Day, 23, 59, 59);
                query = query.Where(f => f.q.EffectiveDate >= startDate && f.q.EffectiveDate <= endDate).AsQueryable();


                var query1 = query.GroupBy(f => f.q.ZipCode)
                      .Select(f => new QuoteZipCodeModel()
                      {
                          title = f.Key,
                          QuoteCount = f.Count(),
                          latitude = f.Select(s => s.z.Latitude).FirstOrDefault(),
                          longitude = f.Select(s => s.z.Longitude).FirstOrDefault()
                      });




                var lst = query1.OrderByDescending(f => f.QuoteCount).ToList();
                return lst;

            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyID"></param>
        /// <param name="startDateFilter"></param>
        /// <param name="endDateFilter"></param>
        /// <returns></returns>
        public List<PolicyAndQuoteSummaryResult> GetQuoteandPolicyCount(int agencyID, DateTime startDateFilter, DateTime endDateFilter)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                long activePolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                long newPollicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.NEW.ToString());

                var query = (from q in db.Quotes
                             let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                             let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                             let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                             select new { q, a }).AsQueryable();


                if (agencyID > 0)
                {
                    query = query.Where(f => f.a.ID == agencyID).AsQueryable();

                }

                var startDate = new DateTime(startDateFilter.Year, startDateFilter.Month, startDateFilter.Day, 0, 0, 0);
                var endDate = new DateTime(endDateFilter.Year, endDateFilter.Month, endDateFilter.Day, 23, 59, 59);
                query = query.Where(f => f.q.EffectiveDate >= startDate && f.q.EffectiveDate <= endDate).AsQueryable();

                PolicyAndQuoteSummaryResult policyAndQuoteSummaryResult = new PolicyAndQuoteSummaryResult();
                policyAndQuoteSummaryResult.SummaryName = "Quote";
                policyAndQuoteSummaryResult.SummaryPolicyCount = query.Where(w => w.q.FkPolicyStatusId == newPollicyStatusId).Count();


                PolicyAndQuoteSummaryResult _policyAndQuoteSummaryResult = new PolicyAndQuoteSummaryResult();
                _policyAndQuoteSummaryResult.SummaryName = "Sold Policy";
                _policyAndQuoteSummaryResult.SummaryPolicyCount = query.Where(w => w.q.FkPolicyStatusId == activePolicyStatusId).Count();

                List<PolicyAndQuoteSummaryResult> listpolicyAndQuoteSummaryResults = new List<PolicyAndQuoteSummaryResult>();
                listpolicyAndQuoteSummaryResults.Add(policyAndQuoteSummaryResult);
                listpolicyAndQuoteSummaryResults.Add(_policyAndQuoteSummaryResult);

                return listpolicyAndQuoteSummaryResults;
            }



        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyID"></param>
        /// <param name="startDateFilter"></param>
        /// <param name="endDateFilter"></param>
        /// <returns></returns>
        public List<CloseRatioModel> GetCloseRatioCount(int agencyID, DateTime startDateFilter, DateTime endDateFilter)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    long activePolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());



                    var query = (from q in db.Quotes
                                 let st = db.States.FirstOrDefault(f => f.ID == q.FkStateId)
                                 let o = db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId)
                                 let a = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                 select new { q, a }).AsQueryable();


                    if (agencyID > 0)
                    {
                        query = query.Where(f => f.a.ID == agencyID).AsQueryable();

                    }


                    var startDate = new DateTime(startDateFilter.Year, startDateFilter.Month, startDateFilter.Day, 0, 0, 0);
                    var endDate = new DateTime(endDateFilter.Year, endDateFilter.Month, endDateFilter.Day, 23, 59, 59);
                    query = query.Where(f => f.q.EffectiveDate >= startDate && f.q.EffectiveDate <= endDate).AsQueryable();

                    int totalquotes = query.Select(w => w.q.ID).Count();
                    int completedquotes = query.Where(w => w.q.FkPolicyStatusId == activePolicyStatusId).Count();
                    int summaryQuotes = completedquotes * 100;
                    int chartQuotes = summaryQuotes / totalquotes;

                    CloseRatioModel closeRatioModel = new CloseRatioModel();
                    closeRatioModel.Percentage = chartQuotes;

                    List<CloseRatioModel> _closeRatioModels = new List<CloseRatioModel>();
                    _closeRatioModels.Add(closeRatioModel);

                    return _closeRatioModels;
                }
                catch(Exception ex)
                {
                    return null;
                }

            }
        }
    }
}
