﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Core.Resources;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Arrowhead.POS.Service
{
    public class AutoVehicleServices : BaseService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public List<AutoVehicleModel> GetAutoVehicleByQuoteID(long QuoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    return (from ad in db.Auto_Vehicle
                            let lienholder = (db.LienHolders.Where(w => w.ID == ad.FkLienHolderID).FirstOrDefault())
                            where ad.FkQuoteID == QuoteId && ad.IsDeleted == false
                            select new AutoVehicleModel()
                            {
                                AutoVehicleId = ad.ID,
                                QuoteId = ad.FkQuoteID,
                                LienHolderId = ad.FkLienHolderID,
                                Year = ad.Year,
                                Make = ad.Make,
                                Vin = ad.VIN,
                                Model = ad.Model,
                                VehicleOrderId = ad.VehicleOrderID,
                                LienHolderAddress = (string.IsNullOrEmpty(ad.LienHoldersAddress) ? lienholder.Address1 : ad.LienHoldersAddress),
                                Address1 = ad.Address1,
                                Address2 = ad.Address2,
                                ZipCode = ad.ZipCode,
                                City = ad.City,
                                StateAbbr = (ad.FkStateId != null ? db.States.Where(w => w.ID == ad.FkStateId).Select(s => s.Code).FirstOrDefault() : ""),
                                StateId = ad.FkStateId,
                                GaragingAddress1 = ad.GaragingAddress1,
                                GaragingAddress2 = ad.GaragingAddress2,
                                GaragingCity = ad.GaragingCity,
                                GaragingStateId = ad.GaragingFKStateCode,
                                GaragingZipCode = ad.GaragingZipCode,
                                EstimatedMileage = ad.EstimatedMileage,
                                PurchaseDate = ad.PurchaseDate,
                                IsMonitoryDevice = ad.IsMonitoryDevice
                            }).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicleID"></param>
        /// <returns></returns>
        public AutoVehicleModel GetAutoVehicleByVehicleID(long vehicleID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    return (from ad in db.Auto_Vehicle
                            let lienholder = (db.LienHolders.Where(w => w.ID == ad.FkLienHolderID).FirstOrDefault())
                            where ad.ID == vehicleID && ad.IsDeleted == false
                            select new AutoVehicleModel()
                            {
                                AutoVehicleId = ad.ID,
                                QuoteId = ad.FkQuoteID,
                                LienHolderId = ad.FkLienHolderID,
                                Year = ad.Year,
                                Make = ad.Make,
                                Vin = ad.VIN,
                                Model = ad.Model,
                                VehicleOrderId = ad.VehicleOrderID,
                                FkOwnerTypeID = ad.FkOwnerTypeID ?? 0,
                                LienHolderAddress = (string.IsNullOrEmpty(ad.LienHoldersAddress) ? lienholder.Address1 : ad.LienHoldersAddress),
                                Address1 = ad.Address1,
                                Address2 = ad.Address2,
                                ZipCode = ad.ZipCode,
                                City = ad.City,
                                StateAbbr = (ad.FkStateId != null ? db.States.Where(w => w.ID == ad.FkStateId).Select(s => s.Code).FirstOrDefault() : ""),
                                StateId = ad.FkStateId,
                                GaragingAddress1 = ad.GaragingAddress1,
                                GaragingAddress2 = ad.GaragingAddress2,
                                GaragingCity = ad.GaragingCity,
                                GaragingStateId = ad.GaragingFKStateCode,
                                GaragingZipCode = ad.GaragingZipCode,
                                EstimatedMileage = ad.EstimatedMileage,
                                PurchaseDate = ad.PurchaseDate,
                                IsMonitoryDevice = ad.IsMonitoryDevice
                            }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicleID"></param>
        public void DeleteAutoVehicleByID(int vehicleID)
        {
            bool isQuoteCoverageExist,isRateCoverageExist = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var quoteId = db.Auto_Vehicle.Where(x => x.ID == vehicleID).Select(x => x.FkQuoteID).FirstOrDefault();
                    isQuoteCoverageExist = db.Auto_QuoteCoverage.Where(x => x.FKVehicleID == vehicleID && x.FKQuoteID == quoteId).Any(); //delete relevant tables entry
                    isRateCoverageExist = db.Auto_RateHistoryDetail.Where(x=> x.VehicleID == vehicleID && x.FKQuoteID == quoteId).Any(); //delete relevant tables entry

                    if (isQuoteCoverageExist)
                    {
                        db.Auto_QuoteCoverage.Where(x => x.FKQuoteID == quoteId && x.FKVehicleID == vehicleID).Delete();
                    }
                    if(isRateCoverageExist)
                    {
                        db.Auto_RateHistoryDetail.Where(x => x.FKQuoteID == quoteId && x.VehicleID == vehicleID).Delete();
                    }

                    db.Auto_Vehicle.Where(f => f.ID == vehicleID).Delete();
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        public void AddLienHolder(AutoVehicleModel vehicle)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _createDate = ConvertTo.GetEstTimeNow();
                    int stateID = db.States.Where(f => f.Code == vehicle.LienHolderStateCD).FirstOrDefault().ID;
                    var groupTransactionId = Guid.NewGuid();
                    LienHolder lienHolder = new LienHolder();
                    lienHolder.CommercialName = vehicle.LienHolderCommercialName;
                    lienHolder.Address1 = StringExtension.FirstCharToUpper(vehicle.LienHolderAddress1);
                    lienHolder.Address2 = StringExtension.FirstCharToUpper(vehicle.LienHolderAddress2);
                    lienHolder.City = vehicle.LienHolderCity;
                    lienHolder.FkStateID = stateID;
                    lienHolder.ZipCode = vehicle.LienHolderZipCode;
                    lienHolder.CreatedDate = _createDate;
                    lienHolder.CreatedByID = vehicle.UserId;
                    db.LienHolders.Add(lienHolder);
                    db.SaveChanges();
                    vehicle.LienHolderId = lienHolder.ID;

                    InsertAuditHistory(Enums.AuditHistoryModule.LI, vehicle.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, lienHolder.ID.ToString(), vehicle.QuoteId, 0, null);

                }

            }
            catch (Exception ex)
            {
                InsertErrorLogHistory(vehicle.AgencyId, Enums.AuditHistoryModule.LI.ToString(), Enums.AuditHistoryOperation.I.ToString(), vehicle.QuoteId, ex.ToString());

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public AutoVehicleModel UpdateAutoVehicle(AutoVehicleModel vehicle)
        {
            try
            {

                using (var db = new ArrowheadPOSEntities())
                {
                    var _currentDate = ConvertTo.GetEstTimeNow();
                    int stateId = db.States.Where(w => w.Code == vehicle.LienHolderStateCD).Select(s => s.ID).FirstOrDefault();
                    var groupTransactionId = Guid.NewGuid();
                    db.Auto_Vehicle.Where(f => f.ID == vehicle.AutoVehicleId).Update(u => new Auto_Vehicle()
                    {
                        Year = vehicle.Year,
                        Make = vehicle.Make,
                        Model = vehicle.Model,
                        VIN = vehicle.Vin,
                        FkOwnerTypeID = vehicle.FkOwnerTypeID,
                        FkLienHolderID = vehicle.LienHolderId,
                        LienHolderName = vehicle.LienHolderCommercialName,
                        LienHoldersAddress = vehicle.LienHolderAddress1 + " " + vehicle.LienHolderAddress2,
                        ModifiedByID = vehicle.UserId,
                        ModifiedDate = _currentDate,
                        IsDeleted = false
                    });
                    InsertAuditHistory(Enums.AuditHistoryModule.AVE, vehicle.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, vehicle.AutoVehicleId.ToString(), vehicle.QuoteId, 0, null);


                    if (vehicle.LienHolderId != null)
                    {
                        db.LienHolders.Where(w => w.ID == vehicle.LienHolderId).Update(u => new LienHolder()
                        {
                            CommercialName = vehicle.LienHolderCommercialName,
                            Address1 = vehicle.LienHolderAddress1,
                            Address2 = vehicle.LienHolderAddress2,
                            City = vehicle.LienHolderCity,
                            FkStateID = stateId,
                            ZipCode = vehicle.LienHolderZipCode,
                            ModifyByID = (int)vehicle.UserId,
                            ModifiedDate = _currentDate

                        });

                        InsertAuditHistory(Enums.AuditHistoryModule.LI, vehicle.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, vehicle.AutoVehicleId.ToString(), vehicle.QuoteId, 0, null);

                    }
                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory(vehicle.AgencyId, Enums.AuditHistoryModule.AVE.ToString(), Enums.AuditHistoryOperation.S.ToString(), vehicle.QuoteId, ex.ToString());

            }

            return vehicle;
        }

        /// <summary>
        /// Get Is Driver Is Frist Driver 
        /// </summary>
        /// <param name="quoteID">QuoteID</param>
        /// <param name="driverID">driver ID</param>
        /// <returns></returns>
        public bool IsFirstVehicle(long quoteID, long? vehicleID = 0)
        {
            bool IsFirstVehicle = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    int vehicleCount = db.Auto_Vehicle.Where(x => x.FkQuoteID == quoteID && x.IsDeleted == false).Count();
                    if (vehicleCount == 0)
                    {
                        IsFirstVehicle = true;                        //return IsFirstDriver;
                    }
                    if (vehicleID > 0)
                    {
                        IsFirstVehicle = db.Auto_Driver.Where(x => x.ID == vehicleID && x.DriverOrderID == 1).Any();
                        // return IsFirstDriver = true;
                    }
                }
            }
            catch (Exception ex) { }

            return IsFirstVehicle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        public void AddAutoVehicle(AutoVehicleModel vehicle)
        {
            try
            {
                GeneralServices generalServices = new GeneralServices();

                using (var db = new ArrowheadPOSEntities())
                {
                    var _createDate = ConvertTo.GetEstTimeNow();
                    int stateId = db.States.Where(w => w.Code == vehicle.LienHolderStateCD).Select(s => s.ID).FirstOrDefault();
                    var groupTransactionId = Guid.NewGuid();
                    Auto_Vehicle autoVehicle = new Auto_Vehicle();
                    autoVehicle.FkQuoteID = vehicle.QuoteId;
                    autoVehicle.FkLienHolderID = vehicle.LienHolderId;
                    autoVehicle.VehicleOrderID = (db.Auto_Vehicle.Where(w => w.FkQuoteID == vehicle.QuoteId).Count() > 0) ? (db.Auto_Vehicle.Where(w => w.FkQuoteID == vehicle.QuoteId).Max(m => m.VehicleOrderID)) + 1 : 1;
                    autoVehicle.Year = vehicle.Year;
                    autoVehicle.Make = vehicle.Make;
                    autoVehicle.Model = vehicle.Model;
                    autoVehicle.VIN = vehicle.Vin;
                    autoVehicle.FkOwnerTypeID = vehicle.FkOwnerTypeID;
                    autoVehicle.FkLienHolderID = vehicle.LienHolderId;
                    autoVehicle.LienHolderName = vehicle.LienHolderCommercialName;
                    autoVehicle.LienHoldersAddress = vehicle.LienHolderAddress1 + " " + vehicle.LienHolderAddress2;
                    autoVehicle.CreatedDate = _createDate;
                    autoVehicle.CreatedByID = vehicle.UserId;
                    autoVehicle.IsDeleted = false;
                    db.Auto_Vehicle.Add(autoVehicle);
                    db.SaveChanges();
                    InsertAuditHistory(Enums.AuditHistoryModule.AVE, vehicle.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, vehicle.AutoVehicleId.ToString(), vehicle.QuoteId, 0, null);

                    if (vehicle.LienHolderId != null)
                    {

                        db.LienHolders.Where(w => w.ID == vehicle.LienHolderId).Update(U => new LienHolder()
                        {
                            CommercialName = vehicle.LienHolderCommercialName,
                            Address1 = vehicle.LienHolderAddress1,
                            Address2 = vehicle.LienHolderAddress2,
                            City = vehicle.LienHolderCity,
                            FkStateID = stateId,
                            ZipCode = vehicle.LienHolderZipCode,
                            ModifyByID = (int)vehicle.UserId,
                            ModifiedDate = _createDate

                        });

                        InsertAuditHistory(Enums.AuditHistoryModule.LI, vehicle.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, vehicle.AutoVehicleId.ToString(), vehicle.QuoteId, 0, null);

                    }
                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory(vehicle.AgencyId, Enums.AuditHistoryModule.AVE.ToString(), Enums.AuditHistoryOperation.S.ToString(), vehicle.QuoteId, ex.ToString());

                var err = ex.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lienHolderID"></param>
        /// <returns></returns>
        public AutoLienHolders GetLienHolderById(long lienHolderID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var lienHolder = (from l in db.LienHolders
                                  join s in db.States on l.FkStateID equals s.ID
                                  where l.ID == lienHolderID
                                  select new AutoLienHolders()
                                  {
                                      AutoLienHolderID = l.ID,
                                      Address1 = l.Address1,
                                      Address2 = l.Address2,
                                      City = l.City,
                                      CommercialName = l.CommercialName,
                                      StateID = l.FkStateID,
                                      ZipCode = l.ZipCode,
                                      StateCD = s.Code
                                  }).FirstOrDefault();
                return lienHolder;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public int GetvehicleCount(long QuoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Auto_Vehicle.Where(x => x.FkQuoteID == QuoteID && x.IsDeleted == false).Count();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="QuoteId"></param>
        /// <param name="_vehicleId"></param>
        /// <returns></returns>
        public bool IsVinNumberExists(string vin, long QuoteId, long _vehicleId)
        {
            int vehicleCount = 0;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _checkVin = db.Auto_Vehicle.Where(w => w.VIN == vin && w.FkQuoteID == QuoteId && w.IsDeleted == false).AsQueryable();
                    vehicleCount = _vehicleId == 0 ? _checkVin.Count() : _checkVin.Where(w => w.ID != _vehicleId).Count();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            if (vehicleCount > 0)
                return false;
            else
                return true;
        }


        /// <summary>
        /// Validate VIN Number and check asterisk
        /// and length must be > 17
        /// </summary>
        /// <param name="VIN"></param>
        /// <returns></returns>
        public VINDLValidate ValidateVin(long quoteID)
        {
            GeneralServices generalServices = new GeneralServices();
            VINDLValidate vINDLValidate = new VINDLValidate();
            long MaritalStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MARITALSTATUS.ToString(), Enums.MaritalStatus.MARRIED.ToString());
            long SpouseStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.SPOUSE.ToString());
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var vehicleList = (from av in db.Auto_Vehicle
                                       join q in db.Quotes on av.FkQuoteID equals q.ID

                                       where av.FkQuoteID == quoteID && av.IsDeleted == false
                                       select new VehicleInfoModel()
                                       {
                                           QuoteID = av.FkQuoteID,
                                           VehicleID = av.ID,
                                           VIN = av.VIN,
                                           Year = av.Year,
                                           Maker = av.Make,
                                           Model = av.Model,
                                           IsVINVerified = (av.VIN.Length < 17 || av.VIN.Contains("*")) ? false : true
                                       }).ToList();

                    var _driver = (from d in db.Auto_Driver
                                   join q in db.Quotes on d.FkQuoteID equals q.ID
                                   join s in db.States on d.FkStateID equals s.ID
                                   //let _driverList = (from au in db.Auto_Driver
                                   //                   where au.FkQuoteID == q.ID && au.DriverOrderID != 1 && au.FkMaritalStatusID == MaritalStatusID
                                   //                   select new { au }).ToList()
                                   //   db.Auto_Driver.Where(w => w.FkQuoteID == q.ID && w.DriverOrderID == 1 && w.FkMaritalStatusID == MaritalStatusID && w.DriverOrderID != 1).ToList())
                                   where d.FkQuoteID == quoteID && d.IsDeleted == false
                                   select new DriverInfoModel
                                   {
                                       QuoteID = d.FkQuoteID,
                                       DriverID = d.ID,
                                       DriverOrderID = d.DriverOrderID,
                                       MaritalStatusID = d.FkMaritalStatusID,
                                       RelationCDId = d.FkRelationCdID,
                                       StateID = d.FkStateID,
                                       StateCode = s.Code,
                                       DOB = d.BirthDate,
                                       isExcluded = d.IsExcluded,
                                       FirstName = d.FirstName,
                                       LastName = d.LastName,
                                       DriverLicense = d.LicenseNo,
                                       ViolationPoints = d.ViolationPoints,
                                       IsSpouseContain = (d.DriverOrderID == 1 && d.FkMaritalStatusID == MaritalStatusID) ? true : false //_driverList != null ? _driverList.Any(x => x.au.DriverOrderID != 1 && x.au.FkRelationCdID == SpouseStatusID) : false
                                   }).ToList();



                    var _quote = (from q in db.Quotes
                                  let c = db.Customers.Where(w => w.ID == q.FkCustomerID).FirstOrDefault()
                                  let ca = db.CustomerAddresses.Where(w => w.FKCustomerId == c.ID).FirstOrDefault()
                                  let driverCount = db.Auto_Driver.Where(x => x.FkQuoteID == q.ID && x.IsDeleted == false).Count()
                                  let vehCount = db.Auto_Vehicle.Where(x => x.FkQuoteID == q.ID && x.IsDeleted == false).Count()
                                  let isSpamContact = (c != null) ? db.SpamContacts.Where(x => x.PhoneNo == c.ContactNo).Any() : false
                                  where q.ID == quoteID && c.IsDeleted == false
                                  select new QuoteInfoModel
                                  {
                                      QuoteId = quoteID,
                                      IsGarageAddressExist = (string.IsNullOrEmpty(q.Address1) ? false : true),
                                      IsCityExist = (string.IsNullOrEmpty(q.City) ? false : true),
                                      IsStateExist = (q.FkStateId <= 0 ? false : true),
                                      IsZipCodeExist = ((string.IsNullOrEmpty(q.ZipCode) || (q.ZipCode == "0")) ? false : true),
                                      IsMailingAddressExist = (string.IsNullOrEmpty(ca.Address1) ? false : true),
                                      IsMailingCityExist = (string.IsNullOrEmpty(ca.City) ? false : true),
                                      IsMailingStateExist = (string.IsNullOrEmpty(ca.State) ? false : true),
                                      IsMailingZipCodeExist = (string.IsNullOrEmpty(ca.ZipCode) ? false : true),
                                      IsDriverExist = (driverCount >= 1 ? true : false),
                                      IsVehicleExist = (vehCount >= 1 ? true : false),
                                      IsSpamContact = isSpamContact,
                                      SpamContactNo = (isSpamContact && c != null) ? c.ContactNo : string.Empty
                                  }).FirstOrDefault();

                    var _IsFirstDriverMarried = _driver.Any(x => x.IsSpouseContain == true);
                    if (_IsFirstDriverMarried)
                    {
                        var _hasSpouse = _driver.Any(x => x.DriverOrderID != 1 && x.RelationCDId == SpouseStatusID);
                        if (_hasSpouse == true)
                            _driver.Where(w => w.DriverOrderID != 1).ToList().ForEach(x => x.IsSpouseContain = _hasSpouse);

                        //var _IsOtherDriverList = _driver.Where(x => x.DriverOrderID != 1).ToList();
                        //if(_driver.Count  == 1 && _hasSpouse == true &&  (_IsOtherDriverList == null || _IsOtherDriverList.Count == 0)) // Married Not Exist Spouse
                        //{
                        //    _driver.Where(w => w.DriverOrderID == 1).ToList().ForEach(x => x.IsSpouseContain = _hasSpouse); // update Current Driver Spouse
                        //}
                    }

                    vINDLValidate._vehicleInfo = vehicleList;
                    vINDLValidate._DriverInfo = _driver;
                    vINDLValidate._quoteInfo = _quote;
                    return vINDLValidate;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return null;
        }

        /// <summary>
        /// Reset Vehicle Order
        /// </summary>
        /// <param name="quoteId"></param>
        public void ResetVehicleByQuoteId(long quoteId)
        {
            int vehCount = 1;
            var _vehList = GetAutoVehicleByQuoteID(quoteId);
            using (var db = new ArrowheadPOSEntities())
            {
                foreach(var items in _vehList)
                {
                    db.Auto_Vehicle.Where(x => x.ID == items.AutoVehicleId).Update(u => new Auto_Vehicle()
                    {
                        VehicleOrderID = vehCount
                    });
                    vehCount++;
                }
            }
        }

        /// <summary>
        /// Get Default Vehicle Id 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public long GetDefaultVehicleID(long QuoteID)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {

                return db.Auto_Vehicle.Where(x => x.FkQuoteID == QuoteID && x.VehicleOrderID == 1).Select(x => x.ID).FirstOrDefault();
            }
        }
    }
}
