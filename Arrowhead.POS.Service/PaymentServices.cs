﻿using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stripe;
using Arrowhead.POS.Core;
using Arrowhead.POS.Model.CCPaymentRequestModel;

namespace Arrowhead.POS.Service
{
    public class PaymentServices
    {
        public PaymentProcessResponseModel CaptureStripeCharge(PaymentProcessRequestModel requestModel)
        {
            PaymentProcessResponseModel responseModel = new PaymentProcessResponseModel();
            GeneralServices generalServices = new GeneralServices();
            var stripeCredential = generalServices.GetStripeCredential();
            var charge = new StripeChargeCreateOptions
            {
                Amount = Convert.ToInt32(requestModel.TotalAmount * 100),
                Currency = "usd",
                Description = requestModel.QuoteID + "-" + "Arrowhead Payment",
                SourceTokenOrExistingSourceId = requestModel.Token,
                Metadata = new Dictionary<string, String>()
                            {
                                { "QuoteId", requestModel.QuoteID.ToString()},
                                { "ApplicantName",requestModel.CustomerName },
                                { "NSDFee",requestModel.NSDAmount.ToString()},
                                { "Amount",Convert.ToString(requestModel.TotalAmount) }
                            },
                Capture = true
            };

            // Test DEV Account
            //var agentAccount = new StripeRequestOptions();
            //agentAccount.StripeConnectAccountId = ConstantVariables.TestStripeAccountId;
            //StripeConfiguration.SetApiKey(ConstantVariables.DevTestAPIKey.ToString());
            //var chargeservice = new StripeChargeService(ConstantVariables.DevTestAPIKey.ToString());
            //StripeCharge stripeCharge = chargeservice.Create(charge, agentAccount);
            // E N D

            //var agentAccount = new StripeRequestOptions();
            //agentAccount.StripeConnectAccountId = ConstantVariables.DevStripeAccountId;

            var chargeService = new StripeChargeService(stripeCredential.APIKey);

            StripeCharge stripeCharge = chargeService.Create(charge);

            if (!string.IsNullOrWhiteSpace(stripeCharge.FailureCode))
            {
                responseModel.StatusCode = stripeCharge.FailureCode;
                responseModel.StatusMessage = stripeCharge.FailureMessage;
                responseModel.Status = false;
            }
            else
            {
                responseModel.StatusCode = stripeCharge.Status;
                responseModel.StatusMessage = "success";
                responseModel.Status = true;
                responseModel.TransactionID = stripeCharge.Id;
                responseModel.CustomerId = stripeCharge.CustomerId;
            }
            return responseModel;
        }

        public PaymentGateWayModel getPaymentGateWayCredentialDetail()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                var credential = (from x in db.PaymentGatewayProviderForms
                                  join pc in db.PaymentGatewayProviderCredentials on x.ID equals pc.FKPaymentGatewayProviderFormID
                                  join pm in db.PaymentGatewayProviderModes on pc.FKPaymentGatewayProviderModeID equals pm.ID
                                  join pp in db.PaymentGatewayProviders on x.FKPaymentGatewayProviderID equals pp.Id
                                  where pm.IsDefault == true && pp.IsActive == true
                                  select new { pc, x }).ToList();

                PaymentGateWayModel paymentGateWayModel = new PaymentGateWayModel();
                if (credential == null)
                {
                    return paymentGateWayModel;
                }
                foreach (var item in credential)
                {
                    if (!string.IsNullOrEmpty(paymentGateWayModel.EndPointURL)
                        && !string.IsNullOrEmpty(paymentGateWayModel.UserName)
                        && !string.IsNullOrEmpty(paymentGateWayModel.Password))
                    {
                        break;
                    }
                    switch (item.x.Code)
                    {
                        case "ENDPOINT":
                            paymentGateWayModel.EndPointURL = item.pc.ValueText;
                            break;
                        case "USERNAME":
                            paymentGateWayModel.UserName = item.pc.ValueText;
                            break;
                        case "PASSWORD":
                            paymentGateWayModel.Password = item.pc.ValueText;
                            break;
                        case "MERCHANTID":
                        case "PRODUCERID":
                            paymentGateWayModel.MerchantID = item.pc.ValueText;
                            break;
                        default:
                            break;
                    }
                }
                return paymentGateWayModel;
            }
        }

        public PaymentProcessResponseModel CaptureStripeViaACH(PaymentProcessRequestModel requestModel)
        {
            PaymentProcessResponseModel responseModel = new PaymentProcessResponseModel();
            GeneralServices generalServices = new GeneralServices();
            var stripeCredential = generalServices.GetStripeCredential();

            var stripeServices = new BankAccountService(stripeCredential.APIKey);

            BankAccountCreateOptions _bankAccount = new BankAccountCreateOptions();
            _bankAccount.SourceBankAccount = new SourceBankAccount()
            {
                AccountHolderName = requestModel.CustomerName,
                AccountHolderType = "individual",
                AccountNumber = requestModel.BankAccountNumber,
                BankName = requestModel.BankName,
                Country = "US",
                Currency = "usd",
                RoutingNumber = requestModel.BankRoutingNumber
            };

            var myToken = new StripeTokenCreateOptions()
            {
                BankAccount = new BankAccountOptions()
                {
                    AccountHolderName = requestModel.CustomerName,
                    AccountHolderType = "individual",
                    AccountNumber = requestModel.BankAccountNumber,
                    Country = "US",
                    Currency = "usd",
                    RoutingNumber = requestModel.BankRoutingNumber
                }
            };
            StripeConfiguration.SetApiKey(stripeCredential.APIKey);
            var tokenService = new StripeTokenService();
            StripeToken stripeToken = tokenService.Create(myToken);

            // Setup the customer
            StripeCustomerCreateOptions options = new StripeCustomerCreateOptions
            {
                Email = requestModel.CustomerEmail,
                Description = "",
                SourceToken = stripeToken.Id
            };


            // Create the customer
            StripeCustomer customer = new StripeCustomerService().Create(options);
            try
            {
                BankAccountVerifyOptions verifyOptions = new BankAccountVerifyOptions
                {
                    AmountOne = 32,
                    AmountTwo = 45
                };
                var bankAccount = stripeServices.Verify(customer.Id, customer.Sources.Select(f => f.BankAccount.Id).FirstOrDefault(), verifyOptions);
                if (bankAccount.Status.Contains("Sucs") || bankAccount.Status.Contains("veri"))
                {

                    var charges = new StripeChargeService();
                    var charge = charges.Create(new StripeChargeCreateOptions
                    {
                        Amount = (int)(requestModel.TotalAmount * 100),
                        Currency = "usd",
                        CustomerId = customer.Id,
                        Description = "Charge from Bank"
                    });
                    if (charge != null && (charge.Status.Contains("suc") || charge.Status.Contains("pending")))
                    {
                        responseModel.Status = true;
                        responseModel.TransactionID = charge.Id;
                        responseModel.StatusMessage = Convert.ToString(charge.StripeResponse.RequestId);
                        responseModel.StatusCode = Convert.ToString(charge.Status);
                        responseModel.CustomerId = charge.CustomerId;
                    }
                    else
                    {
                        responseModel.StatusCode = charge.FailureCode;
                        responseModel.Status = false;
                        responseModel.StatusMessage = Convert.ToString(charge.FailureMessage);
                    }
                }
                else
                {
                    responseModel.StatusCode = "400";
                    responseModel.Status = false;
                    responseModel.StatusMessage = "Bank Account verification failed";
                }

            }
            catch (StripeException ex)
            {
                responseModel.StatusCode = "404";
                responseModel.Status = false;
                responseModel.StatusMessage = ex.Message;
            }

            return responseModel;
        }

        public PaymentProcessResponseModel RefundStripePayment(string chargeId, string methodType)
        {
            PaymentProcessResponseModel responseModel = new PaymentProcessResponseModel();
            GeneralServices generalServices = new GeneralServices();
            var stripeCredential = generalServices.GetStripeCredential();
            StripeConfiguration.SetApiKey(stripeCredential.APIKey);
            var agentAccount = new StripeRequestOptions();
            //agentAccount.StripeConnectAccountId = ConstantVariables.DevStripeAccountId;
            var service = new StripeRefundService();
            StripeRefund refund = service.Create(chargeId, null);

            //var stripeCredential = generalServices.GetStripeCredential();

            // TEST ACCOUNT 
            //StripeConfiguration.SetApiKey(ConstantVariables.DevTestAPIKey);
            //var agentAccount = new StripeRequestOptions();
            //agentAccount.StripeConnectAccountId = ConstantVariables.TestStripeAccountId;
            //var service = new StripeRefundService();
            //StripeRefund refund = service.Create(chargeId, null, agentAccount);
            // E N D

            if (refund.Status.ToLower() == "succeeded" && methodType == Enums.PaymentMethod.CREDITCARD.ToString())
            {
                responseModel.StatusCode = refund.Status;
                responseModel.StatusMessage = "success";
                responseModel.Status = true;

            }
            else if (refund.Status.ToLower() == "pending" && methodType == Enums.PaymentMethod.ECHECK.ToString())
            {
                responseModel.StatusCode = refund.Status;
                responseModel.StatusMessage = "success";
                responseModel.Status = true;

            }
            else
            {
                responseModel.StatusCode = refund.FailureReason;
                responseModel.StatusMessage = refund.FailureReason;
                responseModel.Status = false;
            }
            return responseModel;
        }

        public PaymentProcessResponseModel CreateStripeCustomer(PaymentProcessRequestModel requestModel)
        {
            PaymentProcessResponseModel responseModel = new PaymentProcessResponseModel();
            GeneralServices generalServices = new GeneralServices();
            var agentAccount = new StripeRequestOptions();
            // ### DEV Credentials ### 
            //agentAccount.StripeConnectAccountId = ConstantVariables.DevStripeAccountId;
            //StripeConfiguration.SetApiKey(ConstantVariables.DevTestAPIKey.ToString());
            //var stripeServices = new BankAccountService(ConstantVariables.DevTestAPIKey.ToString());
            //StripeConfiguration.SetApiKey(ConstantVariables.DevTestAPIKey.ToString());

            var stripeCredential = generalServices.GetStripeCredential();
            var stripeServices = new BankAccountService(stripeCredential.APIKey);
            BankAccountCreateOptions _bankAccount = new BankAccountCreateOptions();
            _bankAccount.SourceBankAccount = new SourceBankAccount()
            {
                AccountHolderName = requestModel.CustomerName,
                AccountHolderType = "individual",
                AccountNumber = requestModel.BankAccountNumber,
                BankName = requestModel.BankName,
                Country = "US",
                Currency = "usd",
                RoutingNumber = requestModel.BankRoutingNumber
            };

            var myToken = new StripeTokenCreateOptions()
            {
                BankAccount = new BankAccountOptions()
                {
                    AccountHolderName = requestModel.CustomerName,
                    AccountHolderType = "individual",
                    AccountNumber = requestModel.BankAccountNumber,
                    Country = "US",
                    Currency = "usd",
                    RoutingNumber = requestModel.BankRoutingNumber
                }
            };
            StripeConfiguration.SetApiKey(stripeCredential.APIKey);
            var tokenService = new StripeTokenService();
            StripeToken stripeToken = tokenService.Create(myToken);

            // Setup the customer
            StripeCustomerCreateOptions options = new StripeCustomerCreateOptions
            {
                Email = requestModel.CustomerEmail,
                Description = requestModel.Description,
                SourceToken = stripeToken.Id
            };

            // Create the customer
            StripeCustomer customer = new StripeCustomerService().Create(options);
            try
            {
                //BankAccountVerifyOptions verifyOptions = new BankAccountVerifyOptions
                //{
                //    AmountOne = 32,
                //    AmountTwo = 45
                //};
                //var bankAccount = stripeServices.Verify(customer.Id, customer.Sources.Select(f => f.BankAccount.Id).FirstOrDefault(), verifyOptions);
                if (customer != null)
                {
                    responseModel.StatusCode = "200";
                    responseModel.Status = true;
                    responseModel.StatusMessage = "Bank Account Verified Successfully";
                    responseModel.CustomerId = customer.Id;
                    responseModel.BankAccountId = customer.Sources.Select(f => f.BankAccount.Id).FirstOrDefault();
                }
                else
                {
                    responseModel.StatusCode = "400";
                    responseModel.Status = false;
                    responseModel.StatusMessage = "Bank Account Verification Failed";
                    responseModel.CustomerId = "0";
                    responseModel.BankAccountId = "0";
                }
                return responseModel;
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = "400";
                responseModel.Status = false;
                responseModel.StatusMessage = ex.Message.ToString();
                responseModel.CustomerId = "0";
                responseModel.BankAccountId = "0";
                return responseModel;
                //throw ex;
            }
        }

        /// <summary>
        /// Transfer Funds to Bank Account Via CC 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public PaymentProcessResponseModel CCPayoutStripe(decimal _amount, string _bankAccountId)
        {
            PaymentProcessResponseModel _responseModel = new PaymentProcessResponseModel();
            GeneralServices generalServices = new GeneralServices();

            try
            {
                var stripeCredential = generalServices.GetStripeCredential();
                var agentAccount = new StripeRequestOptions();
                StripeConfiguration.SetApiKey(stripeCredential.APIKey);
                var options = new StripePayoutCreateOptions
                {
                    Amount = (int)(_amount * 100),
                    Currency = "usd",
                    Method = "standard",
                    Destination = _bankAccountId
                };
                var service = new StripePayoutService();
                var payout = service.Create(options);
                if (payout != null && (payout.Status.Contains("suc") || payout.Status.Contains("pending") || payout.Status.Contains("transit")))
                {
                    _responseModel.Status = true;
                    _responseModel.TransactionID = payout.Id;
                    _responseModel.StatusMessage = Convert.ToString(payout.StripeResponse.RequestId);
                    _responseModel.StatusCode = Convert.ToString(payout.Status);
                }
                else
                {
                    _responseModel.StatusCode = payout.FailureCode;
                    _responseModel.Status = false;
                    _responseModel.StatusMessage = Convert.ToString(payout.FailureMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _responseModel;
        }

        public static bool CheckBalanceInStripAccount()
        {
            bool isEnoughBalance = false;
            //Check Balance for strip account
            return isEnoughBalance;
        }
    }
}
