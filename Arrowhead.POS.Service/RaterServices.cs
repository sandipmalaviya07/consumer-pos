﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Process;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Arrowhead.POS.Service
{
    public class RaterServices
    {
        public AutoRequestModel PrepareAutoRequestModel(long quoteId)
        {
            DateTime _currentDate = ConvertTo.GetEstTimeNow();
            AutoRequestModel autoRequestModel = new AutoRequestModel();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            AutoCoverageServices _autoCoverageServices = new AutoCoverageServices();
            NSDServices _nsdServices = new NSDServices();
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            GeneralServices generalServices = new GeneralServices();
            CommonServices commonServices = new CommonServices();

            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    // Delete NSD If Exists 
                    bool _IsNSDPlanExist = db.QuoteNSDPlans.Where(x => x.FKQuoteId == quoteId && x.IsSuccess != true).Any(); // NSD is Not Success
                    if (_IsNSDPlanExist)
                    {
                        _nsdServices.DeleteNsd(quoteId);
                    }

                    var effectiveDate = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.EffectiveDate).FirstOrDefault();
                    if (effectiveDate.Value.Date < _currentDate.Date)
                    {
                        autoQuoteServices.UpdateEffectiveAndExpireDateByQuoteId(quoteId);
                    }

                    var _quote = (from q in db.Quotes
                                  let c = (db.Customers.Where(w => w.ID == q.FkCustomerID).FirstOrDefault())
                                  let ca = (db.CustomerAddresses.Where(w => w.FKCustomerId == q.FkCustomerID).FirstOrDefault())
                                  let stateCode = (db.States.Where(w => w.ID == q.FkStateId).Select(s => s.Code).FirstOrDefault())
                                  where q.ID == quoteId
                                  select new
                                  {
                                      q,
                                      c,
                                      stateCode,
                                      ca

                                  }).FirstOrDefault();

                    var generalStatusList = db.GeneralStatusTypes.ToList();

                    AutoPolicy autoPolicy = new AutoPolicy
                    {
                        BirthDate = _quote.c.DOB,
                        City = _quote.q.City,
                        StateCd = _quote.stateCode,
                        ContactNo = _quote.c.ContactNo,
                        County = string.Empty,
                        EffectiveDate = Convert.ToDateTime(_quote.q.EffectiveDate), // _quote.q.EffectiveDate
                        ExpirationDate = Convert.ToDateTime(_quote.q.ExpirationDate),
                        EmailAddress = _quote.c.EmailID,
                        FirstName = _quote.c.FirstName,
                        LastName = _quote.c.LastName,
                        MailingCity = _quote.ca.City,
                        MailingCounty = string.Empty,
                        MailingStateCd = _quote.stateCode,
                        MailingStreet_Address1 = _quote.ca.Address1,
                        MailingStreet_Address2 = _quote.ca.Address2,
                        MailingZipCode = _quote.ca.ZipCode,
                        MiddleName = _quote.c.MiddleName,
                        OtherContactNo = _quote.c.ContactNo,
                        CreatedDate = _currentDate, // CommonFun.FormateDate(_currentDate);
                        FkHomeTypeId = _quote.q.FkHomeTypeId
                    };
                    autoPolicy.StateCd = _quote.stateCode;
                    autoPolicy.Street_Address1 = _quote.q.Address1;
                    autoPolicy.Street_Address2 = _quote.q.Address2;
                    autoPolicy.ZipCode = _quote.q.ZipCode;
                    if (autoPolicy.FkHomeTypeId != null && autoPolicy.FkHomeTypeId > 0)
                    {
                        autoPolicy.FkHomeTypeCode = commonServices.GetGeneralStatusCodeById(autoPolicy.FkHomeTypeId.Value);
                    }

                    //Add Prior Insurance details


                    var _priorInsurance = db.Auto_PriorInsuranceDetails.Where(w => w.FKQuoteId == quoteId).FirstOrDefault();

                    if (_priorInsurance != null)
                    {
                        AutoPriorInsurer autoPriorInsurer = new AutoPriorInsurer()
                        {
                            Currently_Insured = true,
                            Current_CarrierName = _priorInsurance.InsuranceCompany.Name,
                            ExpirationDate = Convert.ToDateTime(_priorInsurance.PriorExpirationDate),
                            PolicyNumber = _priorInsurance.PolicyNumber,
                            //BICoverage = db.GeneralStatusTypes.Where(w => w.ID == long.Parse(_priorInsurance.BIPriorCoverage)).Select(s => s.Name).FirstOrDefault(),
                            InsurerName = _priorInsurance.InsuranceCompany.Name
                        };
                        autoPolicy.PriorInsurer = autoPriorInsurer;
                    }


                    autoRequestModel.AutoPolicy = autoPolicy;
                    var _driverList = db.Auto_Driver.Where(w => w.FkQuoteID == quoteId && w.IsDeleted == false).OrderBy(o => o.DriverOrderID).ToList();
                    autoRequestModel.AutoDrivers = new List<AutoDriver>();

                    foreach (var _driver in _driverList)
                    {
                        string _driverStateCode = (db.States.Where(w => w.ID == _driver.FkStateID).Select(s => s.Code).FirstOrDefault());
                        AutoDriver _auto = new AutoDriver()
                        {
                            Age_First_Licensed = (_driver.YearsLicensed != null ? (_currentDate.Year - _driver.YearsLicensed ?? 10) : 10),
                            ContactNo = _quote.c.ContactNo,
                            Education_Completed = true,
                            FirstName = _driver.FirstName,
                            Gender = _driver.Gender,
                            HomeOwner = (_driver.DriverOrderID == 1 ? true : false),
                            Id = _driver.DriverOrderID ?? 1,
                            IsExcluded = _driver.IsExcluded,
                            IsGoodCredit = (_driver.FkCreditScoreID != null ? (generalStatusList.Where(w => w.ID == _driver.FkCreditScoreID && (w.Code == "GOOD" || w.Code == "EXCELLENT")).Any()) : false),
                            IsGoodStudent = true,
                            IsSR22 = _driver.IsSR22,
                            IsSR22A = _driver.IsSR22A,
                            Is_Same_Address_As_Primary = true,
                            LastName = _driver.LastName,
                            License_Number = _driver.LicenseNo,
                            License_StateCd = _driverStateCode,
                            Marital_Status = (generalStatusList.Where(w => w.ID == _driver.FkMaritalStatusID).Select(s => s.Code).FirstOrDefault()),
                            MiddleName = string.Empty,
                            PrimaryVehicleId = 1,
                            SR22CaseNumber = string.Empty,
                            SR22Reason = string.Empty,
                            SR22State = _driverStateCode,
                            SSN = string.Empty,
                            RelationCd = (generalStatusList.Where(w => w.ID == _driver.FkRelationCdID).Select(s => s.Code).FirstOrDefault()),
                            BirthDate = _driver.BirthDate,
                            Occupation = (generalStatusList.Where(w => w.ID == _driver.FkDriverOccupationID).Select(s => s.Code).FirstOrDefault()),
                            IsWorkLossBenefit = _driver.IsWorkLossBenefit,
                            FkMilitaryTypeId = _driver.FkMilitaryTypeId == null ? null : generalServices.GetCodeFromGeneralStatusType(_driver.FkMilitaryTypeId.Value) != "NONE" ? _driver.FkMilitaryTypeId : null,
                            FkWarehouseTypeId = _driver.FkWarehouseTypeId == null ? null : generalServices.GetCodeFromGeneralStatusType(_driver.FkWarehouseTypeId.Value) != "NONE" ? _driver.FkWarehouseTypeId : null,
                            IsDefensiveDriverCourse = _driver.IsDefensiveDriverCourse
                        };
                        _auto.AutoDriverViolations = new List<AutoDriverViolation>();
                        var _violList = (from adv in db.Auto_DriverViolation
                                         join av in db.Auto_Violation on adv.FKAutoViolationId equals av.ID
                                         where adv.FkDriverId == _driver.ID
                                         select new
                                         {
                                             adv,
                                             av
                                         });

                        foreach (var _dvio in _violList)
                        {
                            AutoDriverViolation autoDriverViolation = new AutoDriverViolation()
                            {
                                DriverID = Convert.ToInt32(_driver.DriverOrderID),
                                ViolationCd = _dvio.av.Code,
                                Description = _dvio.adv.Note,
                                VioDate = _dvio.adv.ViolationDate,
                                ViolationName = _dvio.av.Name,
                                ViolationPoints = (_driverStateCode.Contains("IT") || _driverStateCode.Contains("FR") ? 0 : _dvio.adv.Points)
                            };
                            _auto.AutoDriverViolations.Add(autoDriverViolation);
                        }

                        autoRequestModel.AutoDrivers.Add(_auto);
                    }

                    var coverageList = (from aqc in db.Auto_QuoteCoverage
                                        join c in db.Coverages on aqc.FkCoverageID equals c.ID
                                        where aqc.FKQuoteID == quoteId
                                        select new StateCoverageValues()
                                        {
                                            Code = c.Code,
                                            Value1 = aqc.Value1 ?? 0,
                                            Value2 = aqc.Value2 ?? 0,
                                            VehicleID = aqc.FKVehicleID ?? 0
                                        }).ToList();

                    var _vehicleList = (db.Auto_Vehicle.Where(w => w.FkQuoteID == quoteId && w.IsDeleted == false)).ToList();
                    autoRequestModel.AutoVehicles = new List<AutoVehicle>();

                    foreach (var _vehicle in _vehicleList)
                    {
                        if ((coverageList.Count > 0))
                        {
                            var vehicleCoverage = coverageList.Where(f => f.VehicleID == _vehicle.ID).ToList();

                            try
                            {
                                if (vehicleCoverage != null)
                                {
                                    foreach (StateCoverageValues item in vehicleCoverage)
                                    {
                                        if ((item.Code == "COMP"))
                                            _vehicle.Comp = item.Value1;
                                        else if ((item.Code == "COLL"))
                                            _vehicle.Coll = item.Value1;
                                        else if ((item.Code == "TL"))
                                            _vehicle.Towing = item.Value1;

                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        var _autoLine = db.LienHolders.Where(w => w.ID == _vehicle.FkLienHolderID).FirstOrDefault();
                        var _vehicleCoverage = coverageList.Where(f => f.VehicleID == _vehicle.ID).ToList();

                        AutoVehicle autoVehicle = new AutoVehicle()
                        {
                            AnnualMileage = (_vehicle.EstimatedMileage == null ? 100 : System.Convert.ToInt32(_vehicle.EstimatedMileage)),
                            Id = (_vehicle.VehicleOrderID == null ? 1 : System.Convert.ToInt32(_vehicle.VehicleOrderID)),
                            IsOwned = (_autoLine != null ? false : true),
                            Make = _vehicle.Make,
                            Model = _vehicle.Model,
                            RentalLimit = "0",
                            TowingLimit = Convert.ToString(_vehicle.Towing),
                            VIN = _vehicle.VIN,
                            Year = _vehicle.Year,
                            PurchaseDate = (_vehicle.PurchaseDate != null ? Convert.ToDateTime(_vehicle.PurchaseDate) : Convert.ToDateTime("01/01/" + _vehicle.Year + "")),
                            CollisionDed = Convert.ToString(_vehicle.Coll),
                            ComprehensiveDed = Convert.ToString(_vehicle.Comp),
                            IsMonitoryDevice = _vehicle.IsMonitoryDevice
                        };
                        var louCoverage = _vehicleCoverage.FirstOrDefault(f => f.Code == Enums.Coverages.LUSE.ToString());
                        if (louCoverage != null)
                        {
                            autoVehicle.LoseOfUse = Convert.ToString(louCoverage.Value1);
                        }
                        var colLevel = _vehicleCoverage.FirstOrDefault(f => f.Code == Enums.Coverages.COLLLEVEL.ToString());
                        if (colLevel != null)
                        {
                            string _value = string.Empty;
                            var CollLevelCode = (from sc in db.StateCoverages
                                                 join c in db.Coverages on sc.FkCoverageID equals c.ID
                                                 where sc.Value1 == colLevel.Value1 && sc.Value2 == colLevel.Value2 &&
                                                 sc.FkStateID == _quote.q.FkStateId && c.Code == Enums.Coverages.COLLLEVEL.ToString()
                                                 select sc.DisplayTextValue1.ToUpper()).FirstOrDefault();

                            if (!string.IsNullOrEmpty(CollLevelCode) && Convert.ToDecimal(colLevel.Value1) > 0)
                            {
                                switch (CollLevelCode)
                                {
                                    case "BROAD":
                                        _value = Enums.ColLevelValues.B.ToString();
                                        break;
                                    case "STANDARD":
                                        _value = Enums.ColLevelValues.S.ToString();
                                        break;
                                    default:
                                        _value = Enums.ColLevelValues.L.ToString();
                                        break;
                                }
                            }
                            else
                            {
                                switch (colLevel.Value1)
                                {
                                    case 1:
                                        _value = Enums.ColLevelValues.B.ToString();
                                        break;
                                    case 2:
                                        _value = Enums.ColLevelValues.S.ToString();
                                        break;
                                    default:
                                        _value = Enums.ColLevelValues.L.ToString();
                                        break;
                                }
                            }
                            autoVehicle.CollissionLevel = Convert.ToString(_value);
                        }
                        // .PrimaryUse = Convert.ToString(generalStatusList.Where(Function(w) w.ID = _vehicle.FkCarUsedforID).[Select](Function(s) s.Name).FirstOrDefault()),
                        AutoLienHolder autoLienHolder = new AutoLienHolder();

                        if (_autoLine != null)
                        {
                            autoLienHolder.City = _autoLine.City;
                            autoLienHolder.Name = _autoLine.CommercialName;
                            autoLienHolder.StateCd = (db.States.Where(w => w.ID == _autoLine.FkStateID).Select(s => s.Code).FirstOrDefault());
                            autoLienHolder.Street_Address1 = _autoLine.Address1;
                            autoLienHolder.Street_Address2 = _autoLine.Address2;
                            autoLienHolder.ZipCode = _autoLine.ZipCode;
                            autoVehicle.AutoLienHolder = autoLienHolder;
                        }

                        autoRequestModel.AutoVehicles.Add(autoVehicle);
                    }



                    AutoCoverage autoCoverage = new AutoCoverage();
                    var biCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.BI.ToString());

                    if (biCoverage != null)
                    {
                        autoCoverage.BodilyInjuryPerPerson = Convert.ToString(biCoverage.Value1);
                        autoCoverage.BodilyInjuryPerAcc = Convert.ToString(biCoverage.Value2);
                    }

                    var lpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.LPD.ToString());
                    if (lpdCoverage != null)
                        autoCoverage.LimitedPropertyDamageLiability = Convert.ToString(lpdCoverage.Value1);

                    var pdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PD.ToString());

                    if (pdCoverage != null)
                        autoCoverage.PropertyDamage = Convert.ToString(pdCoverage.Value1);

                    var umbiCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UMBI.ToString());

                    if (umbiCoverage != null)
                    {
                        autoCoverage.UninsuredBodilyInjuryPerPerson = Convert.ToString(umbiCoverage.Value1);
                        autoCoverage.UninsuredBodilyInjuryPerAcc = Convert.ToString(umbiCoverage.Value2);
                    }

                    var umpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UMPD.ToString());

                    if (umpdCoverage != null)
                        autoCoverage.UninsuredPropertyDamage = Convert.ToString(umpdCoverage.Value1);

                    var medCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.MEDP.ToString());

                    if (medCoverage != null)
                    {
                        autoCoverage.MedicalPayments = Convert.ToString(Math.Round(medCoverage.Value1).ToString("0.00"));
                        long coverageId = db.Coverages.Where(w => w.Code == Enums.Coverages.MEDP.ToString()).Select(s => s.ID).FirstOrDefault();
                        autoCoverage.MedDisplayText = db.StateCoverages.Where(w => w.Value1 == medCoverage.Value1 && w.FkCoverageID == coverageId && w.FkStateID == _quote.q.FkStateId).Select(s => s.DisplayTextValue1).FirstOrDefault();
                    }
                  
                    var pipCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PIP.ToString());

                    if (pipCoverage != null)
                        autoCoverage.PersonalInjuryProtection = Convert.ToString(pipCoverage.Value1);
                    else
                        autoCoverage.PersonalInjuryProtection = "0.00";

                    var ppiCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PPI.ToString());

                    if (ppiCoverage != null)
                        autoCoverage.PropertyProtectionInsurance = Convert.ToString(ppiCoverage.Value1);

                    var pipdedutibleCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PIPDEDOPT.ToString());

                    if (pipdedutibleCoverage != null)
                        autoCoverage.PersonalInjuryProtectionDed = Convert.ToString(pipdedutibleCoverage.Value1);

                    var undmbiCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UIMBI.ToString());

                    if (undmbiCoverage != null)
                    {
                        autoCoverage.UnderinsuredBodilyInjuryPerPerson = Convert.ToString(undmbiCoverage.Value1);
                        autoCoverage.UnderinsuredBodilyInjuryPerAcc = Convert.ToString(undmbiCoverage.Value2);
                    }

                    var undpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UIMPD.ToString());

                    if (undpdCoverage != null)
                        autoCoverage.UnderinsuredPropertyDamage = Convert.ToString(undpdCoverage.Value1);

                    var tortCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.TORT.ToString());

                    if (tortCoverage != null)
                        autoCoverage.Tort = Convert.ToString(tortCoverage.Value1);

                    var addPIPCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.ADDPIP.ToString());

                    if (addPIPCoverage != null)
                        autoCoverage.AdditionalersonalInjuryProtection = Convert.ToString(addPIPCoverage.Value1);

                    var umpddCoverage = coverageList.FirstOrDefault(f => f.Code.Trim() == Enums.Coverages.UMPDD.ToString());

                    if (umpddCoverage != null)
                        autoCoverage.UninsuredPropertyDamageDeductible = Convert.ToString(umpddCoverage.Value1);

                    var umCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UM.ToString());

                    if (umCoverage != null)
                    {
                        autoCoverage.UninsuredMotorist = Convert.ToString(umCoverage.Value1);
                    }

                    var limitedpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.LPD.ToString());

                    if (limitedpdCoverage != null)
                        autoCoverage.LimitedPropertyDamageLiability = Convert.ToString(limitedpdCoverage.Value1);



                    var punitiveCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PDE.ToString());

                    if (punitiveCoverage != null)
                        autoCoverage.PunitiveDamageExclusion = Convert.ToString(punitiveCoverage.Value1);



                    autoRequestModel.Coverage = autoCoverage;


                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }
            autoRequestModel.Api_Version = 1;
            autoRequestModel.Api_Key = "velox";
            autoRequestModel.QuoteID = quoteId;
            return autoRequestModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public AutoCoverageDocusign GetCoverageForDocusign(long quoteId)
        {
            AutoCoverageDocusign autoCoverage = new AutoCoverageDocusign();
            DateTime _currentDate = ConvertTo.GetEstTimeNow();
            AutoRequestModel autoRequestModel = new AutoRequestModel();
            AutoCoverageServices _autoCoverageServices = new AutoCoverageServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var coverageList = (from aqc in db.Auto_QuoteCoverage
                                        join c in db.Coverages on aqc.FkCoverageID equals c.ID
                                        where aqc.FKQuoteID == quoteId
                                        select new StateCoverageValues()
                                        {
                                            Code = c.Code,
                                            Value1 = aqc.Value1 ?? 0,
                                            Value2 = aqc.Value2 ?? 0,
                                            VehicleID = aqc.FKVehicleID ?? 0
                                        }).ToList();

                    var _vehicleList = (db.Auto_Vehicle.Where(w => w.FkQuoteID == quoteId && w.IsDeleted == false)).ToList();
                    autoRequestModel.AutoVehicles = new List<AutoVehicle>();
                    var _rateHistoryDetails = db.Auto_RateHistoryDetail.Where(w => w.FKQuoteID == quoteId).ToList();
                    int counter = 1;
                    foreach (var _vehicle in _vehicleList)
                    {
                        if (coverageList.Count > 0)
                        {
                            try
                            {
                                var vehicleCoverage = _rateHistoryDetails.Where(f => f.VehicleID == _vehicle.ID).ToList();
                                if (vehicleCoverage != null)
                                {
                                    switch (counter)
                                    {
                                        case 1:
                                            foreach (var item in vehicleCoverage)
                                            {
                                                if ((item.Name == Enums.Coverages.COMP.ToString()))
                                                {
                                                    autoCoverage.Veh1Comp = Convert.ToString(item.Amount);
                                                    var compval = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COMP.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    if (compval != null)
                                                        autoCoverage.Veh1ComprehensiveDed = Convert.ToString(compval);
                                                }
                                                else if ((item.Name == Enums.Coverages.COLL.ToString()))
                                                {
                                                    autoCoverage.Veh1Coll = Convert.ToString(item.Amount);
                                                    var collVal = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COLL.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    var collType = _autoCoverageServices.GetCollType(item.Description);
                                                    if (collVal != null)
                                                    {
                                                        autoCoverage.Veh1CollisionDed = Convert.ToString(collVal);
                                                        autoCoverage.Veh1CollisionDedType = collType;
                                                    }
                                                }
                                                else if ((item.Name == Enums.Coverages.TL.ToString()))
                                                    autoCoverage.Veh1TowingLimit = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LUSE.ToString() && item.Amount > 0)
                                                    autoCoverage.Veh1LOS = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.BI.ToString())
                                                    autoCoverage.Veh1BI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PD.ToString())
                                                    autoCoverage.Veh1PD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PIP.ToString())
                                                    autoCoverage.Veh1PIP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PPI.ToString())
                                                    autoCoverage.Veh1PPI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMBI.ToString())
                                                    autoCoverage.Veh1UMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MCCA.ToString())
                                                    autoCoverage.Veh1MCCA = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.SAF.ToString())
                                                    autoCoverage.Veh1SAF = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LPD.ToString())
                                                    autoCoverage.Veh1LPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MEDP.ToString())
                                                    autoCoverage.Veh1MEDP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMBI.ToString())
                                                    autoCoverage.Veh1UIMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMPD.ToString())
                                                    autoCoverage.Veh1UIMPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMPD.ToString())
                                                    autoCoverage.Veh1UMPD = Convert.ToString(item.Amount);

                                            }
                                            break;

                                        case 2:
                                            foreach (var item in vehicleCoverage)
                                            {
                                                if ((item.Name == Enums.Coverages.COMP.ToString()))
                                                {
                                                    autoCoverage.Veh2Comp = Convert.ToString(item.Amount);
                                                    var compval = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COMP.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    if (compval != null)
                                                        autoCoverage.Veh2ComprehensiveDed = Convert.ToString(compval);
                                                }
                                                else if ((item.Name == Enums.Coverages.COLL.ToString()))
                                                {
                                                    autoCoverage.Veh2Coll = Convert.ToString(item.Amount);
                                                    var collVal = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COLL.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    var collType = _autoCoverageServices.GetCollType(item.Description);
                                                    if (collVal != null)
                                                    {
                                                        autoCoverage.Veh2CollisionDed = Convert.ToString(collVal);
                                                        autoCoverage.Veh2CollisionDedType = collType;
                                                    }
                                                }
                                                else if ((item.Name == Enums.Coverages.TL.ToString()))
                                                    autoCoverage.Veh2TowingLimit = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LUSE.ToString() && item.Amount > 0)
                                                    autoCoverage.Veh2LOS = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.BI.ToString())
                                                    autoCoverage.Veh2BI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PD.ToString())
                                                    autoCoverage.Veh2PD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PIP.ToString())
                                                    autoCoverage.Veh2PIP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PPI.ToString())
                                                    autoCoverage.Veh2PPI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMBI.ToString())
                                                    autoCoverage.Veh2UMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MCCA.ToString())
                                                    autoCoverage.Veh2MCCA = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.SAF.ToString())
                                                    autoCoverage.Veh2SAF = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LPD.ToString())
                                                    autoCoverage.Veh2LPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MEDP.ToString())
                                                    autoCoverage.Veh2MEDP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMBI.ToString())
                                                    autoCoverage.Veh2UIMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMPD.ToString())
                                                    autoCoverage.Veh2UIMPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMPD.ToString())
                                                    autoCoverage.Veh2UMPD = Convert.ToString(item.Amount);
                                            }
                                            break;

                                        case 3:
                                            foreach (var item in vehicleCoverage)
                                            {
                                                if ((item.Name == Enums.Coverages.COMP.ToString()))
                                                {
                                                    autoCoverage.Veh3Comp = Convert.ToString(item.Amount);
                                                    var compval = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COMP.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    if (compval != null)
                                                        autoCoverage.Veh3ComprehensiveDed = Convert.ToString(compval);
                                                }
                                                else if ((item.Name == Enums.Coverages.COLL.ToString()))
                                                {
                                                    autoCoverage.Veh3Coll = Convert.ToString(item.Amount);
                                                    var collVal = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COLL.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    var collType = _autoCoverageServices.GetCollType(item.Description);
                                                    if (collVal != null)
                                                    {
                                                        autoCoverage.Veh3CollisionDed = Convert.ToString(collVal);
                                                        autoCoverage.Veh3CollisionDedType = collType;
                                                    }

                                                }
                                                else if ((item.Name == Enums.Coverages.TL.ToString()))
                                                    autoCoverage.Veh3TowingLimit = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LUSE.ToString() && item.Amount > 0)
                                                    autoCoverage.Veh3LOS = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.BI.ToString())
                                                    autoCoverage.Veh3BI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PD.ToString())
                                                    autoCoverage.Veh3PD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PIP.ToString())
                                                    autoCoverage.Veh3PIP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PPI.ToString())
                                                    autoCoverage.Veh3PPI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMBI.ToString())
                                                    autoCoverage.Veh3UMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MCCA.ToString())
                                                    autoCoverage.Veh3MCCA = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.SAF.ToString())
                                                    autoCoverage.Veh3SAF = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LPD.ToString())
                                                    autoCoverage.Veh3LPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MEDP.ToString())
                                                    autoCoverage.Veh3MEDP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMBI.ToString())
                                                    autoCoverage.Veh3UIMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMPD.ToString())
                                                    autoCoverage.Veh3UIMPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMPD.ToString())
                                                    autoCoverage.Veh3UMPD = Convert.ToString(item.Amount);
                                            }
                                            break;
                                        case 4:
                                            foreach (var item in vehicleCoverage)
                                            {
                                                if ((item.Name == Enums.Coverages.COMP.ToString()))
                                                {
                                                    autoCoverage.Veh4Comp = Convert.ToString(item.Amount);
                                                    var compval = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COMP.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    if (compval != null)
                                                        autoCoverage.Veh4ComprehensiveDed = Convert.ToString(compval);
                                                }
                                                else if ((item.Name == Enums.Coverages.COLL.ToString()))
                                                {
                                                    autoCoverage.Veh4Coll = Convert.ToString(item.Amount);
                                                    var collVal = coverageList.Where(w => w.VehicleID == item.VehicleID && w.Code == Enums.Coverages.COLL.ToString()).Select(s => s.Value1).FirstOrDefault();
                                                    var collType = _autoCoverageServices.GetCollType(item.Description);
                                                    if (collVal != null)
                                                    {
                                                        autoCoverage.Veh4CollisionDed = Convert.ToString(collVal);
                                                        autoCoverage.Veh4CollisionDedType = collType;
                                                    }
                                                }
                                                else if ((item.Name == Enums.Coverages.TL.ToString()))
                                                    autoCoverage.Veh4TowingLimit = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LUSE.ToString() && item.Amount > 0)
                                                    autoCoverage.Veh4LOS = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.BI.ToString())
                                                    autoCoverage.Veh4BI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PD.ToString())
                                                    autoCoverage.Veh4PD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PIP.ToString())
                                                    autoCoverage.Veh4PIP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.PPI.ToString())
                                                    autoCoverage.Veh4PPI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMBI.ToString())
                                                    autoCoverage.Veh4UMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MCCA.ToString())
                                                    autoCoverage.Veh4MCCA = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.SAF.ToString())
                                                    autoCoverage.Veh4SAF = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.LPD.ToString())
                                                    autoCoverage.Veh4LPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.MEDP.ToString())
                                                    autoCoverage.Veh4MEDP = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMBI.ToString())
                                                    autoCoverage.Veh4UIMBI = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UIMPD.ToString())
                                                    autoCoverage.Veh4UIMPD = Convert.ToString(item.Amount);
                                                else if (item.Name == Enums.Coverages.UMPD.ToString())
                                                    autoCoverage.Veh4UMPD = Convert.ToString(item.Amount);
                                            }
                                            break;
                                    }

                                }
                                counter = counter + 1;
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        var _autoLine = db.LienHolders.Where(w => w.ID == _vehicle.FkLienHolderID).FirstOrDefault();
                        AutoVehicle autoVehicle = new AutoVehicle()
                        {
                            AnnualMileage = (_vehicle.EstimatedMileage == null ? 100 : System.Convert.ToInt32(_vehicle.EstimatedMileage)),
                            Id = (_vehicle.VehicleOrderID == null ? 1 : System.Convert.ToInt32(_vehicle.VehicleOrderID)),
                            IsOwned = (_autoLine != null ? false : true),
                            Make = _vehicle.Make,
                            Model = _vehicle.Model,
                            RentalLimit = "0",
                            TowingLimit = Convert.ToString(_vehicle.Towing),
                            VIN = _vehicle.VIN,
                            Year = _vehicle.Year,
                            PurchaseDate = (_vehicle.PurchaseDate != null ? Convert.ToDateTime(_vehicle.PurchaseDate) : Convert.ToDateTime("01/01/" + _vehicle.Year + "")),
                            CollisionDed = Convert.ToString(_vehicle.Coll),
                            ComprehensiveDed = Convert.ToString(_vehicle.Comp),
                            IsMonitoryDevice = _vehicle.IsMonitoryDevice
                        };
                        var _vehicleCoverage = coverageList.Where(f => f.VehicleID == _vehicle.ID).ToList();
                        // .PrimaryUse = Convert.ToString(generalStatusList.Where(Function(w) w.ID = _vehicle.FkCarUsedforID).[Select](Function(s) s.Name).FirstOrDefault()),
                        AutoLienHolder autoLienHolder = new AutoLienHolder();

                        if (_autoLine != null)
                        {
                            autoLienHolder.City = _autoLine.City;
                            autoLienHolder.Name = _autoLine.CommercialName;
                            autoLienHolder.StateCd = (db.States.Where(w => w.ID == _autoLine.FkStateID).Select(s => s.Code).FirstOrDefault());
                            autoLienHolder.Street_Address1 = _autoLine.Address1;
                            autoLienHolder.Street_Address2 = _autoLine.Address2;
                            autoLienHolder.ZipCode = _autoLine.ZipCode;
                            autoVehicle.AutoLienHolder = autoLienHolder;
                        }

                        autoRequestModel.AutoVehicles.Add(autoVehicle);
                    }

                    if (_rateHistoryDetails.Where(x => x.Name == Enums.Coverages.MVRFee.ToString()).FirstOrDefault() != null)
                        if (_rateHistoryDetails.Where(x => x.Name == Enums.Coverages.MVRFee.ToString()).FirstOrDefault().Amount > 0)
                            autoCoverage.MVRFee = _rateHistoryDetails.Where(x => x.Name == Enums.Coverages.MVRFee.ToString()).FirstOrDefault().Amount.ToString();

                    if (_rateHistoryDetails.Where(x => x.Name == Enums.Coverages.UMFee.ToString()).FirstOrDefault() != null)
                        if (_rateHistoryDetails.Where(x => x.Name == Enums.Coverages.UMFee.ToString()).FirstOrDefault().Amount > 0)
                            autoCoverage.UMFee = _rateHistoryDetails.Where(x => x.Name == Enums.Coverages.UMFee.ToString()).FirstOrDefault().Amount.ToString();

                    if (_rateHistoryDetails.Where(x => x.Name == Enums.Coverages.POLFE.ToString()).FirstOrDefault() != null)
                        if (_rateHistoryDetails.Where(x => x.Name == Enums.Coverages.POLFE.ToString()).FirstOrDefault().Amount > 0)
                            autoCoverage.PolicyFee = _rateHistoryDetails.Where(x => x.Name == Enums.Coverages.POLFE.ToString()).FirstOrDefault().Amount.ToString();

                    var biCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.BI.ToString());

                    if (biCoverage != null)
                    {
                        autoCoverage.BodilyInjuryPerPerson = Convert.ToString(biCoverage.Value1);
                        autoCoverage.BodilyInjuryPerAcc = Convert.ToString(biCoverage.Value2);
                    }

                    var pdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PD.ToString());

                    if (pdCoverage != null)
                        autoCoverage.PropertyDamage = Convert.ToString(pdCoverage.Value1);

                    var umbiCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UMBI.ToString());

                    if (umbiCoverage != null)
                    {
                        autoCoverage.UninsuredBodilyInjuryPerPerson = Convert.ToString(umbiCoverage.Value1);
                        autoCoverage.UninsuredBodilyInjuryPerAcc = Convert.ToString(umbiCoverage.Value2);
                    }

                    var umpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UMPD.ToString());

                    if (umpdCoverage != null)
                        autoCoverage.UninsuredPropertyDamage = Convert.ToString(umpdCoverage.Value1);

                    var lossofuseCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.LUSE.ToString());
                    if (lossofuseCoverage != null)
                    {
                        autoCoverage.LossOfUse = Convert.ToString(lossofuseCoverage.Value1);
                    }


                    var medCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.MEDP.ToString());

                    if (medCoverage != null)
                        autoCoverage.MedicalPayments = Convert.ToString(medCoverage.Value1);

                    var pipCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PIP.ToString());

                    if (pipCoverage != null)
                    {
                        autoCoverage.PersonalInjuryProtection = Convert.ToString(pipCoverage.Value1);
                        if (pipCoverage.Value1 == 300)
                        {
                            autoCoverage.PersonalInjuryProtectionDetails = "Primary PIP - 300 Deductible";
                        }
                        else if (pipCoverage.Value1 == 500)
                        {
                            autoCoverage.PersonalInjuryProtectionDetails = "Coordinated PIP(Excess) $500 deductible";
                        }

                    }
                    if (pipCoverage == null)
                    {
                        autoCoverage.PersonalInjuryProtectionDetails = "Primary PIP No deductible";
                    }


                    var ppiCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PPI.ToString());

                    if (ppiCoverage != null)
                        autoCoverage.PropertyProtectionInsurance = Convert.ToString(ppiCoverage.Value1);

                    var pipdedutibleCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PIPDEDUCTIBLE.ToString());

                    if (pipdedutibleCoverage != null)
                        autoCoverage.PersonalInjuryProtectionDed = Convert.ToString(pipdedutibleCoverage.Value1);

                    var undmbiCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UIMBI.ToString());

                    if (undmbiCoverage != null)
                    {
                        autoCoverage.UnderinsuredBodilyInjuryPerPerson = Convert.ToString(undmbiCoverage.Value1);
                        autoCoverage.UnderinsuredBodilyInjuryPerAcc = Convert.ToString(undmbiCoverage.Value2);
                    }

                    var undpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UIMPD.ToString());

                    if (undpdCoverage != null)
                        autoCoverage.UnderinsuredPropertyDamage = Convert.ToString(undpdCoverage.Value1);

                    var tortCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.TORT.ToString());

                    if (tortCoverage != null)
                        autoCoverage.Tort = Convert.ToString(tortCoverage.Value1);

                    var addPIPCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.ADDPIP.ToString());

                    if (addPIPCoverage != null)
                        autoCoverage.AdditionalersonalInjuryProtection = Convert.ToString(addPIPCoverage.Value1);

                    var umpddCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UMPDD.ToString());

                    if (umpddCoverage != null)
                        autoCoverage.UninsuredPropertyDamageDeductible = Convert.ToString(umpddCoverage.Value1);

                    var umCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.UM.ToString());

                    if (umCoverage != null)
                        autoCoverage.UninsuredMotorist = Convert.ToString(umCoverage.Value1);

                    var punitiveCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.PDE.ToString());

                    if (punitiveCoverage != null)
                        autoCoverage.PunitiveDamageExclusion = Convert.ToString(punitiveCoverage.Value1);

                    var lpdCoverage = coverageList.FirstOrDefault(f => f.Code == Enums.Coverages.LPD.ToString());
                    if (lpdCoverage != null)
                        autoCoverage.LimitedPropertyDamage = Convert.ToString(lpdCoverage.Value1);

                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }

            return autoCoverage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="autoRateCompanyResponses"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public List<QuoteRateResponseModel> InsertRateHistoryData(List<AutoRateCompanyResponse> autoRateCompanyResponses, long quoteId)
        {
            List<QuoteRateResponseModel> _quoteDetailsResponseall = new List<QuoteRateResponseModel>();
            DateTime _currentDate = ConvertTo.GetEstTimeNow();
            List<QuoteRateResponseModel> _returnQuoteResponeModel = new List<QuoteRateResponseModel>();
            Guid _rateTransactionId = Guid.NewGuid();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _quote = db.Quotes.Where(f => f.ID == quoteId).FirstOrDefault();
                    var stateId = _quote.FkStateId;

                    var CompanyMapping = db.Auto_CompanyMapping.Where(x => x.FKStateID == stateId).ToList();
                    decimal _nsdAmount = 0; // If(db.QuoteNsdplans.Where(Function(w) w.FkquoteId = quoteId).[Select](Function(s) s.Price).Sum(), 0)

                    int _PolicyTerm = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.PolicyTerm).FirstOrDefault();


                    var _StateCoverageList = (from aqc in db.Auto_QuoteCoverage
                                              join c in db.Coverages on aqc.FkCoverageID equals c.ID
                                              let sc = (from sc in db.StateCoverages
                                                        where c.ID == sc.FkCoverageID
                                                        select sc).FirstOrDefault()
                                              where aqc.FKQuoteID == quoteId
                                              select new StateCoverageValues()
                                              {
                                                  Code = (sc == null ? string.Empty : c.Name),
                                                  StateId = (sc == null ? 0 : sc.FkStateID),
                                                  StateCoverageId = sc.ID,
                                                  CoverageId = aqc.FkCoverageID,
                                                  OrderId = sc == null ? 0 : sc.OrderID,
                                                  Value1 = aqc.Value1 ?? 0,
                                                  Value2 = aqc.Value2 ?? 0,
                                                  DisplayTextValue1 = sc.DisplayTextValue1,
                                                  DisplayTextValue2 = sc.DisplayTextValue2,
                                                  IsSelected = (sc == null ? false : sc.IsSelected)
                                              }).ToList();

                    foreach (var item in autoRateCompanyResponses)
                    {

                        var ArrowheadCompanyDetails = db.InsuranceCompanies.Where(f => f.Code == "ARROWHEAD").FirstOrDefault();

                        try
                        {
                            if (item.Errors.Count == 0)
                            {
                                try
                                {
                                    var _autoRateHistoryIds = (from ar in db.Auto_RateHistory
                                                               join arh in db.Auto_RateHistoryDetail on ar.FKQuoteID equals arh.FKQuoteID
                                                               where ar.FKQuoteID == quoteId && ar.RateTransactionID != _rateTransactionId
                                                               select arh).Delete();
                                    db.Auto_QuoteDetail.Where(w => w.FKQuoteID == quoteId).Update(up => new Auto_QuoteDetail()
                                    {
                                        FKRateHistoryID = null
                                    });
                                    db.Auto_RateHistory.Where(x => x.RateTransactionID != _rateTransactionId && x.FKQuoteID == quoteId).Delete();
                                }
                                catch (Exception ex)
                                {
                                }

                                long newRateHistoryID = 0;

                                foreach (var _listResult in item.AutoRateResults)
                                {
                                    if (_listResult.DownPayment > 0)
                                    {
                                        Auto_RateHistory rateHistory = new Auto_RateHistory()
                                        {
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            DownPayment = Convert.ToDecimal(_listResult.DownPayment),
                                            MonthllyPayment = Convert.ToDecimal(_listResult.MontlyPayment),
                                            Premium = _listResult.PayPlanDescription.Contains("PIF (0 Payments, 100% Down)") ? Convert.ToDecimal(_listResult.DownPayment) : Convert.ToDecimal(_listResult.TotalPremium),
                                            BridgeURL = item.BridgeUrl,
                                            CarrierQuoteID = item.CompanyQuoteId,
                                            NSDFee = _nsdAmount,
                                            CreatedDate = _currentDate,
                                            NumberOfPayment = _listResult.NumOfPayments,
                                            RateTransactionID = _rateTransactionId,
                                            Description = _listResult.PayPlanDescription,
                                            InstallmenetFee = Convert.ToDecimal(_listResult.InstallmentFee),
                                            InstallmentDueDate = _listResult.InstallmentDueDt == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : _listResult.InstallmentDueDt,
                                            PayPlanID = _listResult.PayPlanId
                                        };
                                        db.Auto_RateHistory.Add(rateHistory);
                                        db.SaveChanges();



                                        if (newRateHistoryID == 0)
                                            newRateHistoryID = rateHistory.ID;

                                        QuoteRateResponseModel _alldetails = new QuoteRateResponseModel()
                                        {
                                            CompanyId = ArrowheadCompanyDetails.ID,
                                            CompanyName = ArrowheadCompanyDetails.Name,
                                            DownPayment = Convert.ToDecimal(_listResult.DownPayment),
                                            ErrorResponse = string.Empty,
                                            MonthlyPayment = Convert.ToDecimal(_listResult.MontlyPayment),
                                            NSDFees = _nsdAmount,
                                            RateId = rateHistory.ID,
                                            RateTransactionID = Convert.ToString(_rateTransactionId),
                                            IsError = false,
                                            BridgeURL = item.BridgeUrl,
                                            Description = _listResult.PayPlanDescription,
                                            PayableDownPayment = Convert.ToDecimal(_listResult.DownPayment) + _nsdAmount,
                                            TotalPremium = _listResult.PayPlanDescription.Contains("PIF (0 Payments, 100% Down)") ? Convert.ToDecimal(_listResult.DownPayment) : Convert.ToDecimal(_listResult.TotalPremium),
                                            NoOfInstallement = _listResult.NumOfPayments,
                                            PolicyTerm = _PolicyTerm,
                                            StateCoverage = _StateCoverageList,
                                            InstallmentDueDt = _listResult.InstallmentDueDt == DateTime.MinValue ? ConvertTo.GetEstTimeNow() : _listResult.InstallmentDueDt,
                                            DueDiffrence = _listResult.InstallmentDueDt == DateTime.MinValue ? (ConvertTo.GetEstTimeNow().Date - Convert.ToDateTime(_quote.EffectiveDate).Date).Days : (_listResult.InstallmentDueDt.Date - Convert.ToDateTime(_quote.EffectiveDate).Date).Days
                                        };
                                        _quoteDetailsResponseall.Add(_alldetails);
                                    }
                                }

                                db.Auto_QuoteDetail.Where(w => w.FKQuoteID == quoteId).Update(U => new Auto_QuoteDetail()
                                {
                                    RateDate = _currentDate,
                                    IsChange = false,
                                    RateTransactionID = _rateTransactionId
                                });

                                try
                                {
                                    if (item.SR22Fee != 0)
                                    {
                                        Auto_RateHistoryDetail _rateHistoryDetail = new Auto_RateHistoryDetail()
                                        {
                                            FkCoverageID = db.Coverages.Where(x => x.Code == Enums.Coverages.SR22Fee.ToString()).Select(s => s.ID).FirstOrDefault(),
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            Amount = Convert.ToDecimal(item.SR22Fee),
                                            Name = "SR22Fee",
                                            VehicleID = 0,
                                            RateTransactionID = _rateTransactionId
                                        };
                                        db.Auto_RateHistoryDetail.Add(_rateHistoryDetail);
                                        db.SaveChanges();
                                    }
                                    if (item.MVRFee != 0)
                                    {
                                        db.Auto_RateHistoryDetail.Add(new Auto_RateHistoryDetail
                                        {
                                            FkCoverageID = db.Coverages.Where(x => x.Code == Enums.Coverages.MVRFee.ToString()).Select(x => x.ID).FirstOrDefault(),
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            Amount = (decimal)item.MVRFee,
                                            Name = Enums.Coverages.MVRFee.ToString(),
                                            VehicleID = 0,
                                            RateTransactionID = _rateTransactionId
                                        });
                                        db.SaveChanges();
                                    }
                                    if (item.PolicyFee != 0)
                                    {
                                        db.Auto_RateHistoryDetail.Add(new Auto_RateHistoryDetail
                                        {
                                            FkCoverageID = db.Coverages.Where(x => x.Code == Enums.Coverages.POLFE.ToString()).Select(x => x.ID).FirstOrDefault(),
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            Amount = (decimal)item.PolicyFee,
                                            Name = Enums.Coverages.POLFE.ToString(),
                                            VehicleID = 0,
                                            RateTransactionID = _rateTransactionId
                                        });
                                        db.SaveChanges();
                                    }
                                    if (item.UMFundFee != 0)
                                    {
                                        db.Auto_RateHistoryDetail.Add(new Auto_RateHistoryDetail
                                        {
                                            FkCoverageID = db.Coverages.Where(x => x.Code == Enums.Coverages.UMFee.ToString()).Select(x => x.ID).FirstOrDefault(),
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            Amount = (decimal)item.UMFundFee,
                                            Name = Enums.Coverages.UMFee.ToString(),
                                            VehicleID = 0,
                                            RateTransactionID = _rateTransactionId
                                        });
                                        db.SaveChanges();
                                    }

                                    foreach (var res in item.Vehicles)
                                    {
                                        foreach (var cov in res.Coverages)
                                        {
                                            int coverageID = db.Coverages.Where(x => x.Code.Contains(cov.CoverageName)).Select(s => s.ID).FirstOrDefault();
                                            long vehicleID = db.Auto_Vehicle.Where(x => x.VIN.Equals(res.VIN) && x.FkQuoteID == quoteId && x.IsDeleted == false).Select(s => s.ID).FirstOrDefault();
                                            try
                                            {
                                                if (coverageID > 0)
                                                {
                                                    Auto_RateHistoryDetail autoRateHistoryDetail = new Auto_RateHistoryDetail()
                                                    {
                                                        FkCoverageID = coverageID,
                                                        FKQuoteID = quoteId,
                                                        FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                                        Amount = Convert.ToDecimal(cov.Premium),
                                                        Description = cov.option != null ? cov.option.OptionTypeCd : string.Empty,
                                                        Name = cov.CoverageName,
                                                        VehicleID = vehicleID,
                                                        RateTransactionID = _rateTransactionId
                                                    };
                                                    db.Auto_RateHistoryDetail.Add(autoRateHistoryDetail);
                                                    db.SaveChanges();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                db.Auto_Driver.Where(w => w.FkQuoteID == quoteId).Update(u => new Auto_Driver { ViolationPoints = 0 });
                                if (item.DriverViolationDetails != null && item.DriverViolationDetails.Count() > 0)
                                {
                                    foreach (var _vio in item.DriverViolationDetails)
                                    {
                                        db.Auto_Driver.Where(w => w.DriverOrderID == _vio.DriverOrderId
                                        && w.FkQuoteID == quoteId && w.IsDeleted == false).Update(up => new Auto_Driver
                                        {
                                            ViolationPoints = _vio.ViolationPoints
                                        });

                                        // State is Foreign / International Update Auto_DriverViolation Point 
                                        var _drv = db.Auto_Driver.Where(w => w.DriverOrderID == _vio.DriverOrderId
                                        && w.FkQuoteID == quoteId && w.IsDeleted == false && (w.FkStateID == 61 || w.FkStateID == 62)).FirstOrDefault();
                                        if (_drv != null)
                                        {
                                            db.Auto_DriverViolation.Where(w => w.FkDriverId == _drv.ID && w.Points > 0).Update(up => new Auto_DriverViolation
                                            {
                                                Points = _vio.ViolationPoints
                                            });
                                        }

                                    }
                                }
                            }
                            else
                            {
                                var _error = "";
                                try
                                {
                                    System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                                    xml.LoadXml(item.Errors.Select(s => s.Message).FirstOrDefault());
                                    _error = xml.SelectSingleNode("/ACORD/InsuranceSvcRs/PersAutoPolicyQuoteInqRs/MsgStatus/MsgStatusDesc").InnerXml;
                                }
                                catch (Exception)
                                {
                                    _error = item.Errors.Select(s => s.Message).FirstOrDefault();
                                }

                                QuoteRateResponseModel _allerrordetails = new QuoteRateResponseModel()
                                {
                                    CompanyId = Convert.ToInt32(item.CompanyId),
                                    CompanyName = item.CompanyName,
                                    DownPayment = 0,
                                    ErrorResponse = _error.ToString(),
                                    MonthlyPayment = 0,
                                    NSDFees = 0,
                                    RateId = 0,
                                    IsError = true,
                                    BridgeURL = item.BridgeUrl,
                                    Description = "",
                                    PayableDownPayment = 0,
                                    TotalPremium = 0,
                                    RateTransactionID = Convert.ToString(_rateTransactionId),
                                    NoOfInstallement = 0,
                                    PolicyTerm = _PolicyTerm,
                                    StateCoverage = _StateCoverageList
                                };
                                _quoteDetailsResponseall.Add(_allerrordetails);
                            }
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }

                if (_quoteDetailsResponseall.Count > 0)
                {
                    _returnQuoteResponeModel = _quoteDetailsResponseall.OrderBy(o => o.DownPayment).ThenBy(o => o.MonthlyPayment).ToList();

                    foreach (var childRateHistory in _returnQuoteResponeModel)
                        childRateHistory._quoteDetailsResponseAllDetails = _quoteDetailsResponseall.Where(w => w.CompanyId == childRateHistory.CompanyId && w.RateId != childRateHistory.RateId).Select(s => new QuoteRateResponseDetailsModel()
                        {
                            RateId = s.RateId,
                            QuoteID = quoteId,
                            DownPayment = s.DownPayment,
                            MonthlyPayment = s.MonthlyPayment,
                            NSDFees = s.NSDFees,
                            IsError = s.IsError,
                            RateTransactionID = Convert.ToString(_rateTransactionId),
                            ErrorResponse = s.ErrorResponse,
                            BridgeURL = s.BridgeURL,
                            Description = s.Description,
                            PayableDownPayment = s.PayableDownPayment,
                            TotalPremium = s.TotalPremium
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return _returnQuoteResponeModel;
        }



        //public ArrowheadCredential GetArrowheadCredential(string StateCode)
        //{
        //    ArrowheadCredential credential = new ArrowheadCredential();
        //    credential.ArrowheadSPName = "velox.com";
        //    credential.ClientApp_Org = "RateForce";
        //    if ((StateCode == "MI"))
        //    {
        //        credential.ClientApp_Name = "RateForce.Insurance.RateForceRR.MI";
        //        credential.ClientApp_Version = "1.0";
        //        credential.ContractNumber = "162498";
        //        credential.ProducerSubCode = "162498";
        //    }
        //    else
        //    {
        //        credential.ClientApp_Name = "RateForce.Insurance.RateForceRR.GA";
        //        credential.ClientApp_Version = "1.0.0.0";
        //        credential.ContractNumber = "2387";
        //        credential.ProducerSubCode = "2387";
        //    }

        //    credential.CommercialName = "RateForce";
        //    credential.ProducerSurname = "Gallegos";
        //    credential.ProducerName = "Carlos";
        //    credential.CompanyID = "5";
        //    credential.APIPassword = "VLCN916!K*Z%WW ";
        //    // credential.EndpointAddress = "http://www.arrowheadauto.com/ws/RTR/RTRService.svc"
        //    credential.EndpointAddress = "http://www.arrowheadauto.com/dev/ws/RTR/RTRService.svc";
        //    credential.ProducerAddr1 = "3423 Piedmont Road NE";
        //    credential.ProducerAddr2 = "VLCN916!K*Z%WW ";
        //    credential.ProducerCity = "Atlanta";
        //    credential.ProducerStateCD = StateCode;
        //    credential.ProducerPhoneNumber = "770-674-8951";
        //    credential.ProducerPostalCode = "30326";

        //    return credential;
        //}

        internal static int GetIdByServicesProvider(string Code)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.RaterServicesProviders.Where(w => w.Code == Code.ToUpper()).Select(s => s.ID).FirstOrDefault();
            }
        }

        public ArrowheadPolicyBindModel PreparePolicyBindModel(long quoteId, bool isSignInOffice)
        {
            ArrowheadPolicyBindModel bindPolicyModel = new ArrowheadPolicyBindModel();
            QuestionnaireServices questionnaireServices = new QuestionnaireServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            DocusignServices docusignServices = new DocusignServices();
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            CommonServices _commonServices = new CommonServices();

            string envelopeID = string.Empty;
            try
            {
                //if (isSignInOffice == false)
                //{
                envelopeID = docusignServices.GetEnvelopeIDByQuoteId(quoteId);
                //}

                var paymentDetail = autoPolicyServices.GetPaymentDetailByQuoteId(quoteId);
                var authCode = autoPolicyServices.GetAuthCodeByQuoteId(quoteId);
                string customerName = autoQuoteServices.GetQuoteDetailForTab(quoteId).FullName;
                bindPolicyModel.QuoteID = quoteId;
                bindPolicyModel.EnvelopID = envelopeID;
                bindPolicyModel.CustomerName = customerName;
                bindPolicyModel.ConfirmationCode = (paymentDetail.PaymentMethodCd == Enums.PaymentMethod.SWEEP.ToString() ? "" : authCode); // Pass Confirmation Code from Authorize.NET for CC/E-Check
                bindPolicyModel.PaymentMethodCd = _commonServices.GetPaymentMethod(paymentDetail.PaymentMethodCd);
                bindPolicyModel.PayPlanId = autoPolicyServices.GetPayPlanByQuoteId(quoteId);
                bindPolicyModel.questionModels = questionnaireServices.GetQuestionnaireById(quoteId).Select(x => new ArrowheadQuestionModel
                {
                    Question = x.Questionnaire.Question,
                    Answer = x.QuestionnaireAnswer.Answer,
                    Code = x.Questionnaire.Code,
                    Description = x.QuestionnaireAnswer.Description,
                    OrderNo = x.Questionnaire.OrderNo
                }).ToList();

                bindPolicyModel.CreatedDate = ConvertTo.GetEstTimeNow();

                var rateModel = autoQuoteServices.GetRateHistoryDetailsByQuoteID(quoteId);
                ArrowheadRateModel arrowheadRate = new ArrowheadRateModel
                {
                    RateId = rateModel.RateId,
                    RateTransactionID = rateModel.RateTransactionID,
                    CarrierQuoteID = rateModel.CarrierQuoteID,
                    CompanyId = rateModel.CompanyId,
                    CompanyName = rateModel.CompanyName,
                    DownPayment = rateModel.DownPayment - rateModel.NSDFees - rateModel.CCCharge,
                    MonthlyPayment = rateModel.MonthlyPayment,
                    BridgeURL = rateModel.BridgeURL,
                    Description = rateModel.Description,
                    TotalPremium = rateModel.TotalPremium,
                    PolicyTerm = rateModel.PolicyTerm,
                    NoOfInstallement = rateModel.NoOfInstallement
                };
                bindPolicyModel.quoteRateResponse = arrowheadRate;

                var quoteXMLDetails = GetXMLDetailsByQuoteID(quoteId);
                try
                {
                    string URL = ConstantVariables.ArrowheadBlobURL + quoteXMLDetails.ResponseFileURL;
                    HttpClient client = new HttpClient();
                    var responseString = client.GetStringAsync(URL).Result;
                    bindPolicyModel.QuoteResponseXML = responseString;
                }
                catch (Exception ex) { }
            }
            catch (Exception ex) { }

            return bindPolicyModel;
        }

        public ArrowheadCredential GetArrowheadCredentials(string StateCode, long agencyId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    AutoQuoteServices quoteServices = new AutoQuoteServices();
                    int stateID = db.States.Where(f => f.Code == StateCode).FirstOrDefault().ID;


                    var arrowheadCredential = GetRaterCredentials(ConstantVariables.RaterServiceProviderCode.ToString(), stateID, agencyId);
                    var agencyDetails = quoteServices.GetAgencyDetails(agencyId);
                    ArrowheadCredential autoApiClient = new ArrowheadCredential()
                    {
                        ArrowheadSPName = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.ARROWHEADSPNAME.ToString()).Select(s => s.Value).FirstOrDefault(),
                        ClientApp_Org = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.CLIENTAPPORG.ToString()).Select(s => s.Value).FirstOrDefault(),
                        ClientApp_Name = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.CLIENTAPPNAME.ToString()).Select(s => s.Value).FirstOrDefault(),
                        ClientApp_Version = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.CLIENTAPPVERSION.ToString()).Select(s => s.Value).FirstOrDefault(),
                        CommercialName = agencyDetails.AgencyName,
                        ProducerSurname = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.PRODUCERSURNAME.ToString()).Select(s => s.Value).FirstOrDefault(),
                        ProducerName = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.PRODUCERNAME.ToString()).Select(s => s.Value).FirstOrDefault(),
                        CompanyID = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.COMPANYID.ToString()).Select(s => s.Value).FirstOrDefault(),
                        APIPassword = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.APIPASSWORD.ToString()).Select(s => s.Value).FirstOrDefault(),
                        EndpointAddress = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.ENDPOINTADDRESS.ToString()).Select(s => s.Value).FirstOrDefault(),
                        ProducerAddr1 = agencyDetails.Address1,
                        ProducerAddr2 = agencyDetails.Address2,
                        ProducerCity = agencyDetails.City,
                        ProducerStateCD = agencyDetails.StateAbbr,
                        ProducerPhoneNumber = agencyDetails.ContactNo,
                        ProducerPostalCode = agencyDetails.ZipCode,
                        ContractNumber = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.CONTRACTNUMBER.ToString()).Select(s => s.Value).FirstOrDefault(),
                        ProducerSubCode = arrowheadCredential.Where(w => w.Code == Enums.ArrowheadCredential.PRODUCERSUBCODE.ToString()).Select(s => s.Value).FirstOrDefault()
                        //AgencyAddress1= agencyDetails.Address1,
                        //AgencyAddress2=agencyDetails.Address2,
                        //AgencyContactNo=agencyDetails.ContactNo,
                        //AgencyName=agencyDetails.AgencyName,
                        //AgencyCity=agencyDetails.City,
                        //AgencyState=agencyDetails.StateAbbr,
                        //AgencyZipCode=agencyDetails.ZipCode
                    };
                    return autoApiClient;
                }
                catch (Exception ex)
                {
                    return null/* TODO Change to default(_) if this is not a reference type */;
                }
            }
        }

        public WebRaterAPICall GetXMLDetailsByQuoteID(long quoteId)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                long CallTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.CALLTYPE.ToString(), Enums.CallType.RATE.ToString());
                return db.WebRaterAPICalls.Where(w => w.FkQuoteID == quoteId && w.FkCallTypeID == CallTypeID).OrderByDescending(x => x.ID).FirstOrDefault();
            }
        }

        public List<RaterCredentialsModel> GetRaterCredentials(string rateProviderCd, int stateId, long agencyId)
        {
            try
            {
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    var RaterProviderID = GetIdByServicesProvider(rateProviderCd);

                    var listCredential = (from rf in db.RaterProviderForms
                                          join rpc in db.RaterProviderCredentials on rf.ID equals rpc.FKRaterProviderFormId
                                          join rm in db.RaterProviderModes on rf.FKRaterServiceProviderId equals rm.FKRaterServiceProviderID
                                          where rm.IsLiveMode == rpc.IsLiveMode && rpc.FKRaterProviderModeID == rm.ID
                                          && (rm.FKAgencyID == agencyId || rm.FKAgencyID == null)
                                          //&& rf.FKRaterServiceProviderId == RaterProviderID
                                          select new
                                          {
                                              ModeId = rpc.FKRaterProviderModeID,
                                              AgencyId = rm.FKAgencyID,
                                              Code = rf.Code,
                                              Value = rpc.ValueText,
                                              IsLiveMode = rpc.IsLiveMode
                                          }).ToList();

                    var _listWithNullAgency = listCredential.Where(w => w.AgencyId == null).ToList();
                    var _listWithOutNullAgency = listCredential.Where(w => w.AgencyId == agencyId).ToList();

                    List<RaterCredentialsModel> _outputList = (from lw in _listWithOutNullAgency
                                                               select new RaterCredentialsModel
                                                               {
                                                                   Code = lw.Code,
                                                                   Value = lw.Value,
                                                                   IsLiveMode = lw.IsLiveMode
                                                               }).Union(from lwn in _listWithNullAgency
                                                                        where _listWithOutNullAgency.Count(c => c.Code == lwn.Code) == 0
                                                                        select new RaterCredentialsModel
                                                                        {
                                                                            Code = lwn.Code,
                                                                            Value = lwn.Value,
                                                                            IsLiveMode = lwn.IsLiveMode
                                                                        }).ToList();
                    //foreach (var item in _listWithOutNullAgency)
                    //{
                    //    var _overWriteValue = _listWithNullAgency.Where(w => w.Code == item.Code).FirstOrDefault();
                    //    if (_overWriteValue == null)
                    //    {
                    //        RaterCredentialsModel _model = new RaterCredentialsModel()
                    //        {
                    //            Code = item.Code,
                    //            IsLiveMode = item.IsLiveMode,
                    //            Value = item.Value
                    //        };
                    //    }
                    //    else
                    //    {
                    //        RaterCredentialsModel _model = new RaterCredentialsModel()
                    //        {
                    //            Code = item.Code,
                    //            IsLiveMode = item.IsLiveMode,
                    //            Value = !string.IsNullOrEmpty(_overWriteValue) ? _overWriteValue : item.Code
                    //        };
                    //        _outputList.Add(_model);
                    //    }
                    //}

                    //var listCredential = (from rf in db.RaterProviderForms
                    //                      join rpc in db.RaterProviderCredentials on rf.ID equals rpc.FKRaterProviderFormId
                    //                      join rm in db.RaterProviderModes on rf.FKRaterServiceProviderId equals rm.FKRaterServiceProviderID
                    //                      where rm.FKStateID == stateId && rm.IsLiveMode == rpc.IsLiveMode && rpc.FKRaterProviderModeID == rm.ID && rf.FKRaterServiceProviderId == RaterProviderID
                    //                      && rm.FKAgencyID == agencyId
                    //                      select new RaterCredentialsModel()
                    //                      {
                    //                          Code = rf.Code,
                    //                          Value = rpc.ValueText,
                    //                          IsLiveMode = rpc.IsLiveMode
                    //                      }).ToList();


                    // If _StateId = 0 Then
                    // query = query.Where(Function(x) x.rm.FKStateID Is Nothing)
                    // Else
                    // query = query.Where(Function(x) x.rm.FKStateID = _StateId)
                    // End If

                    return _outputList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<QuoteRateResponseModel> GetArrowheadResponse(AutoRequestModel AutoRequest, long quoteID, long agencyId)
        {
            Arrowhead.POS.Process.Arrowhead arrowhead = new Arrowhead.POS.Process.Arrowhead();
            GeneralServices _generalServices = new GeneralServices();
            var stateId = _generalServices.GetStateID(quoteID);
            var stateCode = GeneralServices.GetStateCode(stateId);
            var response = arrowhead.GetRates(AutoRequest, GetArrowheadCredentials(AutoRequest.AutoPolicy.StateCd, agencyId), stateCode);
            var quoteRateResponse = InsertRateHistoryData(response.Response, quoteID);
            UpdateRateXMLAsync(quoteRateResponse, response, quoteID);
            return quoteRateResponse;
        }

        public void UpdateRateXMLAsync(List<QuoteRateResponseModel> quoteResponseModel, AutoResponseModel responseModel, long quoteID)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                string fileName = quoteID + "-" + DateTime.Now.ToFileTime().ToString();
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    GeneralServices generalServices = new GeneralServices();
                    int insuranceCompanyID = db.InsuranceCompanies.Where(x => x.Code == ConstantVariables.ArrowheadCompanyCode).Select(x => x.ID).FirstOrDefault();
                    var RequestXMLByte = Encoding.UTF8.GetBytes(responseModel.RequestFile);
                    var ResponseXMLByte = Encoding.UTF8.GetBytes(responseModel.RequestFile);
                    long CallTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.CALLTYPE.ToString(), Enums.CallType.RATE.ToString());
                    //foreach (var item in quoteResponseModel)
                    //{
                    //    if (item.RateId > 0)
                    //    {
                    if (quoteResponseModel.Count > 0)
                    {
                        var rateDetail = quoteResponseModel.FirstOrDefault();
                        WebRaterAPICall callManagement = new WebRaterAPICall();
                        callManagement.FkQuoteID = quoteID;
                        callManagement.FkInsuranceCompanyID = insuranceCompanyID;
                        callManagement.RequestDate = ConvertTo.GetEstTimeNow();
                        callManagement.ResponseDate = ConvertTo.GetEstTimeNow();
                        callManagement.ResponseTransactionID = rateDetail.RateTransactionID;
                        callManagement.RequestTransactionID = rateDetail.RateTransactionID;
                        callManagement.FkCallTypeID = CallTypeID;
                        callManagement.IsSuccess = (rateDetail.IsError ? false : true);
                        callManagement.RequestFileURL = ConstantVariables.RequestXMLContainer + "/" + fileName + "_reqXML" + extension;
                        callManagement.ResponseFileURL = ConstantVariables.ResponseXMLContainer + "/" + fileName + "_resXML" + extension;
                        db.WebRaterAPICalls.Add(callManagement);
                        db.SaveChanges();
                    }
                    //    }
                    //}

                    new Thread(delegate () { UploadRequestXML(responseModel, fileName); })
                    {
                    }.Start();

                    new Thread(delegate () { UploadResponseXML(responseModel, fileName); })
                    {
                    }.Start();


                }
            }
            catch (Exception ex) { }

        }

        public void UploadRequestXML(AutoResponseModel responseModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var RequestXMLByte = Encoding.UTF8.GetBytes(responseModel.RequestFile);
                var _RequestFileUri = blobFileUtility.Save(ConstantVariables.RequestXMLContainer, fileName + "_reqXML" + extension, RequestXMLByte).Result;
            }
            catch (Exception ex) { }

        }

        public void UploadResponseXML(AutoResponseModel responseModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var ResponseXMLByte = Encoding.UTF8.GetBytes(responseModel.ResponseFile);
                var _ResponseFileUri = blobFileUtility.Save(ConstantVariables.ResponseXMLContainer, fileName + "_resXML" + extension, ResponseXMLByte);
            }
            catch (Exception ex) { }

        }

    }

}
