﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using Arrowhead.POS.Service;

namespace Arrowhead.POS.Service
{
    public class AuditHistoryService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        public void InsertAuditHistory(long quoteId,int agencyId,long userId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {


                    AuditHistory auditHistory = new AuditHistory();
                    {
                        auditHistory.FKQuoteId = quoteId;
                        auditHistory.FKCustomerId = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.FkCustomerID).FirstOrDefault();
                        auditHistory.GroupTransactionId = Guid.NewGuid();
                        auditHistory.IPAddress = HttpContext.Current.Request.UserHostAddress;
                        auditHistory.FKAgencyID = agencyId;
                        auditHistory.FKUserId = userId;
                        auditHistory.FKModuleId = Enums.AuditHistoryModule.AV.ToString();
                        auditHistory.TransactionDate = ConvertTo.GetEstTimeNow();
                        auditHistory.UserAgent = HttpContext.Current.Request.UserAgent;
                        auditHistory.Operation = Enums.AuditHistoryOperation.I.ToString();
                        auditHistory.EntityId = userId.ToString();
                        auditHistory.Remark = "Customer address has been inserted from" + " " + auditHistory.IPAddress + "  " + "on" + " " + ConvertTo.GetEstTimeNow();
                    }
                    db.AuditHistories.Add(auditHistory);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var test = ex.InnerException;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="historyModel"></param>
        public void InsertAuditHistory(AuditHistoryModel historyModel)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    AuditHistory auditHistory = new AuditHistory()
                    {
                        FKAgencyID = historyModel.AgencyID == 0 ? (int?)null : historyModel.AgencyID,
                        EntityId = historyModel.EntityID,
                        FKCustomerId = historyModel.CustomerId,
                        FKQuoteId = historyModel.QuoteID == 0 ? null : historyModel.QuoteID,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        IsError = historyModel.IsError,
                        GroupTransactionId = Guid.NewGuid(),
                        Operation = historyModel.Operation.ToString(),
                        Remark = historyModel.Remarks,
                        TransactionDate = ConvertTo.GetEstTimeNow(),
                        UserAgent = HttpContext.Current.Request.UserAgent,
                        FKModuleId = historyModel.ModuleID
                    };
                    db.AuditHistories.Add(auditHistory);
                    db.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
