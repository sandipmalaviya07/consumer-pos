﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Model;
using Arrowhead.POS.Core;
using Arrowhead.POS.Core.Paging;

namespace Arrowhead.POS.Service
{
    public class LogServices
    {
        public LogModel GetXMLDetailsByQuoteID(long _QuoteId)
        {
            LogModel logModel = new LogModel();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _logDetailList = (from wpc in db.WebRaterAPICalls
                                          join gst in db.GeneralStatusTypes on wpc.FkCallTypeID equals gst.ID
                                          where wpc.FkQuoteID == _QuoteId
                                          select new LogDetailModel()
                                          {
                                              QuoteId = wpc.FkQuoteID,
                                              CallType = gst.Name,
                                              RequestDate = wpc.RequestDate,
                                              ResponseDate = wpc.ResponseDate,
                                              ResponseXML = ConstantVariables.ArrowheadBlobURL + "" + wpc.ResponseFileURL,
                                              RequestXML = ConstantVariables.ArrowheadBlobURL + "" + wpc.RequestFileURL
                                          }).OrderByDescending(x => x.RequestDate).ToList();
                    logModel.LogDetailModel = _logDetailList;
                    return logModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageRows"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public PagerList<ErrorLogViewModel> GetErrorLogList(DateTime startDate, DateTime endDate, int pageNumber = 1, int pageRows = 20, string orderBy = "TransactionDate descending")
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    PagerList<ErrorLogViewModel> paginationObject = null;
                    var startDateFilter = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
                    var endDateFilter = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                    var errorLogView = (from au in db.AuditHistories
                                        let a = db.Agencies.FirstOrDefault(f => f.ID == au.FKAgencyID)
                                        select new { au, a }).AsQueryable();

                    errorLogView = errorLogView.Where(f => f.au.FKAgencyID != null && f.au.IsError == true).AsQueryable();

                    errorLogView = errorLogView.Where(f => f.au.TransactionDate >= startDateFilter && f.au.TransactionDate <= endDateFilter).OrderByDescending(x=>x.au.ID).AsQueryable();

                    paginationObject = (from e in errorLogView
                                        select new ErrorLogViewModel
                                        {
                                            QuoteId = e.au.FKQuoteId ?? 0,
                                            CustomerId = e.au.FKCustomerId ?? 0,
                                            IPAddress = e.au.IPAddress,
                                            Remark = e.au.Remark,
                                            AgencyName = e.a.CommercialName,
                                            Module = e.au.FKModuleId,
                                            TransactionDate=e.au.TransactionDate

                                        }).ToPagerListOrderBy(pageNumber, pageRows, orderBy);

                    return paginationObject;

                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


    }
}
