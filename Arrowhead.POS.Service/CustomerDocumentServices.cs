﻿using System;
using System.Collections.Generic;
using EntityFramework.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Process;

namespace Arrowhead.POS.Service
{
    public class CustomerDocumentServices
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void InsertCustomerDocument(CustomerDocumentModel model)
        {
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var customerDocumentDetail = db.CustomerDocuments.Where(w => w.FkQuoteId == model.QuoteId).FirstOrDefault();
                    if (customerDocumentDetail != null)
                    {
                        db.CustomerDocuments.Where(x => x.FkQuoteId == model.QuoteId).Delete();

                    }

                    var getGeneralStatusType = generalServices.GetGeneralStatusList();
                    long fileTypeId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.FILETYPE.ToString() && w.Code == Enums.FILETYPE.PDF.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                    long documentTypeId = getGeneralStatusType.Where(w => w.Type == Enums.GeneralStatus.DOCUMENTTYPE.ToString() && w.Code == Enums.DOCUMENTTYPE.POLICY.ToString()).Select(s => s.FkGeneralStatusId).FirstOrDefault();
                    long customerId = db.Quotes.Where(w => w.ID == model.QuoteId).Select(w => w.FkCustomerID).FirstOrDefault();
                    CustomerDocument customerDocument = new CustomerDocument
                    {
                        FkFileTypeId = fileTypeId,
                        FkDocumentId = documentTypeId,
                        FkCustomerId = customerId,
                        FkQuoteId = model.QuoteId,
                        DocPath = model.DocPath,
                        CreatedDate = ConvertTo.GetEstTimeNow(),
                        CreatedById = model.CreatedById,
                        EntityCode = Enums.EntityCode.DOCUSIGN.ToString(),
                        IsDeleted = false
                    };
                    db.CustomerDocuments.Add(customerDocument);
                    db.SaveChanges();
                }



            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string officeSignUrl(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.CustomerDocuments.Where(w => w.FkQuoteId == quoteId).Select(s => s.DocPath).FirstOrDefault();
            }
        }

        /// <summary>
        /// Reset Customer Document 
        /// </summary>
        /// <param name="quoteId"></param>
        public void ResetCustomerDocument(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.CustomerDocuments.Where(x => x.FkQuoteId == quoteId).Update(x => new CustomerDocument()
                {
                    DocPath = string.Empty
                });
            }
        }

        /// <summary>
        /// Update Customer Document 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <param name="_customerDocumerModel"></param>
        public bool UpdateCustomerDocument(CustomerDocumentModel _customerDocumerModel)
        {
            bool isCustomerDocumentUpdate = false;
            using (var db = new ArrowheadPOSEntities())
            {
                long customerId = db.Quotes.Where(w => w.ID == _customerDocumerModel.QuoteId).Select(w => w.FkCustomerID).FirstOrDefault();

                db.CustomerDocuments.Where(x => x.FkQuoteId == _customerDocumerModel.QuoteId).Update(x => new CustomerDocument()
                {
                    FkCustomerId = customerId,
                    CreatedById = _customerDocumerModel.CreatedById,
                    DocPath = _customerDocumerModel.DocPath
                });
                isCustomerDocumentUpdate = true;
            }
            return isCustomerDocumentUpdate;
        }
        //public CustomerDocument GetCustomerDocumentDetailsById(long quoteId)
        //{
        //    using (var db = new ArrowheadPOSEntities())
        //    {
        //        var details=db.CustomerDocuments.Where(w=>w.FkQuoteId==quoteId).FirstOrDefault()
        //    }
        //}
    }
}
