﻿using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Extensions;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Arrowhead.POS.Core;


namespace Arrowhead.POS.Service
{
    public class GeneralServices
    {
        public long GetGeneralStatusIdByType(string Type, string Code)
        {
            long i = 0;
            try
            {
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    i = (from st in db.GeneralStatus
                         join ty in db.GeneralStatusTypes on st.ID equals ty.FKGeneralStatusID
                         where st.Code == Type && ty.Code == Code
                         select ty.ID).FirstOrDefault();
                }
            }
            catch (Exception ex) { }
            return i;
        }


        public long GetGeneralStatusOrderIdByType(string Type, string Code)
        {
            long i = 0;
            try
            {
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    i = (from st in db.GeneralStatus
                         join ty in db.GeneralStatusTypes on st.ID equals ty.FKGeneralStatusID
                         where st.Code == Type && ty.Code == Code
                         select ty.OrderNo).FirstOrDefault();
                }
            }
            catch (Exception ex) { }
            return i;
        }
        

        public List<GeneralStatusModel> GetGeneralStatusList()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                var lstGeneralStatusModel = (from gs in db.GeneralStatus
                                             join gst in db.GeneralStatusTypes on gs.ID equals gst.FKGeneralStatusID
                                             select new GeneralStatusModel
                                             {
                                                 Code = gst.Code,
                                                 GeneralStatusName = gs.Name,
                                                 GeneralStatusTypeName = gst.Name,
                                                 Type = gs.Code,
                                                 OrderNo = gst.OrderNo,
                                                 FkGeneralStatusId = gst.ID
                                             }).ToList();
                return lstGeneralStatusModel;
            }
        }

        public int GetStateID(long QuoteID)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.Quotes.Where(x => x.ID == QuoteID).Select(x => x.FkStateId).FirstOrDefault();
            }
        }

        public static string GetStateCode(int stateID)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.States.Where(x => x.ID == stateID).Select(x => x.Code).FirstOrDefault();
            }
        }


        public int GetStateId(string stateAbbr)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.States.Where(x => x.Code == stateAbbr).Select(x => x.ID).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="licenseNo"></param>
        /// <returns></returns>
        public bool LicenseNoCheck(int stateId, string licenseNo)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                //GeneralServices generalServices = new GeneralServices();
                string stateCode = GeneralServices.GetStateCode(stateId);
                string regex = string.Empty;
                switch (stateCode)
                {
                    case "MI":
                        regex = "^[a-zA-Z][0-9]{8}$";
                        break;
                }
                Regex re = new Regex(regex);
                if (regex != null)
                {
                    if (re.IsMatch(licenseNo))
                        return (true);
                    else
                        return (false);
                }
                return true;

            }
        }

        public static string GenerateAuthToken(int charLength)
        {
            var random = new Random();
            var resultToken = new string(Enumerable.Repeat(ConstantVariables.ConstantAllCharsString.ToString(), charLength).Select(token => token[random.Next(token.Length)]).ToArray());
            return resultToken.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="officeId"></param>
        /// <returns></returns>
        public SmtpDetailModel GetSmtpDetails()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    int officeId = db.Offices.Where(x => x.Code == ConstantVariables.ArrowheadCompanyCode).Select(x => x.ID).FirstOrDefault();
                    var officeSmtp = (from os in db.OfficeSmtpDetails
                                      where os.FkOfficeID == officeId
                                      select new SmtpDetailModel
                                      {
                                          OfficeSmtpDetailId = os.ID,
                                          OfficeId = os.FkOfficeID,
                                          EmailFrom = os.EmailFrom,
                                          EmailReplyTo = os.EmailReplyTo,
                                          Name = os.Name,
                                          IsBodyHtml = os.IsBodyHtml,
                                          Priority = os.Priority,
                                          SmtpTypeId = os.FkSmtpTypeID,
                                          Host = os.Host,
                                          Password = os.Password,
                                          Username = os.Username,
                                          Port = os.Port,
                                          IsUseDefaultCredentials = os.IsUseDefaultCredentials,
                                          IsEnableSsl = os.IsEnableSsl,
                                      }).FirstOrDefault();

                    if (officeSmtp == null)
                    {
                        officeSmtp = (from os in db.OfficeSmtpDetails
                                      where os.IsDefault == true && os.IsDeleted == false && os.FkOfficeID == null
                                      select new SmtpDetailModel
                                      {
                                          OfficeSmtpDetailId = os.ID,
                                          OfficeId = os.FkOfficeID,
                                          EmailFrom = os.EmailFrom,
                                          EmailReplyTo = os.EmailReplyTo,
                                          Name = os.Name,
                                          IsBodyHtml = os.IsBodyHtml,
                                          Priority = os.Priority,
                                          SmtpTypeId = os.FkSmtpTypeID,
                                          Host = os.Host,
                                          Password = os.Password,
                                          Username = os.Username,
                                          Port = os.Port,
                                          IsUseDefaultCredentials = os.IsUseDefaultCredentials,
                                          IsEnableSsl = os.IsEnableSsl,
                                      }).FirstOrDefault();
                    }
                    return officeSmtp;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public static TwilioAuthorizationModel GetCommunicationCredentials()
        {
            TwilioAuthorizationModel authorizeDetails = new TwilioAuthorizationModel();
            try
            {
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    int agencyID = db.Agencies.Where(x => x.Code == ConstantVariables.ArrowheadCompanyCode).Select(x => x.ID).FirstOrDefault();
                    var query = from cp in db.CommunicationServicesProviders
                                join cf in db.CommunicationProviderForms on cp.ID equals cf.FKCommunicationServiceProviderId
                                join cpc in db.CommunicationProviderCredentials on cf.ID equals cpc.FKCommunicationProviderFormId
                                join cpm in db.CommunicationProviderModes on cpc.FKCommunicationProviderModeID equals cpm.ID
                                where cpm.FKAgencyID == agencyID && cpm.IsDefault == true && cpm.IsActive == true
                                select new { cp, cf, cpc, cpm };

                    var _getcredential = (from q in query
                                          select new CommunicationCredentialModel
                                          {
                                              Code = q.cf.LableText,
                                              Value = q.cpc.ValueText,
                                              ServiceProvider = q.cp.Code

                                          }).ToList();


                    if (_getcredential != null && _getcredential.Count() > 0)
                    {
                        if (_getcredential.FirstOrDefault().ServiceProvider == Enums.CommunicationServicesProvider.TWILIO.ToString())
                        {
                            authorizeDetails = new TwilioAuthorizationModel
                            {
                                AccountSid = _getcredential.Where(w => w.Code == Enums.CommunicationProviderForm.AccountSid.ToString()).Select(s => s.Value).FirstOrDefault(),
                                AuthToken = _getcredential.Where(w => w.Code == Enums.CommunicationProviderForm.AuthToken.ToString()).Select(s => s.Value).FirstOrDefault(),
                                Name = _getcredential.Where(w => w.Code == Enums.CommunicationProviderForm.Name.ToString()).Select(s => s.Value).FirstOrDefault(),
                                PhoneNumber = _getcredential.Where(w => w.Code == Enums.CommunicationProviderForm.PhoneNumber.ToString()).Select(s => s.Value).FirstOrDefault(),
                                ShortCode = _getcredential.Where(w => w.Code == Enums.CommunicationProviderForm.ShortCode.ToString()).Select(s => s.Value).FirstOrDefault()
                            };


                        }
                    }
                }
            }
            catch (Exception ex) { }
            return authorizeDetails;
        }

        public void UpdateQuoteCurrentStep(long QuoteId, string currentStepCode)
        {
            GeneralServices generalServices = new GeneralServices();
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {

                long fkcurrentstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), currentStepCode.ToString());
                db.Quotes.Where(w => w.ID == QuoteId).Update(U => new Quote
                {
                    FKCurrentStepId = fkcurrentstatusId
                });
            }
        }

        /// <summary>
        /// Update Agency Current Step by Agency and Step Code
        /// </summary>
        /// <param name="AgencyId"></param>
        /// <param name="currentStepCode"></param>
        public void UpdateAgencyCurrentStep(long AgencyId, string currentStepCode)
        {
            GeneralServices generalServices = new GeneralServices();
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {

                long fkcurrentstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.AGENCYSETUP.ToString(), currentStepCode.ToString());
                db.Agencies.Where(w => w.ID == AgencyId).Update(U => new Agency
                {
                    FKAgencyCurrentStepID = fkcurrentstatusId
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteId"></param>
        public void ResetAutoQuoteDetail(long QuoteId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                db.Auto_QuoteDetail.Where(w => w.FKQuoteID == QuoteId).Update(U => new Auto_QuoteDetail
                {
                    FKRateHistoryID = null
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public StripeCredentialModel GetStripeCredential()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                StripeCredentialModel stripeCredentialModel = new StripeCredentialModel();

                var stripePGData = (from x in db.PaymentGatewayProviderForms
                                    join pc in db.PaymentGatewayProviderCredentials on x.ID equals pc.FKPaymentGatewayProviderFormID
                                    join pm in db.PaymentGatewayProviderModes on pc.FKPaymentGatewayProviderModeID equals pm.ID
                                    join pp in db.PaymentGatewayProviders on x.FKPaymentGatewayProviderID equals pp.Id
                                    where pp.Code == "STRIPE"
                                    select new { pc, x }).ToList();
                foreach (var item in stripePGData)
                {
                    if (item.x.Code == "APIKEY")
                        stripeCredentialModel.APIKey = item.pc.ValueText;
                    if (item.x.Code == "PUBLISHABLEKEY")
                        stripeCredentialModel.PublishableKey = item.pc.ValueText;
                    if (item.x.Code == "CCFEES")
                        stripeCredentialModel.CCFees = Convert.ToDecimal(item.pc.ValueText);

                }

                //if (host.Contains("localhost"))
                //{
                //    stripeCredentialModel.PublishableKey = ConstantVariables.DevPublishableKey;
                //    stripeCredentialModel.TestAPIKey = ConstantVariables.DevTestAPIKey;
                //    stripeCredentialModel.StripeAccountId = ConstantVariables.DevStripeAccountId;
                //}
                //else
                //{
                //    stripeCredentialModel.PublishableKey = ConstantVariables.ProdPublishableKey;
                //    stripeCredentialModel.TestAPIKey = ConstantVariables.ProdTestAPIKey;
                //    stripeCredentialModel.StripeAccountId = ConstantVariables.ProdStripeAccountId;
                //}
                return stripeCredentialModel;
            }
        }

        public static StripeCredentialModel GetStripeCredentialStatic(string host)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                StripeCredentialModel stripeCredentialModel = new StripeCredentialModel();

                var stripePGData = (from x in db.PaymentGatewayProviderForms
                                    join pc in db.PaymentGatewayProviderCredentials on x.ID equals pc.FKPaymentGatewayProviderFormID
                                    join pm in db.PaymentGatewayProviderModes on pc.FKPaymentGatewayProviderModeID equals pm.ID
                                    join pp in db.PaymentGatewayProviders on x.FKPaymentGatewayProviderID equals pp.Id
                                    where pm.IsDefault == true && pp.IsActive == true && pp.Code == "STRIPE"
                                    select new { pc, x }).ToList();
                foreach (var item in stripePGData)
                {
                    if (item.x.Code == "APIKEY")
                        stripeCredentialModel.APIKey = item.pc.ValueText; // ConstantVariables.DevTestAPIKey;//
                    if (item.x.Code == "PUBLISHABLEKEY")
                        stripeCredentialModel.PublishableKey = item.pc.ValueText; // ConstantVariables.DevPublishableKey;//
                    if (item.x.Code == "CCFEES")
                        stripeCredentialModel.CCFees = Convert.ToDecimal(item.pc.ValueText);

                }

              
                return stripeCredentialModel;
            }
        }
        /// <summary>
        /// Get Payment Method Type By PaymentMethodID
        /// </summary>
        /// <param name="paymentMethodID"></param>
        /// <returns></returns>
        public string GetPaymentMethodByID(long paymentMethodID)
        {
            string paymentMethod = string.Empty;
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                paymentMethod = (from st in db.GeneralStatus
                                 join ty in db.GeneralStatusTypes on st.ID equals ty.FKGeneralStatusID
                                 where ty.ID == paymentMethodID
                                 select ty.Name).FirstOrDefault();
                return paymentMethod;
            }
        }
        public string GetActivePGCode()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.PaymentGatewayProviders.Where(w => w.IsActive == true).Select(s => s.Code).FirstOrDefault();
            }
        }
        public int GetActivePGID()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.PaymentGatewayProviders.Where(w => w.IsActive == true).Select(s => s.Id).FirstOrDefault();
            }
        }
        public string GetCodeFromGeneralStatusType(long _Id)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.GeneralStatusTypes.Where(w => w.ID == _Id).Select(s => s.Code).FirstOrDefault();
            }
        }
        public decimal ActivePGCCFees()
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                string _PGFees = (from pgc in db.PaymentGatewayProviderCredentials
                                  join pf in db.PaymentGatewayProviderForms on pgc.FKPaymentGatewayProviderFormID equals pf.ID
                                  join pp in db.PaymentGatewayProviders on pf.FKPaymentGatewayProviderID equals pp.Id
                                  where pf.Code == "CCFEES"
                                  select pgc.ValueText).FirstOrDefault();
                return !string.IsNullOrEmpty(_PGFees) ? Convert.ToDecimal(_PGFees) : 0;
            }
        }

        /// <summary>
        /// For ACH Payment Check Bank Details Exist 
        /// </summary>
        /// <param name="agencyID"></param>
        /// <returns></returns>
        public bool IsBankDetailsExistByAgencyId(long agencyID) // ,bool isBankDetailSkip
        {
            //BankDetailModel _bankDetailModel = new BankDetailModel();
            bool _isBankDetailsExist = false;
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                _isBankDetailsExist = db.AgencyBankDetails.Where(x => x.FKAgencyID == agencyID).Any();
            }

            //if(isBankDetailSkip == true)
            //{
            //    _bankDetailModel.IsSuccess = true;
            //}
            //else
            //{
            //    _bankDetailModel.IsSuccess = _isBankDetailsExist; // set result
            //}
            return _isBankDetailsExist;
        }

        public int GetActivePGIDByCode(string _pgcode)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.PaymentGatewayProviders.Where(w => w.Code == _pgcode).Select(s => s.Id).FirstOrDefault();
            }
        }

        /// <summary>
        /// Validate Spam Contact No
        /// </summary>
        /// <param name="contactNo"></param>
        /// <returns></returns>
        public bool IsSpamContactNo (string contactNo)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return (db.SpamContacts.Where(x => x.PhoneNo.Equals(contactNo)).Any());
            }
        }
        
    }
}