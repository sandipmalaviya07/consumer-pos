﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using EntityFramework.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Core.Resources;

namespace Arrowhead.POS.Service
{
    public class AutoDriverServices : BaseService
    {
        /// <summary>
        /// Get Driver Count
        /// </summary>
        /// <param name="QuoteID">QuoteID</param>
        /// <returns></returns>
        public int GetDriverCount(long QuoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Auto_Driver.Where(x => x.FkQuoteID == QuoteID && x.IsDeleted == false).Count();
            }
        }

        /// <summary>
        /// Get Driver Violation By DriverID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AutoDriverViolationModel> GetViolationByDriverID(long id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from dv in db.Auto_DriverViolation
                            join v in db.Auto_Violation on dv.FKAutoViolationId equals v.ID
                            where dv.FkDriverId == id
                            select new AutoDriverViolationModel()
                            {
                                AutoDriverViolationId = dv.ID,
                                DriverId = dv.FkDriverId,
                                AutoViolationId = dv.FKAutoViolationId,
                                ViolationCode = v.Code,
                                ViolationName = v.Name,
                                ViolationType = v.TypeOfViolation,
                                ViolationDate = dv.ViolationDate,
                                Points = dv.Points,
                                Note = dv.Note,
                                BIamount = dv.BIAmount,
                                PDamount = dv.PDAmount,
                                IsDeleted = false,
                                IsMVR = dv.IsMVR
                            }).ToList();
                return item;
            }
        }


        public List<AutoDriverViolationModel> GetViolationByQuoteId(long QuoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from dv in db.Auto_DriverViolation
                            join v in db.Auto_Violation on dv.FKAutoViolationId equals v.ID
                            let ad = db.Auto_Driver.FirstOrDefault(f => f.ID == dv.FkDriverId)
                            where ad.FkQuoteID == QuoteId
                            select new AutoDriverViolationModel()
                            {

                                AutoDriverViolationId = dv.ID,
                                DriverId = dv.FkDriverId,
                                DriverLicenseNumber = !string.IsNullOrEmpty(ad.LicenseNo) ? ad.LicenseNo : string.Empty,
                                AutoViolationId = dv.FKAutoViolationId,
                                ViolationCode = v.Code,
                                ViolationName = v.Name,
                                ViolationType = v.TypeOfViolation,
                                ViolationDate = dv.ViolationDate,
                                Points = dv.Points,
                                Note = dv.Note,
                                BIamount = dv.BIAmount,
                                PDamount = dv.PDAmount,
                                FirstName = ad.FirstName,
                                LastName = ad.LastName,
                                IsDeleted = false,
                                IsMVR = dv.IsMVR
                            }).ToList();
                return item;
            }
        }




        /// <summary>
        /// Get Is Driver Is Frist Driver 
        /// </summary>
        /// <param name="quoteID">QuoteID</param>
        /// <param name="driverID">driver ID</param>
        /// <returns></returns>
        public bool IsFirstDriver(long quoteID, long? driverID = 0)
        {
            bool IsFirstDriver = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    int driverCount = db.Auto_Driver.Where(x => x.FkQuoteID == quoteID && x.IsDeleted == false).Count();
                    if (driverCount == 0)
                    {
                        IsFirstDriver = true;                        //return IsFirstDriver;
                    }
                    if (driverID > 0)
                    {
                        IsFirstDriver = db.Auto_Driver.Where(x => x.ID == driverID && x.DriverOrderID == 1).Any();
                        // return IsFirstDriver = true;
                    }
                }
            }
            catch (Exception ex) { }

            return IsFirstDriver;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        public int GetApplicantStateID(long quoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Quotes.Where(x => x.ID == quoteID).Select(x => x.FkStateId).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public CustomerModel GetCustomerDetailsByQuoteId(long QuoteId)
        {
            using (var db = new ArrowheadPOSEntities())

            {
                var cutomer = (from q in db.Quotes
                               let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                               let d = db.Auto_Driver.FirstOrDefault(f => f.FkQuoteID == q.ID)
                               where q.ID == QuoteId
                               select new CustomerModel
                               {
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   QuoteId = q.ID,
                                   StateId = q.FkStateId
                               }).FirstOrDefault();
                return cutomer;
            }
        }


        public AutoDriverViolationModel GetViolationByID(int id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var item = (from v in db.Auto_Violation
                            where v.ID == id
                            select new AutoDriverViolationModel() { AutoDriverViolationId = v.ID, ViolationName = v.Name, ViolationCode = v.Code, ViolationType = v.TypeOfViolation }).FirstOrDefault();
                return item;
            }
        }

        public void DeleteDriverViolationByID(long driverID)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    db.Auto_DriverViolation.Where(f => f.FkDriverId == driverID && f.IsMVR == false).Delete(); // Delete Without MVR Violations
                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }
        }

        public void DeleteDriverInfo(long driverID)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    db.Auto_DriverViolation.Where(f => f.FkDriverId == driverID && f.IsMVR == false).Delete();
                    db.Auto_Driver.Where(f => f.ID == driverID).Delete();
                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }
        }

        public void InsertDriverViolation(AutoDriverViolationModel violationModel)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _createDate = ConvertTo.GetEstTimeNow();
                    Auto_DriverViolation violation = new Auto_DriverViolation();
                    violation.FkDriverId = violationModel.DriverId;
                    violation.FKAutoViolationId = violationModel.AutoViolationId;
                    violation.ViolationDate = violationModel.ViolationDate;
                    violation.Points = violationModel.Points;
                    violation.CreatedDate = _createDate;
                    violation.IsMVR = false;
                    violation.CreatedById = violationModel.UserId;
                    db.Auto_DriverViolation.Add(violation);
                    db.SaveChanges();

                    InsertAuditHistory(Enums.AuditHistoryModule.ADV, violationModel.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), Guid.NewGuid(), violation.ID.ToString(), violationModel.QuoteId, 0, null);

                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory(violationModel.AgencyId, Enums.AuditHistoryModule.ADV.ToString(), Enums.AuditHistoryOperation.I.ToString(), violationModel.QuoteId, ex.ToString());

                var err = ex.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public List<AutoDriverModel> GetDriverByQuoteID(long QuoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    return (from ad in db.Auto_Driver
                            let maritialStatus = db.GeneralStatusTypes.Where(f => f.ID == ad.FkMaritalStatusID).FirstOrDefault()
                            let relation = db.GeneralStatusTypes.Where(f => f.ID == ad.FkRelationCdID).FirstOrDefault()
                            where ad.FkQuoteID == QuoteID && ad.IsDeleted == false
                            select new AutoDriverModel()
                            {
                                AutoDriverId = ad.ID,
                                QuoteId = ad.FkQuoteID,
                                StateId = ad.FkStateID,
                                Dob = ad.BirthDate,
                                FirstName = ad.FirstName,
                                LastName = ad.LastName,
                                LicenseNo = ad.LicenseNo,
                                CreditScoreID = ad.FkCreditScoreID,
                                DriverEducationID = ad.FkDriverEducationID,
                                MaritalStatusID = ad.FkMaritalStatusID,
                                MaritalStatusName = maritialStatus.Name,
                                MaritalStatusCode = maritialStatus.Code,
                                RelationName = relation.Name,
                                RelationCDId = ad.FkRelationCdID,
                                DriverOrderID = ad.DriverOrderID,
                                HomeTypeID = ad.FkHomeTypeID,
                                IsExcluded = ad.IsExcluded,
                                Gender = ad.Gender,
                                Socialsecurity = ad.SocialSecurity,
                                Sr22 = ad.IsSR22,
                                DateFirstLicense = ad.DateFirstLicense,
                                Employer = ad.Employer,
                                EmployerYears = ad.EmployerYears,
                                MilesToWork = ad.MilesToWork,
                                YearsLicensed = ad.YearsLicensed,
                                IsWorkLossBenefit = ad.IsWorkLossBenefit,
                                IsSeniorDriver = ad.IsSeniorDriver,
                                DriverOccupationID = ad.FkDriverOccupationID,
                                LicenseStatus = ad.LicenseStatus,
                                IsDefensiveDriverCourse = ad.IsDefensiveDriverCourse,
                                DrivertrainingDate = ad.DrivertrainingDate
                            }).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public bool IsAddSpouse(long QuoteId)
        {
            bool isAddSpouse = false;
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    long fkmaritalstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MARITALSTATUS.ToString(), Enums.MaritalStatus.MARRIED.ToString());
                    long spouseRelationshipID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.SPOUSE.ToString());
                    long driverMaritialStatusId = db.Auto_Driver.Where(w => w.FkQuoteID == QuoteId && w.DriverOrderID == 1).Select(x => x.FkMaritalStatusID).FirstOrDefault();

                    if (driverMaritialStatusId == fkmaritalstatusId)
                    {
                        bool IsSpouseAlreadyAdded = db.Auto_Driver.Where(w => w.FkQuoteID == QuoteId && w.FkRelationCdID == spouseRelationshipID).Any();

                        if (!IsSpouseAlreadyAdded)
                            isAddSpouse = true;
                    }
                }
            }
            catch (Exception ex) { }
            return isAddSpouse;
        }

        public long GetDefaultDriverID(long QuoteID)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {

                return db.Auto_Driver.Where(x => x.FkQuoteID == QuoteID && x.DriverOrderID == 1).Select(x => x.ID).FirstOrDefault();
            }
        }
        public AutoDriverModel GetDriverByDriverID(long AutoDriverID)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    GeneralServices generalServices = new GeneralServices();
                    long milatryTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.ARMY.ToString());
                    long warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.SAMSCLUB.ToString());
                    return (from ad in db.Auto_Driver
                            where ad.ID == AutoDriverID
                            let maritialStatus = db.GeneralStatusTypes.Where(f => f.ID == ad.FkMaritalStatusID).FirstOrDefault()
                            let relation = db.GeneralStatusTypes.Where(f => f.ID == ad.FkRelationCdID).FirstOrDefault()
                            select new AutoDriverModel()
                            {
                                AutoDriverId = ad.ID,
                                QuoteId = ad.FkQuoteID,
                                StateId = ad.FkStateID,
                                Dob = ad.BirthDate,
                                FirstName = ad.FirstName,
                                LastName = ad.LastName,
                                LicenseNo = ad.LicenseNo,
                                CreditScoreID = ad.FkCreditScoreID,
                                DriverEducationID = ad.FkDriverEducationID,
                                MaritalStatusID = ad.FkMaritalStatusID,
                                MaritalStatusName = maritialStatus.Name,
                                MaritalStatusCode = maritialStatus.Code,
                                RelationName = relation.Name,
                                RelationCDId = ad.FkRelationCdID,
                                DriverOrderID = ad.DriverOrderID,
                                HomeTypeID = ad.FkHomeTypeID,
                                IsExcluded = ad.IsExcluded,
                                Gender = ad.Gender,
                                Socialsecurity = ad.SocialSecurity,
                                Sr22 = ad.IsSR22,
                                IsSr22A = ad.IsSR22A,
                                DateFirstLicense = ad.DateFirstLicense,
                                Employer = ad.Employer,
                                EmployerYears = ad.EmployerYears,
                                IsWorkLossBenefit = ad.IsWorkLossBenefit,
                                IsSeniorDriver = ad.IsSeniorDriver,
                                DriverOccupationID = ad.FkDriverOccupationID,
                                MilesToWork = ad.MilesToWork,
                                YearsLicensed = ad.YearsLicensed,
                                LicenseStatus = ad.LicenseStatus,
                                IsDefensiveDriverCourse = ad.IsDefensiveDriverCourse,
                                IsGoodStudent = ad.IsGoodStudent,
                                DrivertrainingDate = ad.DrivertrainingDate,
                                FkMilitaryTypeId = ad.FkMilitaryTypeId.Value == null ? 0 : ad.FkMilitaryTypeId.Value,
                                FkWarehouseTypeId = ad.FkWarehouseTypeId.Value == null ? 0 : ad.FkWarehouseTypeId.Value,
                                IsMilitaryDiscount = ad.FkMilitaryTypeId == milatryTypeId ? true : false,
                                IsWareHouseDiscount = ad.FkWarehouseTypeId == warehouseTypeId ? true : false
                            }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        public AutoDriverModel AddAutoDriver(AutoDriverModel driver)
        {
            try
            {

                GeneralServices generalServices = new GeneralServices();
                CommonServices commonServices = new CommonServices();
                using (var db = new ArrowheadPOSEntities())
                {
                    long fkmaritalstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MARITALSTATUS.ToString(), driver.MaritalStatusCode.ToString());
                    long fkrelationId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.INSURED.ToString());
                    string stateCode = commonServices.state(driver.QuoteId);
                    long milatrytypeId = 0;
                    long warehouseTypeId = 0;
                    if (stateCode == "SC")
                    {
                        if (driver.IsMilitaryDiscount == true)
                        {
                            milatrytypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.ARMY.ToString());
                        }
                        else
                        {
                            milatrytypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.NONE.ToString());
                        }

                        if (driver.IsWareHouseDiscount == true)
                        {
                            warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.SAMSCLUB.ToString());
                        }
                        else
                        {
                            warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.NONE.ToString());
                        }
                    }

                    var _createDate = ConvertTo.GetEstTimeNow();
                    var groupTransactionId = Guid.NewGuid();
                    int _driverOrderId = db.Auto_Driver.Count(w => w.FkQuoteID == driver.QuoteId && w.IsDeleted == false) + 1;
                    Auto_Driver auto_Driver = new Auto_Driver();
                    auto_Driver.FkQuoteID = driver.QuoteId;
                    auto_Driver.FkStateID = driver.StateId;
                    auto_Driver.BirthDate = driver.Dob;
                    auto_Driver.FirstName = StringExtension.FirstCharToUpper(driver.FirstName);
                    auto_Driver.LastName = StringExtension.FirstCharToUpper(driver.LastName);
                    auto_Driver.Gender = driver.Gender;
                    auto_Driver.LicenseNo = driver.LicenseNo;
                    auto_Driver.FkMaritalStatusID = fkmaritalstatusId;
                    auto_Driver.FkHomeTypeID = driver.HomeTypeID;

                    auto_Driver.IsExcluded = driver.IsExcluded;
                    auto_Driver.FkDriverEducationID = driver.DriverEducationID;
                    auto_Driver.FkCreditScoreID = driver.CreditScoreID;
                    auto_Driver.DriverOrderID = _driverOrderId;
                    auto_Driver.FkRelationCdID = (_driverOrderId == 1 ? fkrelationId : driver.RelationCDId);
                    auto_Driver.SocialSecurity = driver.Socialsecurity;
                    auto_Driver.IsDeleted = false;
                    auto_Driver.IsSR22 = driver.Sr22;
                    auto_Driver.IsSR22A = driver.IsSr22A;
                    auto_Driver.SR22Reason = driver.SR22Reason;
                    auto_Driver.FKStateSR22AId = driver.StateSR22AId;
                    auto_Driver.FkStateSR22Id = driver.StateSR22Id;
                    auto_Driver.CreatedDate = _createDate;
                    auto_Driver.CreatedByID = driver.UserId;
                    auto_Driver.IsGoodStudent = driver.IsGoodStudent;
                    auto_Driver.DateFirstLicense = (driver.DateFirstLicense != null ? driver.DateFirstLicense.Value : driver.DateFirstLicense);
                    auto_Driver.Employer = driver.Employer;
                    auto_Driver.EmployerYears = driver.EmployerYears;
                    auto_Driver.MilesToWork = driver.MilesToWork;
                    auto_Driver.YearsLicensed = driver.YearsLicensed;
                    auto_Driver.LicenseStatus = driver.LicenseStatus;
                    auto_Driver.IsDefensiveDriverCourse = driver.IsDefensiveDriverCourse;
                    auto_Driver.FkDriverOccupationID = driver.DriverOccupationID;
                    auto_Driver.DrivertrainingDate = driver.DrivertrainingDate;
                    auto_Driver.IsWorkLossBenefit = driver.IsWorkLossBenefit;
                    //auto_Driver.FkMilitaryTypeId = milatrytypeId;
                    //auto_Driver.FkWarehouseTypeId = warehouseTypeId;
                    auto_Driver.FkMilitaryTypeId = milatrytypeId == 0 ? (long?)null : milatrytypeId;
                    auto_Driver.FkWarehouseTypeId = warehouseTypeId == 0 ? (long?)null : warehouseTypeId;
                    db.Auto_Driver.Add(auto_Driver);
                    db.SaveChanges();
                    driver.AutoDriverId = auto_Driver.ID;

                    InsertAuditHistory(Enums.AuditHistoryModule.AD, driver.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), groupTransactionId, driver.AutoDriverId.ToString(), driver.QuoteId, 0, driver.FirstName + " " + driver.LastName);

                    //var fkcurrentstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.DRIVER.ToString());

                    //db.Quotes.Where(w => w.ID == driver.QuoteId).Update(U => new Quote
                    //{
                    //    FKCurrentStepId = fkcurrentstatusId,
                    //});


                    //if (driver.AutoDriverViolations != null)
                    //{
                    //    driver.AutoDriverViolations = driver.AutoDriverViolations.Select(f => { f.DriverId = driver.AutoDriverId; return f; }).ToList();
                    //}
                }

            }
            catch (Exception ex)
            {
                InsertErrorLogHistory(driver.AgencyId, Enums.AuditHistoryModule.AD.ToString(), Enums.AuditHistoryOperation.I.ToString(), driver.QuoteId, ex.ToString());

                var err = ex.ToString();
            }
            return driver;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public AutoDriverModel UpdateAutoDriver(AutoDriverModel driver)
        {
            GeneralServices generalServices = new GeneralServices();
            CommonServices commonServices = new CommonServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _currentDate = ConvertTo.GetEstTimeNow();
                    var groupTransactionId = Guid.NewGuid();
                    long milatrytypeId = 0;
                    long warehouseTypeId = 0;
                    driver.MaritalStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MARITALSTATUS.ToString(), driver.MaritalStatusCode.ToString());
                    long fkrelationid = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.INSURED.ToString());
                    string stateCode = commonServices.state(driver.QuoteId);
                    if (stateCode == "SC")
                    {
                        if (driver.IsMilitaryDiscount == true)
                        {
                            milatrytypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.ARMY.ToString());
                        }
                        else
                        {
                            milatrytypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MILITARY.ToString(), Enums.MILITARY.NONE.ToString());
                        }

                        if (driver.IsWareHouseDiscount == true)
                        {
                            warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.SAMSCLUB.ToString());
                        }
                        else
                        {
                            warehouseTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.WAREHOUSE.ToString(), Enums.WAREHOUSE.NONE.ToString());
                        }
                    }
                   
                    int _driverorderid = (int)db.Auto_Driver.Where(w => w.ID == driver.AutoDriverId).Select(s => s.DriverOrderID).FirstOrDefault();
                    var driverDetails = DriverDetailsById(driver.AutoDriverId);
                    if (driverDetails.FirstName != driver.FirstName || driverDetails.LastName != driver.LastName || driverDetails.LicenseNo != driver.LicenseNo
                        || (driverDetails.BirthDate.HasValue && driver.Dob.HasValue && driverDetails.BirthDate.Value != driver.Dob.Value))
                    {
                        DeleteMVRDriverViolation(driver.AutoDriverId);
                        IsNoHitUpdate(driver.AutoDriverId);
                    }

                    db.Auto_Driver.Where(f => f.ID == driver.AutoDriverId).Update(U => new Auto_Driver
                    {
                        FkStateID = driver.StateId,
                        BirthDate = driver.Dob,
                        FirstName = driver.FirstName,
                        LastName = driver.LastName,
                        LicenseNo = driver.LicenseNo,
                        Gender = driver.Gender,
                        FkMaritalStatusID = driver.MaritalStatusID,
                        FkCreditScoreID = driver.CreditScoreID,
                        FkDriverEducationID = driver.DriverEducationID,
                        FkDriverOccupationID = driver.DriverOccupationID,
                        FkHomeTypeID = driver.HomeTypeID,
                        FkRelationCdID = (_driverorderid == 1 ? fkrelationid : driver.RelationCDId),
                        //DriverOrderID = driver.DriverOrderID,
                        IsExcluded = driver.IsExcluded,
                        IsSR22 = driver.Sr22,
                        IsSR22A = driver.IsSr22A,
                        //   IsNoHit=false,
                        ModifiedByID = driver.UserId,
                        ViolationPoints = 0, // Reset Violation Points 
                        ModifiedDate = _currentDate,
                        FKStateSR22AId = driver.StateSR22AId,
                        FkStateSR22Id = driver.StateSR22Id,
                        SR22Reason = driver.SR22Reason,
                        SocialSecurity = driver.Socialsecurity,
                        DateFirstLicense = (driver.DateFirstLicense != null ? driver.DateFirstLicense.Value : driver.DateFirstLicense),
                        Employer = driver.Employer,
                        EmployerYears = driver.EmployerYears,
                        MilesToWork = driver.MilesToWork,
                        YearsLicensed = driver.YearsLicensed,
                        LicenseStatus = driver.LicenseStatus,
                        IsDefensiveDriverCourse = driver.IsDefensiveDriverCourse,
                        IsGoodStudent = driver.IsGoodStudent,
                        DrivertrainingDate = driver.DrivertrainingDate,
                        IsWorkLossBenefit = driver.IsWorkLossBenefit,
                        //FkMilitaryTypeId = milatrytypeId,
                        //FkWarehouseTypeId = warehouseTypeId
                        FkMilitaryTypeId = milatrytypeId == 0 ? (long?)null : milatrytypeId,
                        FkWarehouseTypeId = warehouseTypeId == 0 ? (long?)null : warehouseTypeId
                    });
                    ManageDriverViolation(driver.AutoDriverViolations, driver.AutoDriverId, driver.UserId);
                    InsertAuditHistory(Enums.AuditHistoryModule.AD, driver.AgencyId, Enums.AuditHistoryOperation.U.ToString(), AuditHistoryRemarks.Update.ToString(), groupTransactionId, driver.AutoDriverId.ToString(), driver.QuoteId, 0, driver.FirstName + " " + driver.LastName);


                }
            }
            catch (Exception ex)
            {
                InsertErrorLogHistory(driver.AgencyId, Enums.AuditHistoryModule.AD.ToString(), Enums.AuditHistoryOperation.U.ToString(), driver.QuoteId, ex.ToString());

                ex.ToString();
            }
            return driver;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_lstDriverViolation"></param>
        /// <param name="driverId"></param>
        /// <param name="userId"></param>
        public void ManageDriverViolation(List<AutoDriverViolationModel> _lstDriverViolation, long driverId, long userId)
        {
            try
            {

                if (_lstDriverViolation != null)
                {
                    foreach (var _violation in _lstDriverViolation)
                    {

                        if (_violation.IsDeleted == false && _violation.AutoDriverViolationId == 0)
                        {
                            AutoDriverViolationModel autoDriverViolationModel = new AutoDriverViolationModel
                            {
                                DriverId = driverId,
                                AutoViolationId = _violation.AutoViolationId,
                                ViolationDate = _violation.ViolationDate,
                                Points = _violation.Points,
                                Note = _violation.Note,
                                BIamount = _violation.BIamount,
                                PDamount = _violation.PDamount,
                                CreatedById = userId
                            };
                            autoDriverViolationModel = SaveAutoViolation(autoDriverViolationModel, userId);
                            _violation.AutoDriverViolationId = autoDriverViolationModel.AutoDriverViolationId;



                        }
                        else if (_violation.IsDeleted == true)
                        {
                            bool result = DeleteDriverViolation(_violation.AutoDriverViolationId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public AutoDriverViolationModel SaveAutoViolation(AutoDriverViolationModel model, long userId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var manageDate = ConvertTo.GetEstTimeNow();
                    if (model.AutoDriverViolationId > 0)
                    {

                    }
                    else
                    {

                        Auto_DriverViolation auto_DriverViolation = new Auto_DriverViolation();
                        auto_DriverViolation.FkDriverId = model.DriverId;
                        auto_DriverViolation.FKAutoViolationId = model.AutoViolationId;
                        auto_DriverViolation.ViolationDate = model.ViolationDate;
                        auto_DriverViolation.Points = model.Points;
                        auto_DriverViolation.Note = model.Note;
                        auto_DriverViolation.BIAmount = model.BIamount;
                        auto_DriverViolation.PDAmount = model.PDamount;
                        auto_DriverViolation.CreatedDate = manageDate;
                        auto_DriverViolation.CreatedById = userId;
                        auto_DriverViolation.ModifiedDate = manageDate;
                        auto_DriverViolation.ModifiedById = userId;
                        db.Auto_DriverViolation.Add(auto_DriverViolation);
                        db.SaveChanges();
                        model.AutoDriverViolationId = auto_DriverViolation.ID;

                        InsertAuditHistory(Enums.AuditHistoryModule.ADV, model.AgencyId, Enums.AuditHistoryOperation.I.ToString(), AuditHistoryRemarks.Insert.ToString(), Guid.NewGuid(), model.AutoDriverViolationId.ToString(), model.QuoteId, 0, null);

                    }
                }
                catch (Exception ex)
                {
                    InsertErrorLogHistory(model.AgencyId, Enums.AuditHistoryModule.ADV.ToString(), Enums.AuditHistoryOperation.I.ToString(), model.QuoteId, ex.ToString());

                    ex.ToString();
                }
                return model;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_driverViolationID"></param>
        /// <returns></returns>
        public bool DeleteDriverViolation(long _driverViolationID)
        {
            bool result = false;
            using (var db = new ArrowheadPOSEntities())
            {
                db.Auto_DriverViolation.Where(w => w.ID == _driverViolationID).Delete();
                result = true;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        public void DeleteMVRDriverViolation(long Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.Auto_DriverViolation.Where(w => w.IsMVR == true && w.FkDriverId == Id).Delete();

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public Auto_Driver DriverDetailsById(long driverId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Auto_Driver.Where(w => w.ID == driverId).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get Rated Driver Count 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public int GetRatedDriverCount(long QuoteID)
        {
            int ratedDriverCount = 0;
            using (var db = new Arrowhead.POS.Model.ArrowheadPOSEntities())
            {
                ratedDriverCount = (db.Auto_Driver.Where(x => x.FkQuoteID == QuoteID && x.IsDeleted == false && x.IsExcluded == false).Count());
            }
            return ratedDriverCount;
        }

        /// <summary>
        /// Get Rated Driver List 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public List<long> GetRatedDriverList(long QuoteID)
        {
            using (var db = new Arrowhead.POS.Model.ArrowheadPOSEntities())
            {
                var _ratedDriverList = (db.Auto_Driver.Where(x => x.FkQuoteID == QuoteID && x.IsDeleted == false && x.IsExcluded == false).Select(x => x.ID).ToList());
                return _ratedDriverList;
            }

            return null;
        }
        /// <summary>
        /// Delete MVR Violations If Exist and Insert New Violations
        /// </summary>
        /// <param name="QuoteId"></param>
        public void DeleteMVRViolations(long QuoteId)
        {
            using (var db = new Arrowhead.POS.Model.ArrowheadPOSEntities())
            {
                (from ad in db.Auto_Driver
                 join adv in db.Auto_DriverViolation on ad.ID equals adv.FkDriverId
                 where adv.IsMVR == true && ad.FkQuoteID == QuoteId
                 select adv).Delete();

                //var _driverList = db.Auto_Driver.Where(x => x.FkQuoteID == QuoteId).ToList();
                //foreach(var items in _driverList)
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        public void IsNoHitUpdate(long Id)
        {

            using (var db = new Arrowhead.POS.Model.ArrowheadPOSEntities())
            {
                db.Auto_Driver.Where(w => w.ID == Id).Update(u => new Auto_Driver
                {
                    IsNoHit = false,
                    ModifiedDate = ConvertTo.GetEstTimeNow()

                });
            }
        }

        /// <summary>
        /// Reset Driver Order 
        /// </summary>
        /// <param name="QuoteId"></param>
        public void ResetDriverOrder(long QuoteId)
        {
            int driverOrderCount = 1;
            var _driverList = GetDriverByQuoteID(QuoteId);
            using (var db = new ArrowheadPOSEntities())
            {
                foreach (var items in _driverList)
                {
                    db.Auto_Driver.Where(x => x.ID == items.AutoDriverId).Update(u => new Auto_Driver()
                    {
                        DriverOrderID = driverOrderCount
                    });
                    driverOrderCount++;
                }
            }
        }
    }
}
