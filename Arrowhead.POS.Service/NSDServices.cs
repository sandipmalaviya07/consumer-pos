﻿using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arrowhead.POS.Model;
using System.Threading.Tasks;
using Arrowhead.POS.Core;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace Arrowhead.POS.Service
{
    public class NSDServices
    {

        public List<AutoNSDDetailsListMode> GetNsdPlanDetailsByQuoteID(long _quoteId, long agencyId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                int _totalCars = db.Auto_Vehicle.Where(w => w.FkQuoteID == _quoteId && w.IsDeleted == false).Count();
                int _noOfPolicyTerm = db.Quotes.Where(w => w.ID == _quoteId).Select(S => S.PolicyTerm).FirstOrDefault();

                List<AutoNSDDetailsListMode> _resp = new List<AutoNSDDetailsListMode>();
                if (agencyId != 0)
                {
                    _resp = (from nsd in db.NSDPlans
                             join np in db.NSD_PlanDetails on nsd.ID equals np.FKNsdPlansId
                             join nv in db.NSD_PlanValues on np.ID equals nv.FKNSDPlanDetailsId
                             where nv.Key1 == _totalCars.ToString() + "CAR" && nv.Key2 == _noOfPolicyTerm.ToString() + "MONTH" && nv.FkAgencyId == agencyId
                             select new AutoNSDDetailsListMode
                             {
                                 IsSelected = false,
                                 NSDPlanId = nsd.ID,
                                 NSDPlanDetailID = np.ID,
                                 PlanName = nv.NSD_PlanDetails.Description,
                                 price = nv.Value1,
                                 PDFUrl = ConstantVariables.ArrowheadBlobURL + np.PreviewPDFURL,
                                 NSDPlanValueId = nv.ID
                             }).ToList();
                    if (_resp.Count == 0)
                    {
                        _resp = (from nsd in db.NSDPlans
                                 join np in db.NSD_PlanDetails on nsd.ID equals np.FKNsdPlansId
                                 join nv in db.NSD_PlanValues on np.ID equals nv.FKNSDPlanDetailsId
                                 where nv.Key1 == _totalCars.ToString() + "CAR" && nv.Key2 == _noOfPolicyTerm.ToString() + "MONTH" && nv.FkAgencyId == null
                                 select new AutoNSDDetailsListMode
                                 {
                                     IsSelected = false,
                                     NSDPlanId = nsd.ID,
                                     NSDPlanDetailID = np.ID,
                                     PlanName = nv.NSD_PlanDetails.Description,
                                     price = nv.Value1,
                                     PDFUrl = ConstantVariables.ArrowheadBlobURL + np.PreviewPDFURL,
                                     NSDPlanValueId = nv.ID
                                 }).ToList();
                    }
                }

                return _resp;
            }
        }

        /// <summary>
        /// Get Agency Plan By Agency Id
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public int GetAgencyPlan(long agencyId)
        {
            int payPlanId = 0;
            using (var db = new ArrowheadPOSEntities())
            {
                payPlanId = db.Agencies.Where(x => x.ID == agencyId).Select(x => x.FKDefaultNSDPlanId).FirstOrDefault()??0;
                return payPlanId;
            }
        }

        /// <summary>
        /// Get NSD Plans 
        /// </summary>
        /// <returns></returns>




        //public AutoNsdDetailModel SaveAutoQuoteNsdPlan(AutoNsdDetailModel model, long userId)
        //{

        //    GeneralServices generalServices = new GeneralServices();
        //    DateTime manageDate = ConvertTo.GetEstTimeNow();
        //    using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
        //    {
        //        try
        //        {
        //            var groupTransactionId = Guid.NewGuid();
        //            var _lstPlans = model.AutoQuoteNsdPlans;
        //            var _lstVehicle = model.AutoQuoteNsdSummary;
        //            DeleteNsd(_lstPlans[0].QuoteId);
        //            decimal? _totalNsdFees = 0;

        //            long _quoteId = _lstPlans[0].QuoteId;
        //            foreach (var _dr in _lstPlans)
        //            {
        //                if (_dr.IsEnable == true)
        //                {
        //                    QuoteNSDPlan autoQuoteNsdplans = new QuoteNSDPlan
        //                    {
        //                        FKNsdId = _dr.NsdPlanId,
        //                        FKQuoteId = _dr.QuoteId,
        //                        PlanId = _dr.PlanId,
        //                        Price = _dr.Price,
        //                        CreatedById = userId,
        //                        CreatedDate = manageDate,
        //                        BeneficiaryRelation = string.IsNullOrEmpty(_dr.beneficiaryRelation) ? null : _dr.beneficiaryRelation,
        //                        BeneficiaryName = string.IsNullOrEmpty(_dr.beneficiaryName) ? null : _dr.beneficiaryName,
        //                    };
        //                    db.QuoteNSDPlans.Add(autoQuoteNsdplans);
        //                    db.SaveChanges();
        //                    _dr.AutoQuoteNsdPlanId = autoQuoteNsdplans.ID;
        //                    _totalNsdFees += _dr.Price;
        //                }
        //                // InsertAuditHistory(Enums.AuditHistoryModule.AQN, Enums.AuditHistoryOperation.I, AuditHistoryRemarks.Insert.ToString(), groupTransactionId, _dr.AutoQuoteNsdPlanId.ToString(), _dr.QuoteId, 0, null);

        //            }

        //            if (_lstPlans.Any(x => x.IsEnable == true))
        //            {
        //                long quoteID = model.AutoQuoteNsdPlans[0].QuoteId;
        //            }

        //            if (_lstVehicle != null)
        //            {
        //                string _ids = string.Empty;

        //                foreach (var _vr in _lstVehicle)
        //                {
        //                    if (_vr.VehicleId != null)
        //                    {
        //                        if (_vr.IsChecked == true)
        //                        {
        //                            if (_ids != null && _ids != "")
        //                                _ids = _ids + "," + Convert.ToString(_vr.VehicleId);
        //                            else
        //                                _ids = Convert.ToString(_vr.VehicleId);
        //                        }
        //                    }
        //                }
        //                if (_ids != null && _ids != "")
        //                {
        //                    //QuoteNsdsummary autoQuoteNsdsummary = new QuoteNsdsummary
        //                    //{
        //                    //    FkquoteId = Convert.ToInt64(_lstVehicle[0].QuoteId),
        //                    //    FkvehiclesIds = _ids
        //                    //};
        //                    //db.QuoteNsdsummary.Add(autoQuoteNsdsummary);
        //                    //db.SaveChanges();
        //                }
        //                //InsertAuditHistory(Enums.AuditHistoryModule.AQN, Enums.AuditHistoryOperation.I, AuditHistoryRemarks.Insert.ToString(), groupTransactionId, null, _lstVehicle[0].QuoteId, 0, null);
        //            }


        //            //bool isAutoQuote = db.Quotes.Any(f => f.Id == _quoteId && f.FkInsuranceTypeId == _insuranceTypeId);

        //            // if (isAutoQuote == true)
        //            // {
        //            var _rateExists = db.Auto_RateHistory.Where(w => w.FKQuoteID == _lstPlans[0].QuoteId).OrderByDescending(o => o.CreatedDate).Select(s => s.RateTransactionID).FirstOrDefault();
        //            if (_rateExists != null)
        //            {
        //                db.Auto_RateHistory.Where(w => w.FKQuoteID == _lstPlans[0].QuoteId).Update(up => new Auto_RateHistory
        //                {
        //                    NSDFee = (decimal)_totalNsdFees
        //                });

        //                //InsertAuditHistory(Enums.AuditHistoryModule.ARH, Enums.AuditHistoryOperation.U, AuditHistoryRemarks.Update.ToString(), groupTransactionId, null, _lstVehicle[0].QuoteId, 0, null);
        //            }
        //            //var isEndorsement = (db.Quotes.Any(f => f.Id == _quoteId && f.FkQuoteType.Code == Enums.QuoteType.ENDORSEMENT.ToString()));
        //            //if (isEndorsement == true)
        //            //{
        //            //    db.Auto_.Where(f => f.FkQuoteId == _quoteId).Update(u => new PolicyHistory
        //            //    {
        //            //        Nsdfee = (decimal)_totalNsdFees
        //            //    });
        //            //}
        //            // }
        //            //else
        //            //{
        //            //db.PolicyHistory.Where(f => f.FkQuoteId == _quoteId).Update(u => new PolicyHistory
        //            //{
        //            //    Nsdfee = (decimal)_totalNsdFees
        //            //});
        //            //}

        //            model.AutoQuoteNsdPlans = _lstPlans;
        //            model.AutoQuoteNsdSummary = _lstVehicle;
        //            db.Quotes.Where(w => w.ID == _quoteId).Update(u => new Quote
        //            {
        //                ModifiedDate = ConvertTo.GetEstTimeNow()
        //            });
        //        }
        //        catch (Exception ex)
        //        {
        //            ex.ToString();
        //        }
        //        return model;

        //    }
        //}

        public decimal SaveAutoQuoteNsdPlan(long quoteId, int planValueId,long userId)
        {
            decimal nsdDecimal = 0;
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                try
                {
                    var _createDate = ConvertTo.GetEstTimeNow();
                    var vehicleCount = (db.Auto_Vehicle.Where(x => x.FkQuoteID == quoteId && x.IsDeleted == false).Count());
                    if (planValueId == 0)
                    {
                        db.QuoteNSDPlans.Where(x => x.FKQuoteId == quoteId && x.IsSuccess != true).Delete();
                    }
                    if (planValueId > 0)
                    {
                        var planDetail = (from nsd in db.NSDPlans
                                          join np in db.NSD_PlanDetails on nsd.ID equals np.FKNsdPlansId
                                          join nv in db.NSD_PlanValues on np.ID equals nv.FKNSDPlanDetailsId
                                          where nv.ID == planValueId && nv.Key1 == vehicleCount.ToString() + "CAR"
                                          select new { nsd, np, nv }).FirstOrDefault();

                        DeleteNsd(quoteId);

                        QuoteNSDPlan quoteNSDPlan = new QuoteNSDPlan
                        {
                            FKQuoteId = quoteId,
                            FKNsdId = planDetail.nsd.ID,
                            PlanId = planDetail.np.PlanNo,
                            Price = planDetail.nv.Value1,
                            CreatedById = userId,
                            CreatedDate = _createDate,
                            BeneficiaryRelation = string.Empty,
                            BeneficiaryName = string.Empty,
                            NetPremiumValue = planDetail.nv.NetPremiumValue,
                            AgentGross = planDetail.nv.AgentGross,
                            RFCost = planDetail.nv.RFCost
                        };
                        db.QuoteNSDPlans.Add(quoteNSDPlan);
                        db.SaveChanges();
                        nsdDecimal = planDetail.nv.Value1;
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return nsdDecimal;
        }

        public bool DeleteNsd(long quoteId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                db.QuoteNSDPlans.Where(x => x.FKQuoteId == quoteId).Delete();
                return true;
            }
        }

        /// <summary>
        /// NSD Process
        /// </summary>
        /// <param name="_quoteId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public NSDContractModel NSDProcess(long _quoteId, long userId)
        {
            NSDContractModel _nsdContractModel = new NSDContractModel();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                GeneralServices generalServices = new GeneralServices();

                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    var quote = (from q in db.Quotes
                                 join c in db.Customers on q.FkCustomerID equals c.ID
                                 join s in db.States on q.FkStateId equals s.ID
                                 where q.ID == _quoteId
                                 select new { q, c, s }).FirstOrDefault();
                    long paymentStatusSuccessId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    long paymentStatusInitiatedId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                    bool isPaid = (db.QuotePayments.Where(w => w.FKQuoteID == _quoteId && (w.FKPaymentStatusID == paymentStatusSuccessId || w.FKPaymentStatusID == paymentStatusInitiatedId)).Count() > 0 ? true : false);

                    var _officeNsdDetails = GetNsddetailsByOffice(quote.q.FkOfficeId);

                    if (isPaid)
                    {
                        var vehicleList = db.Auto_Vehicle.Where(x => x.FkQuoteID == _quoteId).ToList();
                        long customerID = quote.q.FkCustomerID; // db.Quote.Where(x => x.Id == _quoteId).Select(x => x.FkCustomerId).FirstOrDefault();

                        NSD _NSD = new NSD
                        {
                            workingContract = new WorkingContract(),
                            policyInfo = new PolicyInfo(),
                            vehicleInfo = new List<VehicleInfo>(),
                            productInfo = new ProductInfo(),
                            beneficiaryInfo = new BeneficiaryInfo(),

                            Password = _officeNsdDetails.APIPassword,
                            Action = "Bind",
                            Test = _officeNsdDetails.IsTest == true ? "true" : "false"
                        };
                        _NSD.workingContract.Master_Id = "";
                        _NSD.workingContract.Member_No = "";
                        _NSD.policyInfo.Ag_No = _officeNsdDetails.AgencyNo;
                        _NSD.policyInfo.Fname = quote.c.FirstName;
                        _NSD.policyInfo.MI = "";
                        _NSD.policyInfo.Lname = quote.c.LastName;
                        _NSD.policyInfo.Address = quote.q.Address1 + " " + quote.q.Address2;
                        _NSD.policyInfo.City = quote.q.City;
                        _NSD.policyInfo.State = quote.s.Code;
                        _NSD.policyInfo.Zip = quote.q.ZipCode;
                        _NSD.policyInfo.Phone = quote.c.ContactNo.ToPhoneNumber();
                        _NSD.policyInfo.Email = quote.c.EmailID;
                        _NSD.policyInfo.Work_Phone = "";
                        _NSD.policyInfo.Payment_Type = "";

                        foreach (var item in vehicleList)
                        {
                            VehicleInfo _vehicle = new VehicleInfo
                            {
                                Year = Convert.ToString(item.Year),
                                Make = Convert.ToString(item.Make),
                                Model = Convert.ToString(item.Model),
                                VIN = Convert.ToString(item.VIN),
                                Odometer = "46"
                            };
                            _vehicle.Year = Convert.ToString(item.Year);
                            _NSD.vehicleInfo.Add(_vehicle);
                        }

                        var _lstPlans = (from aq in db.QuoteNSDPlans
                                         join qd in db.NSDPlans on aq.FKNsdId equals qd.ID
                                         join npd in db.NSD_PlanDetails on qd.ID equals npd.FKNsdPlansId
                                         where aq.FKQuoteId == _quoteId
                                         select new { aq, qd, npd }).FirstOrDefault();

                        var _NetpremiumPrice = db.NSD_PlanValues.Where(w => w.FKNSDPlanDetailsId == _lstPlans.npd.ID && w.Key1 == vehicleList.Count.ToString() + "CAR").Select(s => s.NetPremiumValue).FirstOrDefault();
                        // foreach (var _dr in _lstPlans)
                        //{
                        if (!string.IsNullOrEmpty(_lstPlans.qd.ProductNumber))
                        {
                            _NSD.productInfo.Pr_No = _lstPlans.qd.ProductNumber;
                            _NSD.productInfo.Plan_No = _lstPlans.aq.PlanId;
                            _NSD.productInfo.Eff_Dt = Convert.ToString(Convert.ToDateTime(quote.q.EffectiveDate.ToString()).ToString("MM/dd/yyyy"));
                            _NSD.productInfo.Exp_Dt = Convert.ToString(Convert.ToDateTime(quote.q.ExpirationDate.ToString()).ToString("MM/dd/yyyy"));
                            //_NSD.productInfo.Eff_Dt = ConvertTo.GetEstTimeNow().ToString("MM/dd/yyyy");
                            //_NSD.productInfo.Exp_Dt = ConvertTo.GetEstTimeNow().AddMonths(6).ToString("MM/dd/yyyy");
                            _NSD.productInfo.NetPremium = Convert.ToString(_NetpremiumPrice);
                            _NSD.productInfo.GrossPremium = Convert.ToString(_lstPlans.aq.Price);

                            if (_lstPlans.qd.Code == Enums.DOCUMENTTYPE.AD.ToString() || _lstPlans.qd.Code == Enums.DOCUMENTTYPE.HIP.ToString())
                            {
                                //if (_dr.fKDriverId != null && _dr.fKDriverId != 0)
                                //{
                                //    var item = db.Auto_Driver.FirstOrDefault(w => w.ID == _dr.fKDriverId);
                                //    var _relationship = db.GeneralStatusTypes.Where(w => w.ID == item.FkRelationCdID).Select(s => new { s.Code, s.Name }).FirstOrDefault();
                                //    BeneficiaryInfo _driver = new BeneficiaryInfo
                                //    {

                                //        Beneficiary_First_Name = item.FirstName,
                                //        Beneficiary_Last_Name = item.LastName,
                                //        Beneficiary_Middle_Initial = "",
                                //        Beneficiary_Address = "",
                                //        Beneficiary_City = "",
                                //        //Beneficiary_State = item.FkState.ToString(),
                                //        Beneficiary_Zip = "",
                                //        Beneficiary_Relation = (item.FkRelationCdID != null && _relationship != null ? (_relationship.Code == "INSURED" ? item.FirstName + " " + item.LastName : _relationship.Name) : "")
                                //    };
                                //    _NSD.beneficiaryInfo = _driver;
                                //}
                                //else
                                //{
                                //    BeneficiaryInfo _driver = new BeneficiaryInfo
                                //    {
                                //        Beneficiary_First_Name = _dr.beneficiaryName,
                                //        Beneficiary_Last_Name = "",
                                //        Beneficiary_Middle_Initial = "",
                                //        Beneficiary_Address = "",
                                //        Beneficiary_City = "",
                                //        Beneficiary_State = "",
                                //        Beneficiary_Zip = "",
                                //        Beneficiary_Relation =
                                //        (!string.IsNullOrWhiteSpace(_dr.beneficiaryRelation) ? (_dr.beneficiaryRelation.ToUpper() == "INSURED" ? _dr.beneficiaryName : _dr.beneficiaryRelation) : "")
                                //    };
                                //    _NSD.beneficiaryInfo = _driver;
                                //}
                            }

                            try
                            {
                                var xmlserializer = new XmlSerializer(typeof(NSD));
                                var stringWriter = new StringWriter();
                                using (var writer = XmlWriter.Create(stringWriter))
                                {
                                    xmlserializer.Serialize(writer, _NSD);
                                    xmlDoc.InnerXml = stringWriter.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("An error occurred", ex);
                            }

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlDoc.OuterXml);
                            HttpWebRequest request;
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            request = (HttpWebRequest)WebRequest.Create(_officeNsdDetails.APIURL);

                            request.Method = "POST";
                            request.ContentLength = bytes.Length;
                            request.ContentType = "text/xml";
                            request.PreAuthenticate = true;

                            using (Stream requestStream = request.GetRequestStream())
                            {
                                requestStream.Write(bytes, 0, bytes.Length);
                            }

                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            Stream resStream = response.GetResponseStream();
                            StreamReader rdStreamRdr = new StreamReader(resStream);
                            string responseText = rdStreamRdr.ReadToEnd();
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(responseText);

                            BlobFileUtility blobFileUtility = new BlobFileUtility();
                            string CloudFolderName = string.Empty;
                            string xmlFileName = ConvertTo.GetEstTimeNow().ToString("yyyyMMddHHmmssfff");
                            try
                            {

                                CloudFolderName = ConstantVariables.CustomerNSDXMLFolder;

                                //Upload Req XML
                                byte[] Reqbytes = Encoding.ASCII.GetBytes(xmlDoc.OuterXml);
                                string xmlReqFileName = _quoteId + "/" + Regex.Replace(_lstPlans.qd.Code.ToLower(), @"[^0-9a-zA-Z]+", "") + "-" + xmlFileName + "-Req" + ".XML";
                                blobFileUtility.Save(CloudFolderName, xmlReqFileName, Reqbytes);

                                //Upload Res XML
                                byte[] Resbytes = Encoding.ASCII.GetBytes(responseText);
                                string xmlResFileName = _quoteId + "/" + Regex.Replace(_lstPlans.qd.Code.ToLower(), @"[^0-9a-zA-Z]+", "") + "-" + xmlFileName + "-Res" + ".XML";
                                blobFileUtility.Save(CloudFolderName, xmlResFileName, Resbytes);

                                //  var quoteDDetails = db.Quotes.Where(w => w.ID == _quoteId).FirstOrDefault();

                                string statusCode = doc.SelectSingleNode("/NSD/Status/Status_Code").InnerText;
                                string statusMessage = doc.SelectSingleNode("/NSD/Status/Status_Message").InnerText;

                                // Set Response in NSD Contract Model
                                _nsdContractModel.QuoteId = _quoteId;
                                _nsdContractModel.AgencyId = _officeNsdDetails.AgencyNo;
                                _nsdContractModel.StatusCode = statusCode;
                                _nsdContractModel.StatusMessage = statusMessage;

                                if (statusCode == "200")
                                {
                                    string pdfBase64 = doc.SelectSingleNode("/NSD/Document/PdfBase64").InnerText;
                                    string fileName = Regex.Replace(_lstPlans.qd.Code.ToLower(), @"[^0-9a-zA-Z]+", "") + "-" + ConvertTo.GetEstTimeNow().ToString("yyyyMMddHHmmssfff") + ".pdf";
                                    byte[] pdfBytes = Convert.FromBase64String(pdfBase64);

                                    CloudFolderName = ConstantVariables.CustomerDocContainer;
                                    blobFileUtility.Save(CloudFolderName, _quoteId + "/" + fileName, pdfBytes);

                                    string _docType = Enums.DOCUMENTTYPE.NSD.ToString();
                                    if (_lstPlans.qd.Code == Enums.DOCUMENTTYPE.TOWBUSTERS.ToString())
                                    {
                                        _docType = Enums.DOCUMENTTYPE.TOWBUSTERSCONTRACT.ToString();
                                    }
                                    //else if (_dr.NsdPlanCode == Enums.DOCUMENTTYPE.TOWINGANDRENTAL.ToString())
                                    //{
                                    //    _docType = Enums.DOCUMENTTYPE.TOWINGANDRENTALCONTRACT.ToString();
                                    //}
                                    //else if (_dr.NsdPlanCode == Enums.DOCUMENTTYPE.AD.ToString())
                                    //{
                                    //    _docType = Enums.DOCUMENTTYPE.ADANDDCONTRACT.ToString();
                                    //}
                                    //else if (_dr.NsdPlanCode == Enums.DOCUMENTTYPE.HIP.ToString())
                                    //{
                                    //    _docType = Enums.DOCUMENTTYPE.HIPCONTRACT.ToString();
                                    //}

                                    //CustomerDocumentModel _model = new CustomerDocumentModel
                                    //{
                                    //    QuoteId = _quoteId,
                                    //    DocumentName = fileName,
                                    //    CreatedById = userId,
                                    //    DocPath = CloudFolderName + "/" + customerID + "/" + fileName,
                                    //    CustomerId = customerID,
                                    //    FileType = Enums.FILETYPE.PDF.ToString(),
                                    //    DocumentType = _docType,
                                    //    EntityCode = Enums.EntityCode.OTHER.ToString(),
                                    //    EntityId = null
                                    //};

                                    ////Add NSD Document In Document Library
                                    //QuoteDocumentService quoteDocumentService = new QuoteDocumentService(_request);
                                    //quoteDocumentService.CustomerDocumentInsert(_model);

                                    try
                                    {
                                        string MemberNo = doc.SelectSingleNode("/NSD/Document/Member_No").InnerText;
                                        string MasterID = doc.SelectSingleNode("/NSD/Document/Master_Id").InnerText;

                                        //NSD Purchase Details
                                        db.QuoteNSDPlans.Where(w => w.FKNsdId == _lstPlans.aq.FKNsdId && w.FKQuoteId == _quoteId).Update(up => new QuoteNSDPlan
                                        {
                                            MasterID = MasterID,
                                            MemberNo = MemberNo,
                                            EffectiveDate = quote.q.EffectiveDate,
                                            ExpiryDate = quote.q.ExpirationDate,
                                            PdfUrl = CloudFolderName + "/" + _quoteId + "/" + fileName,
                                            XMLUrl = Core.ConstantVariables.CustomerNSDXMLFolder + "/" + _quoteId + "/" + Regex.Replace(_lstPlans.qd.Code.ToLower(), @"[^0-9a-zA-Z]+", "") + "-" + xmlFileName + "-Res" + ".XML",
                                            IsSuccess = true,
                                            StatusCode = statusCode,
                                            StatusMessage = statusMessage
                                        });

                                    }
                                    catch (Exception ex) { }
                                }
                                else
                                {
                                    //_nsdContractModel.StatusMessage = _StatusMessage;
                                    db.QuoteNSDPlans.Where(w => w.FKNsdId == _lstPlans.aq.FKNsdId && w.FKQuoteId == _quoteId).Update(up => new QuoteNSDPlan
                                    {
                                        MasterID = string.Empty,
                                        MemberNo = string.Empty,
                                        EffectiveDate = quote.q.EffectiveDate,
                                        ExpiryDate = quote.q.ExpirationDate,
                                        PdfUrl = "",
                                        XMLUrl = Core.ConstantVariables.CustomerNSDXMLFolder + "/" + _quoteId + "/" + Regex.Replace(_lstPlans.qd.Code.ToLower(), @"[^0-9a-zA-Z]+", "") + "-" + xmlFileName + "-Res" + ".XML",
                                        IsSuccess = false,
                                        StatusCode = statusCode,
                                        StatusMessage = statusMessage
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        // }
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return _nsdContractModel;
        }

        public OfficeNSDDetail GetNsddetailsByOffice(int officeId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                return db.OfficeNSDDetails.Where(w => w.ISDeleted == false).FirstOrDefault();
            }
        }

        /// <summary>
        /// Verify NSD Available for Agency
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public bool IsNSDAvailableByAgencyId(long agencyId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (db.Agencies.Where(w => w.ID == agencyId).Select(x => x.IsNSDAvailable).FirstOrDefault());
            }
        }

        /// <summary>
        /// NSD Cancel Contract
        /// </summary>
        /// <param name="_quoteId"></param>
        public void NSDCancelContractProcess(long _quoteId)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                GeneralServices generalServices = new GeneralServices();
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    var quote = (from q in db.Quotes
                                 join c in db.Customers on q.FkCustomerID equals c.ID
                                 join s in db.States on q.FkStateId equals s.ID
                                 where q.ID == _quoteId
                                 select new { q, c, s }).FirstOrDefault();
                    //long paymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    //bool _isnotPaid = (db.QuotePayments.Where(w => w.FKQuoteID == _quoteId && w.FKPaymentStatusID == paymentStatusId).Any());

                    //if (!_isnotPaid)
                    //{
                    var _officeNsdDetails = GetNsddetailsByOffice(quote.q.FkOfficeId);
                    var _OldDetails = db.QuoteNSDPlans.Where(w => w.FKQuoteId == _quoteId && w.IsSuccess == true).ToList();
                    foreach (var item in _OldDetails)
                    {
                        NSDCancelContract _NSD = new NSDCancelContract
                        {
                            Password = _officeNsdDetails.APIPassword
                        };
                        NSDCancelPolicyInfo nSDCancelPolicyInfo = new NSDCancelPolicyInfo
                        {
                            Master_Id = item.MasterID,
                            Cancel_Date = DateTime.UtcNow,
                            Member_No = item.MemberNo
                        };
                        _NSD.policyInfo = nSDCancelPolicyInfo;

                        try
                        {
                            var xmlserializer = new XmlSerializer(typeof(NSDCancelContract));
                            var stringWriter = new StringWriter();
                            using (var writer = XmlWriter.Create(stringWriter))
                            {
                                xmlserializer.Serialize(writer, _NSD);
                                xmlDoc.InnerXml = stringWriter.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("An error occurred", ex);
                        }

                        byte[] bytes = Encoding.UTF8.GetBytes(xmlDoc.OuterXml);
                        HttpWebRequest request;
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                        request = (HttpWebRequest)WebRequest.Create(_officeNsdDetails.APIURL.Replace("contractImport.aspx", "contractCancel.aspx"));

                        request.Method = "POST";
                        request.ContentLength = bytes.Length;
                        request.ContentType = "text/xml";
                        request.PreAuthenticate = true;

                        using (Stream requestStream = request.GetRequestStream())
                        {
                            requestStream.Write(bytes, 0, bytes.Length);
                        }

                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream resStream = response.GetResponseStream();
                        StreamReader rdStreamRdr = new StreamReader(resStream);
                        string responseText = rdStreamRdr.ReadToEnd();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(responseText);

                        string _StatusCode = doc.SelectSingleNode("/NSD/Status/Status_Code").InnerText;
                        string _StatusMessage = doc.SelectSingleNode("/NSD/Status/Status_Message").InnerText;
                        if (!string.IsNullOrEmpty(_StatusCode) && _StatusCode == "200")
                        {
                            db.QuoteNSDPlans.Where(w => w.ID == item.ID).Update(u => new QuoteNSDPlan { IsSuccess = false, MemberNo = string.Empty, MasterID = string.Empty, EffectiveDate = null, ExpiryDate = null, PdfUrl = null, XMLUrl = null, StatusCode = string.Empty, StatusMessage = string.Empty });
                        }
                    }
                    //}
                }

            }
            catch (Exception ex)
            { }
        }

    }
}
