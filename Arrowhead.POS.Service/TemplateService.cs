﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Service
{
    public class TemplateService
    {
        public TemplateModel GetTemplateDetails(string _Code)
        {
            if (string.IsNullOrEmpty(_Code))
            {
                throw new ArgumentException("Code can not be blank", nameof(_Code));
            }
            System.Net.WebClient client = new System.Net.WebClient();
            using (var db = new ArrowheadPOSEntities())
            {
                //if (db.Templates.Where(w => w.Code == _Code && w.IsDeleted == false).Count() == 0)
                //{

                    var _temp = db.Templates.Where(w => w.Code == _Code).Select(s => new TemplateModel
                                {
                                    TemplateId = s.ID,
                                    EmailTemplate = s.EmailTemplate, //(!string.IsNullOrEmpty(s.EmailTemplate) ? client.DownloadString(ConstantVariables.ArrowheadBlobURL + s.EmailTemplate) : ""),
                                    IsEmailActive = s.IsEmailActive,
                                    IsSMSActive = s.IsSMSActive,
                                    SMSTemplate = s.SMSTemplate,
                                    Code = s.Code,
                                    Isactive = s.IsActive,
                                    Name = s.Name,
                                    DynamicField = s.DynamicField,
                                    EmailSubject = s.EmailSubjectLine,
                                    FKTemplateTypeId = (int)s.FKTemplateTypeId
                                }).FirstOrDefault();
                _temp.EmailTemplate = !string.IsNullOrEmpty(_temp.EmailTemplate) ? client.DownloadString(ConstantVariables.ArrowheadBlobURL + _temp.EmailTemplate) : "";
                    return _temp;
            }

            return null;
        }

        public TemplateModel TemplateFieldMapping(string templateCode, long customerId, long quoteId)
        {
            TemplateModel _template = new TemplateModel();
            TemplateService templateService = new TemplateService();
            QuoteModel quoteModel = null;
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            System.Net.WebClient client = new System.Net.WebClient();

            _template = GetTemplateDetails(templateCode);
            if (_template != null)
            {
                if (quoteId != 0)
                {
                    quoteModel = templateService.getDynamicTemplateFieldByQuoteId(quoteId);
                    if (!string.IsNullOrEmpty(_template.EmailTemplate))
                    {
                        _template.EmailTemplate = _template.EmailTemplate.Replace("#CustomerFirstName#", quoteModel.FirstName).
                                                   Replace("#OfficeName#", quoteModel.AgentName);
                    }
                }
                else
                {

                }
            }
            return _template;
        }

        /// <summary>
        /// Get Dynamic template field values by quoteID
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public QuoteModel getDynamicTemplateFieldByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var result = (from q in db.Quotes
                              let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                              let of = db.Offices.FirstOrDefault(f => f.ID == c.FKOfficeID)
                              where q.ID == quoteId
                              select new QuoteModel()
                              {
                                  FirstName = c.FirstName,
                                  OfficeId = of.ID,
                                  AgentName = of.Name
                              }).FirstOrDefault();

                return result;
            }
        }
    }
}
