﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Service
{
    public class AgencySettingServices
    {
        public bool UpdateAgencySetting(AgencyModel _agencyModel)
        {
            GeneralServices _generalServices = new GeneralServices();
            bool isSettingUpdate = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var stateId = !string.IsNullOrEmpty(_agencyModel.StateAbbr) ? _generalServices.GetStateId(_agencyModel.StateAbbr) : 0;
                    db.Agencies.Where(x => x.ID == _agencyModel.AgencyId).Update(x => new Agency()
                    {
                        EmailID = _agencyModel.AgencyEmailId,
                        Address1 = _agencyModel.Address1,
                        Address2 = _agencyModel.Address3,
                        ContactNo = _agencyModel.ContactNo.ToPhoneNumber(),
                        Zipcode = _agencyModel.ZipCode,
                        FKStateId = stateId,
                        City = _agencyModel.City,
                        FKDefaultNSDPlanId = Convert.ToInt32(_agencyModel.NSDPlanId)
                    });
                    isSettingUpdate = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSettingUpdate;
        }

        /// <summary>
        /// Save User Preferences
        /// </summary>
        /// <param name="_userPrefModel"></param>
        /// <returns></returns>
        public bool SaveUserPreference(UserPreferenceModel _userPrefModel)
        {
            GeneralServices _generalServices = new GeneralServices();
            bool isUserPreferenceSaved = false;
            long leftPaneOpenStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.USERPREFERENCETYPE.ToString(), Enums.UserPreferenceType.LEFTPANEOPEN.ToString());
            try
            {
                if (_userPrefModel != null)
                {
                    using (var db = new ArrowheadPOSEntities())
                    {
                        var isUserPreferenceExist = (db.UserPreferences.Where(x => x.FKAgencyID == _userPrefModel.AgencyId && x.FKUserId == _userPrefModel.UserId && x.FKPropertyID == leftPaneOpenStatusId).Count() > 0 ? true : false); 
                        if(!isUserPreferenceExist) // save preferences
                        {
                            UserPreference _userPreference = new UserPreference
                            {
                                FKUserId       = _userPrefModel.UserId,
                                FKAgencyID     = _userPrefModel.AgencyId,
                                FKPropertyID   = leftPaneOpenStatusId,
                                PropertyValue1 = _userPrefModel.IsLeftTabOpen == true ? "TRUE":"FALSE",
                                PropertyValue2 = null,
                                PropertyValue3 = null,
                                PropertyValue4 = null,
                                PropertyValue5 = null,
                                CreatedDate    = ConvertTo.GetEstTimeNow(),
                                ModifiedDate   = ConvertTo.GetEstTimeNow()
                            };
                            db.UserPreferences.Add(_userPreference);
                            db.SaveChanges();
                            isUserPreferenceSaved = true;
                        }
                        else // update preferences 
                        {
                            db.UserPreferences.Where(x => x.FKAgencyID == _userPrefModel.AgencyId && x.FKUserId == _userPrefModel.UserId && x.FKPropertyID == leftPaneOpenStatusId).Update(y => new UserPreference()
                            {
                                PropertyValue1 = _userPrefModel.IsLeftTabOpen == true ? "TRUE" : "FALSE",
                                ModifiedDate   = ConvertTo.GetEstTimeNow()
                            });
                            isUserPreferenceSaved = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                isUserPreferenceSaved = false;
                throw ex;
            }
            return isUserPreferenceSaved;
        }

        /// <summary>
        /// Get Welcome Screen Status by Agency Id
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public bool IsAgencyViewWelcomeScreen(long agencyId)
        {
            if (agencyId > 0)
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    return (db.Agencies.Where(x => x.ID == agencyId).Select(x => x.IsAgencyWelcomeScreen).FirstOrDefault());
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Save Welcome Screen Preference
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public bool SaveAgencyWelcomeScreen(long agencyId)
        {
            bool isAgencyWelcomeScreenSaved = false;
            if (agencyId > 0)
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    db.Agencies.Where(x => x.ID == agencyId).Update(x => new Agency()
                    {
                        IsAgencyWelcomeScreen = true
                    });
                    isAgencyWelcomeScreenSaved = true;
                }
            }

            return isAgencyWelcomeScreenSaved;
        }
    }
}
