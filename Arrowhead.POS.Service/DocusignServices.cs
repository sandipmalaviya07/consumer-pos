﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Process;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Arrowhead.POS.Service
{
    public class DocusignServices
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public SendCustomerToSignModel GetCustomerDetails(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var docusignQuoteModel = (from q in db.Quotes
                                          join c in db.Customers on q.FkCustomerID equals c.ID
                                          let quoteTypeCd = db.GeneralStatusTypes.Where(f => f.ID == q.FkQuoteTypeId).Select(s => s.Code).FirstOrDefault()
                                          where q.ID == quoteId
                                          select new SendCustomerToSignModel()
                                          {
                                              EmailID = c.EmailID,
                                              CustomerName = c.FirstName + " " + c.LastName,
                                              quoteID = quoteId,
                                              PhoneNumber = c.ContactNo
                                          }).FirstOrDefault();

                return docusignQuoteModel;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public SendPaperWorkModel GetCustomerPaperworkDetails(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                DocusignServices docusignServices = new DocusignServices();
                var credentials = docusignServices.GetDocusignApiCredential();
                string envelopeID = docusignServices.GetEnvelopeIDByQuoteId(quoteId);
                var sendPaperwork = (from q in db.Quotes
                                     join c in db.Customers on q.FkCustomerID equals c.ID
                                     where q.ID == quoteId
                                     select new SendPaperWorkModel
                                     {
                                         EmailId = c.EmailID,
                                         QuoteId = q.ID,
                                         ContactNo = c.ContactNo

                                     }).FirstOrDefault();
                return sendPaperwork;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public DocusignQuoteModel GetDocusignQuoteRecipientByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var docusignQuoteModel = (from q in db.Quotes
                                          join c in db.Customers on q.FkCustomerID equals c.ID
                                          join o in db.Offices on c.FKOfficeID equals o.ID
                                          let agency = db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID)
                                          //let user = db.Users.FirstOrDefault(f => f.ID == ConstantVariables.UserID)
                                          let quoteTypeCd = db.GeneralStatusTypes.Where(f => f.ID == q.FkQuoteTypeId).Select(s => s.Code).FirstOrDefault()
                                          where q.ID == quoteId
                                          select new DocusignQuoteModel()
                                          {
                                              CustomerEmailId = c.EmailID,
                                              CustomerName = c.FirstName + " " + c.LastName,
                                              QuoteId = quoteId,
                                              QuoteTypeCd = quoteTypeCd,
                                              UserName = agency.CommercialName,
                                              UserEmailId = agency.EmailID,
                                              StateID = q.FkStateId
                                          }).FirstOrDefault();
                return docusignQuoteModel;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public List<DocusignCredentialModel> GetDocusginCredential(string code, int agencyId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var query = (from ef in db.EnvelopeProviderForms
                             join epc in db.EnvelopeProviderCredentials on ef.ID equals epc.FKEnvelopeProviderFormId
                             join em in db.EnvelopeProviderModes on ef.FKEnvelopeProviderId equals em.FKEnvelopeProviderId
                             join ep in db.EnvelopeProviders on em.FKEnvelopeProviderId equals ep.ID
                             where epc.FKEnvelopeProviderModeID == em.ID && ep.Code == code && em.FKAgencyId == agencyId && em.IsActive == true
                             select new DocusignCredentialModel()
                             {
                                 Code = ef.Code,
                                 Value = epc.ValueText,
                                 IsLiveMode = epc.IsLiveMode,
                                 EnvelopeProviderModeId = em.ID
                             }).ToList();
                return query;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DocusignAuthorizationModel GetDocusignApiCredential()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                int agencyId = db.Agencies.Where(x => x.Code == ConstantVariables.RaterServiceProviderCode).Select(x => x.ID).FirstOrDefault();
                var _getcredential = GetDocusginCredential(Enums.EnvelopeProviders.DOCUSIGN.ToString(), agencyId);
                DocusignAuthorizationModel authorize = new DocusignAuthorizationModel();
                authorize.AccountId = _getcredential.Where(w => w.Code == Enums.EnvelopeProviderForm.ACCOUNTID.ToString()).Select(s => s.Value).FirstOrDefault();
                authorize.Password = _getcredential.Where(w => w.Code == Enums.EnvelopeProviderForm.PASSWORD.ToString()).Select(s => s.Value).FirstOrDefault();
                authorize.IntegratorKey = _getcredential.Where(w => w.Code == Enums.EnvelopeProviderForm.INTEGRATORKEY.ToString()).Select(s => s.Value).FirstOrDefault();
                authorize.UserName = _getcredential.Where(w => w.Code == Enums.EnvelopeProviderForm.USERNAME.ToString()).Select(s => s.Value).FirstOrDefault();
                authorize.Url = _getcredential.Where(w => w.Code == Enums.EnvelopeProviderForm.URL.ToString()).Select(s => s.Value).FirstOrDefault();
                authorize.EnvelopeProviderModeId = _getcredential.FirstOrDefault().EnvelopeProviderModeId;
                return authorize;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public QuoteEnvelope GetEnvelopeById(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var envelope = db.QuoteEnvelopes.Where(w => w.FKQuoteId == quoteId).FirstOrDefault();
                return envelope;
            }
        }

        /// <summary>
        /// Get Signed Medium Type by Quote Id
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string GetSignedMediumTypeByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var envelopeSignedMedium = (from qe in db.QuoteEnvelopes
                                            join gst in db.GeneralStatusTypes on qe.FKSignMediumId equals gst.ID
                                            where qe.FKQuoteId == quoteId
                                            select gst.Name).FirstOrDefault();
                return envelopeSignedMedium;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        public void UpdateEffectiveDate(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {

                DateTime currentDate = new DateTime(ConvertTo.GetEstTimeNow().Year, ConvertTo.GetEstTimeNow().Month, ConvertTo.GetEstTimeNow().Day, 0, 01, 0);
                try
                {
                    var quote = db.Quotes.FirstOrDefault(f => f.ID == quoteId);
                    if (quote.EffectiveDate.HasValue)
                    {
                        DateTime effectiveDate = new DateTime(quote.EffectiveDate.Value.Year, quote.EffectiveDate.Value.Month, quote.EffectiveDate.Value.Day, 0, 01, 0);
                        if (effectiveDate > currentDate)
                        {
                            db.Quotes.Where(w => w.ID == quoteId).Update(U => new Quote
                            {
                                EffectiveDate = effectiveDate
                            });
                        }
                        else if (effectiveDate == currentDate)
                        {
                            currentDate = ConvertTo.GetEstTimeNow();
                            db.Quotes.Where(w => w.ID == quoteId).Update(U => new Quote
                            {
                                EffectiveDate = currentDate
                            });
                        }

                    }
                    else
                    {

                    }

                }
                catch (Exception ex)
                {

                }
            }
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="stateID"></param>
        /// <param name="isPaymentMethod"></param>
        /// <returns></returns>
        public List<OnlineBiddingTemplateModel> GetOnlineBiddingTemplate(int stateID, bool isPaymentMethod, bool isOfficeInSign)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    GeneralServices generalServices = new GeneralServices();

                    if (isPaymentMethod)
                    {
                        var insuranceCompanyID = db.InsuranceCompanies.Where(f => f.Code == ConstantVariables.ArrowheadCompanyCode).FirstOrDefault().ID;



                        return (from o in db.OnlineBiddingTemplates
                                join g in db.GeneralStatusTypes on o.FKTemplateBindingTypeID equals g.ID
                                let quoteTypeCd = db.GeneralStatusTypes.Where(f => f.ID == o.FkEnvelopeTypeId).Select(s => s.Code).FirstOrDefault()
                                where o.FkInsuranceCompanyId == insuranceCompanyID && o.FKStateId == stateID && o.IsOfficeSign == isOfficeInSign
                                orderby o.Order
                                select new OnlineBiddingTemplateModel()
                                {
                                    BindingTypeCode = g.Code,
                                    EnvelopType = quoteTypeCd,
                                    InsuranceCompanyID = o.FkInsuranceCompanyId,
                                    Order = o.Order,
                                    TemplateID = o.TemplateId

                                }).ToList();

                    }
                    else
                    {
                        var insuranceCompanyID = db.InsuranceCompanies.Where(f => f.Code == ConstantVariables.ArrowheadCompanyCode).FirstOrDefault().ID;

                        long envelopeTypeId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ENVELOPETYPE.ToString(), Enums.EnvelopeType.PAYMENTRECEIPT.ToString());

                        return (from o in db.OnlineBiddingTemplates
                                join g in db.GeneralStatusTypes on o.FKTemplateBindingTypeID equals g.ID
                                let quoteTypeCd = db.GeneralStatusTypes.Where(f => f.ID == o.FkEnvelopeTypeId).Select(s => s.Code).FirstOrDefault()
                                where o.FkInsuranceCompanyId == insuranceCompanyID && o.FKStateId == stateID && o.FkEnvelopeTypeId != envelopeTypeId
                                && o.IsOfficeSign == isOfficeInSign
                                orderby o.Order
                                select new OnlineBiddingTemplateModel()
                                {
                                    BindingTypeCode = g.Code,
                                    EnvelopType = quoteTypeCd,
                                    InsuranceCompanyID = o.FkInsuranceCompanyId,
                                    Order = o.Order,
                                    TemplateID = o.TemplateId

                                }).ToList();
                    }


                }
            }
            catch (Exception ex)
            {
                return new List<OnlineBiddingTemplateModel>();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public bool PaymentMethodtype(long QuoteId)
        {
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long paymentMethodId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.CREDITCARD.ToString());
                    var qp = (db.QuotePayments.Where(w => w.FKQuoteID == QuoteId).OrderByDescending(x => x.ID).FirstOrDefault());
                    if (qp != null && qp.FKPaymentMethodID == paymentMethodId)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public bool CheckMethodType(long QuoteId, Enums.PaymentMethod paymentMethod)
        {
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long paymentMethodId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), paymentMethod.ToString());
                    return db.QuotePayments.Where(w => w.FKQuoteID == QuoteId && w.FKPaymentMethodID == paymentMethodId).Any();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string GetEnvelopeIDByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.QuoteEnvelopes.Where(w => w.FKQuoteId == quoteId).Select(x => x.EnvelopeId).FirstOrDefault();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public bool UpdateQuoteEnvelopeStatusByQuoteID(long QuoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                DateTime modifiedDate = ConvertTo.GetEstTimeNow();
                DateTime completedDate = ConvertTo.GetEstTimeNow();
                DateTime effectiveDate = ConvertTo.GetEstTimeNow();
                var fkEnvelopeStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ENVELOPESTATUS.ToString(), Enums.EnvelopeStatus.ESIGNCOMPLETED.ToString());
                db.QuoteEnvelopes.Where(w => w.FKQuoteId == QuoteID).Update(u => new QuoteEnvelope
                {
                    FKEnvelopeStatusId = fkEnvelopeStatusId,
                    ModifiedDate = modifiedDate,
                });

                long QuoteCompleteID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.COMPLETED.ToString());
                //var fkcurrentstatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.POLICY.ToString());
                long fkpolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                long FKCurrentStepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.COMPLETE.ToString());
                db.Quotes.Where(w => w.ID == QuoteID).Update(u => new Quote
                {
                    FkQuoteStatusID = QuoteCompleteID,
                    ModifiedDate = modifiedDate,
                    // CompletedById = UserId,
                    FkPolicyStatusId = fkpolicyStatusId,
                    CompletedDate = completedDate,
                    // EffectiveDate= effectiveDate,
                    FKCurrentStepId = FKCurrentStepId,
                    //EffectiveDate=Convert.ToDateTime(effectiveDate.TimeOfDay)
                    //ExpirationDate = effectiveDate.AddMonths(ConstantVariables.PolicyTerm)
                    //FKCurrentStepId = fkcurrentstatusId,
                });
                return true;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteID"></param>

        public void UpdateQuoteStatus(long QuoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                DateTime modifiedDate = ConvertTo.GetEstTimeNow();
                DateTime completedDate = ConvertTo.GetEstTimeNow();
                long QuoteCompleteID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.COMPLETED.ToString());
                long fkpolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                long FKCurrentStepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.COMPLETE.ToString());
                db.Quotes.Where(w => w.ID == QuoteID).Update(u => new Quote
                {
                    FkQuoteStatusID = QuoteCompleteID,
                    ModifiedDate = modifiedDate,
                    FkPolicyStatusId = fkpolicyStatusId,
                    CompletedDate = completedDate,
                    FKCurrentStepId = FKCurrentStepId,
                   
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public bool CheckPolicyNumberByQuoteId(long quoteId)
        {
            bool policyNumberExist = false;
            using (var db = new ArrowheadPOSEntities())
            {
                var policyNumber = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.PolicyNumber).FirstOrDefault();
                if (policyNumber != null)
                {
                    policyNumberExist = true;
                }
                else
                {
                    policyNumberExist = false;
                }
            }
            return policyNumberExist;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="binderModel"></param>
        /// <returns></returns>
        public bool UpdatePolicyNumber(PolicyBindResponseModel binderModel, bool isSignInOffice)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                if (binderModel.IsSuccess)
                {
                    //if (isSignInOffice)
                    //{
                        //db.Quotes.Where(w => w.ID == binderModel.QuoteID).Update(u => new Quote
                        //{
                        //    PolicyNumber = binderModel.PolicyNumber,
                        //    PolicyMsgDesc = binderModel.MessageDescription,

                        //});
                    //}
                    //else
                    //{
                        long QuoteCompleteID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.COMPLETED.ToString());
                        long fkpolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                        long FKCurrentStepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.COMPLETE.ToString());
                        db.Quotes.Where(w => w.ID == binderModel.QuoteID).Update(u => new Quote
                        {
                            FkQuoteStatusID = QuoteCompleteID,
                            FkPolicyStatusId = fkpolicyStatusId,
                            FKCurrentStepId = FKCurrentStepId,
                            PolicyNumber = binderModel.PolicyNumber,
                            PolicyMsgDesc = binderModel.MessageDescription
                        });
                    //}
                }
                else
                {
                    long fkpolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.BINDING.ToString());
                    long QuoteStatusNewID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.NEW.ToString());
                    long FKCurrentStepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.BINDING.ToString());

                    db.Quotes.Where(w => w.ID == binderModel.QuoteID).Update(u => new Quote
                    {
                        FkQuoteStatusID = QuoteStatusNewID,
                        ModifiedDate = ConvertTo.GetEstTimeNow(),
                        FkPolicyStatusId = fkpolicyStatusId,
                        FKCurrentStepId = FKCurrentStepId,
                        Description = binderModel.MessageDescription,
                        PolicyMsgDesc = binderModel.MessageDescription
                    });
                }
                UpdateBinderXMLAsync(binderModel);
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binderModel"></param>
        public void UpdateBinderXMLAsync(PolicyBindResponseModel binderModel)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                string fileName = binderModel.QuoteID + "-" + DateTime.Now.ToFileTime().ToString();
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    GeneralServices generalServices = new GeneralServices();
                    int insuranceCompanyID = db.InsuranceCompanies.Where(x => x.Code == ConstantVariables.ArrowheadCompanyCode).Select(x => x.ID).FirstOrDefault();
                    var RequestXMLByte = Encoding.UTF8.GetBytes(binderModel.RequestFile);
                    long CallTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.CALLTYPE.ToString(), Enums.CallType.BINDPOLICY.ToString());
                    long rateID = Convert.ToInt32(db.Auto_QuoteDetail.Where(x => x.FKQuoteID == binderModel.QuoteID).Select(x => x.FKRateHistoryID).FirstOrDefault());


                    Guid transactionID = db.Auto_QuoteDetail.Where(x => x.FKQuoteID == binderModel.QuoteID).Select(x => x.RateTransactionID).FirstOrDefault();
                    WebRaterAPICall callManagement = new WebRaterAPICall();
                    callManagement.FkQuoteID = binderModel.QuoteID;
                    callManagement.FkCallTypeID = CallTypeID;
                    callManagement.FkInsuranceCompanyID = insuranceCompanyID;
                    callManagement.RequestDate = ConvertTo.GetEstTimeNow();
                    callManagement.ResponseDate = ConvertTo.GetEstTimeNow();
                    callManagement.ResponseTransactionID = Convert.ToString(transactionID);
                    callManagement.RequestTransactionID = Convert.ToString(transactionID);
                    callManagement.IsSuccess = (binderModel.IsSuccess ? true : false);
                    callManagement.RequestFileURL = ConstantVariables.BinderRequestXMLContainer + "/" + fileName + "_reqXML" + extension;
                    callManagement.ResponseFileURL = ConstantVariables.BinderResponseXMLContainer + "/" + fileName + "_resXML" + extension;
                    db.WebRaterAPICalls.Add(callManagement);
                    db.SaveChanges();

                    new Thread(delegate () { UploadBinderRequestXML(binderModel, fileName); })
                    {
                    }.Start();
                    new Thread(delegate () { UploadBinderResponseXML(binderModel, fileName); })
                    {
                    }.Start();


                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binderModel"></param>
        /// <param name="fileName"></param>
        public void UploadBinderRequestXML(PolicyBindResponseModel binderModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var RequestXMLByte = Encoding.UTF8.GetBytes(binderModel.RequestFile);
                var _RequestFileUri = blobFileUtility.Save(ConstantVariables.BinderRequestXMLContainer, fileName + "_reqXML" + extension, RequestXMLByte).Result;
            }
            catch (Exception ex) { }

        }

        public void UploadDocusignOfficeApp(PolicyBindResponseModel binderModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".pdf";
            try
            {
                var RequestXMLByte = Encoding.UTF8.GetBytes(binderModel.RequestFile);
                var _RequestFileUri = blobFileUtility.Save(ConstantVariables.OfficeSignAppContainer, fileName + "" + extension, RequestXMLByte).Result;
            }
            catch (Exception ex) { }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="binderModel"></param>
        /// <param name="fileName"></param>
        public void UploadBinderResponseXML(PolicyBindResponseModel binderModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var RequestXMLByte = Encoding.UTF8.GetBytes(binderModel.ResponseFile);
                var _RequestFileUri = blobFileUtility.Save(ConstantVariables.BinderResponseXMLContainer, fileName + "_resXML" + extension, RequestXMLByte).Result;
            }
            catch (Exception ex) { }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="createdById"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public AutoEnvelopesModel InsertQuoteEnvelope(AutoEnvelopesModel model, long createdById, long quoteId)
        {
            var currentDate = ConvertTo.GetEstTimeNow();

            using (var db = new ArrowheadPOSEntities())
            {
                var customerID = db.Quotes.Where(f => f.ID == quoteId).FirstOrDefault().FkCustomerID;
                model.CustomerId = customerID;
                var delete = DeleteSignigDocument(quoteId);
                var fkenvelopestatusId = db.GeneralStatusTypes.Where(w => w.Code == Enums.EnvelopeStatus.ESIGNCREATED.ToString()).FirstOrDefault().ID;
                var envelopeProviderMode = db.EnvelopeProviders.Where(w => w.Code == Enums.EnvelopeProviders.DOCUSIGN.ToString()).FirstOrDefault();
                QuoteEnvelope autoenvelopes = new QuoteEnvelope();
                autoenvelopes.EnvelopeId = model.EnvelopeId;
                autoenvelopes.FKCustomerId = model.CustomerId;
                autoenvelopes.FKEnvelopeProviderId = envelopeProviderMode.ID;
                autoenvelopes.FKEnvelopeStatusId = fkenvelopestatusId;
                autoenvelopes.UserSignDate = model.UserSignDate;
                autoenvelopes.CustomerSignDate = model.CustomerSignDate;
                autoenvelopes.FkEnvelopProviderModeId = model.EnvelopeProviderModeId;
                autoenvelopes.FKQuoteId = model.QuoteId;
                autoenvelopes.CreatedDate = currentDate;
                autoenvelopes.CreatedById = createdById;
                autoenvelopes.ModifiedById = createdById;
                autoenvelopes.ModifiedDate = currentDate;
                db.QuoteEnvelopes.Add(autoenvelopes);
                db.SaveChanges();
                model.AutoEnvelopeId = autoenvelopes.Id;
                return model;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="createdById"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public AutoEnvelopesModel InsertPrintQuoteEnvelope(AutoEnvelopesModel model, long createdById, long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {

                var customerID = db.Quotes.Where(f => f.ID == quoteId).FirstOrDefault().FkCustomerID;
                model.CustomerId = customerID;
                var delete = DeleteSignigDocument(quoteId);
                var fkenvelopestatusId = db.GeneralStatusTypes.Where(w => w.Code == Enums.EnvelopeStatus.ESIGNCOMPLETED.ToString()).FirstOrDefault().ID;
                var envelopeProviderMode = db.EnvelopeProviders.Where(w => w.Code == Enums.EnvelopeProviders.DOCUSIGN.ToString()).FirstOrDefault();
                QuoteEnvelope quoteEnvelope = new QuoteEnvelope();
                quoteEnvelope.EnvelopeId = model.EnvelopeId;
                quoteEnvelope.FKCustomerId = model.CustomerId;
                quoteEnvelope.FKEnvelopeProviderId = envelopeProviderMode.ID;
                quoteEnvelope.FKEnvelopeStatusId = fkenvelopestatusId;
                quoteEnvelope.UserSignDate = model.UserSignDate;
                quoteEnvelope.CustomerSignDate = model.CustomerSignDate;
                quoteEnvelope.FkEnvelopProviderModeId = model.EnvelopeProviderModeId;
                quoteEnvelope.FKQuoteId = model.QuoteId;
                quoteEnvelope.CreatedDate = ConvertTo.GetEstTimeNow();
                quoteEnvelope.CreatedById = createdById;
                quoteEnvelope.ModifiedById = createdById;
                quoteEnvelope.ModifiedDate = ConvertTo.GetEstTimeNow();
                db.QuoteEnvelopes.Add(quoteEnvelope);
                db.SaveChanges();
                model.AutoEnvelopeId = quoteEnvelope.Id;
                UpdatePolicyStatus(quoteId);

                return model;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        public void UpdatePolicyStatus(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                db.Quotes.Where(w => w.ID == quoteId).Update(U => new Quote
                {
                    FkPolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString()),
                    PolicyStatusChangeDate = ConvertTo.GetEstTimeNow(),
                    ModifiedDate = ConvertTo.GetEstTimeNow()
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public bool DeleteSignigDocument(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                try
                {
                    var groupTransactionId = Guid.NewGuid();
                    db.QuoteEnvelopes.Where(x => x.FKQuoteId == quoteId).Delete();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public QuoteNSDPlan GetNsdByQuote(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {

                var nsddocument = db.QuoteNSDPlans.Where(w => w.FKQuoteId == quoteId && w.IsSuccess == true).FirstOrDefault();
                return nsddocument;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public Dictionary<string, string> DatasField(long quoteId, long agencyId)
        {
            AutoQuoteServices quoteServices = new AutoQuoteServices();
            AutoDriverServices driverServices = new AutoDriverServices();
            GeneralServices generalServices = new GeneralServices();
            RaterServices raterServices = new RaterServices();
            Dictionary<string, string> _dicPaymentField = new Dictionary<string, string>();
            try
            {
                var stateId = generalServices.GetStateID(quoteId);
                return GetDocusignDataFieldsByStateCode(quoteId, GeneralServices.GetStateCode(stateId));

                using (var db = new ArrowheadPOSEntities())
                {
                    int agencyID = (from q in db.Quotes
                                    join o in db.Offices on q.FkOfficeId equals o.ID
                                    where q.ID == quoteId
                                    select o.FKAgencyID).FirstOrDefault();
                    //db.Agencies.Where(f => f.Code == ConstantVariables.RaterServiceProviderCode).FirstOrDefault().ID;

                    var _agencyDetails = db.Agencies.FirstOrDefault(f => f.ID == agencyID);
                    var _paymentDetails = db.QuotePayments.Where(f => f.FKQuoteID == quoteId).OrderByDescending(x => x.ID).FirstOrDefault();
                    var _quoteDetails = quoteServices.GetQuoteInfo(quoteId);
                    var _projectTerm = (from pt in db.ProjectTerms select pt).ToList();
                    var _rateHistory = quoteServices.GetRateHistoryDetailsByQuoteID(quoteId);
                    //  var userDetails = quoteServices.GetAgentByID((int)userId);
                    var paymentMethod = generalServices.GetPaymentMethodByID(_paymentDetails.FKPaymentMethodID ?? 0);
                    var _raterProviderForm = raterServices.GetArrowheadCredentials(_quoteDetails.QuoteModel.StateAbbr, agencyId);
                    var _agencyproducerDetails = quoteServices.GetAgencyDetails(agencyId);
                    bool _isSeniorDriver = false;

                    //Add Aent Info

                    _dicPaymentField.Add("Agent_Name", _agencyDetails.CommercialName);
                    if (!string.IsNullOrWhiteSpace(_agencyDetails.ContactNo))
                    {
                        _dicPaymentField.Add("Agent_Phone", ConvertTo.PhoneNumberFormatting(_agencyDetails.ContactNo));
                        _dicPaymentField.Add("producerdetail_phone", ConvertTo.PhoneNumberFormatting(_agencyDetails.ContactNo));
                        _dicPaymentField.Add("receipt_AgencyPhone", ConvertTo.PhoneNumberFormatting(_agencyDetails.ContactNo));


                    }
                    else
                    {
                        _dicPaymentField.Add("Agent_Phone", "");
                        _dicPaymentField.Add("producerdetail_phone", "");
                        _dicPaymentField.Add("receipt_AgencyPhone", "");


                    }
                    if (!string.IsNullOrWhiteSpace(_agencyDetails.Fax))
                    {
                        _dicPaymentField.Add("Agent_Fax", ConvertTo.PhoneNumberFormatting(_agencyDetails.Fax));
                    }
                    else
                    {
                        _dicPaymentField.Add("Agent_Fax", "");
                    }
                    string additional_information = string.Empty;
                    if (_quoteDetails.QuoteModel.IsProofOfPrior == true)
                    {
                        int dueDay = (Convert.ToDateTime(_rateHistory.InstallmentDueDt).Date - ConvertTo.GetEstTimeNow().Date).Days;
                        // string proofofprior = "Failure to provide proof of six months prior coverage will result in an increase of premium and a change to your payment plan. Please e-mail your previous insurance policy declarations page to suspense@arrowheadgrp.com."; //" + (dueDay != 0 ? "Your first payment will be due in " + dueDay.ToString() + " days." : "") + "
                        additional_information += _projectTerm.Where(w => w.Code == Enums.ProjectTerm.PROOFOFPRIOR.ToString()).Select(f => f.TermValue).FirstOrDefault();

                    }

                    var _isNoHitDrivers = _quoteDetails.AutoQuoteDrivers.Where(w => w.IsNoHit == true).ToList();
                    if (_isNoHitDrivers.Count > 0)
                    {
                        string _noHitDriverlist = "";
                        foreach (var item in _isNoHitDrivers)
                        {
                            _noHitDriverlist += item.FirstName + " " + item.LastName + ",";
                        }
                        _noHitDriverlist = _noHitDriverlist.TrimEnd(',');
                        additional_information += " MVR was not obtainable for " + _noHitDriverlist + " please email a copy of the drivers license to misuspense@arrowheadgrp.com.";
                        //_dicPaymentField.Add("Additional_Information_Required", );
                    }

                    _dicPaymentField.Add("Additional_Information_Required", additional_information);
                    _dicPaymentField.Add("ProducerSignature", _agencyDetails.CommercialName);
                    if (!string.IsNullOrWhiteSpace(_raterProviderForm.ContractNumber))
                    {
                        _dicPaymentField.Add("Agent_Code", _raterProviderForm.ContractNumber);
                        _dicPaymentField.Add("producer_code", "Producer #:" + _raterProviderForm.ContractNumber);
                        _dicPaymentField.Add("Agency_producer_code", _raterProviderForm.ContractNumber);
                    }

                    if (_quoteDetails.QuoteModel.SR22Fee != 0)
                    {
                        _dicPaymentField.Add("Financial_Responsibility_Filing_Fee", GetValuesWithDollerSign(_quoteDetails.QuoteModel.SR22Fee.ToString()));
                    }


                    _dicPaymentField.Add("Insured_PolicyStartDate", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt") + " " + "(EST)"));
                    _dicPaymentField.Add("AgrentApplication_SignTime", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("hh:mm tt")));
                    _dicPaymentField.Add("InsuredApplication_SignTime", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("hh:mm tt")));
                    _dicPaymentField.Add("effective_dateto", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt")));
                    _dicPaymentField.Add("statement_date", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt")));


                    if (_quoteDetails != null)
                    {
                        //Quote Info
                        _dicPaymentField.Add("Policy_Number", _quoteDetails.QuoteModel.PolicyNumber);
                        _dicPaymentField.Add("billing_policy_number", _quoteDetails.QuoteModel.PolicyNumber);
                        _dicPaymentField.Add("affidavit_policy_number", _quoteDetails.QuoteModel.PolicyNumber);
                        _dicPaymentField.Add("Installment_Policy_Number", _quoteDetails.QuoteModel.PolicyNumber);
                        _dicPaymentField.Add("Receipt_Policy_Number", _quoteDetails.QuoteModel.PolicyNumber);
                        _dicPaymentField.Add("Insured_PolicyEndDate", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.ExpirationDate).ToString("MM/dd/yyyy hh:mm tt") + " " + "(EST)"));
                        _dicPaymentField.Add("Insured_LastName", _quoteDetails.QuoteModel.LastName);
                        _dicPaymentField.Add("Insured_FirstName", _quoteDetails.QuoteModel.FirstName);
                        _dicPaymentField.Add("Insured_MiddleName", _quoteDetails.QuoteModel.MiddleName);
                        _dicPaymentField.Add("Insured_MailingAddress", _quoteDetails.QuoteModel.Address1 + (_quoteDetails.QuoteModel.Address2 != "" ? " " + _quoteDetails.QuoteModel.Address2 + ", " : ", ") + _quoteDetails.QuoteModel.City + ", " + _quoteDetails.QuoteModel.StateAbbr + " " + _quoteDetails.QuoteModel.ZipCode);
                        _dicPaymentField.Add("Insured_Additional_MailingAddress", _quoteDetails.QuoteModel.IsDuplicate == false ? _quoteDetails.QuoteModel.MailingAddress1 + (_quoteDetails.QuoteModel.MailingAddress2 != "" ? " " + _quoteDetails.QuoteModel.MailingAddress2 + ", " : ", ") + _quoteDetails.QuoteModel.MailingCity + ", " + _quoteDetails.QuoteModel.MailingState + " " + _quoteDetails.QuoteModel.MailingZipCode : "");
                        _dicPaymentField.Add("Insured_PrimaryPhone", _quoteDetails.QuoteModel.ContactNo);
                        _dicPaymentField.Add("Insured_EmailAddress", _quoteDetails.QuoteModel.EmailId);
                        _dicPaymentField.Add("Insured_Policy_TotalPremium", GetValuesWithDollerSign(Convert.ToString(_quoteDetails.AutoRateDetail.TotalPremium)));
                        _dicPaymentField.Add("Insured_Policy_DownPayment", GetValuesWithDollerSign(Convert.ToString(_quoteDetails.AutoRateDetail.DownPaymentAmount - _rateHistory.NSDFees - _rateHistory.CCCharge)));
                        _dicPaymentField.Add("Insured_PayPlan", _rateHistory.Description);
                        _dicPaymentField.Add("Payment_Method", paymentMethod);
                        _dicPaymentField.Add("Insured_StateName", _quoteDetails.QuoteModel.StateName);
                        _dicPaymentField.Add("Installment_Fees", GetValuesWithDollerSign(_rateHistory.InstallmentFee.ToString()));
                        AutoDriverServices autoDriverServices = new AutoDriverServices();
                        if (_quoteDetails.AutoQuoteDrivers != null)
                        {
                            var violation = autoDriverServices.GetViolationByQuoteId(quoteId);
                            if (violation != null || violation.Count > 0)
                            {
                                for (int k = 0; k < violation.Count(); k++)
                                {
                                    int _val = k + 1;
                                    _dicPaymentField.Add("Violations_" + _val + "_DriverNumber", violation[k].DriverLicenseNumber);
                                    _dicPaymentField.Add("Violations_" + _val + "_DriverName", violation[k].FirstName + " " + violation[k].LastName);
                                    _dicPaymentField.Add("Violations_" + _val + "_Violation", Convert.ToString(violation[k].ViolationName));
                                    _dicPaymentField.Add("Violations_" + _val + "_Violation_Date", Convert.ToString(Convert.ToDateTime(violation[k].ViolationDate).ToString("MM/dd/yyyy")));
                                }
                            }

                            _dicPaymentField.Add("Declaration_To_Reside_CalendarYear", DateTime.Now.Year.ToString());
                            _dicPaymentField.Add("Declaration_To_Reside_Driver1DriversLicense", _quoteDetails.AutoQuoteDrivers.Where(w => w.DriverOrderID == 1).Select(f => f.LicenseNo).FirstOrDefault());
                            _dicPaymentField.Add("Declaration_To_Reside_Driver1DriversLicenseState", _quoteDetails.AutoQuoteDrivers.Where(w => w.DriverOrderID == 1).Select(f => f.LicenseState).FirstOrDefault());
                            _dicPaymentField.Add("Declaration_To_Reside_PolicyNumber", _quoteDetails.QuoteModel.PolicyNumber);
                            _dicPaymentField.Add("Declaration_To_Reside_EffectiveDate", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy hh:mm tt")));
                            _dicPaymentField.Add("Declaration_NameOfInsurance", _quoteDetails.QuoteModel.FirstName + " " + _quoteDetails.QuoteModel.LastName);


                            var _isNotexculdedDriver = _quoteDetails.AutoQuoteDrivers.Where(w => w.IsExcluded == false).ToList();

                            for (int i = 0; i < _isNotexculdedDriver.Count; i++)
                            {
                                bool _isDSeniorDriver = ((ConvertTo.GetEstTimeNow() - _isNotexculdedDriver[i].Dob.Value).Days / 365.25) < 65 ? false : true;
                                int _vioPoint = driverServices.GetViolationByDriverID(_isNotexculdedDriver[i].AutoDriverId).Select(s => s.Points).Sum();
                                if (_isNotexculdedDriver[i].Sr22 == true)
                                {
                                    _dicPaymentField.Add("SR22_State", _isNotexculdedDriver[i].LicenseState);
                                }
                                if (_isDSeniorDriver && !_isSeniorDriver)
                                {
                                    _isDSeniorDriver = true;
                                }

                                //Discount



                                int _val = i + 1;
                                _dicPaymentField.Add("Insured_Driver" + _val + "_Name", (_isNotexculdedDriver[i].FirstName + " " + _isNotexculdedDriver[i].LastName));
                                _dicPaymentField.Add("Insured_Driver" + _val + "_DOB", Convert.ToString(Convert.ToDateTime(_isNotexculdedDriver[i].Dob).ToString("MM/dd/y")));
                                _dicPaymentField.Add("Insured_Driver" + _val + "_Gender", _isNotexculdedDriver[i].Gender);
                                _dicPaymentField.Add("Insured_Driver" + _val + "_MaritalStatus", _isNotexculdedDriver[i].MaritalStatusName);
                                _dicPaymentField.Add("Insured_Driver" + _val + "_LicenseNumber", _isNotexculdedDriver[i].LicenseNo);
                                _dicPaymentField.Add("Insured_Driver" + _val + "_State", _isNotexculdedDriver[i].LicenseState);
                                _dicPaymentField.Add("Insured_Driver" + _val + "_LicenseStatus", _quoteDetails.AutoQuoteDrivers[i].LicenseStatusName);
                                _dicPaymentField.Add("Insured_Driver" + _val + "_DriverStatus", _projectTerm.Where(w => w.Code == Enums.ProjectTerm.DRIVERSTATUS.ToString()).Select(s => s.TermValue).FirstOrDefault());
                                _dicPaymentField.Add("Insured_Driver" + _val + "_SR22Filing", Convert.ToString(_isNotexculdedDriver[i].Sr22 == false ? "No" : "Yes"));
                                _dicPaymentField.Add("Insured_Driver" + _val + "_Occupation", _isNotexculdedDriver[i].DriverOccupationName.Truncate(6));
                                _dicPaymentField.Add("Insured_Driver" + _val + "_ViolationPoints", Convert.ToString((_isNotexculdedDriver[i].LicenseState == "FR" || _isNotexculdedDriver[i].LicenseState == "IT") ? 1 : _isNotexculdedDriver[i].ViolationPoints));
                                _dicPaymentField.Add("active_driver" + _val, (_isNotexculdedDriver[i].FirstName + " " + _isNotexculdedDriver[i].LastName).Truncate(16));

                                // ### SC New Fields Add Integration 
                                _dicPaymentField.Add("active_driver_" + _val, (_isNotexculdedDriver[i].FirstName + " " + _isNotexculdedDriver[i].LastName).Truncate(16));
                                _dicPaymentField.Add("active_driver_" + _val + "_dob", Convert.ToString(Convert.ToDateTime(_isNotexculdedDriver[i].Dob).ToString("MM/dd/y")));

                                _dicPaymentField.Add("seniordriver_" + _val, Convert.ToString((_isDSeniorDriver == true) ? "Y" : " "));

                            }

                            var _isexculdedDriver = _quoteDetails.AutoQuoteDrivers.Where(w => w.IsExcluded == true).ToList();
                            for (int i = 0; i < _isexculdedDriver.Count; i++)
                            {
                                int _val = i + 1;
                                _dicPaymentField.Add("ExcludedDrivers_" + _val + "_Name", (_isexculdedDriver[i].FirstName + " " + _isexculdedDriver[i].LastName).Truncate(16));
                                _dicPaymentField.Add("ExcludedDrivers_" + _val + "_DOB", Convert.ToString(Convert.ToDateTime(_isexculdedDriver[i].Dob).ToString("MM/dd/yyyy")));
                                _dicPaymentField.Add("SSI_ExcludedDrivers_" + _val + "_RelationShip", _isexculdedDriver[i].RelationName);
                                _dicPaymentField.Add("SSI_ExcludedDrivers_" + _val + "_Age", GetAge(_isexculdedDriver[i].Dob).ToString());
                                _dicPaymentField.Add("excluded_driver_" + _val, _isexculdedDriver[i].FirstName + " " + _isexculdedDriver[i].LastName);
                                _dicPaymentField.Add("excluded_driver_" + _val + "_dob", Convert.ToString(Convert.ToDateTime(_isexculdedDriver[i].Dob).ToString("MM/dd/yyyy")));

                            }

                            for (int i = 1; i < _rateHistory.NoOfInstallement; i++)
                            {
                                //int _val = i + 1;
                                DateTime DueDate = (DateTime)(_rateHistory.InstallmentDueDt);
                                _dicPaymentField.Add("due_date_" + i, Convert.ToString(Convert.ToDateTime(DueDate.AddMonths(i - 1)).ToString("MM/dd/yyyy")));
                                _dicPaymentField.Add("minimum_amount_due" + i, GetValuesWithDollerSign(_rateHistory.MonthlyPayment.ToString()));
                                //_dicPaymentField.Add("late_amount_due" + i, GetValuesWithDollerSign(Convert.ToString(_rateHistory.MonthlyPayment + 10)));
                            }


                            //DateTime currentDate = ConvertTo.GetEstTimeNow();
                            string installmentLateFeedate = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.INSTALLMENTLATEFEEDATE.ToString()).Select(s => s.TermValue).FirstOrDefault();


                            string Text2 = string.Empty;
                            string due = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.LATEDUE.ToString()).Select(s => s.TermValue).FirstOrDefault();

                            for (int i = 1; i < _rateHistory.NoOfInstallement; i++)
                            {
                                decimal dueConvert = Convert.ToDecimal(due);
                                decimal ifAmountDueLate = _rateHistory.MonthlyPayment + dueConvert;
                                if (_quoteDetails.QuoteModel.StateAbbr == "MI")
                                {
                                    if (_rateHistory.Description.Contains("5Pay-175-20 (5 Payments, 17.5% Down)"))
                                    {
                                        _dicPaymentField.Add("late_amount_due" + i, _rateHistory.MonthlyPayment.ToString());
                                    }
                                    else
                                    {
                                        _dicPaymentField.Add("late_amount_due" + i, GetValuesWithDollerSign(Convert.ToString(ifAmountDueLate)));
                                    }
                                }
                                else
                                {
                                    _dicPaymentField.Add("late_amount_due" + i, GetValuesWithDollerSign(Convert.ToString(ifAmountDueLate)));
                                }
                            }


                            if (_quoteDetails.QuoteModel.StateAbbr == "MI")
                            {
                                if (_rateHistory.Description.Contains("5Pay-175-20 (5 Payments, 17.5% Down)"))
                                {
                                    Text2 = "MONTHLY INSTALLMENT NO. 1 OF 5.";
                                    _dicPaymentField.Add("Late_fee", "$0.00");
                                }
                                else
                                {
                                    if (_rateHistory.NoOfInstallement == 1) // Full Pay
                                    {
                                        _dicPaymentField.Add("Late_fee", "N/A");
                                        Text2 = "MONTHLY INSTALLMENT NO. 0 OF 0.IF MAILED AFTER DUE DATE INCLUDE A LATE CHARGE OF $10.00";
                                    }
                                    else
                                    {
                                        Text2 = "MONTHLY INSTALLMENT NO. 1 OF 5.IF MAILED AFTER DUE DATE INCLUDE A LATE CHARGE OF $10.00";
                                        _dicPaymentField.Add("Late_fee", GetValuesWithDollerSign(due));
                                    }
                                }
                            }
                            else
                            {
                                if (_rateHistory.NoOfInstallement == 1) // Full Pay
                                {
                                    _dicPaymentField.Add("Late_fee", "N/A");
                                    Text2 = "MONTHLY INSTALLMENT NO. 0 OF 0.IF MAILED AFTER DUE DATE INCLUDE A LATE CHARGE OF $10.00";
                                }
                                else
                                {
                                    Text2 = "MONTHLY INSTALLMENT NO. 1 OF 5.IF MAILED AFTER DUE DATE INCLUDE A LATE CHARGE OF $10.00";
                                    _dicPaymentField.Add("Late_fee", GetValuesWithDollerSign(due));
                                }
                            }
                            _dicPaymentField.Add("Installment_to_latefee", Text2);





                            if (_rateHistory.NoOfInstallement == 1) // Full Pay
                            {
                                _dicPaymentField.Add("1st_InstallmentDue_Date", "N/A");
                                //_dicPaymentField.Add("Late_fee", "N/A");
                                _dicPaymentField.Add("Number_Installments", "N/A");
                            }
                            else
                            {
                                int numberOfPayments = _rateHistory.NoOfInstallement - 1;
                                _dicPaymentField.Add("1st_InstallmentDue_Date", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy")));
                                //_dicPaymentField.Add("Late_fee", GetValuesWithDollerSign(due));
                                _dicPaymentField.Add("Number_Installments", numberOfPayments.ToString());
                            }

                            _dicPaymentField.Add("receipt_today_date", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy")));
                            _dicPaymentField.Add("Insureds_LastName", _quoteDetails.QuoteModel.LastName);
                            _dicPaymentField.Add("Receipt2_Amount_Paid", GetValuesWithDollerSign(_rateHistory.PayableDownPayment.ToString())); // Display DownPayment + CC Fees + NSD Fees


                            var _isWorkLossDrivers = _quoteDetails.AutoQuoteDrivers.Where(w => w.IsWorkLossBenefit == true).ToList();
                            for (int i = 0; i < _isWorkLossDrivers.Count; i++)
                            {
                                int _val = i + 1;
                                _dicPaymentField.Add("WorkLossDriver_" + _val + "_Name", _isWorkLossDrivers[i].FirstName + " " + _isWorkLossDrivers[i].LastName);
                                _dicPaymentField.Add("WorkLossDriver_" + _val + "_DOB", Convert.ToString(Convert.ToDateTime(_isWorkLossDrivers[i].Dob).ToString("MM/dd/yyyy")));
                            }
                        }

                        if (_quoteDetails.AutoQuoteVehicles != null)
                        {
                            for (int i = 0; i < _quoteDetails.AutoQuoteVehicles.Count; i++)
                            {

                                int _val = i + 1;
                                _dicPaymentField.Add("Insured_Veh" + _val + "_Number", Convert.ToString(i + 1));
                                _dicPaymentField.Add("Insured_Veh" + _val + "_Year", Convert.ToString(_quoteDetails.AutoQuoteVehicles[i].Year));
                                _dicPaymentField.Add("Insured_Veh" + _val + "_MakeModel", (_quoteDetails.AutoQuoteVehicles[i].Make + "/" + _quoteDetails.AutoQuoteVehicles[i].Model).Truncate(20));
                                _dicPaymentField.Add("Insured_Veh" + _val + "_VIN", _quoteDetails.AutoQuoteVehicles[i].Vin);
                                _dicPaymentField.Add("Insured_Veh" + _val + "_Leased", (String.IsNullOrWhiteSpace(_quoteDetails.AutoQuoteVehicles[i].LienHolderCommercialName) ? "Owned" : "Leased"));
                                _dicPaymentField.Add("Lienholders_Veh" + _val, _quoteDetails.AutoQuoteVehicles[i].LienHolderCommercialName);
                                _dicPaymentField.Add("Insured_Veh" + _val + "_LienholderYesNo", _quoteDetails.AutoQuoteVehicles[i].IsLienHolder);
                                _dicPaymentField.Add("vehicle_" + _val + "_ModelYear", _quoteDetails.AutoQuoteVehicles[i].Year.ToString());
                                _dicPaymentField.Add("vehicle_" + _val + "_Make", _quoteDetails.AutoQuoteVehicles[i].Make);
                                _dicPaymentField.Add("vehicle_" + _val + "_vin", _quoteDetails.AutoQuoteVehicles[i].Vin);

                                // SC Lienholder Values
                                if (_quoteDetails.QuoteModel.StateAbbr.ToUpper() == "SC" && _quoteDetails.AutoQuoteVehicles[i].IsLienHolder.ToUpper() == "YES")
                                {
                                    // Lienholders_1_Auto
                                    int lienHolderStateId = _quoteDetails.AutoQuoteVehicles[i].LienHolderStateID ?? 0;
                                    var stateCode = GeneralServices.GetStateCode(lienHolderStateId);
                                    var vehicleModel = _quoteDetails.AutoQuoteVehicles[i].Year + " " + (_quoteDetails.AutoQuoteVehicles[i].Make + " " + _quoteDetails.AutoQuoteVehicles[i].Model).Truncate(20);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_Auto", _quoteDetails.AutoQuoteVehicles[i].Vin);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_Auto_Name", vehicleModel);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_Name", _quoteDetails.AutoQuoteVehicles[i].LienHolderName);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_Add1", _quoteDetails.AutoQuoteVehicles[i].LienHolderAddress1);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_Add2", _quoteDetails.AutoQuoteVehicles[i].LienHolderAddress2);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_City", _quoteDetails.AutoQuoteVehicles[i].LienHolderCity);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_State", stateCode);
                                    _dicPaymentField.Add("Lienholders_" + _val + "_Zip", _quoteDetails.AutoQuoteVehicles[i].LienHolderZipCode);
                                }
                            }
                        }
                    }

                    // Discount 
                    var _seniorDriver = _quoteDetails.AutoQuoteDrivers.Where(w => w.IsExcluded == false).ToList();
                    bool _seniorDriverDiscount = false;
                    for (int j = 0; j < _seniorDriver.Count; j++)
                    {
                        _seniorDriverDiscount = ((ConvertTo.GetEstTimeNow() - _seniorDriver[j].Dob.Value).Days / 365.25) < 65 ? false : true;
                        if (_seniorDriverDiscount)
                        {
                            _seniorDriverDiscount = true;
                        }
                    }
                    _dicPaymentField.Add("Dis_Multi_Vehicle", Convert.ToString(_quoteDetails.AutoQuoteVehicles.Count > 1 ? "Y" : ""));
                    _dicPaymentField.Add("Dis_Work_Loss_Ben", Convert.ToString(_quoteDetails.AutoQuoteDrivers.Where(x => x.IsWorkLossBenefit == true).Any() ? "Y" : ""));
                    _dicPaymentField.Add("Dis_Paid_In_Full", Convert.ToString(_rateHistory.Description.Contains("PIF (0 Payments, 100% Down)") ? "Y" : ""));
                    _dicPaymentField.Add("Dis_Proof_of_Prior", Convert.ToString(_quoteDetails.QuoteModel.IsProofOfPrior == true ? "Y" : ""));
                    _dicPaymentField.Add("Dis_Senior_Driver", Convert.ToString((_seniorDriverDiscount == true) ? "Y" : ""));

                    // END 

                    try
                    {
                        var coverageList = raterServices.GetCoverageForDocusign(quoteId);
                        if (coverageList != null)
                        {
                            if (coverageList.MVRFee != null)
                                _dicPaymentField.Add("Insured_MVRFee_Amt", GetValuesWithDollerSign(coverageList.MVRFee.ToString()));

                            if (coverageList.PolicyFee != null)
                                _dicPaymentField.Add("Insured_PolicyFee_Amt", GetValuesWithDollerSign(coverageList.PolicyFee.ToString()));

                            if (coverageList.UMFee != null)
                            {
                                _dicPaymentField.Add("Insured_UMFee_Veh1_Amt", GetValuesWithDollerSign(coverageList.UMFee.ToString()));
                                _dicPaymentField.Add("Insured_UMFee_Veh2_Amt", GetValuesWithDollerSign(coverageList.UMFee.ToString()));
                                _dicPaymentField.Add("Insured_UMFee_Veh3_Amt", GetValuesWithDollerSign(coverageList.UMFee.ToString()));
                                _dicPaymentField.Add("Insured_UMFee_Veh4_Amt", GetValuesWithDollerSign(coverageList.UMFee.ToString()));
                            }

                            if (_quoteDetails.AutoQuoteVehicles != null)
                            {


                                if (coverageList.PersonalInjuryProtectionDetails == "Coordinated PIP(Excess) $500 deductible")
                                {
                                    _dicPaymentField.Add("PIP_Deductible", GetValuesWithDollerSign("500"));
                                    _dicPaymentField.Add("PIP_Label", "Coordinated ");
                                }
                                else if (coverageList.PersonalInjuryProtectionDetails == "Primary PIP - 300 Deductible")
                                {
                                    _dicPaymentField.Add("PIP_Deductible", GetValuesWithDollerSign("300"));
                                    _dicPaymentField.Add("PIP_Label", "Primary");
                                }

                                else if (coverageList.PersonalInjuryProtectionDetails == "Primary PIP No deductible")
                                {
                                    _dicPaymentField.Add("PIP_Deductible", GetValuesWithDollerSign("0"));
                                    _dicPaymentField.Add("PIP_Label", "Primary");
                                }
                                for (int i = 0; i < _quoteDetails.AutoQuoteVehicles.Count; i++)
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            _dicPaymentField.Add("Insured_BodilyInjuryLiability_Person", GetValuesWithDollerSign(coverageList.BodilyInjuryPerPerson) + " Each Person");
                                            _dicPaymentField.Add("Insured_BodilyInjuryLiability_Accident", GetValuesWithDollerSign(coverageList.BodilyInjuryPerAcc) + " Each Accident");

                                            //SC New
                                            //Insured_Med_Pay_EachPerson
                                            if (Convert.ToDecimal(coverageList.MedicalPayments) > 0)
                                            {
                                                _dicPaymentField.Add("Insured_Med_Pay_EachPerson", GetValuesWithDollerSign(coverageList.MedicalPayments) + " Each Person");
                                            }

                                            //Insured_UMPD_Accident
                                            if (Convert.ToDecimal(coverageList.UninsuredPropertyDamage) > 0)
                                            {
                                                _dicPaymentField.Add("Insured_UMPD_Accident", GetValuesWithDollerSign(coverageList.UninsuredPropertyDamage) + " Each Accident");
                                            }

                                            //Insured_UNDMBI_Person_Accident
                                            if (Convert.ToDecimal(coverageList.UnderinsuredBodilyInjuryPerPerson) > 0)
                                            {
                                                _dicPaymentField.Add("Insured_UNDMBI_Person_Accident", GetValuesWithDollerSign(coverageList.UnderinsuredBodilyInjuryPerPerson) + " Each Person / " + GetValuesWithDollerSign(coverageList.UnderinsuredBodilyInjuryPerAcc) + " Each Accident");
                                            }

                                            //Insured_UNDMPD_Accident
                                            if (Convert.ToDecimal(coverageList.UnderinsuredPropertyDamage) > 0)
                                            {
                                                _dicPaymentField.Add("Insured_UNDMPD_Accident", GetValuesWithDollerSign(coverageList.UnderinsuredPropertyDamage) + " Each Accident");
                                            }



                                            _dicPaymentField.Add("Insured_LossOfUse", GetValuesWithDollerSign(coverageList.LossOfUse));
                                            _dicPaymentField.Add("Insured_BI_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1BI));
                                            _dicPaymentField.Add("Insured_PI_Accident", GetValuesWithDollerSign(coverageList.PropertyDamage) + " Each Accident");
                                            _dicPaymentField.Add("Insured_PI_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1PD));
                                            _dicPaymentField.Add("PIP_Deductible1", coverageList.PersonalInjuryProtectionDetails);
                                            _dicPaymentField.Add("Insured_Veh1_PIP_Amt", GetValuesWithDollerSign(coverageList.Veh1PIP));
                                            _dicPaymentField.Add("PPI_Deductible", GetValuesWithDollerSign(coverageList.PropertyProtectionInsurance) + " Per Accident");
                                            _dicPaymentField.Add("LPD_Limit_Amt", GetValuesWithDollerSign(coverageList.LimitedPropertyDamage) != null ? GetValuesWithDollerSign(coverageList.LimitedPropertyDamage) + " Deductible" : " ");
                                            _dicPaymentField.Add("Insured_Veh1_PPI_Amt", GetValuesWithDollerSign(coverageList.Veh1PPI));
                                            _dicPaymentField.Add("Insured_UMBI_PerPerson", coverageList.UninsuredBodilyInjuryPerPerson != null ? GetValuesWithDollerSign(coverageList.UninsuredBodilyInjuryPerPerson) + " Each Person" : "");
                                            _dicPaymentField.Add("Insured_UMBI_PerAccident", coverageList.UninsuredBodilyInjuryPerAcc != null ? GetValuesWithDollerSign(coverageList.UninsuredBodilyInjuryPerAcc) + " Each Accident" : "");
                                            _dicPaymentField.Add("Insured_UMBI_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1UMBI));
                                            _dicPaymentField.Add("Insured_CompColl_Veh1_Deductible", coverageList.Veh1ComprehensiveDed != null ? GetValuesWithDollerSign(coverageList.Veh1ComprehensiveDed.Split('.')[0]) : " ");
                                            _dicPaymentField.Add("Insured_Comp_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1Comp));
                                            _dicPaymentField.Add("Insured_CompColl_Veh1_Deductible1", coverageList.Veh1CollisionDed != null ? (GetValuesWithDollerSign(coverageList.Veh1CollisionDed.Split('.')[0]) + " " + coverageList.Veh1CollisionDedType) : " ");
                                            _dicPaymentField.Add("Insured_Coll_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1Coll));
                                            _dicPaymentField.Add("Insured_TowingLabor_50-150", GetValuesWithDollerSign(coverageList.Veh1TowingLimit));
                                            _dicPaymentField.Add("Insured_TowingLabor_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1TowingLimit));
                                            _dicPaymentField.Add("Insured_LossOfUse_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1LOS));
                                            _dicPaymentField.Add("Insured_Veh1_SAF_Amt", GetValuesWithDollerSign(coverageList.Veh1SAF));
                                            _dicPaymentField.Add("Insured_Veh1_MCCA_Amt", GetValuesWithDollerSign(coverageList.Veh1MCCA));
                                            _dicPaymentField.Add("Insured_Veh1_LPD_Amt", GetValuesWithDollerSign(coverageList.Veh1LPD));

                                            //Insured_MEDPAY_Veh1_Amt
                                            _dicPaymentField.Add("Insured_MEDPAY_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1MEDP));
                                            //Insured_UNDMBI_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMBI_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1UIMBI));
                                            //Insured_UNDMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMPD_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1UIMPD));
                                            //Insured_UMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UMPD_Veh1_Amt", GetValuesWithDollerSign(coverageList.Veh1UMPD));

                                            break;

                                        case 1:
                                            //_dicPaymentField.Add("Insured_BodilyInjuryLiability_Person", GetValuesWithDollerSign(coverageList.BodilyInjuryPerPerson) + " Each Person");
                                            //_dicPaymentField.Add("Insured_BodilyInjuryLiability_Accident", GetValuesWithDollerSign(coverageList.BodilyInjuryPerAcc) + " Each Accident");
                                            _dicPaymentField.Add("Insured_BI_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2BI));
                                            //_dicPaymentField.Add("Insured_PI_Accident", GetValuesWithDollerSign(coverageList.PropertyDamage) + " Each Accident");
                                            _dicPaymentField.Add("Insured_PI_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2PD));
                                            _dicPaymentField.Add("PIP_Deductible2", (coverageList.PersonalInjuryProtectionDetails));
                                            _dicPaymentField.Add("Insured_Veh2_PIP_Amt", GetValuesWithDollerSign(coverageList.Veh2PIP));
                                            //_dicPaymentField.Add("PPI_Deductible", GetValuesWithDollerSign(coverageList.PropertyProtectionInsurance) + " Per Accident");
                                            _dicPaymentField.Add("Insured_Veh2_PPI_Amt", GetValuesWithDollerSign(coverageList.Veh2PPI));
                                            //_dicPaymentField.Add("Insured_UMBI_PerPerson", GetValuesWithDollerSign(coverageList.UninsuredMotorist));
                                            _dicPaymentField.Add("Insured_UMBI_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2UMBI));
                                            _dicPaymentField.Add("Insured_CompColl_Veh2_Deductible", coverageList.Veh2ComprehensiveDed != null ? GetValuesWithDollerSign(coverageList.Veh2ComprehensiveDed.Split('.')[0]) : " ");
                                            _dicPaymentField.Add("Insured_Comp_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2Comp));
                                            _dicPaymentField.Add("Insured_CompColl_Veh2_Deductible1", coverageList.Veh2CollisionDed != null ? (GetValuesWithDollerSign(coverageList.Veh2CollisionDed.Split('.')[0]) + " " + coverageList.Veh2CollisionDedType) : " ");
                                            _dicPaymentField.Add("Insured_Coll_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2Coll));
                                            // _dicPaymentField.Add("Insured_TowingLabor_50-150", GetValuesWithDollerSign(coverageList.Veh2TowingLimit));
                                            _dicPaymentField.Add("Insured_TowingLabor_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2TowingLimit));
                                            _dicPaymentField.Add("Insured_LossOfUse_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2LOS));
                                            _dicPaymentField.Add("Insured_Veh2_SAF_Amt", GetValuesWithDollerSign(coverageList.Veh2SAF));
                                            _dicPaymentField.Add("Insured_Veh2_MCCA_Amt", GetValuesWithDollerSign(coverageList.Veh2MCCA));
                                            _dicPaymentField.Add("Insured_Veh2_LPD_Amt", GetValuesWithDollerSign(coverageList.Veh2LPD));

                                            //Insured_MEDPAY_Veh1_Amt
                                            _dicPaymentField.Add("Insured_MEDPAY_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2MEDP));
                                            //Insured_UNDMBI_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMBI_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2UIMBI));
                                            //Insured_UNDMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMPD_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2UIMPD));
                                            //Insured_UMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UMPD_Veh2_Amt", GetValuesWithDollerSign(coverageList.Veh2UMPD));

                                            break;

                                        case 2:
                                            //_dicPaymentField.Add("Insured_BodilyInjuryLiability_Person", GetValuesWithDollerSign(coverageList.BodilyInjuryPerPerson) + " Each Person");
                                            //_dicPaymentField.Add("Insured_BodilyInjuryLiability_Accident", GetValuesWithDollerSign(coverageList.BodilyInjuryPerAcc) + " Each Accident");
                                            _dicPaymentField.Add("Insured_BI_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3BI));
                                            // _dicPaymentField.Add("Insured_PI_Accident", GetValuesWithDollerSign(coverageList.PropertyDamage) + " Each Accident");
                                            _dicPaymentField.Add("Insured_PI_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3PD));
                                            _dicPaymentField.Add("PIP_Deductible3", (coverageList.PersonalInjuryProtectionDetails));
                                            _dicPaymentField.Add("Insured_Veh3_PIP_Amt", GetValuesWithDollerSign(coverageList.Veh3PIP));
                                            //_dicPaymentField.Add("PPI_Deductible", GetValuesWithDollerSign(coverageList.PropertyProtectionInsurance) + " Per Accident");
                                            _dicPaymentField.Add("Insured_Veh3_PPI_Amt", GetValuesWithDollerSign(coverageList.Veh3PPI));
                                            //_dicPaymentField.Add("Insured_UMBI_PerPerson", GetValuesWithDollerSign(coverageList.UninsuredMotorist));
                                            _dicPaymentField.Add("Insured_UMBI_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3UMBI));
                                            _dicPaymentField.Add("Insured_CompColl_Veh3_Deductible", coverageList.Veh3ComprehensiveDed != null ? GetValuesWithDollerSign(coverageList.Veh3ComprehensiveDed.Split('.')[0]) : " ");
                                            _dicPaymentField.Add("Insured_Comp_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3Comp));
                                            _dicPaymentField.Add("Insured_CompColl_Veh3_Deductible1", coverageList.Veh3CollisionDed != null ? (GetValuesWithDollerSign(coverageList.Veh3CollisionDed.Split('.')[0]) + " " + coverageList.Veh3CollisionDedType) : " ");
                                            _dicPaymentField.Add("Insured_Coll_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3Coll));
                                            //_dicPaymentField.Add("Insured_TowingLabor_50-150", GetValuesWithDollerSign(coverageList.Veh3TowingLimit));
                                            _dicPaymentField.Add("Insured_TowingLabor_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3TowingLimit));
                                            _dicPaymentField.Add("Insured_LossOfUse_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3LOS));
                                            _dicPaymentField.Add("Insured_Veh3_SAF_Amt", GetValuesWithDollerSign(coverageList.Veh3SAF));
                                            _dicPaymentField.Add("Insured_Veh3_MCCA_Amt", GetValuesWithDollerSign(coverageList.Veh3MCCA));
                                            _dicPaymentField.Add("Insured_Veh3_LPD_Amt", GetValuesWithDollerSign(coverageList.Veh3LPD));

                                            //Insured_MEDPAY_Veh1_Amt
                                            _dicPaymentField.Add("Insured_MEDPAY_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3MEDP));
                                            //Insured_UNDMBI_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMBI_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3UIMBI));
                                            //Insured_UNDMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMPD_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3UIMPD));
                                            //Insured_UMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UMPD_Veh3_Amt", GetValuesWithDollerSign(coverageList.Veh3UMPD));

                                            break;

                                        case 3:
                                            //_dicPaymentField.Add("Insured_BodilyInjuryLiability_Person", GetValuesWithDollerSign(coverageList.BodilyInjuryPerPerson) + " Each Person");
                                            //_dicPaymentField.Add("Insured_BodilyInjuryLiability_Accident", GetValuesWithDollerSign(coverageList.BodilyInjuryPerAcc) + " Each Accident");
                                            _dicPaymentField.Add("Insured_BI_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4BI));
                                            //_dicPaymentField.Add("Insured_PI_Accident", GetValuesWithDollerSign(coverageList.PropertyDamage) + " Each Accident");
                                            _dicPaymentField.Add("Insured_PI_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4PD));
                                            _dicPaymentField.Add("PIP_Deductible4", (coverageList.PersonalInjuryProtectionDetails));
                                            _dicPaymentField.Add("Insured_Veh4_PIP_Amt", GetValuesWithDollerSign(coverageList.Veh4PIP));
                                            //_dicPaymentField.Add("PPI_Deductible", GetValuesWithDollerSign(coverageList.PropertyProtectionInsurance) + " Per Accident");
                                            _dicPaymentField.Add("Insured_Veh4_PPI_Amt", GetValuesWithDollerSign(coverageList.Veh4PPI));
                                            //_dicPaymentField.Add("Insured_UMBI_PerPerson", GetValuesWithDollerSign(coverageList.UninsuredMotorist));
                                            _dicPaymentField.Add("Insured_UMBI_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4UMBI));
                                            _dicPaymentField.Add("Insured_CompColl_Veh4_Deductible", coverageList.Veh4ComprehensiveDed != null ? GetValuesWithDollerSign(coverageList.Veh4ComprehensiveDed.Split('.')[0]) : " ");
                                            _dicPaymentField.Add("Insured_Comp_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4Comp));
                                            _dicPaymentField.Add("Insured_CompColl_Veh4_Deductible1", coverageList.Veh4CollisionDed != null ? (GetValuesWithDollerSign(coverageList.Veh4CollisionDed.Split('.')[0]) + " " + coverageList.Veh4CollisionDedType) : " ");
                                            _dicPaymentField.Add("Insured_Coll_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4Coll));
                                            // _dicPaymentField.Add("Insured_TowingLabor_50-150", GetValuesWithDollerSign(coverageList.Veh4TowingLimit));
                                            _dicPaymentField.Add("Insured_TowingLabor_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4TowingLimit));
                                            _dicPaymentField.Add("Insured_LossOfUse_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4LOS));
                                            _dicPaymentField.Add("Insured_Veh4_SAF_Amt", GetValuesWithDollerSign(coverageList.Veh4SAF));
                                            _dicPaymentField.Add("Insured_Veh4_MCCA_Amt", GetValuesWithDollerSign(coverageList.Veh4MCCA));
                                            _dicPaymentField.Add("Insured_Veh4_LPD_Amt", GetValuesWithDollerSign(coverageList.Veh4LPD));

                                            //Insured_MEDPAY_Veh1_Amt
                                            _dicPaymentField.Add("Insured_MEDPAY_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4MEDP));
                                            //Insured_UNDMBI_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMBI_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4UIMBI));
                                            //Insured_UNDMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UNDMPD_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4UIMPD));
                                            //Insured_UMPD_Veh1_Amt
                                            _dicPaymentField.Add("Insured_UMPD_Veh4_Amt", GetValuesWithDollerSign(coverageList.Veh4UMPD));

                                            break;
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                    try
                    {
                        QuestionnaireServices questionnaireServices = new QuestionnaireServices();
                        if (_quoteDetails.QuoteModel.StateAbbr.ToUpper() == "MI")
                        {
                            var lstQuestion = questionnaireServices.GetQuestionnaireById(quoteId);
                            if (lstQuestion != null)
                            {
                                for (int j = 0; j < lstQuestion.Count; j++)
                                {
                                    int _val = j + 1;
                                    if (lstQuestion[j].QuestionnaireAnswer.Answer == "Y")
                                        _dicPaymentField.Add("UW_Q" + _val.ToString() + "_Yes", lstQuestion[j].QuestionnaireAnswer.Answer);
                                    else if (lstQuestion[j].QuestionnaireAnswer.Answer == "N")
                                        _dicPaymentField.Add("UW_Q" + _val.ToString() + "_No", lstQuestion[j].QuestionnaireAnswer.Answer);

                                    if ((_val == 6 || _val == 13 || _val == 9) && !string.IsNullOrEmpty(lstQuestion[j].QuestionnaireAnswer.Description))
                                    {
                                        _dicPaymentField.Add("Question_" + _val + "_Explain", lstQuestion[j].QuestionnaireAnswer.Description);
                                        _dicPaymentField.Add("Question_" + _val + "_lable", _val.ToString());
                                    }
                                }
                            }
                        }
                        else if (_quoteDetails.QuoteModel.StateAbbr.ToUpper() == "SC")
                        {
                            var lstQuestion = questionnaireServices.GetQuestionnaireById(quoteId);
                            if (lstQuestion != null)
                            {
                                for (int j = 0; j < lstQuestion.Count; j++)
                                {
                                    int _val = j + 1;
                                    if (lstQuestion[j].QuestionnaireAnswer.Answer == "Y")
                                        _dicPaymentField.Add("UW_Q" + _val.ToString() + "_Yes", lstQuestion[j].QuestionnaireAnswer.Answer);
                                    else if (lstQuestion[j].QuestionnaireAnswer.Answer == "N")
                                        _dicPaymentField.Add("UW_Q" + _val.ToString() + "_No", lstQuestion[j].QuestionnaireAnswer.Answer);

                                    if ((_val == 5 || _val == 6 || _val == 16) && !string.IsNullOrEmpty(lstQuestion[j].QuestionnaireAnswer.Description))
                                    {
                                        _dicPaymentField.Add("Question_" + _val + "_Explain", lstQuestion[j].QuestionnaireAnswer.Description);
                                        _dicPaymentField.Add("Question_" + _val + "_lable", _val.ToString());
                                    }

                                }
                            }
                        }
                        else if (_quoteDetails.QuoteModel.StateAbbr.ToUpper() == "GA")
                        {
                            var lstQuestion = questionnaireServices.GetQuestionnaireById(quoteId);
                            if (lstQuestion != null)
                            {
                                for (int j = 0; j < lstQuestion.Count; j++)
                                {
                                    int _val = j + 1;
                                    if (lstQuestion[j].QuestionnaireAnswer.Answer == "Y")
                                        _dicPaymentField.Add("UW_Q" + _val.ToString() + "_Yes", lstQuestion[j].QuestionnaireAnswer.Answer);
                                    else if (lstQuestion[j].QuestionnaireAnswer.Answer == "N")
                                        _dicPaymentField.Add("UW_Q" + _val.ToString() + "_No", lstQuestion[j].QuestionnaireAnswer.Answer);

                                    if ((_val == 1 || _val == 2 || _val == 3 || _val == 12) && !string.IsNullOrEmpty(lstQuestion[j].QuestionnaireAnswer.Description))
                                    {
                                        _dicPaymentField.Add("Question_" + _val + "_Explain", lstQuestion[j].QuestionnaireAnswer.Description);
                                        _dicPaymentField.Add("Question_" + _val + "_lable", _val.ToString());
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                    // Payment Receipt
                    _dicPaymentField.Add("receipt_Agency", _agencyDetails.CommercialName);
                    _dicPaymentField.Add("receipt_Agency_Address1", _agencyDetails.Address1);
                    _dicPaymentField.Add("receipt_Agency_Address2", _agencyDetails.Address2);
                    _dicPaymentField.Add("receipt_customerName", _quoteDetails.QuoteModel.FirstName + " " + _quoteDetails.QuoteModel.LastName);
                    _dicPaymentField.Add("receipt_customerAddress1", _quoteDetails.QuoteModel.Address1);
                    _dicPaymentField.Add("receipt_customerAddress2", _quoteDetails.QuoteModel.City + ", " + _quoteDetails.QuoteModel.StateAbbr + " " + _quoteDetails.QuoteModel.ZipCode);
                    _dicPaymentField.Add("receipt_date1", ConvertTo.GetEstTimeNow().ToString("MM/dd/yyyy"));
                    _dicPaymentField.Add("receipt_invoiceNo", _quoteDetails.QuoteModel.QuoteId.ToString());
                    _dicPaymentField.Add("receipt_customerNo", string.Empty);
                    _dicPaymentField.Add("receipt_receiptBy", string.Empty);
                    _dicPaymentField.Add("receipt_date2", ConvertTo.GetEstTimeNow().ToString("MM/dd/yyyy"));
                    try
                    {
                        _dicPaymentField.Add("receipt_refAmountTendered", GetValuesWithDollerSign(_rateHistory.DownPayment.ToString())); // Display DownPayment + CC Fees + NSD Fees
                        _dicPaymentField.Add("receipt_refNo1", _paymentDetails.TransactionToken);
                        _dicPaymentField.Add("receipt_refNo2", _paymentDetails.TransactionToken);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        // Add billing pdf Info
                        _dicPaymentField.Add("effective_datefrom1", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy")));
                        _dicPaymentField.Add("effective_datefrom2", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy")));

                        _dicPaymentField.Add("producer_name", _agencyproducerDetails.AgencyName);
                        //   _dicPaymentField.Add("producer_lable", userDetails.FirstName + " " + userDetails.LastName);

                        _dicPaymentField.Add("producer_address1", _agencyproducerDetails.Address1);
                        _dicPaymentField.Add("producer_address2", _agencyproducerDetails.Address2);
                        _dicPaymentField.Add("due_date", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy hh:mm tt")));

                        _dicPaymentField.Add("insured_name", _quoteDetails.QuoteModel.FullName.Truncate(16));
                        _dicPaymentField.Add("insured_name2", _quoteDetails.QuoteModel.FullName.Truncate(16));

                        _dicPaymentField.Add("insured_contact_no", _quoteDetails.QuoteModel.ContactNo);

                        _dicPaymentField.Add("insured_address1", _quoteDetails.QuoteModel.Address1);
                        _dicPaymentField.Add("insured_address2", _quoteDetails.QuoteModel.City + " " + _quoteDetails.QuoteModel.StateAbbr + " " + _quoteDetails.QuoteModel.ZipCode);
                        _dicPaymentField.Add("expiration_datefrom", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.ExpirationDate).ToString("MM/dd/yyyy")));


                        _dicPaymentField.Add("producerdetail_name", _agencyproducerDetails.AgencyName);
                        _dicPaymentField.Add("producerdetail_address1", _agencyproducerDetails.Address1);
                        _dicPaymentField.Add("producerdetail_address2", _agencyproducerDetails.Address2);
                        _dicPaymentField.Add("Insurance_Company", "ARROWHEAD General Insurance Agency, Inc.");
                        string agencyPhone = ConvertTo.PhoneNumberFormatting(_projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYPHONE.ToString()).Select(s => s.TermValue).FirstOrDefault());

                        string agencyWebsite = _projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYWEBADDRESS.ToString()).Select(s => s.TermValue).FirstOrDefault();
                        string local_phonenumber = ConvertTo.PhoneNumberFormatting(_projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYLOCALPHONENUMBER.ToString()).Select(s => s.TermValue).FirstOrDefault());
                        string tollfree_number = ConvertTo.PhoneNumberFormatting(_projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYTOLLFREENUMBER.ToString()).Select(s => s.TermValue).FirstOrDefault());
                        string agency_fax = ConvertTo.PhoneNumberFormatting(_projectTerm.Where(w => w.Code == Enums.ProjectTerm.AGENCYFAXNUMBER.ToString()).Select(s => s.TermValue).FirstOrDefault());
                        _dicPaymentField.Add("billing_agency_website", agencyWebsite);
                        _dicPaymentField.Add("billing_website", agencyWebsite);
                        _dicPaymentField.Add("website_local_phonenumber", local_phonenumber);
                        _dicPaymentField.Add("website_tollfree-phonenumber", tollfree_number);
                        _dicPaymentField.Add("website_fax", agency_fax);

                        _dicPaymentField.Add("website_webaddress", agencyWebsite);
                        if (_rateHistory.NoOfInstallement == 1)
                        {
                            _dicPaymentField.Add("Installment_number_to", "0");
                            _dicPaymentField.Add("installment_number_from", "0");
                        }
                        else if (_rateHistory.NoOfInstallement == 6)
                        {
                            _dicPaymentField.Add("Installment_number_to", "1");
                            _dicPaymentField.Add("installment_number_from", "5");
                        }
                        // Full Payment  then  Installment Fee 0 
                        string _installmenetfees = "0";
                        _installmenetfees = _rateHistory.Description.Contains("PIF (0 Payments, 100% Down)") ? "0" : _rateHistory.InstallmentFee.ToString();
                        _dicPaymentField.Add("amount_3", GetValuesWithDollerSign(_installmenetfees));
                        decimal payFullamount = _rateHistory.TotalPremium - _rateHistory.PayableDownPayment + _rateHistory.InstallmentFee + _rateHistory.InstallmentFee ?? 0;
                        _dicPaymentField.Add("amount_1", GetValuesWithDollerSign(_rateHistory.PayableDownPayment.ToString()));
                        _dicPaymentField.Add("pay_fullamount", GetValuesWithDollerSign(payFullamount.ToString()));
                        _dicPaymentField.Add("minimum_due", GetValuesWithDollerSign(_rateHistory.MonthlyPayment.ToString()));
                        //  DateTime date1 = ConvertTo.GetEstTimeNow(); 
                        _dicPaymentField.Add("date_1", Convert.ToString(Convert.ToDateTime(ConvertTo.GetEstTimeNow()).ToString("MM/dd/yyyy")));
                        _dicPaymentField.Add("amount_due", GetValuesWithDollerSign(_rateHistory.MonthlyPayment.ToString()));
                        _dicPaymentField.Add("amount_2", GetValuesWithDollerSign((_rateHistory.MonthlyPayment - Convert.ToDecimal(_installmenetfees)).ToString()));
                        _dicPaymentField.Add("amount_4", GetValuesWithDollerSign(_rateHistory.MonthlyPayment.ToString()));
                        _dicPaymentField.Add("date_2", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy")));
                        _dicPaymentField.Add("date_3", Convert.ToString(Convert.ToDateTime(_rateHistory.InstallmentDueDt).ToString("MM/dd/yyyy hh:mm tt")));
                    }
                    catch (Exception ex)
                    {

                    }
                    //  _dicPaymentField.Add("receipt_refNo2Paid", Formatc(_rateHistory.DownPayment - _rateHistory.StripeFee))
                    // _dicPaymentField.Add("receipt_refTotal", FormatCurrency(_rateHistory.StripeFee))
                    _dicPaymentField.Add("PaymentReceipt_Disclosure", "I authorize RateForce, LLC to charge the credit card indicated in this authorization form. I certify that I am an authorized user of this credit card and that I will not dispute the payment with my credit card company; so long as the transaction corresponds to the terms indicated in this form. I agree that the insurance coverage will become effective at the time the completed and signed application is submitted.  I understand my payment includes a non-refundable processing fee (listed above as CC on your receipt) assessed by RateForce, LLC for the convenience of choosing the Credit Card transaction alternative payment option. By submitting your policy payment using a Credit Card, you acknowledge, understand and agree to this non-refundable processing fee. I understand and acknowledge that this auto insurance policy will not be issued until I have provided all of the required signatures and initials on the following application.  I understand that failure to disclose tickets and violations on my driving record may result in an increase in premium.");  // Convert.ToString(_quoteDetails.ProcessingFeePaid)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  //  End Payment Receipt

                    // NSD Receipt
                    _dicPaymentField.Add("NSD_FullName", _quoteDetails.QuoteModel.FirstName + " " + _quoteDetails.QuoteModel.LastName);
                    _dicPaymentField.Add("NSD_Address1", _quoteDetails.QuoteModel.Address1);
                    _dicPaymentField.Add("NSD_City", _quoteDetails.QuoteModel.City);
                    _dicPaymentField.Add("NSD_State", _quoteDetails.QuoteModel.StateAbbr);
                    _dicPaymentField.Add("NSD_Zip", _quoteDetails.QuoteModel.ZipCode);
                    // _dicPaymentField.Add("NSD_AgencyProducerCode", _agencyDetails.NSD_ProducerCode)
                    _dicPaymentField.Add("NSD_AgencyName", _agencyDetails.CommercialName);
                    _dicPaymentField.Add("NSD_AgencyAddress", _agencyDetails.Address1);
                    _dicPaymentField.Add("NSD_AgencyCity", _agencyDetails.City);
                    // _dicPaymentField.Add("NSD_AgencyState", _agencyDetails.StateProvCd)
                    _dicPaymentField.Add("NSD_AgencyZip", _agencyDetails.Zipcode);
                    _dicPaymentField.Add("NSD_EffectiveDate", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.EffectiveDate).ToString("MM/dd/yyyy")));
                    _dicPaymentField.Add("NSD_ExpirationDate", Convert.ToString(Convert.ToDateTime(_quoteDetails.QuoteModel.ExpirationDate).ToString("MM/dd/yyyy")));
                    _dicPaymentField.Add("NSD_MembershipCost", Convert.ToString(Convert.ToString(_rateHistory.NSDFees)));
                    _dicPaymentField.Add("NSD_ProductID", "126");
                    _dicPaymentField.Add("NSD_Plan", "150");
                    var _vehicles = db.Auto_Vehicle.Where(f => f.FkQuoteID == quoteId).ToList();
                    var countVehicle = 1;
                    foreach (Auto_Vehicle vehicle in _vehicles)
                    {
                        _dicPaymentField.Add("NSD_Veh" + countVehicle.ToString() + "Year", Convert.ToString(vehicle.Year));
                        _dicPaymentField.Add("NSD_Veh" + countVehicle.ToString() + "Make", vehicle.Make);
                        _dicPaymentField.Add("NSD_Veh" + countVehicle.ToString() + "Model", vehicle.Model);
                        _dicPaymentField.Add("NSD_Veh" + countVehicle.ToString() + "VIN", vehicle.VIN);
                        countVehicle = countVehicle + 1;
                    }


                }
            }
            catch (Exception ex)
            {
            }
            return _dicPaymentField;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="agencyId"></param>
        /// <param name="_accrofield"></param>
        public void OfficeSignFields(long quoteId, long agencyId, dynamic _accrofield)
        {
            try
            {
                GeneralServices generalServices = new GeneralServices();
                var stateId = generalServices.GetStateID(quoteId);
                var _dicPaymentField = GetDocusignDataFieldsByStateCode(quoteId, GeneralServices.GetStateCode(stateId));
                foreach (KeyValuePair<string, string> entry in _dicPaymentField)
                {
                    _accrofield.SetField(entry.Key, entry.Value);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Get Docusign Fields From DB For MI State
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetDocusignDataFieldsByStateCode(long quoteId, string stateCode)
        {
            DataTable dtDataFields = new DataTable();

            using (var db = new ArrowheadPOSEntities())
            {
                string connectionString = ConstantVariables.ConnectionString;
                // Dim connectionString As String = "data source=DESKTOP-9SOA1QP\SQLEXPRESS2014;initial catalog=ArrowheadPOS;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;"

                if (connectionString.ToLower().StartsWith("metadata="))
                {
                    System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder efBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(connectionString);
                    connectionString = efBuilder.ProviderConnectionString;
                }

                DataSet dbset = new DataSet();
                string strConnString = connectionString;
                SqlConnection con1 = new SqlConnection(strConnString);
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.CommandText = "spDocusignDataFieldsProcess_" + stateCode;
                cmd2.Parameters.Add("@quote_id", SqlDbType.Int).Value = quoteId;
                cmd2.Connection = con1;
                try
                {
                    con1.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd2;
                    da.Fill(dbset);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    con1.Close();
                    con1.Dispose();
                }

                if (dbset != null && dbset.Tables.Count != 0)
                {
                    dtDataFields = dbset.Tables[0];
                }

                // Prepare Dictionary 
                if (dtDataFields != null && dtDataFields.Rows.Count > 0)
                {
                    var _dictDataFields = GetDict(dtDataFields);
                    if (_dictDataFields != null && _dictDataFields.Count > 0)
                        return _dictDataFields;
                }
            }
            return null;
        }

        internal Dictionary<string, string> GetDict(DataTable dt)
        {
            return dt.AsEnumerable()
              .ToDictionary<DataRow, string, string>(row => row.Field<string>(0),
                                        row => row.Field<string>(1));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_input"></param>
        /// <returns></returns>
        private string GetValuesWithDollerSign(string _input)
        {
            try
            {
                if (!string.IsNullOrEmpty(_input))
                {
                    return string.Format("{0:C}", Convert.ToDecimal(_input));
                }
            }
            catch (Exception ex)
            {
                return _input;
            }
            return _input;

        }

        private int GetAge(DateTime? _dob)
        {
            if (_dob != null)
                return (DateTime.Now.Year - Convert.ToDateTime(_dob).Year);
            else
                return 0;
        }

        /// <summary>
        /// Get Office ID by QuoteID 
        /// </summary>
        /// <param name="_quoteId"></param>
        /// <returns></returns>
        public int GetOfficeIdByQuote(long _quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Quotes.Where(w => w.ID == _quoteId).Select(f => f.FkOfficeId).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetSMSBody()
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var smsBody = db.ProjectTerms.Where(w => w.Code == Enums.ProjectTerm.SMSBODY.ToString()).Select(s => s.TermValue).FirstOrDefault();
                return smsBody;
            }
        }
    }
}

