﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Process;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RazorEngine;
using RazorEngine.Templating;
using Arrowhead.POS.Process.ArrowHeadRTRProd;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using System.Threading;

namespace Arrowhead.POS.Service
{
    public class AutoPolicyServices
    {
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        public PaymentRequestModel GetPaymentByQuoteID(long quoteID)
        {
            var payment = new PaymentRequestModel();
            GeneralServices generalServices = new GeneralServices();
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            try
            {
                string _paymentGatewayCode = _autoPolicyServices.GetPaymentGatewayByQuoteId(quoteID);

                using (var db = new ArrowheadPOSEntities())
                {
                    var IsPaymentCompleted = (from q in db.QuotePayments
                                              join gt in db.GeneralStatusTypes on q.FKPaymentStatusID equals gt.ID
                                              where q.FKQuoteID == quoteID &&
                                              (gt.Code == Enums.PAYMENTSTATUS.SUCCESS.ToString() || gt.Code == Enums.PAYMENTSTATUS.INITIATED.ToString())
                                              orderby q.ID descending
                                              select new { gt.Code }).Any();

                    var docusignCompleted = (from q in db.QuoteEnvelopes
                                             join gt in db.GeneralStatusTypes on q.FKEnvelopeStatusId equals gt.ID
                                             let signMedium = db.GeneralStatusTypes.FirstOrDefault(f => f.ID == q.FKSignMediumId)
                                             where q.FKQuoteId == quoteID && gt.Code == Enums.EnvelopeStatus.ESIGNCOMPLETED.ToString()
                                             select new { q, signMedium }).FirstOrDefault();
                    bool isDocusignCompleted = (docusignCompleted == null ? false : true);
                    bool isSignInOffice = (docusignCompleted == null ? false : docusignCompleted.signMedium.Code == Enums.SignMedium.SIGNINGOFFICE.ToString());


                    string _activePGCd = db.PaymentGatewayProviders.Where(w => w.IsActive == true).Select(s => s.Code).FirstOrDefault();
                    decimal _ccFees = generalServices.ActivePGCCFees();

                    payment = (from q in db.Quotes
                               join qd in db.Auto_QuoteDetail on q.ID equals qd.FKQuoteID
                               let c = db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID)
                               let rh = db.Auto_RateHistory.FirstOrDefault(f => f.ID == qd.FKRateHistoryID)
                               let qp = db.QuotePaymentTransactions.FirstOrDefault(f => f.FKQuoteID == q.ID)
                               let qpay = db.QuotePayments.Where(f => f.FKQuoteID == q.ID).OrderByDescending(o => o.ID).FirstOrDefault()
                               where q.ID == quoteID
                               select new PaymentRequestModel()
                               {
                                   FullName = c.FirstName + " " + c.LastName,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   TotalAmount = rh.DownPayment + rh.NSDFee + rh.PGProcessingFees,
                                   Amount = rh.DownPayment + rh.NSDFee + rh.PGProcessingFees,
                                   TotalPremium = rh.Premium + rh.NSDFee + rh.PGProcessingFees,
                                   TotalNSD = rh.NSDFee,
                                   ZipCode = q.ZipCode,
                                   TempToken = null,
                                   QuoteID = q.ID,
                                   IsPaymentCompleted = IsPaymentCompleted,
                                   IsDocusignCompleted = isDocusignCompleted,
                                   Address1 = q.Address1,
                                   Addres2 = q.Address2,
                                   City = q.City,
                                   PhoneNumber = c.ContactNo,
                                   EffectiveDate = q.EffectiveDate,
                                   ActivePGCode = _activePGCd,
                                   Email = c.EmailID,
                                   CCFees = _ccFees,
                                   PaymentMonth = qp.CustomField4.Substring(qp.CustomField4.Length - 2),
                                   PaymentYear = qp.CustomField4.Substring(0, 2),
                                   TotalCarrier = rh.DownPayment,
                                   PaymentGatewayProviderId = qp.FKPaymentGatewayID ?? 0,
                                   PgTransactionId = qpay.PGTransactionID,
                                   ReceiptID = qpay.FKReceiptId,
                                   IsSignInOffice = isSignInOffice
                               }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
            }
            return payment;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <returns></returns>
        public PolicyStatusModel GetpolicyStatusDetails(long QuoteID)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                var policyModel = (from q in db.Quotes
                                   let gt = db.GeneralStatusTypes.Where(x => x.ID == q.FkPolicyStatusId).FirstOrDefault()
                                   let c = db.Customers.Where(w => w.ID == q.FkCustomerID).FirstOrDefault()
                                   where q.ID == QuoteID
                                   select new PolicyStatusModel()
                                   {
                                       Description = q.Description,
                                       PolicyNumber = q.PolicyNumber,
                                       FirstName = c.FirstName,
                                       LastName = c.LastName,
                                       CustomerId=c.ID,
                                       QuoteID = q.ID,
                                       PolicyStatusCode = gt.Code,
                                       PolicyOrderNo = gt.OrderNo
                                   }).FirstOrDefault();

                return policyModel;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        public string GetPolicyNumber(long quoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.Quotes.Where(x => x.ID == quoteID).Select(x => x.PolicyNumber).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        public string GetCompanyQuoteNumber(long quoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                long rateID = Convert.ToInt64(db.Auto_QuoteDetail.Where(x => x.FKQuoteID == quoteID).Select(x => x.FKRateHistoryID).FirstOrDefault());
                return db.Auto_RateHistory.Where(x => x.ID == rateID).Select(x => x.CarrierQuoteID).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteResponseModel"></param>
        /// <param name="responseModel"></param>
        /// <param name="quoteID"></param>
        public void UpdateMVRXMLAsync(MvrResponseModel model)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                string fileName = model.QuoteID + "-" + DateTime.Now.ToFileTime().ToString();
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    GeneralServices generalServices = new GeneralServices();
                    int insuranceCompanyID = db.InsuranceCompanies.Where(x => x.Code == ConstantVariables.ArrowheadCompanyCode).Select(x => x.ID).FirstOrDefault();
                    var RequestXMLByte = System.Text.Encoding.UTF8.GetBytes(model.RequestFile);
                    var ResponseXMLByte = System.Text.Encoding.UTF8.GetBytes(model.ResponseFile);
                    long CallTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.CALLTYPE.ToString(), Enums.CallType.MVR.ToString());


                    Guid guid = Guid.NewGuid();
                    WebRaterAPICall callManagement = new WebRaterAPICall();
                    callManagement.FkQuoteID = model.QuoteID;
                    callManagement.FkInsuranceCompanyID = insuranceCompanyID;
                    callManagement.RequestDate = ConvertTo.GetEstTimeNow();
                    callManagement.ResponseDate = ConvertTo.GetEstTimeNow();
                    callManagement.ResponseTransactionID = guid.ToString();
                    callManagement.RequestTransactionID = guid.ToString();
                    callManagement.FkCallTypeID = CallTypeID;
                    callManagement.IsSuccess = true;
                    callManagement.RequestFileURL = ConstantVariables.RequestXMLContainer + "/" + fileName + "_reqXML" + extension;
                    callManagement.ResponseFileURL = ConstantVariables.ResponseXMLContainer + "/" + fileName + "_resXML" + extension;
                    db.WebRaterAPICalls.Add(callManagement);
                    db.SaveChanges();



                    new Thread(delegate () { UploadMVRRequestXML(model, fileName); })
                    {
                    }.Start();

                    new Thread(delegate () { UploadMVRResponseXML(model, fileName); })
                    {
                    }.Start();


                }
            }
            catch (Exception ex) { }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseModel"></param>
        /// <param name="fileName"></param>
        public void UploadMVRRequestXML(MvrResponseModel responseModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var RequestXMLByte = System.Text.Encoding.UTF8.GetBytes(responseModel.RequestFile);
                var _RequestFileUri = blobFileUtility.Save(ConstantVariables.RequestXMLContainer, fileName + "_reqXML" + extension, RequestXMLByte).Result;
            }
            catch (Exception ex) { }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseModel"></param>
        /// <param name="fileName"></param>
        public void UploadMVRResponseXML(MvrResponseModel responseModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".xml";
            try
            {
                var ResponseXMLByte = System.Text.Encoding.UTF8.GetBytes(responseModel.ResponseFile);
                var _ResponseFileUri = blobFileUtility.Save(ConstantVariables.ResponseXMLContainer, fileName + "_resXML" + extension, ResponseXMLByte);
            }
            catch (Exception ex) { }

        }

        /// <summary>
        /// Get Driver Violation By DriverID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AutoDriverViolationModel> GetViolationByQuoteID(long quoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {

                var item = (from dv in db.Auto_DriverViolation
                            join v in db.Auto_Violation on dv.FKAutoViolationId equals v.ID
                            join d in db.Auto_Driver on dv.FkDriverId equals d.ID
                            where d.FkQuoteID == quoteID
                            select new AutoDriverViolationModel()
                            {
                                AutoDriverViolationId = dv.ID,
                                DriverId = dv.FkDriverId,
                                AutoViolationId = dv.FKAutoViolationId,
                                ViolationCode = v.Code,
                                ViolationName = v.Name,
                                ViolationType = v.TypeOfViolation,
                                ViolationDate = dv.ViolationDate,
                                Points = dv.Points,
                                Note = dv.Note,
                                BIamount = dv.BIAmount,
                                PDamount = dv.PDAmount,
                                IsDeleted = false,
                                IsMVR = dv.IsMVR,
                                DriverOrderId = d.DriverOrderID ?? 1
                            }).ToList();
                return item;
            }
        }

        public void UpdatePolicyStatus(long quoteID)
        {
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long policyStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.PROGRESS.ToString());
                    db.Quotes.Where(f => f.ID == quoteID).Update(u => new Quote()
                    {
                        FkPolicyStatusId = policyStatusID
                    });

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add New Rate After MVR Process
        /// </summary>
        /// <param name="autoRateCompanyResponses"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public List<QuoteRateResponseModel> InsertNewRate(List<AutoRateCompanyResponse> autoRateCompanyResponses, long quoteId)
        {
            List<QuoteRateResponseModel> _quoteDetailsResponseall = new List<QuoteRateResponseModel>();
            DateTime _currentDate = ConvertTo.GetEstTimeNow();
            List<QuoteRateResponseModel> _returnQuoteResponeModel = new List<QuoteRateResponseModel>();
            Guid _rateTransactionId = Guid.NewGuid();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var stateId = db.Quotes.Where(f => f.ID == quoteId).FirstOrDefault().FkStateId;
                    var CompanyMapping = db.Auto_CompanyMapping.Where(x => x.FKStateID == stateId).ToList();
                    decimal _nsdAmount = 0;
                    int _PolicyTerm = db.Quotes.Where(w => w.ID == quoteId).Select(s => s.PolicyTerm).FirstOrDefault();

                    foreach (var item in autoRateCompanyResponses)
                    {
                        var ArrowheadCompanyDetails = db.InsuranceCompanies.Where(f => f.Code == "ARROWHEAD").FirstOrDefault();

                        try
                        {
                            if (item.Errors.Count == 0)
                            {
                                try
                                {
                                    var _autoRateHistoryIds = (from ar in db.Auto_RateHistory
                                                               join arh in db.Auto_RateHistoryDetail on ar.FKQuoteID equals arh.FKQuoteID
                                                               where ar.FKQuoteID == quoteId && ar.RateTransactionID != _rateTransactionId
                                                               select arh).Delete();
                                    //db.Auto_QuoteDetail.Where(w => w.FKQuoteID == quoteId).Update(up => new Auto_QuoteDetail()
                                    //{
                                    //    //FKRateHistoryID = null
                                    //});
                                    db.Auto_RateHistory.Where(x => x.RateTransactionID != _rateTransactionId && x.FKQuoteID == quoteId).Delete();
                                }
                                catch (Exception ex)
                                {
                                }

                                long newRateHistoryID = 0;

                                foreach (var _listResult in item.AutoRateResults)
                                {
                                    if (_listResult.DownPayment > 0)
                                    {
                                        Auto_RateHistory rateHistory = new Auto_RateHistory()
                                        {
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            DownPayment = Convert.ToDecimal(_listResult.DownPayment),
                                            MonthllyPayment = Convert.ToDecimal(_listResult.MontlyPayment),
                                            Premium = Convert.ToDecimal(_listResult.TotalPremium),
                                            BridgeURL = item.BridgeUrl,
                                            CarrierQuoteID = item.CompanyQuoteId,
                                            NSDFee = _nsdAmount,
                                            CreatedDate = _currentDate,
                                            NumberOfPayment = _listResult.NumOfPayments,
                                            RateTransactionID = _rateTransactionId,
                                            Description = _listResult.PayPlanDescription
                                        };
                                        db.Auto_RateHistory.Add(rateHistory);
                                        db.SaveChanges();

                                        if (newRateHistoryID == 0)
                                            newRateHistoryID = rateHistory.ID;

                                        QuoteRateResponseModel _alldetails = new QuoteRateResponseModel()
                                        {
                                            CompanyId = ArrowheadCompanyDetails.ID,
                                            CompanyName = ArrowheadCompanyDetails.Name,
                                            DownPayment = Convert.ToDecimal(_listResult.DownPayment),
                                            ErrorResponse = string.Empty,
                                            MonthlyPayment = Convert.ToDecimal(_listResult.MontlyPayment),
                                            NSDFees = _nsdAmount,
                                            RateId = rateHistory.ID,
                                            RateTransactionID = Convert.ToString(_rateTransactionId),
                                            IsError = false,
                                            BridgeURL = item.BridgeUrl,
                                            Description = _listResult.PayPlanDescription,
                                            PayableDownPayment = Convert.ToDecimal(_listResult.DownPayment) + _nsdAmount,
                                            TotalPremium = Convert.ToDecimal(_listResult.TotalPremium),
                                            NoOfInstallement = _listResult.NumOfPayments,
                                            PolicyTerm = _PolicyTerm,

                                        };
                                        _quoteDetailsResponseall.Add(_alldetails);
                                    }
                                }

                                db.Auto_QuoteDetail.Where(w => w.FKQuoteID == quoteId).Update(U => new Auto_QuoteDetail()
                                {
                                    RateDate = _currentDate,
                                    IsChange = false,
                                    RateTransactionID = _rateTransactionId
                                });

                                try
                                {
                                    if (item.SR22Fee != 0)
                                    {
                                        Auto_RateHistoryDetail _rateHistoryDetail = new Auto_RateHistoryDetail()
                                        {
                                            FkCoverageID = db.Coverages.Where(x => x.Code == Enums.Coverages.SR22Fee.ToString()).Select(s => s.ID).FirstOrDefault(),
                                            FKQuoteID = quoteId,
                                            FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                            Amount = Convert.ToDecimal(item.SR22Fee),
                                            Name = "SR22Fee",
                                            VehicleID = 0,
                                            RateTransactionID = _rateTransactionId
                                        };
                                        db.Auto_RateHistoryDetail.Add(_rateHistoryDetail);
                                        db.SaveChanges();
                                    }

                                    foreach (var res in item.Vehicles)
                                    {
                                        foreach (var cov in res.Coverages)
                                        {
                                            int coverageID = db.Coverages.Where(x => x.Code.Contains(cov.CoverageName)).Select(s => s.ID).FirstOrDefault();
                                            long vehicleID = db.Auto_Vehicle.Where(x => x.VIN.Equals(res.VIN) && x.FkQuoteID == quoteId).Select(s => s.ID).FirstOrDefault();
                                            try
                                            {
                                                if (coverageID > 0)
                                                {
                                                    Auto_RateHistoryDetail autoRateHistoryDetail = new Auto_RateHistoryDetail()
                                                    {
                                                        FkCoverageID = coverageID,
                                                        FKQuoteID = quoteId,
                                                        FKInsuranceCompanyID = ArrowheadCompanyDetails.ID,
                                                        Amount = Convert.ToDecimal(cov.Premium),
                                                        Name = cov.CoverageName,
                                                        VehicleID = vehicleID,
                                                        RateTransactionID = _rateTransactionId
                                                    };
                                                    db.Auto_RateHistoryDetail.Add(autoRateHistoryDetail);
                                                    db.SaveChanges();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                            else
                            {
                                QuoteRateResponseModel _allerrordetails = new QuoteRateResponseModel()
                                {
                                    CompanyId = Convert.ToInt32(item.CompanyId),
                                    CompanyName = item.CompanyName,
                                    DownPayment = 0,
                                    ErrorResponse = item.Errors.Select(s => s.Message).FirstOrDefault(),
                                    MonthlyPayment = 0,
                                    NSDFees = 0,
                                    RateId = 0,
                                    IsError = true,
                                    BridgeURL = item.BridgeUrl,
                                    Description = "",
                                    PayableDownPayment = 0,
                                    TotalPremium = 0,
                                    RateTransactionID = Convert.ToString(_rateTransactionId),
                                    NoOfInstallement = 0,
                                    PolicyTerm = _PolicyTerm,
                                    // StateCoverage = _StateCoverageList
                                };
                                _quoteDetailsResponseall.Add(_allerrordetails);
                            }
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }

                if (_quoteDetailsResponseall.Count > 0)
                {
                    _returnQuoteResponeModel = _quoteDetailsResponseall.OrderBy(o => o.DownPayment).ThenBy(o => o.MonthlyPayment).ToList();

                    foreach (var childRateHistory in _returnQuoteResponeModel)
                        childRateHistory._quoteDetailsResponseAllDetails = _quoteDetailsResponseall.Where(w => w.CompanyId == childRateHistory.CompanyId && w.RateId != childRateHistory.RateId).Select(s => new QuoteRateResponseDetailsModel()
                        {
                            RateId = s.RateId,
                            QuoteID = quoteId,
                            DownPayment = s.DownPayment,
                            MonthlyPayment = s.MonthlyPayment,
                            NSDFees = s.NSDFees,
                            IsError = s.IsError,
                            RateTransactionID = Convert.ToString(_rateTransactionId),
                            ErrorResponse = s.ErrorResponse,
                            BridgeURL = s.BridgeURL,
                            Description = s.Description,
                            PayableDownPayment = s.PayableDownPayment,
                            TotalPremium = s.TotalPremium
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return _returnQuoteResponeModel;
        }


        public void UpdateNewPurchaseInitiated(long rateHistoryID, long quoteID, decimal nsdPrice, long userId)
        {
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var _currentDate = ConvertTo.GetEstTimeNow();
                    var ratehistorydetails = db.Auto_RateHistory.Where(w => w.ID == rateHistoryID).FirstOrDefault();

                    db.Auto_RateHistory.Where(f => f.FKQuoteID == quoteID).Update(u => new Auto_RateHistory
                    {
                        NSDFee = nsdPrice
                    });

                    long policyStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());

                    db.Quotes.Where(f => f.ID == quoteID).Update(u => new Quote()
                    {
                        FkPolicyStatusId = policyStatusID,
                        FKInsuranceCompanyId = ratehistorydetails.FKInsuranceCompanyID,
                        ModifiedDate = _currentDate,
                        InitiatedById = userId,
                        InitiatedDate = _currentDate
                    });
                    db.Auto_QuoteDetail.Where(f => f.FKQuoteID == quoteID).Update(f => new Auto_QuoteDetail() { FKRateHistoryID = rateHistoryID });
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="violations"></param>
        /// <param name="quoteID"></param>
        public void AddArrowheadMVRViolation(DriverViolation[] violations, long quoteID, long userId)
        {

            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var driverViolationModels = GetViolationByQuoteID(quoteID);
                    var _createDate = ConvertTo.GetEstTimeNow();
                    foreach (var item in violations)
                    {
                        int violationID = db.Auto_Violation.Where(x => x.Name == item.Violation).Select(x => x.ID).FirstOrDefault();
                        long driverID = db.Auto_Driver.Where(x => x.FkQuoteID == quoteID && x.DriverOrderID == item.DriverID).Select(x => x.ID).FirstOrDefault();

                        if (item.Violation == "NO HIT/NO MATCH/ERROR")
                        {
                            db.Auto_Driver.Where(w => w.ID == driverID).Update(U => new Auto_Driver
                            {
                                IsNoHit = true,

                                // ModifiedByID = ConstantVariables.UserID,
                                ModifiedDate = ConvertTo.GetEstTimeNow()
                            });
                        }

                        if (driverViolationModels.Count > 0)
                        {
                            driverViolationModels = GetViolationByQuoteID(quoteID);

                            if (violations.Where(w => w.Violation == item.Violation && w.DriverID == item.DriverID).Count()
                                > driverViolationModels.Where(w => w.ViolationName == item.Violation && w.DriverOrderId == item.DriverID).Count()
                                || driverViolationModels.Count(w => w.ViolationName == item.Violation && w.DriverOrderId == item.DriverID) == 0)
                            {
                                Auto_DriverViolation violation = new Auto_DriverViolation
                                {
                                    FkDriverId = driverID,
                                    FKAutoViolationId = violationID,
                                    ViolationDate = Convert.ToDateTime(item.ViolationDate) == DateTime.MinValue ? _createDate : Convert.ToDateTime(item.ViolationDate),
                                    CreatedDate = _createDate,
                                    IsMVR = true,
                                    Points = item.Points,
                                    CreatedById = userId
                                };
                                db.Auto_DriverViolation.Add(violation);
                                db.SaveChanges();
                            }

                            //else
                            //{
                            //    var autoDriverViolation = driverViolationModels.FirstOrDefault(w => w.ViolationName == item.Violation && w.DriverOrderId == item.DriverID && w.ViolationDate == item.ViolationDate && w.IsMVR == false);
                            //    if (autoDriverViolation != null)
                            //    {
                            //        db.Auto_DriverViolation.Where(w => w.ID == autoDriverViolation.AutoDriverViolationId).Update(u => new Auto_DriverViolation
                            //        {
                            //            IsMVR = true
                            //        });
                            //    }
                            //}
                        }
                        else
                        {
                            if (item.Violation != "NO HIT/NO MATCH/ERROR")
                            {
                                if (violationID == 0)
                                {
                                    Auto_Violation auto_Violation = new Auto_Violation
                                    {
                                        Code = item.StateCode,
                                        Point1 = item.Points,
                                        Name = item.Violation,
                                        TypeOfViolation = "Other",
                                        ViolationFor = item.Violation,
                                        ValidityInMonth = 24,
                                        EnableCoverages = false
                                    };
                                    db.Auto_Violation.Add(auto_Violation);
                                    db.SaveChanges();

                                    Auto_DriverViolation violation = new Auto_DriverViolation
                                    {
                                        FkDriverId = driverID,
                                        FKAutoViolationId = auto_Violation.ID,
                                        ViolationDate = Convert.ToDateTime(item.ViolationDate) == DateTime.MinValue ? _createDate : Convert.ToDateTime(item.ViolationDate),
                                        CreatedDate = _createDate,
                                        IsMVR = true,
                                        Points = item.Points,
                                        CreatedById = userId
                                    };
                                    db.Auto_DriverViolation.Add(violation);
                                    db.SaveChanges();

                                }
                                if (violationID > 0)
                                {
                                    Auto_DriverViolation violation = new Auto_DriverViolation
                                    {
                                        FkDriverId = driverID,
                                        FKAutoViolationId = violationID,
                                        ViolationDate = Convert.ToDateTime(item.ViolationDate) == DateTime.MinValue ? _createDate : Convert.ToDateTime(item.ViolationDate),
                                        CreatedDate = _createDate,
                                        IsMVR = true,
                                        Points = item.Points,
                                        CreatedById = userId
                                    };
                                    db.Auto_DriverViolation.Add(violation);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteResponseModel"></param>
        /// <param name="responseModel"></param>
        /// <param name="quoteID"></param>
        public void UpdatePaymentXMLAsync(PaymentResponseModel model)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".json";
            try
            {
                string fileName = model.QuoteId + "-" + DateTime.Now.ToFileTime().ToString();
                using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
                {
                    GeneralServices generalServices = new GeneralServices();
                    int insuranceCompanyID = db.InsuranceCompanies.Where(x => x.Code == ConstantVariables.ArrowheadCompanyCode).Select(x => x.ID).FirstOrDefault();
                    var ResponseXMLByte = System.Text.Encoding.UTF8.GetBytes(model.ResponseFile);
                    long CallTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.CALLTYPE.ToString(), Enums.CallType.PAYMENT.ToString());


                    Guid guid = Guid.NewGuid();
                    WebRaterAPICall callManagement = new WebRaterAPICall();
                    callManagement.FkQuoteID = model.QuoteId;
                    callManagement.FkInsuranceCompanyID = insuranceCompanyID;
                    callManagement.RequestDate = ConvertTo.GetEstTimeNow();
                    callManagement.ResponseDate = ConvertTo.GetEstTimeNow();
                    callManagement.ResponseTransactionID = guid.ToString();
                    callManagement.RequestTransactionID = guid.ToString();
                    callManagement.FkCallTypeID = CallTypeID;
                    callManagement.IsSuccess = true;
                    //    callManagement.RequestFileURL = ConstantVariables.RequestXMLContainer + "/" + fileName + "_reqXML" + extension;
                    callManagement.ResponseFileURL = ConstantVariables.ResponseXMLContainer + "/" + fileName + "_resXML" + extension;
                    db.WebRaterAPICalls.Add(callManagement);
                    db.SaveChanges();

                    new Thread(delegate () { UploadPaymentResponseXML(model, fileName); })
                    {
                    }.Start();


                }
            }
            catch (Exception ex) { }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseModel"></param>
        /// <param name="fileName"></param>
        public void UploadPaymentResponseXML(PaymentResponseModel responseModel, string fileName)
        {
            BlobFileUtility blobFileUtility = new BlobFileUtility();
            string extension = ".json";
            try
            {
                var ResponseXMLByte = System.Text.Encoding.UTF8.GetBytes(responseModel.ResponseFile);
                var _ResponseFileUri = blobFileUtility.Save(ConstantVariables.ResponseXMLContainer, fileName + "_resXML" + extension, ResponseXMLByte);
            }
            catch (Exception ex) { }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AutoDriverViolationModel> GetMVRViolationByQuoteID(long id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                //  long driverID = db.Auto_Driver.Where(x => x.FkQuoteID == id && x.DriverOrderID == 1).Select(x => x.ID).FirstOrDefault();

                //var item = (from dv in db.Auto_DriverViolation
                //            join v in db.Auto_Violation on dv.FKAutoViolationId equals v.ID

                //            where dv.Auto_Driver.FkQuoteID == id /*&& dv.IsMVR == true*/
                //            select new AutoDriverViolationModel()
                //            {
                //                AutoDriverViolationId = dv.ID,
                //                DriverId = dv.FkDriverId,
                //                AutoViolationId = dv.FKAutoViolationId,
                //                ViolationCode = v.Code,
                //                ViolationName = v.Name,
                //                ViolationType = v.TypeOfViolation,
                //                ViolationDate = dv.ViolationDate,
                //                Points = dv.Points,
                //                Note = dv.Note,
                //                BIamount = dv.BIAmount,
                //                DriverOrderId = dv.Auto_Driver.DriverOrderID ?? 0,
                //                PDamount = dv.PDAmount,
                //                IsNoHit = dv.Auto_Driver.IsNoHit,
                //                DriverName = dv.Auto_Driver.FirstName + " " + dv.Auto_Driver.LastName,
                //                IsDeleted = false,
                //                IsMVR = dv.IsMVR
                //            }).ToList();

                var item = (from ad in db.Auto_Driver
                            join adv in db.Auto_DriverViolation on ad.ID equals adv.FkDriverId
                            join v in db.Auto_Violation on adv.FKAutoViolationId equals v.ID
                            where ad.FkQuoteID == id
                            select new AutoDriverViolationModel
                            {

                                AutoDriverViolationId = adv.ID,
                                DriverId = adv.FkDriverId,
                                AutoViolationId = adv.FKAutoViolationId,
                                ViolationCode = v.Code,
                                ViolationName = v.Name,
                                ViolationType = v.TypeOfViolation,
                                ViolationDate = adv.ViolationDate,
                                Points = adv.Points,
                                Note = adv.Note,
                                BIamount = adv.BIAmount,
                                DriverOrderId = ad.DriverOrderID ?? 0,
                                PDamount = adv.PDAmount,
                                IsNoHit = ad.IsNoHit,
                                DriverName = ad.FirstName + " " + ad.LastName,
                                IsDeleted = false,
                                IsMVR = adv.IsMVR
                            }).ToList();
                return item;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SignMediumId"></param>
        public void UpdateSignTracking(long SignMediumId, long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.QuoteEnvelopes.Where(w => w.FKQuoteId == quoteId).Update(u => new QuoteEnvelope
                {
                    FKSignMediumId = SignMediumId,
                    // ModifiedById = ConstantVariables.UserID,
                    ModifiedDate = ConvertTo.GetEstTimeNow()
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public List<AutoDriverModel> GetNoHitDrivers(long Id)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var drivers = (from ad in db.Auto_Driver
                               where ad.FkQuoteID == Id && ad.IsNoHit == true
                               select new AutoDriverModel
                               {
                                   IsNoHit = ad.IsNoHit,
                                   DriverName = ad.FirstName + " " + ad.LastName,
                                   FirstName = ad.FirstName,
                                   LastName = ad.LastName,
                                   DriverOrderID = ad.DriverOrderID ?? 0,
                               }).ToList();
                return drivers;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        public void UpdateQuotePayment(PaymentRequestModel req, string pgCode, string _success = "SUCCESS")
        {
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            GeneralServices generalServices = new GeneralServices();
            var _createDate = ConvertTo.GetEstTimeNow();
            long PaymentStatusId = 0;
            try
            {
                string token = GeneralServices.GenerateAuthToken(50);
                string _pStatus = _success;
                string paymentGatewayCode = pgCode;

                //if (_pStatus.ToUpper() == Enums.PAYMENTSTATUS.SUCCESS.ToString() && req.PaymentMethodCode == Enums.PaymentMethod.CREDITCARD.ToString() &&
                //    paymentGatewayCode == Enums.PaymentGateway.AUTHORIZENET.ToString())
                //{
                //    PaymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                //}
                //else
                //{
                PaymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), _pStatus);
                //}

                long PaymentMethodID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), req.PaymentMethodCode);
                long PolicyTypeID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYINSTALLMENTTYPES.ToString(), Enums.PolicyInstallmentType.NEWBUSINESS.ToString());

                using (var db = new ArrowheadPOSEntities())
                {
                    if (_success != Enums.PAYMENTSTATUS.FAIL.ToString())
                    {
                        var Quote = db.Quotes.FirstOrDefault(w => w.ID == req.QuoteID);
                        QuotePaymentReceipt receipt = new QuotePaymentReceipt()
                        {
                            Amount = req.Amount,
                            CreatedByID = req.UserId,
                            CreatedDate = _createDate,
                            FKQuoteID = req.QuoteID,
                            ModifiedByID = req.UserId,
                            ModifiedDate = _createDate,
                            FKCustomerId = Quote.FkCustomerID,
                            FKStatusId = PaymentStatusId,
                            TransactionToken = token
                        };
                        db.QuotePaymentReceipts.Add(receipt);
                        db.SaveChanges();


                        long _receiptId = receipt.ID;

                        long _qOLPay = 0;
                        long _qpTransId = 0;
                        if (req.PaymentMethodCode == Enums.PaymentMethod.CREDITCARD.ToString())
                        {
                            string _OPtransactionToken = SysFunctions.GenerateAuthToken(50);
                            int paymentGateway = (from x in db.PaymentGatewayProviders
                                                  where x.IsActive == true
                                                  select x.Id).FirstOrDefault();
                            QuoteOnlinePayment quoteOnlinePayment = new QuoteOnlinePayment
                            {
                                Amount = req.Amount,
                                FkquoteId = req.QuoteID,
                                FkreceiptId = _receiptId,
                                FkcustId = Quote.FkCustomerID,
                                TransactionToken = _OPtransactionToken,
                                FkPaymentGateWayId = paymentGateway,
                                CreatedDate = _createDate,
                                PgTransactionId = req.PgTransactionId,
                                FkPaymentStatusId = PaymentStatusId,
                                TokenExpiryDate = _createDate.AddDays(1),
                                PaymentDate = _createDate
                            };
                            db.QuoteOnlinePayments.Add(quoteOnlinePayment);
                            db.SaveChanges();
                            _qOLPay = quoteOnlinePayment.Id;

                        }
                        //if (_success != Enums.PAYMENTSTATUS.FAIL.ToString())
                        //{
                        QuotePaymentTransaction quotePaymentTransaction = new QuotePaymentTransaction()
                        {
                            CreatedByID = req.UserId,
                            CreatedDate = _createDate,
                            FKPaymentGatewayID = (req.PaymentGatewayProviderId != 0) ? req.PaymentGatewayProviderId : (int?)null,
                            FKPaymentTypeID = PaymentMethodID,
                            FKQuoteID = req.QuoteID,
                            FKQuoteOnlinePaymentID = _qOLPay > 0 ? _qOLPay : (long?)null,
                            FKReceiptID = _receiptId,
                            FKStatusID = PaymentStatusId,
                            ModifiedByID = req.UserId,
                            ModifiedDate = _createDate,
                            Notes = "Payment Transaction",
                            TotalAmount = req.TotalCarrier + req.TotalNSD,
                            CustomField1 = req.PgTransactionId,
                            CustomField2 = req.AuthCode
                        };
                        db.QuotePaymentTransactions.Add(quotePaymentTransaction);
                        db.SaveChanges();
                        _qpTransId = quotePaymentTransaction.ID;
                        //}



                        db.Auto_QuoteDetail.Where(f => f.FKQuoteID == req.QuoteID).Update(f => new Auto_QuoteDetail() { FKReceiptId = _receiptId });
                        if (req.TotalCarrier > 0)
                        {
                            var PaymentForID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CARRIER.ToString());
                            SavePayment(req.TotalCarrier, req.UserId, req.QuoteID, token, req.PaymentGatewayProviderId, PaymentStatusId, PaymentMethodID,
                                PaymentForID, PolicyTypeID, db, _receiptId, req.AgencyId, _qpTransId, req.PgTransactionId, req.PGCustomerId);
                        }
                        if (req.TotalNSD > 0)
                        {
                            var PaymentForID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.NSD.ToString());
                            SavePayment(req.TotalNSD, req.UserId, req.QuoteID, token, req.PaymentGatewayProviderId, PaymentStatusId, PaymentMethodID,
                                PaymentForID, PolicyTypeID, db, _receiptId, req.AgencyId, _qpTransId, req.PgTransactionId, req.PGCustomerId, true);
                        }
                        if (req.PaymentMethodCode == Enums.PaymentMethod.CREDITCARD.ToString() && req.CCFees > 0)
                        {
                            var PaymentForID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CREDITCARDFEES.ToString());
                            SavePayment(req.CCFees, req.UserId, req.QuoteID, token, req.PaymentGatewayProviderId, PaymentStatusId, PaymentMethodID,
                                PaymentForID, PolicyTypeID, db, _receiptId, req.AgencyId, _qpTransId, req.PgTransactionId, req.PGCustomerId);

                            db.Auto_RateHistory.Where(w => w.FKQuoteID == req.QuoteID).Update(up => new Auto_RateHistory
                            {
                                PGProcessingFees = req.CCFees
                            });
                        }
                        ManagePolicyHistory(req);

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Overloaded Method of Quote Payment for Web hook response
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="pgStatusId"></param>
        /// <param name="quoteId"></param>
        public bool UpdateQuotePayment(string transactionId, string status)
        {
            bool isPaymentUpdate = false;
            GeneralServices generalServices = new GeneralServices();
            long pgStatusId = 0;
            long quoteId = 0;

            if (status != string.Empty)
            {
                var pgStatus = GetAuthorizeStatus(status);
                if (!string.IsNullOrEmpty(pgStatus))
                {
                    pgStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.AUTHORIZENETPGSTATUS.ToString(), pgStatus);
                }
            }

            if (pgStatusId > 0)
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    quoteId = db.QuotePayments.Where(x => x.PGTransactionID == transactionId).Select(x => x.FKQuoteID).FirstOrDefault();
                    if (db.QuotePayments.Where(x => x.FKQuoteID == quoteId && x.PGTransactionID == transactionId).Any())
                    {
                        if (quoteId > 0)
                        {
                            db.QuotePayments.Where(x => x.FKQuoteID == quoteId && x.PGTransactionID == transactionId).Update(x => new QuotePayment()
                            {
                                FKPGCurrentStatusID = pgStatusId,
                                PGStatusModifiedDate = ConvertTo.GetEstTimeNow()
                            });
                            isPaymentUpdate = true;
                        }
                    }
                }
            }
            return isPaymentUpdate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public int BillingAgencyId(long quoteId)
        {
            int agencyID = 0;
            using (var db = new ArrowheadPOSEntities())
            {
                agencyID = (from q in db.Quotes
                            join o in db.Offices on q.FkOfficeId equals o.ID
                            where q.ID == quoteId
                            select o.FKAgencyID).FirstOrDefault();
            }
            return agencyID;
        }

        /// <summary>
        /// Update Quote Payment Status By Quote Id 
        /// </summary>
        /// <param name="response"></param>
        public void UpdateQuotePaymentStatus(PaymentProcessResponseModel _respModel, long QuoteId, string response)
        {
            try
            {
                GeneralServices _generalServices = new GeneralServices();
                long PaymentStatusId = 0;
                long PaymentVoidStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.VOID.ToString());

                // Fail then VOID 
                if (response == Enums.PAYMENTSTATUS.FAIL.ToString())
                {
                    PaymentStatusId = PaymentVoidStatusId;
                }
                else
                {
                    PaymentStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), response);
                }
                using (var db = new ArrowheadPOSEntities())
                {
                    db.QuotePayments.Where(x => x.FKQuoteID == QuoteId && x.FKPaymentStatusID != PaymentVoidStatusId).Update(f => new QuotePayment()
                    {
                        FKPaymentStatusID = PaymentStatusId,
                        PGTransactionID = _respModel.TransactionID
                    });

                    db.QuotePaymentReceipts.Where(x => x.FKQuoteID == QuoteId && x.FKStatusId != PaymentVoidStatusId).Update(f => new QuotePaymentReceipt()
                    {
                        FKStatusId = PaymentStatusId,
                        ModifiedDate = ConvertTo.GetEstTimeNow()
                    });

                    db.QuotePaymentTransactions.Where(x => x.FKQuoteID == QuoteId && x.FKStatusID != PaymentVoidStatusId).Update(f => new QuotePaymentTransaction()
                    {
                        FKStatusID = PaymentStatusId,
                        ModifiedDate = ConvertTo.GetEstTimeNow(),
                        CustomField2 = _respModel.AuthCode // update auth Code 
                    });

                    db.QuoteOnlinePayments.Where(x => x.FkquoteId == QuoteId && x.FkPaymentStatusId != PaymentVoidStatusId).Update(h => new QuoteOnlinePayment()
                    {
                        FkPaymentStatusId = PaymentStatusId,
                        PgTransactionId = _respModel.TransactionID
                    });
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private static void SavePayment(decimal _amount, long userId,
            long QuoteId, string token, int PaymentGatewayProviderID, long PaymentStatusId, long PaymentMethodID, long PaymentForID,
            long PolicyTypeID, ArrowheadPOSEntities db, long _receiptId, long _agencyId,
            long _qpTransId, string _pgTransactionId = "", string _pgCustId = "", bool _IsNSD = false)
        {
            GeneralServices general = new GeneralServices();
            QuotePayment payment = new QuotePayment()
            {
                BilledAmount = Convert.ToDecimal(_amount),
                CreatedBy = userId,
                CreatedDate = ConvertTo.GetEstTimeNow(),
                FKPaymentForID = PaymentForID,
                FKPaymentGateWayID = PaymentGatewayProviderID,
                FKPaymentMethodID = PaymentMethodID,
                FKPaymentStatusID = PaymentStatusId,
                FKQuoteID = QuoteId,
                FKReceiptId = _receiptId,
                PaidAmount = Convert.ToDecimal(_amount),
                PaymentDate = ConvertTo.GetEstTimeNow(),
                FKPolicyType = PolicyTypeID,
                TransactionToken = token,
                PGTransactionID = _pgTransactionId,
                CustomField1 = _pgCustId
            };
            db.QuotePayments.Add(payment);
            db.SaveChanges();


            var _PaymentForID = general.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.CREDITCARDFEES.ToString());

            if (PaymentForID != _PaymentForID && _qpTransId > 0)
            {
                decimal _RFCost = 0;
                decimal _AgencyCost = 0;
                if (_IsNSD)
                {
                    var _NSDPlan = (from qpnsd in db.QuoteNSDPlans
                                    join nps in db.NSD_PlanDetails on qpnsd.PlanId equals nps.PlanNo
                                    join npv in db.NSD_PlanValues on nps.ID equals npv.FKNSDPlanDetailsId
                                    where qpnsd.FKQuoteId == QuoteId && npv.Value1 == qpnsd.Price
                                    select new { npv.RFCost, npv.AgentGross, qpnsd.CreatedDate }).OrderByDescending(o => o.CreatedDate).FirstOrDefault();
                    if (_NSDPlan != null)
                    {
                        _RFCost = _NSDPlan.RFCost;
                        _AgencyCost = _NSDPlan.AgentGross;
                    }
                }
                QuotePaymentTransactionDetail _qptDetails = new QuotePaymentTransactionDetail()
                {
                    Amount = _amount,
                    FKAgencyID = (int)_agencyId,
                    FKPaymentForID = PaymentForID,
                    FKQuotePayTransID = _qpTransId,
                    IsSettled = false,
                    IsSuccess = true,
                    RFCost = _RFCost,
                    AgencyCost = _AgencyCost
                };
                db.QuotePaymentTransactionDetails.Add(_qptDetails);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        public void ManageEcheck(PaymentRequestModel req)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var _createDate = ConvertTo.GetEstTimeNow();

                var _bankName = CryptoGrapher.EncryptText(req.BankName, ConstantVariables.AgencyEncryptionKey.ToString());
                var _routingNumber = CryptoGrapher.EncryptText(req.RoutingNumber, ConstantVariables.AgencyEncryptionKey.ToString());
                var _accountNumber = CryptoGrapher.EncryptText(req.AccountNumber, ConstantVariables.AgencyEncryptionKey.ToString());

                try
                {
                    long customerId = db.Quotes.Where(w => w.ID == req.QuoteID).Select(s => s.FkCustomerID).FirstOrDefault();
                    QuoteEcheck quoteEcheck = new QuoteEcheck
                    {
                        AccountHolderName = req.FullName,
                        FKAccountTypeId = req.AccountTypeId,
                        FkCustomerId = customerId,
                        FkQuoteId = req.QuoteID,
                        BankName = _bankName,
                        BankAddress = req.BankAddress,
                        AccountNumber = _accountNumber,
                        RoutingNumber = _routingNumber,
                        CreatedDate = _createDate,
                        CheckNumber = req.CheckNumber,
                        ModifiedDate = _createDate,
                        ModifiedById = req.UserId,
                        CreatedById = req.UserId
                    };
                    db.QuoteEchecks.Add(quoteEcheck);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void ManagePolicyHistory(PaymentRequestModel model)
        {
            decimal? nsdPlanPrice = 0;
            using (var db = new ArrowheadPOSEntities())
            {
                var currentDate = ConvertTo.GetEstTimeNow();
                try
                {
                    var groupTransactionId = Guid.NewGuid();
                    GeneralServices generalServices = new GeneralServices();
                    var CreatedDate = ConvertTo.GetEstTimeNow();
                    var PolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.ACTIVE.ToString());
                    var insuranceCompanyID = db.InsuranceCompanies.Where(f => f.Code == "ARROWHEAD").FirstOrDefault().ID;
                    var quote = db.Quotes.Where(w => w.ID == model.QuoteID).FirstOrDefault();
                    var rateHistoryID = db.Auto_QuoteDetail.Where(f => f.FKQuoteID == model.QuoteID).FirstOrDefault().FKRateHistoryID;
                    var rateHistoryDetails = db.Auto_RateHistory.Where(f => f.ID == rateHistoryID).FirstOrDefault();

                    if (model.TotalNSD > 0)
                    {
                        nsdPlanPrice = db.QuoteNSDPlans.Where(x => x.FKQuoteId == model.QuoteID).FirstOrDefault().Price;
                    }
                    PolicyHistory policyHistory = new PolicyHistory()
                    {
                        FkQuoteId = model.QuoteID,
                        FkPolicyStatusId = PolicyStatusId,
                        FkInsuranceCompanyId = insuranceCompanyID,
                        PolicyNumber = null,
                        EffectiveDate = quote.EffectiveDate,
                        ExpirationDate = quote.ExpirationDate,
                        PolicyTerm = quote.PolicyTerm,
                        Downpayment = rateHistoryDetails.DownPayment,
                        MontlyPayment = rateHistoryDetails.MonthllyPayment,
                        TotalPremium = rateHistoryDetails.Premium,
                        BridgeUrl = rateHistoryDetails.BridgeURL,
                        NSDFee = nsdPlanPrice != null ? nsdPlanPrice ?? 0 : rateHistoryDetails.NSDFee,
                        OtherFee = rateHistoryDetails.OtherFee,
                        NumberOfPayment = Convert.ToInt32(rateHistoryDetails.NumberOfPayment),
                        CreatedBy = model.UserId,
                        CreatedDate = CreatedDate,
                        DueDay = 0,
                        Description = rateHistoryDetails.Description,
                        FkSourceId = quote.FkSourceID
                    };
                    db.PolicyHistories.Add(policyHistory);
                    db.SaveChanges();

                    db.Quotes.Where(w => w.ID == model.QuoteID).Update(U => new Quote()
                    {
                        FkPolicyHistoryID = policyHistory.ID,
                        FkPolicyStatusId = PolicyStatusId,
                        PolicyNumber = null,
                        //PolicyTerm = quote.PolicyTerm,
                        FkSourceID = quote.FkSourceID,
                        PolicyStatusChangeDate = currentDate
                    });
                }
                catch (Exception ex)
                {
                }
            }
        }

        /// <summary>
        /// Get Payment Details by Quote Id
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public PaymentDetailModel GetPaymentDetailByQuoteId(long quoteId)
        {
            PaymentDetailModel paymentDetailModel = new PaymentDetailModel();
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                long paymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                long paymentStatusInti = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                try
                {
                    paymentDetailModel = (from qp in db.QuotePayments
                                          join gst in db.GeneralStatusTypes on qp.FKPaymentMethodID equals gst.ID
                                          join gstst in db.GeneralStatusTypes on qp.FKPaymentStatusID equals gstst.ID
                                          where qp.FKQuoteID == quoteId && (qp.FKPaymentStatusID == paymentStatusId || qp.FKPaymentStatusID == paymentStatusInti)
                                          select new PaymentDetailModel
                                          {
                                              QuoteID = quoteId,
                                              PaymentMethodCd = gst.Code,
                                              PGTransactionId = qp.PGTransactionID,
                                              ReceiptId = qp.FKReceiptId,
                                              PaymentStatus = gstst.Code
                                          }
                                         ).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }
                return paymentDetailModel;
            }
        }

        public QuoteEcheck GetEcheckDetailsByQuotesId(long quoteId)
        {
            QuoteEcheck quoteEcheck = new QuoteEcheck();
            if (quoteId > 0)
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    try
                    {
                        quoteEcheck = db.QuoteEchecks.Where(x => x.FkQuoteId == quoteId).FirstOrDefault();
                        quoteEcheck.BankName = CryptoGrapher.DecryptText(quoteEcheck.BankName, ConstantVariables.AgencyEncryptionKey.ToString());
                        quoteEcheck.RoutingNumber = CryptoGrapher.DecryptText(quoteEcheck.RoutingNumber, ConstantVariables.AgencyEncryptionKey.ToString());
                        quoteEcheck.AccountNumber = CryptoGrapher.DecryptText(quoteEcheck.AccountNumber, ConstantVariables.AgencyEncryptionKey.ToString());

                        //--Encryption--
                        var _bankName = CryptoGrapher.EncryptText(quoteEcheck.BankName, ConstantVariables.BindPolicyEncryptionKey.ToString());
                        var _routingNumber = CryptoGrapher.EncryptText(quoteEcheck.RoutingNumber, ConstantVariables.BindPolicyEncryptionKey.ToString());
                        var _accountNumber = CryptoGrapher.EncryptText(quoteEcheck.AccountNumber, ConstantVariables.BindPolicyEncryptionKey.ToString());

                        quoteEcheck.BankName = _bankName;
                        quoteEcheck.RoutingNumber = _routingNumber;
                        quoteEcheck.AccountNumber = _accountNumber;

                        ////--Decryption--
                        //var _bankName1 = CryptoGrapher.DecryptText(_bankName, ConstantVariables.BindPolicyEncryptionKey.ToString());
                        //var _routingNumber1 = CryptoGrapher.DecryptText(_routingNumber, ConstantVariables.BindPolicyEncryptionKey.ToString());
                        //var _accountNumber1 = CryptoGrapher.DecryptText(_accountNumber, ConstantVariables.BindPolicyEncryptionKey.ToString());
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return quoteEcheck;
        }

        /// <summary>
        /// Update Void Status in Quote Payment Receipt
        /// </summary>
        /// <param name="receiptId"></param>
        /// <param name="quoteId"></param>
        public void UpdateVoidStatusByReceiptId(long receiptId)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                long paymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.VOID.ToString());

                db.QuotePaymentReceipts.Where(w => w.ID == receiptId).Update(u => new QuotePaymentReceipt
                {
                    FKStatusId = paymentStatusId
                });

                db.QuotePaymentTransactions.Where(w => w.FKReceiptID == receiptId).Update(u => new QuotePaymentTransaction
                {
                    FKStatusID = paymentStatusId
                });

                db.QuotePayments.Where(w => w.FKReceiptId == receiptId).Update(u => new QuotePayment
                {
                    FKPaymentStatusID = paymentStatusId
                });

            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public NewPolicyModel GetNewPolicyDetails(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var _newPolicy = (from q in db.Quotes
                                  let c = (db.Customers.FirstOrDefault(f => f.ID == q.FkCustomerID))
                                  let o = (db.Offices.FirstOrDefault(f => f.ID == q.FkOfficeId))
                                  let a = (db.Agencies.FirstOrDefault(f => f.ID == o.FKAgencyID))
                                  let state = (db.States.FirstOrDefault(f => f.ID == q.FkStateId))
                                  where q.ID == quoteId
                                  select new
                                  {
                                      q,
                                      c,
                                      a,
                                      state
                                  }).FirstOrDefault();
                var _projectTerm = (from pt in db.ProjectTerms select pt).ToList();


                if (!string.IsNullOrWhiteSpace(_newPolicy.a.ContactNo))
                {
                    string agencyContactNo = ConvertTo.PhoneNumberFormatting(_newPolicy.a.ContactNo);
                }
                else
                {
                    string agencyContactNo = string.Empty;
                }
                NewPolicyModel newPolicyModel = new NewPolicyModel
                {
                    AgencyName = _newPolicy.a.Name,
                    PolicyNumber = _newPolicy.q.PolicyNumber,
                    AgencyPhone = !string.IsNullOrEmpty(_newPolicy.a.ContactNo) ? ConvertTo.PhoneNumberFormatting(_newPolicy.a.ContactNo) : "",
                    FirstName = _newPolicy.c.FirstName,
                    LastName = _newPolicy.c.LastName,
                    EffectiveDate = Convert.ToDateTime(Convert.ToDateTime(_newPolicy.q.EffectiveDate).ToString("MM/dd/yyyy")),
                    ExpirationDate = Convert.ToDateTime(Convert.ToDateTime(_newPolicy.q.ExpirationDate).ToString("MM/dd/yyyy")),
                    State = _newPolicy.state.Code,

                };


                var _rateDetails = (from aq in db.Auto_QuoteDetail
                                    join rt in db.Auto_RateHistory on aq.FKRateHistoryID equals rt.ID
                                    where aq.FKQuoteID == quoteId
                                    select new SchedulePaymentModel
                                    {
                                        MonthlyPayments = rt.MonthllyPayment,
                                        DueDate = (DateTime)(rt.InstallmentDueDate),
                                        NumberOfPayments = (int)rt.NumberOfPayment
                                    }).FirstOrDefault();

                List<SchedulePaymentModel> _listSchedule = new List<SchedulePaymentModel>();
                for (int i = 1; i < _rateDetails.NumberOfPayments; i++)
                {
                    SchedulePaymentModel schedulePaymentModel = new SchedulePaymentModel();
                    schedulePaymentModel.DueDate = Convert.ToDateTime(Convert.ToDateTime(_rateDetails.DueDate.AddMonths(i - 1).ToString("MM/dd/yyyy")));
                    schedulePaymentModel.MonthlyPayments = _rateDetails.MonthlyPayments;
                    schedulePaymentModel.NumberOfPayments = i;
                    _listSchedule.Add(schedulePaymentModel);
                }

                List<VehicleIDCardModel> vehicleIDCardModels = new List<VehicleIDCardModel>();
                var vehicleIDList = (from ad in db.Auto_Vehicle
                                     let lienholder = (db.LienHolders.Where(w => w.ID == ad.FkLienHolderID).FirstOrDefault())
                                     where ad.FkQuoteID == quoteId && ad.IsDeleted == false
                                     select new VehicleIDCardModel()
                                     {
                                         AutoVehicleId = ad.ID,
                                         QuoteId = ad.FkQuoteID,
                                         Year = ad.Year,
                                         Make = ad.Make,
                                         Vin = ad.VIN,
                                         Model = ad.Model,
                                         VehicleOrderId = ad.VehicleOrderID,

                                     }).ToList();

                newPolicyModel.VehicleIDCardModel = vehicleIDList;

                newPolicyModel.SchedulePaymentModel = _listSchedule;



                return newPolicyModel;
            }

        }

        /// <summary>
        /// Reset Quote Payment by Quote ID 
        /// </summary>
        /// <param name="quoteId"></param>
        public void ResetQuotePaymentByQuoteId(long quoteId)
        {
            GeneralServices generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long paymentSuccessStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    long paymentVoidStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.VOID.ToString());

                    db.QuotePayments.Where(x => x.FKQuoteID == quoteId && x.FKPaymentStatusID == paymentSuccessStatusId).Update(u => new QuotePayment
                    {
                        FKPaymentStatusID = paymentVoidStatusId
                    });
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateAutoRateHistory(PaymentRequestModel model)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var rateHistoryDetails = db.Auto_QuoteDetail.Where(f => f.FKQuoteID == model.QuoteID).FirstOrDefault();
                    var rateHistory = db.Auto_RateHistory.Where(w => w.ID == rateHistoryDetails.FKRateHistoryID).FirstOrDefault();
                    db.Auto_RateHistory.Where(w => w.ID == rateHistoryDetails.FKRateHistoryID).Update(u => new Auto_RateHistory
                    {
                        DownPayment = (model.Amount - rateHistory.NSDFee - model.CCFees),
                    });

                    decimal actualDP = model.Amount - model.TotalNSD - model.CCFees;
                    decimal remainingAmount = rateHistory.Premium + ConvertTo.Decimal(rateHistory.InstallmenetFee) - actualDP;
                    if (rateHistory.NumberOfPayment > 1)
                    {
                        int numberofPayments = (int)rateHistory.NumberOfPayment - 1;
                        decimal finallyMonthlyAmount = (remainingAmount / numberofPayments) + ConvertTo.Decimal(rateHistory.InstallmenetFee);
                        db.Auto_RateHistory.Where(w => w.ID == rateHistoryDetails.FKRateHistoryID).Update(u => new Auto_RateHistory
                        {
                            MonthllyPayment = finallyMonthlyAmount
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void UpdateAutoRateHistoryPGFees(long _quoteID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.Auto_RateHistory.Where(w => w.FKQuoteID == _quoteID).Update(up => new Auto_RateHistory
                {
                    PGProcessingFees = 0
                });
            }
        }

        public decimal GetNSDAmount(long _quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (from arh in db.Auto_RateHistory
                        join aqd in db.Auto_QuoteDetail on arh.ID equals aqd.FKRateHistoryID
                        where aqd.FKQuoteID == _quoteId
                        select new { arh, aqd }).Select(x => x.arh.NSDFee).FirstOrDefault();
            }
        }


        public bool IsNSDExists(long _quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.QuoteNSDPlans.Where(qp => qp.FKQuoteId == _quoteId).Any();
            }
        }

        /// <summary>
        /// Verify NSD Contract Exist 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public bool IsNSDContractNotExist(long QuoteId)
        {
            bool isNSDContractExist = false;
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    isNSDContractExist = (from q in db.Quotes
                                          join qnp in db.QuoteNSDPlans on q.ID equals qnp.FKQuoteId
                                          where qnp.FKQuoteId == QuoteId && (qnp.IsSuccess == false || qnp.IsSuccess == null) && q.PolicyNumber != null
                                          select new { q.ID }).Any();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isNSDContractExist;
        }

        public bool IsNSDCompleted(long _quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.QuoteNSDPlans.Where(qp => qp.FKQuoteId == _quoteId && qp.IsSuccess.HasValue && qp.IsSuccess.Value).Any();
            }
        }

        public decimal GetDownPaymentByQuoteId(long _quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (from arh in db.Auto_RateHistory
                        join aqd in db.Auto_QuoteDetail on arh.ID equals aqd.FKRateHistoryID
                        where aqd.FKQuoteID == _quoteId
                        select new { arh, aqd }).Select(x => x.arh.DownPayment).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get Pay Plan By QuoteId 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public string GetPayPlanByQuoteId(long QuoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (from arh in db.Auto_RateHistory
                        join aqd in db.Auto_QuoteDetail on arh.ID equals aqd.FKRateHistoryID
                        where aqd.FKQuoteID == QuoteId
                        select arh.PayPlanID).FirstOrDefault();

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        public void PolicyNumberUpdate(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.Quotes.Where(w => w.ID == quoteId).Update(u => new Quote
                {
                    PolicyNumber = null,
                    ModifiedDate = ConvertTo.GetEstTimeNow()
                });
            }

        }

        public void UpdatePolicyStatusAfterVoid(long quoteid)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                long fkpolicyStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.POLICYSTATUS.ToString(), Enums.PolicyStatus.VOID.ToString());
                long fkQuoteNewID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.NEW.ToString());
                long FKCurrentStepId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QUOTEORDERSTATUS.POLICY.ToString());

                db.Quotes.Where(w => w.ID == quoteid).Update(u => new Quote { FkQuoteStatusID = fkQuoteNewID, FkPolicyStatusId = fkpolicyStatusId });

            }
        }

        public AutoDriverModel GetSr22InsuredDriver(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                var insuredId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.INSURED.ToString());
                return (db.Auto_Driver.Where(w => w.FkQuoteID == quoteId && w.FkRelationCdID == insuredId && w.IsSR22 == true).Select(
                    s => new AutoDriverModel
                    {
                        AutoDriverId = s.ID,
                    }
                    ).FirstOrDefault());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public AutoDriverModel GetSr22AInsuredDriver(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                GeneralServices generalServices = new GeneralServices();
                var insuredId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.INSURED.ToString());
                return (db.Auto_Driver.Where(w => w.FkQuoteID == quoteId && w.FkRelationCdID == insuredId && w.IsSR22A == true).Select(
                    s => new AutoDriverModel
                    {
                        AutoDriverId = s.ID,
                    }
                    ).FirstOrDefault());
            }
        }

        public string GetPaymentTransactionID(long quoteId, long receiptId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return db.QuoteOnlinePayments.Where(x => x.FkquoteId == quoteId && x.FkreceiptId == receiptId).Select(x => x.PgTransactionId).FirstOrDefault();
            }
        }

        /// <summary>
        /// Encrypt and Save Bank Details
        /// </summary>
        /// <param name="bankDetailModel"></param>
        /// <param name="agencyID"></param>
        /// <returns></returns>
        public bool SaveBankDetails(BankDetailModel bankDetailModel, long agencyID)
        {
            bool result = false;
            string secretKey = ConstantVariables.AgencyEncryptionKey.ToString();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    AgencyBankDetail agencyBankDetail = new AgencyBankDetail
                    {
                        FKAgencyID = Convert.ToInt32(agencyID),
                        ACHolderName = CryptoGrapher.EncryptText(bankDetailModel.AccountHolderName, secretKey),
                        FKACHolderType = CryptoGrapher.EncryptText(bankDetailModel.AccountTypeId.ToString(), secretKey),
                        ACNumber = CryptoGrapher.EncryptText(bankDetailModel.AccountNumber, secretKey),
                        BankRoutingNumber = CryptoGrapher.EncryptText(bankDetailModel.RoutingNumber, secretKey),
                        BankName = CryptoGrapher.EncryptText(bankDetailModel.BankName, secretKey)
                    };
                    db.AgencyBankDetails.Add(agencyBankDetail);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Check Quote Plan Selected or not used in MVR 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public bool IsQuotePlanNotSelected(long quoteId)
        {
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    return (db.Auto_QuoteDetail.Where(x => x.FKQuoteID == quoteId && x.FKRateHistoryID == null).Any());
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        /// <summary>
        /// Verify Quote is in Payment or in Underwriting Question Step
        /// </summary>
        /// <param name="quoteID"></param>
        /// <param name="currentStepCode"></param>
        public void IsQuotePaymentStep(long quoteID)
        {
            GeneralServices _generalServices = new GeneralServices();
            AutoQuoteServices _autoQuoteServices = new AutoQuoteServices();
            long _coverageStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QuoteOrderStatus.COVERAGE.ToString());
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    var currentStep = _autoQuoteServices.GetCurrentStepDetails(quoteID);
                    if (currentStep.CusrrentStepId < 5) // Underwriting Question
                    {
                        //db.Quotes.Where(w => w.ID == quoteID).Update(U => new Quote
                        //{
                        //    FKCurrentStepId = _coverageStatusId
                        //});
                    }
                    //if(quoteStep)
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Resign Policy 
        /// </summary>
        /// <param name="QuoteId"></param>
        public void ReProcessDocusign(long QuoteId)
        {
            GeneralServices _generalServices = new GeneralServices();
            try
            {
                long quoteStatusID = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTESTATUS.ToString(), Enums.QuoteStatus.NEW.ToString());
                long _policyStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTEORDERSTATUS.ToString(), Enums.QuoteOrderStatus.POLICY.ToString());
                long _envelopeStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.ENVELOPESTATUS.ToString(), Enums.EnvelopeStatus.ESIGNCREATED.ToString());

                using (var db = new ArrowheadPOSEntities())
                {
                    db.Quotes.Where(x => x.ID == QuoteId).Update(y => new Quote()
                    {
                        FkQuoteStatusID = quoteStatusID,
                        FKCurrentStepId = _policyStatusId
                    });

                    db.QuoteEnvelopes.Where(x => x.FKQuoteId == QuoteId).Update(f => new QuoteEnvelope()
                    {
                        FKEnvelopeStatusId = _envelopeStatusId
                    });
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Get Payment Gateway by QuoteID 
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public string GetPaymentGatewayByQuoteId(long QuoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                return (from qp in db.QuotePayments
                        join pgp in db.PaymentGatewayProviders on qp.FKPaymentGateWayID equals pgp.Id
                        where qp.FKQuoteID == QuoteId
                        select pgp.Code).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get Initiated Payment transaction ID 
        /// </summary>
        /// <returns></returns>
        public List<PaymentTransactionModel> GetPaymentTransactionList()
        {
            GeneralServices _generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long paymentStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                    long paymentMethodStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.ECHECK.ToString());

                    var _paymentList = (from qp in db.QuotePayments
                                        join qpr in db.QuotePaymentReceipts on qp.FKReceiptId equals qpr.ID
                                        join pg in db.PaymentGatewayProviders on qp.FKPaymentGateWayID equals pg.Id
                                        where qp.FKPaymentMethodID == paymentMethodStatusId && qp.FKPaymentStatusID == paymentStatusId && pg.Code == Enums.PaymentGateway.AUTHORIZENET.ToString()
                                        select new PaymentTransactionModel()
                                        {
                                            PgTransactionID = qp.PGTransactionID,
                                            QuoteId = qp.FKQuoteID,
                                            ChargeAmount = qpr.Amount
                                        }).Distinct().ToList();

                    return _paymentList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentTransactionModel> GetPaymentValidationTransactionList()
        {
            GeneralServices _generalServices = new GeneralServices();
            try
            {
                using (var db = new ArrowheadPOSEntities())
                {
                    long paymentInitStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                    long paymentSuccessStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                    DateTime yesterdayDate = ConvertTo.GetEstTimeNow().AddDays(-1);


                    var startDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 0, 0, 0);
                    var endDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 23, 59, 59);

                    var _paymentList = (from qpt in db.QuotePaymentTransactions
                                        join gs in db.GeneralStatusTypes on qpt.FKPaymentTypeID equals gs.ID
                                        where (qpt.FKStatusID == paymentInitStatusId || qpt.FKStatusID == paymentSuccessStatusId)
                                        && gs.Code != Enums.PaymentMethod.SWEEP.ToString()
                                        && qpt.CreatedDate >= startDateFilter && qpt.CreatedDate <= endDateFilter && !string.IsNullOrEmpty(qpt.CustomField1)
                                        select new PaymentTransactionModel()
                                        {
                                            PGID = qpt.FKPaymentGatewayID != null ? (int)qpt.FKPaymentGatewayID : 0,
                                            PgTransactionID = qpt.CustomField1,
                                            QuoteId = qpt.FKQuoteID,
                                            PaymentMethod = gs.Code,
                                            ChargeAmount = db.QuotePaymentReceipts.Where(w => (w.FKStatusId == paymentSuccessStatusId || w.FKStatusId == paymentInitStatusId) && w.FKQuoteID == qpt.FKQuoteID)
                                                                                  .Select(s => s.Amount)
                                                                                  .FirstOrDefault()
                                        }).ToList();

                    return _paymentList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get Auth Code for Authorize.Net pass in Bind XML
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public string GetAuthCodeByQuoteId(long QuoteId)
        {
            GeneralServices _generalServices = new GeneralServices();
            try
            {
                long paymentSuccessStatus = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                long paymentInitiatedStatus = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.INITIATED.ToString());
                long paymentECheckStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.ECHECK.ToString());
                long paymentCCStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.CREDITCARD.ToString());

                using (var db = new ArrowheadPOSEntities())
                {
                    var authCode = (from
                                     qpt in db.QuotePayments
                                    where qpt.FKQuoteID == QuoteId && ((qpt.FKPaymentStatusID == paymentSuccessStatus && qpt.FKPaymentMethodID == paymentCCStatusId) || (qpt.FKPaymentStatusID == paymentInitiatedStatus && qpt.FKPaymentMethodID == paymentECheckStatusId))
                                    select qpt.PGTransactionID).FirstOrDefault();
                    return authCode;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertTransactionVerification(TransactionVerification _model)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.TransactionVerifications.Add(_model);
                db.SaveChanges();
            }
        }
        public void SendDailySalesMismatchValueEmail()
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                DateTime yesterdayDate = ConvertTo.GetEstTimeNow();
                var startDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 0, 0, 0);
                var endDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 23, 59, 59);

                var _mismatchAmountList =
                    db.TransactionVerifications.Where(w => w.PGChargeValue != w.ActualChargeValue
                    && w.CreatedDate >= startDateFilter && w.CreatedDate <= endDateFilter).ToList();



                string _emailBody = "The Authorize.net settlement amount did not match the daily sales amount <br/><br/><br/>";
                _emailBody += "<table style='border: 1px solid black;text-align :center;width:50%'><tr><th>QuoteID</th><th>PG TransID</th> <th>PG Value</th><th>Policy Value</th><th>Total Variation</th></tr>";

                foreach (var item in _mismatchAmountList)
                {
                    decimal _diff = item.ActualChargeValue - item.PGChargeValue;
                    string _bgColor = _diff > 0 ? "lightgreen" : "lightcoral";
                    _emailBody += "<tr style='background-color:" + _bgColor + "'><td>" + item.FKQuoteID + "</td><td>" + item.PGTransID + "</td><td>$" + item.PGChargeValue + "</td><td>$" + item.ActualChargeValue + "</td><td>" + _diff + "</td> </tr>";
                }
                _emailBody += "</table>";

                var officeSMTPlDetail = generalServices.GetSmtpDetails();
                _ = MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, ConstantVariables.PGErrorNotifyEmail, "", "", "Arrowhead Daily Settlement ERROR", _emailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, null, null);
            }
        }
        /// <summary>
        /// Get Transaction Amount Verification Report
        /// </summary>
        /// <param name="templateModel"></param>
        /// <returns></returns>
        public string GetTransactionAmountReportTemplate(TemplateModel templateModel)
        {
            List<VerificationModel> _lstVerificationModel = new List<VerificationModel>();
            TransactionVerificationModel _transactionVerificationModel = new TransactionVerificationModel();
            TemplateModel _template = templateModel;
            Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
            string template = string.Empty;

            using (var db = new ArrowheadPOSEntities())
            {
                DateTime yesterdayDate = ConvertTo.GetEstTimeNow();
                var startDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 0, 0, 0);
                var endDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 23, 59, 59);

                var _mismatchAmountList =
                    db.TransactionVerifications.Where(w => w.PGChargeValue != w.ActualChargeValue
                    && w.CreatedDate >= startDateFilter && w.CreatedDate <= endDateFilter).ToList();

                foreach (var item in _mismatchAmountList)
                {
                    decimal _diff = item.ActualChargeValue - item.PGChargeValue;
                    string _bgColor = _diff > 0 ? "lightgreen" : "lightcoral";

                    VerificationModel _VerificationModel = new VerificationModel
                    {
                        BgColor = _bgColor,
                        FKQuoteID = item.FKQuoteID.ToString(),
                        PGTransID = item.PGTransID,
                        PGChargeValue = item.PGChargeValue,
                        ActualChargeValue = item.ActualChargeValue,
                        diff = _diff
                    };
                    _lstVerificationModel.Add(_VerificationModel);
                }

                //Set Heading
                _transactionVerificationModel.Heading = "The Authorize.net settlement amount did not match the daily sales amount";
                // Set Verification Model List 
                _transactionVerificationModel.VerificationModel = _lstVerificationModel;

                try
                {
                    if (_transactionVerificationModel.VerificationModel != null && _transactionVerificationModel.VerificationModel.Count > 0)
                    {
                        string razorTemplate = _template.EmailTemplate;
                        string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, _transactionVerificationModel);
                        if (!String.IsNullOrEmpty(razorTemplateString))
                        {
                            return razorTemplateString;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //CommonFunctions.OnException(Enums.AuditHistoryModule.T.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                    throw ex;
                }
            }
            return string.Empty;
        }


        /// <summary>
        /// Notify Arrowhead for E-Check Failed,Refund,Void
        /// </summary>
        //public void SendArrowheadNotificationECheckValueEmail(string transId, long quoteId, string chargeAmount, string PgValue)
        //{
        //    GeneralServices _generalServices = new GeneralServices();
        //    using (var db = new ArrowheadPOSEntities())
        //    {
        //        string _bgColor = "lightgreen";
        //        string _emailBody = "The Authorize.net failed settlement for E-Check <br/><br/><br/>";

        //        _emailBody += "<table style='border: 1px solid black;text-align :center;width:50%'><tr><th>QuoteID</th><th>PG TransID</th> <th>PG Value</th><th>Policy Value</th></tr>";
        //        _emailBody += "<tr style='background-color:" + _bgColor + "'><td>" + quoteId + "</td><td>" + transId + "</td><td>$" + chargeAmount + "</td><td>$" + PgValue + "</td></tr>";
        //        _emailBody += "</table>";

        //        var officeSMTPlDetail = _generalServices.GetSmtpDetails();
        //        _ = MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, "", "", "", "Arrowhead E-Check Settlement Error Report", _emailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, null, null); //ConstantVariables.PGErrorNotifyEmail
        //    }
        //}

        /// <summary>
        /// Get E-Check Report Template
        /// </summary>
        /// <param name="templateModel"></param>
        /// <param name="PgTransId"></param>
        /// <returns></returns>
        public string GetTransactionECheckReportTemplate(TemplateModel templateModel, string PgTransId)
        {
            GeneralServices _generalServices = new GeneralServices();
            AutoDriverServices _autoDriverServices = new AutoDriverServices();
            TransactionVerificationModel _transVerificationModel = new TransactionVerificationModel();
            List<VerificationModel> _lstVerificationModel = new List<VerificationModel>();
            TemplateModel _template = templateModel;
            try
            {
                var verificationTypeId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.VERIFICATIONTYPE.ToString(), Enums.VerificationType.ECHECKAMOUNTVERIFICATION.ToString());
                string _bgColor = "lightcoral"; // default bg Color 

                using (var db = new ArrowheadPOSEntities())
                {
                    var _paymentTransactionDetail = db.TransactionVerifications.Where(x => x.PGTransID == PgTransId && x.FKVerificationTypeID == verificationTypeId).ToList();
                    long quoteId = _paymentTransactionDetail.Select(x => x.FKQuoteID).FirstOrDefault();
                    var _customerModel = _autoDriverServices.GetCustomerDetailsByQuoteId(quoteId);

                    foreach (var items in _paymentTransactionDetail)
                    {
                        VerificationModel _VerificationModel = new VerificationModel();
                        _VerificationModel.BgColor = _bgColor;
                        _VerificationModel.FKQuoteID = items.FKQuoteID.ToString();
                        _VerificationModel.PGTransID = items.PGTransID;
                        _VerificationModel.PGChargeValue = items.PGChargeValue;
                        _VerificationModel.CustomerName = (_customerModel.FirstName + " " + _customerModel.LastName);
                        _lstVerificationModel.Add(_VerificationModel);
                    }

                    //Set Heading and Verification Model 
                    _transVerificationModel.Heading = "The Authorize.net failed settlement report for E-Check";

                    // Set Verification Model
                    _transVerificationModel.VerificationModel = _lstVerificationModel;

                    try
                    {
                        if (_transVerificationModel.VerificationModel != null && _transVerificationModel.VerificationModel.Count > 0)
                        {
                            string razorTemplate = _template.EmailTemplate;
                            string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, _transVerificationModel);
                            if (!String.IsNullOrEmpty(razorTemplateString))
                            {
                                return razorTemplateString;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //CommonFunctions.OnException(Enums.AuditHistoryModule.T.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                        throw ex;
                    }

                }
                return string.Empty;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<int?> GetAgencyIDsForSendMail()
        {
            GeneralServices generalServices = new GeneralServices();
            DateTime yesterdayDate = ConvertTo.GetEstTimeNow().AddDays(-1);
            long nsdStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.NSD.ToString());
            long sweepStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.SWEEP.ToString());
            var startDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 0, 0, 0);
            var endDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 23, 59, 59);
            using (var db = new ArrowheadPOSEntities())
            {
                var qpt = db.QuotePaymentTransactions.Where(x => x.CreatedDate >= startDateFilter && x.CreatedDate <= endDateFilter && x.FKPaymentTypeID == sweepStatusID).Select(x => x.ID).ToList();
                var result = db.QuotePaymentTransactionDetails.Where(x => qpt.Contains(x.FKQuotePayTransID) && x.FKPaymentForID == nsdStatusId).Select(s => s.FKAgencyID).Distinct().ToList();
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateModel"></param>
        /// <param name="_agencyID"></param>
        /// <returns></returns>
        public string GetNSDDailySweepTemplate(TemplateModel templateModel, int _agencyID)
        {
            try
            {
                NSDDailySales nSDDailySales = new NSDDailySales();
                GeneralServices generalServices = new GeneralServices();
                long nsdStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTFOR.ToString(), Enums.PAYMENTFOR.NSD.ToString());
                long paymentStatusId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTSTATUS.ToString(), Enums.PAYMENTSTATUS.SUCCESS.ToString());
                long sweepStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.PAYMENTMETHOD.ToString(), Enums.PaymentMethod.SWEEP.ToString());
                using (var db = new ArrowheadPOSEntities())
                {
                    DateTime yesterdayDate = ConvertTo.GetEstTimeNow().AddDays(-1);

                    var startDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 0, 0, 0);
                    var endDateFilter = new DateTime(yesterdayDate.Year, yesterdayDate.Month, yesterdayDate.Day, 23, 59, 59);

                    var qpt = db.QuotePaymentTransactions.Where(x => x.CreatedDate >= startDateFilter && x.CreatedDate <= endDateFilter && x.FKPaymentTypeID == sweepStatusID).Select(x => x.ID).ToList();
                    var qptd = db.QuotePaymentTransactionDetails.Where(x => qpt.Contains(x.FKQuotePayTransID) && x.FKPaymentForID == nsdStatusId && x.FKAgencyID == _agencyID).ToList();

                    nSDDailySales.TotalSales = qptd.Count();
                    nSDDailySales.TotalGrossSales = Convert.ToDouble(qptd.Select(x => x.Amount).Sum());
                    nSDDailySales.ACHDraftAmount = Convert.ToDouble(qptd.Select(x => x.RFCost).Sum());
                    nSDDailySales.DraftDate = yesterdayDate.AddDays(2);
                }
                if (nSDDailySales.TotalSales > 0 || nSDDailySales.TotalGrossSales > 0 || nSDDailySales.ACHDraftAmount > 0)
                {
                    string razorTemplate = templateModel.EmailTemplate;
                    string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, nSDDailySales);
                    if (!String.IsNullOrEmpty(razorTemplateString))
                    {
                        return razorTemplateString;
                    }
                    else
                        return null;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Authorize PG Status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public string GetAuthorizeStatus(string status)
        {
            string pgStatus = string.Empty;

            switch (status)
            {
                case "net.authorize.payment.authorization.created":
                    pgStatus = "AUTHORIZATIONCREATED";
                    break;
                case "net.authorize.payment.authcapture.created":
                    pgStatus = "AUTHCAPTURECREATED";
                    break;
                case "net.authorize.payment.capture.created":
                    pgStatus = "CAPTURECREATED";
                    break;
                case "net.authorize.payment.refund.created":
                    pgStatus = "REFUNDCREATED";
                    break;
                case "net.authorize.payment.priorAuthCapture.created":
                    pgStatus = "PRIORAUTHCAPTURECREATED";
                    break;
                case "net.authorize.payment.void.created":
                    pgStatus = "VOIDCREATED";
                    break;
                case "AUTHORIZATIONCREATED":
                    pgStatus = "Authorize";
                    break;
                case "AUTHCAPTURECREATED":
                    pgStatus = "Authorized & Captured";
                    break;
                case "CAPTURECREATED":
                    pgStatus = "Captured";
                    break;
                case "PRIORAUTHCAPTURECREATED":
                    pgStatus = "Prior Authorized & Captured";
                    break;
                case "REFUNDCREATED":
                    pgStatus = "Refunded";
                    break;
                case "VOIDCREATED":
                    pgStatus = "Voided";
                    break;
                case "FRAUDDECLINED":
                    pgStatus = "Declined";
                    break;
                case "FRAUDHELD":
                    pgStatus = "Transaction was held Suspicious";
                    break;
                case "FRAUDAPPROVED":
                    pgStatus = "Previous Transaction Approved";
                    break;
                default:
                    pgStatus = string.Empty;
                    break;
            }

            return pgStatus;
        }

    }

}

