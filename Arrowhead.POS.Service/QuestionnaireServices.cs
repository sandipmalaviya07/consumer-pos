﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Arrowhead.POS.Service
{
    public class QuestionnaireServices
    {

        public List<QuestionnaireAnswerModel> GetQuestionnaireById(long quoteId)
        {
            using (ArrowheadPOSEntities db = new ArrowheadPOSEntities())
            {
                var quote = db.Quotes.Where(w => w.ID == quoteId).FirstOrDefault();
                var lst = (from q in db.Questionnaires
                           let qa = db.QuestionnaireAnswers.FirstOrDefault(f => f.FkQuestionnaireId == q.ID && f.FkQuoteId == quoteId)
                           where q.FkInsuranceCompanyId == quote.FKInsuranceCompanyId && q.FkStateId == quote.FkStateId
                           select new QuestionnaireAnswerModel()
                           {
                               Questionnaire = q,
                               QuestionnaireAnswer = qa
                           }).ToList();

                string stateCode = db.States.Where(w => w.ID == quote.FkStateId).Select(s => s.Code).FirstOrDefault();
                var _vehicleList = db.Auto_Vehicle.Where(w => w.FkQuoteID == quoteId && w.IsDeleted == false);
                string vehicleList = "";
                foreach (var vehicle in _vehicleList)
                {
                    vehicleList += vehicle.Year + " " + vehicle.Make + " " + vehicle.Model + ",";
                }
                vehicleList = vehicleList.TrimEnd(',');

                var lstQuestionnaire = new List<QuestionnaireAnswerModel>();
                foreach(var item in lst)
                {
                    item.Questionnaire.Question = item.Questionnaire.Question.Replace("[Year Make VIN]", vehicleList);
                    lstQuestionnaire.Add(item);
                }

               

                return lstQuestionnaire;
            }
        }

        public List<Questionnaire> GetParentQuestion(int stateID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var InsuranceCompanyID = db.InsuranceCompanies.Where(f => f.Code == ConstantVariables.RaterServiceProviderCode).FirstOrDefault().ID;

                var item = db.Questionnaires.Where(f => f.FkInsuranceCompanyId == InsuranceCompanyID && (f.FkStateId == stateID)).ToList();
                return item;
            }
        }

        public void DeleteQuestionnaireAnswersByQuoteId(long quoteId)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                db.QuestionnaireAnswers.Where(f => f.FkQuoteId == quoteId).Delete();
                db.SaveChanges();
            }
        }

        public void SaveQuestionnaireAnswers(List<QuestionnaireAnswer> questionnaireAnswers, long QuoteID)
        {
            GeneralServices generalServices = new GeneralServices();
            using (var db = new ArrowheadPOSEntities())
            {
                foreach (var item in questionnaireAnswers)
                    db.QuestionnaireAnswers.Add(item);
                db.SaveChanges();

            }
        }

        public List<QuestionnaireAnswerModel> GetQuestionnaireAnswersByQuoteIdInsuranceCompanyId(int quoteId, int insuranceCompanyId, int stateID)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                var lst = (from q in db.Questionnaires
                           let qa = db.QuestionnaireAnswers.FirstOrDefault(f => f.FkQuestionnaireId == q.ID && f.FkQuoteId == quoteId)
                           where q.FkInsuranceCompanyId == insuranceCompanyId && (q.FkStateId == stateID)
                           select new QuestionnaireAnswerModel()
                           {
                               Questionnaire = q,
                               QuestionnaireAnswer = qa
                           }).ToList();
                return lst;
            }
        }
    }
}
