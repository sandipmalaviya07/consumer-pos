<!DOCTYPE html>
<html>
<head>
	<title>Arrowhead</title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700&display=swap" rel="stylesheet"> 
</head>
<body style="margin: 0; padding: 0; font-family:'poppins'; color: #353535; font-size: 14px;">
	<div style="width:600px; margin: 0 auto;padding:20px;justify-content: space-between; align-items: center;background: #ffffff; border: 1px solid #ddd;">
		
		<table style="width: 100%">
			<tbody>
				<tr><td><img style="width: 100%;display:block" src="https://arrowheadpos.blob.core.windows.net/templates/banner-header.jpg"></td></tr>
			</tbody>
		</table>
                                  <table style="width: 100%;margin-top:20px; font-size: 14px; line-height: 1.6;">
			<tbody>
				<tr><td style="font-size: 20px;text-transform: uppercase;color: #66bb6a;font-weight: 500;">WELCOME TO ARROWHEAD,</td></tr>
				<tr><td style="font-weight: 500; padding-bottom: 5px;">Hi @Model.FirstName @Model.LastName, thank you for your auto insurance purchase.  Please find your attached ID cards and signed application</td></tr>
   <tr><td style="padding-bottom: 10px;">Policy No:  @Model.PolicyNumber</td></tr>
			                <tr><td style="padding-bottom: 10px;">Effective Date:  @Model.EffectiveDate.ToString("MM/dd/yyyy")</td></tr>
                                                                        <tr><td style="padding-bottom: 10px;">Expiration Date: @Model.ExpirationDate.ToString("MM/dd/yyyy")</td></tr>
				
			</tbody>
		</table>


					<table width="100%" style="text-align: center; border-collapse: collapse;">
						<thead>
							<tr>
								<th style="font-size: 20px; text-transform: uppercase; width: 40%; border-bottom: 2px solid #ccc; padding-bottom: 5px; margin-bottom: 5px;">Payment No.</th>
								<th style="font-size: 20px; text-transform: uppercase; width: 20%; border-bottom: 2px solid #ccc; padding-bottom: 5px; margin-bottom: 5px;">DUE DATE</th>
								<th style="font-size: 20px; text-transform: uppercase; width: 20%; border-bottom: 2px solid #ccc; padding-bottom: 5px; margin-bottom: 5px;">Amount Due</th>
							</tr>
						</thead>
						<tbody>
							@{foreach (var item in @Model.SchedulePaymentModel)
						{<tr>
								<td style="padding: 5px 0;">@item.NumberOfPayments</td>
								<td style="padding: 5px 0;">@item.DueDate.ToString("MM/dd/yyyy")</td>
								<td style="padding: 5px 0;">$@item.MonthlyPayments</td>
						
							</tr>}}
						</tbody>
					</table>
<table style="width: 100%">
<tbody>
				<tr><td>Your Agent's Information</td></tr>
<tr><td>@Model.AgencyName</td></tr>
<tr><td>@Model.AgencyPhone</td></tr>
			</tbody>
</table>
				
	</div>
</body>
</html>