﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Arrowhead.POS.Startup))]
namespace Arrowhead.POS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // ConfigureAuth(app)
            app.MapSignalR();
        } 
    }
}