﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Arrowhead.POS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.LowercaseUrls = true;
            //enabling attribute routing
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                //defaults: new { controller = "AutoQuote", action = "Index", id = UrlParameter.Optional }
                defaults: new { controller = "Login", action = "Login",id = UrlParameter.Optional } // producerCode = UrlParameter.Optional,
            );

            //routes.MapRoute(
            //       name: "EditVehicle",
            //       url: "vehicle/edit/{qid}/{id}",
            //       defaults: new { controller = "AutoVehicle", action = " EditVehicleDetail", qid = UrlParameter.Optional, id = UrlParameter.Optional }
            //                );

            //routes.MapRoute(
            //    name: "EditDriverDetail",
            //    url: "driver/edit/{qid}/{id}",
            //    defaults: new { controller = "AutoDriver", action = " EditDriverDetail", qid = UrlParameter.Optional, id = UrlParameter.Optional }
            //             );
        }
    }
}
