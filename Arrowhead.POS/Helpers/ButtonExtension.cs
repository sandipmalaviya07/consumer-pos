﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Arrowhead.POS.Core;

namespace Arrowhead.POS.Helpers
{
    public static class ButtonExtension
    {
        public static HtmlString Sorting(this HtmlHelper htmlHelper, string url = "", string sortingBy = "", string htmlString = "", string currentSortingBy = "", object htmlAttribute = null)
        {
            string descending = "descending";
            string ascending = "ascending";
            TagBuilder tag = new TagBuilder("th");
            tag.MergeAttribute("data-sortby", sortingBy);
            tag.MergeAttribute("data-role", "columnheader");
            tag.MergeAttribute("data-url", url);
            tag.AddCssClass("custom-sorting");
            if (!string.IsNullOrWhiteSpace(currentSortingBy))
            {
                string[] parameter = currentSortingBy.Trim().Split(' ');
                if (parameter.Length == 2)
                {
                    if (parameter[0].ToLower() == sortingBy.ToLower() && parameter[1].ToLower() == descending)
                    {
                        tag.AddCssClass("sorting_desc");
                        tag.MergeAttribute("aria-sort", "");
                    }
                    else
                    {
                        tag.AddCssClass("sorting");
                        tag.MergeAttribute("aria-sort", "" + descending);
                    }
                }
                else
                {
                    if (parameter[0].ToLower() == sortingBy.ToLower())
                    {
                        tag.AddCssClass("sorting_asc");
                        tag.MergeAttribute("aria-sort", "" + descending);
                    }
                    else
                    {
                        tag.AddCssClass("sorting");
                        tag.MergeAttribute("aria-sort", "" + descending);
                    }
                }
            }
            else
            {
                tag.AddCssClass("sorting");
                tag.MergeAttribute("aria-sort", "" + descending);
            }
            if (htmlAttribute != null)
            {
                foreach (var prop in htmlAttribute.GetType().GetProperties())
                {
                    if (prop.Name != "class")
                        tag.MergeAttribute(prop.Name.Replace("_", "-"), ConvertTo.String(prop.GetValue(htmlAttribute)));
                    else
                        tag.AddCssClass(ConvertTo.String(prop.GetValue(htmlAttribute)));
                }
            }
            tag.InnerHtml = htmlString;
            return MvcHtmlString.Create(tag.ToString());
        }
    }
}