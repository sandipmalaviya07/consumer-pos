﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Arrowhead.POS.Core;


namespace Arrowhead.POS.Helpers
{
    public static class PagerExtension
    {
        public static HtmlString Pager(this HtmlHelper htmlHelper, string url, int currentPage, int totalItem, int itemPerPage, string sortBy = "")
        {
            int totalPage = ConvertTo.Integer(Math.Ceiling(ConvertTo.Double(totalItem) / ConvertTo.Double(itemPerPage)));
            if (totalPage > 1)
            {
                var firstrecordindex = (currentPage - 1) * itemPerPage;
                var lastrecordindex = firstrecordindex + itemPerPage;
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class='col-sm-4'>");
                sb.Append("<span>Showing " + (firstrecordindex + 1) + " to " + (lastrecordindex > totalItem ? totalItem : lastrecordindex) + " of " + totalItem + " entries</span>");
                sb.Append("</div>");
                sb.Append("<div class='col-sm-8'>");
                sb.Append("<div class='btn-group pull-right pager-link'>");
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='1'  table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a>");
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='" + (currentPage - 1) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-left'></i></a>");
                if (currentPage <= 5)
                {
                    int looping = totalPage > 9 ? 9 : totalPage;
                    for (int page = 1; page <= looping; page++)
                    {
                        sb.Append("<a class='btn btn-white " + (currentPage == page ? "active" : "") + "' href='" + url + "' data-page='" + page + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'>" + page + "</a>");
                    }
                }
                else
                {
                    int start = -4, end = 4;
                    if (currentPage + 4 > totalPage)
                    {
                        start = start - (currentPage + 4 - totalPage);
                        end = end - (currentPage + 4 - totalPage);
                    }
                    for (int page = start; page <= end; page++)
                    {

                        sb.Append("<a class='btn btn-white " + (page == 0 ? "active" : "") + "' href='" + url + "' data-page='" + (currentPage + page) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'>" + (currentPage + page) + "</a>");
                    }
                }
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='" + (currentPage + 1) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-right'></i> </a>");
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='" + totalPage + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i> </a>");
                sb.Append("</div>");
                sb.Append("</div>");
                return MvcHtmlString.Create(sb.ToString());
            }

            return MvcHtmlString.Create("");
        }
    }
}