﻿using Arrowhead.POS.App_Start;
using Arrowhead.POS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Arrowhead.POS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebAPIConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalFilters.Filters.Add(new HandleErrorAttribute());
            log4net.Config.XmlConfigurator.Configure();

        }
      
        //protected void Application_Error()
        //{
        //    var ex = Server.GetLastError();
           
        //    bool isSuccess = MailingUtility.SendEmail("policysupport@rateforce.net", "jinali", "", "", "Arrowhead - Application level error", ex.ToString(),
        //        false, 1, "", "", "smtp.sendgrid.net", "apikey", "SG.eW-e9frmQLWlC8aYanV0Iw.C9WI3Ls8ekwQmoQfn7qfqYe2e5zA7QV0QidqNy-QinY", 587, 
        //        true, false, true, null, null);
        //    Server.ClearError();

        //    //log the error!            
        //}
    }
}
