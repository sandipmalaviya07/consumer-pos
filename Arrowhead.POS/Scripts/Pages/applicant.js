﻿$('#MailingZipCode').attr('disabled', true);
$('#MailingAddress1').attr('disabled', true);
$('#rowPriorCarrier').attr('disabled', true);
$('#rowPriorPolicyNumber').attr('disabled', true);

$(document).ready(function () {
    var isProofOfPrior = $('#sameAsPriorYes').is(':checked');
    if (isProofOfPrior == true) {
        $("#sameAsPriorYes").parent().addClass('Checked');
    }
    else {
        $("#sameAsPriorNo").parent().addClass('Checked');
    }
    
    var sameAsHome = $("#sameAsHomeYes").is(':checked');
    if (sameAsHome == true)
    {
        $("#rowMailingAddress").hide();
        $("#rowMailingPostalCode").hide();
        $("#sameAsHomeYes").parent().addClass('Checked');
    }
    else
    {
        $('#MailingZipCode').removeAttr("disabled");
        $('#MailingAddress1').removeAttr("disabled");
        $("#rowMailingAddress").show();
        $("#rowMailingPostalCode").show();
        $("#sameAsHomeNo").parent().addClass('Checked');
    }
});



$('input[type=radio][name=sameAsPrior]').change(function () {
    $("#sameAsPriorYes").parent().removeClass('Checked');
    $("#sameAsPriorNo").parent().removeClass('Checked');

    if (this.value == 'yes') {
        $("#sameAsPriorYes").parent().addClass('Checked');
    }
    else if (this.value == 'no') {
        $("#sameAsPriorNo").parent().addClass('Checked');
    }
});


$('input[type=radio][name=IsDuplicate]').change(function ()
{
    $("#sameAsHomeYes").parent().removeClass('Checked');
    $("#sameAsHomeNo").parent().removeClass('Checked');

    if (this.value == 'true') {
        $("#sameAsHomeYes").parent().addClass('Checked');
    }
    else if (this.value == 'false') {
        $("#sameAsHomeNo").parent().addClass('Checked');
    }
});

$('input[type=radio][name=IsDuplicate]').click(function () {
    if (this.value == 'true') {
        $("#rowMailingAddress").hide();
        $("#rowMailingPostalCode").hide();
    }
    else if (this.value == 'false') {
        $('#MailingZipCode').removeAttr("disabled");
        $('#MailingAddress1').removeAttr("disabled");
        $("#rowMailingAddress").show();
        $("#rowMailingPostalCode").show();
    }
});



$('input[type=radio][name=sameAsPrior]').click(function ()
{
    if (this.value == 'yes') {
        $("#rowPriorCarrier").show();
        $("#rowPriorCarrier").removeAttr("disabled"); 
        $("#rowPriorPolicyNumber").removeAttr("disabled");
        $("#rowPriorPolicyNumber").show();
        $("#showText").show();
        $("#IspriorPolicy").val("True");

    }
    else
    {
        $('#rowPriorCarrier').removeAttr("disabled");
        $('#rowPriorPolicyNumber').removeAttr("disabled");
        $("#rowPriorCarrier").hide();
        $("#rowPriorPolicyNumber").hide();
        $("#showText").hide();
        $("#IspriorPolicy").val("False");
        $('#InsuranceCompanyId').removeAttr("disabled");
        $('#DaysLapes').removeAttr("disabled");
        $('#PriorExpirationDate').removeAttr("disabled");

        // reset values on change selection
        $('#InsuranceCompanyId').val('');
        $('#ddlDayLapse').val('');
        $('#PriorExpirationDate').val('');

        
    }
});

$("#EffectiveDate").inputmask("mm/dd/yyyy", {
    "placeholder": "mm/dd/yyyy"
});

$("#PriorExpirationDate").inputmask("mm/dd/yyyy", {
    "placeholder": "mm/dd/yyyy"
});

$("#ContactNo").inputmask("mask", {
    "mask": "(999) 999-9999"
});


$("#ZipCode").change(function () {
    var el = $(this);
    var zip = el.val();
    if (zip.length == 5) {
        $("body").CenterLoader(11001);
        ZipCode(zip, $(this));
    }
});



$("#MailingZipCode").change(function () {
    var el = $(this);
    var zip = el.val();
    if (zip.length == 5) {
        $("body").CenterLoader(11001);
        ZipCode(zip, $(this));
    }
});

function successCallback(result, obj) {
    var city = result.city;
    var state = result.state;
    var country = result.country;
    if ($(obj).attr('id') === "ZipCode") {
        $("#City").val(city);
        $("#StateAbbr").val(state);
        $("#County").val(country);

        //if (state == "SC") {
        //    $('.divHomeType').show();
        //} else {
        //    $('.divHomeType').hide();
        //}
    }
    else {
        $("#MailingCity").val(result.city);
        $("#MailingState").val(result.state);
        $("#MailingCountry").val(result.country);
    }
    if (result.error != null) {
        alert(result.error);
        result.city = "";
        result.state = "";
    }
}