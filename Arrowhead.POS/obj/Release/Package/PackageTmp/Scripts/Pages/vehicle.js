﻿function changeToVin() {
    $("#Year").removeAttr("disabled");
    $("#Make").removeAttr("disabled");
    $("#Model").removeAttr("disabled");

    $("#Make").empty();
    $("#Model").empty();
    $("#Make").append('<option value>Select</option>');
    $("#Model").append('<option value>Select</option>');
}

//$('#LienHolderId').change(function () {

//    var a = $('#LienHolderId').val();
//    if ($('#LienHolderId').val() == "10000") {
//        $('#LienHolderCommercialName').removeAttr("disabled");
//        $("#LienHolderAddress1").removeAttr("disabled");
//        $("#LienholderCity").removeAttr("disabled");
//        $("#LienholderState").removeAttr("disabled");
//        $("#LienHolderZipCode").removeAttr("disabled");

//        $("#LienHolderCommercialName").val('');
//        $("#LienHolderAddress1").val('');
//        $("#LienholderCity").val('');
//        $("#LienholderState").val('');
//        $("#LienHolderZipCode").val('');

//        $("#divLienholderName").show();
//        $("#divLienholderAddress1").show();
//        $("#divLienholderCity").show();
//        $("#divLienholderState").show();
//        $("#divLienholderZipCode").show();


//    }
//    else {
//        $("#LienHolderCommercialName").attr('disabled', true);
//        $("#LienHolderAddress1").attr('disabled', true);
//        $("#LienHolderZipCode").attr('disabled', true);
//        //$("#LienHolderCity").attr('disabled', true);
//        //$("#LienHolderStateCD").attr('disabled', true);

//        $("#divLienholderName").hide();
//        $("#divLienholderAddress1").hide();
//        $("#divLienholderCity").hide();
//        $("#divLienholderState").hide();
//        $("#divLienholderZipCode").hide();
//    }
//});




$('#isFinance').change(function ()
{
    if ($('#isFinance option:selected').text().toUpperCase() == 'LEASED')
    {
        $("#LienHolderId").prop("disabled", false);
        $("#divLienholder").show();
    }
    else if ($('#isFinance option:selected').text().toUpperCase() == 'FINANCED')
    {
        $("#LienHolderId").prop("disabled", false);
        $("#divLienholder").show();
    }
    else if ($('#isFinance option:selected').text().toUpperCase() == 'OWNED')
    {
        $("#divLienholder").hide();
        $("#divLienholderName").hide();
        $("#divLienholderAddress1").hide();
        $("#divLienholderCity").hide();
        $("#divLienholderState").hide();
        $("#divLienholderZipCode").hide();

        $("#LienHolderId").prop("disabled", true);
        $("#LienHolderId").val(''); // default selection true
        //$("#Lienholder").val('');
        $("#LienholderName").val('');
        $("#LienHolderAddress1").val('');
        $("#LienholderCity").val('');
        $("#LienholderState").val('');
        $("#LienHolderZipCode").val('');
    }
});

