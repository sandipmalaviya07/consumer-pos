﻿
$(document).ready(function () {
    $(".divIsDefensive").css("display", "block");
    $(".divStateFilling").css("display", "block");
    $('.divNsd').hide(); // hide NSD Plan

    //id #4 is for UMPD and id #17 for UMPDD.
    //if UMPD change then UMPDD must select for SC region which is $200. - code by HD
    $('[data-code="UMPD"]').change(function () {
        var thisVal = $(this).prop('selectedIndex');
        if (thisVal != 0) {
            $('[data-code="UMPDD"]').attr("style", "pointer-events: none;");
            $('[data-code="UMPDD"]').prop('selectedIndex', 1);
        } else {
            $('[data-code="UMPDD"]').attr("style", "pointer-events: ;");
            $('[data-code="UMPDD"]').prop('selectedIndex', 0);
        }
    });

    $('[data-code="UMPDD"]').change(function () {
        var thisVal = $(this).prop('selectedIndex');
        if (thisVal != 0) {
            $('[data-code="UMPD"]').prop('selectedIndex', 1);
        } else {
            $('[data-code="UMPD"]').prop('selectedIndex', 0);
        }
    });

    //if UIMBI change then UIMPD must select for SC region which is $200. - code by HD
    $('[data-code="UIMBI"]').change(function () {
        var thisVal = $(this).prop('selectedIndex');
        if (thisVal != 0) {
            $('[data-code="UIMPD"]').attr("style", "pointer-events: none;");
            $('[data-code="UIMPD"]').prop('selectedIndex', 1);
        } else {
            $('[data-code="UIMPD"]').attr("style", "pointer-events: ;");
            $('[data-code="UIMPD"]').prop('selectedIndex', 0);
        }
    });

    $('[data-code="UIMPD"]').change(function () {
        var thisVal = $(this).prop('selectedIndex');
        if (thisVal != 0) {
            $('[data-code="UIMBI"]').prop('selectedIndex', 1);
        } else {
            $('[data-code="UIMBI"]').prop('selectedIndex', 0);
        }
    });

    //$('[coverage-name="COMP"][vehicle-id="5205"]').change(function () {
    //    debugger;
    //    var thisVal = $(this).prop('selectedIndex');
    //    if (thisVal != 0) {
    //        $('[coverage-name="COLL"][vehicle-id="5205"]').prop('selectedIndex', thisVal);
    //    } else {
    //        $('[coverage-name="COLL"][vehicle-id="5205"]').prop('selectedIndex', 0);
    //    }
    //});

    $('.divWorkLoss').hide();

    var vehArr = new Array();
    
    // Vehicle Coverage 
    $('.vehicleCoverage').on('change', function ()
    {
        var coverageCode = $(this).attr("coverage-name");
        var vehicleID = $(this).attr("vehicle-id");
        //console.log("Changed Vehicle Id" + vehicleID);

        var col = "COLLLEVEL";
        var collable = "Collision-Level";

       
        if (coverageCode == "COLLLEVEL")
        {
            var option = $("option:selected", this).attr('value');
            var collText = $(this).find("option:selected").html();
            
            if (collText.toLowerCase() != "none")
            {
                // Iterate All Vehicle IDs
                //console.log("Before loop starts"+vehArr.length );
                for (i = 0; i < vehArr.length; i++)
                {
                    //console.log("in loop vehicle Id:" + vehicleID);
                    //console.log("in loop local storage vehicle Id:" + localStorage.getItem("vehicleID"));

                    if (vehicleID == localStorage.getItem("vehicleID")) // main vehicle selection
                    {
                        if (vehArr[i] != localStorage.getItem("vehicleID")) // local Storage Contains main vehicle ID
                        {
                            $("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] > option").each(function (index) {
                                if ($(this).val() == option) {
                                    $(this).show();
                                }
                                else {
                                    if ($(this).val().trim() == "0.00/0.00") // NONE
                                    {
                                        $(this).show();
                                        $("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);

                                        var comp = "COMP";
                                        var complable = "Comprehensive";
                                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehArr[i] + "]").css("display", "none");
                                        $("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehArr[i] + "]").css("display", "none");
                                        //$("select[coverage-name =" + comp + "][vehicle-id=" + vehArr[i] + "]").hide();
                                        //$("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehArr[i] + "]").hide();
                                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);

                                        var coll = "COLL";
                                        var colllable = "Collision";

                                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "]").css("display", "none");
                                        $("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehArr[i] + "]").css("display", "none");
                                        //$("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "]").hide();
                                        //$("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehArr[i] + "]").hide();
                                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);
                                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('disabled', false);

                                        var lou = "LUSE";
                                        var loulable = "Loss-of-Use";
                                        $("select[coverage-name =" + lou + "][vehicle-id=" + vehArr[i] + "]").css("display", "none");
                                        $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehArr[i] + "]").css("display", "none");
                                        //$("select[coverage-name =" + lou + "][vehicle-id=" + vehArr[i] + "]").hide();
                                        //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehArr[i] + "]").hide();
                                        $("select[coverage-name =" + lou + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);

                                    }
                                    else {
                                        $(this).hide();
                                    }
                                }
                            });
                        }
                    }
                }
            }
            else if (collText.toLowerCase() == "none")
            {
                count = 0; // reset vehicle ids 
                for (i = 0; i < vehArr.length; i++)
                {
                    if (vehicleID == localStorage.getItem("vehicleID")) // main vehicle selection
                    {
                        if (vehArr[i] != localStorage.getItem("vehicleID")) {
                            $("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='1.00/0.00 ']").css("display", "block"); // Broad
                            $("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='2.00/0.00 ']").css("display", "block"); // Standard
                            $("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='3.00/0.00 ']").css("display", "block"); // Limited
                            //$("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='1.00/0.00 ']").show(); // Broad
                            //$("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='2.00/0.00 ']").show(); // Standard
                            //$("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='3.00/0.00 ']").show(); // Limited
                            var col = "COLLLEVEL";

                            $("select[coverage-name =" + col + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);

                            var comp = "COMP";
                            var complable = "Comprehensive";
                            $("select[coverage-name =" + comp + "][vehicle-id=" + vehArr[i] + "]").css("display", "none");
                            $("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehArr[i] + "]").css("display", "none");
                            //$("select[coverage-name =" + comp + "][vehicle-id=" + vehArr[i] + "]").hide();
                            //$("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehArr[i] + "]").hide();
                            $("select[coverage-name =" + comp + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);

                            var coll = "COLL";
                            var colllable = "Collision";

                            $("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "]").css("display", "none");
                            $("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehArr[i] + "]").css("display", "none");
                            //$("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "]").hide();
                            //$("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehArr[i] + "]").hide();
                            $("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);
                            $("select[coverage-name =" + coll + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('disabled', false);

                            var lou = "LUSE";
                            var loulable = "Loss-of-Use";
                            $("select[coverage-name =" + lou + "][vehicle-id=" + vehArr[i] + "]").css("display", "none");
                            $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehArr[i] + "]").css("display", "none");
                            //$("select[coverage-name =" + lou + "][vehicle-id=" + vehArr[i] + "]").hide();
                            //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehArr[i] + "]").hide();
                            $("select[coverage-name =" + lou + "][vehicle-id=" + vehArr[i] + "] option[value='0.00/0.00 ']").prop('selected', true);

                            //});

                        }
                    }
                }
            }


            if ($("option:selected", this).text().toUpperCase() == "NONE") {
                var comp = "COMP";
                var complable = "Comprehensive";
                $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                $("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                //$("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").hide();
                //$("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").hide();
                $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);

                var coll = "COLL";
                var colllable = "Collision";

                $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('disabled', false);
                $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                $("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                //$("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").hide();
                //$("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").hide();
                $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);

                var lou = "LUSE";
                var loulable = "Loss-of-Use";
                $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").hide();
                //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").hide();
                $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);
                //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").val(option);

                var towing = "TL";
                var towinglable = "Towing";
                $("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                $("[lable-vehicle=" + towinglable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                $("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);
                //$("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "]").val(option);
            }
            else {
                if ($("option:selected", this).text().toUpperCase() == "LIMITED") {
                    var comp = "COMP";
                    var complable = "Comprehensive";
                    $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                    $("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                    //$("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").show();
                    //$("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").show();
                    $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").removeAttr('disabled').show();
                    $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='250.00/0.00 ']").prop('selected', true);

                    var coll = "COLL";
                    var colllable = "Collision";
                    $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                    $("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                    //$("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").show();
                    //$("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").show();
                    $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").show();
                    $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);
                    $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").prop('disabled', true);

                    var lou = "LUSE";
                    var loulable = "Loss-of-Use";
                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                    $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                    //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").show();
                    //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").show();

                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);

                }
                else {
                    // Standard / Broad Default Selection and Remove No Deductible Option
                    if ($("option:selected", this).text().toUpperCase() == "STANDARD" ||
                        $("option:selected", this).text().toUpperCase() == "BROAD") {
                        var comp = "COMP";
                        var complable = "Comprehensive";
                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                        $("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                        //$("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").show();
                        //$("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").show();
                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='250.00/0.00 ']").prop('selected', true);
                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").attr('disabled', 'disabled').hide();

                        var coll = "COLL";
                        var colllable = "Collision";
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                        $("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                        //$("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").show();
                        //$("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").show();
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").prop('disabled', false);
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "] option[value='250.00/0.00 ']").prop('selected', true);
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").attr('disabled', 'disabled').hide();

                        var lou = "LUSE";
                        var loulable = "Loss-of-Use";
                        $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                        $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                        //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").show();
                        //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").show();
                        $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);
                    }
                    else {
                        var comp = "COMP";
                        var complable = "Comprehensive";
                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                        $("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                        //$("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "]").show();
                        //$("[lable-vehicle=" + complable + "][lable-vehicleId=" + vehicleID + "]").show();
                        $("select[coverage-name =" + comp + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);

                        var coll = "COLL";
                        var colllable = "Collision";
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                        $("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                        //$("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").show();
                        //$("[lable-vehicle=" + colllable + "][lable-vehicleId=" + vehicleID + "]").show();
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "]").prop('disabled', false);
                        $("select[coverage-name =" + coll + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);
                    }
                }
            }
        }
        else if (coverageCode != "COLLLEVEL")
        {
            debugger;
            if (coverageCode == "COLL") {
                var option = $("option:selected", this).attr('value');

                if (option == "0.00/0.00 " || option == "No Coverage" || option == "") {
                    var towing = "TL";
                    var towinglable = "Towing";
                    //$('#aioConceptName').find(":selected").text();

                    $("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                    $("[lable-vehicle=" + towinglable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                    //$("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);

                    var lou = "LUSE";
                    var loulable = "Loss-of-Use";
                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                    $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                    //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").hide();
                    //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").hide();
                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "] option[value='0.00/0.00 ']").prop('selected', true);
                }
                else {
                    var towing = "TL";
                    var towinglable = "Towing";
                    $("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                    $("[lable-vehicle=" + towinglable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");

                    var lou = "LUSE";
                    var loulable = "Loss-of-Use";
                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                    $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                    //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").show();
                    //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").show();
                }
                var compName = "COMP";
                $("select[coverage-name =" + compName + "][vehicle-id=" + vehicleID + "]").val(option);
            }
            if (coverageCode == "COMP") {
                var option = $("option:selected", this).attr('value');
                if (option == "0.00/0.00 " || option == "No Coverage" || option == "") {
                    var towing = "TL";
                    var towinglable = "Towing";
                    $("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                    $("[lable-vehicle=" + towinglable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");

                    var lou = "LUSE";
                    var loulable = "Loss-of-Use";
                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                    $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");
                    //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").hide();
                    //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").hide();

                }
                else {
                    var towing = "TL";
                    var towinglable = "Towing";
                    $("select[coverage-name =" + towing + "][vehicle-id=" + vehicleID + "]").css("display", "none");
                    $("[lable-vehicle=" + towinglable + "][lable-vehicleId=" + vehicleID + "]").css("display", "none");

                    var lou = "LUSE";
                    var loulable = "Loss-of-Use";
                    $("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").css("display", "block");
                    $("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").css("display", "block");
                    //$("select[coverage-name =" + lou + "][vehicle-id=" + vehicleID + "]").show();
                    //$("[lable-vehicle=" + loulable + "][lable-vehicleId=" + vehicleID + "]").show();

                }
                var compName = "COLL";
                var col = "COLLLEVEL";
                // Limited Selection Restrict Value to Update and Set COMP 250
                if ($("select[coverage-name =" + col + "][vehicle-id=" + vehicleID + "] option:selected").html() != undefined && $("select[coverage-name =" + col + "][vehicle-id=" + vehicleID + "] option:selected").html().toUpperCase() != "LIMITED") // Restrict to update when Limited
                {
                    $("select[coverage-name =" + compName + "][vehicle-id=" + vehicleID + "]").val(option);
                }
                else if ($("select[coverage-name =" + col + "][vehicle-id=" + vehicleID + "] option:selected").html() == undefined) // COMP/COLL Set If COLL Level not get for GA/AL
                {
                    $("select[coverage-name =" + compName + "][vehicle-id=" + vehicleID + "]").val(option);
                }

            }
        }
    });

    
});