﻿$.fn.CenterLoader = function (zindex, id, flag) {
    var height = $(this).outerHeight() + "px";
    var width = $(this).outerWidth() + "px";
    var top = $(this).offset().top + "px";
    var left = $(this).offset().left + "px";
    var loadingContainer = "body";
    if (flag == true) {
        top = "0px";
        left = "0px";
        loadingContainer = this;
    }
    var centerTop = Math.max(0, (($(this).outerHeight() / 2) - 8)) + "px";
    var centerLeft = Math.max(0, (($(this).outerWidth() / 2) - 8)) + "px";

    var loadingContain;
    if (id === "" || id === null || id === undefined) {
        id = "loader-image";

    }
    //else {
    //    var _loadIdAppend = id
    //    id = id + "_Loader";
    //}
    if (id === "dvSendCustomerContainer" || id === "dvSignInOfficeContainer") {
        loadingContain = "<div style='position:absolute;height:" + height + ";width:" + width + ";background:#ccc;z-index:" + zindex + ";top:" + top + ";left:" + left + ";display: flex; opacity:0.9;    align-items: center;justify-content: center;' id='" + id + "' class='loader-image'><div style='color:black;font-weight:800;margin-top: 45px;'><div style='z-index: 10001;'><img src='/Assets/images/prepare-documents-loading_dark.gif'></div>Preparing Documents</div></div>";
    }
    else {
        loadingContain = "<div style='position:absolute;height:" + height + ";width:" + width + ";background:#ccc;z-index:" + zindex + ";top:" + top + ";left:" + left + ";opacity:0.7' id='" + id + "' class='loader-image'><div style='position:absolute;top:" + centerTop + ";left:" + centerLeft + ";color:white;height: 28px;width: 28px;background: transparent url(/Assets/images/loader.gif) no-repeat scroll 0 0;' class='loader-style'></div></div>";
    }


    $(loadingContainer).append(loadingContain);

};

$.fn.RemoveLoader = function (id) {

    if (id === "" || id === null || id === undefined) {
        $(".loader-image").remove();
    } else {
        $("#" + id + "_Loader").remove();
    }
};


function alphabeticInput(event) {
    var value = String.fromCharCode(event.which);
    var pattern = new RegExp(/[a-zåäö ]/i);
    return pattern.test(value);
}

function IsiOSversion() {
    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        return true;
    }
    return false;
}

// Keyboard shortcut ctrl + f to show search bar in right side 
$(window).keydown(function (e)
{
    if ((e.ctrlKey || e.metaKey) && e.keyCode === 70) 
    {
        e.preventDefault();
        $("#myModal2").modal('show');
        //$("#dvLatestQuote").show();
        $('#txtSearch').val('');
        $('#txtSearch').focus();
    }
});


function formatCurrency(total) {
    var neg = false;
    if (total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

// Export CSV,Excel 
jQuery.download = function (url, key, reportName, strJson) {
    // Build a form
    var form = $('<form></form>').attr('action', url).attr('method', 'post');
    // Add the one key/value
    form.append($("<input></input>").attr('type', 'hidden').attr('name', "jsonStr").attr('value', strJson));
    form.append($("<input></input>").attr('type', 'hidden').attr('name', "reportName").attr('value', reportName));
    //send request
    form.appendTo('body').submit().remove();
};

//Export Multiple Tables in CSV,Excel
jQuery.downloadMultiple = function (url, key, reportName, strJson, otherJson) {
    // Build a form
    var form = $('<form></form>').attr('action', url).attr('method', 'post');
    // Add the one key/value
    form.append($("<input></input>").attr('type', 'hidden').attr('name', "jsonStr").attr('value', strJson));
    form.append($("<input></input>").attr('type', 'hidden').attr('name', "otherJsonStr").attr('value', otherJson));
    form.append($("<input></input>").attr('type', 'hidden').attr('name', "reportName").attr('value', reportName));
    //send request
    form.appendTo('body').submit().remove();
};

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

/* Define functin to find and replace specified term with replacement string */
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}