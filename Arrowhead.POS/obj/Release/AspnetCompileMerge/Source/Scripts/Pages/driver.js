﻿
$('#radioBtn a').on('click', function () {
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#' + tog).prop('value', sel);

    $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
});
$("#Dob").inputmask("mm/dd/yyyy", {
    "placeholder": "mm/dd/yyyy"
});
$("#PriorCancelDeclineDt").inputmask("mm/dd/yyyy", {
    "placeholder": "mm/dd/yyyy"
});
$("#IncidentDate").inputmask("mm/dd/yyyy", {
    "placeholder": "mm/dd/yyyy"
});

$("#LicenseStatus").change(function () {
    $("#LicenseStatus").focus();
});

$("#IncidentDate").datepicker({
    format: 'mm/dd/yyyy'
});

$("#PriorCancelDeclineDt").datepicker({
    format: 'mm/dd/yyyy'
});




$('input[type=radio][name=isAccidents]').change(function () {
    $("#isAccidentsYes").parent().removeClass('Checked');
    $("#isAccidentsNo").parent().removeClass('Checked');

    if (this.value == 'yes') {
        $("#isAccidentsYes").parent().addClass('Checked');
    }
    else if (this.value == 'no') {
        $("#isAccidentsNo").parent().addClass('Checked');
    }
});
$('input[type=radio][name=isAccidents]').click(function () {
    if (this.value == 'yes') {
        $("#divIncidentDate").show();
        $("#divIncidentType").show();
        $("#divIncidentSave").show();
        $("#divIncidentList").show();
    }
    else if (this.value == 'no') {
        $("#divIncidentDate").hide();
        $("#divIncidentType").hide();
        $("#divIncidentSave").hide();
        $("#divIncidentList").hide();

        ClearIncidents(0);
    }
});
