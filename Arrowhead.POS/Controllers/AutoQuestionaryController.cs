﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("auto")]
    public class AutoQuestionaryController : BaseController
    {
        // GET: AutoQuestionary
        [Route("questionary/{id?}")]
        public ActionResult Index(long? id = 0)
        {
            QuestionnaireServices questionnaireServices = new QuestionnaireServices();
            List<QuestionnaireAnswerModel> lstQuestion = new List<QuestionnaireAnswerModel>();
            if (id > 0)
            {
                ProjectSession.QuoteID = Convert.ToInt64(id);
            }

            // Add New Tab If Not Added In ProjectSession.TabDetail
            if (ProjectSession.QuoteID > 0)
                CommonFunctions.AddNewTab(ProjectSession.QuoteID);

            lstQuestion = questionnaireServices.GetQuestionnaireById(ProjectSession.QuoteID);

            // Update Quote CurrentStep  Details
            CommonFunctions.UpdateCurrentQuoteStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.QUESTIONNAIRE.ToString());

            ViewBag.IsQuestionEdit = true;
            return View(lstQuestion);
        }

        /// <summary>
        /// Save AutoQuestionary
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveQuestionnaire()
        {
            QuestionnaireServices questionaryServices = new QuestionnaireServices();
            GeneralServices generalServices = new GeneralServices();
            
            int stateID = generalServices.GetStateID(ProjectSession.QuoteID);
            var lstquestionnaire = questionaryServices.GetParentQuestion(stateID);
            questionaryServices.DeleteQuestionnaireAnswersByQuoteId(ProjectSession.QuoteID);

            try
            {
               
                var lstQuestionnaireAnswer = new List<QuestionnaireAnswer>();
                foreach (var item in lstquestionnaire)
                {
                    if (Request["question_" + item.ID] != null)
                    {
                        QuestionnaireAnswer questionnaireAnswer = new QuestionnaireAnswer();
                        questionnaireAnswer.Description = Request["question_additional_" + item.ID];
                        questionnaireAnswer.Answer = Request["question_" + item.ID];
                        questionnaireAnswer.FkQuestionnaireId = item.ID;
                        questionnaireAnswer.FkQuoteId = ProjectSession.QuoteID;
                        lstQuestionnaireAnswer.Add(questionnaireAnswer);
                    }
                }
                questionaryServices.SaveQuestionnaireAnswers(lstQuestionnaireAnswer , ProjectSession.QuoteID);
                return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QA.ToString(), Enums.AuditHistoryOperation.I.ToString(), ex.ToString());

                return RedirectToAction("Index", "AutoQuestionary", new { @id = ProjectSession.QuoteID });
            }

        }
    }
}