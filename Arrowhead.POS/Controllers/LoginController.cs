﻿using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Arrowhead.POS.Service;
using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;

namespace Arrowhead.POS.Controllers
{
    
    [RoutePrefix("Login")]
    public class LoginController : Controller
    {
        // GET: Login

        public ActionResult Index()
        {
            return RedirectToAction("Index", "AutoQuote");
        }

       // [CustomExceptionFilter(ModuleID = "LI", OperationType = 'U', AfterErrorRedirectOn = "login/index")]
        public ActionResult Login(LoginModel loginModel)
        {
            try
            {

               
                LoginService loginService = new LoginService();

                if (!string.IsNullOrWhiteSpace(loginModel.ProducerCode))
                {
                    bool checkAgencyAuth = loginService.CheckLoginDetails(loginModel);
                    if (checkAgencyAuth == true)
                    {
                        return RedirectToAction("AgencyProducerCode", "ArrowheadBridge", new { pc = loginModel.ProducerCode.ToString() });
                    }
                    else
                    {
                        this.ShowMessage(MessageExtension.MessageType.Error, "Unauthorized access, please contact 800-961-6549 for assistance", true);
                        return RedirectToAction("Login", "Login");
                    }
                }
                else if (loginModel.ProducerCode == null)
                {
                    ModelState.Remove("ProducerCode");
                    ModelState.Remove("Password");
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.L.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                throw ex;
            }
            return View();
        }



    }
}