﻿using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Service;
using Arrowhead.POS.Core;
using DocuSign.eSign.Api;
using DocuSign.eSign.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Arrowhead.POS.Model;
using Microsoft.AspNet.SignalR;
using Arrowhead.POS.Hubs;
using RazorEngine.Templating;
using RazorEngine;
using System.IO;
using iTextSharp.text.pdf;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using Arrowhead.POS.PaymentProcess;
using Arrowhead.POS.Process;
using System.Net;

namespace Arrowhead.POS.Controllers
{
    public class CustomerEsignController : Controller
    {
       
        [Route("~/customer-sign/view/{id}")]
        public ActionResult Index(string id)
        {
            int esignCustomerId = 2;
            long quoteId = Convert.ToInt64(CryptoGrapher.CBase64Decrypt(id));
            DocusignServices docusignServices = new DocusignServices();
            CommonServices commonServices = new CommonServices();
            var credentials = docusignServices.GetDocusignApiCredential();
            var quoteEnvelopes = docusignServices.GetEnvelopeById(quoteId);
            var DocusignQuoteModel = docusignServices.GetDocusignQuoteRecipientByQuoteId(quoteId);
            var cfi = DocusignConfiguration.DocusignConfigurations(credentials);
            EnvelopesApi envelopesApi = new EnvelopesApi(cfi);
            var hostUrl = commonServices.CheckHost(Request.Url.Host);
            string returnUrl = hostUrl + "/customer-signed/completed?qid=" + quoteId;
            RecipientViewRequest recipientViewRequest = new RecipientViewRequest();
            recipientViewRequest.ReturnUrl = returnUrl;
            recipientViewRequest.Email = DocusignQuoteModel.CustomerEmailId;
            recipientViewRequest.RecipientId = esignCustomerId.ToString();
            recipientViewRequest.AuthenticationMethod = Core.ConstantVariables.AuthenticationMethod;
            recipientViewRequest.UserName = DocusignQuoteModel.CustomerName;
            recipientViewRequest.ClientUserId = esignCustomerId.ToString();
            // create the recipient view (aka signing URL)
            ViewUrl recipientView = envelopesApi.CreateRecipientView(credentials.AccountId, quoteEnvelopes.EnvelopeId, recipientViewRequest);

            return Redirect(recipientView.Url);
        }

        //[Route("~/esign/thank-you")]
        [Route("~/customer-signed/completed")]
        public ActionResult CustomerSignedCompleted(long qid)
        {
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            CustomerDocumentServices customerDocumentServices = new CustomerDocumentServices();
            CommonServices commonServices = new CommonServices();
            AutoPolicyController autoPolicyController = new AutoPolicyController();
            NewPolicyModel _newPolicyModel = new NewPolicyModel();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            PolicyBindResponseModel _policyBindRespModel = new PolicyBindResponseModel();
            string errorMessage = string.Empty;
            bool isError = false;
            string _Issuccess = Enums.PAYMENTSTATUS.SUCCESS.ToString();
            var quoteDetail = autoQuoteServices.GetQuoteInfo(qid);
            var paymentDetailModel = autoPolicyServices.GetPaymentDetailByQuoteId(qid);
            var paymentGatewayCode = autoPolicyServices.GetPaymentGatewayByQuoteId(qid);
            var hostUrl = commonServices.CheckHost(Request.Url.Host);

            if (Request.QueryString["event"] == "signing_complete")
            {
                GeneralServices generalServices = new GeneralServices();

                DocusignServices docusignServices = new DocusignServices();
                AuthorizeProcess _authorizeProcess = new AuthorizeProcess();

                Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
                try
                {

                    // Capture Payment CC/ACH
                    if (paymentDetailModel != null && paymentDetailModel.PaymentMethodCd == Enums.PaymentMethod.CREDITCARD.ToString() &&
                        paymentGatewayCode == Enums.PaymentGateway.AUTHORIZENET.ToString() && paymentDetailModel.PaymentStatus == Enums.PAYMENTSTATUS.INITIATED.ToString() &&
                        string.IsNullOrWhiteSpace(quoteDetail.QuoteModel.PolicyNumber))
                    {
                        // Prepare PaymentReq Model
                        var paymentReqModel = autoPolicyServices.GetPaymentByQuoteID(qid);

                        PaymentProcessRequestModel requestModel = new PaymentProcessRequestModel()
                        {
                            CarrierAmount = paymentReqModel.TotalCarrier,
                            CurrencyCode = "USA",
                            CustomerName = paymentReqModel.FullName,
                            QuoteID = paymentReqModel.QuoteID,
                            TotalAmount = paymentReqModel.TotalAmount,
                            Description = "Payment toward policy",
                            Token = paymentReqModel.PgTransactionId
                        };

                        var _respProcess = _authorizeProcess.CapturePayment(requestModel);

                        PaymentResponseModel paymentResponseModel = new PaymentResponseModel();
                        paymentResponseModel.QuoteId = qid;
                        paymentResponseModel.ResponseFile = _respProcess.ResponseString;
                        autoPolicyServices.UpdatePaymentXMLAsync(paymentResponseModel);

                        _Issuccess = _respProcess.Status ? Enums.PAYMENTSTATUS.SUCCESS.ToString()
                            : Enums.PAYMENTSTATUS.FAIL.ToString();
                        errorMessage = _respProcess.StatusMessage;

                        if (string.IsNullOrEmpty(_respProcess.TransactionID) || _respProcess.TransactionID.ToString() == "0")
                        {
                            this.ShowMessage(MessageExtension.MessageType.Error, _respProcess.StatusMessage, true);
                        }

                        autoPolicyServices.UpdateQuotePaymentStatus(_respProcess, qid, _Issuccess);


                        
                    }
                    else if (paymentDetailModel != null)
                    {
                        _Issuccess = Enums.PAYMENTSTATUS.SUCCESS.ToString();
                    }
                    if (paymentDetailModel != null && _Issuccess.ToUpper() == Enums.PAYMENTSTATUS.SUCCESS.ToString())
                    {

                        var lstQuoteVehicles = docusignServices.UpdateQuoteEnvelopeStatusByQuoteID(qid);



                        if (string.IsNullOrWhiteSpace(quoteDetail.QuoteModel.PolicyNumber)) // if policy number not exist then only bind 
                        {
                            Arrowhead.POS.Process.Arrowhead arrowhead = new Arrowhead.POS.Process.Arrowhead();
                            RaterServices raterServices = new RaterServices();
                            var binderRequestModel = raterServices.PreparePolicyBindModel(qid, false);

                            
                            if (binderRequestModel.PaymentMethodCd == "CHECK")
                            {
                                if (hostUrl.Contains("arrowhead-pos-qa") || hostUrl.Contains("localhost"))
                                {
                                    QuoteEcheck quoteEcheck = autoPolicyServices.GetEcheckDetailsByQuotesId(binderRequestModel.QuoteID);
                                    binderRequestModel.IsNSDPlanSelected = autoPolicyServices.IsNSDExists(binderRequestModel.QuoteID);
                                    if (quoteEcheck != null && quoteEcheck.AccountNumber != null && quoteEcheck.RoutingNumber != null)
                                    {
                                        binderRequestModel.AccountNumber = quoteEcheck.AccountNumber;
                                        binderRequestModel.RoutingNumber = quoteEcheck.RoutingNumber;
                                    }
                                }
                            }

                            var policyModel = arrowhead.BindPolicy(binderRequestModel);
                            policyModel.QuoteID = qid;
                            docusignServices.UpdatePolicyNumber(policyModel, false);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(quoteDetail.QuoteModel.PolicyNumber))
                            {
                                // Prepare Policy Bind Response model 
                                _policyBindRespModel = new PolicyBindResponseModel()
                                {
                                    IsSuccess = true,
                                    PolicyNumber = quoteDetail.QuoteModel.PolicyNumber,
                                    MessageDescription = quoteDetail.QuoteModel.PolicyMsgDesc,
                                    QuoteID = qid
                                };
                                docusignServices.UpdatePolicyNumber(_policyBindRespModel, false);
                            }
                        }

                        _newPolicyModel = autoPolicyServices.GetNewPolicyDetails(qid);




                        if (ConnectionList.MyConnectionList != null)
                        {
                            IHubContext _msgContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                            _msgContext.Clients.All.SignedCompleted(qid);
                        }

                        long customerSignTrackId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.SIGNMEDIUM.ToString(), Enums.SignMedium.CUSTOMERSIGN.ToString());

                        autoPolicyServices.UpdateSignTracking(customerSignTrackId, qid);


                        //Get attachement
                        System.Net.WebClient client = new System.Net.WebClient();
                        List<System.Net.Mail.Attachment> attachment = new List<System.Net.Mail.Attachment>();
                        var policyHolderDetails = quoteDetail;
                        var agent_Details = autoQuoteServices.GetAgencyDetailsByQuoteId(qid);
                        try
                        {
                            var credentials = docusignServices.GetDocusignApiCredential();
                            string envelopeID = docusignServices.GetEnvelopeIDByQuoteId(qid);
                            var cfi = DocusignConfiguration.DocusignConfigurations(credentials);
                            EnvelopesApi envelopesApi = new EnvelopesApi(cfi);
                            MemoryStream docStream = (MemoryStream)envelopesApi.GetDocument(credentials.AccountId, envelopeID, "combined");
                            string filename = "Application_" + _newPolicyModel.PolicyNumber.Substring(4, 6) + ".pdf";
                            BlobFileUtility blobFileUtility = new BlobFileUtility();
                            string CloudFolderName = ConstantVariables.OfficeSignAppContainer;
                            blobFileUtility.Save(CloudFolderName, filename, docStream.ToArray());


                            CustomerDocumentModel customerDocumentModel = new CustomerDocumentModel();
                            customerDocumentModel.CreatedById = ProjectSession.AgencyDetails.UserId;
                            customerDocumentModel.DocPath = ConstantVariables.ArrowheadBlobURL + CloudFolderName + "/" + filename;
                            customerDocumentModel.QuoteId = qid;
                            customerDocumentModel.FileTypeName = filename;
                            customerDocumentServices.InsertCustomerDocument(customerDocumentModel);
                            byte[] pdfByte;
                            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain);
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                docStream.CopyTo(memoryStream);
                                pdfByte = memoryStream.ToArray();
                                var file = File(pdfByte, System.Net.Mime.MediaTypeNames.Application.Pdf, filename);
                                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(pdfByte), filename);
                                attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                                attach.Name = filename;
                                attachment.Add(attach);

                            }
                        }
                        catch (Exception ex)
                        {
                            CommonFunctions.OnException(Enums.AuditHistoryModule.QE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                            commonServices.SendErrorMail(ex.ToString(), commonServices.CheckHost(Request.Url.Host));
                        }

                        try
                        {

                            string pdfTemplate = string.Empty;


                            pdfTemplate = Server.MapPath("~/Documents/PDF/" + policyHolderDetails.QuoteModel.StateAbbr + "/ID_Card.pdf");
                            

                            for (int i = 0; i < policyHolderDetails.AutoQuoteVehicles.Count; i++)
                            {
                                var _vehicle = policyHolderDetails.AutoQuoteVehicles[i];
                                PdfReader _reader = new PdfReader(pdfTemplate);
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    PdfStamper stamper = new PdfStamper(_reader, ms);
                                    using (stamper)
                                    {
                                        Dictionary<string, string> info = _reader.Info;
                                        info["Title"] = "ID Card";
                                        stamper.MoreInfo = info;
                                        commonServices.IDCardFields(qid, (int)_vehicle.AutoVehicleId, stamper.AcroFields);
                                    }

                                    ms.Close();
                                    byte[] IDCardByte;
                                    IDCardByte = ms.ToArray();
                                    var file = File(IDCardByte, System.Net.Mime.MediaTypeNames.Application.Pdf, "IDCards.pdf");
                                    System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(IDCardByte), "IDCards.pdf");
                                    attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                                    attach.Name = "Veh" + (i + 1).ToString() + "_IDCards_" + _newPolicyModel.PolicyNumber.Substring(5, 6) + ".pdf";
                                    attachment.Add(attach);

                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            commonServices.SendErrorMail(ex.ToString(), commonServices.CheckHost(Request.Url.Host));
                            CommonFunctions.OnException(Enums.AuditHistoryModule.QE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                        }

                        
                        try
                        {
                            string billingTemplate = string.Empty;


                            billingTemplate = Server.MapPath("~/Documents/PDF/" + policyHolderDetails.QuoteModel.StateAbbr + "/Billing_Statement.pdf");
                            string filename = "Billing_Statement" + _newPolicyModel.PolicyNumber.Substring(4, 6) + ".pdf";


                            PdfReader _reader = new PdfReader(billingTemplate);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                PdfStamper stamper = new PdfStamper(_reader, ms);
                                using (stamper)
                                {
                                    Dictionary<string, string> info = _reader.Info;
                                    commonServices.BillingField(qid, stamper.AcroFields);
                                }

                                ms.Close();
                                byte[] BillingPdfByte;
                                BillingPdfByte = ms.ToArray();
                                var file = File(BillingPdfByte, System.Net.Mime.MediaTypeNames.Application.Pdf, "Billing_Statement.pdf");
                                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(BillingPdfByte), "Billing_Statement.pdf");
                                attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                                attach.Name = filename;
                                attachment.Add(attach);
                            }

                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                            CommonFunctions.OnException(Enums.AuditHistoryModule.QE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                        }
                    

                        

                        autoPolicyController.SendOfficeSigningAppMail(qid);

                        TemplateModel template = templateServices.GetTemplateDetails(Enums.TemplateType.SENDPAPERWORK.ToString());
                        string EmailBody = GetEmailTemplate(qid, template);
                        string subject = template.EmailSubject;
                        var officeSMTPlDetail = generalServices.GetSmtpDetails();
                        MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, quoteDetail.QuoteModel.EmailId, "", "", subject, EmailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, attachment, null);

                        TemplateModel templateArrowheadNotification = templateServices.GetTemplateDetails(Enums.TemplateType.SENDARROWHEADNOTIFICATION.ToString());
                        string to = "minewapp@arrowheadgrp.com";
                        if (hostUrl.Contains("arrowhead-pos-qa") || hostUrl.Contains("localhost"))
                        {
                            to = "randy@rateforce.com";
                        }
                        string emailBodyArrowheadNotification = GetArrowheadNotificationTemplate(qid, templateArrowheadNotification);
                        string subjectArrowheadNotification = templateArrowheadNotification.EmailSubject;
                        MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, to, "", "", subjectArrowheadNotification, emailBodyArrowheadNotification, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, attachment, null);

                    }
                }
                catch (Exception ex)
                {
                    isError = true;
                    errorMessage = ex.Message;
                    CommonFunctions.OnException(Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.U.ToString(), ex.ToString());

                }
            }
            else
            {

            }
            string eqid = CryptoGrapher.CBase64Encrypt(qid.ToString());
            return RedirectToAction("ThankYou", new { qid = eqid });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qid"></param>
        /// <returns></returns>
        [Route("~/thank-you")]
        public ActionResult ThankYou(string qid)
        {
            long dqid = Convert.ToInt64(CryptoGrapher.CBase64Decrypt(qid));
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            NewPolicyModel _newPolicyModel = new NewPolicyModel();
            _newPolicyModel = autoPolicyServices.GetNewPolicyDetails(dqid);
            return View(_newPolicyModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qid"></param>
        /// <returns></returns>
        public ActionResult ThankYouSignedApplication(long qid)
        {
            CustomerDocumentServices customerDocumentServices = new CustomerDocumentServices();
            CommonServices commonServices = new CommonServices();
            string document = customerDocumentServices.officeSignUrl(qid);
            var pdfByte = commonServices.GetTemplateByte(document); 
            return File(pdfByte, System.Net.Mime.MediaTypeNames.Application.Pdf, "Signed Application.pdf");
        }

        /// <summary>
        /// Get Email Template 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string GetEmailTemplate(long quoteId, TemplateModel _template)
        {
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            try
            {
                var newPolicyModel = autoPolicyServices.GetNewPolicyDetails(quoteId);
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, newPolicyModel);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.T.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                throw ex;
            }
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="templateModel"></param>
        /// <returns></returns>
        public string GetArrowheadNotificationTemplate(long quoteId, TemplateModel templateModel)
        {
            TemplateModel _template = templateModel;
            Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            string template = string.Empty;
            try
            {
                var newPolicyModel = autoPolicyServices.GetNewPolicyDetails(quoteId);
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, newPolicyModel);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.T.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                throw ex;
            }
            return string.Empty;
        }


    }
}