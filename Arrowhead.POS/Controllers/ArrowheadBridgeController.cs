﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Process.ProcessXMLModel;
using Arrowhead.POS.Service;
using Arrowhead.POS.Process.ArrowHeadRTRProd;
using Arrowhead.POS.Model;
using Arrowhead.POS.Core;
using log4net;

namespace Arrowhead.POS.Controllers
{
    public class ArrowheadBridgeController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ArrowheadBridgeController));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Rq"></param>
        /// <returns></returns>
        [Route("arrowhead/bridge")]
        public ActionResult Index(string Rq)
        {
            RaterServices raterServices = new RaterServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            PolicyXmlServices policyXmlServices = new PolicyXmlServices();
            CommonServices commonServices = new CommonServices();
            XmlDocument doc = new XmlDocument();
            string xmlPolicy = string.Empty;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xC0 | 0x300 | 0xC00);
            Process.Arrowhead arrowhead = new Process.Arrowhead();
            try
            {
                xmlPolicy = arrowhead.GetPolicyXml(Rq);
                ViewBag.XMLPolicy = xmlPolicy;
                doc.LoadXml(xmlPolicy);
            }
            catch (Exception ex)
            {
                // prepare error response model
                BridgerXMLModel _bridgerXMLModel = new BridgerXMLModel();
                _bridgerXMLModel.RequestId = Rq;
                _bridgerXMLModel.MessageDescription = ex.Message.ToString();
                _bridgerXMLModel.RequestFile = Rq + ".xml";
                commonServices.UploadBridgeResponseXML(CommonFunctions.GetXMLFromObject(_bridgerXMLModel), Rq);
                ProjectSession.IsBridgeError = 1; // set bridge error flag
                this.ShowMessage(MessageExtension.MessageType.Error, "Unable to bridge quote to Arrowhead.  Please try creating a new quote directly on Arrowhead or bridging in a new quote.  We apologize for the inconvenience.", true);
                return RedirectToAction("Login", "Login");
            }

            XmlNode oNode = doc.DocumentElement;
            var requestList = oNode.OuterXml;
            XmlSerializer serializer = new XmlSerializer(typeof(ACORD));
            var objResponse = new ACORD();
            bool isInternalError = false;
            using (TextReader reader1 = new StringReader(requestList.ToString()))
            {
                try
                {

                    objResponse = (ACORD)serializer.Deserialize(reader1);
                    commonServices.UploadBridgeResponseXML(xmlPolicy, Rq);
                    Log.Debug("Arrowhead Bridge Response start :" + Newtonsoft.Json.JsonConvert.SerializeObject(objResponse));
                }
                catch (Exception ex)
                {

                    Log.Error("Arrowhead Bridge exception : " + ex.Message.ToString());
                    CommonFunctions.OnException(Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());
                    isInternalError = true;
                }


            }
            
            if (objResponse.InsuranceSvcRs.PersAutoPolicyQuoteInqRs != null && !isInternalError)
            {
                var quoteInfo = policyXmlServices.InsertXmlPolicyProcess(objResponse);

                if (quoteInfo.QuoteId > 0)
                {
                    ProjectSession.QuoteID = Convert.ToInt64(quoteInfo.QuoteId);
                    ProjectSession.AgencyDetails = quoteInfo;
                    CommonFunctions.AddNewTab(ProjectSession.QuoteID);

                    if (quoteInfo.PolicyXMLStep == Enums.PolicyXMLStep.APPLICANT.ToString())
                    {
                        if (ProjectSession.QuoteID > 0)
                        {
                            return RedirectToAction("Index", "AutoQuote", new { @id = ProjectSession.QuoteID });
                        }
                        else
                        {
                            return RedirectToAction("Index", "AutoQuote", new { @id = 0 });
                        }
                    }
                    if (quoteInfo.PolicyXMLStep == Enums.PolicyXMLStep.COVERAGE.ToString())
                    {
                        return RedirectToAction("Index", "AutoCoverage", new { @id = ProjectSession.QuoteID });
                    }
                    else
                    {
                        var request = raterServices.PrepareAutoRequestModel(ProjectSession.QuoteID);
                        var rateResponse = raterServices.GetArrowheadResponse(request, ProjectSession.QuoteID, ProjectSession.AgencyDetails.AgencyId);
                        return View("Index", rateResponse);
                    }
                }
                else
                {
                    this.ShowMessage(MessageExtension.MessageType.Error, "Unable to bridge quote to Arrowhead.  Please try creating a new quote directly on Arrowhead or bridging in a new quote.  We apologize for the inconvenience.", true);
                    return RedirectToAction("Login", "Login");
                }

            }
            else
            {
                ProjectSession.IsBridgeError = 1; // set bridge error flag
                this.ShowMessage(MessageExtension.MessageType.Error, "Unable to bridge quote to Arrowhead.  Please try creating a new quote directly on Arrowhead or bridging in a new quote.  We apologize for the inconvenience.", true);
                return RedirectToAction("Login", "Login");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("arrowhead/agency")]
        public ActionResult AgencyProducerCode(string pc, bool search = false)
        {
            PolicyXmlServices policyXmlServices = new PolicyXmlServices();
            try
            {
                if (!string.IsNullOrWhiteSpace(pc))
                {
                    Process.ArrowHeadRTRProd.Agency agencyDetails = null;
                    RTRServiceClient client = new RTRServiceClient();
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                    var agencyProducerDetails = policyXmlServices.CheckProducerCode(pc);
                    if (agencyProducerDetails != null)
                    {
                        // Set Values into Session 
                        ProjectSession.AgencyDetails = agencyProducerDetails;
                        if (agencyProducerDetails.UserId == 0)
                        {
                            agencyProducerDetails.UserId = ConstantVariables.UserID;
                        }
                        ProjectSession.AgencyDetails.UserId = agencyProducerDetails.UserId;


                        // Check Agency Wise On Board Process 
                        if (!agencyProducerDetails.IsSetupCompleted)
                        {
                            return RedirectToAction("Redirect", "AgencyOnBoard", new { id = agencyProducerDetails.AgencyId });
                        }
                        else
                        {
                            if (search == true)
                                return RedirectToAction("Index", "AutoQuote", new { id = 0, search = search });
                            else
                                return RedirectToAction("Index", "AutoQuote", new { id = 0 });
                        }

                    }
                    else
                    {
                        agencyDetails = client.GetAgency(pc);
                        if (agencyDetails != null && !(String.IsNullOrEmpty(agencyDetails.AgencyName)))
                        {
                            AgencyProducerCodeModel agencyProducerCodeModel = new AgencyProducerCodeModel();
                            {
                                agencyProducerCodeModel.ProducerCode = agencyDetails.ProducerCode;
                                agencyProducerCodeModel.Fax = agencyDetails.Fax;
                                agencyProducerCodeModel.Phone = agencyDetails.Phone;
                                agencyProducerCodeModel.State = agencyDetails.State;
                                agencyProducerCodeModel.Zip = agencyDetails.Zip;
                                agencyProducerCodeModel.Address2 = agencyDetails.Address2;
                                agencyProducerCodeModel.Address = agencyDetails.Address;
                                agencyProducerCodeModel.City = agencyDetails.City;
                                agencyProducerCodeModel.AgencyName = agencyDetails.AgencyName;
                            }


                            var producerAgencyDetails = policyXmlServices.InserAgencyByProducerCode(agencyProducerCodeModel);
                            ProjectSession.AgencyDetails = producerAgencyDetails;
                            if (producerAgencyDetails.UserId == 0)
                            {
                                producerAgencyDetails.UserId = ConstantVariables.UserID;
                            }
                            ProjectSession.AgencyDetails.UserId = producerAgencyDetails.UserId;

                            // Check Agency Wise On Board Process 
                            if (!producerAgencyDetails.IsSetupCompleted)
                            {
                                return RedirectToAction("Redirect", "AgencyOnBoard", new { id = producerAgencyDetails.AgencyId });
                            }
                            else
                            {
                                if (search == true)
                                    return RedirectToAction("Index", "AutoQuote", new { id = 0, search = search });
                                else
                                    return RedirectToAction("Index", "AutoQuote", new { id = 0 });
                            }
                        }
                    }

                    if (agencyProducerDetails == null && agencyDetails.AgencyName == null)
                    {
                        this.ShowMessage(MessageExtension.MessageType.Error, "Producer Code doesn't exist", true);
                        return RedirectToAction("Login", "Login");
                    }
                }
                if (search)
                {
                    return RedirectToAction("Login", "Login");
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.A.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                throw ex;
            }

            return View();
        }

    }
}