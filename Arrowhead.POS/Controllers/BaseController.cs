﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Arrowhead.POS.Service;
using Arrowhead.POS.Infrastructure;
using System.Web.Routing;

namespace Arrowhead.POS.Controllers
{
    [ValidateInput(false)]
    public class BaseController : Controller
    {
        #region Base Controller please attach to all controller
        protected override bool DisableAsyncSupport
        {
            get
            {
                return true;
            }
        }
        protected override void ExecuteCore()
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-GB");
            base.ExecuteCore();
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if(ProjectSession.AgencyDetails == null || ProjectSession.AgencyDetails.AgencyName == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    this.ShowMessage(MessageExtension.MessageType.Error, "Unauthorized access, please contact 800-961-6549 for assistance", true);
                    filterContext.Result = new System.Web.Mvc.HttpStatusCodeResult(404);
                    return;
                }
                else
                {
                    this.ShowMessage(MessageExtension.MessageType.Error, "Unauthorized access, please contact 800-961-6549 for assistance", true);
                    filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Login", action = "Login"}));
                    return;
                }                
            }
         
            base.OnActionExecuted(filterContext);
        }
        #endregion
    }
}