﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using RazorEngine;
using RazorEngine.Templating;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("auto")]
    public class AutoCoverageController : BaseController
    {
        // GET: AutoCoverage
        [Route("coverage/{id?}")]
        public ActionResult Index(long? id = 0)
        {
            if (id > 0)
            {
                ProjectSession.QuoteID = Convert.ToInt64(id);
            }

            // Add New Tab If Not Added In ProjectSession.TabDetail
            if (ProjectSession.QuoteID > 0)
                CommonFunctions.AddNewTab(ProjectSession.QuoteID);

            AutoCoverageServices coverageServices = new AutoCoverageServices();
            GeneralServices generalServices = new GeneralServices();
            var coverageList = coverageServices.GetQuoteCoverages(ProjectSession.QuoteID);
            if (coverageList.Count > 0)
                TempData["headerTitle"] = "Coverage Options";



            // Update Quote CurrentStep  Details
            CommonFunctions.UpdateCurrentQuoteStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.COVERAGE.ToString());

            return View(coverageList);
        }

        /// <summary>
        /// Save Coverages To DB
        /// </summary>
        /// <param name="coverage">Coverage list Json String</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveCoverageDetails(string coverage)
        {
            var rateResponseModel = new List<QuoteRateResponseModel>();
            GeneralServices _generalServices = new GeneralServices();
            AutoCoverageServices coverageServices = new AutoCoverageServices();
            AutoQuoteServices _autoQuoteServices = new AutoQuoteServices();
            List<VerifyLienholderModel> lienholderModel = null;
            long quoteStatusId = _generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.QUOTETYPE.ToString(), Enums.QuoteType.QUOTE.ToString());
            try
            {


                TempData["headerTitle"] = "Payment Plan";

                var quoteCoverage = JsonConvert.DeserializeObject<List<Model.StateCoveragesModel>>(coverage);
               
                coverageServices.InsertQuoteCoverage(quoteCoverage, ProjectSession.QuoteID);
                lienholderModel = coverageServices.isLienHolderExist(ProjectSession.QuoteID);
                lienholderModel = lienholderModel.Where(w => w.IsLeinComCollVerified == false).ToList();
                if (lienholderModel != null && lienholderModel.Count > 0)
                {
                    return PartialView("~/Views/AutoCoverage/Partial/_DisplayMissingCoverages.cshtml", lienholderModel);
                }
                else
                {
                    rateResponseModel = GetRateResponse();
                    // Check Quick Quote and Rates Exist
                    if (_autoQuoteServices.IsQuickQuoteByQuoteId(ProjectSession.QuoteID) && _autoQuoteServices.IsQuoteRatesExist(ProjectSession.QuoteID))
                    {
                        _autoQuoteServices.UpdateQuoteTypeByQuoteId(ProjectSession.QuoteID, quoteStatusId);
                    }
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.COV.ToString(), Enums.AuditHistoryOperation.I.ToString(), ex.ToString());

                var err = ex.ToString();
            }
            return PartialView("~/Views/AutoCoverage/Partial/_PayPlans.cshtml", rateResponseModel);
        }

        /// <summary>
        /// Update Purchase Initiated When Click on Buy Now
        /// </summary>
        /// <param name="rateID">RateHistory ID</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdatePurchaseInitiated(int rateID, int planID)
        {
            var rateResponseModel = new List<QuoteRateResponseModel>();
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            NSDServices nsdServices = new NSDServices();
            decimal nsdPrice = 0;

            try
            {
                if (!quotesServices.IsGarrageAddressMissing(ProjectSession.QuoteID))
                {
                    return Json(new { IsError = true, IsException = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (planID > 0)
                    {
                        nsdPrice = nsdServices.SaveAutoQuoteNsdPlan(ProjectSession.QuoteID, planID, ProjectSession.AgencyDetails.UserId);
                    }

                    if (rateID > 0)
                    {
                        quotesServices.UpdatePurchaseInitiated(rateID, ProjectSession.QuoteID, nsdPrice, ProjectSession.AgencyDetails.UserId);
                    }
                    return Json(new { IsError = false, IsException = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.COV.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                var err = ex.ToString();
                return Json(new { IsError = true, IsException = true }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PrintQuote(long quoteId, long rateID)
        {
            CommonServices commonServices = new CommonServices();
            GeneralServices generalServices = new GeneralServices();
            Arrowhead.POS.Service.TemplateService templateService = new Arrowhead.POS.Service.TemplateService();
            string printQuoteDetails = string.Empty;
            try
            {
                TemplateModel template = templateService.GetTemplateDetails(Enums.TemplateType.PRINTQUOTE.ToString());
                if (ProjectSession.QuoteID > 0)
                    printQuoteDetails = GetPrintQuoteTemplate(ProjectSession.QuoteID, rateID, template);
                else
                    printQuoteDetails = GetPrintQuoteTemplate(quoteId, rateID, template);

                return Json(new { PrinQuoteHTML = printQuoteDetails, QuoteId = (ProjectSession.QuoteID > 0 ? ProjectSession.QuoteID : quoteId) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        /// <summary>
        /// Download Pdf File from JSON String
        /// </summary>
        /// <param name="GridHtml"></param>
        /// <returns></returns>
        public FileResult PdfFile(string jsonStr)
        {
            PolicyStatusModel _policyStatusModel = null;
            string[] jsonDetails = null;
            try
            {
                using (MemoryStream stream = new System.IO.MemoryStream())
                {
                    string strDelimitor = "$$$";
                    if (jsonStr.Contains(strDelimitor)) // quick quote 
                    {
                        jsonDetails = jsonStr.Split(new[] { strDelimitor }, StringSplitOptions.None); // split html string and quote id 

                        StringReader sr = new StringReader(jsonDetails[0]);
                        AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                        Document pdfDoc = new Document(PageSize.A4, 40f, 0f, 20f, 0f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                        pdfDoc.Open();
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                        pdfDoc.Close();
                        _policyStatusModel = autoPolicyServices.GetpolicyStatusDetails(Convert.ToInt32(jsonDetails[1]));
                    }
                    else
                    {
                        StringReader sr = new StringReader(jsonStr);
                        AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                        Document pdfDoc = new Document(PageSize.A4, 40f, 0f, 20f, 0f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                        pdfDoc.Open();
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                        pdfDoc.Close();
                        _policyStatusModel = autoPolicyServices.GetpolicyStatusDetails(ProjectSession.QuoteID);
                    }
                    string fileName = " Auto Quote" + " " + "#" + (ProjectSession.QuoteID > 0 ? ProjectSession.QuoteID : Convert.ToInt32(jsonDetails[1])) + " " + _policyStatusModel.FirstName + " " + _policyStatusModel.LastName + ".pdf";
                    return File(stream.ToArray(), "application/pdf", fileName);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="templateModel"></param>
        /// <returns></returns>
        public string GetPrintQuoteTemplate(long quoteId, long rateId, TemplateModel templateModel)
        {
            TemplateModel _template = templateModel;
            Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            CommonServices commonServices = new CommonServices();

            string template = string.Empty;
            try
            {
                var printQuoteDetails = commonServices.GetPrintQuoteDetailsById(quoteId, rateId, ProjectSession.AgencyDetails.AgencyId);
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, printQuoteDetails);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Arrowhead Rate Response 
        /// </summary>
        /// <returns></returns>
        public List<QuoteRateResponseModel> GetRateResponse()
        {
            RaterServices raterServices = new RaterServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            var request = raterServices.PrepareAutoRequestModel(ProjectSession.QuoteID);
            var rateResponse = raterServices.GetArrowheadResponse(request, ProjectSession.QuoteID, ProjectSession.AgencyDetails.AgencyId);
            return rateResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ValidateVIN(long quoteID)
        {
            GeneralServices generalServices = new GeneralServices();
            AutoVehicleServices autoVehicleServices = new AutoVehicleServices();
            long MaritalStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.MARITALSTATUS.ToString(), Enums.MaritalStatus.MARRIED.ToString());
            long SpouseStatusID = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.SPOUSE.ToString());
            try
            {

                var vinValidateList = autoVehicleServices.ValidateVin(ProjectSession.QuoteID);

                //Validate Address Validate Error Exist
                bool IsAddressValidationError = (vinValidateList != null && vinValidateList._quoteInfo != null ? (((!(vinValidateList._quoteInfo.IsGarageAddressExist)) ||
                                                (!(vinValidateList._quoteInfo.IsVehicleExist)) ||
                                                (!(vinValidateList._quoteInfo.IsDriverExist)) ||
                                                (vinValidateList._quoteInfo.IsGarageAddressExist &&  // Garage Address Exist but Zip Code/State/City Missing
                                                (!vinValidateList._quoteInfo.IsZipCodeExist || !vinValidateList._quoteInfo.IsStateExist || !vinValidateList._quoteInfo.IsCityExist)) ||
                                                (vinValidateList._quoteInfo.IsMailingAddressExist && // Mailing Address Exist but garage address missing 
                                                (!(vinValidateList._quoteInfo.IsGarageAddressExist))) ||
                                                ((vinValidateList._quoteInfo.IsMailingZipCodeExist && vinValidateList._quoteInfo.IsMailingStateExist && vinValidateList._quoteInfo.IsMailingCityExist &&
                                                (!vinValidateList._quoteInfo.IsMailingAddressExist))) || // Mailing Address Exist but Zip Code/State/City Missing
                                                (vinValidateList._quoteInfo.IsMailingAddressExist && ((!vinValidateList._quoteInfo.IsMailingCityExist) || (!vinValidateList._quoteInfo.IsMailingStateExist) || (!vinValidateList._quoteInfo.IsMailingZipCodeExist)))) ? true : false) : false);

                if (vinValidateList._vehicleInfo.Count > 0 || vinValidateList._DriverInfo.Count > 0 || IsAddressValidationError)
                {
                    if (vinValidateList._vehicleInfo.Any(x => x.IsVINVerified == false) ||
                        vinValidateList._DriverInfo.Any(x => x.isExcluded == false && String.IsNullOrEmpty(x.DriverLicense)) ||
                        (vinValidateList._DriverInfo.Any(x => x.IsSpouseContain == false) && vinValidateList._DriverInfo.Any(x => x.DriverOrderID != 1 && x.RelationCDId != SpouseStatusID)) ||
                        (vinValidateList._DriverInfo.Count == 1 && vinValidateList._DriverInfo.Any(x => x.DriverOrderID == 1 && x.MaritalStatusID == MaritalStatusID && x.IsSpouseContain == true) && vinValidateList._DriverInfo.Count(x => x.DriverOrderID != 1) == 0) ||
                        vinValidateList._DriverInfo.Any(x => x.ViolationPoints > 8) || IsAddressValidationError || vinValidateList._quoteInfo.IsSpamContact) // Garage Address Exist but Mailing Address Missing
                    {
                        long driverMaritalStatusID = vinValidateList._DriverInfo.Where(x => x.DriverOrderID == 1).Select(x => x.MaritalStatusID).FirstOrDefault();
                        if ((driverMaritalStatusID == MaritalStatusID || vinValidateList._vehicleInfo.Any(x => x.IsVINVerified == false)) ||
                             vinValidateList._DriverInfo.Any(x => x.ViolationPoints > 8) || vinValidateList._DriverInfo.Any(x => x.isExcluded == false && String.IsNullOrEmpty(x.DriverLicense))
                             || IsAddressValidationError || vinValidateList._quoteInfo.IsSpamContact) // Married / Address Validation Error Exist
                        {
                            return PartialView("~/Views/AutoCoverage/Partial/_DisplayMissingVIN.cshtml", vinValidateList);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AVE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                throw ex;
            }
        }

        
    }
}