﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("auto")]
    public class AutoVehicleController : BaseController
    {
        // GET: AutoVehicle
        [Route("vehicle/{id?}")]
        public ActionResult Index(long? id = 0)
        {
            AutoVehicleServices vehicleServices = new AutoVehicleServices();
            AutoDriverServices driverServices = new AutoDriverServices();
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            List<AutoVehicleModel> lstVehicle = new List<AutoVehicleModel>();
            try
            {

                if (id > 0)
                {
                    ProjectSession.QuoteID = Convert.ToInt64(id);
                }
                else if (id == 0)
                {
                    //if(ProjectSession.QuoteID ==0)
                    //    return RedirectToAction("applicant", "auto");
                }

                // Add New Tab If Not Added In ProjectSession.TabDetail
                if (ProjectSession.QuoteID > 0)
                {
                    CommonFunctions.AddNewTab(ProjectSession.QuoteID);
                    lstVehicle = vehicleServices.GetAutoVehicleByQuoteID(ProjectSession.QuoteID);
                }

                int vehicleCount = vehicleServices.GetvehicleCount(ProjectSession.QuoteID);
                ViewBag.VehicleCount = vehicleCount;
                if (vehicleCount == 0)
                {
                    CommonFunctions.UpdateCurrentQuoteStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.VEHICLE.ToString());
                   // return RedirectToAction("AddVehicleDetail", "AutoVehicle", new { id = ProjectSession.QuoteID });

                }

                // ViewBag.DriverCount = driverServices.GetDriverCount(ProjectSession.QuoteID);

                // Update Quote CurrentStep  Details

                vehicleServices.GetAutoVehicleByQuoteID(Convert.ToInt64(id));
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AVE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

            }
            return View(lstVehicle);
        }

        /// <summary>
        /// Add Vehicle Details
        /// </summary>
        /// <returns></returns>
        [Route("vehicle/add/{id}")]
        public ActionResult AddVehicleDetail(long id)
        {
            if (id > 0)
                ProjectSession.QuoteID = Convert.ToInt64(id);

            // Add New Tab If Not Added In ProjectSession.TabDetail
            if (ProjectSession.QuoteID > 0)
                CommonFunctions.AddNewTab(ProjectSession.QuoteID);

            AutoVehicleServices autoVehicleServices = new AutoVehicleServices();
            AutoVehicleModel autoVehicle = new AutoVehicleModel();
            autoVehicle.IsFirstVehicle = autoVehicleServices.IsFirstVehicle(ProjectSession.QuoteID);
            if (autoVehicle.IsFirstVehicle == true)
            {
                //return View("AddVehicle", autoVehicle);
                return PartialView("~/Views/AutoVehicle/Partial/_ManageVehicle.cshtml", autoVehicle);

            }
            else
            {
                return PartialView("~/Views/AutoVehicle/Partial/_ManageVehicle.cshtml", autoVehicle);
            }
        }

        /// <summary>
        /// Edit Vehicle Details
        /// </summary>
        /// <param name="qid">QuoteID</param>
        /// <param name="id">VehicleID</param>
        /// <returns></returns>
        [Route("vehicle/edit/{qid}/{id}")]
        public ActionResult EditVehicleDetail(long qid, long id)
        {
            AutoVehicleServices vehicleServices = new AutoVehicleServices();
            AutoVehicleModel autoVehicle = new AutoVehicleModel();
            autoVehicle = vehicleServices.GetAutoVehicleByVehicleID(Convert.ToInt64(id));
            TempData["vehicleId"] = id;
            //   return View("EditVehicle", autoVehicle);
            return RedirectToAction("Index", "AutoVehicle", new { id = ProjectSession.QuoteID });
        }

        /// <summary>
        /// Delete Vehicle Details
        /// </summary>
        /// <param name="Id">Vehicle ID</param>
        /// <returns></returns>
        [Route("vehicle/delete/{id}")]
        public ActionResult DeleteVehicle(int Id)
        {
            AutoVehicleServices vehicleServices = new AutoVehicleServices();
            vehicleServices.DeleteAutoVehicleByID(Id);
            CommonFunctions.UpdateQuoteToCoverageStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.COVERAGE.ToString()); //reset quote coverage step
            ResetVehicleOrder(ProjectSession.QuoteID); // reset all vehicle order 
            return RedirectToAction("Index", "AutoVehicle", new { @id = ProjectSession.QuoteID });
        }

        /// <summary>
        /// Add / Edit Vehicle Details
        /// </summary>
        /// <param name="vehicle">Auto Vehicle Model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveVehicleDetails(AutoVehicleModel vehicle, string submit)
        {
            AutoVehicleServices vehicleServices = new AutoVehicleServices();
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            try
            {
                vehicle.QuoteId = ProjectSession.QuoteID;
                vehicle.UserId = ProjectSession.AgencyDetails.UserId;

             
                if (vehicle.LienHolderId == 10000)
                {
                    if (vehicle.LienHolderCommercialName != null)
                        vehicle.QuoteId = ProjectSession.QuoteID;
                    vehicle.AgencyId = (int)ProjectSession.AgencyDetails.AgencyId;
                    vehicleServices.AddLienHolder(vehicle);
                }
               




                if (vehicle.AutoVehicleId > 0)
                    vehicleServices.UpdateAutoVehicle(vehicle);
                else
                    vehicleServices.AddAutoVehicle(vehicle);

                // Reset Quote Step to Coverage Step 
                if (vehicleServices.GetvehicleCount(vehicle.QuoteId) >= 1)
                {
                    CommonFunctions.UpdateQuoteToCoverageStep(vehicle.QuoteId, Enums.QuoteOrderStatus.COVERAGE.ToString());
                }

                if (submit.Contains("Save & Continue to Drivers"))
                {
                    TempData["IsVehAddNew"] = false;
                    return RedirectToAction("Index", "AutoDriver", new { @id = ProjectSession.QuoteID });
                }
                else if (submit.Contains("Save & Add New Vehicle"))
                {
                    TempData["IsVehAddNew"] = true;
                    return RedirectToAction("Index", "AutoVehicle", new { @id = ProjectSession.QuoteID });
                }
                else
                {
                    TempData["IsVehAddNew"] = false;
                    return RedirectToAction("Index", "AutoVehicle", new { @id = ProjectSession.QuoteID });
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AVE.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                return RedirectToAction("Index", "AutoVehicle");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult EditVehicleDetailTab(long qid, long id)
        {
            AutoVehicleModel autoVehicleModel = new AutoVehicleModel();
            AutoVehicleServices autoVehicleServices = new AutoVehicleServices();
            autoVehicleModel = autoVehicleServices.GetAutoVehicleByVehicleID(Convert.ToInt64(id));
            return PartialView("~/Views/AutoVehicle/Partial/_ManageVehicle.cshtml", autoVehicleModel);
        }

        /// <summary>
        /// Get Vehicle Make By Year
        /// </summary>
        /// <param name="year">Year</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllVehicleMakesByYear(string year)
        {
            var lst = new List<SelectListItem>();
            if (string.IsNullOrWhiteSpace(year))
                lst = new List<SelectListItem>();
            else
                lst = ITCVehicleServices.GetAllVehicleMakesByYear(year);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Vehicle Model By Year And Make
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="make">ake</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllVehicleModelsByYearMake(string year, string make)
        {
            var lst = new List<SelectListItem>();
            if (string.IsNullOrWhiteSpace(year) || string.IsNullOrWhiteSpace(make))
                lst = new List<SelectListItem>();
            else
                lst = ITCVehicleServices.GetAllVehicleModelsByYearMake(year, make);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get All Vehicle VIN by Year Make ANd Model
        /// </summary>
        /// <param name="year">year</param>
        /// <param name="make">make</param>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllVehicleModelsByYearMakeModel(string year, string make, string model)
        {
            var lst = string.Empty;
            if (string.IsNullOrWhiteSpace(year) || string.IsNullOrWhiteSpace(make))
                lst = string.Empty;
            else
                lst = ITCVehicleServices.GetVinByVehicleDetail(year, make, model);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get VIN Information
        /// </summary>
        /// <param name="vin">VIN Number</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetInfoByVin(string vin)
        {
            ITCVehicleServices itcVehicleServices = new ITCVehicleServices();
            var vehicleInformation = itcVehicleServices.DecodeVIN(vin);

            if (vehicleInformation != null)
            {
                var info = itcVehicleServices.DecodeVIN(vin);

                if (info != null)
                {
                    vehicleInformation.Maker = info.Maker;
                    vehicleInformation.Model = info.Model;
                    vehicleInformation.Year = info.Year;
                }
            }

            return Json(vehicleInformation, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetLienHolder(long id)
        {
            AutoVehicleServices vehicleServices = new AutoVehicleServices();
            var lienholder = vehicleServices.GetLienHolderById(id);
            return Json(lienholder, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteId"></param>
        public void ResetVehicleOrder(long QuoteId)
        {
            AutoVehicleServices _autoVehicleServices = new AutoVehicleServices();
            try
            {
                _autoVehicleServices.ResetVehicleByQuoteId(QuoteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}