﻿using Arrowhead.POS.Service;
using Arrowhead.POS.Model;
using Arrowhead.POS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Arrowhead.POS.Infrastructure;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("nsd")]
    public class NSDController : Controller
    {
        // GET: NSD
        //  [Route("create/{id?}")]
        [Route("create/{id}")]
        public JsonResult Index(long id = 0)
        {
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            NSDServices _nsdServices = new NSDServices();
            NSDContractModel _nsdContractModel = new NSDContractModel();

            try
            {

                if (id > 0)
                {
                    if(_autoPolicyServices.IsNSDContractNotExist(id))
                    {
                        _nsdContractModel = _nsdServices.NSDProcess(id, ProjectSession.AgencyDetails.UserId);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QN.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                return Json(ex, JsonRequestBehavior.AllowGet);
            }
            return Json(_nsdContractModel,JsonRequestBehavior.AllowGet);
        }
    }
}