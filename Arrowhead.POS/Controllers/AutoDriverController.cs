﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("auto")]
    public class AutoDriverController : BaseController
    {
        [Route("driver/{id?}")]
        public ActionResult Index(long? id = 0)
        {
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            GeneralServices generalServices = new GeneralServices();
            List<AutoDriverModel> lstDriver = new List<AutoDriverModel>();
            try
            {

                if (id > 0)
                {
                    ProjectSession.QuoteID = Convert.ToInt64(id);
                }
                else if (id == 0)
                {
                    //if(ProjectSession.QuoteID ==0)
                    //    return RedirectToAction("driver", "auto");
                }

                // Add New Tab If Not Added In ProjectSession.TabDetail
                if (ProjectSession.QuoteID > 0)
                {
                    CommonFunctions.AddNewTab(ProjectSession.QuoteID);
                    lstDriver = autoDriverServices.GetDriverByQuoteID(ProjectSession.QuoteID);
                    AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                    var paymentDetails = autoPolicyServices.GetPaymentByQuoteID(ProjectSession.QuoteID);
                    ViewBag.paymentCompleted = paymentDetails!=null ? paymentDetails.IsPaymentCompleted : false;
                }
                    

                int driverCount = autoDriverServices.GetDriverCount(ProjectSession.QuoteID);
                ViewBag.DriverCount = driverCount;

                // Update Quote CurrentStep  Details
                autoDriverServices.GetDriverByQuoteID(Convert.ToInt64(id));
                if (driverCount == 0)
                {
                    CommonFunctions.UpdateCurrentQuoteStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.DRIVER.ToString());
                    //return RedirectToAction("AddDriverDetail", "AutoDriver", new { id = ProjectSession.QuoteID });
                }
                else
                {
                    var addSpouse = autoDriverServices.IsAddSpouse(ProjectSession.QuoteID);
                    if (addSpouse == true && driverCount > 0 && driverCount < 8)
                    {
                        this.ShowMessage(MessageExtension.MessageType.Info, "Required to add a spouse", true);
                        return View(lstDriver);
                        //return RedirectToAction("AddDriverDetail", "AutoDriver", new { id = ProjectSession.QuoteID });
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AD.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
            }
            return View(lstDriver);
            //return View();
        }

        /// <summary>
        /// Add Driver Detais
        /// </summary>
        /// <param name="id">QuoteID</param>
        /// <returns></returns>
        [Route("driver/add/{id}")]
        public ActionResult AddDriverDetail(long id)
        {
            if (id > 0)
                ProjectSession.QuoteID = Convert.ToInt64(id);

            GeneralServices generalServices = new GeneralServices();
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            AutoDriverModel autoDriverModel = new AutoDriverModel();
            autoDriverModel.IsFirstDriver = autoDriverServices.IsFirstDriver(ProjectSession.QuoteID);
            autoDriverModel.StateId = autoDriverServices.GetApplicantStateID(ProjectSession.QuoteID);
            autoDriverModel.LicenseStatus = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.LICENSESTATUS.ToString(), Enums.LicenseStatus.VALID.ToString());
            autoDriverModel.QuoteId = ProjectSession.QuoteID;
            autoDriverModel.IsExcluded = false;
            autoDriverModel.Gender = Enums.Gender.M.ToString();
            autoDriverModel.MaritalStatusCode = Enums.MaritalStatus.SINGLE.ToString();
            var customer = autoDriverServices.GetCustomerDetailsByQuoteId(ProjectSession.QuoteID);
            if (autoDriverModel.IsFirstDriver == true)
            {
                autoDriverModel.FirstName = customer.FirstName;
                autoDriverModel.LastName = customer.LastName;
            }

            var addSpouse = autoDriverServices.IsAddSpouse(ProjectSession.QuoteID);
            if (addSpouse == true)
            {
                long fkrelationId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.DRIVERRELATION.ToString(), Enums.DRIVERRELATION.SPOUSE.ToString());
                autoDriverModel.RelationCDId = fkrelationId;
                autoDriverModel.MaritalStatusCode = Enums.MaritalStatus.MARRIED.ToString();
                var firstDriverId = autoDriverServices.GetDefaultDriverID(ProjectSession.QuoteID);
                var driverDetails = autoDriverServices.GetDriverByDriverID(firstDriverId);
                if (driverDetails.Gender == "M")
                {
                    autoDriverModel.Gender = Enums.Gender.F.ToString();
                }
                else if (driverDetails.Gender == "F")
                {
                    autoDriverModel.Gender = Enums.Gender.M.ToString();
                }

             }
            

            TempData["QuoteDriverViolationsList"] = new List<AutoDriverViolationModel>();
            TempData.Keep("QuoteDriverViolationsList");
             
            //return View("AddDriver", autoDriverModel);
            return PartialView("~/Views/AutoDriver/Partial/_ManageDriver.cshtml", autoDriverModel);
        }

        /// <summary>
        /// Edit Driver Details
        /// </summary>
        /// <param name="qid">QuoteID</param>
        /// <param name="id">Driver ID</param>
        /// <returns></returns>
        [Route("driver/edit/{qid}/{id}")]
        public ActionResult EditDriverDetail(long qid, long id)
        {
            AutoDriverModel autoDriverModel = new AutoDriverModel();
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            autoDriverModel = autoDriverServices.GetDriverByDriverID(Convert.ToInt64(id));
            if (autoDriverModel != null)
            {
                autoDriverModel.IsFirstDriver = autoDriverServices.IsFirstDriver(ProjectSession.QuoteID, id);
                autoDriverModel.QuoteId = ProjectSession.QuoteID;
            }

            TempData["QuoteDriverViolationsList"] = autoDriverServices.GetViolationByDriverID(Convert.ToInt64(id));
            TempData.Keep("QuoteDriverViolationsList");
            TempData["driverId"] = id;
            //return View("EditDriver", autoDriverModel);
            return RedirectToAction("Index", "AutoDriver", new { id = ProjectSession.QuoteID });
        }

        public PartialViewResult EditDriverDetailTab(long qid, long id)
        {
            AutoDriverModel autoDriverModel = new AutoDriverModel();
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            autoDriverModel = autoDriverServices.GetDriverByDriverID(Convert.ToInt64(id));
            if (autoDriverModel != null)
            {
                autoDriverModel.IsFirstDriver = autoDriverServices.IsFirstDriver(ProjectSession.QuoteID, id);
                autoDriverModel.QuoteId = ProjectSession.QuoteID;
            }

            TempData["QuoteDriverViolationsList"] = autoDriverServices.GetViolationByDriverID(Convert.ToInt64(id));
            TempData.Keep("QuoteDriverViolationsList");
            return PartialView("~/Views/AutoDriver/Partial/_ManageDriver.cshtml", autoDriverModel);
        }

        /// <summary>
        /// Delete DriverID
        /// </summary>
        /// <param name="Id"> Driver ID</param>
        /// <returns></returns>
        [Route("driver/delete/{id}")]
        public ActionResult DeleteDriver(long Id)
        {
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            autoDriverServices.DeleteDriverInfo(Id);
            autoDriverServices.ResetDriverOrder(ProjectSession.QuoteID); // reset driver order 
            CommonFunctions.UpdateQuoteToCoverageStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.COVERAGE.ToString()); //reset quote coverage step
            return RedirectToAction("Index", "AutoDriver", new { @id = ProjectSession.QuoteID });

        }

        /// <summary>
        /// Add / Edit Driver Details
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveDriverDetails(AutoDriverModel driver, string submit)
        {
            //var driverRelationStatus = db.auto
            CommonServices commonServices = new CommonServices();
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            AutoVehicleServices autoVehicleServices = new AutoVehicleServices();
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            var driverRelationStatus = commonServices.GetGeneralStatusCodeById(driver.RelationCDId);
           
            if (driverRelationStatus == Core.Enums.DRIVERRELATION.SPOUSE.ToString())
            {
                driver.MaritalStatusCode = Enums.MaritalStatus.MARRIED.ToString();
            }
            bool _isDriverRated = false;
            int _driverCount = 0;
            int _vehicleCount = 0;
            try
            {
                driver.QuoteId = ProjectSession.QuoteID;
                _driverCount = autoDriverServices.GetDriverCount(ProjectSession.QuoteID);
                _vehicleCount = autoVehicleServices.GetvehicleCount(ProjectSession.QuoteID);
               
                var _ratedDriverList = autoDriverServices.GetRatedDriverList(driver.QuoteId);
                
                if(_vehicleCount >= 3) // Vehicle Count > 3 then Max 5 Rated Driver Allowed 
                {
                    if (_ratedDriverList.Count >= 5)  // Match driverId with Rated Driver list 
                    {
                        _isDriverRated = _ratedDriverList.Any(x => x == driver.AutoDriverId);
                        if (!_isDriverRated && driver.IsExcluded == false) // if isExcluded is false then Validation Will Fire 
                        {
                            this.ShowMessage(MessageExtension.MessageType.Error, "You can't add more than 5 rated drivers.", true);
                            return RedirectToAction("EditDriverDetail", "AutoDriver", new { qid = ProjectSession.QuoteID, id = driver.AutoDriverId });
                        }
                    }
                }
                else if (_ratedDriverList.Count >= 4)  // Match driverId with Rated Driver list 
                {
                    _isDriverRated = _ratedDriverList.Any(x => x == driver.AutoDriverId);
                    if (!_isDriverRated && driver.IsExcluded == false) // if isExcluded is false then Validation Will Fire 
                    {
                        this.ShowMessage(MessageExtension.MessageType.Error, "You can't add more than 4 rated drivers.", true);
                        return RedirectToAction("EditDriverDetail", "AutoDriver", new { qid = ProjectSession.QuoteID, id = driver.AutoDriverId });
                    }
                }

                driver.AgencyId =(int)ProjectSession.AgencyDetails.AgencyId;
                List<AutoDriverViolationModel> lst = (List<AutoDriverViolationModel>)TempData["QuoteDriverViolationsList"] ?? new List<AutoDriverViolationModel>();
                driver.UserId = ProjectSession.AgencyDetails.UserId;
                driver.QuoteId = ProjectSession.QuoteID;
                if (driver.AutoDriverId > 0)
                {
                    autoDriverServices.DeleteDriverViolationByID(driver.AutoDriverId);
                    autoDriverServices.UpdateAutoDriver(driver);
                }
                else
                {
                    driver = autoDriverServices.AddAutoDriver(driver);
                }

                // Reset Quote Step to Coverage Step 
                if(autoDriverServices.GetDriverCount(driver.QuoteId) >= 1)
                {
                    CommonFunctions.UpdateQuoteToCoverageStep(driver.QuoteId,Enums.QuoteOrderStatus.COVERAGE.ToString());
                }

                if (lst.Count > 0)
                {
                    foreach (var item in lst)
                    {
                        if (item.IsMVR == false) // if not mvr then only add violation 
                        {
                            item.DriverId = driver.AutoDriverId;
                            item.AgencyId =(int) ProjectSession.AgencyDetails.AgencyId;
                            item.QuoteId = ProjectSession.QuoteID;
                            item.UserId = ProjectSession.AgencyDetails.UserId;
                            autoDriverServices.InsertDriverViolation(item);
                        }
                    }
                }

                

                if(submit.Contains("Save & Continue to Coverages"))
                {
                    TempData["IsAddNewDriver"] = false;
                    return RedirectToAction("Index", "AutoCoverage", new { @id = ProjectSession.QuoteID });
                }                    
                else if(submit.Contains("Save & Add New Driver"))
                {
                    TempData["IsAddNewDriver"] = true;
                    return RedirectToAction("Index", "AutoDriver", new { @id = ProjectSession.QuoteID });
                }
                else
                {
                    TempData["IsAddNewDriver"] = false;
                    return RedirectToAction("Index", "AutoDriver", new { @id = ProjectSession.QuoteID });
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AD.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                return RedirectToAction("Index", "AutoDriver");
            }
        }

        /// <summary>
        /// Save Violation
        /// </summary>
        /// <param name="incidentDate">Violation Date</param>
        /// <param name="violationId">Violation ID</param>
        /// <returns></returns>
        public PartialViewResult SaveIncident(DateTime incidentDate, int violationId)
        {
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            try
            {
                List<AutoDriverViolationModel> lst = (List<AutoDriverViolationModel>)TempData["QuoteDriverViolationsList"] ?? new List<AutoDriverViolationModel>();

                var violation = autoDriverServices.GetViolationByID(violationId);

                lst.Add(new AutoDriverViolationModel()
                {
                    ViolationDate = Convert.ToDateTime(incidentDate),
                    ViolationName = violation.ViolationName,
                    AutoViolationId = violationId,
                    ViolationCode = violation.ViolationCode,
                    ViolationType = violation.ViolationType
                });

                TempData["QuoteDriverViolationsList"] = lst;
                TempData.Keep("QuoteDriverViolationsList");
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.ADV.ToString(), Enums.AuditHistoryOperation.I.ToString(), ex.ToString());

            }
            return PartialView("~/Views/AutoDriver/Partial/_IncidentList.cshtml");
        }

        /// <summary>
        /// Remove Violation From Temp Data
        /// </summary>
        /// <param name="id">Violation Row ID</param>
        /// <returns></returns>
        public PartialViewResult ClearIncidents(int row)
        {
            AutoDriverServices autoDriverServices = new AutoDriverServices();
            if (row > 0)
            {
                List<AutoDriverViolationModel> lst = (List<AutoDriverViolationModel>)TempData["QuoteDriverViolationsList"] ?? new List<AutoDriverViolationModel>();
                //lst = new List<AutoDriverViolationModel>();
                lst.RemoveAt(row - 1);
                TempData["QuoteDriverViolationsList"] = lst;
                TempData.Keep("QuoteDriverViolationsList");
            }
            else
            {
                // Fetch Violations By Quote ID 
                var _lstViolations = autoDriverServices.GetViolationByQuoteId(ProjectSession.QuoteID);

                // Fetch MVR Violations 
                var _lstMVRViolations = _lstViolations.Where(x => x.IsMVR == true).ToList();

                if (_lstMVRViolations!=null && _lstMVRViolations.Count > 0)
                {
                    TempData["QuoteDriverViolationsList"] = _lstMVRViolations;
                    TempData.Keep("QuoteDriverViolationsList");
                }
                else
                {
                    TempData["QuoteDriverViolationsList"] = new List<AutoDriverViolationModel>();
                    TempData.Keep("QuoteDriverViolationsList");
                }
            }

            return PartialView("~/Views/AutoDriver/Partial/_IncidentList.cshtml");
        }

    }
}

