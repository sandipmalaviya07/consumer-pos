﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using DocuSign.eSign.Api;
using DocuSign.eSign.Model;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Stripe;
using System.Net;
using System.Linq.Dynamic;
using RazorEngine.Templating;
using RazorEngine;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using Arrowhead.POS.PaymentProcess;
using log4net;
using Arrowhead.POS.Process;
using System.Threading.Tasks;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("auto")]
    public class AutoPolicyController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AutoPolicyController));
        [Route("policy/{id}")]
        public ActionResult Index(long id = 0)
        {
            try
            {

                if (id > 0)
                {
                    ProjectSession.QuoteID = Convert.ToInt64(id);
                }

                // Add New Tab If Not Added In ProjectSession.TabDetail
                if (ProjectSession.QuoteID > 0)
                    CommonFunctions.AddNewTab(ProjectSession.QuoteID);


                AutoQuoteServices quoteServices = new AutoQuoteServices();
                GeneralServices generalServices = new GeneralServices();

                // Update Quote CurrentStep  Details
                CommonFunctions.UpdateCurrentQuoteStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.POLICY.ToString());



                QuoteRateResponseModel rateHistoryList = quoteServices.GetRateHistoryDetailsByQuoteID(ProjectSession.QuoteID);
                ViewBag.RateHistoryList = rateHistoryList;

                bool isQuoteCompleted = quoteServices.IsQuoteCompleted(ProjectSession.QuoteID);
                ViewBag.IsQuoteCompleted = isQuoteCompleted;
                AutoPolicyServices autopolicyServices = new AutoPolicyServices();
                var paymentDetails = autopolicyServices.GetPaymentByQuoteID(ProjectSession.QuoteID);
                var policyDetails = autopolicyServices.GetpolicyStatusDetails(ProjectSession.QuoteID);
                ViewBag.policyDetails = policyDetails;

                DateTime currentDate = ConvertTo.GetEstTimeNow();
                if (isQuoteCompleted == false)
                {
                    if (paymentDetails != null && paymentDetails.EffectiveDate != null)
                    {
                        if (paymentDetails.EffectiveDate.Value.Date < currentDate.Date)
                        {
                            if (quoteServices.UpdateEffectiveAndExpireDateByQuoteId(ProjectSession.QuoteID)) // if update then display message
                            {
                                this.ShowMessage(MessageExtension.MessageType.Error, "Effective date was updated to:" + currentDate.ToString("MM/dd/yyyy"));
                                CommonFunctions.UpdateQuoteToCoverageStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.COVERAGE.ToString());
                                return RedirectToAction("Index", "AutoCoverage", new { @id = ProjectSession.QuoteID });
                            }
                        }
                    }
                    if (rateHistoryList.DownPayment == 0)
                    {
                        generalServices.UpdateQuoteCurrentStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.COVERAGE.ToString());
                        return RedirectToAction("Index", "AutoCoverage", new { @id = ProjectSession.QuoteID });
                    }
                }

                return View(paymentDetails);
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QP.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                this.ShowMessage(MessageExtension.MessageType.Error, ex.Message.ToString(), true);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("policy/{id?}")]
        public ActionResult SaveQuotesPayment(PaymentRequestModel payment)
        {
            try
            {
                //Log.Debug("SaveQuotesPayment request start :" + Newtonsoft.Json.JsonConvert.SerializeObject(payment));
                Log.Debug("SaveQuotesPayment request start :" + Newtonsoft.Json.JsonConvert.SerializeObject("Quote: " + payment.QuoteID + ", PaymentType: " + payment.PaymentMethodCode + ", Amount: " + payment.Amount));

                AutoPolicyServices autopolicyServices = new AutoPolicyServices();
                AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
                GeneralServices generalServices = new GeneralServices();
                NSDServices nsdServices = new NSDServices();
                NSDContractModel _nsdContractModel = new NSDContractModel();
                decimal NSDAmount = 0;
                payment.UserId = ProjectSession.AgencyDetails.UserId;

                if (payment.PaymentMethodCode != Enums.PaymentMethod.CREDITCARD.ToString())
                {
                    payment.CCFees = 0;
                }

                // Manage Error handling for Declined CC 
                if (payment.TotalPremium == 0)
                {
                    var _payment = autopolicyServices.GetPaymentByQuoteID(payment.QuoteID);
                    payment.TotalPremium = _payment.TotalPremium;
                }

                var _downpayment = autopolicyServices.GetDownPaymentByQuoteId(payment.QuoteID);
                NSDAmount = autopolicyServices.GetNSDAmount(payment.QuoteID);
                payment.TotalNSD = NSDAmount;
                if ((payment.Amount) > (_downpayment + NSDAmount + payment.CCFees))
                {
                    autopolicyServices.UpdateAutoRateHistory(payment);
                }

                //payment.TotalCarrier = payment.Amount - payment.CCFees;

                payment.AgencyId = ProjectSession.AgencyDetails.AgencyId;
                var _custDetails = autoQuoteServices.GetPolicyHolderDetailById(payment.QuoteID);

                if (!string.IsNullOrEmpty(payment.PhoneNumber))
                {
                    payment.PhoneNumber = payment.PhoneNumber.ToPhoneNumber();
                }
                // Payment Reminder Update Applicant Contact if Applicant Contact / Payment Contact Not Matched
                if (_custDetails != null && !string.IsNullOrEmpty(_custDetails.ContactNo) && !string.IsNullOrEmpty(payment.PhoneNumber) && (_custDetails.ContactNo != payment.PhoneNumber))
                {
                    autoQuoteServices.UpdateQuoteContactDetails(payment.QuoteID, payment.PhoneNumber.ToPhoneNumber());
                }

                var _isNSDPlanSelected = autopolicyServices.IsNSDExists(payment.QuoteID);
                if (_isNSDPlanSelected)
                {
                    //NSDAmount = autopolicyServices.GetNSDAmount(payment.QuoteID);
                    payment.TotalCarrier = payment.Amount - NSDAmount - payment.CCFees;
                }
                else
                {
                    payment.TotalCarrier = payment.Amount - payment.CCFees;
                }

                string _Issuccess = Enums.PAYMENTSTATUS.SUCCESS.ToString();
                string errorMessage = string.Empty;
                string _PGCode = generalServices.GetActivePGCode();
                int _paymentGatewayMethodId = generalServices.GetActivePGID();

                PaymentProcess.TranzPayProcess tranzPayProcess = new PaymentProcess.TranzPayProcess();
                PaymentProcess.StripeProcess stripeProcess = new PaymentProcess.StripeProcess();
                PaymentProcess.AuthorizeProcess authorizeProcess = new PaymentProcess.AuthorizeProcess();

                if (payment.PaymentMethodCode == Enums.PaymentMethod.CREDITCARD.ToString())
                {
                    Log.Debug("SaveQuotesPayment CC Process start ");
                    payment.PaymentGatewayProviderId = _paymentGatewayMethodId; // set PaymentGatewayProviderId                

                    PaymentProcessRequestModel requestModel = new PaymentProcessRequestModel()
                    {
                        CarrierAmount = payment.TotalCarrier,
                        CCExpMonth = Convert.ToInt32(payment.PaymentMonth),
                        CCExpYear = Convert.ToInt32(payment.PaymentYear),
                        CCHolderName = payment.FullName,
                        CCNumber = payment.CardNumber.Replace("-", "").Replace("  ", "").Trim(),
                        CurrencyCode = "USA",
                        CustomerName = payment.FullName,
                        CVVNumber = payment.CVV,
                        QuoteID = payment.QuoteID,
                        TotalAmount = payment.Amount,
                        NSDAmount = payment.TotalNSD,
                        CustomerAddress1 = _custDetails.Address1,
                        CustomerAddress2 = _custDetails.Address2,
                        CustomerCity = _custDetails.City,
                        CustomerEmail = _custDetails.EmailId,
                        CustomerFirstName = _custDetails.FirstName,
                        CustomerLastName = _custDetails.LastName,
                        CustomerPhone = _custDetails.ContactNo,
                        CustomerState = _custDetails.StateAbbr,
                        CustomerZipcode = payment.ZipCode, // user entered zip code
                        Description = "Payment toward policy",
                        Token = payment.PGToken,
                        AgencyName = ProjectSession.AgencyDetails.AgencyName
                    };

                    if (_PGCode == Enums.PaymentGateway.STRIPE.ToString())
                    {
                        Log.Debug("SaveQuotesPayment CC STRIPE Start ");
                        var _respProcess = stripeProcess.CapturePayment(requestModel);
                        _Issuccess = _respProcess.StatusMessage.Contains("pending")
                              ? Enums.PAYMENTSTATUS.INITIATED.ToString()
                              : _respProcess.StatusMessage.Contains("suc") ? Enums.PAYMENTSTATUS.SUCCESS.ToString() :
                                  Enums.PAYMENTSTATUS.FAIL.ToString();
                        payment.PgTransactionId = _respProcess.TransactionID;
                        payment.PGCustomerId = _respProcess.CustomerId;
                    }
                    else if (_PGCode == Enums.PaymentGateway.TRANZPAY.ToString())
                    {
                        Log.Debug("SaveQuotesPayment CC TRANZPAY Start ");
                        var _respProcess = tranzPayProcess.CapturePayment(requestModel);

                        _Issuccess = _respProcess.Status ? "success" : "fail";
                        errorMessage = _respProcess.StatusMessage;
                        payment.PgTransactionId = _respProcess.TransactionID;
                        payment.PGCustomerId = _respProcess.CustomerId;
                    }
                    else if (_PGCode == Enums.PaymentGateway.AUTHORIZENET.ToString())
                    {
                        Log.Debug("SaveQuotesPayment CC  AUTHORIZENET Start ");
                        var _respProcess = authorizeProcess.AuthorizeCCPayment(requestModel);

                        _Issuccess = _respProcess.Status ? Enums.PAYMENTSTATUS.INITIATED.ToString() : Enums.PAYMENTSTATUS.FAIL.ToString();
                        errorMessage = _respProcess.StatusMessage;
                        payment.PgTransactionId = _respProcess.TransactionID;
                        payment.PGCustomerId = _respProcess.CustomerId;
                        payment.AuthCode = _respProcess.AuthCode;

                        if (String.IsNullOrEmpty(_respProcess.TransactionID) || _respProcess.TransactionID.ToString() == "0" || _respProcess.StatusMessage.ToUpper() == "ERROR")
                        {
                            Log.Debug("SaveQuotesPayment CC  AUTHORIZENET Fail ");
                            this.ShowMessage(MessageExtension.MessageType.Error, _respProcess.StatusMessage, true);
                            decimal amount = payment.Amount - payment.CCFees;
                            payment.Amount = amount;
                            return RedirectToQuotePaymentIndex(payment); // error cc authorize 
                        }
                    }

                }
                else if (payment.PaymentMethodCode == Enums.PaymentMethod.ECHECK.ToString())
                {
                    CommonServices commonServices = new CommonServices();
                    var hostUrl = commonServices.CheckHost(Request.Url.Host);
                    if ((hostUrl.Contains("arrowhead-pos-qa") || hostUrl.Contains("localhost")) && !_isNSDPlanSelected)
                    {
                        Log.Debug("SaveQuotesPayment ECHECK Without NSD start ");
                        payment.PaymentGatewayProviderId = _paymentGatewayMethodId; // set PaymentGatewayProviderId
                        payment.QuoteID = ProjectSession.QuoteID;
                        autopolicyServices.ManageEcheck(payment);
                    }
                    else
                    {
                        Log.Debug("SaveQuotesPayment ECHECK start ");
                        payment.PaymentGatewayProviderId = _paymentGatewayMethodId; // set PaymentGatewayProviderId
                        payment.QuoteID = ProjectSession.QuoteID;
                        autopolicyServices.ManageEcheck(payment);
                        PaymentProcessRequestModel requestModel = new PaymentProcessRequestModel()
                        {
                            CarrierAmount = payment.TotalCarrier,
                            CurrencyCode = "USA",
                            CustomerName = payment.FullName,
                            QuoteID = payment.QuoteID,
                            TotalAmount = payment.Amount,
                            CustomerAddress1 = _custDetails.Address1,
                            CustomerAddress2 = _custDetails.Address2,
                            CustomerCity = _custDetails.City,
                            CustomerEmail = _custDetails.EmailId,
                            CustomerFirstName = _custDetails.FirstName,
                            CustomerLastName = _custDetails.LastName,
                            CustomerPhone = _custDetails.ContactNo,
                            CustomerState = _custDetails.StateAbbr,
                            CustomerZipcode = _custDetails.ZipCode,
                            Description = "Payment toward policy",
                            BankRoutingNumber = payment.RoutingNumber,
                            BankAccountType = generalServices.GetCodeFromGeneralStatusType(payment.AccountTypeId),  //db.GeneralStatusTypes.Where(w => w.ID == payment.AccountTypeId).Select(s => s.Code).FirstOrDefault(),
                            BankAccountNumber = payment.AccountNumber,
                            CheckType = "bu"
                        };
                        if (_PGCode == Enums.PaymentGateway.STRIPE.ToString())
                        {
                            Log.Debug("SaveQuotesPayment ECHECK STRIPE ");
                            var _respProcess = stripeProcess.CaptureACHPayment(requestModel);
                            _Issuccess = _respProcess.StatusCode.Contains("pending")
                                ? Enums.PAYMENTSTATUS.INITIATED.ToString()
                                : _respProcess.StatusMessage.Contains("suc") ? Enums.PAYMENTSTATUS.SUCCESS.ToString() :
                                    Enums.PAYMENTSTATUS.FAIL.ToString();

                            //errorMessage = _respProcess.StatusMessage;
                            payment.PgTransactionId = _respProcess.TransactionID;
                            payment.PGCustomerId = _respProcess.CustomerId;

                        }
                        else if (_PGCode == Enums.PaymentGateway.TRANZPAY.ToString())
                        {
                            Log.Debug("SaveQuotesPayment ECHECK TRANZPAY ");
                            var _respProcess = tranzPayProcess.CaptureACHPayment(requestModel);
                            _Issuccess = _respProcess.Status ? Enums.PAYMENTSTATUS.SUCCESS.ToString()
                                : Enums.PAYMENTSTATUS.FAIL.ToString();
                            errorMessage = _respProcess.StatusMessage;
                            payment.PGCustomerId = _respProcess.CustomerId;
                            payment.PgTransactionId = _respProcess.TransactionID;
                            payment.AuthCode = _respProcess.AuthCode;
                        }
                        else if (_PGCode == Enums.PaymentGateway.AUTHORIZENET.ToString())
                        {
                            Log.Debug("SaveQuotesPayment ECHECK AUTHORIZENET ");
                            var _respProcess = authorizeProcess.CaptureACHPayment(requestModel);
                            _Issuccess = _respProcess.Status ? Enums.PAYMENTSTATUS.INITIATED.ToString()
                                : Enums.PAYMENTSTATUS.FAIL.ToString();
                            errorMessage = _respProcess.StatusMessage;
                            payment.PGCustomerId = _respProcess.CustomerId;
                            payment.PgTransactionId = _respProcess.TransactionID;
                            if (String.IsNullOrEmpty(_respProcess.TransactionID) || _respProcess.TransactionID.ToString() == "0" || _respProcess.StatusMessage.ToUpper() == "ERROR")
                            {
                                this.ShowMessage(MessageExtension.MessageType.Error, _respProcess.StatusMessage, true);
                                return RedirectToAction("Index", "AutoPolicy", new
                                {
                                    @id = ProjectSession.QuoteID
                                });
                            }
                        }
                    }
                }

                if (payment != null)
                {
                    Log.Debug("SaveQuotesPayment Payment entry processing start " + Newtonsoft.Json.JsonConvert.SerializeObject("Quote: " + payment.QuoteID + ", Payment Method: " + payment.PaymentMethodCode + ", TransID: " + payment.PgTransactionId + ", Amount: " + payment.Amount));
                    autopolicyServices.UpdateQuotePayment(payment, _PGCode, _Issuccess);
                    if ((_Issuccess.ToUpper() == Enums.PAYMENTSTATUS.SUCCESS.ToString() || _Issuccess.ToUpper() == Enums.PAYMENTSTATUS.INITIATED.ToString()) && _isNSDPlanSelected)
                    {
                        Log.Debug("SaveQuotesPayment Payment NSD Process ");
                        _nsdContractModel = nsdServices.NSDProcess(ProjectSession.QuoteID, ProjectSession.AgencyDetails.UserId);
                        if (_nsdContractModel.StatusCode != "200")
                        {
                            Log.Debug("SaveQuotesPayment Payment NSD Process Failed");
                            this.ShowMessage(MessageExtension.MessageType.Error, _nsdContractModel.StatusMessage, true);
                        }
                    }
                }

                return RedirectToAction("Index", "AutoPolicy", new
                {
                    @id = ProjectSession.QuoteID
                });
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QP.ToString(), Enums.AuditHistoryOperation.I.ToString(), ex.ToString());
                throw ex;
            }
        }

        /// <summary>
        ///  While Error Return Partial View 
        /// </summary>
        /// <param name="_paymentReqModel"></param>
        /// <returns></returns>
        public ActionResult RedirectToQuotePaymentIndex(PaymentRequestModel _paymentReqModel)
        {
            try
            {
                AutoQuoteServices _autoQuoteServices = new AutoQuoteServices();
                QuoteRateResponseModel rateHistoryList = _autoQuoteServices.GetRateHistoryDetailsByQuoteID(_paymentReqModel.QuoteID);
                ViewBag.RateHistoryList = rateHistoryList;
                return View("~/Views/AutoPolicy/Index.cshtml", _paymentReqModel);
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QP.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                throw ex;
            }
        }

        /// <summary>
        /// Open Model For Docusign Sign link To Customer
        /// </summary>
        /// <returns></returns>
        public PartialViewResult SendCustomerLinkModel()
        {
            DocusignServices docusignServices = new DocusignServices();
            CommonServices commonServices = new CommonServices();
            SendCustomerToSignModel lstCustomerInfo = new SendCustomerToSignModel();

            lstCustomerInfo = docusignServices.GetCustomerDetails(ProjectSession.QuoteID);
            GetCustomerSignInURl(false);
            var hostUrl = commonServices.CheckHost(Request.Url.Host);
            DateTime currentDate = ConvertTo.GetEstTimeNow();
            lstCustomerInfo.URL = hostUrl + "customer-sign/view/" + CryptoGrapher.CBase64Encrypt(Convert.ToString(ProjectSession.QuoteID));
            lstCustomerInfo.PhoneNumber = ConvertTo.PhoneNumberFormatting(lstCustomerInfo.PhoneNumber);

            lstCustomerInfo.MessageTypeCode = Enums.MessageTypeCode.EMAIL.ToString();
            return PartialView("~/Views/AutoPolicy/Partial/_SendCustomerToSign.cshtml", lstCustomerInfo);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult SendCustomerPaperwork()
        {
            DocusignServices docusignServices = new DocusignServices();
            SendPaperWorkModel sendPaperwork = new SendPaperWorkModel();
            sendPaperwork = docusignServices.GetCustomerPaperworkDetails(ProjectSession.QuoteID);
            sendPaperwork.EmailSubject = ConstantVariables.EmailSubject;
            sendPaperwork.ContactNo = ConvertTo.PhoneNumberFormatting(sendPaperwork.ContactNo);
            sendPaperwork.DocumentName = "Signed Application.pdf ";
            return PartialView("~/Views/AutoPolicy/Partial/_SendCustomerPaperwork.cshtml", sendPaperwork);
        }



        /// <summary>
        /// Update Purchase Initiated When Click on Buy Now
        /// </summary>
        /// <param name="rateID">RateHistory ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateNewPurchaseInitiated(int rateID, int planID)
        {
            var rateResponseModel = new List<QuoteRateResponseModel>();
            AutoPolicyServices policyServices = new AutoPolicyServices();
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            NSDServices nsdServices = new NSDServices();
            decimal nsdPrice = 0;
            try
            {
                if (planID > 0)
                {
                    nsdPrice = nsdServices.SaveAutoQuoteNsdPlan(ProjectSession.QuoteID, planID, ProjectSession.AgencyDetails.UserId);
                }

                if (rateID > 0)
                {
                    policyServices.UpdateNewPurchaseInitiated(rateID, ProjectSession.QuoteID, nsdPrice, ProjectSession.AgencyDetails.UserId);
                }
                return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
                CommonFunctions.OnException(Enums.AuditHistoryModule.QN.ToString(), Enums.AuditHistoryOperation.U.ToString(), err);
                return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
            }
        }


        /// <summary>
        /// CallBack method Return When Completed SignIn From Office
        /// </summary>
        /// <returns></returns>
        public ActionResult CompleteSignIn(string isofficeSigned, string envelopeid)
        {
            try
            {
                if (Request.QueryString["event"] == "signing_complete")
                {

                    GeneralServices generalServices = new GeneralServices();
                    AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                    DocusignServices docusignServices = new DocusignServices();
                    AutoQuoteServices quotesServices = new AutoQuoteServices();
                    CommonServices commonServices = new CommonServices();
                    Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();

                    var lstQuoteVehicles = docusignServices.UpdateQuoteEnvelopeStatusByQuoteID(ProjectSession.QuoteID);
                    if (isofficeSigned == "0")
                    {
                        BindArrowheadPolicy(ProjectSession.QuoteID, false);
                    }
                    SendOfficeSigningAppMail(ProjectSession.QuoteID);

                    long officeSignTrackId = generalServices.GetGeneralStatusIdByType(Enums.GeneralStatus.SIGNMEDIUM.ToString(), Enums.SignMedium.SIGNINGOFFICE.ToString());

                    autoPolicyServices.UpdateSignTracking(officeSignTrackId, ProjectSession.QuoteID);

                    // Get Quote Details 
                    var quoteDetail = quotesServices.GetQuoteInfo(ProjectSession.QuoteID);
                    TemplateModel template = templateServices.GetTemplateDetails(Enums.TemplateType.SENDPAPERWORK.ToString());
                    TemplateModel templateArrowheadNotification = templateServices.GetTemplateDetails(Enums.TemplateType.SENDARROWHEADNOTIFICATION.ToString());
                    List<System.Net.Mail.Attachment> attachment = new List<System.Net.Mail.Attachment>();
                    var policyHolderDetails = quotesServices.GetQuoteInfo(ProjectSession.QuoteID);
                    try
                    {
                        System.Net.WebClient client = new System.Net.WebClient();
                        var credentials = docusignServices.GetDocusignApiCredential();
                        string envelopeID = docusignServices.GetEnvelopeIDByQuoteId(ProjectSession.QuoteID);
                        var cfi = DocusignConfiguration.DocusignConfigurations(credentials);
                        EnvelopesApi envelopesApi = new EnvelopesApi(cfi);
                        var envelope = envelopesApi.GetDocument(credentials.AccountId, envelopeID, "combined");
                        string filename = "Application_" + policyHolderDetails.QuoteModel.PolicyNumber.Substring(4, 6) + ".pdf";
                        byte[] pdfByte;
                        System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain);
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            envelope.CopyTo(memoryStream);
                            pdfByte = memoryStream.ToArray();
                            var file = File(pdfByte, System.Net.Mime.MediaTypeNames.Application.Pdf, filename);
                            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(pdfByte), filename);
                            attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                            attach.Name = filename;
                            attachment.Add(attach);

                        }
                    }


                    catch (Exception ex) { }

                    try
                    {

                        string pdfTemplate = string.Empty;


                        // if (policyHolderDetails.QuoteModel.StateAbbr == "MI" || policyHolderDetails.QuoteModel.StateAbbr == "SC")
                        pdfTemplate = Server.MapPath("~/Documents/PDF/" + policyHolderDetails.QuoteModel.StateAbbr + "/ID_Card.pdf");

                        //else
                        //    pdfTemplate = Server.MapPath("~/Documents/PDF/IDCards.pdf");

                        for (int i = 0; i < policyHolderDetails.AutoQuoteVehicles.Count; i++)
                        {
                            var _vehicle = policyHolderDetails.AutoQuoteVehicles[i];
                            PdfReader _reader = new PdfReader(pdfTemplate);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                PdfStamper stamper = new PdfStamper(_reader, ms);
                                using (stamper)
                                {
                                    Dictionary<string, string> info = _reader.Info;
                                    info["Title"] = "ID Card";
                                    stamper.MoreInfo = info;
                                    commonServices.IDCardFields(ProjectSession.QuoteID, (int)_vehicle.AutoVehicleId, stamper.AcroFields);
                                }

                                ms.Close();
                                byte[] IDCardByte;
                                IDCardByte = ms.ToArray();
                                var file = File(IDCardByte, System.Net.Mime.MediaTypeNames.Application.Pdf, "IDCards.pdf");
                                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(IDCardByte), "IDCards.pdf");
                                attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                                attach.Name = "Veh" + (i + 1).ToString() + "_IDCards_" + policyHolderDetails.QuoteModel.PolicyNumber.Substring(5, 6) + ".pdf";
                                attachment.Add(attach);

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                    var hostUrl = commonServices.CheckHost(Request.Url.Host);
                    //if (hostUrl.Contains("arrowhead-pos-qa") || hostUrl.Contains("localhost"))
                    //{
                    try
                    {
                        string billingTemplate = string.Empty;
                        billingTemplate = Server.MapPath("~/Documents/PDF/" + policyHolderDetails.QuoteModel.StateAbbr + "/Billing_Statement.pdf");
                        string filename = "Billing_Statement" + policyHolderDetails.QuoteModel.PolicyNumber.Substring(4, 6) + ".pdf";
                        PdfReader _reader = new PdfReader(billingTemplate);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            PdfStamper stamper = new PdfStamper(_reader, ms);
                            using (stamper)
                            {
                                Dictionary<string, string> info = _reader.Info;
                                commonServices.BillingField(ProjectSession.QuoteID, stamper.AcroFields);
                            }

                            ms.Close();
                            byte[] BillingPdfByte;
                            BillingPdfByte = ms.ToArray();
                            var file = File(BillingPdfByte, System.Net.Mime.MediaTypeNames.Application.Pdf, "Billing_Statement.pdf");
                            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(BillingPdfByte), "Billing_Statement.pdf");
                            attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                            attach.Name = filename;
                            attachment.Add(attach);
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                    //}
                    string EmailBody = GetThankyouEmailTemplate(ProjectSession.QuoteID, template);
                    string subject = template.EmailSubject;
                    var officeSMTPlDetail = generalServices.GetSmtpDetails();
                    string to = "minewapp@arrowheadgrp.com";
                    if (hostUrl.Contains("arrowhead-pos-qa") || hostUrl.Contains("localhost"))
                    {
                        to = "randy@rateforce.com";
                    }
                    MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, quoteDetail.QuoteModel.EmailId, "", "", subject, EmailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, attachment, null);
                    string emailBodyArrowheadNotification = GetArrowheadNotificationTemplate(ProjectSession.QuoteID, templateArrowheadNotification);
                    string subjectArrowheadNotification = templateArrowheadNotification.EmailSubject;
                    MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, to, "", "", subjectArrowheadNotification, emailBodyArrowheadNotification, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, attachment, null);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.U.ToString(), ex.ToString());
            }
            return View();
        }






        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        public void SendOfficeSigningAppMail(long quoteId)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();

                AutoQuoteServices quotesServices = new AutoQuoteServices();
                CustomerDocumentServices customerDocumentServices = new CustomerDocumentServices();
                Service.TemplateService templateServices = new Service.TemplateService();
                CommonServices commonServices = new CommonServices();

                GeneralServices generalServices = new GeneralServices();
                List<System.Net.Mail.Attachment> attachment = new List<System.Net.Mail.Attachment>();


                string document = customerDocumentServices.officeSignUrl(quoteId);
                string filename = System.IO.Path.GetFileName(document);
                MemoryStream memoryStream = new MemoryStream();
                var fileBytes = commonServices.GetTemplateByte(document);
                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(fileBytes), filename);
                attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                attach.Name = filename;
                attachment.Add(attach);
                string subject = ConstantVariables.SigningAppSubject;
                var officeSMTPlDetail = generalServices.GetSmtpDetails();
                bool isSuccess = MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, ConstantVariables.SignAppEmail, "", "", subject, "",
                    officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo,
                    officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials,
                    officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, attachment, null);
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.OS.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

            }
        }



        /// <summary>
        /// Binding Arrowhead Policy And Update Policy Number
        /// </summary>
        /// <param name="QuoteID"></param>
        public void BindArrowheadPolicy(long QuoteID, bool isSignInOffice)
        {
            Arrowhead.POS.Process.Arrowhead arrowhead = new Arrowhead.POS.Process.Arrowhead();
            RaterServices raterServices = new RaterServices();
            DocusignServices _docusignServices = new DocusignServices();
            CommonServices commonServices = new CommonServices();
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            try
            {
                bool checkPolicyNumberExist = _docusignServices.CheckPolicyNumberByQuoteId(QuoteID);
                if (checkPolicyNumberExist == false)
                {
                    var binderRequestModel = raterServices.PreparePolicyBindModel(QuoteID, isSignInOffice);

                    var hostUrl = commonServices.CheckHost(Request.Url.Host);
                    var quotePayment = autoPolicyServices.GetPaymentDetailByQuoteId(ProjectSession.QuoteID);
                    if ((hostUrl.Contains("arrowhead-pos-qa") || hostUrl.Contains("localhost")) && quotePayment.PaymentMethodCd == Enums.PaymentMethod.ECHECK.ToString() && !autoPolicyServices.IsNSDExists(binderRequestModel.QuoteID))
                    {
                        QuoteEcheck quoteEcheck = autoPolicyServices.GetEcheckDetailsByQuotesId(binderRequestModel.QuoteID);
                        if (quoteEcheck != null && quoteEcheck.AccountNumber != null && quoteEcheck.RoutingNumber != null)
                        {
                            binderRequestModel.AccountNumber = quoteEcheck.AccountNumber;
                            binderRequestModel.RoutingNumber = quoteEcheck.RoutingNumber;
                            binderRequestModel.PaymentMethodCd = "CHECK";
                        }
                    }
                    var policyModel = arrowhead.BindPolicy(binderRequestModel);

                    policyModel.QuoteID = QuoteID;
                    DocusignServices docusignServices = new DocusignServices();
                    docusignServices.UpdatePolicyNumber(policyModel, isSignInOffice);
                }
                

            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.U.ToString(), ex.ToString());
            }

        }

        /// <summary>
        /// Send Docusign Link To Cutomer By Email/Text
        /// </summary>
        /// <param name="signModel">Sign Model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendDocusignLink(SendCustomerToSignModel signModel)
        {
            GeneralServices generalServices = new GeneralServices();
            Service.TemplateService templateServices = new Service.TemplateService();
            CommonServices commonServices = new CommonServices();
            DocusignServices docusignServices = new DocusignServices();

            try
            {
                // Set Default Message Type Code 
                if (signModel.MessageTypeCode == null)
                {
                    signModel.MessageTypeCode = "EMAIL";
                }

                if (signModel.MessageTypeCode == Enums.MessageTypeCode.EMAIL.ToString())
                {

                    var messageTemplate = templateServices.TemplateFieldMapping(Enums.TemplateType.SENDSIGNLINK.ToString(), 0, signModel.quoteID);
                    string subject = messageTemplate.EmailSubject;
                    string emailBody = messageTemplate.EmailTemplate;
                    emailBody = emailBody.Replace("#CustomerSignUrl#", signModel.URL);
                    var officeSMTPlDetail = generalServices.GetSmtpDetails();
                    bool isSuccess = MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, signModel.EmailID, "", "", subject, emailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, null);
                }
                if (signModel.MessageTypeCode == Enums.MessageTypeCode.TEXT.ToString())
                {
                    string sms = docusignServices.GetSMSBody();
                    sms = sms.Replace("#signurl#", signModel.URL).Replace("#customername#", string.Empty).Replace("#quoteid#", string.Empty).Replace("#agencyname#", string.Empty);
                    string SMSBody = sms;
                    string _SMSBody = SMSBody.Replace("<br/>", Environment.NewLine);
                    var authorizeDetails = GeneralServices.GetCommunicationCredentials();
                    TwilioUtility.SendSMS(authorizeDetails.AccountSid, authorizeDetails.AuthToken, signModel.PhoneNumber, authorizeDetails.PhoneNumber, authorizeDetails.ShortCode, _SMSBody, null);

                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                throw ex;
            }
            return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendPaperWorkmodel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendPaperWorkDetails(SendPaperWorkModel sendPaperWorkmodel)
        {
            GeneralServices generalServices = new GeneralServices();
            DocusignServices docusignServices = new DocusignServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            CustomerDocumentServices customerDocumentServices = new CustomerDocumentServices();

            string fileName = "";
            string document = customerDocumentServices.officeSignUrl(sendPaperWorkmodel.QuoteId);

            //string subject = sendPaperWorkmodel.EmailSubject;
            if (sendPaperWorkmodel.SendTypeCode == "EMAIL")
            {
                System.Net.WebClient client = new System.Net.WebClient();
                List<System.Net.Mail.Attachment> attachment = new List<System.Net.Mail.Attachment>();
                var policyHolderDetails = autoQuoteServices.GetQuoteInfo(ProjectSession.QuoteID);
                var agent_Details = autoQuoteServices.GetAgencyDetailsByQuoteId(ProjectSession.QuoteID);
                try
                {

                    System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain);
                    string fileext = System.IO.Path.GetExtension(document);
                    fileName = System.IO.Path.GetFileName(document);
                    MemoryStream memoryStream = new MemoryStream();
                    var fileBytes = client.DownloadData(document);
                    var file = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
                    System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(fileBytes), fileName);
                    attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                    attach.Name = fileName;
                    attachment.Add(attach);

                }
                catch (Exception ex)
                {
                    return null;
                }
                try
                {
                    if (policyHolderDetails.AutoQuoteVehicles != null && policyHolderDetails.AutoQuoteVehicles.Count > 0)
                    {
                        string pdfTemplate = string.Empty;


                        //  if (policyHolderDetails.QuoteModel.StateAbbr == "MI" || policyHolderDetails.QuoteModel.StateAbbr == "SC")
                        pdfTemplate = Server.MapPath("~/Documents/PDF/" + policyHolderDetails.QuoteModel.StateAbbr + "/ID_Card.pdf");
                        //else
                        //    pdfTemplate = Server.MapPath("~/Documents/PDF/IDCards.pdf");

                        for (int i = 0; i < policyHolderDetails.AutoQuoteVehicles.Count; i++)
                        {
                            var _vehicle = policyHolderDetails.AutoQuoteVehicles[i];
                            PdfReader _reader = new PdfReader(pdfTemplate);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                PdfStamper stamper = new PdfStamper(_reader, ms);
                                using (stamper)
                                {
                                    Dictionary<string, string> info = _reader.Info;
                                    info["Title"] = "ID Card";
                                    stamper.MoreInfo = info;
                                    CommonServices commonServices = new CommonServices();
                                    commonServices.IDCardFields(ProjectSession.QuoteID, (int)_vehicle.AutoVehicleId, stamper.AcroFields);

                                }

                                ms.Close();
                                byte[] IDCardByte;
                                IDCardByte = ms.ToArray();
                                var file = File(IDCardByte, System.Net.Mime.MediaTypeNames.Application.Pdf, "IDCards.pdf");
                                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(new MemoryStream(IDCardByte), "IDCards.pdf");
                                attach.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                                attach.Name = "Veh_" + (i + 1).ToString() + "_IDCard.pdf";
                                attachment.Add(attach);

                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    //  CommonFunctions.OnException(Enums.AuditHistoryModule.QE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                    ex.ToString();
                }
                // Get Template Details 
                Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
                TemplateModel template = templateServices.GetTemplateDetails(Enums.TemplateType.SENDPAPERWORK.ToString());
                string EmailBody = GetThankyouEmailTemplate(sendPaperWorkmodel.QuoteId, template);
                string subject = template.EmailSubject;
                var officeSMTPlDetail = generalServices.GetSmtpDetails();
                bool isSuccess = MailingUtility.SendEmail(officeSMTPlDetail.EmailFrom, sendPaperWorkmodel.EmailId, "", "", subject, EmailBody, officeSMTPlDetail.IsBodyHtml, Convert.ToInt32(officeSMTPlDetail.Priority), "", officeSMTPlDetail.EmailReplyTo, officeSMTPlDetail.Host, officeSMTPlDetail.Username, officeSMTPlDetail.Password, officeSMTPlDetail.Port, officeSMTPlDetail.IsUseDefaultCredentials, officeSMTPlDetail.IsEnableSsl, officeSMTPlDetail.IsDefault, attachment, null);
            }

            else if (sendPaperWorkmodel.SendTypeCode == "FAX")
            {
                var authorizeDetails = GeneralServices.GetCommunicationCredentials();
                TwilioUtility.SendFax(authorizeDetails.AccountSid, authorizeDetails.AuthToken, sendPaperWorkmodel.ContactNo, authorizeDetails.PhoneNumber, document);
            }

            return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });


        }

        /// <summary>
        /// Get MVR Report Check Driver Violation if New Violation Found Then Get rate Again
        /// </summary>
        /// <returns></returns>
      //  [CustomExceptionFilter(ModuleID = "ADV", OperationType = 'I')]
        public JsonResult GetMVRReport()
        {
            AutoPolicyServices autoPolicy = new AutoPolicyServices();
            AutoDriverServices _autoDriverServices = new AutoDriverServices();
            string companyQuoteID = string.Empty;
            try
            {

                companyQuoteID = autoPolicy.GetCompanyQuoteNumber(ProjectSession.QuoteID);
                Arrowhead.POS.Process.Arrowhead arrowheadProcess = new Arrowhead.POS.Process.Arrowhead();

                // Delete MVR Violations If Exist
                _autoDriverServices.DeleteMVRViolations(ProjectSession.QuoteID);

                var mvrReport = arrowheadProcess.GetArrowheadMVRReport(companyQuoteID);
                MvrResponseModel mvrResponseModel = new MvrResponseModel();
                mvrResponseModel.QuoteID = ProjectSession.QuoteID;
                mvrResponseModel.RequestFile = "<xml><companyQuoteID>" + companyQuoteID + "</companyQuoteID></xml>";
                mvrResponseModel.ResponseFile = CommonFunctions.GetXMLFromObject(mvrReport);
                autoPolicy.UpdateMVRXMLAsync(mvrResponseModel);

                if (mvrReport.Count() > 0 || (mvrReport.Count() == 0 && autoPolicy.IsQuotePlanNotSelected(ProjectSession.QuoteID)))
                {

                    bool IsAdditionalViolation = false;
                    var driverViolation = autoPolicy.GetViolationByQuoteID(ProjectSession.QuoteID);

                    if (driverViolation != null && driverViolation.Count() > 0)
                    {
                        foreach (var item in driverViolation)
                        {
                            if (mvrReport.Any(x => x.Violation != item.ViolationName))
                            {
                                IsAdditionalViolation = true;
                                break;
                            }
                        }
                    }
                    else
                        IsAdditionalViolation = true;


                    if (IsAdditionalViolation)
                    {
                        autoPolicy.AddArrowheadMVRViolation(mvrReport, ProjectSession.QuoteID, ProjectSession.AgencyDetails.UserId);
                        AutoQuoteServices quoteServices = new AutoQuoteServices();
                        RaterServices raterServices = new RaterServices();
                        // var oldRates = quoteServices.GetRateHistoryDetailsByQuoteID(ProjectSession.QuoteID);
                        var request = raterServices.PrepareAutoRequestModel(ProjectSession.QuoteID);

                        var quoteRateResponse = GetRateResponse();
                        ViewBag.violationList = autoPolicy.GetMVRViolationByQuoteID(ProjectSession.QuoteID);
                        return Json(new { IsRedirect = false, Content = RenderRazorViewToString("~/Views/AutoPolicy/Partial/_QuoteMVRReport.cshtml", quoteRateResponse) }, JsonRequestBehavior.AllowGet);
                    }
                    else if (!IsAdditionalViolation && (mvrReport.Count() == 0 && autoPolicy.IsQuotePlanNotSelected(ProjectSession.QuoteID))) // Plan Not Selected in MVR and Not Found Additional Violation
                    {
                        RaterServices raterServices = new RaterServices();
                        var request = raterServices.PrepareAutoRequestModel(ProjectSession.QuoteID);
                        var quoteRateResponse = GetRateResponse();
                        return Json(new { IsRedirect = false, Content = RenderRazorViewToString("~/Views/AutoPolicy/Partial/_QuoteMVRReport.cshtml", quoteRateResponse) }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.ADV.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                return Json(new { IsRedirect = true, RedirectUrl = Url.Action("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID }) }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                if (!string.IsNullOrEmpty(companyQuoteID)) // if companyQuoteID Null then Not Update Policy Status
                {
                    autoPolicy.UpdatePolicyStatus(ProjectSession.QuoteID);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.ADV.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());
            }
            return Json(new { IsRedirect = true, RedirectUrl = Url.Action("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID }) }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Arrowhead Rate Response 
        /// </summary>
        /// <returns></returns>
        public List<QuoteRateResponseModel> GetRateResponse()
        {
            RaterServices raterServices = new RaterServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            var request = raterServices.PrepareAutoRequestModel(ProjectSession.QuoteID);
            var rateResponse = raterServices.GetArrowheadResponse(request, ProjectSession.QuoteID, ProjectSession.AgencyDetails.AgencyId);
            return rateResponse;
        }

        /// <summary>
        /// Return Partial View Result In String
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                try
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                      viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                                 ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                }
                catch (Exception ex)
                {
                    // CommonFunctions.OnException(Enums.AuditHistoryModule.ADV.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                }
                return sw.GetStringBuilder().ToString();
            }
        }


        /// <summary>
        /// Create URl For Docusign ANd Add Entry in Envelop And Return SignIn URL
        /// Capture Payment 
        /// </summary>
        /// <returns></returns>
        public string GetCustomerSignInURl(bool isSignInOffice)
        {
            string signInURL = string.Empty;
            CommonServices commonServices = new CommonServices();
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            AuthorizeProcess _authorizeProcess = new AuthorizeProcess();
            try
            {

                DocusignServices docusignServices = new DocusignServices();

                if (isSignInOffice == true)
                {
                    bool checkPolicyNumberExist = docusignServices.CheckPolicyNumberByQuoteId(ProjectSession.QuoteID);
                    if (checkPolicyNumberExist == false)
                    {
                        // Capture Payment CC/ACH 
                        string _Issuccess = Enums.PAYMENTSTATUS.SUCCESS.ToString();
                        string errorMessage = string.Empty;

                        var paymentDetailModel = _autoPolicyServices.GetPaymentDetailByQuoteId(ProjectSession.QuoteID);
                        string paymentGatewayCode = _autoPolicyServices.GetPaymentGatewayByQuoteId(ProjectSession.QuoteID);

                        if (paymentDetailModel != null && paymentDetailModel.PaymentMethodCd == Enums.PaymentMethod.CREDITCARD.ToString() &&
                            paymentGatewayCode == Enums.PaymentGateway.AUTHORIZENET.ToString() && paymentDetailModel.PaymentStatus == Enums.PAYMENTSTATUS.INITIATED.ToString())
                        {
                            // Prepare PaymentReq Model
                            var paymentReqModel = _autoPolicyServices.GetPaymentByQuoteID(ProjectSession.QuoteID);
                            var _custDetails = autoQuoteServices.GetPolicyHolderDetailById(ProjectSession.QuoteID);
                            PaymentProcessRequestModel requestModel = new PaymentProcessRequestModel()
                            {
                                CarrierAmount = paymentReqModel.TotalCarrier,
                                CurrencyCode = "USA",
                                CustomerName = paymentReqModel.FullName,
                                QuoteID = paymentReqModel.QuoteID,
                                TotalAmount = paymentReqModel.TotalAmount,
                                Description = "Payment toward policy",
                                Token = paymentReqModel.PgTransactionId
                            };

                            var _respProcess = _authorizeProcess.CapturePayment(requestModel);
                            PaymentResponseModel paymentResponseModel = new PaymentResponseModel();
                            paymentResponseModel.QuoteId = ProjectSession.QuoteID;
                            paymentResponseModel.ResponseFile = _respProcess.ResponseString;
                            _autoPolicyServices.UpdatePaymentXMLAsync(paymentResponseModel);

                            _Issuccess = _respProcess.Status ? Enums.PAYMENTSTATUS.SUCCESS.ToString()
                                : Enums.PAYMENTSTATUS.FAIL.ToString();
                            errorMessage = _respProcess.StatusMessage;

                            if (_respProcess.TransactionID == null || _respProcess.TransactionID.ToString() == string.Empty || _respProcess.TransactionID.ToString() == "0")
                            {
                                this.ShowMessage(MessageExtension.MessageType.Error, _respProcess.StatusMessage, true);
                            }

                            _autoPolicyServices.UpdateQuotePaymentStatus(_respProcess, ProjectSession.QuoteID, _Issuccess); // Update Payment Status 


                            //payment.PGCustomerId = _respProcess.CustomerId;
                            //payment.PgTransactionId = _respProcess.TransactionID;
                        }
                        else if (paymentDetailModel != null)
                        {
                            _Issuccess = Enums.PAYMENTSTATUS.SUCCESS.ToString();
                        }

                        // After Capture Bind Policy
                        if (_Issuccess.ToUpper() == Enums.PAYMENTSTATUS.SUCCESS.ToString())
                        {
                            BindArrowheadPolicy(ProjectSession.QuoteID, isSignInOffice);
                            //RedirectToAction("OfficeSigning", new { @quoteId = ProjectSession.QuoteID });
                        }
                        else
                        {
                            return string.Empty; // Failed Capture Payment 
                        }
                    }
                }


                var DocusignQuoteModel = docusignServices.GetDocusignQuoteRecipientByQuoteId(ProjectSession.QuoteID);

                var credentials = docusignServices.GetDocusignApiCredential();

                EnvelopeDefinition envdef = new EnvelopeDefinition()
                {
                    EmailSubject = ConstantVariables.EsignSubject,
                    Status = ConstantVariables.Envdefstatus,
                    CompositeTemplates = new List<CompositeTemplate>(),
                    Recipients = new Recipients()
                };

                long _custId = 0;

                TemplateRole customerRole = new TemplateRole();
                if (isSignInOffice)
                {
                    customerRole.Email = DocusignQuoteModel.UserEmailId;
                    customerRole.Name = DocusignQuoteModel.UserName;
                    customerRole.ClientUserId = customerRole.Email;
                    customerRole.RoleName = Enums.DocusignRole.Agent.ToString();
                }
                else
                {
                    customerRole.Email = DocusignQuoteModel.CustomerEmailId;
                    customerRole.Name = DocusignQuoteModel.CustomerName;
                    customerRole.ClientUserId = customerRole.Email;
                    customerRole.RoleName = Enums.DocusignRole.Customer.ToString();
                }

                List<TemplateRole> rolesList = new List<TemplateRole>();
                rolesList.Add(customerRole);
                // rolesList.Add(agentRole);
                envdef.TemplateRoles = rolesList;
                envdef.Status = "sent";
                docusignServices.UpdateEffectiveDate(ProjectSession.QuoteID);
                var _dataFields = docusignServices.DatasField(ProjectSession.QuoteID, ProjectSession.AgencyDetails.AgencyId);
                bool IspaymentMethod = docusignServices.PaymentMethodtype(ProjectSession.QuoteID);
                var bindingTemplate = docusignServices.GetOnlineBiddingTemplate(DocusignQuoteModel.StateID, IspaymentMethod, isSignInOffice);
                int esignCustomerId = 2;

                int counter = 1;

                foreach (OnlineBiddingTemplateModel item in bindingTemplate)
                {
                    //var compositeTemplates = new CompositeTemplate();
                    if (item.EnvelopType == Enums.EnvelopeType.SR22FORM.ToString())
                    {
                        AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                        var sr22Driver = autoPolicyServices.GetSr22InsuredDriver(ProjectSession.QuoteID);
                        if (sr22Driver == null)
                        {
                            continue;
                        }
                    }

                    if (item.EnvelopType == Enums.EnvelopeType.SR22AFORM.ToString())
                    {
                        AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
                        var sr22ADriver = autoPolicyServices.GetSr22AInsuredDriver(ProjectSession.QuoteID);
                        if (sr22ADriver == null)
                        {
                            continue;
                        }
                    }
                    if (!(string.IsNullOrWhiteSpace(item.TemplateID)))
                    {
                        Recipients recipients = new Recipients()
                        {
                            RecipientCount = "2",
                            CurrentRoutingOrder = "1"
                        };
                        envdef.Recipients = recipients;

                        recipients.Signers = new List<Signer>();
                        string esignUserName = DocusignQuoteModel.UserName;
                        string esignUserEmail = DocusignQuoteModel.UserEmailId;

                        string esignCustomerName = DocusignQuoteModel.CustomerName;
                        string esignCustomerEmail = DocusignQuoteModel.CustomerEmailId;

                        var customerSigner = new Signer()
                        {
                            Email = esignCustomerEmail,
                            Name = esignCustomerName,
                            RoleName = Enums.DocusignRole.Customer.ToString(),
                            RecipientId = esignCustomerId.ToString(),
                            // RoutingOrder = esignCustomerId.ToString(),
                            ClientUserId = esignCustomerId.ToString()
                        };
                        if (isSignInOffice)
                        {
                            customerSigner = new Signer()
                            {
                                Email = esignUserEmail,
                                Name = esignUserName,
                                RoleName = Enums.DocusignRole.Agent.ToString(),
                                RecipientId = esignCustomerId.ToString(),
                                ClientUserId = esignCustomerId.ToString()
                            };
                        }

                        var Policycompositetemplate = new CompositeTemplate();
                        Policycompositetemplate.InlineTemplates = new List<InlineTemplate>();
                        List<ServerTemplate> lstserverTemplate = new List<ServerTemplate>();
                        InlineTemplate inlineTemplate = new InlineTemplate();
                        inlineTemplate = new InlineTemplate()
                        {
                            Sequence = counter.ToString(),
                            Recipients = new Recipients(),
                            Documents = new List<Document>()
                        };

                        Policycompositetemplate.CompositeTemplateId = Convert.ToString(item.Order);
                        lstserverTemplate = new List<ServerTemplate>();
                        ServerTemplate serverTemplate = new ServerTemplate();
                        serverTemplate.TemplateId = item.TemplateID;
                        serverTemplate.Sequence = counter.ToString();
                        lstserverTemplate.Add(serverTemplate);

                        var templateDetails = DocusignConfiguration.GetTemplate(credentials, item.TemplateID);
                        List<Tabs> _tabs = new List<Tabs>();
                        if ((templateDetails != null) && (templateDetails.Recipients != null))
                        {
                            for (int f = 0; f < templateDetails.Documents.Count; f++)
                            {
                                var templateTabs = DocusignConfiguration.GetTemplateTabs(credentials, templateDetails, templateDetails.Documents[f].DocumentId);
                                for (int i = 0; i < templateDetails.Recipients.Signers.Count; i++)
                                {
                                    try
                                    {
                                        if (i == templateDetails.Recipients.Signers.Count)
                                            break;

                                        if (templateTabs != null)
                                        {
                                            if (templateTabs.TextTabs.Count > 0)
                                            {
                                                for (int j = 0; j < templateTabs.TextTabs.Count; j++)
                                                {
                                                    if (j == templateTabs.TextTabs.Count)
                                                        break;

                                                    var _nameField = _dataFields.Where(x => x.Key == templateTabs.TextTabs[j].TabLabel);
                                                    if (_nameField.Count() != 0)
                                                        templateTabs.TextTabs[j].Value = _nameField.FirstOrDefault().Value;
                                                }
                                                templateDetails.Recipients.Signers[i].Tabs = templateTabs;
                                                templateDetails.Documents[f].Tabs = templateTabs;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        commonServices.SendErrorMail(ex.ToString(), commonServices.CheckHost(Request.Url.Host));
                                    }
                                }

                                for (int k = 0; k < templateDetails.Recipients.Signers.Count; k++)
                                {
                                    if (templateDetails.Recipients.Signers[k].RoleName == customerSigner.RoleName)
                                    {
                                        customerSigner.Tabs = templateDetails.Recipients.Signers[k].Tabs;
                                    }
                                }
                            }
                        }

                        recipients.Signers.Add(customerSigner);
                        inlineTemplate.Documents = templateDetails.Documents;
                        inlineTemplate.Recipients = recipients;
                        Policycompositetemplate.ServerTemplates = lstserverTemplate;
                        Policycompositetemplate.InlineTemplates.Add(inlineTemplate);
                        envdef.CompositeTemplates.Add(Policycompositetemplate);
                    }
                    counter = counter + 1;
                }
                string filename = "";
                string fileext = "";
                string base64String = string.Empty;

                var _nsd = docusignServices.GetNsdByQuote(ProjectSession.QuoteID);

                //NSD Document
                if (_nsd != null)
                {
                    using (WebClient client = new WebClient())
                    {
                        var bytes = client.DownloadData(Core.ConstantVariables.ArrowheadBlobURL + _nsd.PdfUrl);
                        base64String = Convert.ToBase64String(bytes);
                        filename = System.IO.Path.GetFileName(_nsd.PdfUrl);
                        fileext = System.IO.Path.GetExtension(_nsd.PdfUrl);
                        CompositeTemplate compositeTemplate = new CompositeTemplate();
                        compositeTemplate.InlineTemplates = new List<InlineTemplate>();
                        InlineTemplate inlineTemplate = new InlineTemplate
                        {
                            Sequence = ConstantVariables.DocusignSequence.ToString(),
                            Recipients = new Recipients(),
                            Documents = new List<Document>()
                        };
                        Random r = new Random();
                        int _docId = r.Next(100, 9999999);

                        Document docs = new Document
                        {
                            DocumentBase64 = base64String,
                            DocumentId = _docId.ToString(),
                            FileExtension = fileext,
                            Name = filename,
                            Order = ConstantVariables.DocusignSequence.ToString()
                        };
                        envdef.Recipients = new Recipients();



                        var pdfCustomerSigner = new Signer()
                        {
                            Email = DocusignQuoteModel.CustomerEmailId,
                            Name = DocusignQuoteModel.CustomerName,
                            RoleName = Enums.DocusignRole.Customer.ToString(),
                            RecipientId = esignCustomerId.ToString(),
                            ClientUserId = esignCustomerId.ToString()
                        };
                        if (isSignInOffice)
                        {
                            pdfCustomerSigner = new Signer()
                            {
                                Email = DocusignQuoteModel.UserEmailId,
                                Name = DocusignQuoteModel.UserName,
                                RoleName = Enums.DocusignRole.Agent.ToString(),
                                RecipientId = esignCustomerId.ToString(),
                                ClientUserId = esignCustomerId.ToString()
                            };
                        }
                        else
                        {
                            pdfCustomerSigner.Tabs = new Tabs();
                            pdfCustomerSigner.Tabs.SignHereTabs = new List<SignHere>();
                            SignHere signhere = new SignHere();
                            signhere.DocumentId = _docId.ToString();
                            signhere.PageNumber = "3";
                            signhere.RecipientId = esignCustomerId.ToString();
                            signhere.XPosition = "90";
                            signhere.YPosition = "328";
                            pdfCustomerSigner.Tabs.SignHereTabs.Add(signhere);
                        }


                        envdef.Recipients.Signers = new List<Signer>();
                        envdef.Recipients.Signers.Add(pdfCustomerSigner);

                        inlineTemplate.Documents.Add(docs);
                        inlineTemplate.Recipients = envdef.Recipients;

                        compositeTemplate.InlineTemplates.Add(inlineTemplate);

                        envdef.CompositeTemplates.Add(compositeTemplate);
                    }
                }

                DateTime currentTime = ConvertTo.GetEstTimeNow();
                envdef.ExpireDateTime = (new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 23, 59, 59)).ToString("MM/dd/yyyy HH:mm:ss");

                var cfi = DocusignConfiguration.DocusignConfigurations(credentials);
                EnvelopesApi envelopesApi = new EnvelopesApi(cfi);
                envelopesApi.Configuration.Timeout = 500000;
                EnvelopeSummary envelopeSummary = envelopesApi.CreateEnvelope(credentials.AccountId, envdef);

                AutoEnvelopesModel autoEnvelopesModel = new AutoEnvelopesModel()
                {
                    EnvelopeId = envelopeSummary.EnvelopeId,
                    CustomerId = _custId,
                    UserId = ProjectSession.AgencyDetails.UserId,
                    QuoteId = ProjectSession.QuoteID,
                    EnvelopeProviderModeId = credentials.EnvelopeProviderModeId
                };

                var autoenvelope = docusignServices.InsertQuoteEnvelope(autoEnvelopesModel, ProjectSession.AgencyDetails.UserId, ProjectSession.QuoteID);

                RecipientViewRequest recipientViewRequest = new RecipientViewRequest();
                var hostUrl = commonServices.CheckHost(Request.Url.Host);
                string returnUrl = hostUrl + this.Url.Action("CompleteSignIn", "AutoPolicy") + "?isofficeSigned=" + (isSignInOffice ? "1" : "0") + "&envelopeid=" + autoEnvelopesModel.EnvelopeId;
                recipientViewRequest.ReturnUrl = returnUrl;
                recipientViewRequest.Email = DocusignQuoteModel.CustomerEmailId;
                recipientViewRequest.UserName = DocusignQuoteModel.CustomerName;
                if (isSignInOffice)
                {
                    recipientViewRequest.Email = DocusignQuoteModel.UserEmailId;
                    recipientViewRequest.UserName = DocusignQuoteModel.UserName;
                }
                recipientViewRequest.RecipientId = esignCustomerId.ToString();
                recipientViewRequest.AuthenticationMethod = ConstantVariables.AuthenticationMethod;

                recipientViewRequest.ClientUserId = esignCustomerId.ToString();

                ViewUrl recipientView = envelopesApi.CreateRecipientView(credentials.AccountId, autoEnvelopesModel.EnvelopeId, recipientViewRequest);
                signInURL = recipientView.Url;
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QE.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                commonServices.SendErrorMail(ex.ToString(), commonServices.CheckHost(Request.Url.Host));
                this.ShowMessage(MessageExtension.MessageType.Error, ex.Message);
            }
            return signInURL;
        }

        /// <summary>
        /// Model For Vehicle List For Get ID Cards
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult GetVehicleListForIDCard()
        {
            AutoVehicleServices autoVehicleServices = new AutoVehicleServices();
            var lstQuoteVehicles = autoVehicleServices.GetAutoVehicleByQuoteID(ProjectSession.QuoteID);

            return PartialView("~/Views/AutoPolicy/Partial/_VehicleListForIDCards.cshtml", lstQuoteVehicles);
        }

        /// <summary>
        /// Return ID Card For Vehicle
        /// </summary>
        /// <param name="id">Vehicle ID</param>
        /// <returns></returns>
        [Route("policy/idcard/{id}")]
        public ActionResult CreateIDCard(int id)
        {
            CommonServices commonServices = new CommonServices();
            try
            {
                string pdfTemplate = string.Empty;
                long quoteId = commonServices.VehicleByQuoteId(id);
                string stateAbr = commonServices.state(quoteId);
                pdfTemplate = Server.MapPath("~/Documents/PDF/" + stateAbr + "/ID_Card.pdf");
                PdfReader _reader = new PdfReader(pdfTemplate);
                using (MemoryStream ms = new MemoryStream())
                {

                    PdfStamper stamper = new PdfStamper(_reader, ms);
                    using (stamper)
                    {

                        Dictionary<string, string> info = _reader.Info;
                        info["Title"] = "ID Card";
                        stamper.MoreInfo = info;
                        commonServices.IDCardFields(quoteId, id, stamper.AcroFields);

                    }
                    Response.Expires = 0;
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "inline; filename=" + "IDCards.pdf");
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    Response.End();
                    _reader.AcroFields.GenerateAppearances = true;
                    _reader.Close();
                    return File(ms, "application/pdf", "IDCards.pdf");
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AV.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());

                ex.ToString();
            }
            return View();
        }


        




        //  [Route("policy/signed-application/{qid}")]
        //[HttpPost]
        public ActionResult SignedApplication(long qid)
        {
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            CustomerDocumentServices customerDocumentServices = new CustomerDocumentServices();
            CommonServices commonServices = new CommonServices();
            var signingApp = autoPolicyServices.GetpolicyStatusDetails(qid);
            string document = customerDocumentServices.officeSignUrl(qid);
            var fileBytes = commonServices.GetTemplateByte(document); 
            string fileName = signingApp.PolicyNumber + "_" + signingApp.FirstName + "_" + signingApp.LastName + ".pdf";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
        }



      
        /// <summary>
        /// Update Void Status in QuotePaymentReceipt
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        [Route("policy/void-quote/{quoteId?}")]
        public ActionResult VoidQuote(long quoteId)
        {
            string PgTransactionId = string.Empty;
            bool _isNSDPlanSelected = false;
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            NSDServices nsdServices = new NSDServices();
            PolicyXmlServices policyXmlServices = new PolicyXmlServices();
            var quoteRateList = new List<QuoteRateResponseModel>();

            autoPolicyServices.PolicyNumberUpdate(quoteId);
            _isNSDPlanSelected = autoPolicyServices.IsNSDCompleted(quoteId);
            quoteRateList = policyXmlServices.GetExistsQuoteRatesList(quoteId);
            var paymentDetailModel = autoPolicyServices.GetPaymentDetailByQuoteId(quoteId);
            var paymentGatewayCode = autoPolicyServices.GetPaymentGatewayByQuoteId(quoteId);
            if (paymentDetailModel != null && paymentGatewayCode != string.Empty && (paymentDetailModel.PaymentMethodCd == Enums.PaymentMethod.CREDITCARD.ToString() ||
                paymentDetailModel.PaymentMethodCd == Enums.PaymentMethod.ECHECK.ToString()))
            {
                try
                {

                    if (paymentDetailModel != null && String.IsNullOrEmpty(paymentDetailModel.PGTransactionId))
                    {
                        PgTransactionId = autoPolicyServices.GetPaymentTransactionID(paymentDetailModel.QuoteID, paymentDetailModel.ReceiptId);
                        paymentDetailModel.PGTransactionId = PgTransactionId;
                    }

                    if (paymentGatewayCode == Enums.PaymentGateway.STRIPE.ToString())
                    {
                        IPaymentGateway paymentServices = new StripeProcess();
                        Model.CCPaymentRequestModel.PaymentProcessResponseModel refund = paymentServices.RefundProcess(paymentDetailModel.PGTransactionId, paymentDetailModel.PaymentMethodCd);
                        //if (refund.Status.ToLower() == "succeeded")
                        {
                            autoPolicyServices.UpdateVoidStatusByReceiptId(paymentDetailModel.ReceiptId);
                            autoPolicyServices.UpdateAutoRateHistoryPGFees(quoteId);
                            autoPolicyServices.UpdatePolicyStatusAfterVoid(quoteId);
                            if (_isNSDPlanSelected == true)
                            {
                                nsdServices.NSDCancelContractProcess(quoteId); // Cancel NSD Contract If Exist
                            }
                            else
                            {
                                // nsdServices.DeleteNsd(quoteId);
                            }
                            this.ShowMessage(MessageExtension.MessageType.Success, "Void Successfully", true);
                        }
                    }
                    else if (paymentGatewayCode == Enums.PaymentGateway.AUTHORIZENET.ToString())
                    {
                        IPaymentGateway _paymentServices = new AuthorizeProcess();
                        var _reqModel = autoPolicyServices.GetPaymentByQuoteID(quoteId);
                        if (paymentDetailModel.PaymentMethodCd == Enums.PaymentMethod.CREDITCARD.ToString()) // CC Payment
                        {
                            long receiptId = _reqModel.ReceiptID ?? 0;

                            PaymentProcessRequestModel _paymentRefundReqModel = new PaymentProcessRequestModel()
                            {
                                QuoteID = _reqModel.QuoteID,
                                Token = _reqModel.PgTransactionId
                            };

                            var _respRefundModel = _paymentServices.VoidPayment(_paymentRefundReqModel);
                            if (_respRefundModel.StatusMessage.ToUpper() == Enums.PAYMENTSTATUS.SUCCESS.ToString())
                            {
                                if (receiptId > 0)
                                {
                                    autoPolicyServices.UpdateVoidStatusByReceiptId(receiptId);
                                    autoPolicyServices.UpdateAutoRateHistoryPGFees(quoteId);
                                    autoPolicyServices.UpdatePolicyStatusAfterVoid(quoteId);
                                    if (_isNSDPlanSelected == true)
                                    {
                                        nsdServices.NSDCancelContractProcess(quoteId); // Cancel NSD Contract If Exist
                                    }
                                    else
                                    {
                                        // nsdServices.DeleteNsd(quoteId);
                                    }
                                }
                                this.ShowMessage(MessageExtension.MessageType.Success, "Void Successfully", true);
                            }
                            else
                            {
                                this.ShowMessage(MessageExtension.MessageType.Error, _respRefundModel.StatusMessage);
                            }
                        }
                        else if (paymentDetailModel.PaymentMethodCd == Enums.PaymentMethod.ECHECK.ToString()) // ECheck Payment
                        {
                            PaymentProcessRequestModel _paymentRefundReqModel = new PaymentProcessRequestModel()
                            {
                                QuoteID = _reqModel.QuoteID,
                                Token = _reqModel.PgTransactionId
                            };

                            var _respRefundModel = _paymentServices.VoidPayment(_paymentRefundReqModel);
                            if (_respRefundModel.StatusMessage.ToUpper() == Enums.PAYMENTSTATUS.SUCCESS.ToString())
                            {
                                long receiptId = _reqModel.ReceiptID ?? 0;
                                if (receiptId > 0)
                                {
                                    autoPolicyServices.UpdateVoidStatusByReceiptId(receiptId);
                                    autoPolicyServices.UpdateAutoRateHistoryPGFees(quoteId);
                                    autoPolicyServices.UpdatePolicyStatusAfterVoid(quoteId);
                                    if (_isNSDPlanSelected == true)
                                    {
                                        nsdServices.NSDCancelContractProcess(quoteId); // Cancel NSD Contract If Exist
                                    }
                                    else
                                    {
                                        //nsdServices.DeleteNsd(quoteId);
                                    }
                                }
                            }
                            else
                            {
                                this.ShowMessage(MessageExtension.MessageType.Error, _respRefundModel.StatusMessage);
                            }
                        }
                    }
                }
                catch (StripeException ex)
                {
                    CommonFunctions.OnException(Enums.AuditHistoryModule.QP.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());

                    this.ShowMessage(MessageExtension.MessageType.Error, ex.Message.ToString(), true);
                }
            }
            else // using SWEEP / ECHECK 
            {
                if (paymentDetailModel != null)
                {
                    autoPolicyServices.UpdateVoidStatusByReceiptId(paymentDetailModel.ReceiptId);
                    autoPolicyServices.UpdatePolicyStatusAfterVoid(quoteId);
                    if (_isNSDPlanSelected == true)
                    {
                        nsdServices.NSDCancelContractProcess(quoteId); // Cancel NSD Contract If Exist
                    }
                    else
                    {
                        //nsdServices.DeleteNsd(quoteId);
                    }
                    this.ShowMessage(MessageExtension.MessageType.Success, "Void Successfully", true);
                }
            }
            return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
        }

        /// <summary>
        /// Import NSD If fails 
        /// </summary>
        /// <param name="quoteID"></param>
        /// <returns></returns>
        [Route("policy/ImportNSD/{quoteId?}")]
        public ActionResult ImportNSD(long quoteID)
        {
            NSDServices nsdServices = new NSDServices();
            NSDContractModel _nsdContractModel = new NSDContractModel();
            try
            {
                _nsdContractModel = nsdServices.NSDProcess(quoteID, ProjectSession.AgencyDetails.UserId);
                if (!String.IsNullOrEmpty(_nsdContractModel.StatusMessage))
                {
                    this.ShowMessage(MessageExtension.MessageType.Error, _nsdContractModel.StatusMessage, true);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QN.ToString(), Enums.AuditHistoryOperation.U.ToString(), ex.ToString());

                throw ex;
            }
            return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
        }


        /// <summary>
        /// Get Email Template Thank you
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string GetThankyouEmailTemplate(long quoteId, TemplateModel templateModel)
        {
            TemplateModel _template = templateModel;
            Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            string template = string.Empty;
            try
            {
                var newPolicyModel = autoPolicyServices.GetNewPolicyDetails(quoteId);
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, newPolicyModel);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.T.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                throw ex;
            }
            return string.Empty;
        }



        /// <summary>
        /// Get Email Template Arrowhead Notification
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public string GetArrowheadNotificationTemplate(long quoteId, TemplateModel templateModel)
        {
            TemplateModel _template = templateModel;
            Arrowhead.POS.Service.TemplateService templateServices = new Arrowhead.POS.Service.TemplateService();
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            string template = string.Empty;
            try
            {
                var newPolicyModel = autoPolicyServices.GetNewPolicyDetails(quoteId);
                string razorTemplate = _template.EmailTemplate;
                string razorTemplateString = Engine.Razor.RunCompile(razorTemplate, DateTime.Now.TimeOfDay.ToString(), null, newPolicyModel);
                if (!String.IsNullOrEmpty(razorTemplateString))
                {
                    return razorTemplateString;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.T.ToString(), Enums.AuditHistoryOperation.V.ToString(), ex.ToString());
                throw ex;
            }
            return string.Empty;
        }




        /// <summary>
        /// Check Quote Payment by QuoteID and Reset Quote Payment 
        /// </summary>
        /// <param name="id"></param>
        public void ResetQuotePayment(long qid)
        {
            AutoPolicyServices autoPolicyServices = new AutoPolicyServices();
            try
            {
                autoPolicyServices.ResetQuotePaymentByQuoteId(qid);
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.QP.ToString(), Enums.AuditHistoryOperation.U.ToString(), ex.ToString());

                throw;
            }
        }

        
        /// <summary>
        /// Reset Quote To Coverage Step
        /// </summary>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ResetQuoteToCoverageStep(long QuoteId)
        {
            CommonFunctions.UpdateQuoteToCoverageStep(QuoteId, Enums.QuoteOrderStatus.COVERAGE.ToString());
            return Json(new { IsQuoteReset = true }, JsonRequestBehavior.AllowGet);
        }

    }
}