﻿using Arrowhead.POS.Service;
using Arrowhead.POS.Core;
using Arrowhead.POS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using Arrowhead.POS.Model;
using Arrowhead.POS.Process.ArrowHeadRTRProd;

namespace Arrowhead.POS.Controllers
{
    public class RemoteValidationController : Controller
    {
        // GET: RemoteValidation
        [HttpPost]
        public ActionResult IsStateLicenseNo(string licenseNo, int StateId)
        {
            GeneralServices generalServices = new GeneralServices();
            CommonServices commonServices = new CommonServices();
            AutoDriverLicenseModel driverLicenseModel = new AutoDriverLicenseModel();
            string stateCode = GeneralServices.GetStateCode(StateId);
            string regex = string.Empty;
            bool isValidLicense = true;
            driverLicenseModel = commonServices.IsValidLicenseNo(stateCode, licenseNo);
            if (driverLicenseModel.IsSuccess == true)
            {
                isValidLicense = true;
            }
            else
            {
                isValidLicense = false;
            }



            return Json(isValidLicense, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult IsVinNumber(string vin, long AutoVehicleId)
        {
            AutoVehicleServices autoVehicleServices = new AutoVehicleServices();
            return Json(autoVehicleServices.IsVinNumberExists(vin, ProjectSession.QuoteID, AutoVehicleId), JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult IsStateRateAvailable(string zipcode)
        {
            CommonServices commonServices = new CommonServices();
            bool iszipcodecheck = true;

            var validZipCode = commonServices.IsZipCodeCheck(zipcode);

            if (validZipCode != null)
            {

                if (validZipCode.state == "MI" || validZipCode.state == "SC" || validZipCode.state == "GA" || validZipCode.state == "AL")
                    iszipcodecheck = true;
                else
                    iszipcodecheck = false;
            }

            return Json(iszipcodecheck, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsStateRateAvailableByZipCode(string zipcode, int agencyId)
        {
            CommonServices commonServices = new CommonServices();
            RTRServiceClient client = new RTRServiceClient();
            bool iszipcodecheck = false;
            try
            {
                var producerCode = commonServices.getProducerCodeByAgencyId(agencyId);
                var _agencyProgramResp = client.GetAgencyPrograms(producerCode, zipcode, ConvertTo.GetEstTimeNow()); // validate zip code is working in zip code
                if (_agencyProgramResp.Length > 0)
                {
                    var validZipCode = commonServices.IsZipCodeCheck(zipcode);
                    if (validZipCode != null)
                    {
                        if (validZipCode.state == "MI" || validZipCode.state == "SC" || validZipCode.state == "AL" || validZipCode.state == "GA")
                            iszipcodecheck = true;
                        else
                            iszipcodecheck = false;
                    }

                    // validate zip code and return JSON
                    if (iszipcodecheck)
                    {
                        TempData["ValidStateCode"] = validZipCode.state;
                        var stateID = commonServices.GetStateIdByCode(validZipCode.state);
                        return Json(new { IsSuccess = iszipcodecheck, StateId = stateID, IsValid = true, validZipCode = zipcode }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { IsSuccess = iszipcodecheck, StateId = 0, IsValid = (validZipCode != null ? true : false), validZipCode = string.Empty }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { IsSuccess = iszipcodecheck, StateId = 0, IsValid = true, validZipCode = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, StateId = 0, IsValid = false, validZipCode = string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult IsSpamContactNo(string contactNo)
        {
            GeneralServices _generalServices = new GeneralServices();
            bool isValidContactNo = true;
            try
            {
                if (_generalServices.IsSpamContactNo(contactNo.ToPhoneNumber()))
                {
                    isValidContactNo = false;
                }

                return Json(isValidContactNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult ValidateDate(DateTime EffectiveDate)
        {
            if (Convert.ToDateTime(EffectiveDate).Date >= ConvertTo.GetEstTimeNow().Date && Convert.ToDateTime(EffectiveDate).Date <= ConvertTo.GetEstTimeNow().Date.AddDays(30))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false);
        }

        [HttpPost]
        public ActionResult PriorExpirationDate(DateTime? PriorExpirationDate)
        {
            if (Convert.ToDateTime(PriorExpirationDate).Date >= ConvertTo.GetEstTimeNow().Date)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false);
        }


        [HttpPost]
        public JsonResult AddressValidation(AddressValidationModel model)
        {
            AddressValidation addressValidation = new AddressValidation();
            AuditHistoryService auditHistoryService = new AuditHistoryService();

            var response = addressValidation.CheckAddress(model);
            return Json(response);
        }


        public PartialViewResult ValidateAddress(AddressResponseModel model)
        {
            return PartialView("~/Views/AutoQuote/Partial/_ManageAddressVerification.cshtml", model);
        }
       

       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="licenseNo"></param>
        /// <returns></returns>
        public bool ChecklicenseNo(string regex, string licenseNo)
        {
            bool isCheckLicense = false;
            Regex re = new Regex(regex);
            if (regex != null && regex != " ")
            {
                if (re.IsMatch(licenseNo))
                    isCheckLicense = true;
                else
                    isCheckLicense = false;
            }
            return isCheckLicense;
        }




    }
}