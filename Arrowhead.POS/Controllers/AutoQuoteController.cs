﻿using Arrowhead.POS.Infrastructure;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using Arrowhead.POS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Arrowhead.POS.Process.ArrowHeadRTRProd;
using Newtonsoft.Json;
using log4net;

namespace Arrowhead.POS.Controllers
{
    [RoutePrefix("auto")]
    public class AutoQuoteController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AutoQuoteController));
        public ActionResult Index()
        {
            return RedirectToAction("Index", "AutoQuote");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("applicant/{id?}")]
        public ActionResult Index(long id = 0)
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            CommonServices commonServices = new CommonServices();
            // Update Quote CurrentStep  Details
            //commonFunctions.UpdateCurrentQuoteStep(ProjectSession.QuoteID, Enums.QuoteOrderStatus.APPLICANT.ToString());
            ApplicantModel quoteModel = new ApplicantModel();
            if (id > 0)
            {
                ProjectSession.QuoteID = Convert.ToInt64(id);
                quoteModel = quotesServices.GetPolicyHolderDetailById(Convert.ToInt64(id));
            }
            else
            {
                ProjectSession.QuoteID = 0;
                quoteModel.EffectiveDate = ConvertTo.GetEstTimeNow();
                quoteModel.IsDuplicate = true;
            }

            if (ProjectSession.QuoteID > 0)
            {
                CommonFunctions.AddNewTab(ProjectSession.QuoteID);
            }

            var host = commonServices.CheckHost(Request.Url.Host);
            // only for developer check Agency and Set Agency Details into Session
            if (ProjectSession.AgencyDetails == null && Request.Url.Host.Contains("pos-qa"))
            {
                var lstAgency = commonServices.GetAllAgency();
                var agencyID = lstAgency.Where(x => x.Code == ConstantVariables.RaterServiceProviderCode.ToString()).Select(x => x.ID).FirstOrDefault();
                CommonFunctions.AgencyInfo(agencyID);
            }

            return View(quoteModel);
        }




        /// <summary>
        /// Edit Applicant Details
        /// </summary>
        /// <param name="id">QuoteID</param>
        /// <returns></returns>
        [Route("applicant/edit/{id}")]
        public ActionResult Edit(long? id)
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            ApplicantModel quoteModel = new ApplicantModel();
            if (id > 0)
            {
                quoteModel = quotesServices.GetPolicyHolderDetailById(Convert.ToInt64(id));
            }
            else
            {
                quoteModel.EffectiveDate = ConvertTo.GetEstTimeNow();
                quoteModel.IsDuplicate = true;
            }

            return View(quoteModel);
        }

        /// <summary>
        /// Add/Edit Quote Applicant Details
        /// </summary>
        /// <param name="quoteModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("applicant/{id?}")]
        public ActionResult SaveApplicant(ApplicantModel quoteModel)
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            AuditHistoryService auditHistoryService = new AuditHistoryService();
            CommonServices commonServices = new CommonServices();
            BaseService baseService = new BaseService();
            AutoVehicleServices vehicleServices = new AutoVehicleServices();
            RTRServiceClient client = new RTRServiceClient();
            try
            {
                if (ProjectSession.AgencyDetails != null)
                {
                    if (ProjectSession.AgencyDetails.ProducerCode > 0)
                    {
                        try
                        {
                            var agencyProgram = client.GetAgencyPrograms(ProjectSession.AgencyDetails.ProducerCode, quoteModel.ZipCode, ConvertTo.GetEstTimeNow());
                            if (agencyProgram.Length == 0)
                            {

                                this.ShowMessage(MessageExtension.MessageType.Error, "Sorry you are not authorized for this zip code.", true);
                                if (quoteModel.QuoteId > 0)
                                {
                                    return RedirectToAction("Index", "AutoQuote", new { @id = ProjectSession.QuoteID });
                                }
                                else
                                {
                                    return View("Index", quoteModel);
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }

                }

                quoteModel.InsuranceTypeId = commonServices.GetAutoInsuranceID();
                quoteModel.OfficeId = ProjectSession.AgencyDetails.OfficeId;
                quoteModel.UserId = ProjectSession.AgencyDetails.UserId;
                quoteModel.AgencyId = ProjectSession.AgencyDetails.AgencyId;
                if (quoteModel.QuoteId > 0)
                {
                  
                    quotesServices.UpdateQuoteDetails(quoteModel);
                    CommonFunctions.UpdateQuoteToCoverageStep(quoteModel.QuoteId, Enums.QUOTEORDERSTATUS.COVERAGE.ToString());
                }
                else
                {
                    quoteModel = quotesServices.InsertQuote(quoteModel);
                }
                ProjectSession.QuoteID = quoteModel.QuoteId;
                int vehicleCount = vehicleServices.GetvehicleCount(ProjectSession.QuoteID);
                return RedirectToAction("Index", "AutoVehicle", new { @id = ProjectSession.QuoteID });
            }
            catch (Exception ex)
            {
                CommonFunctions.OnException(Enums.AuditHistoryModule.AQ.ToString(), Enums.AuditHistoryOperation.S.ToString(), ex.ToString());
                this.ShowMessage(MessageExtension.MessageType.Error, ex.Message, true);
                return RedirectToAction("Index", "AutoQuote");
            }
        }

        

        /// <summary>
        /// Remove Tab If Clicked On Closed
        /// </summary>
        /// <param name="id">QuoteID</param>
        /// <returns></returns>
        public ActionResult RemoveTab(long? id)
        {
            if (ProjectSession.TabDetail.Any(x => x.QuoteID == id))
            {
                List<TabModel> tabModelList = new List<TabModel>();
                tabModelList = ProjectSession.TabDetail.Where(x => x.QuoteID != id).ToList();
                ProjectSession.QuoteID = 0;
                ProjectSession.TabDetail = tabModelList;
            }
            return RedirectToAction("Index", "AutoQuote", new { @id = 0 });
        }

        /// <summary>
        /// Get Quote Details For Display Box In Header
        /// </summary>
        /// <param name="id">QuoteID</param>
        /// <returns></returns>
        public ActionResult GetQuoteDetails(long? id)
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            var quoteDetails = quotesServices.GetQuoteInfo(Convert.ToInt64(id));
            return PartialView("~/Views/Partial/_QuoteContent.cshtml", quoteDetails);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        /// 
        [Route("check/session")]
        public JsonResult SessionTimeChecker(long quoteId)
        {
            if (quoteId != ProjectSession.QuoteID)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Current Step Details And Used In Layout
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCurrentStepDetails()
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            var currentStepDetails = quotesServices.GetCurrentStepDetails(ProjectSession.QuoteID);
            if (currentStepDetails == null)
                currentStepDetails = new CurrentStepModel();
            return PartialView("~/Views/Partial/_Sidebar.cshtml", currentStepDetails);
        }


        /// <summary>
        /// Redirect To Completed Step
        /// </summary>
        /// <param name="id">quoteID</param>
        /// <returns></returns>
        [Route("redirect/{id}")]
        public ActionResult Redirect(long id = 0)
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            GeneralServices _generalServices = new GeneralServices();

            if (id > 0)
                ProjectSession.QuoteID = id;

            long currentStepID = 1;

            var currentStepDetails = quotesServices.GetCurrentStepDetails(ProjectSession.QuoteID);

            // Identify Quick Quote and Rates & Redirect
            if (quotesServices.IsQuickQuoteByQuoteId(id)) // Quick Quote and Rates Exist for Quick Quote
            {
                return RedirectToAction("Index", "AutoQuote", new { @id = ProjectSession.QuoteID }); // redirect to applicant screen 
            }

            if (currentStepDetails != null)
                currentStepID = currentStepDetails.CurrentStepOrderId;

            if (currentStepID == 1)
                return RedirectToAction("Index", "AutoQuote", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 2)
                return RedirectToAction("Index", "AutoVehicle", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 3)
                return RedirectToAction("Index", "AutoDriver", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 4)
                return RedirectToAction("Index", "AutoCoverage", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 5)
                return RedirectToAction("Index", "AutoQuestionary", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 6)
                return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 7)
                return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
            else if (currentStepID == 8)
                return RedirectToAction("Index", "AutoPolicy", new { @id = ProjectSession.QuoteID });
            else
                return RedirectToAction("Index", "AutoQuote", new { @id = ProjectSession.QuoteID });
        }
    }
}