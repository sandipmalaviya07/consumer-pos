﻿using Arrowhead.POS.Service;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Arrowhead.POS.Infrastructure
{
    public class CustomExceptionFilter : FilterAttribute,
IExceptionFilter
    {
        public string ModuleID { get; set; }
        public char OperationType { get; set; }
        public string AfterErrorRedirectOn { get; set; }

        public Task ExecuteExceptionFilterAsync(System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
        public void OnException(ExceptionContext filterContext)
        {
            AuditHistoryService auditHistoryService = new AuditHistoryService();
            if (!(filterContext.Exception is NullReferenceException))
            {
                filterContext.ExceptionHandled = true;
                //filterContext.Result = new RedirectResult(AfterErrorRedirectOn);
                Model.AuditHistoryModel _historyModel = new Model.AuditHistoryModel()
                {
                    AgencyID = ProjectSession.AgencyDetails != null ? (int)ProjectSession.AgencyDetails.AgencyId : 0,
                    IsError = true,
                    ModuleID = ModuleID,
                    Operation = OperationType,
                    Remarks = "Got error in Module : " + ModuleID + " is " + filterContext.Exception.Message.ToString(),
                    QuoteID = ProjectSession.QuoteID,

                };
                auditHistoryService.InsertAuditHistory(_historyModel);
            }

        }
    }
}