﻿using Arrowhead.POS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Arrowhead.POS.Infrastructure
{
    public class ProjectSession
    {
        public static long QuoteID
        {
            get
            {
                if (HttpContext.Current.Session["QuoteID"] == null)
                    return 0;
                return (long)HttpContext.Current.Session["QuoteID"];
            }
            set
            {
                HttpContext.Current.Session["QuoteID"] = value;
            }
        }

        public static int QuoteStateID
        {
            get
            {
                if (HttpContext.Current.Session["QuoteStateID"] == null)
                    return 0;
                return (int)HttpContext.Current.Session["QuoteStateID"];
            }
            set
            {
                HttpContext.Current.Session["QuoteStateID"] = value;
            }
        }

        public static bool IsLeftPaneOpen
        {
            get
            {
                if (HttpContext.Current.Session["IsLeftPaneOpen"] == null)
                    return false;
                return (bool)HttpContext.Current.Session["IsLeftPaneOpen"];
            }
            set
            {
                HttpContext.Current.Session["IsLeftPaneOpen"] = value;
            }
        }

        public static int IsBridgeError
        {
            get
            {
                if (HttpContext.Current.Session["IsBridgeError"] == null)
                    return 0;
                return (int)HttpContext.Current.Session["IsBridgeError"];
            }
            set
            {
                HttpContext.Current.Session["IsBridgeError"] = value;
            }
        }
        public static List<TabModel> TabDetail
        {
            get
            {
                if (HttpContext.Current.Session["TabDetail"] == null)
                    return new List<TabModel>();
                return (List<TabModel>)HttpContext.Current.Session["TabDetail"];
            }
            set
            {
                HttpContext.Current.Session["TabDetail"] = value;
            }
        }

        public static AgencyModel AgencyDetails
        {
            get
            {
                if (HttpContext.Current.Session["AgencyDetails"] == null)
                    return new AgencyModel();
                return (AgencyModel)HttpContext.Current.Session["AgencyDetails"];
            }
            set
            {
                HttpContext.Current.Session["AgencyDetails"] = value;
            }
        }


    }


    
}