﻿using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Arrowhead.POS.Infrastructure
{
    public class DocusignConfiguration
    {
        public static EnvelopeTemplate GetTemplate(DocusignAuthorizationModel credentials, string templateid)
        {
            ApiClient apiclient = new ApiClient(credentials.Url);
            Configuration cfi = new Configuration(apiclient);
            string authHeader = "{\"Username\":\"" + credentials.UserName + "\", \"Password\":\"" + credentials.Password + "\", \"IntegratorKey\":\"" + credentials.IntegratorKey + "\"}";
            cfi.AddDefaultHeader("X-DocuSign-Authentication", authHeader);
            TemplatesApi templatesapi = new TemplatesApi(cfi);

            var gettemplateDetails = templatesapi.Get(credentials.AccountId, templateid);
            try
            {
                var details = templatesapi.GetDocumentTabs(credentials.AccountId, templateid, gettemplateDetails.Documents.FirstOrDefault().DocumentId);
            }
            catch (Exception ex) { }

            return gettemplateDetails;
        }

        public static Configuration DocusignConfigurations(DocusignAuthorizationModel credential)
        {
            ApiClient apiclient = new ApiClient(credential.Url);
            Configuration cfi = new Configuration(apiclient);
            string url = string.Empty;
            string authHeader = "{\"Username\":\"" + credential.UserName + "\", \"Password\":\"" + credential.Password + "\", \"IntegratorKey\":\"" + credential.IntegratorKey + "\"}";
            cfi.AddDefaultHeader("X-DocuSign-Authentication", authHeader);
            return cfi;
        }

        public static Tabs GetTemplateTabs(DocusignAuthorizationModel credentials, EnvelopeTemplate template, string DocumentID)
        {
            ApiClient apiclient = new ApiClient(credentials.Url);
            Configuration cfi = new Configuration(apiclient);
            string authHeader = "{\"Username\":\"" + credentials.UserName + "\", \"Password\":\"" + credentials.Password + "\", \"IntegratorKey\":\"" + credentials.IntegratorKey + "\"}";
            cfi.AddDefaultHeader("X-DocuSign-Authentication", authHeader);
            TemplatesApi templatesapi = new TemplatesApi(cfi);
            //string documentID = string.Empty;
            Tabs tabs = new Tabs();

            try
            {
                if (template != null)
                {
                    tabs = templatesapi.GetDocumentTabs(credentials.AccountId, template.TemplateId, DocumentID);
                    //if (template.Documents.Count > 0)
                    //{
                    //    documentID = template.Documents[0].DocumentId;
                    //    tabs = templatesapi.GetDocumentTabs(credentials.AccountId, template.TemplateId, DocumentID);
                    //}
                }
            }
            catch (Exception ex) { }
            return tabs;
        }
    }
}