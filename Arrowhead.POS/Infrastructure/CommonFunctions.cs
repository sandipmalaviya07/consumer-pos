﻿using Arrowhead.POS.Core;
using Arrowhead.POS.Model;
using Arrowhead.POS.Service;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Arrowhead.POS.Infrastructure
{
    public static class CommonFunctions
    {
        public static void AddNewTab(long QuoteID, bool _IsQuoteTab = true)
        {
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            if (ProjectSession.TabDetail != null && ProjectSession.TabDetail.Where(w => w.QuoteID == QuoteID).Count() == 0)
            {
                if (_IsQuoteTab)
                {
                    if (!ProjectSession.TabDetail.Any(x => x.QuoteID == QuoteID))
                    {
                        var quoteDetail = quotesServices.GetQuoteDetailForTab(ProjectSession.QuoteID);

                        List<TabModel> tabModelList = new List<TabModel>();
                        tabModelList = ProjectSession.TabDetail;
                        if (quoteDetail != null)
                        {
                            tabModelList.Add(quoteDetail);
                        }
                        ProjectSession.TabDetail = tabModelList;
                    }
                }
                else
                {
                    List<TabModel> tabModelList = new List<TabModel>();
                    tabModelList = ProjectSession.TabDetail;

                    TabModel tabModel = new TabModel()
                    {
                        FullName = "Policy Report",
                        QuoteID = QuoteID,

                    };

                    tabModelList.Add(tabModel);
                    ProjectSession.TabDetail = tabModelList;
                }

            }
            else
            {
                if (_IsQuoteTab)
                {
                    if(ProjectSession.TabDetail.Any(x => x.QuoteID == QuoteID))
                    {
                        var quoteDetail = quotesServices.GetQuoteDetailForTab(ProjectSession.QuoteID);
                        ProjectSession.TabDetail.Where(x => x.QuoteID == QuoteID).First().FullName = quoteDetail.FullName;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public static bool IsPaymentCompleted(long quoteId)
        {
            bool IsPaymentCompleted = false;
            using (var db = new ArrowheadPOSEntities())
            {
                IsPaymentCompleted = (from q in db.QuotePayments
                                      join gt in db.GeneralStatusTypes on q.FKPaymentStatusID equals gt.ID
                                      where q.FKQuoteID == quoteId &&
                                      (gt.Code == Enums.PAYMENTSTATUS.SUCCESS.ToString() || gt.Code == Enums.PAYMENTSTATUS.INITIATED.ToString())
                                      orderby q.ID descending
                                      select new { gt.Code }).Any();
            }

            return IsPaymentCompleted;
        }

        public static bool GetUserPreferenceByPropertyId(long agencyId, long userId, long propertyId)
        {
            // Set User Preference into Session 
            using (var db = new ArrowheadPOSEntities())
            {
                string isUserPrefExist =  (db.UserPreferences.Where(x => x.FKAgencyID == agencyId && x.FKUserId == userId && x.FKPropertyID == propertyId).Select(x => x.PropertyValue1).FirstOrDefault());
                if(!string.IsNullOrEmpty(isUserPrefExist) && isUserPrefExist.ToUpper() == "TRUE")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool IsAgencyPreferenceExistByPropertyID(long agencyId, long userId, long propertyId)
        {
            // Set User Preference into Session 
            using (var db = new ArrowheadPOSEntities())
            {
                var agencyPref = (db.UserPreferences.Where(x => x.FKAgencyID == agencyId && x.FKUserId == userId && x.FKPropertyID == propertyId).FirstOrDefault());
                if (agencyPref != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public static void AgencyInfo(long agencyID)
        {
            AutoQuoteServices autoQuoteServices = new AutoQuoteServices();
            //if(ProjectSession.AgencyDetails != null)
            //{
            if (agencyID > 0)
            {
                var agencyDetails = autoQuoteServices.GetAgencyDetails(agencyID);

                AgencyModel agencyModel = new AgencyModel();
                agencyModel = ProjectSession.AgencyDetails;
                if (agencyDetails != null)
                {
                    agencyModel = agencyDetails;
                }
                ProjectSession.AgencyDetails = agencyModel;
            }
            //}
        }

        public static void UpdateCurrentQuoteStep(long quoteID, string currentStepCode)
        {
            GeneralServices generalServices = new GeneralServices();
            AutoQuoteServices quotesServices = new AutoQuoteServices();
            {
                try
                {
                    if (quoteID > 0)
                    {
                        var currentStep = quotesServices.GetCurrentStepDetails(quoteID);
                        var quoteStep = quotesServices.QuoteStepId(currentStepCode);
                        long currentStepOrderID = currentStep.CurrentStepOrderId;
                        long QuoteSetpOrdeID = quoteStep;
                        if (currentStepOrderID < QuoteSetpOrdeID)
                            generalServices.UpdateQuoteCurrentStep(quoteID, currentStepCode);

                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        /// <summary>
        /// Update Agency by ID and Current Step Code
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="currentStepCode"></param>
        public static void UpdateCurrentAgencyStep(long agencyId, string currentStepCode)
        {
            if (string.IsNullOrEmpty(currentStepCode))
            {
                throw new ArgumentException("current step can not be empty !!", nameof(currentStepCode));
            }

            GeneralServices generalServices = new GeneralServices();
            AgencySetupServices _agencySetupServices = new AgencySetupServices();
            try
            {
                if (agencyId > 0)
                {
                    var currentStep = _agencySetupServices.GetCurrentStepDetails(agencyId);
                    var agencyStep = _agencySetupServices.AgencyStepId(currentStepCode);
                    if (currentStep.CurrentStepOrderId < agencyStep)
                    {
                        generalServices.UpdateAgencyCurrentStep(agencyId, currentStepCode);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdfFilePath"></param>
        /// <param name="outputPath"></param>
        /// <param name="startPage"></param>
        /// <param name="interval"></param>
        /// <param name="pdfFileName"></param>
        public static void SplitAndSaveInterval(byte[] pdfFilePath, string outputPath, int startPage, int interval, string pdfFileName)
        {
            using (PdfReader reader = new PdfReader(pdfFilePath))
            {
                iTextSharp.text.Document document = new iTextSharp.text.Document();
                PdfCopy copy = new PdfCopy(document, new FileStream(outputPath + "/" + pdfFileName, FileMode.Create));
                document.Open();

                PdfReader pdfReader = new PdfReader(outputPath + "/" + pdfFileName);



                for (int pagenumber = startPage; pagenumber < (startPage + interval); pagenumber++)
                {
                    if (reader.NumberOfPages >= pagenumber)
                    {
                        copy.AddPage(copy.GetImportedPage(reader, pagenumber));
                    }
                    else
                    {
                        break;
                    }

                }

                document.Close();
            }
        }


        /// <summary>
        /// Reset Quote to Coverage Step if current step is Policy 
        /// </summary>
        /// <param name="quoteId"></param>
        public static void UpdateQuoteToCoverageStep(long quoteId, string coverageStepCode)
        {
            AutoQuoteServices _quoteServices = new AutoQuoteServices();
            GeneralServices _generalServices = new GeneralServices();
            AutoPolicyServices _autoPolicyServices = new AutoPolicyServices();
            try
            {
                if (quoteId > 0)
                {
                    var currentStep = _quoteServices.GetCurrentStepDetails(quoteId);
                    var paymentDetails = _autoPolicyServices.GetPaymentByQuoteID(quoteId);
                    if (currentStep.CurrentStepOrderId >= 5 && paymentDetails.IsPaymentCompleted == false && paymentDetails.IsDocusignCompleted == false) // 5 - Underwriting Question Step 
                    {
                        _generalServices.UpdateQuoteCurrentStep(quoteId, coverageStepCode);
                        _generalServices.ResetAutoQuoteDetail(quoteId);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static string GetXMLFromObject(object o)
        {

            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(o.GetType());
            foreach (var node in
            from XmlNode node in xmlDoc
            where node.NodeType == XmlNodeType.XmlDeclaration
            select node)
            {
                xmlDoc.RemoveChild(node);
            }
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, o);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string JsonToXML(string json)
        {
            XmlDocument doc = new XmlDocument();

            using (var reader = JsonReaderWriterFactory.CreateJsonReader(Encoding.UTF8.GetBytes(json), XmlDictionaryReaderQuotas.Max))
            {
                XElement xml = XElement.Load(reader);
                doc.LoadXml(xml.ToString());
            }

            return doc.InnerXml;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void OnException(string moduleId, string OperationType, string errormessage)
        {
            using (var db = new ArrowheadPOSEntities())
            {
                AuditHistoryService auditHistoryService = new AuditHistoryService();

                AuditHistoryModel _historyModel = new AuditHistoryModel()
                {
                    AgencyID = ProjectSession.AgencyDetails != null ? (int)ProjectSession.AgencyDetails.AgencyId : 0,
                    IsError = true,
                    ModuleID = moduleId,
                    Operation = Convert.ToChar(OperationType),
                    Remarks = "Got error in Module : " + moduleId + " is " + errormessage,
                    QuoteID = ProjectSession.QuoteID,
                    CustomerId = db.Quotes.Where(w => w.ID == ProjectSession.QuoteID).Select(s => s.FkCustomerID).FirstOrDefault()
                };
                auditHistoryService.InsertAuditHistory(_historyModel);
            }
        }


        public static string ActivationUrl(string url)
        {
            string returnUrl;
            if (url.Contains("onboarding.rateforce.com") || url.Contains("quotes.arrowheadauto.com") || url.Contains("arrowhead-pos-prod-mirror.azurewebsites.net") || url.Contains("arrowhead-pos-prod.azurewebsites.net"))
                returnUrl = "https://onboarding.rateforce.com/";
            else
                returnUrl = "https://arrowhead-pos-qa.azurewebsites.net/";
            return returnUrl;
        }


        public static string AfterActivationRedirectionUrl(string url)
        {
            string returnUrl = string.Empty;
            if (url.Contains("onboarding.rateforce.com") || url.Contains("quotes.arrowheadauto.com") || url.Contains("arrowhead-pos-prod-mirror.azurewebsites.net") || url.Contains("arrowhead-pos-prod.azurewebsites.net"))
                returnUrl = "https://quotes.arrowheadauto.com/";
            else
                returnUrl = "https://arrowhead-pos-qa.azurewebsites.net/";
            return returnUrl;
        }

    }
}