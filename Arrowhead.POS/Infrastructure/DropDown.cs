﻿using Arrowhead.POS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Arrowhead.POS.Model;
using Arrowhead.POS.Core;

namespace Arrowhead.POS.Infrastructure
{
    public class DropDown
    {
        public static SelectList GetAllQuoteSourceStatus()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetQuoteSource();
            return new SelectList(lstAgency, "ID", "Source");
        }

        public static SelectList GetAllOwnership()
        {
            var commonService = new CommonServices();
            var lstOwnership = commonService.GetAllOwnership();
            return new SelectList(lstOwnership, "ID", "Name");
        }

        public static SelectList GetAllDealerLienHolders()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetAllLienHolders();
            return new SelectList(lstAgency, "ID", "CommercialName");
        }

        public static SelectList GetAllDriverLicenseStatus()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetDriverLicenseStatus();
            return new SelectList(lstAgency, "ID", "Name");
        }

        public static SelectList GetAllHomeTypes()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetHomeTypesList();
            return new SelectList(lstAgency, "ID", "Name");
        }

        //Military discount types for SC region driver only.
        public static SelectList GetMilitaryTypes()
        {
            var commonService = new CommonServices();
            var lstMilitary = commonService.GetMilitaryTypes();
            return new SelectList(lstMilitary, "ID", "Name");
        }

        //Warehouse discount types for SC region driver only.
        public static SelectList GetWarehouseTypes()
        {
            var commonService = new CommonServices();
            var lstMilitary = commonService.GetWarehouseTypes();
            return new SelectList(lstMilitary, "ID", "Name");
        }

        public static SelectList GetDriverRelationShip()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetDriverRelationShip();
            return new SelectList(lstAgency, "ID", "Name");
        }

        public static SelectList GetAllViolation()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetAllViolation();
            return new SelectList(lstAgency, "AutoDriverViolationId", "ViolationName");
        }

        public static SelectList GetAllDriverOccupation()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetDriverOccupation();
            return new SelectList(lstAgency, "ID", "Name");
        }

        public static SelectList GetAllAccountType()
        {
            var commonService=new CommonServices();
            var lstAccountType = commonService.GetAccountType();
            return new SelectList(lstAccountType, "ID", "Name");
        }

        public static SelectList GetAllState()
        {
            var commonService = new CommonServices();
            
            var lstAgency = commonService.GetAllState(ProjectSession.QuoteID);
            return new SelectList(lstAgency, "ID", "Name");
        }

        public static SelectList GetAllStateForPayoutReport()
        {
            var commonService = new CommonServices();

            var lstState= commonService.GetAllState(ProjectSession.QuoteID);
            if (lstState != null && lstState.Count > 0)
                lstState.Insert(0, new State { Code = "All", Name = "All" });

            return new SelectList(lstState, "Code", "Name");
        }

        public static SelectList GetAllAgency()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetAllAgency();
            return new SelectList(lstAgency, "ID", "Name");
        }

        public static SelectList GetAllAgencyForNSDPayOutReport()
        {
            var commonService = new CommonServices();
            // Add Default Value into List 
            Agency agency = new Agency();
            agency.Name = "All Agency";
            agency.ID = 0;
            var lstAgency = commonService.GetAllAgency();
            lstAgency.Insert(0, agency);
            return new SelectList(lstAgency, "ID", "Name",ProjectSession.AgencyDetails.AgencyId);
        }


        public static SelectList GetAllPriorCarrier()
        {
            var commonService = new CommonServices();
            var lstAgency = commonService.GetPriorCarrier();
            return new SelectList(lstAgency, "ID", "Name");
        }

        public static SelectList GetAllPriorInsuranceLapse()
        {
            var commonService = new CommonServices();
            var lstPriorInsuranceLapse = commonService.GetPriorInsuranceLapse();
            return new SelectList(lstPriorInsuranceLapse, "ID", "Name");
        }

        public static SelectList GetAllCarriers()
        {
            var commonService = new CommonServices();
            var lstCarrier = commonService.GetAllCarrier();
            return new SelectList(lstCarrier, "ID", "Name");
        }

        public static SelectList GetAllSoldBy()
        {
            var commonService = new CommonServices();
            var lstSoldBy = commonService.GetSoldBy();
            return new SelectList(lstSoldBy, "ID", "Name");
        }

        public static SelectList GetYears()
        {
            List<YearModel> ddlYears = new List<YearModel>();
            int CurrentYear = ConvertTo.GetEstTimeNow().Year;

            for (int i = CurrentYear; i <= CurrentYear + 20; i++)
            {
                ddlYears.Add(new YearModel
                {
                    Text = i.ToString(),
                    Value = i.ToString().Substring(2,2)
                });
            }

            return new SelectList(ddlYears, "Value","Text");
         

        }

        public static List<SelectListItem> GetPolicyYear(int startYear, int endYear)
        {
            List<SelectListItem> _getYear = new List<SelectListItem>();
            for (int i = startYear; i <= endYear; i++)
            {
                SelectListItem _obj = new SelectListItem() { Text = i.ToString(), Value = i.ToString(),Selected= true };
                _getYear.Add(_obj);
            }
            return _getYear;
        }



        public static SelectList GetGeneralStatusByType(string statusName)
        {
            var commonService = new CommonServices();
            var lstGeneralStatusTypeList = commonService.GetGeneralStatusByType(statusName);
            return new SelectList(lstGeneralStatusTypeList, "ID", "Name");
        }

        public static SelectList GetNSDPlans()
        {
            var commonService = new CommonServices();
            var lstNSDPlans = commonService.GetNSDPlans();
            if(lstNSDPlans!=null && lstNSDPlans.Count > 0)
                lstNSDPlans.Insert(0, new DropdownModel { ID = 0, Name = "None" });
            return new SelectList(lstNSDPlans, "ID", "Name");
        }

    }
}