﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Arrowhead.POS.Infrastructure
{
    public class ITCVehicleServices
    {
        //public const string url = "https://ratingqa.itcdataservices.com/Webservices/imp/api/vehicles/";
        //public const string VehicleDetailUrl = "https://ratingqa.itcdataservices.com/Webservices/imp/api/vehicles/detail/";
        //public const string impAccountId = "4A69E926-E440-48C8-A41D-BCDB1ED5BFF3";



        public const string url = "https://www.itcratingservices.com/Webservices/imp/api/vehicles/";
        public const string VehicleDetailUrl = "https://www.itcratingservices.com/Webservices/imp/api/vehicles/detail/";
        public const string impAccountId = "4a69e926-e440-48c8-a41d-bcdb1ed5bff3";


        public static List<SelectListItem> GetAllVehicleYears()
        {
            List<int> lst = new List<int>();
            int Year1 = DateTime.Now.Year;

            for (int value = Year1; value >= 1980; value += -1)
                lst.Add(value);

            List<SelectListItem> lstYears = (from item in lst
                                             select new SelectListItem() { Text = Convert.ToString( item), Value = Convert.ToString(item) }).ToList();
            return lstYears;
        }

        public static List<SelectListItem> GetAllVehicleMakesByYear(string year)
        {
            List<string> lst = new List<string>();

            if (!(string.IsNullOrWhiteSpace(year)))
            {
                string response = GetResponse(url + year, impAccountId);
                response = response.Replace("[", "").Replace("]", "");
                List<string> VehicleList = response.ToString().Replace("\"", "").Split(',').ToList();
                lst = VehicleList;
            }


            List<SelectListItem> lstMakes = (from item in lst
                                             select new SelectListItem() { Text = item, Value = item }).ToList();
            return lstMakes;
        }

        public static List<SelectListItem> GetAllVehicleModelsByYearMake(string year, string make)
        {
            List<string> lst = new List<string>();

            if (!(string.IsNullOrWhiteSpace(make) && string.IsNullOrWhiteSpace(year)))
            {
                string response = GetResponse(url + year + "/" + make, impAccountId);
                response = response.Replace("[", "").Replace("]", "");
                List<string> VehicleList = response.ToString().Replace("\"", "").Split(',').ToList();
                lst = VehicleList;
            }
            List<SelectListItem> lstModels = (from item in lst
                                              select new SelectListItem() { Text = item, Value = item }).ToList();
            return lstModels;
        }

        public VehicleDetailModel DecodeVIN(string vin)
        {
            VehicleDetailModel VINDetailModel = new VehicleDetailModel();

            if (!(string.IsNullOrWhiteSpace(vin)))
            {
                string response = GetResponse(VehicleDetailUrl + vin, impAccountId);
                response = response.Replace("[", "").Replace("]", "");
                VINDetailModel = JsonConvert.DeserializeObject<VehicleDetailModel>(response);
            }

            return VINDetailModel;
        }

        public static string GetVinByVehicleDetail(string year, string make, string model)
        {
            List<VehicleDetailModel> VINDetailModel = new List<VehicleDetailModel>();
            string vin = string.Empty;

            if (!(string.IsNullOrWhiteSpace(make) && string.IsNullOrWhiteSpace(year) && string.IsNullOrWhiteSpace(model)))
            {
                string response = GetResponse(url + year + "/" + make + "/" + model, impAccountId);

                VINDetailModel = JsonConvert.DeserializeObject<List<VehicleDetailModel>>(response);
                if (VINDetailModel != null)
                    vin = VINDetailModel[0].VIN;
            }
            return vin;
        }

        public static string GetResponse(string url, string impAccountId)
        {
            string response = string.Empty;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(impAccountId.ToString() + ":" + impAccountId.ToString())));
                    response = client.DownloadString(url.Replace("*", ""));
                }
            }
            catch (Exception ex)
            {
            }

            return response;
        }

        public static DataTable ToDataTable(List<string> data, string ColumnName)
        {
            DataTable table = new DataTable();
            table.Columns.Add(ColumnName, typeof(string));

            foreach (string str in data)
            {
                DataRow rows = table.NewRow();
                rows[ColumnName] = str.Replace("\"", @"\""");
                table.Rows.Add(rows);
            }
            return table;
        }
    }

    public class VehicleDetailModel
    {
        public string AntiLock { get; set; }
        public string BodyType { get; set; }
        public string FuelType { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public int ModelGroupCode { get; set; }
        public int ModelCode { get; set; }
        public string PassSeatRestraint { get; set; }
        public string AirBags { get; set; }
        public string AntiTheft { get; set; }
        public string TruckSize { get; set; }
        public int UniqueSymCode { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public bool FrontWD { get; set; }
        public int MSRP { get; set; }
        public int NumOfCyl { get; set; }
        public int NumOfDoors { get; set; }
        public string VehicleType { get; set; }
    }
}