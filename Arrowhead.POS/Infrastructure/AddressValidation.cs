﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Arrowhead.POS.Model;
using System.Net;
using Arrowhead.POS.Core;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Reflection;

namespace Arrowhead.POS.Infrastructure
{
    public class AddressValidation
    {
        public AddressResponseModel CheckAddress(AddressValidationModel model)
        {
            string fullAddress = string.Empty;
            string validationAddress = @"<AddressValidateRequest USERID='" + ConstantVariables.AddressUserId.ToString() + "'><Address><Address1> " + model.Address1 + " </Address1>" + "<Address2>" + model.Address2 + "</Address2>" + "<City>" + model.City + " </City>" + "<State>" + model.State + "</State>" + "<Zip5>" + model.ZipCode + "</Zip5><Zip4></Zip4></Address></AddressValidateRequest>";

            AddressResponseModel addressValidationModel = new AddressResponseModel();

            ApplicantModel applicantModel = new ApplicantModel();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;


            HttpWebRequest request = WebRequest.Create("https://secure.shippingapis.com/ShippingAPI.dll?API=Verify&XML=" + validationAddress) as HttpWebRequest;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            WebHeaderCollection header = response.Headers;


            if (!String.IsNullOrEmpty(model.Address1) || !String.IsNullOrEmpty(model.Address2))
            {
                if (model.Address1 != string.Empty && (model.Address1.ToLower().Contains("po box") == true || model.Address1.ToLower().Contains("p.o. box") == true ||
                    model.Address1.ToLower().Contains("p.o box") == true))
                {
                    model.isAddressValidated = false;
                    model.isPOBoxError = true;
                }
                else if (!(String.IsNullOrEmpty(model.Address2)))
                {
                    if (model.Address2.ToLower().Contains("po box") == true || model.Address2.ToLower().Contains("p.o. box") == true ||
                        model.Address2.ToLower().Contains("p.o box") == true)
                    {
                        model.isAddressValidated = false;
                        model.isPOBoxError = true;
                    }
                } // Validate Address 1 Contains  %apt%, %ste%, %suite% or %#%
                else if (model.Address1 != string.Empty && (model.Address1.ToLower().Contains("apt") || model.Address1.ToLower().Contains("ste") ||
                         model.Address1.ToLower().Contains("suite") || model.Address1.ToLower().Contains("#")))
                {
                    model.isAddressValidated = true;
                }
            }


            if (!String.IsNullOrEmpty(model.Address1))
            {
                fullAddress = model.Address1 + "|";
            }
            if (!String.IsNullOrEmpty(model.Address2))
            {
                fullAddress += model.Address2 + "|";
            }
            else
            {
                fullAddress += "|"; // blank 
            }
            if (!String.IsNullOrEmpty(model.City))
            {
                fullAddress += model.City + "|";
            }
            if (!String.IsNullOrEmpty(model.State))
            {
                fullAddress += model.State + "|";
            }
            if (!String.IsNullOrEmpty(model.ZipCode))
            {
                fullAddress += model.ZipCode;
            }

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream, Encoding.UTF8);
                var responseString = reader.ReadToEnd();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseString);
                if (xmlDoc.SelectSingleNode("/AddressValidateResponse/Address/Error") != null && model.isAddressValidated == false) // isAddressChecked flag will by pass validation 
                {

                    addressValidationModel.IsSuccess = false;
                    addressValidationModel.Message = xmlDoc.SelectSingleNode("/AddressValidateResponse/Address/Error/Description").InnerText;
                    addressValidationModel.FullAddress = fullAddress;

                    // Override Error Message if Address Contains P.O Box ss
                    if (model.isPOBoxError)
                    {
                        addressValidationModel.Message = "Garaging Address cannot be a PO Box.";
                    }
                }
                else if (xmlDoc.SelectSingleNode("/AddressValidateResponse/Address/ReturnText") != null && model.isAddressValidated == false) // isAddressChecked flag will by pass validation 
                {
                    addressValidationModel.IsSuccess = false;
                    addressValidationModel.Message = xmlDoc.SelectSingleNode("/AddressValidateResponse/Address/ReturnText").InnerText;
                    addressValidationModel.FullAddress = fullAddress;

                    // Override Error Message if Address Contains P.O Box ss
                    if (model.isPOBoxError)
                    {
                        addressValidationModel.Message = "Garaging Address cannot be a PO Box.";
                    }
                }
                else
                {
                    addressValidationModel.IsSuccess = true;
                    addressValidationModel.Message = string.Empty;
                }

                if (addressValidationModel.Message.ToLower().Contains("the address you entered was found")) // apt missing then set flag 
                {
                    model.isAddressValidated = true;
                }
            }
            return addressValidationModel;

        }
    }
}