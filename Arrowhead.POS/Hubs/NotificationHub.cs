﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using Arrowhead.POS.Service;
using Arrowhead.POS.Model;
using Microsoft.AspNet.SignalR;

namespace Arrowhead.POS.Hubs
{
    public class NotificationHub : Hub
    {
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }


        public async Task AddSignalR(long UserID)
        {

            try
            {
                SignalRConnection signalRConnection = new SignalRConnection();
                signalRConnection.ConnectionID = Context.ConnectionId;
                signalRConnection.UserID = UserID;
                //var alreadyExists = ConnectionList.MyConnectionList.FirstOrDefault(x => x.UserID == UserID);
                //if (alreadyExists != null)
                //{
                //    ConnectionList.MyConnectionList.Add(alreadyExists);
                //}

                ConnectionList.MyConnectionList.Add(signalRConnection);

            }
            catch (Exception ex)
            {

                throw;
            }

            //var test = AuthToken;
            //var con = Context.ConnectionId;
        }

        public async Task SendNotification(NotificationModel model)
        {
            Clients.Client(Context.ConnectionId).SendAsync("send",model.QuoteId);
            //await Clients.All.SendAsync("send", user, message);
        }

        public async Task OnDisconnectedAsync(Exception exception)
        {
            ConnectionList.MyConnectionList.RemoveAll(c => c.ConnectionID == Context.ConnectionId);
            await OnDisconnectedAsync(exception);
        }
    }
}