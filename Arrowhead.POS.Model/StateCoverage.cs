//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class StateCoverage
    {
        public long ID { get; set; }
        public int FkStateID { get; set; }
        public int FkCoverageID { get; set; }
        public int OrderID { get; set; }
        public decimal Value1 { get; set; }
        public decimal Value2 { get; set; }
        public string DisplayTextValue1 { get; set; }
        public string DisplayTextValue2 { get; set; }
        public bool IsSelected { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedById { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedById { get; set; }
        public string Code { get; set; }
    
        public virtual Coverage Coverage { get; set; }
        public virtual State State { get; set; }
    }
}
