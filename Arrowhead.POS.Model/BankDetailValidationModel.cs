﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public  class BankDetailValidationModel
    {
        public long AgencyId { get; set; }
        public bool isBankDetailSkip { get; set; }
    }
}
