﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class PolicyListModel
    {
        public long QuoteId { get; set; }

        public long CustomerId { get; set; }

        public string EnvelopeId { get; set; }

        public long FkEnvelopeStatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        public long QuoteEnvelopeId { get; set; }

        public string CustomerName { get; set; }

        public string PolicyNumber { get; set; }

        public string AgencyName { get; set; }

        public string EmailId { get; set; }

        public string AgencyEmailId { get; set; }

        public decimal Payment { get; set; }

        public string PaymentType { get; set; }
    }
}
