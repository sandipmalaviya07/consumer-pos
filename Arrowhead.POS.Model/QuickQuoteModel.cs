﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Arrowhead.POS.Model
{
    public class QuickQuoteModel
    {
        //public QuickQuoteInfoModel QuoteInfoModel { get; set; }   
        public long QuoteId { get; set; }
        public List<QuickQuoteVehicleModel> lstQuickQuoteVehicleModel { get; set; }
        public QuickQuoteVehicleModel _QuickQuoteVehicleModel { get; set; }
        public List<QuickQuoteDriverModel> lstQuickQuoteDriverModel { get; set; }        
        public QuickQuoteDriverModel _QuickQuoteDriverModel { get; set; }
        public string Coverage { get; set; }
        public List<StateCoveragesModel> lstStateCoveragesModels { get; set; }
    }

    public class QuickQuoteInfoModel
    {
        public long QuoteId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Dob { get; set; }
    }

    public class QuickQuoteVehicleModel
    {
        public long AutoVehicleId { get; set; }
        public long QuoteId { get; set; }
        public int? VehicleOrderId { get; set; }
        [Required(ErrorMessage = "Please enter Year")]
        [Display(Name = "Year")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Please enter Make")]
        [StringLength(100)]
        [Display(Name = "Make")]
        public string Make { get; set; }

        public string Model { get; set; }

        [Required(ErrorMessage = "Please enter Vin")]
        //[Remote("IsVinNumber", "RemoteValidation", AdditionalFields = "AutoVehicleId", ErrorMessage = "VIN already exists.Please add another vin number.", HttpMethod = "POST")]
        [StringLength(100)]
        [Display(Name = "VIN")]
        public string Vin { get; set; }

        public long? FkOwnerTypeID { get; set; }

        public string COMPCOLLValue { get; set; }
        

    }

    public class QuickQuoteDriverModel
    {
        public long AutoDriverId { get; set; }
        public System.DateTime DOBStartDate { get; set; }
        public long QuoteId { get; set; }
        public long CustomerId { get; set; }
        public long UserId { get; set; }

        [Required(ErrorMessage = "Please enter first name")]
        [StringLength(75)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter Last Name")]
        [StringLength(75)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Date of Birth")]
        [LessThanOrEqualTo("DOBStartDate")]
        [Display(Name = "Date of Birth")]
        public DateTime? Dob { get; set; }

        [Required(ErrorMessage = "Please select Gender")]
        [StringLength(30)]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        public long MaritalStatusID { get; set; }
        public string MaritalStatusName { get; set; }

        [Required(ErrorMessage = "Please select Marital Status")]
        [StringLength(30)]
        [Display(Name = "Marital Status")]
        public string MaritalStatusCode { get; set; }

        public int? DriverOrderID { get; set; }


        [Required(ErrorMessage = "Please select Driver Relation")]
        [Display(Name = "This Driver Is")]
        public long RelationCDId { get; set; }

        public bool IsFirstDriver { get; set; }

        [Display(Name = "Excluded")]
        public bool IsExcluded { get; set; }

        [Required(ErrorMessage = "Please enter zip code")]
        [StringLength(5, ErrorMessage = "Must be at least 5 characters long.")]
        [Remote("IsStateRateAvailable", "RemoteValidation", ErrorMessage = "Unable to rate this state.", HttpMethod = "POST")]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }
    }

    public class QuickQuoteCoverage
    {
        public int StateId { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public int CoverageId { get; set; }
        public string CoverageName { get; set; }
        public string CoverageFormControl { get; set; }
        public List<QuickCoveragesFormControlValues> CoveragesFormControlValues { get; set; } = new List<QuickCoveragesFormControlValues>();
        public string Selected { get; set; }
        public bool IsVehicleCoverage { get; set; }
        public long? VehicleID { get; set; }
        public string VehicleTitle { get; set; }
        public int? VehicleOrderID { get; set; } = 0;
        public bool IsDuplicate { get; set; } = false;
        public bool IsEnable { get; set; } = true;
        public int? ParentId { get; set; }
        public string QuoteSelectedValue { get; set; }
        public string GroupName { get; set; }
        public bool IsEdit { get; set; }
    }

    public class QuickCoveragesFormControlValues
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }
    }

    public class QuickStateCoverageValues
    {
        public long StateCoverageId { get; set; }
        public int StateId { get; set; }
        public long? VehicleID { get; set; }
        public int CoverageId { get; set; }
        public int OrderId { get; set; }
        public decimal Value1 { get; set; }
        public decimal Value2 { get; set; }
        public string DisplayTextValue1 { get; set; }
        public string DisplayTextValue2 { get; set; }
        public bool IsSelected { get; set; }
        public string Code { get; set; }
    }
}
