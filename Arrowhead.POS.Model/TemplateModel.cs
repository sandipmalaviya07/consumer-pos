﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{

    public class TemplateModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Isactive { get; set; }
        public string SMSTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public bool IsSMSActive { get; set; }
        public bool IsEmailActive { get; set; }
        public string DynamicField { get; set; }
        public string EmailSubject { get; set; }
        public int OfficeId { get; set; }
        public string officeName { get; set; }
        public int FKTemplateTypeId { get; set; }
        public long TemplateId { get; set; }
    }

}
