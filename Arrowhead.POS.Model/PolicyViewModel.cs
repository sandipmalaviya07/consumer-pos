﻿using Arrowhead.POS.Core.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class PolicyReportModel
    {
        public PagerList<PolicyViewModel> PolicyView { get; set; }


        public PolicySummaryModel PolicySummary { get; set; }
    }

    public class PolicyViewModel
    {
        public long QuoteId { get; set; }

        public string PolicyNumber { get; set; }

        public DateTime? CompletedDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string State { get; set; }

        public string PolicyStatus { get; set; }

        public decimal? DownPayment { get; set; }

        public string InsuranceCompanyName { get; set; }

        public string SourceName { get; set; }

        public int StateId { get; set; }
        public int CarrierId { get; set; }

        public long UserId { get; set; }

        public string Payment { get; set; }

        public bool IsPaymentCompleted { get; set; }

        public string EnvelopeStatus { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsNsd { get; set; }

        public decimal? NsdFee { get; set; }

        public DateTime PaymentDate { get; set; }

        public string AgencyName { get; set; }

        public string ProducerCode { get; set; }

        public string ProducerPhone { get; set; }

        public decimal? DownPremium { get; set; }

        public decimal? Nsd { get; set; }

        public decimal? CCFee { get; set; }

        public int NsdCount { get; set; }

        public decimal Premium { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal SummaryTotal { get; set; }
    }

    public class PolicySummaryModel
    {
        public decimal NsdFee { get; set; }

        public decimal CCFee { get; set; }

        public decimal Premium { get; set; }

        public decimal TotalMotorClubSales { get; set; }

        public decimal TotalMotorClubNet { get; set; }

        public int TotalPolicySold { get; set; }

        public int TotalCCEchekSold { get; set; }

        public int TotalSweepCount { get; set; }

        public decimal SummaryPremium { get; set; }

        public decimal TotalPremium { get; set; }

        public decimal EcheckCCPremium { get; set; }

        public DateTime UpcomingACHDraftDate { get; set; }
        public decimal  WeeklyACHDraftAmt { get; set; }

    }


}
