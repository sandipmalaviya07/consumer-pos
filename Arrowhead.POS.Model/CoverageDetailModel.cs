﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class CoverageDetailModel
    {
        public int CoverageId { get; set; }
        public string CoverageName { get; set; }
        public string CoverageFormControl { get; set; }
        public string CoverageCode { get; set; }
        public string GroupName { get; set; }
        public string ControlType { get; set; }
        public bool IsVehicleCoverage { get; set; }
        public string DisplayText { get; set; }
        public decimal Value1 { get; set; }
        public decimal Value2 { get; set; }
    }
}
