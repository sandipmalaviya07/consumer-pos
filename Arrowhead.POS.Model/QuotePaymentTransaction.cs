//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuotePaymentTransaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QuotePaymentTransaction()
        {
            this.QuotePaymentTransactionDetails = new HashSet<QuotePaymentTransactionDetail>();
        }
    
        public long ID { get; set; }
        public long FKQuoteID { get; set; }
        public long FKReceiptID { get; set; }
        public Nullable<long> FKQuoteOnlinePaymentID { get; set; }
        public long FKStatusID { get; set; }
        public decimal TotalAmount { get; set; }
        public long FKPaymentTypeID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public Nullable<long> ModifiedByID { get; set; }
        public string Notes { get; set; }
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string CustomField3 { get; set; }
        public string CustomField4 { get; set; }
        public Nullable<int> FKPaymentGatewayID { get; set; }
    
        public virtual GeneralStatusType GeneralStatusType { get; set; }
        public virtual GeneralStatusType GeneralStatusType1 { get; set; }
        public virtual Quote Quote { get; set; }
        public virtual QuoteOnlinePayment QuoteOnlinePayment { get; set; }
        public virtual QuotePaymentReceipt QuotePaymentReceipt { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuotePaymentTransactionDetail> QuotePaymentTransactionDetails { get; set; }
        public virtual PaymentGatewayProvider PaymentGatewayProvider { get; set; }
    }
}
