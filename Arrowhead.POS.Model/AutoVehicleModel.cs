﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class AutoVehicleModel
    {
        public long AutoVehicleId { get; set; }
        public long QuoteId { get; set; }

        public long? CarUsedforId { get; set; }

        public long? FkOwnerTypeID { get; set; }
        public long? FrequencyMileId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string StateAbbr { get; set; }
        public int? EstimatedMileage { get; set; }
        public int? VehicleOrderId { get; set; }
        public string BodyType { get; set; }

        [Required(ErrorMessage = "Please enter Year")]
        [Display(Name = "Year")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Please enter Make")]
        [StringLength(100)]
        [Display(Name = "Make")]
        public string Make { get; set; }

        [Required(ErrorMessage = "Please enter Vin")]
        [Remote("IsVinNumber", "RemoteValidation", AdditionalFields = "AutoVehicleId", ErrorMessage = "VIN already exists.Please add another vin number.", HttpMethod = "POST")]
        [StringLength(100)]
        [Display(Name = "VIN")]
        public string Vin { get; set; }

        public string Model { get; set; }

        [Display(Name = " Lien Holder")]
        public int? LienHolderId { get; set; }

        [Display(Name = "LienHolderCommercialName")]
        [Required(ErrorMessage = "Please enter Name.")]
        public string LienHolderCommercialName { get; set; }
        
        public string LienHolderAddress { get; set; }

        [Display(Name = "LienHolderAddress1")]
        [Required(ErrorMessage = "Please enter Address.")]
        public string LienHolderAddress1 { get; set; }

        public string LienHolderAddress2 { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "Please enter City.")]
        public string LienHolderCity { get; set; }

        [Display(Name = "Zip Code")]
        [Required(ErrorMessage = "Please enter ZipCode.")]
        public string LienHolderZipCode { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "Please enter State.")]
        public string LienHolderStateCD { get; set; }

        public int? LienHolderStateID { get; set; }
        public string ZipCode { get; set; }
        public int? StateId { get; set; }
        public string City { get; set; }
        public string GaragingAddress1 { get; set; }
        public string GaragingAddress2 { get; set; }
        public string GaragingZipCode { get; set; }
        public string GaragingStateId { get; set; }
        public string GaragingCity { get; set; }
        public string QuoteType { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public bool? IsMonitoryDevice { get; set; } = false;
        public long? EndorsementParentVehicleId { get; set; }

        public string VehicleDetails { get; set; }

        public string IsLienHolder { get; set; }

        public bool IsFirstVehicle { get; set; }

        public string LienHolderName { get; set; }

        public int AgencyId { get; set; }

        public long UserId { get; set; }
    }

    public class AutoLienHolders
    {
        public int? AutoLienHolderID { get; set; }
        public string StateCD { get; set; }
        public int? StateID { get; set; }
        public string CommercialName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string QuoteType { get; set; }
    }

}
