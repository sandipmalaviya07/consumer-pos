﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class SignalRConnection
    {
        public string ConnectionID { get; set; }
        public long UserID { get; set; }
    }


    public static class ConnectionList
    {
        public static List<SignalRConnection> MyConnectionList = new List<SignalRConnection>();
    }
}
