﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class PrintQuoteModel
    {
        public long QuoteId { get; set; }

        public string MultiVehicleDisocunt { get; set; }

        public string InsuredName { get; set; }

        public string CustomerContactNo { get; set; }

        public string CustomerEmailId { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string StateCode { get; set; }

        public string WorkLossBenefitsDiscount { get; set; }

        public string SeniorDriverDiscount { get; set; }

        public string PaidInFullDiscount { get; set; }

        public string ProofofPriorDiscount { get; set; }

        public string HomeownerDiscount { get; set; }

        public string DriverDefensiveDiscount { get; set; }

        public string AgentName { get; set; }

        public string AgentNumber { get; set; }

        public string AgentAddress1 { get; set; }

        public string AgentAddress2 { get; set; }

        public string AgentCity { get; set; }

        public string AgentZipCode { get; set; }

        public string AgentStateCode { get; set; }

        public string ExcludeDriver { get; set; }

        public DateTime CurrentDate { get; set; }

        public List<RateHistoryPlanModel> RateHistoryDetails { get; set; }
        public List<DisplayCoverageModel> DisplayCoverageDetailsWithVehicle { get; set; }
        public List<DisplayCoverageModel> DisplayCoverageDetailsWithoutVehicle { get; set; }

        public AutoQuoteCovrageDetailsModel AutoQuoteCovrageDetails { get; set; }

        public QuotePlanDetailModel QuotePlanDetail { get; set; }

        public List<AutoVehicleDetailModel> AutoVehicleDetailModels { get; set; }
    }

    public class DisplayCoverageModel
    {
        public string DisplayCoverageName { get; set; }

        public string DisplayCoverageCode { get; set; }

        public string Value1 { get; set; }

        public string Value2 { get; set; }
        public string VehicleName { get; set; }
        public long VehicleId { get; set; }
    }

    public class RateHistoryPlanModel
    {
        public string DownPayment { get; set; }

        public string MonthlyPayment { get; set; }

        public DateTime InstallmentDueDate { get; set; }

        public string Premium { get; set; }

        public string Description { get; set; }

        public decimal NSDFee { get; set; }

        public int NumberOfPayment { get; set; }

        public decimal InstallmenetFee { get; set; }

        public decimal PGProcessingFees { get; set; }

        public decimal OtherFee { get; set; }

        public decimal ApplicationFee { get; set; }

        public int PolicyTerm { get; set; }
    }

    public class AutoQuoteCovrageDetailsModel
    {
        public string BodilyInjuryPerPerson { get; set; }

        public string BodilyInjuryPerAcc { get; set; }

        public string LossOfUse { get; set; }

        public string PropertyDamage { get; set; }

        public string MedicalPayments { get; set; }

        public string LimitedPropertyDamage { get; set; }

        public string UninsuredMotorist { get; set; }

        public string UninsuredBodilyInjuryPerPerson { get; set; }

        public string UninsuredBodilyInjuryPerAcc { get; set; }

        public string UninsuredBodilyInjuryDeductible { get; set; }

        public string UninsuredPropertyDamage { get; set; }

        public string UninsuredPropertyDamageDeductible { get; set; }

        public string UnderinsuredBodilyInjuryPerPerson { get; set; }

        public string UnderinsuredBodilyInjuryPerAcc { get; set; }

        public string UnderinsuredPropertyDamage { get; set; }

        public string PersonalInjuryProtection { get; set; }

        public string PersonalInjuryProtectionDetails { get; set; }

        public string PersonalInjuryProtectionDed { get; set; }

        public string AdditionalersonalInjuryProtection { get; set; }

        public string PropertyProtectionInsurance { get; set; }
        public string AnnualMileage { get; set; }
        public string Tort { get; set; }
        public string AccidentalDeath { get; set; }
        public string PunitiveDamageExclusion { get; set; }
        public string Veh1ComprehensiveDed { get; set; }

        public string Veh1BI { get; set; }
        public string Veh2BI { get; set; }
        public string Veh3BI { get; set; }
        public string Veh4BI { get; set; }
        public string Veh1PD { get; set; }
        public string Veh2PD { get; set; }
        public string Veh3PD { get; set; }
        public string Veh4PD { get; set; }
        public string Veh1PIP { get; set; }
        public string Veh2PIP { get; set; }
        public string Veh3PIP { get; set; }
        public string Veh4PIP { get; set; }
        public string Veh1PPI { get; set; }
        public string Veh2PPI { get; set; }
        public string Veh3PPI { get; set; }
        public string Veh4PPI { get; set; }
        public string Veh1UMBI { get; set; }
        public string Veh2UMBI { get; set; }
        public string Veh3UMBI { get; set; }
        public string Veh4UMBI { get; set; }
        public string Veh1UMPD { get; set; }
        public string Veh2UMPD { get; set; }
        public string Veh3UMPD { get; set; }
        public string Veh4UMPD { get; set; }
        public string Veh1UMPDD { get; set; }
        public string Veh2UMPDD { get; set; }
        public string Veh3UMPDD { get; set; }
        public string Veh4UMPDD { get; set; }
        public string Veh1MEDP { get; set; }
        public string Veh2MEDP { get; set; }
        public string Veh3MEDP { get; set; }
        public string Veh4MEDP { get; set; }
        public string Veh1Coll { get; set; }
        public string Veh2Coll { get; set; }
        public string Veh3Coll { get; set; }
        public string Veh4Coll { get; set; }
        public string Veh1Comp { get; set; }
        public string Veh2Comp { get; set; }
        public string Veh3Comp { get; set; }
        public string Veh4Comp { get; set; }
        public string Veh2ComprehensiveDed { get; set; }
        public string Veh3ComprehensiveDed { get; set; }
        public string Veh4ComprehensiveDed { get; set; }
        public string Veh1CollisionDed { get; set; }
        public string Veh2CollisionDed { get; set; }
        public string Veh3CollisionDed { get; set; }
        public string Veh4CollisionDed { get; set; }
        public string Veh1TowingLimit { get; set; }
        public string Veh2TowingLimit { get; set; }
        public string Veh3TowingLimit { get; set; }
        public string Veh4TowingLimit { get; set; }
        public string Veh1RentalLimit { get; set; }
        public string Veh2RentalLimit { get; set; }
        public string Veh3RentalLimit { get; set; }
        public string Veh41RentalLimit { get; set; }
        public string Veh1LOS { get; set; }
        public string Veh1LOSDed { get; set; }
        public string Veh2LOS { get; set; }
        public string Veh2LOSDed { get; set; }
        public string Veh3LOS { get; set; }
        public string Veh3LOSDed { get; set; }
        public string Veh4LOS { get; set; }
        public string Veh4LOSDed { get; set; }
        public string Veh1SAF { get; set; }
        public string Veh2SAF { get; set; }
        public string Veh3SAF { get; set; }
        public string Veh4SAF { get; set; }
        public string Veh1MCCA { get; set; }
        public string Veh2MCCA { get; set; }
        public string Veh3MCCA { get; set; }
        public string Veh4MCCA { get; set; }
        public string Veh1LPD { get; set; }
        public string Veh2LPD { get; set; }
        public string Veh3LPD { get; set; }
        public string Veh4LPD { get; set; }
        public string Driver1 { get; set; }
        public string Driver2 { get; set; }
        public string Driver3 { get; set; }
        public string Driver4 { get; set; }
        public string InstallmentFees { get; set; }
        public string Veh1CollisionDedType { get; set; }
        public string Veh2CollisionDedType { get; set; }
        public string Veh3CollisionDedType { get; set; }
        public string Veh4CollisionDedType { get; set; }
        public string Vehicle1VIN { get; set; }
        public int Vehicle1Year { get; set; }
        public string Vehicle1Make { get; set; }
        public string Vehicle1Model { get; set; }

        public string Vehicle2VIN { get; set; }
        public int Vehicle2Year { get; set; }
        public string Vehicle2Make { get; set; }
        public string Vehicle2Model { get; set; }

        public string Vehicle3VIN { get; set; }
        public int Vehicle3Year { get; set; }
        public string Vehicle3Make { get; set; }
        public string Vehicle3Model { get; set; }

        public string Vehicle4VIN { get; set; }
        public int Vehicle4Year { get; set; }
        public string Vehicle4Make { get; set; }
        public string Vehicle4Model { get; set; }
    }


    public class QuotePlanDetailModel
    {
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompany { get; set; }
        public string Description { get; set; }
        public int NumberOfPayments { get; set; }
        public decimal CarierDownpaymentAmount { get; set; }
        public decimal DownPaymentAmount { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal MonthlyPremium { get; set; }
        public string BridgeUrl { get; set; }
        public decimal OtherFeesAmount { get; set; }
        public long? BrowserTypeId { get; set; }
        public DateTime? InstallmentDueDate { get; set; }
        public decimal NsdAmount { get; set; }
        public decimal CCCharge { get; set; }
    }


    public class AutoVehicleDetailModel
    {
        public int Id { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SubModel { get; set; }
        public string VehFullName { get; set; }
        public int AnnualMileage { get; set; }
        public bool IsOwned { get; set; }
        public string PrimaryUse { get; set; }
        public string ComprehensiveDed { get; set; }
        public string ComprehensiveDedVal2 { get; set; }
        public string CollisionDed { get; set; }
        public string CollisionDedVal2 { get; set; }
        public string TowingLimit { get; set; }
        public string RentalLimit { get; set; }
        public decimal? CarPurchaseCost { get; set; }
        public DateTime PurchaseDate { get; set; }
        public bool IsMonitoryDevice { get; set; }
        public string NumCylinders { get; set; }
        public string LicensePlateNumber { get; set; }
        public int PrimaryDriverId { get; set; }
        public bool ParkedAtMainAddress { get; set; }
        public string LoseOfUse { get; set; }
        public string LoseOfUseVal2 { get; set; }
        public string CollissionLevel { get; set; }
    }

    public class StateCoverageModel
    {
        public string CoverageName { get; set; }
        public string CoverageCode { get; set; }
        public string Value { get; set; }
        public bool IsVehicleCoverage { get; set; }
    }



}
