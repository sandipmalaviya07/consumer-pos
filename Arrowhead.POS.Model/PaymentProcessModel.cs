﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model.CCPaymentRequestModel
{
    public class PaymentProcessRequestModel
    {
        public long QuoteID { get; set; }
        public long ReceiptID { get; set; }
        public decimal NSDAmount { get; set; }
        public decimal CarrierAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public string CustomerName { get; set; }
        public string Token { get; set; }
        public string CurrencyCode { get; set; }
        public string ReturnURL { get; set; }

        //Details for credit card
        public string CCHolderName { get; set; }
        public string CCNumber { get; set; }
        public string CVVNumber { get; set; }
        public int CCExpMonth { get; set; }
        public int CCExpYear { get; set; }

        //Details for ACH
        public string BankRoutingNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string CheckType { get; set; }
        public string BankName { get; set; }

        public string Description { get; set; }       
        public Dictionary<string, string> MetaData { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerAddress1 { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustomerZipcode { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }

        // Agency 
        public string AgencyName { get; set; }
    }
    public class PaymentProcessResponseModel
    {
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string TransactionID { get; set; }        
        public string TransactionType { get; set; }
        public string AuthCode { get; set; }
        public string CustomerId { get; set; }
        public string BankAccountId { get; set; }
        public string ResponseString { get; set; }
        public double TransactionAmount { get; set; }
    }

    public class PaymentProcessRefundRequestModel
    {
        public long QuoteID { get; set; }
        public string TransactionID { get; set; }
        public decimal Amount { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
    }

    public class PaymentProcessRefundResponseModel
    {
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string TransactionID { get; set; }
        public string CustomerId { get; set; }
    }
   
}
