﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class CustomerDocumentModel
    {
        public Guid CustomerDocumentId { get; set; }
        public long CustomerId { get; set; }
        public long? QuoteId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentTypeName { get; set; }
        public string FileType { get; set; }
        public string FileTypeName { get; set; }
        public string PolicyNumber { get; set; }
        public string DocumentName { get; set; }
        public string DocPath { get; set; }
        public string EntityCode { get; set; }
        public long? EntityId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedById { get; set; }
        public string EnvelopeCurrentStatus { get; set; }
        public int OrderNo { get; set; }
        public bool IsChecked { get; set; } = false;
        public string DocumentDetailname { get; set; }
        public string DocumentDetail { get; set; }
        public string QuoteType { get; set; }
    }
    public class CustomerDocsModel
    {
        public Guid CustomerDocumentId { get; set; }
    }
    public class CustomerDocumentSelectedModel
    {
        public long QuoteId { get; set; }
        public List<CustomerDocsModel> customerDocumentModels { get; set; }
    }

    public class CurrentEnvelopeStatus
    {
        public string EnvelopeStatus { get; set; }
        public long QuoteId { get; set; }
    }

    public class SelectedDocumentModel
    {
        public long QuoteId { get; set; }
        public string EmailId { get; set; }
        public string EmailSubject { get; set; }
        public List<CustomerDocumentModel> selectedDocument { get; set; }
    }

    public class DocumentUrlModel
    {
        public string DoscumentUrl { get; set; }
    }
}
