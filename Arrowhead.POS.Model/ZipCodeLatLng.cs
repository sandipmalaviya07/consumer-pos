//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZipCodeLatLng
    {
        public int ZipCode { get; set; }
        public string StateCd { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
