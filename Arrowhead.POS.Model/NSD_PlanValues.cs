//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class NSD_PlanValues
    {
        public int ID { get; set; }
        public int FKNSDPlanDetailsId { get; set; }
        public string Key1 { get; set; }
        public string Key2 { get; set; }
        public decimal Value1 { get; set; }
        public decimal Value2 { get; set; }
        public bool IsEndorsement { get; set; }
        public decimal NetPremiumValue { get; set; }
        public decimal RFCost { get; set; }
        public decimal AgentGross { get; set; }
        public Nullable<int> FkAgencyId { get; set; }
    
        public virtual NSD_PlanDetails NSD_PlanDetails { get; set; }
        public virtual Agency Agency { get; set; }
    }
}
