﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class NewPolicyModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string AgencyName { get; set; }
        public string AgencyPhone { get; set; }
        public string State { get; set; }
        public List<SchedulePaymentModel> SchedulePaymentModel { get; set; }
        public List<VehicleIDCardModel> VehicleIDCardModel { get; set; }
    }


    public class SchedulePaymentModel
    {
        public DateTime DueDate { get; set; }
        public decimal MonthlyPayments { get; set; }
        public int NumberOfPayments { get; set; }
    }


    public class VehicleIDCardModel
    {

        public long AutoVehicleId { get; set; }
        public long QuoteId { get; set; }
        public int? VehicleOrderId { get; set; }
        public string Vin { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
    }
}
