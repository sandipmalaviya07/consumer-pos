﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public int OrderNo { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Description { get; set; }
        public string FormControlCode { get; set; }
        public string UnAcceptableAnswer { get; set; }
        public bool IsAddionalQuestion { get; set; }
        public string AdditionalQuestion { get; set; }
        public string DefaultSelectedAnswer { get; set; }
        public bool IsQuestion { get; set; }
        public string Code { get; set; }
        public int FkParentId { get; set; }
        public List<QuestionModel> ChildQuestionModel { get; set; }
    }
    public class QuestionnaireAnswerModel
    {
        public long QuoteId { get; set; }
        public int StateId { get; set; }
        public Questionnaire Questionnaire { get; set; }
        public QuestionnaireAnswer QuestionnaireAnswer { get; set; }
    }
}
