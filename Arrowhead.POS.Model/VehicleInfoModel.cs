﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class VINDLValidate
    {
        public QuoteInfoModel _quoteInfo { get; set; }
        public List<VehicleInfoModel> _vehicleInfo { get; set; }
        public List<DriverInfoModel> _DriverInfo { get; set; }
    }
    public class VehicleInfoModel
    {
        public long QuoteID { get; set; }
        public long VehicleID { get; set; }
        public string AntiLock { get; set; }
        public string BodyType { get; set; }
        public string FuelType { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public int ModelGroupCode { get; set; }
        public int ModelCode { get; set; }
        public string PassSeatRestraint { get; set; }
        public string AirBags { get; set; }
        public string AntiTheft { get; set; }
        public string TruckSize { get; set; }
        public int UniqueSymCode { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public bool FrontWD { get; set; }
        public int MSRP { get; set; }
        public int NumOfCyl { get; set; }
        public int NumOfDoors { get; set; }
        public string VehicleType { get; set; }
        public bool IsVINVerified { get; set; }
        // public bool IsDriverLicenseVerified { get; set; }
    }

    public class DriverInfoModel
    {
        public long QuoteID { get; set; }
        public long DriverID { get; set; }
        public DateTime? DOB { get; set; }
        public int? DriverOrderID { get; set; }
        public long MaritalStatusID { get; set; }
        public long RelationCDId { get; set; }
        public int StateID { get; set; }
        public string StateCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DriverLicense { get; set; }
        public bool isExcluded { get; set; }
        public bool IsDriverLicenseVerified { get; set; }
        public bool IsSpouseContain { get; set; }
        public int ViolationPoints { get; set; }
    }

    public class QuoteInfoModel
    {
        public long QuoteId { get; set; }
        public bool IsGarageAddressExist { get; set;  }
        public bool IsZipCodeExist { get; set; }
        public bool IsStateExist { get; set; }
        public bool IsCityExist { get; set; }
        public bool IsMailingAddressExist { get; set; }
        public bool IsMailingZipCodeExist { get; set; }
        public bool IsMailingStateExist { get; set; }
        public bool IsMailingCityExist { get; set; }
        public bool IsVehicleExist { get; set; }
        public bool IsDriverExist { get; set; }
        public bool IsSpamContact { get; set; }
        public string SpamContactNo { get; set; }
    }
}
