﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class VerifyLienholderModel
    {
        public long QuoteId { get; set; }
        public long VehicleId { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public bool IsLeinComCollVerified { get; set; }
         
    }
}
