﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class CarrierViewModel
    {
        public DateTime PaymentDate { get; set; }
        public decimal Sweep { get; set; }
        public decimal CreditCard { get; set; }
        public decimal EFT { get; set; }
        public decimal TotalPayment { get; set; }
    }
}
