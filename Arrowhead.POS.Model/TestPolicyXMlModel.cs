﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Arrowhead.POS.Model
{
    public class TestPolicyXMlModel
    {
        public string RequestID { get; set; }

        [AllowHtml]
        public string PolicyXMl { get; set; }

        public string RequestTypeCode { get; set; }

        public string AccountHolderName { get; set; }
        public string AccountNumber     { get; set; }
        public string RoutingNumber     { get; set; }
        public string BankName          { get; set; }
        public string AccountTypeId     { get; set; }
    }

    public class TestAgencyModel
    {
        public string pc { get; set; }
    }

    public class TestProgramModel
    {
        public int ProducerCode { get; set; }
        public string ZipCode { get; set; }
    }
}
