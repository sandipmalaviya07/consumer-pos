﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class NsdPlanModel
    {
        public int NsdPlanId { get; set; }
        public string NsdCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class NSDPlanDetailsRequestModel
    {
        public int PlanId { get; set; }
        public long QuoteId { get; set; }
        public int NoOfCar { get; set; }
        public int NoOfMonth { get; set; }
    }
    public class NSDPlanDetailsModel
    {
        public string PlanNo { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public string JsonDescription { get; set; }
        public List<NSDPlanValuesModel> NSDPlanValues { get; set; }
    }
    public class NSDPlanValuesModel
    {
        public int FKPlanDetailsId { get; set; }
        public string Key1 { get; set; }
        public string Key2 { get; set; }
        public decimal Value1 { get; set; }
        public decimal Value2 { get; set; }
    }
}
