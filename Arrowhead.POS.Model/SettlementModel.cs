﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class SettlementModel
    {
        public DateTime PaymentDate { get; set; }        
        public decimal Sweep { get; set; }
        public decimal CreditCard { get; set; }
        public decimal EFT { get; set; }
        public decimal TotalPayment { get; set; }        
    }

    public class SettlementHistoryModel
    {
        public DateTime PaymentDate { get; set; }
        public decimal Sweep { get; set; }
        public decimal CreditCard { get; set; }
        public decimal EFT { get; set; }
        public decimal TotalPayment { get; set; }
        public bool isSettled { get; set; }

        // History NSD Model
        public long QuoteId { get; set; }
        public long ReceiptId { get; set; }
        public string PlanNo { get; set; }
        public string TransactionToken { get; set; }
        public decimal TotalAmount  { get; set; }
        public decimal RFCost { get; set; }
        public decimal AgencyCost { get; set; }
        public string PaymentType { get; set; }
        public string CustomerName { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
    }

    public class AutoSettlementModel
    {
        public List<SettlementModel> _SettlementModel { get; set; }
        public List<NSDSettlementModel> _NSDSettlementModel { get; set; }
    }

    public class NSDSettlementModel
    {
        public string Title { get; set; }
        public decimal Sweep { get; set; }
        public decimal CreditCard { get; set; }
        public decimal EFT { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class AgencyTotalAmount
    {
        public int AgencyId { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class SettlementUpdateModel
    {
        public string Note { get; set; }
        public string TransactionId { get; set; }
        public bool IsSuccess { get; set; }
        public DateTime? PaymentDate { get; set; } 
        public int AgencyId { get; set; }
    }

    public class ACHModel
    {
        public long QuoteId { get; set; }
        public long ReceiptId { get; set; }
        public decimal TotalAmount { get; set; }
        public string CustomerName { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public string Status { get; set; }
    }

    public class NSDPayoutModel
    {
        public DateTime date { get; set; }
        public string AgencyName { get; set; }
        public long ReceiptId { get; set; }
        public int NSDSoldCount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal RFCost { get; set; }
        public decimal AgencyCost { get; set; }
        public decimal TotalPayOut { get; set; }
        public string PaymentMethod { get; set; }
        public long QuoteId { get; set; }
        public string CustomerName { get; set; }
        public string AgencyPhone { get; set; }
        public string ContractNo { get; set; }
          

        public int AgencyId { get; set; }
    }

    public class PaymentGatewayHistoryModel
    {
        public string PaymentDate { get; set; }
        public string SettlementDate { get; set; }
        public string TotalAmount { get; set; }
        public string TransactionId { get; set; }
        public string TransactionStatus { get; set; }
        public string CustomerName { get; set; }
        public string PaymentType { get; set; }
        public long QuoteId { get; set; }
    }

}
