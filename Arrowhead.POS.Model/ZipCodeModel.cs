﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class ZipCodeModel
    {
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
    }
}
