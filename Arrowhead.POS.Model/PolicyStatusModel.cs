﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class PolicyStatusModel
    {
        public long QuoteID { get; set; }
        public string PolicyStatusCode { get; set; }
        public string PolicyNumber { get; set; }
        public string Description { get; set; }
        public int PolicyOrderNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public long CustomerId { get; set; }
    }
}
