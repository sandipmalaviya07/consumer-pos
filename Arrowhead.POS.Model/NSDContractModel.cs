﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class NSDContractModel
    {
        public long QuoteId { get; set; }
        public string AgencyId { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
    }

    public class NSDDailySales
    {
        public int TotalSales { get; set; }
        public double TotalGrossSales { get; set; }
        public double ACHDraftAmount { get; set; }
        public DateTime DraftDate { get; set; }
    }
}
