﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Arrowhead.POS.Model
{
    public class ApplicantModel
    {
        public long QuoteId { get; set; }
        public long CustomerId { get; set; }
        public long UserId { get; set; }

        [Required(ErrorMessage = "Please enter first name")]
        [StringLength(100)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }


        [Required(ErrorMessage = "Please enter last name")]
        [StringLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter address 1")]
        [RegularExpression(@"^[a-zA-Z0-9.\s]+$", ErrorMessage = "Address1 letters and numbers only")]
        [StringLength(100)]
        [Display(Name = "Address1")]
        public string Address1 { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9#\s]+$", ErrorMessage = "Apt, Suite letters and numbers only")]
        public string Address2 { get; set; }
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter zip code")]
        [StringLength(5, ErrorMessage = "Must be at least 5 characters long.")]
        [Remote("IsStateRateAvailable", "RemoteValidation", ErrorMessage = "Unable to rate this state.", HttpMethod = "POST")]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Please enter email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [StringLength(100)]
        [Display(Name = "Email")]
        public string EmailId { get; set; }

        [Remote("IsSpamContactNo","RemoteValidation",HttpMethod ="POST",ErrorMessage = "Invalid Phone Number")]
        [Required(ErrorMessage = "Please enter contact no")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        [StringLength(100)]
        [Display(Name = "Contact")]
        public string ContactNo { get; set; }

        public int StateId { get; set; }
        public string StateAbbr { get; set; }



        [Remote("ValidateDate", "RemoteValidation", HttpMethod = "POST", ErrorMessage = "Invalid Date")]
        [Display(Name = "Effective date")]
        public DateTime? EffectiveDate { get; set; }

        [Required(ErrorMessage = "Please enter source")]
        public long SourceId { get; set; }

        public DateTime ExpirationDate { get; set; }

        //public int? InsuranceCompanyId { get; set; }
        //public string InsuranceCompanyName { get; set; }
        public int InsuranceTypeId { get; set; }
        //public string InsuranceTypeName { get; set; }
        public int OfficeId { get; set; }
        //public long? ParentId { get; set; }
        //public long? PolicyHistoryId { get; set; }
        //public string PolicyNumber { get; set; }
        public long PolicyStatusId { get; set; }
        public int PolicyTerm { get; set; }
        //public string OtherPhoneNo { get; set; }
        //public string FaxNumber { get; set; }
        //public long QuoteTypeId { get; set; }
        //public string QuoteTypeCode { get; set; }
        //public string Country { get; set; }
        //public string AgentName { get; set; }
        //public bool IsPaid { get; set; }

        public int? ClaimFree { get; set; }
        public int? ContinuousCoverage { get; set; }


        public string BIPriorCoverage { get; set; }

        [Required(ErrorMessage = "Please enter expiration date.")]
        [Remote("PriorExpirationDate", "RemoteValidation", HttpMethod = "POST", ErrorMessage = "Due to a lapse in coverage the selected discount is not applicable.")]
        public DateTime? PriorExpirationDate { get; set; }

        [Required(ErrorMessage = "Please select lapse.")]
        public int? DaysLapes { get; set; }

        [Required(ErrorMessage = "Please enter carrier.")]
        public int? InsuranceCompanyId { get; set; }
        public int? NumberOfRenewal { get; set; }
        public short? MonthsInPriorPolicy { get; set; }


        public string PolicyNumber { get; set; }
        public bool IsAgentOfRecord { get; set; }
        public bool IspriorPolicy { get; set; }
        public bool IsHomeOwnersDiscount { get; set; }
        public bool IsGoodStudentDiscount { get; set; }
        public bool IsPaperLessDiscount { get; set; }
        public bool IsApplicantAddressChecked { get; set; }
        public string InsuranceCompanyName { get; set; }



        public string SourceName { get; set; }
        // public long AssumeCreditId { get; set; }
        public DateTime? CreatedDate { get; set; }
        //public decimal TotalDueAmount { get; set; }
        // public decimal? NSDPayAmount { get; set; }
        // public string AssumeCreditCardName { get; set; }
        //   public long? InitiatedById { get; set; }
        public string PolicyStatusCd { get; set; }

        public bool IsDuplicate { get; set; }

        [Required(ErrorMessage = "Please enter address 1")]
        [RegularExpression(@"^[a-zA-Z0-9.\s]+$", ErrorMessage = "Address1 not allow # symbol.")]
        [StringLength(100)]
        [Display(Name = "Address1")]
        public string MailingAddress1 { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9#\s]+$", ErrorMessage = "Address2 not allow # symbol.")]
        [Display(Name = "Address2")]
        public string MailingAddress2 { get; set; }

        [Required(ErrorMessage = "Please enter zip code")]
        [StringLength(5, ErrorMessage = "Must be at least 5 characters long.")]
        [Remote("IsStateRateAvailable", "RemoteValidation", ErrorMessage = "Unable to rate this state.", HttpMethod = "POST")]
        [Display(Name = "Mailing ZipCode")]
        public string MailingZipCode { get; set; }

        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingCountry { get; set; }


        public bool isAddressVerified { get; set; }

        public long AgencyId { get; set; }

        public long? FkHomeTypeId { get; set; }
        //  public int CurrentStepOrderNo { get; set; }
    }

    public class QuoteModel
    {
        public long QuoteId { get; set; }
        public long CustomerId { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FullName { get; set; }


        public string LastName { get; set; }


        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string City { get; set; }

        public string ZipCode { get; set; }


        public string EmailId { get; set; }

        public string ContactNo { get; set; }

        public int StateId { get; set; }
        public string StateAbbr { get; set; }
        public string StateName { get; set; }


        public DateTime? EffectiveDate { get; set; }

        public decimal SR22Fee { get; set; }


        public long SourceId { get; set; }

        public DateTime ExpirationDate { get; set; }
        // Public Property CusrrentStepId As Integer
        // Public Property CurrentStepOrderId As Integer
        public int? InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public int InsuranceTypeId { get; set; }
        public string InsuranceTypeName { get; set; }
        public int OfficeId { get; set; }
        public long? ParentId { get; set; }
        public long? PolicyHistoryId { get; set; }
        public string PolicyNumber { get; set; }
        public long PolicyStatusId { get; set; }
        public int PolicyTerm { get; set; }
        public string OtherPhoneNo { get; set; }
        public string FaxNumber { get; set; }
        public long QuoteTypeId { get; set; }
        public string QuoteTypeCode { get; set; }
        public string Country { get; set; }
        public string AgentName { get; set; }
        public bool IsPaid { get; set; }

        public bool IsPaymentComplete { get; set; }
        public bool IsDocusignComplete { get; set; }


        public string SourceName { get; set; }
        public long AssumeCreditId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public decimal TotalDueAmount { get; set; }
        public decimal? NSDPayAmount { get; set; }
        public string AssumeCreditCardName { get; set; }
        public long? InitiatedById { get; set; }
        public string PolicyStatusCd { get; set; }

        public bool IsDuplicate { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingZipCode { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingCountry { get; set; }

        public int CurrentStepOrderNo { get; set; }

        public bool IsProofOfPrior { get; set; }
        public bool IsBridgeQuote { get; set; }
        public string AgencyProducerCode { get; set; }
        public string PaymentGatewayCd { get; set; }
        public string PolicyMsgDesc { get; set; }
    }

    public class QuoteSourceModel
    {
        public long ID { get; set; }
        public string Source { get; set; }
    }

    [Serializable]
    public class TabModel
    {
        public long QuoteID { get; set; }
        public string FullName { get; set; }
        public bool IsOpenPolicy { get; set; }
    }


    [Serializable]
    public class AgencyModel
    {
        public string AgencyName { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string FaxNo { get; set; }

        //public string FirstName { get; set; }
        //public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter zip code")]
        [RegularExpression(@"^\d{5}(-\d{4})?$", ErrorMessage = "Invalid Zip Code")]
        [Display(Name ="Zip Code")]
        public string ZipCode { get; set; }

        public string AgencyCode { get; set; }

        [Required(ErrorMessage = "Please enter email address")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name ="Email Address")]
        public string AgencyEmailId { get; set; }

        [Required(ErrorMessage ="Please enter the Contact Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        [StringLength(100)]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = "Please enter the Address1")]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string AgencyAddress { get; set; }
        public long AgencyId { get; set; }
        public int OfficeId { get; set; }
        public long QuoteId { get; set; }

        [Required(ErrorMessage = "Please select the State")]
        public int StateId { get; set; }

        public bool IsSetupCompleted { get; set; }

        [Required(ErrorMessage = "Please enter the city")]
        public string City { get; set; }

        [Required(ErrorMessage ="Please enter the PIN")]
        [DataType(DataType.Password)]
        public string PIN { get; set; }

        [Required(ErrorMessage = "Please enter the confirm PIN")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("PIN")]
        public string ConfirmPIN { get; set; }

        public bool IsNSDAvailable { get; set; }
        public int ProducerCode { get; set; }
        public string StateAbbr { get; set; }

        public long UserId { get; set; }

        public string UserFirstName { get; set; }
        public string UserLastname { get; set; }

        public bool IsError { get; set; } = true;

        public string IsErrorMessage { get; set; }

        public string PolicyXMLStep { get; set; }

        [Display(Name ="NSD Plan")]
        public int? NSDPlanId { get; set; }

        public bool IsAgencyNSDAvailable { get; set; }
    }



}
