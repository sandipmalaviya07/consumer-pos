﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class PaymentResponseModel
    {
        public long QuoteId { get; set; }
        public string ResponseFile { get; set; }
        public bool IsSuccess { get; set; }
        public string MessageDescription { get; set; }
        public string CompanyQuoteNumber { get; set; }
    }
}
