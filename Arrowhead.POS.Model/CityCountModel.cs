﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class CityCountModel
    {
        public string ColumnName { get; set; }
        public int Count { get; set; }
    }

    public class TotalSoldPolicy
    {
        public int PolicyCount { get; set; }
        public string MonthName { get; set; }
    }


    public class PolicyAndQuoteSummaryResult
    {
        public int SummaryPolicyCount { get; set; }
        public string SummaryName { get; set; }
    }

    public class CloseRatioModel
    {
        public decimal Percentage { get; set; }
    }

    public class QuoteZipCodeModel
    {
        public string title { get; set; }
        public int QuoteCount { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}
