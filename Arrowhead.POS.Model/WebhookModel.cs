﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class WebhookModel
    {
        public class Self
        {
            public string href { get; set; }
        }

        public class Links
        {
            public Self self { get; set; }
        }

        public class RootObject
        {
            public Links _links { get; set; }
            public string webhookId { get; set; }
            public string name { get; set; }
            public List<string> eventTypes { get; set; }
            public string status { get; set; }
            public string url { get; set; }
        }
    }

    public class WebhookMainModel
    {
        [JsonProperty("ChildrenTokens", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string data { get; set; }

        public List<string> ChildrenTokens { get; set; }
    }

    public class WebhookRespmodel
    {
        public class Payload
        {
            public int responseCode { get; set; }
            public string avsResponse { get; set; }
            public double authAmount { get; set; }
            public string merchantReferenceId { get; set; }
            public string entityName { get; set; }
            public string id { get; set; }
        }

        public class RootObject
        {
            public string notificationId { get; set; }
            public string eventType { get; set; }
            public DateTime eventDate { get; set; }
            public string webhookId { get; set; }
            public Payload payload { get; set; }
        }
    }
}
