﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    [Serializable]
    public class AutoViolationModel
    {
        public int AutoViolationId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string TypeOfViolation { get; set; }
        public int Point1 { get; set; }
        public int Point2 { get; set; }
        public string ViolationFor { get; set; }
        public int ValidityInMonth { get; set; }
        public bool EnableCoverages { get; set; }
    }

    [Serializable]
    public class AutoDriverViolationModel
    {
        public long AutoDriverViolationId { get; set; }
        public long DriverId { get; set; }
        public int AutoViolationId { get; set; }
        public string ViolationCode { get; set; }
        public string ViolationName { get; set; }
        public string ViolationType { get; set; }
        public string DriverLicenseNumber { get; set; }
        public DateTime ViolationDate { get; set; }
        public int Points { get; set; }
        public string Note { get; set; }
        public int? BIamount { get; set; }
        public int? PDamount { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedById { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long ModifiedById { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string DriverName { get; set; }

        public long DriverOrderId { get; set; }

        public bool IsMVR { get; set; }

        public bool IsNoHit { get; set; }

        public int AgencyId { get; set; }

        public long QuoteId { get; set; }

        public long UserId { get; set; }

    }
}
