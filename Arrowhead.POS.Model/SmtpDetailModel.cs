﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class SmtpDetailModel
    {
        public long OfficeSmtpDetailId { get; set; }
        public int? AgencyId { get; set; }
        public int? OfficeId { get; set; }
        public int? SmtpTypeId { get; set; }
        public string OfficeName { get; set; }
        public string Name { get; set; }
        public string EmailFrom { get; set; }
        public string EmailReplyTo { get; set; }
        public bool IsBodyHtml { get; set; }
        public byte Priority { get; set; }
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool IsUseDefaultCredentials { get; set; }
        public bool IsEnableSsl { get; set; }
        public bool IsDefault { get; set; }
    }
}
