﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class UserPreferenceModel
    {
        public long QuoteId       { get; set; }
        public int AgencyId       { get; set; }
        public int UserId         { get; set; }
        public long PropertyId    { get; set; }
        public bool IsLeftTabOpen { get; set; }
    }
}
