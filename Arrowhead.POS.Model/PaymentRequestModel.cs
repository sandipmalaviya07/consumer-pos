﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Arrowhead.POS.Model
{
    public class PaymentRequestModel
    {
        public long QuoteID { get; set; }
        public long? ReceiptID { get; set; }
        public string ExpiryDate { get; set; }



        [Required(ErrorMessage = "Please Enter CVV.")]
        [Display(Name = "CVV")]
        public string CVV { get; set; }
        public string PaymentMethodCode { get; set; }
        public int PaymentGatewayProviderId { get; set; }
        public bool IsPaymentCompleted { get; set; }
        public bool IsDocusignCompleted { get; set; }
        public string Account { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address1 { get; set; }
        public string Addres2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string TempToken { get; set; }
        public string PGToken { get; set; }
        public string PgTransactionId { get; set; }
        public long CustomerId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? EffectiveDate { get; set; }


        [Required(ErrorMessage = "Please Enter CardNumber.")]
        [Display(Name = "CardNumber")]
        public string CardNumber { get; set; }
        public string PaymentMonth { get; set; }
        public string PaymentYear { get; set; }


        [Required(ErrorMessage = "Please Enter BankName.")]
        [Display(Name = "BankName")]
        public string BankName { get; set; }


        [Required(ErrorMessage = "Please Enter RoutingNumber.")]
        [Display(Name = "RoutingNumber")]
        public string RoutingNumber { get; set; }
        public string AccountHolderName { get; set; }


        [Required(ErrorMessage = "Please Enter AccountNumber.")]
        [Display(Name = "AccountNumber")]
        public string AccountNumber { get; set; }

        public int CheckNumber { get; set; }
        public string BankAddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedById { get; set; }
        public long AccountTypeId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long ModifiedById { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal CCFees { get; set; }
        public decimal TotalNSD { get; set; }
        public decimal TotalCarrier { get; set; }
        public long AgencyId { get; set; }
        public string PGCustomerId { get; set; }
        public string ActivePGCode { get; set; }
        public bool IsPaymentCaptured { get; set; }
        public string AuthCode { get; set; }

        public bool IsSignInOffice { get; set; }

        public long UserId { get; set; }
    }


    public class PaymentDetailModel
    {
        public long QuoteID { get; set; }
        public string PaymentMethodCd { get; set; }
        public string PGTransactionId { get; set; }
        public long ReceiptId { get; set; }
        public string PaymentStatus { get; set; }
    }
    public class PaymentGateWayModel
    {
        public int PaymentgateWayID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string EndPointURL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MerchantID { get; set; }
        public bool isLive { get; set; }
    }

    public class PaymentTransactionModel
    {
        public string PgTransactionID { get; set; }
        public long QuoteId { get; set; }
        public int PGID { get; set; }
        public string PaymentMethod { get; set; }
        public decimal ChargeAmount { get; set; }
    }


}
