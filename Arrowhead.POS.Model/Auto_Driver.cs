//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Auto_Driver
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Auto_Driver()
        {
            this.Auto_DriverViolation = new HashSet<Auto_DriverViolation>();
        }
    
        public long ID { get; set; }
        public long FkQuoteID { get; set; }
        public int FkStateID { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LicenseNo { get; set; }
        public string Gender { get; set; }
        public string SocialSecurity { get; set; }
        public Nullable<int> DriverOrderID { get; set; }
        public long FkMaritalStatusID { get; set; }
        public Nullable<long> FkCreditScoreID { get; set; }
        public Nullable<long> FkDriverEducationID { get; set; }
        public Nullable<long> FkHomeTypeID { get; set; }
        public long FkRelationCdID { get; set; }
        public bool IsExcluded { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSR22 { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public long CreatedByID { get; set; }
        public Nullable<long> ModifiedByID { get; set; }
        public string Employer { get; set; }
        public Nullable<short> EmployerYears { get; set; }
        public Nullable<short> MilesToWork { get; set; }
        public Nullable<short> YearsLicensed { get; set; }
        public Nullable<long> LicenseStatus { get; set; }
        public Nullable<System.DateTime> DateFirstLicense { get; set; }
        public bool IsDefensiveDriverCourse { get; set; }
        public Nullable<System.DateTime> DrivertrainingDate { get; set; }
        public bool IsSR22A { get; set; }
        public Nullable<int> FkStateSR22Id { get; set; }
        public Nullable<int> FKStateSR22AId { get; set; }
        public string SR22Reason { get; set; }
        public bool IsWorkLossBenefit { get; set; }
        public bool IsSeniorDriver { get; set; }
        public Nullable<long> FkDriverOccupationID { get; set; }
        public int ViolationPoints { get; set; }
        public bool IsNoHit { get; set; }
        public Nullable<long> FkMilitaryTypeId { get; set; }
        public Nullable<long> FkWarehouseTypeId { get; set; }
        public bool IsGoodStudent { get; set; }
    
        public virtual Quote Quote { get; set; }
        public virtual State State { get; set; }
        public virtual GeneralStatusType GeneralStatusType { get; set; }
        public virtual GeneralStatusType GeneralStatusType1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Auto_DriverViolation> Auto_DriverViolation { get; set; }
        public virtual GeneralStatusType GeneralStatusType2 { get; set; }
        public virtual GeneralStatusType GeneralStatusType3 { get; set; }
        public virtual GeneralStatusType GeneralStatusType4 { get; set; }
    }
}
