﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class StateMinimumCoverageModel
    {
        public decimal CoverageValue1 { get; set; }
        public decimal CoverageValue2 { get; set; }
        public string CoverageCode { get; set; }

        public long CoverageId { get; set; }

        public int StateId { get; set; }

        public string StateCode { get; set; }
    }
}
