﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class OnBoardBankDetailModel
    {
        [Required(ErrorMessage = "Please enter Account holder name")]
        public string AccountHolderName { get; set; }

        [Required(ErrorMessage = "Please enter Account Number")]
        public string AccountNumber { get; set; }

        public string AccountType { get; set; }

        [Required(ErrorMessage = "Please enter Routing Number")]
        public string RoutingNumber { get; set; }

        [Required(ErrorMessage = "Please enter Bank Name")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "Please select Account Type")]
        [Display(Name = "AccountType")]
        public int AccountTypeId { get; set; }

        public bool IsSuccess { get; set; }
    }
}
