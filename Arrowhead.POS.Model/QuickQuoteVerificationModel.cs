﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class QuickQuoteVerificationModel
    {
        [Required(ErrorMessage = "Please enter zip code")]
        [StringLength(5, ErrorMessage = "Must be at least 5 characters long.")]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }
    }
}
