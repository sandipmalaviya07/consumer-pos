﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Arrowhead.POS.Model
{
    public class AutoModel
    {
        public QuoteModel QuoteModel { get; set; }
        public AutoRateDetailModel AutoRateDetail { get; set; }
        public AutoPriorInsuranceDetailModel AutoPriorInsurance { get; set; }
        public AutoQuoteDetailModel AutoQuoteDetail { get; set; }
        public List<AutoDriverModel> AutoQuoteDrivers { get; set; }
        public List<AutoVehicleModel> AutoQuoteVehicles { get; set; }
        public List<AutoQuoteCoverageModel> AutoQuoteCoverages { get; set; }
        public List<AutoQuoteNsdDetailModel> AutoQuoteNsds { get; set; }
        public bool IsQuoteCompleted { get; set; }
        public bool IsQuoteInitiated { get; set; }
    }

    public class AutoRateDetailModel
    {
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompany { get; set; }
        public string Description { get; set; }
        public int NumberOfPayments { get; set; }
        public decimal CarierDownpaymentAmount { get; set; }
        public decimal DownPaymentAmount { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal MonthlyPremium { get; set; }
        public string BridgeUrl { get; set; }
        public decimal OtherFeesAmount { get; set; }
        public long? BrowserTypeId { get; set; }
        public DateTime? InstallmentDueDate { get; set; }
        public decimal NsdAmount { get; set; }
        public decimal CCCharge { get; set; }
    }





    public class AutoPriorInsuranceDetailModel
    {
        public int? ClaimFree { get; set; }
        public int? ContinuousCoverage { get; set; }
        public string BIPriorCoverage { get; set; }
        public DateTime? PriorExpirationDate { get; set; }
        public int? DaysLapes { get; set; }
        public int? InsuranceCompanyId { get; set; }
        public int? NumberOfRenewal { get; set; }
        public short? MonthsInPriorPolicy { get; set; }
        public string PolicyNumber { get; set; }
        public bool IsAgentOfRecord { get; set; } = false;
        public long QuoteId { get; set; }
        public bool IspriorPolicy { get; set; } = false;
        public bool IsHomeOwnersDiscount { get; set; }
        public bool IsGoodStudentDiscount { get; set; }
        public bool IsPaperLessDiscount { get; set; }
        public string InsuranceCompanyName { get; set; }
    }

    public class AutoQuoteDetailModel
    {
        public long QuoteId { get; set; }
        public long RateHistoryId { get; set; }
        public DateTime? RateDate { get; set; }
        public bool IsHomeOwnersDiscount { get; set; }
        public bool IsGoodStudentDiscount { get; set; }
        public bool IsPaperLessDiscount { get; set; }
        public bool IsNonOwner { get; set; } = false;
        public Guid RateTransactionID { get; set; }
        public long? ResidenceType { get; set; }
        public int? MonthsInResidence { get; set; }
    }


    public class AutoQuoteCoverageModel
    {

        public int CoverageId { get; set; }
        public string CoverageName { get; set; }
        public string CoverageCode { get; set; }
        public string DisplaySelectedValue { get; set; }
        public long? VehicleId { get; set; }
        public bool IsDuplicate { get; set; } = false;
        public string VehicleName { get; set; }
        public string GroupName { get; set; }
    }



    public class AutoQuoteNsdDetailModel

    {
        public long AutoQuoteNsdPlanId { get; set; }
        public long QuoteId { get; set; }
        public int NsdPlanId { get; set; }
        public string NsdPlanName { get; set; }
        public string NsdPlanCode { get; set; }
        public string NsdPlanDesription { get; set; }
        public bool IsNsdPlanDeleted { get; set; }
        public string PlanId { get; set; }
        public decimal? Price { get; set; } = 0;
        public bool IsEnable { get; set; } = false;
        public string NSDPlanJsonDescription { get; set; }
        public int NSDPaymentAmount { get; set; }

    }


    public class AutoPolicyHolderModel
    {
        public AutoQuoteDetailModel AutoQuoteDetail { get; set; }
        public QuoteModel QuoteModel { get; set; }
    }

    [XmlRoot(ElementName = "NSD")]
    public class NSDCancelContract
    {
        [XmlElement(ElementName = "Password")]
        public string Password { get; set; }

        [XmlElement(ElementName = "PolicyInfo")]
        public NSDCancelPolicyInfo policyInfo { get; set; }
    }

    [XmlRoot(ElementName = "PolicyInfo")]
    public class NSDCancelPolicyInfo
    {
        [XmlElement(ElementName = "Member_No")]
        public string Member_No { get; set; }
        [XmlElement(ElementName = "Master_Id")]
        public string Master_Id { get; set; }

        [XmlElement(ElementName = "Cancel_Date")]
        public DateTime Cancel_Date { get; set; }
    }
}
