﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public  class YearModel
    {
        public string Text { get; set; }
        public string Value { get; set; }

    }
}
