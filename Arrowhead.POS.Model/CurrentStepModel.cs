﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class CurrentStepModel
    {
        public long QuoteId { get; set; }
        public int DriverCount { get; set; }
        public int VehicleCount { get; set; }
        public long CusrrentStepId { get; set; }
        public int CurrentStepOrderId { get; set; }
        public string CurrentStepName { get; set; }
        public string CurrentStepCode { get; set; }
        public long AgencyId { get; set; }

    }
}
