﻿namespace Arrowhead.POS.Model
{
    public class AuditHistoryModel
    {
        public char Operation { get; set; }
        public string ModuleID { get; set; }
        public string Remarks { get; set; }
        public long? QuoteID { get; set; }
        public bool IsError { get; set; } = false;
        public string EntityID { get; set; }
        public int AgencyID { get; set; }
        public long CustomerId { get; set; }
    }
}
