﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    
    public  class LogModel
    {
        [Required(ErrorMessage ="Please enter QuoteID.")]
        public long QuoteID { get; set; }
        public List<LogDetailModel> LogDetailModel { get; set; }
    }

    public class LogDetailModel
    {
        public long QuoteId { get; set; }
        public string CallType { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime? ResponseDate { get; set; }
        public string RequestXML { get; set; }
        public string ResponseXML { get; set; }
    }
}
