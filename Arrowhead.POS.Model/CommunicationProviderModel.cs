﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class CommunicationProviderModel
    {
        public int AgencyId { get; set; }
        public int OfficeID { get; set; }
        public long CommunicationProvideModeID { get; set; }
        public string OfficeName { get; set; }
        public string PhoneNumber { get; set; }
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Name { get; set; }
    }
    public class customPager
    {
        public List<CommunicationProviderModel> list { get; set; }
        public int totalRows { get; set; }
    }
   
    public class CommunicationAuthorizationModel
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class CommunicationCredentialModel
    {
        public string Code { get; set; }

        public string Value { get; set; }

        public bool IsLiveMode { get; set; }

        public string ServiceProvider { get; set; }
    }
}
