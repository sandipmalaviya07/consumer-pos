﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class TransactionVerificationModel
    {
        public string Heading { get; set; }
        public List<VerificationModel> VerificationModel { get; set; }
    }

    public class VerificationModel
    {
        public string FKQuoteID { get; set; }
        public string PGTransID { get; set; }
        public string CustomerName { get; set; }
        public decimal PGChargeValue { get; set; }
        public decimal ActualChargeValue { get; set; }
        public string BgColor { get; set; }
        public decimal diff { get; set; }
    }
}
