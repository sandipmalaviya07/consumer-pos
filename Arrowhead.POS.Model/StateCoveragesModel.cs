﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class StateCoveragesModel
    {
        public int StateId { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public int CoverageId { get; set; }
        public string CoverageName { get; set; }
        public string CoverageFormControl { get; set; }
        public List<CoveragesFormControlValues> CoveragesFormControlValues { get; set; } = new List<CoveragesFormControlValues>();
        public string Selected { get; set; }
        public bool IsVehicleCoverage { get; set; }
        public long? VehicleID { get; set; }
        public string VehicleTitle { get; set; }
        public int? VehicleOrderID { get; set; } = 0;
        public bool IsDuplicate { get; set; } = false;
        public bool IsEnable { get; set; } = true;
        public int? ParentId { get; set; }
        public string QuoteSelectedValue { get; set; }
        public string GroupName { get; set; }
        public bool IsEdit { get; set; }
    }

    public class VehicleCoveragesModel
    {
        public int StateId { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public int CoverageId { get; set; }
        public string CoverageName { get; set; }
        public string CoverageFormControl { get; set; }
        public List<CoveragesFormControlValues> CoveragesFormControlValues { get; set; } = new List<CoveragesFormControlValues>();
        public string Selected { get; set; }
        public bool IsVehicleCoverage { get; set; }
        public int? VehicleID { get; set; }
    }

    public class CoveragesFormControlValues
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }
    }

    public class CoverageModel
    {
        public int CoverageId { get; set; }
        public string CoverageCode { get; set; }
        public string ShortName { get; set; }
        public string CoverageName { get; set; }
        public string Description { get; set; }
        public int OrderId { get; set; }
        public string ControlType { get; set; }
        public bool IsVehicleCoverage { get; set; }
        public int ParentId { get; set; }
        public string GroupName { get; set; }
        public List<StateCoverageValues> StateCoverageValues { get; set; } = new List<StateCoverageValues>();
    }

    public class StateCoverageValues
    {
        public long StateCoverageId { get; set; }
        public int StateId { get; set; }
        public long? VehicleID { get; set; }
        public int CoverageId { get; set; }
        public int OrderId { get; set; }
        public decimal Amount { get; set; }
        public decimal Value1 { get; set; }
        public decimal Value2 { get; set; }
        public string DisplayTextValue1 { get; set; }
        public string DisplayTextValue2 { get; set; }
        public bool IsSelected { get; set; }
        public string Code { get; set; }
    }



}
