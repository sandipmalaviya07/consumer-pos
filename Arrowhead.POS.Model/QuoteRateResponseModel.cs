﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class QuoteRateResponseModel
    {
        public long QuoteId { get; set; }
        public long RateId { get; set; }
        public string RateTransactionID { get; set; }
        public string CarrierQuoteID { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public decimal DownPayment { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal NSDFees { get; set; }
        public bool IsErrorInNSD { get; set; }
        public bool IsError { get; set; } = false;
        public string ErrorResponse { get; set; }
        public string BridgeURL { get; set; }
        public string Description { get; set; }
        public bool Subtablevisible { get; set; } = false;
        public decimal PayableDownPayment { get; set; }
        public decimal TotalPremium { get; set; }
        public int PolicyTerm { get; set; }
        public int NoOfInstallement { get; set; }
        public decimal? InstallmentFee { get; set; }
        public string PayPlanId { get; set; }
        public DateTime? InstallmentDueDt { get; set; }
        public int DueDiffrence { get; set; }
        public decimal CCCharge { get; set; }
        public List<StateCoverageValues> StateCoverage { get; set; }
        public List<QuoteRateResponseDetailsModel> _quoteDetailsResponseAllDetails { get; set; }
    }

    public class QuoteRateResponseDetailsModel
    {
        public long RateId { get; set; }
        public long QuoteID { get; set; }
        public decimal DownPayment { get; set; }
        public string RateTransactionID { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal NSDFees { get; set; }
        public bool IsError { get; set; } = false;
        public string ErrorResponse { get; set; }
        public string BridgeURL { get; set; }
        public string Description { get; set; }
        public decimal PayableDownPayment { get; set; }
        public decimal TotalPremium { get; set; }
    }
}
