﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please Enter Producer Code.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Producer Code must be numeric.")]
        [Display(Name = "ProducerCode")]
        public string ProducerCode { get; set; }

        [Required(ErrorMessage = "Please Enter PIN Code.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

   }
}
