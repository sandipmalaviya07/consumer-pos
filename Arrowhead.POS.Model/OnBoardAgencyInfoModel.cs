﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class OnBoardAgencyInfoModel
    {
        public string AgencyName { get; set; }
        public string Heading    { get; set; }
        public string ProducerCode { get; set; }
        public List<AgencyInfoModel> AgencyInfoModel { get; set; }
    }

    public class AgencyInfoModel
    {
        public string labelText { get; set; }
        public string oldTextVal { get; set; }
        public string newTextVal { get; set; }
    }

}
