﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class SendPaperWorkModel
    {
        public long QuoteId { get; set; }
        public string EmailId { get; set; }
        public string EmailSubject { get; set; }
        public string DocumentName { get; set; }
        public string SendTypeCode { get; set; }
        public string ContactNo { get; set; }
    }
}
