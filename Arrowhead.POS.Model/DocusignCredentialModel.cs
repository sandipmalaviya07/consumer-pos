﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class DocusignCredentialModel
    {
        public int? AgencyId { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public bool IsLiveMode { get; set; }
        public long EnvelopeProviderModeId { get; set; }
    }

    public class DocusignAuthorizationModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IntegratorKey { get; set; }
        public string AccountId { get; set; }
        public string Url { get; set; }
        public long EnvelopeProviderModeId { get; set; }
    }

    public class DocusignUrlModel
    {
        public string ESignReturnUrl { get; set; }
        public string EnvelopeId { get; set; }
        public int AutoEnvelopeId { get; set; }
    }

    public class DocusignQuoteModel
    {
        public string UserName { get; set; }
        public string UserEmailId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmailId { get; set; }
        public string QuoteTypeCd { get; set; }
        public long QuoteId { get; set; }
        public int StateID { get; set; }
    }

    public class DocusignDetailModel
    {
        public string SigningUrl { get; set; }
        public DateTime? SignDate { get; set; }
    }

    public class AutoEnvelopesModel
    {
        public long AutoEnvelopeId { get; set; }
        public long CustomerId { get; set; }
        public long? QuoteId { get; set; }
        public string EnvelopeId { get; set; }
        public long EnvelopeStatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? CustomerSignDate { get; set; }
        public DateTime? UserSignDate { get; set; }
        public long? UserId { get; set; }
        public long? CreatedById { get; set; }
        public long? ModifiedById { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? EntityTypeId { get; set; }
        public int? EnvelopeProviderId { get; set; }
        public long EnvelopeProviderModeId { get; set; }
    }

    public class OnlineBiddingTemplateModel
    {
        public int InsuranceCompanyID { get; set; }
        public string EnvelopType { get; set; }
        public string TemplateID { get; set; }
        public int Order { get; set; }
        public string BindingTypeCode { get; set; }
    }

    public class SendCustomerToSignModel
    {
        public long quoteID { get; set; }
        public string MessageTypeCode { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string URL { get; set; }
        public string PolicyNumber { get; set; }
    }
}
