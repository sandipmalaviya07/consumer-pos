﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class AddressValidationModel
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public bool isAddressValidated { get; set; }

        public bool isPOBoxError { get; set; }
    }


    public class AddressResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string FullAddress { get; set; }
        
    }
}
