﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Foolproof;

namespace Arrowhead.POS.Model
{
    public class AutoDriverModel
    {
        public long AutoDriverId { get; set; }
        public long QuoteId { get; set; }
        public System.DateTime DOBStartDate { get; set; }

        [Required(ErrorMessage = "Please select State")]
        [Display(Name = "State")]
        public int StateId { get; set; }

        public string StateAbbr { get; set; }

        [Required(ErrorMessage = "Please enter Date of Birth")]
        [LessThanOrEqualTo("DOBStartDate")]
        [Display(Name = "Date of Birth")]
        public DateTime? Dob { get; set; }

        [Required(ErrorMessage = "Please enter First Name")]
        [StringLength(75)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        public string Socialsecurity { get; set; }

        [Required(ErrorMessage = "Please enter Last Name")]
        [StringLength(75)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        //[Required(ErrorMessage = "Please enter Driver License No")]
        [Remote("IsStateLicenseNo", "RemoteValidation", AdditionalFields = "StateId", ErrorMessage = "License number is not valid.", HttpMethod = "POST")]
        [StringLength(50)]
        [Display(Name = "Driver License No")]
        public string LicenseNo { get; set; }

        [Required(ErrorMessage = "Please select Gender")]
        [StringLength(30)]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        public long MaritalStatusID { get; set; }
        public string MaritalStatusName { get; set; }

        [Required(ErrorMessage = "Please select Marital Status")]
        [StringLength(30)]
        [Display(Name = "Marital Status")]
        public string MaritalStatusCode { get; set; }

        public int? DriverOrderID { get; set; }
        public long? CreditScoreID { get; set; }
        public long? DriverEducationID { get; set; }
        public long? HomeTypeID { get; set; }

        [Required(ErrorMessage = "Please select Driver Relation")]
        [Display(Name = "This Driver Is")]
        public long RelationCDId { get; set; }

        public string RelationName { get; set; }
        public bool IsHomeOwner { get; set; }

        [Display(Name = "State Filing")]
        public bool Sr22 { get; set; }

        [Display(Name = "Sr22A")]
        public bool IsSr22A { get; set; }
       
        public int? StateSR22Id { get; set; }
        public int? StateSR22AId { get; set; }
        public string SR22Reason { get; set; }

        [Display(Name = "Work Loss Benefit")]
        public bool IsWorkLossBenefit { get; set; }

        [Display(Name = "Senior Driver")]
        public bool IsSeniorDriver { get; set; }

        [Display(Name = "Occupation")]
        public long? DriverOccupationID { get; set; }

        public bool IsFirstDriver { get; set; }


        [Display(Name = "Excluded")]
        public bool IsExcluded { get; set; }
        public string Employer { get; set; }
        public short? EmployerYears { get; set; }
        public short? MilesToWork { get; set; }
        public short? YearsLicensed { get; set; }
        public long? LicenseStatus { get; set; }
        public string LicenseStatusName { get; set; }
        public string DriverOccupationName { get; set; }
        public DateTime? DateFirstLicense { get; set; }
        public bool IsDefensiveDriverCourse { get; set; }
        public bool IsGoodStudent { get; set; }
        public bool IsMilitaryDiscount { get; set; }
        public bool IsWareHouseDiscount { get; set; }
        public DateTime? DrivertrainingDate { get; set; }
        public string QuoteType { get; set; }
        public long? EndorsementParentDriverId { get; set; }
        public string LicenseState { get; set; }
        public string DriverName { get; set; }
        public int AgencyId { get; set; }
        public bool IsNoHit { get; set; }

        public long UserId { get; set; }
        public int ViolationPoints { get; set; }
        public List<AutoDriverViolationModel> AutoDriverViolations { get; set; }

        public long FkMilitaryTypeId { get; set; }
        public long FkWarehouseTypeId { get; set; }

        
    }

    public class DriverRelationModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class DropdownModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
