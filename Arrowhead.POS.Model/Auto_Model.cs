﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Model;
namespace Arrowhead.POS.Model
{
    public class Auto_Model
    {
        public QuoteModel QuoteModel { get; set; }
        public AutoRateDetailModel AutoRateDetail { get; set; }
        public AutoPriorInsuranceDetailModel AutoPriorInsurance { get; set; }
        public AutoQuoteDetailModel AutoQuoteDetail { get; set; }
        public List<AutoDriverModel> AutoQuoteDrivers { get; set; }
        public List<AutoVehicleModel> AutoQuoteVehicles { get; set; }
        public List<AutoQuoteCoverageModel> AutoQuoteCoverages { get; set; }
        public List<AutoQuoteNsdDetailModel> AutoQuoteNsds { get; set; }
    }
}
