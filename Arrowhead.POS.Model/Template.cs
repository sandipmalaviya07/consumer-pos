//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Template
    {
        public long ID { get; set; }
        public Nullable<int> FKOfficeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string SMSTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public bool IsSMSActive { get; set; }
        public bool IsEmailActive { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedByID { get; set; }
        public string DynamicField { get; set; }
        public string EmailSubjectLine { get; set; }
        public Nullable<int> FKTemplateTypeId { get; set; }
    
        public virtual Office Office { get; set; }
        public virtual TemplateType TemplateType { get; set; }
    }
}
