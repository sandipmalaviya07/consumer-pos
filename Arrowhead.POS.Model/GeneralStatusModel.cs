﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class GeneralStatusModel
    {
        public string Code { get; set; }
        public string Type { get; set; }
        public string GeneralStatusName { get; set; }
        public string GeneralStatusTypeName { get; set; }
        public long FkGeneralStatusId { get; set; }
        public int OrderNo { get; set; }
    }
}
