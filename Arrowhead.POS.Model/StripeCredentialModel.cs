﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public  class StripeCredentialModel
    {
        public string PublishableKey { get; set; }
        public string APIKey { get; set; }
        public string StripeAccountId { get; set; }
        public decimal CCFees { get; set; }
    }
}
