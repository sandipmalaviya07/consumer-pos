﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class AgencyOnBoardActivationModel
    {
        public string ActivationURL { get; set; }
    }
}
