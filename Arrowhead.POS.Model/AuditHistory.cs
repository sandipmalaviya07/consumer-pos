//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditHistory
    {
        public long ID { get; set; }
        public Nullable<long> FKUserId { get; set; }
        public string Operation { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string FKModuleId { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public string IPAddress { get; set; }
        public string DeviceId { get; set; }
        public string UserAgent { get; set; }
        public string Remark { get; set; }
        public string EntityId { get; set; }
        public Nullable<long> FKParentId { get; set; }
        public Nullable<System.Guid> GroupTransactionId { get; set; }
        public Nullable<long> FKQuoteId { get; set; }
        public Nullable<long> FKCustomerId { get; set; }
        public Nullable<int> FKAgencyID { get; set; }
        public bool IsError { get; set; }
    
        public virtual Agency Agency { get; set; }
    }
}
