﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Model;
using System.Xml.Serialization;

namespace Arrowhead.POS.Model
{
    public class AutoQuoteNsdPlansModel
    {
        public long AutoQuoteNsdPlanId { get; set; }
        public long QuoteId { get; set; }
        public int NsdPlanId { get; set; }
        public string NsdPlanName { get; set; }
        public string NsdPlanCode { get; set; }
        public string NsdPlanDesription { get; set; }
        public bool IsNsdPlanDeleted { get; set; }
        public string PlanId { get; set; }
        public decimal? Price { get; set; } = 0;
        public bool IsEnable { get; set; } = false;
        public string productNumber { get; set; }
        public string beneficiaryName { get; set; }
        public string beneficiaryRelation { get; set; }
        public long? fKDriverId { get; set; } = 0;
        public List<NSDPlanDetailsModel> nSDPlanDetailsModel { get; set; }

    }

    public class AutoQuoteNsdSummaryModel
    {
        public long QuoteId { get; set; }
        public long VehicleId { get; set; }
        public string Make { get; set; }
        public int Year { get; set; }
        public string Model { get; set; }

        public bool IsChecked { get; set; }
    }

    public class AutoNsdDetailModel
    {
        public List<AutoQuoteNsdPlansModel> AutoQuoteNsdPlans { get; set; }
        public List<AutoQuoteNsdSummaryModel> AutoQuoteNsdSummary { get; set; }
    }
    public class AutoNSDDetailsListMode
    {
        public int NSDPlanId { get; set; }
        public int NSDPlanDetailID { get; set; }
        public decimal? price { get; set; }
        public string PlanName { get; set; }
        public bool IsSelected { get; set; }
        public string PDFUrl { get; set; }
        public int NSDPlanValueId { get; set; }
    }


    [XmlRoot(ElementName = "NSD")]
    public class NSD
    {
        [XmlElement(ElementName = "Password")]
        public string Password { get; set; }
        [XmlElement(ElementName = "Action")]
        public string Action { get; set; }
        [XmlElement(ElementName = "Test")]
        public string Test { get; set; }
        [XmlElement(ElementName = "WorkingContract")]
        public WorkingContract workingContract { get; set; }
        [XmlElement(ElementName = "PolicyInfo")]
        public PolicyInfo policyInfo { get; set; }
        [XmlElement(ElementName = "VehicleInfo")]
        public List<VehicleInfo> vehicleInfo { get; set; }
        [XmlElement(ElementName = "ProductInfo")]
        public ProductInfo productInfo { get; set; }
        [XmlElement(ElementName = "BeneficiaryInfo")]
        public BeneficiaryInfo beneficiaryInfo { get; set; }


    }

    [XmlRoot(ElementName = "WorkingContract")]
    public class WorkingContract
    {
        [XmlElement(ElementName = "Member_No")]
        public string Member_No { get; set; }
        [XmlElement(ElementName = "Master_Id")]
        public string Master_Id { get; set; }
    }

    [XmlRoot(ElementName = "PolicyInfo")]
    public class PolicyInfo
    {
        [XmlElement(ElementName = "Ag_No")]
        public string Ag_No { get; set; }
        [XmlElement(ElementName = "Fname")]
        public string Fname { get; set; }
        [XmlElement(ElementName = "MI")]
        public string MI { get; set; }
        [XmlElement(ElementName = "Lname")]
        public string Lname { get; set; }
        [XmlElement(ElementName = "Address")]
        public string Address { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
        [XmlElement(ElementName = "Zip")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "Phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "Email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "Work_Phone")]
        public string Work_Phone { get; set; }
        [XmlElement(ElementName = "Payment_Type")]
        public string Payment_Type { get; set; }
    }


    [XmlRoot(ElementName = "VehicleInfo")]
    public class VehicleInfo
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Make")]
        public string Make { get; set; }
        [XmlElement(ElementName = "Model")]
        public string Model { get; set; }
        [XmlElement(ElementName = "VIN")]
        public string VIN { get; set; }
        [XmlElement(ElementName = "New_Or_Used")]
        public string New_Or_Used { get; set; }
        [XmlElement(ElementName = "Odometer")]
        public string Odometer { get; set; }
        [XmlElement(ElementName = "Vehicle_Price")]
        public string Vehicle_Price { get; set; }
        [XmlElement(ElementName = "Key_Remotes_Issued")]
        public string Key_Remotes_Issued { get; set; }
        [XmlElement(ElementName = "Tire_Size")]
        public string Tire_Size { get; set; }
        [XmlElement(ElementName = "Tire_Make")]
        public string Tire_Make { get; set; }
        [XmlElement(ElementName = "Lender_Name")]
        public string Lender_Name { get; set; }
        [XmlElement(ElementName = "Lender_Address")]
        public string Lender_Address { get; set; }
        [XmlElement(ElementName = "Lender_City")]
        public string Lender_City { get; set; }
        [XmlElement(ElementName = "Lender_State")]
        public string Lender_State { get; set; }
        [XmlElement(ElementName = "Lender_Zip")]
        public string Lender_Zip { get; set; }
        [XmlElement(ElementName = "Wheel_Count")]
        public string Wheel_Count { get; set; }
        [XmlElement(ElementName = "Vehicle_Type")]
        public string Vehicle_Type { get; set; }
        [XmlElement(ElementName = "Loan_Or_Lease")]
        public string Loan_Or_Lease { get; set; }
        [XmlElement(ElementName = "MSRP")]
        public string MSRP { get; set; }
        [XmlElement(ElementName = "Amt_Financed")]
        public string Amt_Financed { get; set; }
        [XmlElement(ElementName = "APR")]
        public string APR { get; set; }
        [XmlElement(ElementName = "Monthly_Payment")]
        public string Monthly_Payment { get; set; }
    }


    [XmlRoot(ElementName = "ProductInfo")]
    public class ProductInfo
    {
        [XmlElement(ElementName = "Pr_No")]
        public string Pr_No { get; set; }
        [XmlElement(ElementName = "Plan_No")]
        public string Plan_No { get; set; }
        [XmlElement(ElementName = "Eff_Dt")]
        public string Eff_Dt { get; set; }
        [XmlElement(ElementName = "Exp_Dt")]
        public string Exp_Dt { get; set; }
        [XmlElement(ElementName = "NetPremium")]
        public string NetPremium { get; set; }
        [XmlElement(ElementName = "GrossPremium")]
        public string GrossPremium { get; set; }
        [XmlElement(ElementName = "Trans_Dt")]
        public string Trans_Dt { get; set; }
    }


    [XmlRoot(ElementName = "BeneficiaryInfo")]
    public class BeneficiaryInfo
    {
        [XmlElement(ElementName = "Beneficiary_First_Name")]
        public string Beneficiary_First_Name { get; set; }
        [XmlElement(ElementName = "Beneficiary_Middle_Initial")]
        public string Beneficiary_Middle_Initial { get; set; }
        [XmlElement(ElementName = "Beneficiary_Last_Name")]
        public string Beneficiary_Last_Name { get; set; }
        [XmlElement(ElementName = "Beneficiary_Address")]
        public string Beneficiary_Address { get; set; }
        [XmlElement(ElementName = "Beneficiary_City")]
        public string Beneficiary_City { get; set; }
        [XmlElement(ElementName = "Beneficiary_State")]
        public string Beneficiary_State { get; set; }
        [XmlElement(ElementName = "Beneficiary_Zip")]
        public string Beneficiary_Zip { get; set; }
        [XmlElement(ElementName = "Beneficiary_Relation")]
        public string Beneficiary_Relation { get; set; }
    }
}
