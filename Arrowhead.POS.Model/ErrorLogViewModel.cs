﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class ErrorLogViewModel
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public long QuoteId { get; set; }

        public string Remark { get; set; }

        public bool IsError { get; set; }

        public string IPAddress { get; set; }

        public string Module { get; set; }

        public long CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string AgencyName { get; set; }

        public DateTime? TransactionDate { get; set; }

    }
}
