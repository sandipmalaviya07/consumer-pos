﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
   public class AgencyProducerCodeModel
    {
        public string ProducerCode { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string AgencyName { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
     
    }
}
