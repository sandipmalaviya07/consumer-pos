﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{
    public class MessageModel
    {
        public long? MessageId { get; set; }
        public long CustomerId { get; set; }
        public long UserId { get; set; }
        public string MessageFrom { get; set; }
        public string MessageTo { get; set; }
        public string Message { get; set; }
        public long? TemplateId { get; set; }
        public bool IsRead { get; set; }
        public bool IsSentByAgent { get; set; }
        public bool IsAutoResponse { get; set; }
        public long? OfficeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long TotalCount { get; set; } = 0;
        public string CustomerName { get; set; }
        public string CustomerContactNumber { get; set; }
        public string CustomerProfileName { get; set; }
        public string UserName { get; set; }
        public string UserProfileName { get; set; }
        public string MessageSID { get; set; }
        public bool IsMMS { get; set; }
        public string MediaUrls { get; set; }
        public int InComingMsgCount { get; set; } = 0;
     
        public bool IsSelected { get; set; } = false;
        public int MediaCount { get; set; } = 0;
        public long? QuoteId { get; set; }
        public bool IsOfferBetterRate { get; set; } = false;
    }


    public class TwilioAuthorizationModel
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ShortCode { get; set; }
    }
}
