﻿using Arrowhead.POS.Core.Paging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Model
{

    public class CarrierPayoutModel
    {
        public PagerList<SettlementDetailModel> SettlementDetail { get; set; }
        public SummaryModel SummaryDetail { get; set; }
    }

    public class SummaryModel
    {
        public decimal ArrowheadDeposite { get; set; }
        public decimal CreditCardFee { get; set; }
        public decimal NsdCreditCardFee { get; set; }
        public decimal CCCreditCardFee { get; set; }
        public decimal TotalCreditCard { get; set; }
        public decimal EcheckFee { get; set; }
        public decimal NsdEcheckFee { get; set; }
        public decimal TotalEcheck { get; set; }
        public decimal SweepFee { get; set; }
        public decimal NsdSweepFee { get; set; }
        public decimal TotalSweep { get; set; }
        public decimal NetPremium { get; set; }
        public decimal NSDFee { get; set; }
        public decimal CCFee { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalCount { get; set; }
    }
    public class SettlementDetailModel
    {
        public DateTime PaymentDate { get; set; }
        public string PolicyNumber { get; set; }
        public string Agency { get; set; }
        public string CustomerName { get; set; }
        public string PaymentType { get; set; }
        public decimal NetPremium { get; set; }

        public string ProducerCode { get; set; }

        public long QuoteId { get; set; }
        public long ReceiptId { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal NSDFee { get; set; }
        
        public decimal CCFee { get; set; }
        public decimal EcheckFee { get; set; }
        public decimal SweepFee { get; set; }
        public decimal CreditCardFee { get; set; }
        public decimal NsdCreditCardFee { get; set; }
        public decimal NsdEcheckFee { get; set; }
        public decimal NsdSweepFee { get; set; }
        public decimal CCCreditCardFee { get; set; }
        public decimal TotalSweep { get; set; }
        public decimal TotalEcheck { get; set; }
        public decimal TotalCreditCard { get; set; }
        public decimal ArrowheadDeposite { get; set; }
        public decimal RFCost { get; set; }
        
        public decimal AgencyCost { get; set; }
        
        
        public string EmailID { get; set; }
        public string ContactNo { get; set; }
        public string PlanNo { get; set;}
        public string TransactionToken { get; set; }
        
        public string PaymentStatus { get; set; }

        public string PolicySignedMediumType { get; set; }
    }
}
