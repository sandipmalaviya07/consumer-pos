//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Arrowhead.POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class TransactionVerification
    {
        public int ID { get; set; }
        public string PGTransID { get; set; }
        public long FKQuoteID { get; set; }
        public decimal PGChargeValue { get; set; }
        public decimal ActualChargeValue { get; set; }
        public Nullable<int> FKPaymentGatewayID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsSettled { get; set; }
        public Nullable<long> FKVerificationTypeID { get; set; }
        public string Description { get; set; }
    
        public virtual Quote Quote { get; set; }
        public virtual GeneralStatusType GeneralStatusType { get; set; }
    }
}
