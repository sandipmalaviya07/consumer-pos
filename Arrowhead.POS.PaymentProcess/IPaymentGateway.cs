﻿using Arrowhead.POS.Model.CCPaymentRequestModel;

namespace Arrowhead.POS.PaymentProcess
{
    public interface IPaymentGateway
    {
        PaymentProcessResponseModel CapturePayment(PaymentProcessRequestModel requestModel);
        PaymentProcessResponseModel RefundProcess(string chargeId,string methodType);
        PaymentProcessResponseModel CaptureACHPayment(PaymentProcessRequestModel requestModel);
        PaymentProcessResponseModel AuthorizeCCPayment(PaymentProcessRequestModel requestModel);
        PaymentProcessRefundResponseModel RefundCCPayment(PaymentProcessRefundRequestModel refundModel);
        PaymentProcessResponseModel VoidPayment(PaymentProcessRequestModel requestModel);
        PaymentProcessResponseModel GetTransactionDetails(string transactionID);
    }   
}
