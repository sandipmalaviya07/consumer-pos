﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using Arrowhead.POS.Service;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Arrowhead.POS.Core;
using log4net;

namespace Arrowhead.POS.PaymentProcess
{
    public class AuthorizeProcess : IPaymentGateway
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AuthorizeProcess));
        #region "CC Payment Methods"
        public PaymentProcessResponseModel AuthorizeCCPayment(PaymentProcessRequestModel requestModel)
        {
            Log.Debug("AuthorizeCCPayment process Start value : " + CreateJSON("Quote : " + requestModel.QuoteID + ", Amount : " + requestModel.TotalAmount + ", CustomerName: " + requestModel.CustomerName));

            PaymentServices paymentServices = new PaymentServices();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            AuthorizeModel.RootObject _rootObj = new AuthorizeModel.RootObject();

            try
            {
                var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();

                AuthorizeModel.CreateTransactionRequest _payRequest = new AuthorizeModel.CreateTransactionRequest
                {
                    merchantAuthentication = new AuthorizeModel.MerchantAuthentication()
                };

                _payRequest.merchantAuthentication.name = _getCredentials.UserName; // Set Merchant Authentication
                _payRequest.merchantAuthentication.transactionKey = _getCredentials.Password;


                _payRequest.transactionRequest = new AuthorizeModel.TransactionRequest
                {
                    transactionType = Enums.TransactionType.authOnlyTransaction.ToString(), //"authOnlyTransaction",
                    amount = requestModel.TotalAmount
                };

                // Set Transaction Details 
                _payRequest.refId = GeneralServices.GenerateAuthToken(20);

                // Set Credit Card Details 
                _payRequest.transactionRequest.payment = new AuthorizeModel.Payment
                {
                    creditCard = new AuthorizeModel.CreditCard()
                };
                _payRequest.transactionRequest.payment.creditCard.cardNumber = requestModel.CCNumber;
                _payRequest.transactionRequest.payment.creditCard.expirationDate = (ConvertTo.GetEstTimeNow().Year.ToString().Substring(0, 2) + requestModel.CCExpYear.ToString() + "-" + requestModel.CCExpMonth.ToString());
                _payRequest.transactionRequest.payment.creditCard.cardCode = requestModel.CVVNumber.ToString();


                // Set User Details 
                _payRequest.transactionRequest.billTo = new AuthorizeModel.BillTo
                {
                    firstName = requestModel.CustomerFirstName,
                    lastName = requestModel.CustomerLastName,
                    company = requestModel.AgencyName,
                    address = requestModel.CustomerAddress1 + "" + requestModel.CustomerAddress2,
                    city = requestModel.CustomerCity,
                    state = requestModel.CustomerState,
                    zip = requestModel.CustomerZipcode,
                    phoneNumber = requestModel.CustomerPhone
                };

                _payRequest.transactionRequest.customer = new AuthorizeModel.Customer
                {
                    id = requestModel.QuoteID.ToString(),
                    email = requestModel.CustomerEmail.ToString()
                };

                _payRequest.transactionRequest.userFields = new AuthorizeModel.UserFields
                {
                    userField = new List<AuthorizeModel.UserField>()
                };

                _payRequest.transactionRequest.userFields.userField.Add(new AuthorizeModel.UserField { name = "QuoteId", value = requestModel.QuoteID.ToString() });
                _payRequest.transactionRequest.userFields.userField.Add(new AuthorizeModel.UserField { name = "ApplicantName", value = requestModel.CustomerFirstName.ToString() + " " + requestModel.CustomerLastName.ToString() });
                _payRequest.transactionRequest.userFields.userField.Add(new AuthorizeModel.UserField { name = "NSDFee", value = requestModel.NSDAmount.ToString() });
                _payRequest.transactionRequest.userFields.userField.Add(new AuthorizeModel.UserField { name = "Amount", value = requestModel.TotalAmount.ToString() });


                // Set Payment Request Model into Root Obj
                _rootObj.createTransactionRequest = _payRequest;
                string requestJSON = CreateJSON(_rootObj);
                GetHttpCall(_getCredentials, _respModel, requestJSON);
            }
            catch (Exception ex)
            {
                Log.Error("AuthorizeCCPayment process exception : " + ex.Message.ToString());
                _respModel.Status = false;
                _respModel.StatusMessage = ex.Message.ToString();
                return _respModel;
                throw ex;
            }
            return _respModel;
            throw new NotImplementedException();
        }

        int count = 0;
        public PaymentProcessResponseModel CapturePayment(PaymentProcessRequestModel requestModel)
        {
            Log.Debug("CapturePayment process Start value : " + CreateJSON(requestModel));
            PaymentServices paymentServices = new PaymentServices();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            AuthorizeCaptureModel.AuthCaptureModel _rootObj = new AuthorizeCaptureModel.AuthCaptureModel();
            AuthorizeCaptureModel.CreateTransactionRequest _payRequest = new AuthorizeCaptureModel.CreateTransactionRequest();
            _payRequest.merchantAuthentication = new AuthorizeCaptureModel.MerchantAuthentication();

            try
            {
                Model.PaymentGateWayModel _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();

                // Set Merchant Authentication
                _payRequest.merchantAuthentication.name = _getCredentials.UserName;
                _payRequest.merchantAuthentication.transactionKey = _getCredentials.Password;

                // Set Transaction Details 
                _payRequest.transactionRequest = new AuthorizeCaptureModel.TransactionRequest
                {
                    transactionType = Enums.TransactionType.priorAuthCaptureTransaction.ToString(),//"priorAuthCaptureTransaction",
                    amount = requestModel.TotalAmount.ToString(),
                    refTransId = requestModel.Token
                };
                _payRequest.refId = GeneralServices.GenerateAuthToken(20);

                _rootObj.createTransactionRequest = _payRequest;
                string requestJSON = CreateJSON(_rootObj);

                GetHttpCall(_getCredentials, _respModel, requestJSON);

                while (count <= 3)
                {
                    var result = GetTransactionDetails(_respModel.TransactionID);
                    if (result.StatusMessage.ToUpper() == Enums.TransactionStatus.AUTHORIZEDPENDINGCAPTURED.ToString())
                    {
                        count++;
                        CapturePayment(requestModel);
                    }
                    else
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error("CapturePayment process error : " + ex.Message.ToString());
                _respModel.Status = false;
                _respModel.StatusMessage = ex.Message.ToString();
                return _respModel;
            }
            return _respModel;
        }

        public PaymentProcessRefundResponseModel RefundCCPayment(PaymentProcessRefundRequestModel refundModel)
        {
            Log.Debug("RefundCCPayment process Start value : " + CreateJSON(refundModel));
            PaymentServices paymentServices = new PaymentServices();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            AuthorizeRefundModel.RootObject _rootObj = new AuthorizeRefundModel.RootObject();
            AuthorizeRefundModel.CreateTransactionRequest _payRequest = new AuthorizeRefundModel.CreateTransactionRequest();
            try
            {
                _payRequest.merchantAuthentication = new AuthorizeRefundModel.MerchantAuthentication();
                var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();

                // Set Merchant Authentication
                _payRequest.merchantAuthentication.name = _getCredentials.UserName;
                _payRequest.merchantAuthentication.transactionKey = _getCredentials.Password;

                // Set Transaction Details 
                _payRequest.transactionRequest = new AuthorizeRefundModel.TransactionRequest();
                _payRequest.transactionRequest.transactionType = Enums.TransactionType.voidTransaction.ToString();//"voidTransaction";
                _payRequest.transactionRequest.amount = refundModel.Amount.ToString();
                _payRequest.refId = GeneralServices.GenerateAuthToken(20);

                // Set Credit Card Details 
                _payRequest.transactionRequest.payment = new AuthorizeRefundModel.Payment();
                _payRequest.transactionRequest.payment.creditCard = new AuthorizeRefundModel.CreditCard();
                _payRequest.transactionRequest.payment.creditCard.cardNumber = refundModel.CardNumber.Substring(refundModel.CardNumber.Length - 4); // pass last 4 digit of CCNumber
                _payRequest.transactionRequest.payment.creditCard.expirationDate = "XXXX"; // masked 
                _payRequest.transactionRequest.refTransId = refundModel.TransactionID;

                // Set Payment Request Model into Root Obj
                _rootObj.createTransactionRequest = _payRequest;
                string requestJSON = CreateJSON(_rootObj);
                GetHttpCall(_getCredentials, _respModel, requestJSON);
            }
            catch (Exception ex)
            {
                Log.Error("RefundCCPayment process error : " + ex.Message.ToString());
                throw;
            }
            throw new NotImplementedException();
        }
        #endregion

        #region "ACH Payment Method"
        public PaymentProcessResponseModel CaptureACHPayment(PaymentProcessRequestModel requestModel)
        {
            Log.Debug("CaptureACHPayment process Start value : " + CreateJSON(requestModel));
            PaymentServices paymentServices = new PaymentServices();
            var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            ACHPaymentProcess.RootObject _rootObj = new ACHPaymentProcess.RootObject();
            ACHPaymentProcess.CreateTransactionRequest _payRequest = new ACHPaymentProcess.CreateTransactionRequest();
            _payRequest.merchantAuthentication = new ACHPaymentProcess.MerchantAuthentication();

            try
            {
                // Set Merchant Authentication
                _payRequest.merchantAuthentication.name = _getCredentials.UserName;
                _payRequest.merchantAuthentication.transactionKey = _getCredentials.Password;

                // Set Transaction Details 
                _payRequest.transactionRequest = new ACHPaymentProcess.TransactionRequest
                {
                    transactionType = Enums.TransactionType.authCaptureTransaction.ToString(),//"authCaptureTransaction",
                    amount = requestModel.TotalAmount
                };
                _payRequest.refId = GeneralServices.GenerateAuthToken(20);


                // Set User Details 
                _payRequest.transactionRequest.billTo = new ACHPaymentProcess.BillTo
                {
                    firstName = requestModel.CustomerFirstName,
                    lastName = requestModel.CustomerLastName,
                    company = requestModel.AgencyName,
                    address = requestModel.CustomerAddress1 + "" + requestModel.CustomerAddress2,
                    city = requestModel.CustomerCity,
                    state = requestModel.CustomerState,
                    zip = requestModel.CustomerZipcode,
                    phoneNumber = requestModel.CustomerPhone
                };

                _payRequest.transactionRequest.customer = new ACHPaymentProcess.Customer
                {
                    id = requestModel.QuoteID.ToString(),
                    email = requestModel.CustomerEmail.ToString()
                };

                // Set Bank Details 
                _payRequest.transactionRequest.payment = new ACHPaymentProcess.Payment
                {
                    bankAccount = new ACHPaymentProcess.BankAccount()
                };
                _payRequest.transactionRequest.payment.bankAccount.accountType = requestModel.BankAccountType == null ? "checking" : requestModel.BankAccountType.ToLower();
                _payRequest.transactionRequest.payment.bankAccount.routingNumber = requestModel.BankRoutingNumber;
                _payRequest.transactionRequest.payment.bankAccount.accountNumber = requestModel.BankAccountNumber;
                _payRequest.transactionRequest.payment.bankAccount.nameOnAccount = requestModel.CustomerName;

                // Set _payRequest into Root Object
                _rootObj.createTransactionRequest = _payRequest;

                string requestJSON = CreateJSON(_rootObj);
                GetHttpCall(_getCredentials, _respModel, requestJSON);
            }
            catch (Exception ex)
            {
                Log.Debug("CaptureACHPayment process error : " + ex.Message.ToString());
                _respModel.Status = false;
                _respModel.StatusMessage = ex.Message.ToString();
                return _respModel;
                throw ex;
            }
            return _respModel;
            throw new NotImplementedException();
        }

        public PaymentProcessResponseModel RefundProcess(string chargeId, string methodType)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Transaction Method"
        public PaymentProcessResponseModel GetTransactionDetails(string transactionID)
        {
            PaymentServices paymentServices = new PaymentServices();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            PaymentTransactionProcess.AuthorizeTransactionModel.RootObject _rootObj = new PaymentTransactionProcess.AuthorizeTransactionModel.RootObject();
            PaymentTransactionProcess.AuthorizeTransactionModel.GetTransactionDetailsRequest _payRequest = new PaymentTransactionProcess.AuthorizeTransactionModel.GetTransactionDetailsRequest();
            try
            {
                _payRequest.merchantAuthentication = new PaymentTransactionProcess.AuthorizeTransactionModel.MerchantAuthentication();
                var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();

                // Set Merchant Authentication
                _payRequest.merchantAuthentication.name = _getCredentials.UserName;
                _payRequest.merchantAuthentication.transactionKey = _getCredentials.Password;

                // Set Ref Transaction ID for Cancel Transaction 
                _payRequest.transId = transactionID;

                // Set Payment Request Model into Root Obj
                _rootObj.getTransactionDetailsRequest = _payRequest;
                string requestJSON = CreateJSON(_rootObj);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_getCredentials.EndPointURL);
                byte[] bytes;
                bytes = Encoding.ASCII.GetBytes(requestJSON);
                request.ContentType = "application/json";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    var resultingMessage = (PaymentTransactionProcess.AuthorizeTransactionModel.ResponseRootObject)JsonConvert.DeserializeObject(responseStr, typeof(PaymentTransactionProcess.AuthorizeTransactionModel.ResponseRootObject));

                    switch (resultingMessage.messages.resultCode)
                    {
                        case "Ok":
                            _respModel.Status = true;
                            _respModel.StatusCode = "0";
                            _respModel.StatusMessage = resultingMessage.transaction.transactionStatus;
                            _respModel.TransactionID = resultingMessage.transaction.transId;
                            _respModel.TransactionType = resultingMessage.transaction.transactionType;
                            _respModel.TransactionAmount = resultingMessage.transaction.settleAmount;
                            break;
                        case "Error":
                            _respModel.StatusCode = resultingMessage.messages.message.First().code;
                            _respModel.StatusMessage = resultingMessage.messages.message.First().text;
                            _respModel.TransactionAmount = 0;
                            break;
                        default:
                            _respModel.Status = false;
                            _respModel.StatusCode = "404";
                            _respModel.TransactionAmount = 0;
                            //_respModel.StatusMessage = resultingMessage.Messages.message.;
                            break;
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _respModel;
            throw new NotImplementedException();
        }
        #endregion



        #region "Common Methods"

        public PaymentProcessResponseModel VoidPayment(PaymentProcessRequestModel requestModel)
        {
            PaymentServices paymentServices = new PaymentServices();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            AuthorizeVoidModel.RootObject _rootObj = new AuthorizeVoidModel.RootObject();
            AuthorizeVoidModel.CreateTransactionRequest _payRequest = new AuthorizeVoidModel.CreateTransactionRequest();
            try
            {
                _payRequest.merchantAuthentication = new AuthorizeVoidModel.MerchantAuthentication();
                var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();

                // Set Merchant Authentication
                _payRequest.merchantAuthentication.name = _getCredentials.UserName;
                _payRequest.merchantAuthentication.transactionKey = _getCredentials.Password;

                // Set Transaction Details 
                _payRequest.transactionRequest = new AuthorizeVoidModel.TransactionRequest();
                _payRequest.transactionRequest.transactionType = Enums.TransactionType.voidTransaction.ToString();//"voidTransaction";
                _payRequest.refId = GeneralServices.GenerateAuthToken(20);

                // Set Ref Transaction ID for Cancel Transaction 
                _payRequest.transactionRequest.refTransId = requestModel.Token;

                // Set Payment Request Model into Root Obj
                _rootObj.createTransactionRequest = _payRequest;
                string requestJSON = CreateJSON(_rootObj);
                GetHttpCall(_getCredentials, _respModel, requestJSON);
            }
            catch (Exception ex)
            {
                _respModel.Status = false;
                _respModel.StatusMessage = ex.Message.ToString();
                return _respModel;
                throw ex;
            }
            return _respModel;
            throw new NotImplementedException();
        }

        private static void GetHttpCall(Model.PaymentGateWayModel _getCredentials, PaymentProcessResponseModel _respModel, string requestJSON)
        {
            try
            {
                string pType, pAmount, pQuote;
                try
                {
                    var tempJsonData = Newtonsoft.Json.Linq.JObject.Parse(requestJSON);
                    pType = (string)tempJsonData["createTransactionRequest"]["transactionRequest"]["transactionType"];
                    pAmount = (string)tempJsonData["createTransactionRequest"]["transactionRequest"]["amount"];
                    pQuote = (string)tempJsonData["createTransactionRequest"]["transactionRequest"]["userFields"]["userField"][0]["value"];
                    Log.Debug("GetHttpCall process Request : " + "Type: " + pType + ", Amount: " + pAmount + ", Quote:" + pQuote);
                }
                catch
                {
                    Log.Debug("GetHttpCall process Request : " + requestJSON);
                }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_getCredentials.EndPointURL);
                byte[] bytes;
                bytes = Encoding.ASCII.GetBytes(requestJSON);
                request.ContentType = "application/json";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();

                    Log.Debug("GetHttpCall process response : " + responseStr);

                    var resultingMessage = (ACHPaymentProcess.ResponseRootObject)JsonConvert.DeserializeObject(responseStr, typeof(ACHPaymentProcess.ResponseRootObject));

                    if (resultingMessage.TransactionResponse != null && resultingMessage.TransactionResponse.errors == null)
                    {
                        switch (resultingMessage.Messages.resultCode)
                        {
                            case "Ok":
                                _respModel.Status = true;
                                _respModel.StatusCode = "200";
                                _respModel.StatusMessage = "Success";
                                _respModel.AuthCode = resultingMessage.TransactionResponse.authCode;
                                _respModel.TransactionID = resultingMessage.TransactionResponse.transId;
                                _respModel.ResponseString = responseStr;
                                break;
                            case "Error":
                                _respModel.Status = false;
                                _respModel.StatusCode = resultingMessage.Messages.message.First().code;
                                _respModel.ResponseString = responseStr;
                                _respModel.StatusMessage = resultingMessage.TransactionResponse.errors.First().errorText == string.Empty ? resultingMessage.Messages.message.First().text : resultingMessage.TransactionResponse.errors.First().errorText;
                                break;
                            default:
                                _respModel.Status = false;
                                _respModel.StatusCode = "404";
                                _respModel.ResponseString = responseStr;
                                //_respModel.StatusMessage = resultingMessage.Messages.message.;
                                break;
                        }
                    }
                    else if (resultingMessage.TransactionResponse.errors != null)
                    {
                        _respModel.Status = false;
                        _respModel.StatusCode = resultingMessage.TransactionResponse.errors.Select(x => x.errorCode).FirstOrDefault();
                        _respModel.ResponseString = responseStr;
                        _respModel.StatusMessage = resultingMessage.TransactionResponse.errors.Select(x => x.errorText).FirstOrDefault();
                    }
                    else if (resultingMessage.Messages != null)
                    {
                        _respModel.Status = false;
                        _respModel.StatusCode = resultingMessage.Messages.message.Select(s => s.code).FirstOrDefault();
                        _respModel.ResponseString = responseStr;
                        _respModel.StatusMessage = resultingMessage.Messages.message.Select(s => s.text).FirstOrDefault();
                    }
                    else // ERRROR 
                    {
                        _respModel.Status = false;
                        _respModel.StatusCode = "404";
                        _respModel.ResponseString = responseStr;
                        _respModel.StatusMessage = "Unknown error";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetHttpCall process exception : " + ex.Message.ToString());
                _respModel.Status = false;
                _respModel.StatusMessage = ex.Message.ToString();
                throw ex;
            }



            //switch (resultingMessage.Messages.resultCode)
            //{
            //    case "Ok":
            //        _respModel.Status = true;
            //        _respModel.StatusCode = "200";
            //        _respModel.StatusMessage = "Success";
            //        _respModel.AuthCode = resultingMessage.TransactionResponse.authCode;
            //        _respModel.TransactionID = resultingMessage.TransactionResponse.transId;
            //        _respModel.ResponseString = responseStr;
            //        break;
            //    case "Error":
            //        _respModel.Status = false;
            //        _respModel.StatusCode = resultingMessage.Messages.message.First().code;
            //        _respModel.ResponseString = responseStr;
            //        _respModel.StatusMessage = resultingMessage.TransactionResponse.errors.First().errorText == string.Empty ? resultingMessage.Messages.message.First().text : resultingMessage.TransactionResponse.errors.First().errorText;
            //        break;
            //    default:
            //        _respModel.Status = false;
            //        _respModel.StatusCode = "404";
            //        _respModel.ResponseString = responseStr;
            //        //_respModel.StatusMessage = resultingMessage.Messages.message.;
            //        break;
            //}
        }

        internal string CreateJSON(object YourClassObject)
        {
            string jsonString = JsonConvert.SerializeObject(YourClassObject);
            return jsonString;
        }

        #endregion
    }
}
