﻿using System.Xml.Serialization;

namespace Arrowhead.POS.PaymentProcess
{
    public class TranzPayModel
    {
    }

    public class TranzPayRequest
    {
        [XmlElement(ElementName = "TransactionType")]
        public string TransactionType { get; set; }
        [XmlElement(ElementName = "UserName")]
        public string UserName { get; set; }
        [XmlElement(ElementName = "Password")]
        public string Password { get; set; }
        [XmlElement(ElementName = "ProducerID")]
        public string ProducerID { get; set; }
        [XmlElement(ElementName = "CardNumber")]
        public string CardNumber { get; set; }
        [XmlElement(ElementName = "ExpirationDateMMYY")]
        public string ExpirationDateMMYY { get; set; }
        [XmlElement(ElementName = "CVV2")]
        public string CVV2 { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "TransactionAmount")]
        public decimal TransactionAmount { get; set; }
        [XmlElement(ElementName = "BillingNameFirst")]
        public string BillingNameFirst { get; set; }
        [XmlElement(ElementName = "BillingNameLast")]
        public string BillingNameLast { get; set; }
        [XmlElement(ElementName = "BillingAddress")]
        public string BillingAddress { get; set; }
        [XmlElement(ElementName = "BillingCity")]
        public string BillingCity { get; set; }
        [XmlElement(ElementName = "BillingState")]
        public string BillingState { get; set; }
        [XmlElement(ElementName = "BillingZipCode")]
        public string BillingZipCode { get; set; }
        [XmlElement(ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlElement(ElementName = "Email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "PolicyNumber")]
        public string PolicyNumber { get; set; }
        [XmlElement(ElementName = "CustomField1")]
        public string CustomField1 { get; set; }
        [XmlElement(ElementName = "CustomField2")]
        public string CustomField2 { get; set; }
        [XmlElement(ElementName = "CustomField3")]
        public string CustomField3 { get; set; }
        [XmlElement(ElementName = "CustomField4")]
        public string CustomField4 { get; set; }
    }



    [XmlRoot(ElementName = "GatewayResponse")]
    public class GatewayResponse
    {
        [XmlElement(ElementName = "PostedDate")]
        public string PostedDate { get; set; }
        [XmlElement(ElementName = "StatusID")]
        public string StatusID { get; set; }
        [XmlElement(ElementName = "TransactionType")]
        public string TransactionType { get; set; }
        [XmlElement(ElementName = "PaymentType")]
        public string PaymentType { get; set; }
        [XmlElement(ElementName = "TransactionID")]
        public string TransactionID { get; set; }
        [XmlElement(ElementName = "TransactionAmount")]
        public string TransactionAmount { get; set; }
        [XmlElement(ElementName = "AuthorizationCode")]
        public string AuthorizationCode { get; set; }
        [XmlElement(ElementName = "ResponseCode")]
        public string ResponseCode { get; set; }
        [XmlElement(ElementName = "ResponseMessage")]
        public string ResponseMessage { get; set; }
        [XmlElement(ElementName = "CVV2ResponseCode")]
        public string CVV2ResponseCode { get; set; }
        [XmlElement(ElementName = "CVV2ResponseMessage")]
        public string CVV2ResponseMessage { get; set; }
        [XmlElement(ElementName = "AVSResponseCode")]
        public string AVSResponseCode { get; set; }
        [XmlElement(ElementName = "AVSResponseMessage")]
        public string AVSResponseMessage { get; set; }
        [XmlElement(ElementName = "CardNumber")]
        public string CardNumber { get; set; }
        [XmlElement(ElementName = "CustomerName")]
        public string CustomerName { get; set; }
        [XmlElement(ElementName = "BillingNameFirst")]
        public string BillingNameFirst { get; set; }
        [XmlElement(ElementName = "BillingNameLast")]
        public string BillingNameLast { get; set; }
        [XmlElement(ElementName = "BillingAddress")]
        public string BillingAddress { get; set; }
        [XmlElement(ElementName = "BillingCity")]
        public string BillingCity { get; set; }
        [XmlElement(ElementName = "BillingState")]
        public string BillingState { get; set; }
        [XmlElement(ElementName = "BillingZipCode")]
        public string BillingZipCode { get; set; }
        [XmlElement(ElementName = "BillingCountry")]
        public string BillingCountry { get; set; }
        [XmlElement(ElementName = "BillingPhone")]
        public string BillingPhone { get; set; }
        [XmlElement(ElementName = "BillingEmail")]
        public string BillingEmail { get; set; }
    }

    [XmlRoot(ElementName = "Transaction")]
    public class Transaction
    {
        [XmlElement(ElementName = "CardType")]
        public string CardType { get; set; }
        [XmlElement(ElementName = "GatewayResponse")]
        public GatewayResponse GatewayResponse { get; set; }
    }

    [XmlRoot(ElementName = "TranzPayResponse")]
    public class TranzPayResponse
    {
        [XmlElement(ElementName = "ProducerID")]
        public string ProducerID { get; set; }
        [XmlElement(ElementName = "CustomerID")]
        public string CustomerID { get; set; }
        [XmlElement(ElementName = "GatewayConnect")]
        public string GatewayConnect { get; set; }
        [XmlElement(ElementName = "Transaction")]
        public Transaction Transaction { get; set; }
    }

    [XmlRoot(ElementName = "Error")]
    public class Error
    {
        [XmlElement(ElementName = "Code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "Msg")]
        public string Msg { get; set; }
    }

    [XmlRoot(ElementName = "TranzPayResponse")]
    public class TranzPayErrorResponse
    {
        [XmlElement(ElementName = "ProducerID")]
        public string ProducerID { get; set; }
        [XmlElement(ElementName = "CustomerID")]
        public string CustomerID { get; set; }
        [XmlElement(ElementName = "GatewayConnect")]
        public string GatewayConnect { get; set; }
        [XmlElement(ElementName = "Error")]
        public Error Error { get; set; }
    }


}

namespace Arrowhead.POS.ACHPaymentProcess
{
    [XmlRoot(ElementName = "TranzPayRequest")]
    public class TranzPayRequest
    {
        [XmlElement(ElementName = "TransactionType")]
        public string TransactionType { get; set; }
        [XmlElement(ElementName = "UserName")]
        public string UserName { get; set; }
        [XmlElement(ElementName = "Password")]
        public string Password { get; set; }
        [XmlElement(ElementName = "ProducerID")]
        public string ProducerID { get; set; }
        [XmlElement(ElementName = "BankRoutingNumber")]
        public string BankRoutingNumber { get; set; }
        [XmlElement(ElementName = "BankAccountNumber")]
        public string BankAccountNumber { get; set; }
        [XmlElement(ElementName = "BankAccountType")]
        public string BankAccountType { get; set; }
        [XmlElement(ElementName = "CheckType")]
        public string CheckType { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "TransactionAmount")]
        public decimal TransactionAmount { get; set; }
        [XmlElement(ElementName = "BillingNameFirst")]
        public string BillingNameFirst { get; set; }
        [XmlElement(ElementName = "BillingNameLast")]
        public string BillingNameLast { get; set; }
        [XmlElement(ElementName = "BillingAddress")]
        public string BillingAddress { get; set; }
        [XmlElement(ElementName = "BillingCity")]
        public string BillingCity { get; set; }
        [XmlElement(ElementName = "BillingState")]
        public string BillingState { get; set; }
        [XmlElement(ElementName = "BillingZipCode")]
        public string BillingZipCode { get; set; }
        [XmlElement(ElementName = "Email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "PolicyNumber")]
        public string PolicyNumber { get; set; }
        [XmlElement(ElementName = "CustomField1")]
        public string CustomField1 { get; set; }
        [XmlElement(ElementName = "CustomField2")]
        public string CustomField2 { get; set; }
        [XmlElement(ElementName = "CustomField3")]
        public string CustomField3 { get; set; }
        [XmlElement(ElementName = "CustomField4")]
        public string CustomField4 { get; set; }
    }

    [XmlRoot(ElementName = "GatewayResponse")]
    public class GatewayResponse
    {
        [XmlElement(ElementName = "PostedDate")]
        public string PostedDate { get; set; }
        [XmlElement(ElementName = "StatusID")]
        public string StatusID { get; set; }
        [XmlElement(ElementName = "TransactionType")]
        public string TransactionType { get; set; }
        [XmlElement(ElementName = "PaymentType")]
        public string PaymentType { get; set; }
        [XmlElement(ElementName = "TransactionID")]
        public string TransactionID { get; set; }
        [XmlElement(ElementName = "TransactionAmount")]
        public string TransactionAmount { get; set; }
        [XmlElement(ElementName = "ResponseMessage")]
        public string ResponseMessage { get; set; }
        [XmlElement(ElementName = "BankRoutingNumber")]
        public string BankRoutingNumber { get; set; }
        [XmlElement(ElementName = "BankAccountNumber")]
        public string BankAccountNumber { get; set; }
        [XmlElement(ElementName = "CustomerName")]
        public string CustomerName { get; set; }
        [XmlElement(ElementName = "BillingNameFirst")]
        public string BillingNameFirst { get; set; }
        [XmlElement(ElementName = "BillingNameLast")]
        public string BillingNameLast { get; set; }
        [XmlElement(ElementName = "BillingAddress")]
        public string BillingAddress { get; set; }
        [XmlElement(ElementName = "BillingCity")]
        public string BillingCity { get; set; }
        [XmlElement(ElementName = "BillingState")]
        public string BillingState { get; set; }
        [XmlElement(ElementName = "BillingZipCode")]
        public string BillingZipCode { get; set; }
        [XmlElement(ElementName = "BillingCountry")]
        public string BillingCountry { get; set; }
        [XmlElement(ElementName = "BillingPhone")]
        public string BillingPhone { get; set; }
        [XmlElement(ElementName = "BillingEmail")]
        public string BillingEmail { get; set; }
    }


}


