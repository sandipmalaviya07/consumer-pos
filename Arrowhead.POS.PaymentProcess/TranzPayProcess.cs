﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using Arrowhead.POS.Service;

namespace Arrowhead.POS.PaymentProcess
{
    public class TranzPayProcess : IPaymentGateway
    {
        public PaymentProcessResponseModel AuthorizeCCPayment(PaymentProcessRequestModel requestModel)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessResponseModel CaptureACHPayment(PaymentProcessRequestModel requestModel)
        {
            PaymentServices paymentServices = new PaymentServices();
            var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();

            ACHPaymentProcess.TranzPayRequest payRequest = new ACHPaymentProcess.TranzPayRequest()
            {
                BillingAddress = requestModel.CustomerAddress1,
                BillingCity = requestModel.CustomerCity,
                BillingNameFirst = requestModel.CustomerFirstName,
                BillingNameLast = requestModel.CustomerLastName,
                BillingState = requestModel.CustomerState,
                BillingZipCode = requestModel.CustomerZipcode,
                Description = requestModel.Description,
                Email = requestModel.CustomerEmail,
                Password = _getCredentials.Password,
                PolicyNumber = Core.SysFunctions.GenerateAuthToken(6),
                ProducerID = _getCredentials.MerchantID,
                TransactionAmount = requestModel.TotalAmount,
                TransactionType = "ACHCharge",
                UserName = _getCredentials.UserName,
                BankAccountNumber = requestModel.BankAccountNumber,
                BankAccountType = requestModel.BankAccountType,
                BankRoutingNumber = requestModel.BankRoutingNumber,
                CheckType = requestModel.CheckType
            };

            string requestXml = CreateXML(payRequest);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_getCredentials.EndPointURL);
            byte[] bytes;
            bytes = Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();

                XmlSerializer serializer = new XmlSerializer(typeof(ACHPaymentProcess.GatewayResponse));
                StringReader rdr = new StringReader(responseStr);
                ACHPaymentProcess.GatewayResponse resultingMessage = (ACHPaymentProcess.GatewayResponse)serializer.Deserialize(rdr);

                switch (resultingMessage.ResponseMessage)
                {
                    case "":
                        _respModel.Status = true;
                        _respModel.StatusCode = "0";
                        _respModel.StatusMessage = "Success";
                        _respModel.TransactionID = resultingMessage.TransactionID;                        
                        break;
                    default:
                        _respModel.Status = false;
                        _respModel.StatusCode = "404";
                        _respModel.StatusMessage = resultingMessage.ResponseMessage;
                        break;
                }
            }
            return _respModel;

        }

        public PaymentProcessResponseModel CapturePayment(PaymentProcessRequestModel requestModel)
        {
            PaymentServices paymentServices = new PaymentServices();
            var _getCredentials = paymentServices.getPaymentGateWayCredentialDetail();
            PaymentProcessResponseModel _respModel = new PaymentProcessResponseModel();
            TranzPayRequest payRequest = new TranzPayRequest()
            {
                BillingAddress = requestModel.CustomerAddress1,
                BillingCity = requestModel.CustomerCity,
                BillingNameFirst = requestModel.CustomerFirstName,
                BillingNameLast = requestModel.CustomerLastName,
                BillingState = requestModel.CustomerState,
                BillingZipCode = requestModel.CustomerZipcode,
                CardNumber = requestModel.CCNumber,
                CVV2 = requestModel.CVVNumber,
                Description = requestModel.Description,
                Email = requestModel.CustomerEmail,
                ExpirationDateMMYY = (requestModel.CCExpMonth.ToString().Length
                                     == 1 ? "0" + requestModel.CCExpMonth.ToString() :
                                     requestModel.CCExpMonth.ToString()).ToString() + requestModel.CCExpYear.ToString(),
                Password = _getCredentials.Password,
                PhoneNumber = requestModel.CustomerPhone,
                PolicyNumber = Core.SysFunctions.GenerateAuthToken(6),
                ProducerID = _getCredentials.MerchantID,
                TransactionAmount = requestModel.TotalAmount,
                TransactionType = "CreditCardCharge",
                UserName = _getCredentials.UserName
            };

            string requestXml = CreateXML(payRequest);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_getCredentials.EndPointURL);
            byte[] bytes;
            bytes = Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();

                if (responseStr.Contains("Error"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(TranzPayErrorResponse));
                    StringReader rdr = new StringReader(responseStr);

                    TranzPayErrorResponse resultingMessage = (TranzPayErrorResponse)serializer.Deserialize(rdr);
                    _respModel.Status = false;
                    _respModel.StatusCode = resultingMessage.Error.Code;
                    _respModel.StatusMessage = resultingMessage.Error.Msg;
                }
                else
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(TranzPayResponse));
                    StringReader rdr = new StringReader(responseStr);
                    TranzPayResponse resultingMessage = (TranzPayResponse)serializer.Deserialize(rdr);
                    try
                    {
                        switch (resultingMessage.Transaction.GatewayResponse.ResponseMessage)
                        {
                            case "APPROVED":
                                _respModel.Status = true;
                                _respModel.StatusCode = "0";
                                _respModel.StatusMessage = "Success";
                                _respModel.TransactionID = resultingMessage.Transaction.GatewayResponse.TransactionID;
                                _respModel.CustomerId = resultingMessage.CustomerID;
                                break;
                            default:
                                _respModel.Status = false;
                                _respModel.StatusCode = resultingMessage.Transaction.GatewayResponse.ResponseCode.ToString();
                                _respModel.StatusMessage = resultingMessage.Transaction.GatewayResponse.ResponseMessage;
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        _respModel.Status = false;
                        _respModel.StatusCode = resultingMessage.Transaction.GatewayResponse.ResponseCode.ToString();
                        _respModel.StatusMessage = resultingMessage.Transaction.GatewayResponse.ResponseMessage;
                    }

                }

            }
            return _respModel;
        }

        public PaymentProcessRefundResponseModel GetTransactionDetails(string transactionID)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessRefundResponseModel RefundCCPayment(PaymentProcessRefundRequestModel refundModel)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessResponseModel RefundProcess(string ReqId,string methodType)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessResponseModel VoidPayment(PaymentProcessRequestModel requestModel)
        {
            throw new NotImplementedException();
        }

        internal string CreateXML(object YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            foreach (var node in
            from XmlNode node in xmlDoc
            where node.NodeType == XmlNodeType.XmlDeclaration
            select node)
            {
                xmlDoc.RemoveChild(node);
            }
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }

        PaymentProcessResponseModel IPaymentGateway.GetTransactionDetails(string transactionID)
        {
            throw new NotImplementedException();
        }
    }

}
