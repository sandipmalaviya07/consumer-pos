﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.PaymentProcess
{
    public class AuthorizeModel
    {
        public class MerchantAuthentication
        {
            public string name { get; set; }
            public string transactionKey { get; set; }
        }

        public class CreditCard
        {
            public string cardNumber { get; set; }
            public string expirationDate { get; set; }
            public string cardCode { get; set; }
        }

        public class Payment
        {
            public CreditCard creditCard { get; set; }
        }

        public class LineItem
        {
            public string itemId { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public string quantity { get; set; }
            public string unitPrice { get; set; }
        }

        public class LineItems
        {
            public LineItem lineItem { get; set; }
        }

        public class Tax
        {
            public string amount { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

        public class Duty
        {
            public string amount { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

        public class Shipping
        {
            public string amount { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

        public class Customer
        {
            public string id { get; set; }
            public string email { get; set; }
        }

        public class BillTo
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string company { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zip { get; set; }
            public string country { get; set; }
            public string phoneNumber { get; set; }
        }

        public class ShipTo
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string company { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zip { get; set; }
            public string country { get; set; }
        }

        public class Setting
        {
            public string settingName { get; set; }
            public string settingValue { get; set; }
        }

        public class TransactionSettings
        {
            public Setting setting { get; set; }
        }

        public class UserField
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class UserFields
        {
            public List<UserField> userField { get; set; }
        }

        public class TransactionRequest
        {
            public string transactionType { get; set; }
            public decimal amount { get; set; }
            public Payment payment { get; set; }
            //public LineItems lineItems { get; set; }
            //public Tax tax { get; set; }
            //public Duty duty { get; set; }
            //public Shipping shipping { get; set; }
            //public string poNumber { get; set; }
            public Customer customer { get; set; }
            public BillTo billTo { get; set; }
            public ShipTo shipTo { get; set; }
            //public string customerIP { get; set; }
            //public TransactionSettings transactionSettings { get; set; }
            public UserFields userFields { get; set; }
        }

        public class CreateTransactionRequest
        {
            public MerchantAuthentication merchantAuthentication { get; set; }
            public string refId { get; set; }
            public TransactionRequest transactionRequest { get; set; }
        }

        public class RootObject
        {
            public CreateTransactionRequest createTransactionRequest { get; set; }
        }

        // Response Object 
        public class Message
        {
            public string code { get; set; }
            public string description { get; set; }
        }

        public class ResponseUserField
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class TransactionResponse
        {
            public string responseCode { get; set; }
            public string authCode { get; set; }
            public string avsResultCode { get; set; }
            public string cvvResultCode { get; set; }
            public string cavvResultCode { get; set; }
            public string transId { get; set; }
            public string refTransID { get; set; }
            public string transHash { get; set; }
            public string testRequest { get; set; }
            public string accountNumber { get; set; }
            public string accountType { get; set; }
            public List<Message> messages { get; set; }
            public List<UserField> userFields { get; set; }
            public string transHashSha2 { get; set; }
            public int SupplementalDataQualificationIndicator { get; set; }
        }

        public class Message2
        {
            public string code { get; set; }
            public string text { get; set; }
        }

        public class Messages
        {
            public string resultCode { get; set; }
            public List<Message2> message { get; set; }
        }

        public class ResponseRootObject
        {
            public TransactionResponse transactionResponse { get; set; }
            public string refId { get; set; }
            public Messages messages { get; set; }
        }
    }
    public class AuthorizeRefundModel
    {
        public class MerchantAuthentication
        {
            public string name { get; set; }
            public string transactionKey { get; set; }
        }

        public class CreditCard
        {
            public string cardNumber { get; set; }
            public string expirationDate { get; set; }
        }

        public class Payment
        {
            public CreditCard creditCard { get; set; }
        }

        public class TransactionRequest
        {
            public string transactionType { get; set; }
            public string amount { get; set; }
            public Payment payment { get; set; }
            public string refTransId { get; set; }
        }

        public class CreateTransactionRequest
        {
            public MerchantAuthentication merchantAuthentication { get; set; }
            public string refId { get; set; }
            public TransactionRequest transactionRequest { get; set; }
        }

        public class RootObject
        {
            public CreateTransactionRequest createTransactionRequest { get; set; }
        }
    }
    public class AuthorizeVoidModel
    {
        public class MerchantAuthentication
        {
            public string name { get; set; }
            public string transactionKey { get; set; }
        }

        public class TransactionRequest
        {
            public string transactionType { get; set; }
            public string refTransId { get; set; }
        }

        public class CreateTransactionRequest
        {
            public MerchantAuthentication merchantAuthentication { get; set; }
            public string refId { get; set; }
            public TransactionRequest transactionRequest { get; set; }
        }

        public class RootObject
        {
            public CreateTransactionRequest createTransactionRequest { get; set; }
        }
    }
    public class AuthorizeCaptureModel
    {
        public class MerchantAuthentication
        {
            public string name { get; set; }
            public string transactionKey { get; set; }
        }

        public class TransactionRequest
        {
            public string transactionType { get; set; }
            public string amount { get; set; }
            public string refTransId { get; set; }
        }

        public class CreateTransactionRequest
        {
            public MerchantAuthentication merchantAuthentication { get; set; }
            public string refId { get; set; }
            public TransactionRequest transactionRequest { get; set; }
        }

        public class AuthCaptureModel
        {
            public CreateTransactionRequest createTransactionRequest { get; set; }
        }
    }
}

namespace Arrowhead.POS.ACHPaymentProcess
{
    public class MerchantAuthentication
    {
        public string name { get; set; }
        public string transactionKey { get; set; }
    }

    public class BankAccount
    {
        public string accountType { get; set; }
        public string routingNumber { get; set; }
        public string accountNumber { get; set; }
        public string nameOnAccount { get; set; }
    }

    public class Payment
    {
        public BankAccount bankAccount { get; set; }
    }

    public class LineItem
    {
        public string itemId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string quantity { get; set; }
        public string unitPrice { get; set; }
    }

    public class Customer
    {
        public string id { get; set; }
        public string email { get; set; }
    }

    public class LineItems
    {
        public LineItem lineItem { get; set; }
    }

    public class Tax
    {
        public string amount { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class Duty
    {
        public string amount { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class Shipping
    {
        public string amount { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class BillTo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string company { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string phoneNumber { get; set; }
    }

    public class ShipTo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string company { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }

    public class TransactionRequest
    {
        public string transactionType { get; set; }
        public decimal amount { get; set; }
        public Payment payment { get; set; }
        public Customer customer { get; set; }
        //public LineItems lineItems { get; set; }
        //public Tax tax { get; set; }
        //public Duty duty { get; set; }
        //public Shipping shipping { get; set; }
        //public string poNumber { get; set; }
        public BillTo billTo { get; set; }
        public ShipTo shipTo { get; set; }
        //public string customerIP { get; set; }
    }

    public class CreateTransactionRequest
    {
        public MerchantAuthentication merchantAuthentication { get; set; }
        public string refId { get; set; }
        public TransactionRequest transactionRequest { get; set; }
    }

    public class RootObject
    {
        // Request
        public CreateTransactionRequest createTransactionRequest { get; set; }
    }

    // Response 
    public class Error
    {
        public string errorCode { get; set; }
        public string errorText { get; set; }
    }

    //public class ShipTo
    //{
    //}

    public class TransactionResponse
    {
        public string responseCode { get; set; }
        public string authCode { get; set; }
        public string avsResultCode { get; set; }
        public string cvvResultCode { get; set; }
        public string cavvResultCode { get; set; }
        public string transId { get; set; }
        public string refTransID { get; set; }
        public string transHash { get; set; }
        public string testRequest { get; set; }
        public string accountNumber { get; set; }
        public string accountType { get; set; }
        public List<Error> errors { get; set; }
        public ShipTo shipTo { get; set; }
        public string transHashSha2 { get; set; }
        public int SupplementalDataQualificationIndicator { get; set; }
    }

    public class Message
    {
        public string code { get; set; }
        public string text { get; set; }
    }

    public class Messages
    {
        public string resultCode { get; set; }
        public List<Message> message { get; set; }
    }

    public class ResponseRootObject
    {
        // Response
        public TransactionResponse TransactionResponse { get; set; }
        public string refId { get; set; }
        public Messages Messages { get; set; }
    }
}

namespace Arrowhead.POS.PaymentTransactionProcess
{
    public class AuthorizeTransactionModel
    {
        public class MerchantAuthentication
        {
            public string name { get; set; }
            public string transactionKey { get; set; }
        }

        public class GetTransactionDetailsRequest
        {
            public MerchantAuthentication merchantAuthentication { get; set; }
            public string transId { get; set; }
        }

        public class RootObject
        {
            public GetTransactionDetailsRequest getTransactionDetailsRequest { get; set; }
        }


        // Response
        public class CreditCard
        {
            public string cardNumber { get; set; }
            public string expirationDate { get; set; }
            public string cardType { get; set; }
        }

        public class Payment
        {
            public CreditCard creditCard { get; set; }
        }

        public class Customer
        {
            public string id { get; set; }
            public string email { get; set; }
        }

        public class BillTo
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string company { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zip { get; set; }
        }

        public class Transaction
        {
            public string transId { get; set; }
            public DateTime submitTimeUTC { get; set; }
            public DateTime submitTimeLocal { get; set; }
            public string transactionType { get; set; }
            public string transactionStatus { get; set; }
            public int responseCode { get; set; }
            public int responseReasonCode { get; set; }
            public string responseReasonDescription { get; set; }
            public string authCode { get; set; }
            public string AVSResponse { get; set; }
            public string cardCodeResponse { get; set; }
            public double authAmount { get; set; }
            public double settleAmount { get; set; }
            public bool taxExempt { get; set; }
            public Payment payment { get; set; }
            public Customer customer { get; set; }
            public BillTo billTo { get; set; }
            public bool recurringBilling { get; set; }
            public string customerIP { get; set; }
            public string product { get; set; }
            public string marketType { get; set; }
        }

        public class Message
        {
            public string code { get; set; }
            public string text { get; set; }
        }

        public class Messages
        {
            public string resultCode { get; set; }
            public List<Message> message { get; set; }
        }

        public class ResponseRootObject
        {
            public Transaction transaction { get; set; }
            public string transrefId { get; set; }
            public Messages messages { get; set; }
        }
    }
}


