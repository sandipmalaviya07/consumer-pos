﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Model;
using Arrowhead.POS.Model.CCPaymentRequestModel;
using Arrowhead.POS.Service;

namespace Arrowhead.POS.PaymentProcess
{
    public class StripeProcess : IPaymentGateway
    {
        PaymentServices paymentServices = new PaymentServices();

        public PaymentProcessResponseModel AuthorizeCCPayment(PaymentProcessRequestModel requestModel)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessResponseModel CaptureACHPayment(PaymentProcessRequestModel requestModel)
        {
            PaymentProcessResponseModel resp = paymentServices.CaptureStripeViaACH(requestModel);
            return resp;
        }

        public PaymentProcessResponseModel CapturePayment(PaymentProcessRequestModel _requestModel)
        {
            PaymentProcessResponseModel resp = paymentServices.CaptureStripeCharge(_requestModel);
            return resp;
        }

        public PaymentProcessRefundResponseModel GetTransactionDetails(string transactionID)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessRefundResponseModel RefundCCPayment(PaymentProcessRefundRequestModel refundModel)
        {
            throw new NotImplementedException();
        }

        public PaymentProcessResponseModel RefundProcess(string chargeId, string methodType)
        {
            PaymentProcessResponseModel resp = paymentServices.RefundStripePayment(chargeId, methodType);
            return resp;
        }

        public PaymentProcessResponseModel VoidPayment(PaymentProcessRequestModel requestModel)
        {
            throw new NotImplementedException();
        }

        PaymentProcessResponseModel IPaymentGateway.GetTransactionDetails(string transactionID)
        {
            throw new NotImplementedException();
        }
    }
}
