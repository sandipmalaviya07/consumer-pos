﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Arrowhead.POS.Process.ProcessXMLModel
{

    public class ArrowheadProcessXMLModel
    {
        [XmlElement(ElementName = "ACORD")]
        public ACORD ACORD { get; set; }
    }
    [XmlRoot(ElementName = "Status")]
    public class Status
    {
        [XmlElement(ElementName = "StatusCd")]
        public string StatusCd { get; set; }
        [XmlElement(ElementName = "StatusDesc")]
        public string StatusDesc { get; set; }
    }

    [XmlRoot(ElementName = "ClientApp")]
    public class ClientApp
    {
        [XmlElement(ElementName = "Org")]
        public string Org { get; set; }
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Version")]
        public string Version { get; set; }
    }

    [XmlRoot(ElementName = "SignonRs")]
    public class SignonRs
    {
        [XmlElement(ElementName = "Status")]
        public Status Status { get; set; }
        [XmlElement(ElementName = "ClientApp")]
        public ClientApp ClientApp { get; set; }
    }

    [XmlRoot(ElementName = "CommlName")]
    public class CommlName
    {
        [XmlElement(ElementName = "CommercialName")]
        public string CommercialName { get; set; }
    }

    [XmlRoot(ElementName = "PersonName")]
    public class PersonName
    {
        [XmlElement(ElementName = "Surname")]
        public string Surname { get; set; }
        [XmlElement(ElementName = "GivenName")]
        public string GivenName { get; set; }
        [XmlElement(ElementName = "OtherGivenName")]
        public string OtherGivenName { get; set; }
        [XmlElement(ElementName = "NameSuffix")]
        public string NameSuffix { get; set; }
    }

    [XmlRoot(ElementName = "TaxIdentity")]
    public class TaxIdentity
    {
        [XmlElement(ElementName = "TaxIdTypeCd")]
        public string TaxIdTypeCd { get; set; }
        [XmlElement(ElementName = "TaxId")]
        public string TaxId { get; set; }
    }

    [XmlRoot(ElementName = "NameInfo")]
    public class NameInfo
    {
        [XmlElement(ElementName = "CommlName")]
        public CommlName CommlName { get; set; }
        [XmlElement(ElementName = "PersonName")]
        public PersonName PersonName { get; set; }
        [XmlElement(ElementName = "TaxIdentity")]
        public TaxIdentity TaxIdentity { get; set; }
    }

    [XmlRoot(ElementName = "Addr")]
    public class Addr
    {
        [XmlElement(ElementName = "AddrTypeCd")]
        public string AddrTypeCd { get; set; }
        [XmlElement(ElementName = "Addr1")]
        public string Addr1 { get; set; }
        [XmlElement(ElementName = "Addr2")]
        public string Addr2 { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "StateProvCd")]
        public string StateProvCd { get; set; }
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "County")]
        public string County { get; set; }
        [XmlElement(ElementName = "StateProv")]
        public string StateProv { get; set; }
    }

    [XmlRoot(ElementName = "PhoneInfo")]
    public class PhoneInfo
    {
        [XmlElement(ElementName = "PhoneTypeCd")]
        public string PhoneTypeCd { get; set; }
        [XmlElement(ElementName = "CommunicationUseCd")]
        public string CommunicationUseCd { get; set; }
        [XmlElement(ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
    }

    [XmlRoot(ElementName = "Communications")]
    public class Communications
    {
        [XmlElement(ElementName = "PhoneInfo")]
        public List<PhoneInfo> PhoneInfo { get; set; }
        [XmlElement(ElementName = "EmailInfo")]
        public EmailInfo EmailInfo { get; set; }
    }

    [XmlRoot(ElementName = "GeneralPartyInfo")]
    public class GeneralPartyInfo
    {
        [XmlElement(ElementName = "NameInfo")]
        public NameInfo NameInfo { get; set; }
        [XmlElement(ElementName = "Addr")]
        public List<Addr> Addr { get; set; }
        [XmlElement(ElementName = "Communications")]
        public Communications Communications { get; set; }
    }

    [XmlRoot(ElementName = "ProducerInfo")]
    public class ProducerInfo
    {
        [XmlElement(ElementName = "ContractNumber")]
        public string ContractNumber { get; set; }
    }

    [XmlRoot(ElementName = "Producer")]
    public class Producer
    {
        [XmlElement(ElementName = "GeneralPartyInfo")]
        public GeneralPartyInfo GeneralPartyInfo { get; set; }
        [XmlElement(ElementName = "ProducerInfo")]
        public ProducerInfo ProducerInfo { get; set; }
    }

    [XmlRoot(ElementName = "EmailInfo")]
    public class EmailInfo
    {
        [XmlElement(ElementName = "CommunicationUseCd")]
        public string CommunicationUseCd { get; set; }
        [XmlElement(ElementName = "EmailAddr")]
        public string EmailAddr { get; set; }
    }

    [XmlRoot(ElementName = "PersonInfo")]
    public class PersonInfo
    {
        [XmlElement(ElementName = "GenderCd")]
        public string GenderCd { get; set; }
        [XmlElement(ElementName = "BirthDt")]
        public string BirthDt { get; set; }
        [XmlElement(ElementName = "MaritalStatusCd")]
        public string MaritalStatusCd { get; set; }
        [XmlElement(ElementName = "OccupationDesc")]
        public string OccupationDesc { get; set; }
        [XmlElement(ElementName = "OccupationClassCd")]
        public string OccupationClassCd { get; set; }
        [XmlElement(ElementName = "LengthTimeEmployed")]
        public LengthTimeEmployed LengthTimeEmployed { get; set; }
        [XmlElement(ElementName = "LengthTimeCurrentOccupation")]
        public LengthTimeCurrentOccupation LengthTimeCurrentOccupation { get; set; }
        [XmlElement(ElementName = "MiscParty")]
        public MiscParty MiscParty { get; set; }
    }

    [XmlRoot(ElementName = "InsuredOrPrincipalInfo")]
    public class InsuredOrPrincipalInfo
    {
        [XmlElement(ElementName = "InsuredOrPrincipalRoleCd")]
        public string InsuredOrPrincipalRoleCd { get; set; }
        [XmlElement(ElementName = "PersonInfo")]
        public PersonInfo PersonInfo { get; set; }
    }

    [XmlRoot(ElementName = "InsuredOrPrincipal")]
    public class InsuredOrPrincipal
    {
        [XmlElement(ElementName = "GeneralPartyInfo")]
        public GeneralPartyInfo GeneralPartyInfo { get; set; }
        [XmlElement(ElementName = "InsuredOrPrincipalInfo")]
        public InsuredOrPrincipalInfo InsuredOrPrincipalInfo { get; set; }
    }

    [XmlRoot(ElementName = "DurationPeriod")]
    public class DurationPeriod
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "ContractTerm")]
    public class ContractTerm
    {
        [XmlElement(ElementName = "EffectiveDt")]
        public string EffectiveDt { get; set; }
        [XmlElement(ElementName = "ExpirationDt")]
        public string ExpirationDt { get; set; }
        [XmlElement(ElementName = "StartTime")]
        public string StartTime { get; set; }
        [XmlElement(ElementName = "EndTime")]
        public string EndTime { get; set; }
        [XmlElement(ElementName = "Lapse")]
        public string Lapse { get; set; }
        [XmlElement(ElementName = "DurationPeriod")]
        public DurationPeriod DurationPeriod { get; set; }
    }

    [XmlRoot(ElementName = "CurrentTermAmt")]
    public class CurrentTermAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
    }

    [XmlRoot(ElementName = "InstallmentAmt")]
    public class InstallmentAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
    }

    [XmlRoot(ElementName = "InstallmentInfo")]
    public class InstallmentInfo
    {
        [XmlElement(ElementName = "InstallmentNumber")]
        public string InstallmentNumber { get; set; }
        [XmlElement(ElementName = "InstallmentDate")]
        public string InstallmentDate { get; set; }
        [XmlElement(ElementName = "InstallmentAmt")]
        public InstallmentAmt InstallmentAmt { get; set; }
        [XmlElement(ElementName = "InstallmentDueDt")]
        public string InstallmentDueDt { get; set; }
    }

    [XmlRoot(ElementName = "PaymentOption")]
    public class PaymentOption
    {
        [XmlElement(ElementName = "NumPayments")]
        public string NumPayments { get; set; }
        [XmlElement(ElementName = "DownPaymentPct")]
        public string DownPaymentPct { get; set; }
        [XmlElement(ElementName = "MethodPaymentCd")]
        public string MethodPaymentCd { get; set; }
        [XmlElement(ElementName = "InstallmentFeeAmt")]
        public string InstallmentFeeAmt { get; set; }
        [XmlElement(ElementName = "TotalPremium")]
        public string TotalPremium { get; set; }
        [XmlElement(ElementName = "PayplanId")]
        public string PayplanId { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "InstallmentInfo")]
        public List<InstallmentInfo> InstallmentInfo { get; set; }
    }

    [XmlRoot(ElementName = "QuoteInfo")]
    public class QuoteInfo
    {
        [XmlElement(ElementName = "CompanysQuoteNumber")]
        public string CompanysQuoteNumber { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "QuotePreparedDt")]
        public string QuotePreparedDt { get; set; }
    }

    [XmlRoot(ElementName = "CreditScoreInfo")]
    public class CreditScoreInfo
    {
        [XmlElement(ElementName = "CreditScore")]
        public string CreditScore { get; set; }
        [XmlElement(ElementName = "CreditScoreDt")]
        public string CreditScoreDt { get; set; }
        [XmlElement(ElementName = "CSReasonCd")]
        public string CSReasonCd { get; set; }
        [XmlElement(ElementName = "CSReasonDesc")]
        public string CSReasonDesc { get; set; }
    }

    [XmlRoot(ElementName = "QuestionAnswer")]
    public class QuestionAnswer
    {
        [XmlElement(ElementName = "QuestionCd")]
        public string QuestionCd { get; set; }
        [XmlElement(ElementName = "YesNoCd")]
        public string YesNoCd { get; set; }
        [XmlElement(ElementName = "Explanation")]
        public string Explanation { get; set; }
    }

    [XmlRoot(ElementName = "PersApplicationInfo")]
    public class PersApplicationInfo
    {
        [XmlElement(ElementName = "InsuredOrPrincipal")]
        public InsuredOrPrincipal InsuredOrPrincipal { get; set; }
        [XmlElement(ElementName = "Addr")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "ResidenceOwnedRentedCd")]
        public string ResidenceOwnedRentedCd { get; set; }
        [XmlElement(ElementName = "ResidenceTypeCd")]
        public string ResidenceTypeCd { get; set; }
    }

    [XmlRoot(ElementName = "DriverVeh")]
    public class DriverVeh
    {
        [XmlElement(ElementName = "UsePct")]
        public string UsePct { get; set; }
        [XmlAttribute(AttributeName = "DriverRef")]
        public string DriverRef { get; set; }
        [XmlAttribute(AttributeName = "VehRef")]
        public string VehRef { get; set; }
    }

    [XmlRoot(ElementName = "PersPolicy")]
    public class PersPolicy
    {
        [XmlElement(ElementName = "LOBCd")]
        public string LOBCd { get; set; }
        [XmlElement(ElementName = "LOBSubCd")]
        public string LOBSubCd { get; set; }
        [XmlElement(ElementName = "OtherOrPriorPolicy")]
        public OtherOrPriorPolicy OtherOrPriorPolicy { get; set; }
        [XmlElement(ElementName = "ContractTerm")]
        public ContractTerm ContractTerm { get; set; }
        [XmlElement(ElementName = "CurrentTermAmt")]
        public CurrentTermAmt CurrentTermAmt { get; set; }
        [XmlElement(ElementName = "OtherInsuranceWithCompanyCd")]
        public string OtherInsuranceWithCompanyCd { get; set; }
        [XmlElement(ElementName = "PaymentOption")]
        public List<PaymentOption> PaymentOption { get; set; }
        [XmlElement(ElementName = "QuoteInfo")]
        public QuoteInfo QuoteInfo { get; set; }
        [XmlElement(ElementName = "CreditScoreInfo")]
        public CreditScoreInfo CreditScoreInfo { get; set; }
        [XmlElement(ElementName = "QuestionAnswer")]
        public List<QuestionAnswer> QuestionAnswer { get; set; }
        [XmlElement(ElementName = "PersApplicationInfo")]
        public PersApplicationInfo PersApplicationInfo { get; set; }
        [XmlElement(ElementName = "DriverVeh")]
        public DriverVeh DriverVeh { get; set; }
        [XmlElement(ElementName = "CompanyProductCd")]
        public string CompanyProductCd { get; set; }
        [XmlElement(ElementName = "AccidentViolation")]
        public List<AccidentViolation> AccidentViolation { get; set; }
    }

    [XmlRoot(ElementName = "Location")]
    public class Location
    {
        [XmlElement(ElementName = "ItemIdInfo")]
        public string ItemIdInfo { get; set; }
        [XmlElement(ElementName = "Addr")]
        public Addr Addr { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "FormatCurrencyAmt")]
    public class FormatCurrencyAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
    }

    [XmlRoot(ElementName = "NumericValue")]
    public class NumericValue
    {
        [XmlElement(ElementName = "FormatCurrencyAmt")]
        public FormatCurrencyAmt FormatCurrencyAmt { get; set; }
    }

    [XmlRoot(ElementName = "CreditOrSurcharge")]
    public class CreditOrSurcharge
    {
        [XmlElement(ElementName = "CreditSurchargeCd")]
        public string CreditSurchargeCd { get; set; }
        [XmlElement(ElementName = "NumericValue")]
        public NumericValue NumericValue { get; set; }
    }

    [XmlRoot(ElementName = "Coverage")]
    public class Coverage
    {
        [XmlElement(ElementName = "CoverageCd")]
        public string CoverageCd { get; set; }
        [XmlElement(ElementName = "CoverageDesc")]
        public string CoverageDesc { get; set; }
        [XmlElement(ElementName = "CreditOrSurcharge")]
        public CreditOrSurcharge CreditOrSurcharge { get; set; }
        [XmlElement(ElementName = "CurrentTermAmt")]
        public CurrentTermAmt CurrentTermAmt { get; set; }
        [XmlElement(ElementName = "Limit")]
        public Limit Limit { get; set; }
        [XmlElement(ElementName = "Option")]
        public Option Option { get; set; }
    }

    [XmlRoot(ElementName = "LengthTimeEmployed")]
    public class LengthTimeEmployed
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "LengthTimeCurrentOccupation")]
    public class LengthTimeCurrentOccupation
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "MiscPartyInfo")]
    public class MiscPartyInfo
    {
        [XmlElement(ElementName = "MiscPartyRoleCd")]
        public string MiscPartyRoleCd { get; set; }
    }

    [XmlRoot(ElementName = "MiscParty")]
    public class MiscParty
    {
        [XmlElement(ElementName = "GeneralPartyInfo")]
        public GeneralPartyInfo GeneralPartyInfo { get; set; }
        [XmlElement(ElementName = "MiscPartyInfo")]
        public MiscPartyInfo MiscPartyInfo { get; set; }
    }

    [XmlRoot(ElementName = "License")]
    public class License
    {
        [XmlElement(ElementName = "LicenseStatusCd")]
        public string LicenseStatusCd { get; set; }
        [XmlElement(ElementName = "LicensedDt")]
        public string LicensedDt { get; set; }
        [XmlElement(ElementName = "LicensePermitNumber")]
        public string LicensePermitNumber { get; set; }
        [XmlElement(ElementName = "LicenseClassCd")]
        public string LicenseClassCd { get; set; }
        [XmlElement(ElementName = "StateProvCd")]
        public string StateProvCd { get; set; }
    }

    [XmlRoot(ElementName = "DriversLicense")]
    public class DriversLicense
    {
        [XmlElement(ElementName = "LicensedDt")]
        public string LicensedDt { get; set; }
        [XmlElement(ElementName = "DriversLicenseNumber")]
        public string DriversLicenseNumber { get; set; }
        [XmlElement(ElementName = "LicenseClassCd")]
        public string LicenseClassCd { get; set; }
        [XmlElement(ElementName = "StateProvCd")]
        public string StateProvCd { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "DriverInfo")]
    public class DriverInfo
    {
        [XmlElement(ElementName = "PersonInfo")]
        public PersonInfo PersonInfo { get; set; }
        [XmlElement(ElementName = "License")]
        public License License { get; set; }
        [XmlElement(ElementName = "DriversLicense")]
        public DriversLicense DriversLicense { get; set; }
        [XmlElement(ElementName = "QuestionAnswer")]
        public string QuestionAnswer { get; set; }
    }

    [XmlRoot(ElementName = "FilingFeeAmt")]
    public class FilingFeeAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
    }

    [XmlRoot(ElementName = "FinancialResponsibilityFiling")]
    public class FinancialResponsibilityFiling
    {
        [XmlElement(ElementName = "FilingCd")]
        public string FilingCd { get; set; }
        [XmlElement(ElementName = "FilingFeeAmt")]
        public FilingFeeAmt FilingFeeAmt { get; set; }
        [XmlElement(ElementName = "NameInfo")]
        public NameInfo NameInfo { get; set; }
        [XmlElement(ElementName = "FilingOriginalDt")]
        public string FilingOriginalDt { get; set; }
        [XmlElement(ElementName = "FilingReasonCd")]
        public string FilingReasonCd { get; set; }
        [XmlElement(ElementName = "StateProvCd")]
        public string StateProvCd { get; set; }
        [XmlElement(ElementName = "FilingStatusCd")]
        public string FilingStatusCd { get; set; }
    }

    [XmlRoot(ElementName = "PersDriverInfo")]
    public class PersDriverInfo
    {
        [XmlElement(ElementName = "DefensiveDriverCd")]
        public string DefensiveDriverCd { get; set; }
        [XmlElement(ElementName = "com.Arrowhead.Workloss")]
        public string IsWorkLoss { get; set; }
        [XmlElement(ElementName = "DriverRelationshipToApplicantCd")]
        public string DriverRelationshipToApplicantCd { get; set; }
        [XmlElement(ElementName = "DriverTrainingInd")]
        public string DriverTrainingInd { get; set; }
        [XmlElement(ElementName = "DriverTypeCd")]
        public string DriverTypeCd { get; set; }
        [XmlElement(ElementName = "FinancialResponsibilityFiling")]
        public FinancialResponsibilityFiling FinancialResponsibilityFiling { get; set; }
        [XmlElement(ElementName = "MatureDriverInd")]
        public string MatureDriverInd { get; set; }
        [XmlElement(ElementName = "DistantStudentInd")]
        public string DistantStudentInd { get; set; }
        [XmlElement(ElementName = "GoodStudentCd")]
        public string GoodStudentCd { get; set; }
        [XmlElement(ElementName = "GoodDriverInd")]
        public string GoodDriverInd { get; set; }
        [XmlElement(ElementName = "Military")]
        public string Military { get; set; }
        [XmlElement(ElementName = "MembershipClub")]
        public string MembershipClub { get; set; }
    }

    [XmlRoot(ElementName = "PersDriver")]
    public class PersDriver
    {
        [XmlElement(ElementName = "GeneralPartyInfo")]
        public GeneralPartyInfo GeneralPartyInfo { get; set; }
        [XmlElement(ElementName = "DriverInfo")]
        public DriverInfo DriverInfo { get; set; }
        [XmlElement(ElementName = "PersDriverInfo")]
        public PersDriverInfo PersDriverInfo { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "FullTermAmt")]
    public class FullTermAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
    }

    [XmlRoot(ElementName = "DistanceOneWay")]
    public class DistanceOneWay
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "Amt")]
    public class Amt
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "CostNewAmt")]
    public class CostNewAmt
    {
        [XmlElement(ElementName = "Amt")]
        public Amt Amt { get; set; }
    }

    [XmlRoot(ElementName = "Model")]
    public class Model
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "EstimatedAnnualDistance")]
    public class EstimatedAnnualDistance
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "OdometerReading")]
    public class OdometerReading
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "VehUseCd")]
    public class VehUseCd
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "Limit")]
    public class Limit
    {
        [XmlElement(ElementName = "FormatInteger")]
        public string FormatInteger { get; set; }
        [XmlElement(ElementName = "LimitAppliesToCd")]
        public string LimitAppliesToCd { get; set; }
    }

    [XmlRoot(ElementName = "Option")]
    public class Option
    {
        [XmlElement(ElementName = "OptionTypeCd")]
        public string OptionTypeCd { get; set; }
    }

    [XmlRoot(ElementName = "PersVeh")]
    public class PersVeh
    {
        [XmlElement(ElementName = "ModelYear")]
        public string ModelYear { get; set; }
        [XmlElement(ElementName = "FullTermAmt")]
        public FullTermAmt FullTermAmt { get; set; }
        [XmlElement(ElementName = "VehIdentificationNumber")]
        public string VehIdentificationNumber { get; set; }
        [XmlElement(ElementName = "VehSymbolCd")]
        public string VehSymbolCd { get; set; }
        [XmlElement(ElementName = "DaytimeRunningLightInd")]
        public string DaytimeRunningLightInd { get; set; }
        [XmlElement(ElementName = "DistanceOneWay")]
        public DistanceOneWay DistanceOneWay { get; set; }
        [XmlElement(ElementName = "CostNewAmt")]
        public CostNewAmt CostNewAmt { get; set; }
        [XmlElement(ElementName = "AntiTheftDeviceCd")]
        public string AntiTheftDeviceCd { get; set; }
        [XmlElement(ElementName = "VehBodyTypeCd")]
        public string VehBodyTypeCd { get; set; }
        [XmlElement(ElementName = "Manufacturer")]
        public string Manufacturer { get; set; }
        [XmlElement(ElementName = "Model")]
        public Model Model { get; set; }
        [XmlElement(ElementName = "VehPerformanceCd")]
        public string VehPerformanceCd { get; set; }
        [XmlElement(ElementName = "AirBagTypeCd")]
        public string AirBagTypeCd { get; set; }
        [XmlElement(ElementName = "SeatBeltTypeCd")]
        public string SeatBeltTypeCd { get; set; }
        [XmlElement(ElementName = "AntiLockBrakeCd")]
        public string AntiLockBrakeCd { get; set; }
        [XmlElement(ElementName = "EstimatedAnnualDistance")]
        public EstimatedAnnualDistance EstimatedAnnualDistance { get; set; }
        [XmlElement(ElementName = "LeasedVehInd")]
        public string LeasedVehInd { get; set; }
        [XmlElement(ElementName = "LicensePlateNumber")]
        public string LicensePlateNumber { get; set; }
        [XmlElement(ElementName = "NumCylinders")]
        public string NumCylinders { get; set; }
        [XmlElement(ElementName = "PurchaseDt")]
        public string PurchaseDt { get; set; }
        [XmlElement(ElementName = "TerritoryCd")]
        public string TerritoryCd { get; set; }
        [XmlElement(ElementName = "EngineTypeCd")]
        public string EngineTypeCd { get; set; }
        [XmlElement(ElementName = "GaragingCd")]
        public string GaragingCd { get; set; }
        [XmlElement(ElementName = "MultiCarDiscountInd")]
        public string MultiCarDiscountInd { get; set; }
        [XmlElement(ElementName = "NewVehInd")]
        public string NewVehInd { get; set; }
        [XmlElement(ElementName = "NonOwnedVehInd")]
        public string NonOwnedVehInd { get; set; }
        [XmlElement(ElementName = "OdometerReading")]
        public OdometerReading OdometerReading { get; set; }
        [XmlElement(ElementName = "PrincipalOperatorInd")]
        public string PrincipalOperatorInd { get; set; }
        [XmlElement(ElementName = "SeenCarInd")]
        public string SeenCarInd { get; set; }
        [XmlElement(ElementName = "VehUseCd")]
        public VehUseCd VehUseCd { get; set; }
        [XmlElement(ElementName = "FourWheelDriveInd")]
        public string FourWheelDriveInd { get; set; }
        [XmlElement(ElementName = "QuestionAnswer")]
        public QuestionAnswer QuestionAnswer { get; set; }
        [XmlElement(ElementName = "Coverage")]
        public List<Coverage> Coverage { get; set; }
        [XmlAttribute(AttributeName = "LocationRef")]
        public string LocationRef { get; set; }
        [XmlAttribute(AttributeName = "RatedDriverRef")]
        public string RatedDriverRef { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "AdditionalInterest")]
        public AdditionalInterest AdditionalInterest { get; set; }
    }

    [XmlRoot(ElementName = "PersAutoLineBusiness")]
    public class PersAutoLineBusiness
    {
        [XmlElement(ElementName = "LOBCd")]
        public string LOBCd { get; set; }
        [XmlElement(ElementName = "LOBSubCd")]
        public string LOBSubCd { get; set; }
        [XmlElement(ElementName = "CompanyURL")]
        public string CompanyURL { get; set; }
        [XmlElement(ElementName = "Coverage")]
        public List<Coverage> Coverage { get; set; }
        [XmlElement(ElementName = "PersDriver")]
        public List<PersDriver> PersDriver { get; set; }
        [XmlElement(ElementName = "PersVeh")]
        public List<PersVeh> PersVeh { get; set; }
    }

    [XmlRoot(ElementName = "PolicySummaryInfo")]
    public class PolicySummaryInfo
    {
        [XmlElement(ElementName = "FullTermAmt")]
        public FullTermAmt FullTermAmt { get; set; }
        [XmlElement(ElementName = "PolicyStatusCd")]
        public string PolicyStatusCd { get; set; }
    }

    [XmlRoot(ElementName = "PersAutoPolicyQuoteInqRs")]
    public class PersAutoPolicyQuoteInqRs
    {
        [XmlElement(ElementName = "RqUID")]
        public string RqUID { get; set; }
        [XmlElement(ElementName = "TransactionResponseDt")]
        public string TransactionResponseDt { get; set; }
        [XmlElement(ElementName = "TransactionEffectiveDt")]
        public string TransactionEffectiveDt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
        [XmlElement(ElementName = "MsgStatus")]
        public string MsgStatus { get; set; }
        [XmlElement(ElementName ="MsgStatusDesc")]
        public string MsgStatusDesc { get; set; }
        [XmlElement(ElementName = "Producer")]
        public Producer Producer { get; set; }
        [XmlElement(ElementName = "InsuredOrPrincipal")]
        public InsuredOrPrincipal InsuredOrPrincipal { get; set; }
        [XmlElement(ElementName = "PersPolicy")]
        public PersPolicy PersPolicy { get; set; }
        [XmlElement(ElementName = "Location")]
        public Location Location { get; set; }
        [XmlElement(ElementName = "PersAutoLineBusiness")]
        public PersAutoLineBusiness PersAutoLineBusiness { get; set; }
        [XmlElement(ElementName = "PolicySummaryInfo")]
        public PolicySummaryInfo PolicySummaryInfo { get; set; }
    }

    [XmlRoot(ElementName = "InsuranceSvcRs")]
    public class InsuranceSvcRs
    {
        [XmlElement(ElementName = "RqUID")]
        public string RqUID { get; set; }
        [XmlElement(ElementName = "Status")]
        public Status Status { get; set; }
        [XmlElement(ElementName = "PersAutoPolicyQuoteInqRs")]
        public PersAutoPolicyQuoteInqRs PersAutoPolicyQuoteInqRs { get; set; }
    }

    [XmlRoot(ElementName = "ACORD")]
    public class ACORD
    {
        [XmlElement(ElementName = "SignonRs")]
        public SignonRs SignonRs { get; set; }
        [XmlElement(ElementName = "InsuranceSvcRs")]
        public InsuranceSvcRs InsuranceSvcRs { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string NoNamespaceSchemaLocation { get; set; }
    }

    [XmlRoot(ElementName = "OtherOrPriorPolicy")]
    public class OtherOrPriorPolicy
    {
        [XmlElement(ElementName = "PolicyCd")]
        public string PolicyCd { get; set; }
        [XmlElement(ElementName = "PolicyNumber")]
        public string PolicyNumber { get; set; }
        [XmlElement(ElementName = "LOBCd")]
        public string LOBCd { get; set; }
        [XmlElement(ElementName = "NAICCd")]
        public string NAICCd { get; set; }
        [XmlElement(ElementName = "InsurerName")]
        public string InsurerName { get; set; }
        [XmlElement(ElementName = "ContractTerm")]
        public ContractTerm ContractTerm { get; set; }
        [XmlElement(ElementName = "PolicyTransferInd")]
        public string PolicyTransferInd { get; set; }
        
        [XmlElement(ElementName = "LengthTimeWithPreviousInsurer")]
        public LengthTimeWithPreviousInsurer LengthTimeWithPreviousInsurer { get; set; }
        [XmlElement(ElementName = "Coverage")]
        public List<Coverage> Coverage { get; set; }
    }

    [XmlRoot(ElementName = "LengthTimeWithPreviousInsurer")]
    public class LengthTimeWithPreviousInsurer
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

    [XmlRoot(ElementName = "AdditionalInterest")]
    public class AdditionalInterest
    {
        [XmlElement(ElementName = "NatureInterestCd")]
        public string NatureInterestCd { get; set; }
        [XmlElement(ElementName = "GeneralPartyInfo")]
        public GeneralPartyInfo GeneralPartyInfo { get; set; }
    }


    [XmlRoot(ElementName = "AccidentViolation")]
    public class AccidentViolation
    {
        [XmlAttribute]
        public string id;
        [XmlAttribute]
        public string DriverRef;
        [XmlElement(ElementName = "AccidentViolationCd")]
        public string AccidentViolationCd { get; set; }
        [XmlElement(ElementName = "AccidentViolationDt")]
        public string AccidentViolationDt { get; set; }
        [XmlElement(ElementName = "BIInd")]
        public string BIInd { get; set; }
        [XmlElement(ElementName = "AccidentViolationDesc")]
        public string AccidentViolationDesc { get; set; }
        [XmlElement(ElementName = "ConvictionDt")]
        public string ConvictionDt { get; set; }
        [XmlElement(ElementName = "PDInd")]
        public string PDInd { get; set; }
        [XmlElement(ElementName = "ExcessSpeed")]
        public ExcessSpeed ExcessSpeed { get; set; }

        [XmlElement(ElementName = "NumSurchargePoints")]
        public int NumSurchargePoints { get; set; }


    }

    [XmlRoot(ElementName = "ExcessSpeed")]
    public class ExcessSpeed
    {
        [XmlElement(ElementName = "NumUnits")]
        public string NumUnits { get; set; }
        [XmlElement(ElementName = "UnitMeasurementCd")]
        public string UnitMeasurementCd { get; set; }
    }

}
