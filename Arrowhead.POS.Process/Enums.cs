﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Process
{
    internal class Enums
    {
        public enum AddressType
        {
            StreetAddress,
            MailingAddress,
            GaragingAddress
        }

        public enum CommunicationUse
        {
            Business,
            Alternate,
            Home,
            Day
        }

        public enum XMLAttribute
        {
            Veh,
            Loc,
            Drv,
            Vio_,
            DriverRef,
            Gar1
        }


        public enum UnitMeasurement
        {
            MON,
            Months,
            Years,
            Miles
        }

        public enum TaxIdType
        {
            SSN
        }

        public enum LicenseType
        {
            Driver
        }

        public enum CoverageCode
        {
            BI,
            PD,
            COMP,
            COLL,
            MEDP,
            UM,
            UMPD,
            UMBI,
            TL,
            LPD,
            PPI,
            PIP,
            LUSE,
            UIMBI, //Under Insured Body Injury
            //UIBI,    //Under Insured Bodily Injury
            UIMPD,   //Under Insured Property Damage
            //UIPD    //Under Insured Property Damage
            UMPDD,
            POLFE,  //Policy Fee
            MVRFee,
            UMFee,
            SR22Fee,
            SR22AFee
        }

        public enum CoverageLimitAppliedCode
        {
            PerPerson,
            PerAccident,
            PerAcc,
            PropDam,
            PDEachOcc,
            MedExp
        }

        public enum LicenseStatus
        {
            Active,
            InActive
        }

        public enum ResidenceType
        {
            OWNED,
            RENTD
        }

        public enum Question
        {
            GENRL14,
            AUTOP05,
            AUPMA12,
            Current_Insurance_Value
        }

        public enum QuestionAnswer
        {
            yes,
            No,
            NC
        }

        public enum IsEnsured
        {
            Yes,
            No
        }
    }
}
