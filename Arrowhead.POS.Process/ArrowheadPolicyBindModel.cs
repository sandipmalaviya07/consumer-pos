﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Process
{
    public class ArrowheadPolicyBindModel
    {
        public long QuoteID { get; set; }
        public string CustomerName { get; set; }
        public string EnvelopID { get; set; }
        public string QuoteResponseXML { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<ArrowheadQuestionModel> questionModels { get; set; }
        public ArrowheadRateModel quoteRateResponse { get; set; }
        public string PaymentMethodCd { get; set; }
        public string ConfirmationCode { get; set; }
        public string PayPlanId { get; set; }

        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public bool IsNSDPlanSelected { get; set; }
    }

    public class ArrowheadQuestionModel
    {
        public int OrderNo { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }

    public class ArrowheadRateModel
    {
        public long RateId { get; set; }
        public string RateTransactionID { get; set; }
        public string CarrierQuoteID { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public decimal DownPayment { get; set; }
        public decimal MonthlyPayment { get; set; }
        public string BridgeURL { get; set; }
        public string Description { get; set; }
        public decimal TotalPremium { get; set; }
        public int PolicyTerm { get; set; }
        public int NoOfInstallement { get; set; }
    }
}
