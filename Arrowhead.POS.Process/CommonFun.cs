﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Process
{
    public class CommonFun
    {
        public static string FormateDate(DateTime dateVal)
        {
            return dateVal.ToString("yyyy-MM-dd");
        }

        public static string FormateBirthDate(DateTime dateVal)
        {
            return dateVal.ToString("MMM d, yyyy");
        }

        public static string FormateDeductiableAmount(string amount)
        {
            string returnVal = string.Empty;
            try
            {
                string[] arr = Convert.ToString(amount).Split('.');
                if (arr != null)
                    returnVal = arr[0];
            }
            catch (Exception ex) { }
            return returnVal;
        }

        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
    }

}
