﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Process
{
    internal class ConstantVariable
    {
        public const string InsuredOrPrincipalRole = "Insured";
        public const string LOBCd = "AUTOP";
        public const string xmlns = "http://www.ACORD.org/standards/PC_Surety/ACORD1.12.0/xml/";
        public const string PolicyCd = "Prior";
        public const string AutoBusinessCoverage = "WGPPO";
        public const string AutoBusinessCoverageDesc = "PPO Program";
        public const string AutoBusinessCoverageCreditSurcharge = "ITCCSOTHER";
        public const string TaxIdType = "SSN";
        public const string MiscPartyRole = "Employer";
        public const string VehBodyType = "PP";
        public const string VehPerformance = "BASIC";
        public const string SeatBeltType = "PassBoth";
        public const string AirBagType = "FrontBoth";
        public const string GaragingCode = "N";
        public const string MultiCarDiscount = "1";
        public const string AntiTheftDeviceCode = "y";
        public const string VehUseCode = "DW";
        public const string PrincipalOperator = "1";
        public const string PhoneType = "Phone";
        public const string LeanHolderNatureInterest = "LOSSP";
        public const string BICoverageDesc = "Bodily Injury Liability";
        public const string PDCoverageDesc = "Property Damage-Single Limit";
        public const string UMPDDCoverageDesc = "Uninsured Motorist Property Damage Deductible";
        public const string COMPCoverageDesc = "Comprehensive Coverage";
        public const string TLCoverageDesc = "Towing And Labor";
        public const string UMCoverageDesc = "Uninsured Motorist Liability";
        public const string COLLCoverageDesc = "Collision";
        public const string MEDPCoverageDesc = "Medical Payments";
        public const string UIMBDCoverageDesc = "Underinsured Motorist Bodily Injury";
        public const string UIMPDCoverageDesc = "Underinsured Motorist Propery Damage";
        public const string LPDCoverageDesc = "Limited Property Damage Liability (MI)";
        public const string UMBICoverageDesc = "Uninsured Motorist Liability";
        public const string PPICoverageDesc = "Property Protection Insurance";
        public const string PIPCoverageDesc = "Personal Injury Protection (MI)";
        public const string LOUCoverageDesc = "Lose of Use";
        public const string UMOptionTypeCode = "NT";
        public const string COLLOptionTypeCode1 = "Misc1";
        public const string COLLOptionTypeCode2 = "B";
        public const string UMPDCoverageDesc = "Uninsured Motorist Property Damage";
        public const int DedctableAmount = 0;
        public const string strDedctableAmount = "0";
        public const string DriverLengthTimeDuration = "60";
        public const string DriverOccupationCode = "60";
        public const string DefensiveDriverCode = "N";
        public const string MatureDriverInd = "N";
        public const string PolicyContractTermIND = "1";
        public const string Amount = "0";
        public const string Currency = "USD";

        public const string PayPlanID = "102";
        public const string MethodPaymentCd = "CASH";
        
    }

}
