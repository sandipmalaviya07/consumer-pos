﻿using Arrowhead.POS.Process;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Arrowhead.POS.ProcessResponse
{
    class ArrowheadRespModel
    {
    }

    [XmlRoot(ElementName = "PersAutoPolicyQuoteInqRs")]
    public class RespPersAutoPolicyQuoteInqRs
    {
        [XmlElement(ElementName = "RqUID")]
        public string RqUID { get; set; }
        [XmlElement(ElementName = "TransactionResponseDt")]
        public string TransactionResponseDt { get; set; }
        [XmlElement(ElementName = "TransactionEffectiveDt")]
        public string TransactionEffectiveDt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
        [XmlElement(ElementName = "MsgStatus")]
        
        public MsgStatus MsgStatus { get; set; }
        public Producer Producer { get; set; }
        [XmlElement(ElementName = "InsuredOrPrincipal")]
        public InsuredOrPrincipal InsuredOrPrincipal { get; set; }
        [XmlElement(ElementName = "PersPolicy")]
        public ResPersPolicy PersPolicy { get; set; }
        [XmlElement(ElementName = "Location")]
        public Location Location { get; set; }
        [XmlElement(ElementName = "PersAutoLineBusiness")]
        public ResPPersAutoLineBusiness PersAutoLineBusiness { get; set; }
        [XmlElement(ElementName = "PolicySummaryInfo")]
        public PolicySummaryInfo PolicySummaryInfo { get; set; }

    }

    [XmlRoot(ElementName = "PersAutoLineBusiness")]
    public class ResPPersAutoLineBusiness
    {
        [XmlElement(ElementName = "CompanyURL")]
        public string CompanyURL { get; set; }
        [XmlElement(ElementName = "PersVeh")]
        public List<RespPersVeh> PersVeh { get; set; }

        [XmlElement(ElementName = "Coverage")]
        public List<RespCoverage> Coverage { get; set; }
    }

    [XmlRoot(ElementName = "FullTermAmt")]
    public class RespFullTermAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
    }    

    [XmlRoot(ElementName = "Amt")]
    public class Amt
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }  

    [XmlRoot(ElementName = "Model")]
    public class Model
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }      

    [XmlRoot(ElementName = "VehUseCd")]
    public class VehUseCd
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }      

    [XmlRoot(ElementName = "CurrentTermAmt")]
    public class CurrentTermAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
    }

    [XmlRoot(ElementName = "Coverage")]
    public class RespCoverage
    {
        [XmlElement(ElementName = "CoverageCd")]
        public string CoverageCd { get; set; }
        [XmlElement(ElementName = "CoverageDesc")]
        public string CoverageDesc { get; set; }
        [XmlElement(ElementName = "Limit")]
        public List<Limit> Limit { get; set; }
        [XmlElement(ElementName = "CurrentTermAmt")]
        public CurrentTermAmt CurrentTermAmt { get; set; }
        [XmlElement(ElementName = "Option")]
        public Option _option { get; set; }
        [XmlElement(ElementName = "CreditOrSurcharge")]
        public CreditOrSurcharge CreditOrSurcharge { get; set; }
    }

    [XmlRoot(ElementName = "CreditOrSurcharge")]
    public class CreditOrSurcharge
    {
        [XmlElement(ElementName = "CreditSurchargeCd")]
        public string CreditSurchargeCd { get; set; }
        [XmlElement(ElementName = "NumericValue")]
        public NumericValue NumericValue { get; set; }
    }

    [XmlRoot(ElementName = "FormatCurrencyAmt")]
    public class NumericValue
    {
        public FormatCurrencyAmt FormatCurrencyAmt { get; set; }
    }

    [XmlRoot(ElementName = "PersVeh")]
    public class RespPersVeh
    {
        [XmlElement(ElementName = "ModelYear")]
        public string ModelYear { get; set; }
        [XmlElement(ElementName = "FullTermAmt")]
        public FullTermAmt FullTermAmt { get; set; }
        [XmlElement(ElementName = "VehIdentificationNumber")]
        public string VehIdentificationNumber { get; set; }
        [XmlElement(ElementName = "VehSymbolCd")]
        public string VehSymbolCd { get; set; }
        [XmlElement(ElementName = "DaytimeRunningLightInd")]
        public string DaytimeRunningLightInd { get; set; }
        [XmlElement(ElementName = "DistanceOneWay")]
        public DistanceOneWay DistanceOneWay { get; set; }
        [XmlElement(ElementName = "CostNewAmt")]
        public CostNewAmt CostNewAmt { get; set; }
        [XmlElement(ElementName = "AntiTheftDeviceCd")]
        public string AntiTheftDeviceCd { get; set; }
        [XmlElement(ElementName = "VehBodyTypeCd")]
        public string VehBodyTypeCd { get; set; }
        [XmlElement(ElementName = "Manufacturer")]
        public string Manufacturer { get; set; }
        [XmlElement(ElementName = "Model")]
        public Model Model { get; set; }
        [XmlElement(ElementName = "VehPerformanceCd")]
        public string VehPerformanceCd { get; set; }
        [XmlElement(ElementName = "AirBagTypeCd")]
        public string AirBagTypeCd { get; set; }
        [XmlElement(ElementName = "SeatBeltTypeCd")]
        public string SeatBeltTypeCd { get; set; }
        [XmlElement(ElementName = "AntiLockBrakeCd")]
        public string AntiLockBrakeCd { get; set; }
        [XmlElement(ElementName = "EstimatedAnnualDistance")]
        public EstimatedAnnualDistance EstimatedAnnualDistance { get; set; }
        [XmlElement(ElementName = "LeasedVehInd")]
        public string LeasedVehInd { get; set; }
        [XmlElement(ElementName = "LicensePlateNumber")]
        public string LicensePlateNumber { get; set; }
        [XmlElement(ElementName = "NumCylinders")]
        public string NumCylinders { get; set; }
        [XmlElement(ElementName = "PurchaseDt")]
        public string PurchaseDt { get; set; }
        [XmlElement(ElementName = "TerritoryCd")]
        public string TerritoryCd { get; set; }
        [XmlElement(ElementName = "EngineTypeCd")]
        public string EngineTypeCd { get; set; }
        [XmlElement(ElementName = "GaragingCd")]
        public string GaragingCd { get; set; }
        [XmlElement(ElementName = "MultiCarDiscountInd")]
        public string MultiCarDiscountInd { get; set; }
        [XmlElement(ElementName = "NewVehInd")]
        public string NewVehInd { get; set; }
        [XmlElement(ElementName = "NonOwnedVehInd")]
        public string NonOwnedVehInd { get; set; }
        [XmlElement(ElementName = "OdometerReading")]
        public OdometerReading OdometerReading { get; set; }
        [XmlElement(ElementName = "PrincipalOperatorInd")]
        public string PrincipalOperatorInd { get; set; }
        [XmlElement(ElementName = "SeenCarInd")]
        public string SeenCarInd { get; set; }
        [XmlElement(ElementName = "VehUseCd")]
        public VehUseCd VehUseCd { get; set; }
        [XmlElement(ElementName = "FourWheelDriveInd")]
        public string FourWheelDriveInd { get; set; }
        [XmlElement(ElementName = "QuestionAnswer")]
        public QuestionAnswer QuestionAnswer { get; set; }
        [XmlElement(ElementName = "Coverage")]
        public List<RespCoverage> Coverage { get; set; }
        [XmlAttribute(AttributeName = "LocationRef")]
        public string LocationRef { get; set; }
        [XmlAttribute(AttributeName = "RatedDriverRef")]
        public string RatedDriverRef { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }
}
