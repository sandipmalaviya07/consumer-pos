﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Process
{
    public class AutoRequestModel
    {
        public int Api_Version { get; set; }
        public long QuoteID { get; set; }
        public string Api_Key { get; set; }
        public AutoPolicy AutoPolicy { get; set; }
        public List<AutoDriver> AutoDrivers { get; set; }
        public List<AutoVehicle> AutoVehicles { get; set; }
        public AutoCoverage Coverage { get; set; }
    }

    public class AutoPolicy
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ContactNo { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public string OtherContactNo { get; set; }
        public string EmailAddress { get; set; }
        public string Street_Address1 { get; set; }
        public string Street_Address2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string StateCd { get; set; }
        public string County { get; set; }
        public string MailingStreet_Address1 { get; set; }
        public string MailingStreet_Address2 { get; set; }
        public string MailingZipCode { get; set; }
        public string MailingCity { get; set; }
        public string MailingStateCd { get; set; }
        public string CreditScore { get; set; }
        public string MailingCounty { get; set; }
        public int PolicyTerm { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Dictionary<string, string> Coverage_Level { get; set; }
        public DateTime CreatedDate { get; set; }
        public AutoPriorInsurer PriorInsurer { get; set; }

        public long? FkHomeTypeId { get; set; }
        public string FkHomeTypeCode { get; set; }
    }

    public class AutoDriver
    {
        public int Id { get; set; }
        public string License_StateCd { get; set; }
        public string License_Number { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ContactNo { get; set; }
        public string License_Status { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool HomeOwner { get; set; }
        public string Gender { get; set; }
        public string Marital_Status { get; set; }
        public bool Education_Completed { get; set; }
        public int Age_First_Licensed { get; set; }
        public bool IsSR22 { get; set; }
        public bool IsSR22A { get; set; }
        public string SR22CaseNumber { get; set; }
        public string SR22Reason { get; set; }
        public string SR22State { get; set; }
        public string SSN { get; set; }
        public bool Is_Same_Address_As_Primary { get; set; }
        public bool IsExcluded { get; set; }
        public string Occupation { get; set; }
        public string RelationCd { get; set; }
        public bool IsGoodCredit { get; set; }
        public bool IsGoodStudent { get; set; }
        public DateTime? DriverTrainingDate { get; set; }
        public bool IsDefensiveDriverCourse { get; set; }
        public int? PrimaryVehicleId { get; set; }
        public bool IsWorkLossBenefit { get; set; }
        public List<AutoDriverViolation> AutoDriverViolations { get; set; }

        public long? FkMilitaryTypeId { get; set; }
        public long? FkWarehouseTypeId { get; set; }
    }

    public class AutoDriverViolation
    {
        public int DriverID { get; set; }
        public string ViolationCd { get; set; }
        public DateTime VioDate { get; set; }
        public string Description { get; set; }
        public bool IsMVRAppeal { get; set; }
        public bool AtFault { get; set; } = false;
        public string Location { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZipCode { get; set; }
        public string ViolationName { get; set; }
        public int ViolationPoints { get; set; }
    }

    public class AutoPriorInsurer
    {
        public string InsurerName { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int Duration { get; set; }
        public string IsPreviouslyInsured { get; set; }
        public string PolicyNumber { get; set; }
        public bool Currently_Insured { get; set; }
        public string Current_CarrierName { get; set; }
        public int Current_Insured_Duration { get; set; }
        public string BICoverage { get; set; }

    }

    public class AutoVehicle
    {
        public int Id { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SubModel { get; set; }
        public int AnnualMileage { get; set; }
        public bool IsOwned { get; set; }
        public string PrimaryUse { get; set; }
        public string ComprehensiveDed { get; set; }
        public string CollisionDed { get; set; }
        public string TowingLimit { get; set; }
        public string RentalLimit { get; set; }
        public decimal? CarPurchaseCost { get; set; }
        public DateTime PurchaseDate { get; set; }
        public bool IsMonitoryDevice { get; set; }
        public string NumCylinders { get; set; }
        public string LicensePlateNumber { get; set; }
        public int PrimaryDriverId { get; set; }
        public bool ParkedAtMainAddress { get; set; }
        public string GarageType { get; set; }
        public string GaragingStreet_Address1 { get; set; }
        public string GaragingStreet_Address2 { get; set; }
        public string GaragingCity { get; set; }
        public string GaragingStateCd { get; set; }
        public string GaragingZipCode { get; set; }
        public AutoLienHolder AutoLienHolder { get; set; }
        public string LoseOfUse { get; set; }
        public string CollissionLevel { get; set; }
    }

    public class AutoLienHolder
    {
        public string Name { get; set; }
        public string Street_Address1 { get; set; }
        public string Street_Address2 { get; set; }
        public string City { get; set; }
        public string StateCd { get; set; }
        public string ZipCode { get; set; }
    }

    public class AutoCoverage
    {
        public string BodilyInjuryPerPerson { get; set; }
        public string BodilyInjuryPerAcc { get; set; }
        public string PropertyDamage { get; set; }
        public string LossOfUse { get; set; }
        public string MedicalPayments { get; set; }
        public string UninsuredMotorist { get; set; }
        public string UninsuredBodilyInjuryPerPerson { get; set; }
        public string UninsuredBodilyInjuryPerAcc { get; set; }
        public string UninsuredBodilyInjuryDeductible { get; set; }
        public string UninsuredPropertyDamage { get; set; }
        public string UninsuredPropertyDamageDeductible { get; set; }
        public string UnderinsuredBodilyInjuryPerPerson { get; set; }
        public string UnderinsuredBodilyInjuryPerAcc { get; set; }
        public string UnderinsuredPropertyDamage { get; set; }
        public string PersonalInjuryProtection { get; set; }
        public string PersonalInjuryProtectionDed { get; set; }
        public string AdditionalersonalInjuryProtection { get; set; }
        public string PropertyProtectionInsurance { get; set; }
        public string MedDisplayText { get; set; }
        public string AnnualMileage { get; set; }
        public string Tort { get; set; }
        public string AccidentalDeath { get; set; }
        public string PunitiveDamageExclusion { get; set; }
        public string LimitedPropertyDamageLiability { get; set; }
    }

    public class AutoCoverageDocusign
    {
        public string BodilyInjuryPerPerson { get; set; }
        public string BodilyInjuryPerAcc { get; set; }
        public string LossOfUse { get; set; }
        public string PropertyDamage { get; set; }
        public string MedicalPayments { get; set; }
        public string LimitedPropertyDamage { get; set; }
        public string UninsuredMotorist { get; set; }
        public string UninsuredBodilyInjuryPerPerson { get; set; }
        public string UninsuredBodilyInjuryPerAcc { get; set; }
        public string UninsuredBodilyInjuryDeductible { get; set; }
        public string UninsuredPropertyDamage { get; set; }
        public string UninsuredPropertyDamageDeductible { get; set; }
        public string UnderinsuredBodilyInjuryPerPerson { get; set; }
        public string UnderinsuredBodilyInjuryPerAcc { get; set; }
        public string UnderinsuredPropertyDamage { get; set; }
        public string PersonalInjuryProtection { get; set; }
        public string PersonalInjuryProtectionDetails { get; set; }
        public string PersonalInjuryProtectionDed { get; set; }
        public string AdditionalersonalInjuryProtection { get; set; }
        public string PropertyProtectionInsurance { get; set; }
        public string AnnualMileage { get; set; }
        public string Tort { get; set; }
        public string AccidentalDeath { get; set; }
        public string PunitiveDamageExclusion { get; set; }
        public string Veh1ComprehensiveDed { get; set; }
        public string Veh1BI { get; set; }
        public string Veh2BI { get; set; }
        public string Veh3BI { get; set; }
        public string Veh4BI { get; set; }
        public string Veh1PD { get; set; }
        public string Veh2PD { get; set; }
        public string Veh3PD { get; set; }
        public string Veh4PD { get; set; }
        public string Veh1PIP { get; set; }
        public string Veh2PIP { get; set; }
        public string Veh3PIP { get; set; }
        public string Veh4PIP { get; set; }
        public string Veh1PPI { get; set; }
        public string Veh2PPI { get; set; }
        public string Veh3PPI { get; set; }
        public string Veh4PPI { get; set; }
        public string Veh1UMBI { get; set; }
        public string Veh2UMBI { get; set; }
        public string Veh3UMBI { get; set; }
        public string Veh4UMBI { get; set; }
        public string Veh1Coll { get; set; }
        public string Veh2Coll { get; set; }
        public string Veh3Coll { get; set; }
        public string Veh4Coll { get; set; }
        public string Veh1Comp { get; set; }
        public string Veh2Comp { get; set; }
        public string Veh3Comp { get; set; }
        public string Veh4Comp { get; set; }
        public string Veh2ComprehensiveDed { get; set; }
        public string Veh3ComprehensiveDed { get; set; }
        public string Veh4ComprehensiveDed { get; set; }
        public string Veh1CollisionDed { get; set; }
        public string Veh2CollisionDed { get; set; }
        public string Veh3CollisionDed { get; set; }
        public string Veh4CollisionDed { get; set; }
        public string Veh1TowingLimit { get; set; }
        public string Veh2TowingLimit { get; set; }
        public string Veh3TowingLimit { get; set; }
        public string Veh4TowingLimit { get; set; }
        public string Veh1RentalLimit { get; set; }
        public string Veh2RentalLimit { get; set; }
        public string Veh3RentalLimit { get; set; }
        public string Veh41RentalLimit { get; set; }
        public string Veh1LOS { get; set; }
        public string Veh2LOS { get; set; }
        public string Veh3LOS { get; set; }
        public string Veh4LOS { get; set; }
        public string Veh1SAF { get; set; }
        public string Veh2SAF { get; set; }
        public string Veh3SAF { get; set; }
        public string Veh4SAF { get; set; }
        public string Veh1MCCA { get; set; }
        public string Veh2MCCA { get; set; }
        public string Veh3MCCA { get; set; }
        public string Veh4MCCA { get; set; }
        public string Veh1LPD { get; set; }
        public string Veh2LPD { get; set; }
        public string Veh3LPD { get; set; }
        public string Veh4LPD { get; set; }
        public string InstallmentFees { get; set; }
        public string Veh1CollisionDedType { get; set; }
        public string Veh2CollisionDedType { get; set; }
        public string Veh3CollisionDedType { get; set; }
        public string Veh4CollisionDedType { get; set; }

        //SC required coverages - code by HD
        public string Veh1MEDP { get; set; }
        public string Veh2MEDP { get; set; }
        public string Veh3MEDP { get; set; }
        public string Veh4MEDP { get; set; }
        public string Veh1UIMBI { get; set; }
        public string Veh2UIMBI { get; set; }
        public string Veh3UIMBI { get; set; }
        public string Veh4UIMBI { get; set; }
        public string Veh1UIMPD { get; set; }
        public string Veh2UIMPD { get; set; }
        public string Veh3UIMPD { get; set; }
        public string Veh4UIMPD { get; set; }
        public string Veh1UMPD { get; set; }
        public string Veh2UMPD { get; set; }
        public string Veh3UMPD { get; set; }
        public string Veh4UMPD { get; set; }

        public string MVRFee { get; set; }
        public string PolicyFee { get; set; }
        public string UMFee { get; set; }
    }
}
