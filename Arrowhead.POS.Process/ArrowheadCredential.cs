﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Process
{
    public class ArrowheadCredential
    {
        public string APIPassword { get; set; }
        public string ArrowheadSPName { get; set; }
        public string ClientApp_Org { get; set; }
        public string ClientApp_Name { get; set; }
        public string ClientApp_Version { get; set; }
        public string CommercialName { get; set; }
        public string ProducerSurname { get; set; }
        public string ProducerName { get; set; }
        public string NAICCode { get; set; }
        public string CompanyID { get; set; }
        public string ProducerAddr1 { get; set; }
        public string ProducerAddr2 { get; set; }
        public string ProducerCity { get; set; }
        public string ProducerStateCD { get; set; }
        public string ProducerPhoneNumber { get; set; }
        public string ProducerPostalCode { get; set; }
        public string ContractNumber { get; set; }
        public string ProducerSubCode { get; set; }
        public string EndpointAddress { get; set; }
        //public string AgencyAddress1 { get; set; }
        //public string AgencyAddress2 { get; set; }
        //public string AgencyContactNo { get; set; }
        //public string AgencyName { get; set; }
        //public string AgencyCity { get; set; }
        //public string AgencyState { get; set; }
        //public string AgencyZipCode { get; set; }
    }
}
