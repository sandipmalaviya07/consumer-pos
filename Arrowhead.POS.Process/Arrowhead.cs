﻿using Arrowhead.POS.Process.ArrowHeadRTRProd;
using Arrowhead.POS.ProcessResponse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;

namespace Arrowhead.POS.Process
{
    public class Arrowhead
    {
        public AutoResponseModel GetRates(AutoRequestModel model, ArrowheadCredential credential, string stateCode)
        {
            AutoResponseModel _autoResponseModel = new AutoResponseModel();
            try
            {
                ArrowheadReqModel _request = PrepareArrowheadRequest(model, credential);
                var _policy = AutoCreatePolicy(_request, model, stateCode);
                _autoResponseModel = AutoGetRateResponse(_policy, credential, model.QuoteID);
            }
            catch (Exception)
            {
            }

            return _autoResponseModel;
        }

        private AutoResponseModel AutoGetRateResponse(ArrowheadReqModel _request, ArrowheadCredential _credential, long QuoteID)
        {
            XmlDocument response = new XmlDocument();
            string res = string.Empty;

            XmlDocument xmlRequest = prepareRequestXML(_request, QuoteID);

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xC0 | 0x300 | 0xC00);
            try
            {

                RTRServiceClient client = new RTRServiceClient(); // ArrowHeadRTRProd.RTRService()
                string result = client.GetRate("VLCN916!K*Z%WW ", xmlRequest.OuterXml);
                response.InnerXml = result;
            }
            catch (Exception ex)
            {
            }

            return ProcessResponse(response, xmlRequest.OuterXml);
        }

        public PolicyBindResponseModel BindPolicy(ArrowheadPolicyBindModel policyBindModel)
        {

            XmlDocument xmlResponse = new XmlDocument();
            xmlResponse.LoadXml(policyBindModel.QuoteResponseXML);
            XmlDocument doc = new XmlDocument();
            XmlDocument xml = new XmlDocument();
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xC0 | 0x300 | 0xC00);
            try
            {
                var _requestModel = PreparePolicyBindRequest(policyBindModel);
                xml = CreateBinderXML(_requestModel, xmlResponse);

                XmlNode responseNodes = xml.SelectSingleNode("/ACORD/InsuranceSvcRs");
                responseNodes.ParentNode.RemoveChild(responseNodes);

                RTRServiceClient client = new RTRServiceClient(); // ArrowHeadRTRProd.RTRService()
                string result = client.BindPolicy("VLCN916!K*Z%WW ", xml.OuterXml);
                doc.LoadXml(result.ToString());
            }
            catch (Exception)
            {
            }
            return ProcessBindPolicy(doc, xml.OuterXml);
        }

        public PolicyBindResponseModel ProcessBindPolicy(XmlDocument xmlResponse, string requestXML)
        {
            PolicyBindResponseModel bindResponseModel = new PolicyBindResponseModel();
            try
            {
                XmlNode oNode = xmlResponse.DocumentElement;
                var oPaymentList = oNode.SelectSingleNode("/ACORD/InsuranceSvcRs/PersAutoPolicyQuoteInqRs").OuterXml;
                XmlSerializer serializer = new XmlSerializer(typeof(BinderPersAutoPolicyQuoteInqRs));
                var objResponse = new BinderPersAutoPolicyQuoteInqRs();

                using (TextReader reader1 = new StringReader(oPaymentList.ToString()))
                {
                    try
                    {
                        objResponse = (BinderPersAutoPolicyQuoteInqRs)serializer.Deserialize(reader1);
                    }
                    catch (InvalidOperationException)
                    {
                    }
                }

                if (objResponse.MsgStatus != null)
                {
                    switch (objResponse.MsgStatus.MsgStatusCd)
                    {
                        case "Success":
                            bindResponseModel.IsSuccess = true;
                            bindResponseModel.MessageDescription = objResponse.MsgStatus.MsgStatusDesc;
                            bindResponseModel.PolicyNumber = objResponse.PersPolicy[0].QuoteInfo.CompanysPolicyNumber;
                            break;
                        default:
                            bindResponseModel.IsSuccess = false;
                            bindResponseModel.MessageDescription = objResponse.MsgStatus.MsgStatusDesc;
                            break;
                    }
                }
                else
                {
                    bindResponseModel.IsSuccess = false;
                    bindResponseModel.MessageDescription = objResponse.MsgStatus.MsgStatusDesc;
                }
            }
            catch (Exception ex)
            {
                bindResponseModel.IsSuccess = false;
                bindResponseModel.MessageDescription = ex.ToString();
            }
            bindResponseModel.RequestFile = requestXML;
            bindResponseModel.ResponseFile = xmlResponse.OuterXml;
            return bindResponseModel;
        }

        public string GetPolicyXml(string reqId)
        {
            RTRServiceClient client = new RTRServiceClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var getpolicy = client.GetPolicy("VLCN916!K*Z%WW", reqId);
            return getpolicy;
        }

        private BindPolicy PreparePolicyBindRequest(ArrowheadPolicyBindModel policyBindMode)
        {
            BindPolicy bindPolicy = new BindPolicy();
            try
            {
                BinderInsuranceSvcRq binderInsuranceSvcRq = new BinderInsuranceSvcRq();
                binderInsuranceSvcRq.PersAutoPolicyQuoteInqRq = new BinderPersAutoPolicyQuoteInqRq();

                BinderPersPolicy persPolicy = new BinderPersPolicy();
                BinderQuoteInfo quoteInfo = new BinderQuoteInfo
                {
                    CompanysQuoteNumber = policyBindMode.quoteRateResponse.CarrierQuoteID,
                    Description = "https://www.everestre.com",
                    QuotePreparedDt = CommonFun.FormateDate(policyBindMode.CreatedDate)
                };
                persPolicy.QuoteInfo = quoteInfo;

                List<BinderQuestionAnswer> questionAnswer = new List<BinderQuestionAnswer>();
                BinderQuestionAnswer binderQuestion = new BinderQuestionAnswer();
                if (policyBindMode.questionModels.Count > 0)
                {
                    int _questionCounter = 1;
                    foreach (var item in policyBindMode.questionModels)
                    {
                        binderQuestion = new BinderQuestionAnswer
                        {
                            Id = _questionCounter.ToString(),
                            QuestionCd = item.Code,
                            QuestionDescription = item.Question,
                            YesNoCd = (item.Answer == "Y" ? "1" : "0"),
                            Explanation = item.Description
                        };
                        questionAnswer.Add(binderQuestion);
                        _questionCounter = _questionCounter + 1;
                    }

                    persPolicy.QuestionAnswer = questionAnswer;
                }

                BinderPaymentOption paymentOption = new BinderPaymentOption
                {
                    PayplanId = policyBindMode.PayPlanId,             //ConstantVariable.PayPlanID,
                    MethodPaymentCd = policyBindMode.PaymentMethodCd, // ConstantVariable.MethodPaymentCd,
                    NumPayments = Convert.ToString(policyBindMode.quoteRateResponse.NoOfInstallement),
                    Description = policyBindMode.quoteRateResponse.Description,
                    PaymentPlanCd = policyBindMode.quoteRateResponse.Description.Substring(0, 7)
                };
                BinderDownPaymentAmt downPaymentAmt = new BinderDownPaymentAmt
                {
                    Amt = Convert.ToString(policyBindMode.quoteRateResponse.DownPayment)
                    //,CurCd = ConstantVariable.Currency
                };
                paymentOption.DownPaymentAmt = downPaymentAmt;
                paymentOption.ConfirmationCode = policyBindMode.ConfirmationCode == null ? "" : policyBindMode.ConfirmationCode;
                if (!policyBindMode.IsNSDPlanSelected && policyBindMode.PaymentMethodCd == "CHECK")
                {
                    paymentOption.AccountNumber = policyBindMode.AccountNumber;
                    paymentOption.RoutingNumber = policyBindMode.RoutingNumber;
                }
                persPolicy.PaymentOption = paymentOption;

                BinderElectronicSignature electronicSignature = new BinderElectronicSignature
                {
                    SignedDt = Convert.ToString(policyBindMode.CreatedDate),
                    SignedByCd = policyBindMode.CustomerName,
                    HashCode = !string.IsNullOrEmpty(policyBindMode.EnvelopID) ? policyBindMode.EnvelopID : string.Empty
                };
                persPolicy.ElectronicSignature = electronicSignature;

                binderInsuranceSvcRq.PersAutoPolicyQuoteInqRq.PersPolicy = persPolicy;
                bindPolicy.InsuranceSvcRq = binderInsuranceSvcRq;
            }
            catch (Exception)
            {
            }

            return bindPolicy;
        }

        private XmlDocument CreateBinderXML(BindPolicy binderRequest, XmlDocument xmlResponse)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                var xmlserializer = new XmlSerializer(typeof(BinderInsuranceSvcRq));
                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    xmlserializer.Serialize(writer, binderRequest.InsuranceSvcRq, ns);
                    xmlDoc.InnerXml = stringWriter.ToString();
                }

                XmlDocumentFragment xmlDocFragment = xmlResponse.CreateDocumentFragment();
                xmlDocFragment.InnerXml = xmlDoc.SelectSingleNode("/InsuranceSvcRq").OuterXml;
                var xml = xmlResponse.SelectSingleNode("/ACORD");
                xml.AppendChild(xmlDocFragment);
            }
            catch (Exception)
            {
            }

            return xmlResponse;
        }

        public DriverViolation[] GetArrowheadMVRReport(string policyID)
        {
            DriverViolation[] violation = new DriverViolation[0];
            try
            {
                RTRServiceClient client = new RTRServiceClient();
                violation = client.GetMVR("VLCN916!K*Z%WW ", Convert.ToInt32(policyID));
                //DriverViolation driverViolation = new DriverViolation();
                //violation[0] = new DriverViolation();
                //driverViolation.ConvictionDate = DateTime.Now.AddDays(-300);
                //driverViolation.ViolationDate = DateTime.Now.AddDays(-300);
                //driverViolation.DriverID = 1;
                //driverViolation.StateCode = "NOT";
                //driverViolation.Violation = "NO HIT/NO MATCH/ERROR";
                //violation[0] = driverViolation;

                //driverViolation = new DriverViolation();
                //violation[1] = new DriverViolation();
                //driverViolation.ConvictionDate = DateTime.Now.AddDays(-500);
                //driverViolation.ViolationDate = DateTime.Now.AddDays(-500);
                //driverViolation.DriverID = 2;
                //driverViolation.StateCode = "NOT";
                //driverViolation.Violation = "NO HIT/NO MATCH/ERROR";
                //violation[1] = driverViolation;

                //driverViolation = new DriverViolation();
                //violation[2] = new DriverViolation();
                //driverViolation.ConvictionDate = DateTime.Now.AddDays(-500);
                //driverViolation.ViolationDate = DateTime.Now.AddDays(-500);
                //driverViolation.DriverID = 3;
                //driverViolation.StateCode = "A/C";
                //driverViolation.Violation = "ACCIDENT/CARELESS";
                //violation[2] = driverViolation;

                return violation;
            }
            catch (Exception ex)
            {
                return violation;
            }
        }

        private ArrowheadReqModel AutoCreatePolicy(ArrowheadReqModel _Request, AutoRequestModel model, string stateCode)
        {
            try
            {
                //    if (model.AutoPolicy.PriorInsurer != null)
                //    {
                //        ContractTerm _contract = new ContractTerm
                //        {
                //            ExpirationDt = CommonFun.FormateDate((DateTime)model.AutoPolicy.PriorInsurer.ExpirationDate)
                //        };
                //        Coverage _coverage = new Coverage()
                //        {
                //            CoverageCd = "BI",                        
                //        };
                //        OtherOrPriorPolicy _otherOrPriorPolicy = new OtherOrPriorPolicy()
                //        {
                //            ContractTerm = _contract,


                //        };
                //        _Request.ACORD.InsuranceSvcRq.PersAutoPolicyQuoteInqRq.PersPolicy.OtherOrPriorPolicy = _otherOrPriorPolicy;
                //    }
                int _driverCount = 1;
                model.AutoDrivers.ForEach(_driver =>
                {
                    _Request.ACORD.InsuranceSvcRq.PersAutoPolicyQuoteInqRq.PersAutoLineBusiness.PersDriver.Add(AddDriverDetails(_driver, model, _driverCount));
                    _driverCount = _driverCount + 1;
                });
                int _vehicleCount = 1;
                model.AutoVehicles.ForEach(_vehicle =>
                {
                    _Request.ACORD.InsuranceSvcRq.PersAutoPolicyQuoteInqRq.PersAutoLineBusiness.PersVeh.Add(AddVehicleDetails(_vehicle, model, _vehicleCount, stateCode));
                    _vehicleCount = _vehicleCount + 1;
                });
                int _violationCount = 1;
                _Request.ACORD.InsuranceSvcRq.PersAutoPolicyQuoteInqRq.PersPolicy.AccidentViolation = new List<AccidentViolation>();
                model.AutoDrivers.ForEach(_violation =>
                {
                    if (_violation.AutoDriverViolations.Count > 0)
                    {
                        _Request.ACORD.InsuranceSvcRq.PersAutoPolicyQuoteInqRq.PersPolicy.AccidentViolation.AddRange(AddDriverViolations(_violation, _violationCount));
                        _violationCount += 1;
                    }
                });
            }
            catch (Exception ex)
            {
            }

            return _Request;
        }

        private PersDriver AddDriverDetails(AutoDriver model, AutoRequestModel requestModel, int driverCount)
        {
            Driver _driver = new Driver();
            _driver.PersDriver = new PersDriver();

            try
            {
                _driver.PersDriver.id = Enums.XMLAttribute.Drv.ToString() + driverCount;
                GeneralPartyInfo generalPartyInfo = new GeneralPartyInfo();
                NameInfo genNameInfo = new NameInfo
                {
                    PersonName = new PersonName()
                };
                genNameInfo.PersonName.GivenName = model.FirstName;
                genNameInfo.PersonName.Surname = model.LastName;
                //TaxIdentity taxIdentity = new TaxIdentity
                //{
                //    TaxIdTypeCd = ConstantVariable.TaxIdType,
                //    TaxId = model.License_Number
                //};
                //genNameInfo.TaxIdentity = taxIdentity;

                generalPartyInfo.NameInfo = genNameInfo;
                generalPartyInfo.Addr = new List<Addr>
                {
                    AddStreetAddress(requestModel, Enums.AddressType.GaragingAddress.ToString()),
                    AddMailingAddress(requestModel, Enums.AddressType.MailingAddress.ToString()),
                };
                Communications communications = new Communications
                {
                    PhoneInfo = new List<PhoneInfo>()
                };
                PhoneInfo cmpHomePhoneInfo = new PhoneInfo
                {
                    PhoneTypeCd = ConstantVariable.PhoneType,
                    CommunicationUseCd = Enums.CommunicationUse.Home.ToString(),
                    PhoneNumber = model.ContactNo
                };
                communications.PhoneInfo.Add(cmpHomePhoneInfo);
                PhoneInfo cmpBusinessPhoneInfo = new PhoneInfo
                {
                    PhoneTypeCd = ConstantVariable.PhoneType,
                    CommunicationUseCd = Enums.CommunicationUse.Business.ToString(),
                    PhoneNumber = model.ContactNo
                };
                communications.PhoneInfo.Add(cmpBusinessPhoneInfo);
                EmailInfo emailInfo = new EmailInfo
                {
                    EmailAddr = "",
                    CommunicationUseCd = Enums.CommunicationUse.Day.ToString()
                };
                communications.EmailInfo = emailInfo;
                generalPartyInfo.Communications = communications;
                _driver.PersDriver.GeneralPartyInfo = generalPartyInfo;
                DriverInfo driverInfo = new DriverInfo();
                string _strBirthDate = string.Empty;

                try
                {
                    _strBirthDate = CommonFun.FormateDate(Convert.ToDateTime(model.BirthDate));
                }
                catch (Exception)
                {
                }


                LengthTimeCurrentAddr lengthTimeCurrentAddr = new LengthTimeCurrentAddr
                {
                    DurationPeriod = new DurationPeriod()
                };
                lengthTimeCurrentAddr.DurationPeriod.NumUnits = ConstantVariable.DriverLengthTimeDuration;
                lengthTimeCurrentAddr.DurationPeriod.UnitMeasurementCd = Enums.UnitMeasurement.MON.ToString();

                DriverPersonInfo personInfo = new DriverPersonInfo
                {
                    GenderCd = model.Gender,
                    BirthDt = _strBirthDate,
                    LengthTimeCurrentAddr = lengthTimeCurrentAddr,
                    MaritalStatusCd = model.Marital_Status.Substring(0, 1),
                    OccupationDesc = model.Occupation,
                    OccupationClassCd = ConstantVariable.DriverOccupationCode,
                    EducationLevelCd = ""
                };

                LengthTimeEmployed lengthTimeEmployed = new LengthTimeEmployed
                {
                    NumUnits = ConstantVariable.Amount,
                    UnitMeasurementCd = Enums.UnitMeasurement.MON.ToString()
                };
                personInfo.LengthTimeEmployed = lengthTimeEmployed;
                LengthTimeCurrentOccupation lengthTimeCurrentOccupation = new LengthTimeCurrentOccupation
                {
                    NumUnits = ConstantVariable.Amount,
                    UnitMeasurementCd = Enums.UnitMeasurement.Years.ToString()
                };
                personInfo.LengthTimeCurrentOccupation = lengthTimeCurrentOccupation;
                MiscParty miscParty = new MiscParty();
                GeneralPartyInfo miscGeneralPartyInfo = new GeneralPartyInfo();
                NameInfo miscNameInfo = new NameInfo
                {
                    CommlName = new CommlName()
                };
                miscNameInfo.CommlName.CommercialName = "";
                Addr miscAddr = new Addr
                {
                    AddrTypeCd = Enums.AddressType.GaragingAddress.ToString(),
                    id = Enums.XMLAttribute.Gar1.ToString(),
                    Addr1 = string.Empty,
                    Addr2 = string.Empty,
                    City = string.Empty,
                    StateProvCd = string.Empty,
                    PostalCode = string.Empty,
                    Country = string.Empty
                };
                miscGeneralPartyInfo.NameInfo = miscNameInfo;
                miscGeneralPartyInfo.Addr = new List<Addr>();
                miscGeneralPartyInfo.Addr.Add(miscAddr);

                MiscPartyInfo miscPartyInfo = new MiscPartyInfo
                {
                    MiscPartyRoleCd = ConstantVariable.MiscPartyRole
                };
                miscParty.MiscPartyInfo = miscPartyInfo;
                miscParty.GeneralPartyInfo = miscGeneralPartyInfo;
                personInfo.MiscParty = miscParty;
                driverInfo.PersonInfo = personInfo;

                DriversLicense driversLicense = new DriversLicense
                {
                    LicensedDt = string.Empty,
                    DriversLicenseNumber = model.License_Number,
                    LicenseClassCd = getLicenseStatus(model.License_Status),
                    StateProvCd = model.License_StateCd
                };
                driverInfo.DriversLicense = driversLicense;

                string licenseStatus = string.Empty;
                if (model.License_StateCd.Contains("IT"))
                    licenseStatus = "International/Foreign";
                else if (model.License_StateCd.Contains("FR"))
                    licenseStatus = "Foreign";
                else
                    licenseStatus = Enums.LicenseStatus.Active.ToString();

                License license = new License
                {
                    LicenseTypeCd = Enums.LicenseType.Driver.ToString(),
                    LicenseStatusCd = licenseStatus,
                    LicensedDt = string.Empty,
                    LicensePermitNumber = model.License_Number,
                    LicenseClassCd = getLicenseStatus(model.License_Status),
                    StateProvCd = model.License_StateCd
                };
                driverInfo.License = license;
                driverInfo.com_turborater_TemporaryVisitorLicense = ConstantVariable.Amount;
                driverInfo.QuestionAnswer = new List<QuestionAnswer>();

                QuestionAnswer questionAnswer = new QuestionAnswer
                {
                    QuestionCd = Enums.Question.GENRL14.ToString(),
                    YesNoCd = Enums.QuestionAnswer.No.ToString()
                };
                driverInfo.QuestionAnswer.Add(questionAnswer);

                questionAnswer = new QuestionAnswer
                {
                    QuestionCd = Enums.Question.AUTOP05.ToString(),
                    YesNoCd = Enums.QuestionAnswer.No.ToString()
                };
                driverInfo.QuestionAnswer.Add(questionAnswer);
                _driver.PersDriver.DriverInfo = driverInfo;

                string isMatureDriver = "N";

                if (model.BirthDate != null && model.BirthDate != DateTime.MinValue)
                {
                    isMatureDriver = (DateTime.Now.Year - model.BirthDate.Value.Year) >= 65 ? "Y" : "N";
                }

                PersDriverInfo persDriverInfo = new PersDriverInfo
                {
                    //DefensiveDriverCd = ConstantVariable.DefensiveDriverCode,
                    DefensiveDriverCd = model.IsDefensiveDriverCourse == true ? "1" : "0",
                    DriverRelationshipToApplicantCd = getDriverRelationShip(model.RelationCd),
                    //DriverTrainingInd = ConstantVariable.Amount,
                    DriverTrainingInd = model.IsDefensiveDriverCourse == true ? "1" : "0",
                    GoodStudentCd = model.IsGoodStudent ? "Y" : "N",
                    MatureDriverInd = isMatureDriver,
                    IsWorkLossBenefit = model.IsWorkLossBenefit == true ? "Opt2" : "Opt1",
                    Military = model.FkMilitaryTypeId > 0 ? "Y" : "N",
                    MembershipClub = model.FkWarehouseTypeId > 0 ? "Y" : "N"

                    // =  ConstantVariable.MatureDriverInd
                };
                if (model.IsExcluded)
                {
                    persDriverInfo.DriverTypeCd = "E";
                }

                FinancialResponsibilityFiling financialResponsibilityFiling = new FinancialResponsibilityFiling();

                if (model.IsSR22 == true)
                {
                    financialResponsibilityFiling.FilingCd = "1";
                    financialResponsibilityFiling.StateProvCd = model.SR22State;
                    financialResponsibilityFiling.FilingReasonCd = "SR22";
                    financialResponsibilityFiling.ReasonForFilingDesc = "SR22-Y";
                    financialResponsibilityFiling.FilingStatusCd = "Y";
                    financialResponsibilityFiling.AccuAuto_FinancialResponsibilityCaseNumber = string.Empty;
                    financialResponsibilityFiling.AccuAuto_FinancialResponsibilityFilingType = "Sr22";
                }
                else if(model.IsSR22A == true)
                {
                    financialResponsibilityFiling.FilingCd = "4";
                    financialResponsibilityFiling.StateProvCd = model.SR22State;
                    financialResponsibilityFiling.FilingReasonCd = "SR22A";
                    financialResponsibilityFiling.ReasonForFilingDesc = "SR22-Y";
                    financialResponsibilityFiling.FilingStatusCd = "Y";
                    financialResponsibilityFiling.AccuAuto_FinancialResponsibilityCaseNumber = string.Empty;
                    financialResponsibilityFiling.AccuAuto_FinancialResponsibilityFilingType = "Sr22A";
                }

                //if (model.IsSR22 == false)
                //{
                //    financialResponsibilityFiling.FilingCd = "0";
                //    financialResponsibilityFiling.StateProvCd = model.SR22State;
                //}

                persDriverInfo.FinancialResponsibilityFiling = financialResponsibilityFiling;
                _driver.PersDriver.PersDriverInfo = persDriverInfo;
            }
            catch (Exception ex)
            {
            }

            return _driver.PersDriver;
        }

        private PersVeh AddVehicleDetails(AutoVehicle model, AutoRequestModel reqModel, int vehicleCount, string stateCode)
        {
            Vehicle _vehicle = new Vehicle();
            PersVeh persVeh = new PersVeh();

            try
            {
                if (model.Model.Contains("&"))
                {
                    model.Model = model.Model.Replace("&", "");
                }


                persVeh.id = Enums.XMLAttribute.Veh.ToString() + vehicleCount;
                persVeh.LocationRef = Enums.XMLAttribute.Loc.ToString() + vehicleCount;
                persVeh.RatedDriverRef = Enums.XMLAttribute.Drv.ToString() + vehicleCount;
                persVeh.Manufacturer = model.Make;
                persVeh.Model = model.Model;
                persVeh.ModelYear = Convert.ToString(model.Year);
                persVeh.VehBodyTypeCd = ConstantVariable.VehBodyType;
                persVeh.VehTypeCd = ConstantVariable.VehBodyType;
                CostNewAmt costNewAmt = new CostNewAmt();
                costNewAmt.Amt = string.Empty;
                persVeh.CostNewAmt = costNewAmt;
                EstimatedAnnualDistance estimatedAnnualDistance = new EstimatedAnnualDistance();
                estimatedAnnualDistance.NumUnits = ConstantVariable.Amount;
                estimatedAnnualDistance.UnitMeasurementCd = Enums.UnitMeasurement.Miles.ToString();
                persVeh.EstimatedAnnualDistance = estimatedAnnualDistance;
                FullTermAmt fullTermAmt = new FullTermAmt();
                fullTermAmt.Amt = ConstantVariable.Amount;
                persVeh.FullTermAmt = fullTermAmt;
                persVeh.LeasedVehInd = ConstantVariable.Amount;
                persVeh.LicensePlateNumber = model.LicensePlateNumber;
                persVeh.NumCylinders = model.NumCylinders;
                persVeh.PurchaseDt = CommonFun.FormateDate(model.PurchaseDate);
                persVeh.TerritoryCd = string.Empty;
                persVeh.VehIdentificationNumber = model.VIN;
                persVeh.VehSymbolCd = string.Empty;
                persVeh.AntiLockBrakeCd = string.Empty;
                persVeh.DaytimeRunningLightInd = string.Empty;
                persVeh.EngineTypeCd = string.Empty;
                persVeh.GaragingCd = ConstantVariable.GaragingCode;
                DistanceOneWay distanceOneWay = new DistanceOneWay();
                distanceOneWay.NumUnits = ConstantVariable.Amount;
                distanceOneWay.UnitMeasurementCd = Enums.UnitMeasurement.Miles.ToString();
                persVeh.DistanceOneWay = distanceOneWay;
                persVeh.MultiCarDiscountInd = ConstantVariable.MultiCarDiscount;
                persVeh.NewVehInd = ConstantVariable.VehPerformance;
                persVeh.NonOwnedVehInd = ConstantVariable.Amount;
                OdometerReading odometerReading = new OdometerReading();
                odometerReading.NumUnits = ConstantVariable.Amount;
                odometerReading.UnitMeasurementCd = Enums.UnitMeasurement.Miles.ToString();
                persVeh.OdometerReading = odometerReading;
                persVeh.AntiTheftDeviceCd = ConstantVariable.AntiTheftDeviceCode;
                persVeh.PrincipalOperatorInd = ConstantVariable.PrincipalOperator;
                persVeh.SeenCarInd = ConstantVariable.Amount;
                persVeh.VehPerformanceCd = ConstantVariable.VehPerformance;
                persVeh.VehUseCd = ConstantVariable.VehUseCode;
                persVeh.FourWheelDriveInd = ConstantVariable.Amount;
                persVeh.SeatBeltTypeCd = ConstantVariable.SeatBeltType;
                persVeh.AirBagTypeCd = ConstantVariable.AirBagType;
                QuestionAnswer questionAnswer = new QuestionAnswer();
                questionAnswer.QuestionCd = Enums.Question.GENRL14.ToString();
                questionAnswer.YesNoCd = Enums.QuestionAnswer.No.ToString();
                persVeh.QuestionAnswer = new List<QuestionAnswer>();
                persVeh.QuestionAnswer.Add(questionAnswer);

                if (model.AutoLienHolder != null)
                    persVeh.AdditionalInterest = AddLeinHolder(model);


                //Uncomment below for send line holder in request - uncomment by HD
                if (model.AutoLienHolder != null)
                {
                    AdditionalInterest additionalInterest = new AdditionalInterest();
                    additionalInterest = AddLeinHolder(model);
                    persVeh.AdditionalInterest = additionalInterest;
                }

                if (reqModel.Coverage != null)
                    persVeh.Coverage = AddCoverageDetails(reqModel.Coverage, model, stateCode);

                //try
                //{
                //    if (model.AutoLienHolder != null)
                //    {
                //    }
                //}
                //catch (Exception __unusedException1__)
                //{
                //}

                _vehicle.PersVeh = persVeh;
            }
            catch (Exception ex)
            {
            }

            return _vehicle.PersVeh;
        }

        private List<AccidentViolation> AddDriverViolations(AutoDriver model, int driverRefrenceID)
        {
            int _countViolation = 0;
            List<AccidentViolation> lstViolations = new List<AccidentViolation>();

            try
            {
                foreach (var item in model.AutoDriverViolations)
                {
                    AccidentViolation accidentViolation = new AccidentViolation
                    {
                        AccidentViolationCd = item.ViolationName,
                        AccidentViolationDt = CommonFun.FormateDate(item.VioDate),
                        ConvictionDt = CommonFun.FormateDate(item.VioDate),
                        AccidentViolationDesc = item.Description,
                        id = Enums.XMLAttribute.Vio_.ToString() + Convert.ToString(_countViolation + 1),
                        DriverRef = Enums.XMLAttribute.Drv.ToString() + Convert.ToString(model.Id),
                        NumSurchargePoints = item.ViolationPoints
                    };
                    lstViolations.Add(accidentViolation);
                    _countViolation += 1;
                }
            }
            catch (Exception ex)
            {
            }

            return lstViolations;
        }

        private OtherOrPriorPolicy AddOtherOrPriorPolicy(AutoRequestModel model, ArrowheadCredential credential)
        {
            OtherOrPriorPolicy OtherOrPriorPolicy = new OtherOrPriorPolicy();

            try
            {
                if (model.AutoPolicy.PriorInsurer.InsurerName != null && model.AutoPolicy.PriorInsurer.InsurerName != "")
                {
                    OtherOrPriorPolicy.PolicyCd = ConstantVariable.PolicyCd;
                    OtherOrPriorPolicy.PolicyNumber = model.AutoPolicy.PriorInsurer.PolicyNumber;
                    OtherOrPriorPolicy.LOBCd = ConstantVariable.LOBCd;
                    OtherOrPriorPolicy.NAICCd = credential.NAICCode;
                    OtherOrPriorPolicy.InsurerName = model.AutoPolicy.PriorInsurer.InsurerName;
                    OtherOrPriorPolicy.ContractTerm = new ContractTerm();
                    OtherOrPriorPolicy.LengthTimeWithPreviousInsurer = new LengthTimeWithPreviousInsurer();
                    OtherOrPriorPolicy.LengthTimeWithPreviousInsurer.NumUnits = Convert.ToString(model.AutoPolicy.PriorInsurer.Duration);
                    OtherOrPriorPolicy.LengthTimeWithPreviousInsurer.UnitMeasurementCd = Enums.UnitMeasurement.Months.ToString();

                    if (model.AutoPolicy.PriorInsurer.ExpirationDate != null && model.AutoPolicy.PriorInsurer.ExpirationDate != DateTime.MinValue)
                    {
                        OtherOrPriorPolicy.OriginalInceptionDt = CommonFun.FormateDate(Convert.ToDateTime(model.AutoPolicy.PriorInsurer.ExpirationDate).AddMonths(-6));
                        OtherOrPriorPolicy.ContractTerm.EffectiveDt = CommonFun.FormateDate(Convert.ToDateTime(model.AutoPolicy.PriorInsurer.ExpirationDate).AddMonths(-6));
                        OtherOrPriorPolicy.ContractTerm.ExpirationDt = CommonFun.FormateDate(Convert.ToDateTime(model.AutoPolicy.PriorInsurer.ExpirationDate));
                    }
                    OtherOrPriorPolicy.ContractTerm.DurationPeriod = new DurationPeriod();
                    OtherOrPriorPolicy.ContractTerm.DurationPeriod.NumUnits = Convert.ToString(model.AutoPolicy.PriorInsurer.Duration);
                    OtherOrPriorPolicy.ContractTerm.DurationPeriod.UnitMeasurementCd = Enums.UnitMeasurement.MON.ToString();
                    OtherOrPriorPolicy.ContractTerm.ContinuousInd = ConstantVariable.PolicyContractTermIND;
                    OtherOrPriorPolicy.PolicyTransferInd = ConstantVariable.PolicyContractTermIND;
                    OtherOrPriorPolicy.LengthTimeWithPreviousInsurer.NumUnits = Convert.ToString(model.AutoPolicy.PriorInsurer.Duration);
                    OtherOrPriorPolicy.LengthTimeWithPreviousInsurer.UnitMeasurementCd = Enums.UnitMeasurement.MON.ToString();
                }
            }
            catch (Exception ex)
            {
            }

            return OtherOrPriorPolicy;
        }


        private List<Coverage> AddCoverageDetails(AutoCoverage Coverage, AutoVehicle VehicleDetail, string stateCode)
        {
            List<Coverage> _lstCoverage = new List<Coverage>();
            string _quoteState = stateCode;
            try
            {
                if (!string.IsNullOrEmpty(Coverage.BodilyInjuryPerPerson) && Convert.ToDecimal(Coverage.BodilyInjuryPerPerson) > 0)
                {
                    // Coverage _Coverage = new Coverage();
                    // Limit _limit = new Limit();
                    // List<Limit> _lstLimit = new List<Limit>();
                    //// _limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    // _limit.FormatInteger = new FormatInteger();
                    // _Coverage.CoverageCd = Enums.CoverageCode.BI.ToString();
                    // _Coverage.CoverageDesc = ConstantVariable.BICoverageDesc;
                    // //_limit.FormatCurrencyAmt.Amt = Coverage.BodilyInjuryPerPerson;
                    // //_limit.FormatCurrencyAmt.CurCd = "USD";
                    // _limit.FormatInteger.Amt = Coverage.BodilyInjuryPerPerson;
                    // _limit.FormatInteger.CurCd = "USD";
                    // _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                    // _lstLimit.Add(_limit);
                    // _limit = new Limit();
                    // //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    // _limit.FormatInteger = new FormatInteger();
                    // _limit.FormatInteger.Amt = Coverage.BodilyInjuryPerAcc;
                    // _limit.FormatInteger.CurCd = "USD";
                    // _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                    // _lstLimit.Add(_limit);
                    // Deductible deductible = new Deductible();
                    // // deductible.FormatCurrencyAmt = New FormatInteger()
                    // deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    // _Coverage.Deductible = deductible;
                    // _Coverage.Limit = _lstLimit;
                    // _lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.BI.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.BICoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.BodilyInjuryPerPerson;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                    _lstLimit.Add(_limit);
                    _limit = new Limit();
                    _limit.FormatInteger = new FormatInteger();
                    _limit.FormatInteger.Amt = Coverage.BodilyInjuryPerAcc;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                    _lstLimit.Add(_limit);
                    Deductible deductible = new Deductible();
                    // deductible.FormatCurrencyAmt = New FormatInteger()
                    deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    _Coverage.Deductible = deductible;
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);
                }

                if (!string.IsNullOrEmpty(Coverage.PropertyDamage) && Convert.ToDecimal(Coverage.PropertyDamage) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.PD.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.PDCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.PropertyDamage;
                    //_limit.FormatCurrencyAmt.CurCd = "USD";
                    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PropDam.ToString();
                    //_lstLimit.Add(_limit);
                    //Deductible deductible = new Deductible();
                    //// deductible.FormatCurrencyAmt = New FormatInteger()
                    //deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    //_Coverage.Deductible = deductible;
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.PD.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.PDCoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.PropertyDamage;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PropDam.ToString();
                    _lstLimit.Add(_limit);
                    Deductible deductible = new Deductible();
                    // deductible.FormatCurrencyAmt = New FormatInteger()
                    deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    _Coverage.Deductible = deductible;
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);
                }




                if (!string.IsNullOrEmpty(VehicleDetail.LoseOfUse) && Convert.ToDecimal(VehicleDetail.LoseOfUse) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.LUSE.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.LOUCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = VehicleDetail.LoseOfUse;
                    //_lstLimit.Add(_limit);
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.LUSE.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.LOUCoverageDesc;
                    _limit.FormatInteger.Amt = VehicleDetail.LoseOfUse;
                    _lstLimit.Add(_limit);
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);
                }

                if (!string.IsNullOrEmpty(Coverage.UninsuredBodilyInjuryPerAcc) && Convert.ToDecimal(Coverage.UninsuredBodilyInjuryPerAcc) > 0)
                {
                    //    //Coverage _Coverage = new Coverage();
                    //    //Limit _limit = new Limit();
                    //    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //    //_Coverage.CoverageCd = Enums.CoverageCode.UMBI.ToString();
                    //    ////   _Coverage.CoverageDesc = ConstantVariable.UMCoverageDesc;
                    //    //_limit.FormatCurrencyAmt.Amt = Coverage.UninsuredBodilyInjuryPerPerson;
                    //    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                    //    //List<Limit> _lstLimit = new List<Limit>();
                    //    //_lstLimit.Add(_limit);
                    //    //_limit = new Limit();
                    //    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //    //_limit.FormatCurrencyAmt.Amt = Coverage.UninsuredBodilyInjuryPerAcc;
                    //    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                    //    //_lstLimit.Add(_limit);
                    //    ////Deductible deductible = new Deductible();
                    //    ////deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    //    ////_Coverage.Deductible = deductible;
                    //    //_Coverage.Limit = _lstLimit;
                    //    //_lstCoverage.Add(_Coverage);

                    if (!string.IsNullOrEmpty(_quoteState) && _quoteState.ToUpper() != "GA")
                    {

                        Coverage _Coverage = new Coverage();
                        Limit _limit = new Limit();
                        _limit.FormatInteger = new FormatInteger();
                        _Coverage.CoverageCd = Enums.CoverageCode.UMBI.ToString();
                        //   _Coverage.CoverageDesc = ConstantVariable.UMCoverageDesc;
                        _limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerPerson;
                        _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                        List<Limit> _lstLimit = new List<Limit>();
                        _lstLimit.Add(_limit);
                        _limit = new Limit();
                        _limit.FormatInteger = new FormatInteger();
                        _limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerAcc;
                        _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                        _lstLimit.Add(_limit);
                        //Deductible deductible = new Deductible();
                        //deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                        //_Coverage.Deductible = deductible;
                        _Coverage.Limit = _lstLimit;
                        _lstCoverage.Add(_Coverage);
                    }
                }

                if (!string.IsNullOrEmpty(Coverage.UninsuredPropertyDamage) && Convert.ToDecimal(Coverage.UninsuredPropertyDamage) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.UMPD.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.UMPDCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.UninsuredPropertyDamage;
                    //_limit.FormatCurrencyAmt.CurCd = "USD";
                    ////_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                    //_lstLimit.Add(_limit);
                    //Deductible deductible = new Deductible();
                    ////// deductible.FormatCurrencyAmt = New FormatInteger()
                    //deductible.FormatCurrencyAmt = Decimal.ToInt32(Convert.ToDecimal(Coverage.UninsuredPropertyDamageDeductible)).ToString();
                    //_Coverage.Deductible = deductible;
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.UMPD.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.UMPDCoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.UninsuredPropertyDamage;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                    _lstLimit.Add(_limit);
                    Deductible deductible = new Deductible();
                    // deductible.FormatCurrencyAmt = New FormatInteger()
                    deductible.FormatCurrencyAmt = Decimal.ToInt32(Convert.ToDecimal(Coverage.UninsuredPropertyDamageDeductible)).ToString();
                    _Coverage.Deductible = deductible;
                    _Coverage.Limit = _lstLimit;
                    //Option _Option = new Option();
                    //_Option.OptionTypeCd = ConstantVariable.UMOptionTypeCode;
                    //_Coverage.Option = _Option;
                    _lstCoverage.Add(_Coverage);
                }

                if (!string.IsNullOrEmpty(VehicleDetail.CollisionDed) && Convert.ToDecimal(VehicleDetail.CollisionDed) > 0)
                {
                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    _Coverage.CoverageCd = Enums.CoverageCode.COLL.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.COLLCoverageDesc;

                    Option _Option = new Option();
                    _Option.OptionTypeCd = ConstantVariable.COLLOptionTypeCode1;
                    _Option.OptionCd = "S";
                    if (!string.IsNullOrEmpty(VehicleDetail.CollissionLevel) && VehicleDetail.CollissionLevel != "0")
                    {
                        _Option.OptionCd = VehicleDetail.CollissionLevel;
                    }
                    if (_Option.OptionCd != "L")
                    {
                        Deductible deductible = new Deductible();
                        deductible.FormatCurrencyAmt = CommonFun.FormateDeductiableAmount(VehicleDetail.CollisionDed);
                        _Coverage.Deductible = deductible;
                    }

                    _Coverage.Option = _Option;
                    _lstCoverage.Add(_Coverage);
                }
                else if (VehicleDetail.CollissionLevel == "L" && Convert.ToDecimal(VehicleDetail.CollisionDed) > 0)
                {
                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    _Coverage.CoverageCd = Enums.CoverageCode.COLL.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.COLLCoverageDesc;

                    Option _Option = new Option();
                    _Option.OptionTypeCd = ConstantVariable.COLLOptionTypeCode1;
                    _Option.OptionCd = "S";
                    if (!string.IsNullOrEmpty(VehicleDetail.CollissionLevel) && VehicleDetail.CollissionLevel != "0")
                    {
                        _Option.OptionCd = VehicleDetail.CollissionLevel;
                    }
                    if (_Option.OptionCd == "L")
                    {
                        Deductible deductible = new Deductible();
                        deductible.FormatCurrencyAmt = CommonFun.FormateDeductiableAmount("0.00");
                        _Coverage.Deductible = deductible;
                    }

                    _Coverage.Option = _Option;
                    _lstCoverage.Add(_Coverage);
                }

                if (!string.IsNullOrEmpty(VehicleDetail.ComprehensiveDed) && Convert.ToDecimal(VehicleDetail.ComprehensiveDed) > 0)
                {
                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    _Coverage.CoverageCd = Enums.CoverageCode.COMP.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.COMPCoverageDesc;
                    Deductible deductible = new Deductible();
                    // deductible.FormatCurrencyAmt = New FormatInteger()
                    deductible.FormatCurrencyAmt = CommonFun.FormateDeductiableAmount(VehicleDetail.ComprehensiveDed);
                    _Coverage.Deductible = deductible;
                    Option _Option = new Option();
                    _Option.OptionCd = "S";
                    if (!string.IsNullOrEmpty(VehicleDetail.CollissionLevel) && VehicleDetail.CollissionLevel != "0")
                    {
                        _Option.OptionCd = VehicleDetail.CollissionLevel;
                    }
                    _Coverage.Option = _Option;
                    _lstCoverage.Add(_Coverage);
                }

                if (!string.IsNullOrEmpty(VehicleDetail.TowingLimit) && Convert.ToDecimal(VehicleDetail.TowingLimit) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //_Coverage.CoverageCd = Enums.CoverageCode.TL.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.TLCoverageDesc;
                    //Deductible deductible = new Deductible();
                    //deductible.FormatCurrencyAmt = CommonFun.FormateDeductiableAmount(VehicleDetail.TowingLimit);
                    //_Coverage.Deductible = deductible;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    _Coverage.CoverageCd = Enums.CoverageCode.TL.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.TLCoverageDesc;
                    Deductible deductible = new Deductible();
                    deductible.FormatCurrencyAmt = CommonFun.FormateDeductiableAmount(VehicleDetail.TowingLimit);
                    _Coverage.Deductible = deductible;
                    _lstCoverage.Add(_Coverage);
                }




                if (!string.IsNullOrEmpty(Coverage.LimitedPropertyDamageLiability) && Convert.ToDecimal(Coverage.LimitedPropertyDamageLiability) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.LPD.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.LPDCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.LimitedPropertyDamageLiability;
                    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PDEachOcc.ToString();
                    //_lstLimit.Add(_limit);
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.LPD.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.LPDCoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.LimitedPropertyDamageLiability;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PDEachOcc.ToString();
                    _lstLimit.Add(_limit);
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);
                }


                //if (!string.IsNullOrEmpty(Coverage.PersonalInjuryProtection) && Convert.ToDecimal(Coverage.PersonalInjuryProtection) > 0)
                if (!string.IsNullOrEmpty(Coverage.PersonalInjuryProtection) && !string.IsNullOrEmpty(_quoteState) && _quoteState.ToUpper() == "MI")
                {
                    Coverage _Coverage = new Coverage();
                    if (Coverage.PersonalInjuryProtection == "500.00")
                    {
                        _Coverage.CoverageCd = Enums.CoverageCode.PIP.ToString();
                        _Coverage.CoverageDesc = ConstantVariable.PIPCoverageDesc;
                        Option option = new Option
                        {
                            OptionTypeCd = "Opt1",
                            OptionCd = "MW"
                        };
                        _Coverage.Option = option;
                        Deductible deductible = new Deductible
                        {
                            FormatCurrencyAmt = CommonFun.FormateDeductiableAmount(Coverage.PersonalInjuryProtection)
                        };
                        _Coverage.Deductible = deductible;
                    }
                    else if (Convert.ToDecimal(Coverage.PersonalInjuryProtection) == 0)
                    {
                        Coverage _coverage = new Coverage();
                        _coverage.CoverageCd = Enums.CoverageCode.PIP.ToString();
                        _coverage.CoverageDesc = ConstantVariable.PIPCoverageDesc;
                        _coverage.IterationNumber = "6";
                        _coverage.CoverageTypeCd = "Coverage";
                        _lstCoverage.Add(_coverage);
                    }
                    else
                    {
                        _Coverage.CoverageCd = Enums.CoverageCode.PIP.ToString();
                        _Coverage.CoverageDesc = ConstantVariable.PIPCoverageDesc;
                        Deductible deductible = new Deductible();
                        deductible.FormatCurrencyAmt = CommonFun.FormateDeductiableAmount(Coverage.PersonalInjuryProtection);
                        _Coverage.Deductible = deductible;
                        Option option = new Option
                        {
                            OptionTypeCd = "Opt3",
                            OptionCd = "I"
                        };
                        _Coverage.Option = option;
                        Limit _limit = new Limit();
                        List<Limit> _lstLimit = new List<Limit>();
                        _limit.FormatInteger = new FormatInteger();
                        _limit.FormatInteger.Amt = Coverage.PersonalInjuryProtection;
                        _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.MedExp.ToString();
                        _lstLimit.Add(_limit);
                        _Coverage.Limit = _lstLimit;
                    }
                    _lstCoverage.Add(_Coverage);

                    // For No Deductible remove didn't pass any PIP node
                    //else if (!string.IsNullOrEmpty(_quoteState) && _quoteState.ToUpper() == "MI")
                    //{
                    //    Coverage _coverage = new Coverage();
                    //    _coverage.CoverageCd = Enums.CoverageCode.PIP.ToString();
                    //    _coverage.CoverageDesc = ConstantVariable.PIPCoverageDesc;
                    //    _coverage.IterationNumber = "6";
                    //    _coverage.CoverageTypeCd = "Coverage";
                    //    _lstCoverage.Add(_coverage);
                    //}
                }

                if (!string.IsNullOrEmpty(Coverage.PropertyProtectionInsurance) && Convert.ToDecimal(Coverage.PropertyProtectionInsurance) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.PPI.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.PPICoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.PropertyProtectionInsurance;
                    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAcc.ToString();
                    //_lstLimit.Add(_limit);
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);



                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.PPI.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.PPICoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.PropertyProtectionInsurance;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAcc.ToString();
                    _lstLimit.Add(_limit);
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);


                }

                if (!string.IsNullOrEmpty(Coverage.UninsuredMotorist) && Convert.ToDecimal(Coverage.UninsuredMotorist) > 0)
                {
                    Coverage _coverage = new Coverage();
                    if (Coverage.UninsuredMotorist == "1.00")
                    {
                        Limit limit = new Limit();
                        limit.FormatInteger = new FormatInteger();
                        _coverage.CoverageCd = Enums.CoverageCode.UM.ToString();
                        _coverage.CoverageDesc = ConstantVariable.UMCoverageDesc;
                        Option option = new Option
                        {
                            OptionTypeCd = "Opt1",
                            OptionCd = "A"
                        };
                        _coverage.Option = option;
                        limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerPerson;
                        limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                        List<Limit> lstLimit = new List<Limit>();
                        lstLimit.Add(limit);
                        limit = new Limit();
                        limit.FormatInteger = new FormatInteger();
                        limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerAcc;
                        limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                        lstLimit.Add(limit);
                        Deductible deductible = new Deductible();
                        deductible.FormatCurrencyAmt = "0";

                        _coverage.Deductible = deductible;
                        _coverage.Limit = lstLimit;
                    }
                    else
                    {
                        Limit limit = new Limit();
                        limit.FormatInteger = new FormatInteger();
                        _coverage.CoverageCd = Enums.CoverageCode.UM.ToString();
                        _coverage.CoverageDesc = ConstantVariable.UMCoverageDesc;
                        Option option = new Option
                        {
                            OptionTypeCd = "Opt1",
                            OptionCd = "R"
                        };
                        _coverage.Option = option;
                        limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerPerson;
                        limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                        List<Limit> lstLimit = new List<Limit>();
                        lstLimit.Add(limit);
                        limit = new Limit();
                        limit.FormatInteger = new FormatInteger();
                        limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerAcc;
                        limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                        lstLimit.Add(limit);
                        Deductible deductible = new Deductible();
                        deductible.FormatCurrencyAmt = "0";
                        _coverage.Deductible = deductible;
                        _coverage.Limit = lstLimit;
                    }
                    _lstCoverage.Add(_coverage);

                }

                if (!string.IsNullOrEmpty(Coverage.MedicalPayments) && Convert.ToDecimal(Coverage.MedicalPayments) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.MEDP.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.MEDPCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.MedicalPayments;
                    //_limit.FormatCurrencyAmt.CurCd = ConstantVariable.Currency;
                    //_lstLimit.Add(_limit);
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    if (!string.IsNullOrEmpty(_quoteState) && _quoteState.ToUpper() == "GA")
                    {
                        Coverage _coverage = new Coverage();
                        if (Coverage.MedDisplayText.ToLower().Contains("chiropractor"))
                        {
                            Limit limit = new Limit();
                            List<Limit> lstLimit = new List<Limit>();
                            limit.FormatInteger = new FormatInteger();
                            _coverage.CoverageCd = Enums.CoverageCode.MEDP.ToString();
                            _coverage.CoverageDesc = ConstantVariable.MEDPCoverageDesc;
                            Option option = new Option
                            {
                                OptionCd = "NC"
                            };
                            _coverage.Option = option;
                            limit.FormatInteger.Amt = Coverage.MedicalPayments;
                            lstLimit.Add(limit);
                            _coverage.Limit = lstLimit;

                        }
                        else
                        {
                            Limit limit = new Limit();
                            List<Limit> lstLimit = new List<Limit>();
                            limit.FormatInteger = new FormatInteger();
                            _coverage.CoverageCd = Enums.CoverageCode.MEDP.ToString();
                            _coverage.CoverageDesc = ConstantVariable.MEDPCoverageDesc;
                            Option option = new Option
                            {
                                OptionCd = ""
                            };
                            _coverage.Option = option;
                            limit.FormatInteger.Amt = Coverage.MedicalPayments;
                            lstLimit.Add(limit);
                            _coverage.Limit = lstLimit;
                        }
                        _lstCoverage.Add(_coverage);

                    }
                    else
                    {
                        Coverage _Coverage = new Coverage();
                        Limit _limit = new Limit();
                        List<Limit> _lstLimit = new List<Limit>();
                        _limit.FormatInteger = new FormatInteger();
                        _Coverage.CoverageCd = Enums.CoverageCode.MEDP.ToString();
                        _Coverage.CoverageDesc = ConstantVariable.MEDPCoverageDesc;
                        _limit.FormatInteger.Amt = Coverage.MedicalPayments;
                        //_limit.FormatInteger.CurCd = ConstantVariable.Currency;
                        _lstLimit.Add(_limit);
                        _Coverage.Limit = _lstLimit;
                        _lstCoverage.Add(_Coverage);
                    }
                }

                if (!string.IsNullOrEmpty(Coverage.UnderinsuredBodilyInjuryPerAcc) && Convert.ToDecimal(Coverage.UnderinsuredBodilyInjuryPerAcc) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.UIMBI.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.UIMBDCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.UnderinsuredBodilyInjuryPerPerson;
                    //_limit.FormatCurrencyAmt.CurCd = "USD";
                    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_lstLimit.Add(_limit);
                    //_limit = new Limit();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_limit.FormatCurrencyAmt.Amt = Coverage.UnderinsuredBodilyInjuryPerAcc;
                    //_limit.FormatCurrencyAmt.CurCd = "USD";
                    //_limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAcc.ToString();
                    //_lstLimit.Add(_limit);
                    ////Deductible deductible = new Deductible();
                    ////deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    ////_Coverage.Deductible = deductible;
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.UIMBI.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.UIMBDCoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.UnderinsuredBodilyInjuryPerPerson;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                    List<Limit> _lstLimit = new List<Limit>();
                    _lstLimit.Add(_limit);
                    _limit = new Limit();
                    _limit.FormatInteger = new FormatInteger();
                    _limit.FormatInteger.Amt = Coverage.UnderinsuredBodilyInjuryPerAcc;
                    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAcc.ToString();
                    _lstLimit.Add(_limit);
                    //Deductible deductible = new Deductible();
                    //deductible.FormatCurrencyAmt = ConstantVariable.strDedctableAmount;
                    //_Coverage.Deductible = deductible;
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);
                }
                if (!string.IsNullOrEmpty(Coverage.UnderinsuredPropertyDamage) && Convert.ToDecimal(Coverage.UnderinsuredPropertyDamage) > 0)
                {
                    //Coverage _Coverage = new Coverage();
                    //Limit _limit = new Limit();
                    //List<Limit> _lstLimit = new List<Limit>();
                    //_limit.FormatCurrencyAmt = new FormatCurrencyAmt();
                    //_Coverage.CoverageCd = Enums.CoverageCode.UIMPD.ToString();
                    //_Coverage.CoverageDesc = ConstantVariable.UIMPDCoverageDesc;
                    //_limit.FormatCurrencyAmt.Amt = Coverage.UnderinsuredPropertyDamage;
                    //_limit.FormatCurrencyAmt.CurCd = ConstantVariable.Currency;
                    //_lstLimit.Add(_limit);
                    //_Coverage.Limit = _lstLimit;
                    //_lstCoverage.Add(_Coverage);

                    Coverage _Coverage = new Coverage();
                    Limit _limit = new Limit();
                    List<Limit> _lstLimit = new List<Limit>();
                    _limit.FormatInteger = new FormatInteger();
                    _Coverage.CoverageCd = Enums.CoverageCode.UIMPD.ToString();
                    _Coverage.CoverageDesc = ConstantVariable.UIMPDCoverageDesc;
                    _limit.FormatInteger.Amt = Coverage.UnderinsuredPropertyDamage;
                    //_limit.FormatInteger.CurCd = ConstantVariable.Currency;
                    _lstLimit.Add(_limit);
                    _Coverage.Limit = _lstLimit;
                    _lstCoverage.Add(_Coverage);
                }



                //if (!string.IsNullOrEmpty(Coverage.UninsuredBodilyInjuryPerPerson) && Coverage.UninsuredBodilyInjuryPerPerson != "0")
                //{
                //    Coverage _Coverage = new Coverage();
                //    Limit _limit = new Limit();
                //    List<Limit> _lstLimit = new List<Limit>();
                //    _limit.FormatInteger = new FormatInteger();
                //    _Coverage.CoverageCd = Enums.CoverageCode.UM.ToString();
                //    _Coverage.CoverageDesc = ConstantVariable.UMCoverageDesc;
                //    _limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerPerson;
                //    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerPerson.ToString();
                //    _lstLimit.Add(_limit);
                //    _limit = new Limit();
                //    _limit.FormatInteger = new FormatInteger();
                //    _limit.FormatInteger.Amt = Coverage.UninsuredBodilyInjuryPerAcc;
                //    _limit.LimitAppliesToCd = Enums.CoverageLimitAppliedCode.PerAccident.ToString();
                //    _lstLimit.Add(_limit);
                //    Deductible deductible = new Deductible();
                //    // deductible.FormatCurrencyAmt = New FormatInteger()
                //    deductible.FormatCurrencyAmt = Convert.ToString(Coverage.UninsuredBodilyInjuryDeductible);
                //    _Coverage.Deductible = deductible;
                //    _Coverage.Limit = _lstLimit;
                //    Option _Option = new Option();
                //    _Option.OptionTypeCd = ConstantVariable.UMOptionTypeCode;
                //    _Coverage.Option = _Option;
                //    _lstCoverage.Add(_Coverage);
                //}


            }
            catch (Exception ex)
            {
            }

            return _lstCoverage;
        }

        private AdditionalInterest AddLeinHolder(AutoVehicle model)
        {
            AdditionalInterest leanHolder = new AdditionalInterest();

            GeneralPartyInfo generalPartyInfo = new GeneralPartyInfo();
            generalPartyInfo.Addr = new List<Addr>();
            leanHolder.NatureInterestCd = ConstantVariable.LeanHolderNatureInterest;
            NameInfo nameInfo = new NameInfo
            {
                CommlName = new CommlName()
            };
            nameInfo.CommlName.CommercialName = model.AutoLienHolder.Name;
            generalPartyInfo.NameInfo = nameInfo;

            Addr addr = new Addr
            {
                AddrTypeCd = Enums.AddressType.MailingAddress.ToString(),
                Addr1 = model.AutoLienHolder.Street_Address1,
                Addr2 = "  ",
                City = model.AutoLienHolder.City,
                StateProv = model.AutoLienHolder.StateCd,
                PostalCode = model.AutoLienHolder.ZipCode
            };
            generalPartyInfo.Addr.Add(addr);
            leanHolder.GeneralPartyInfo = generalPartyInfo;
            return leanHolder;
        }

        private AutoResponseModel ProcessResponse(XmlDocument xmlResponse, string requestXML)
        {
            AutoResponseModel autoResponse = new AutoResponseModel();
            ErrorResponse errorResponse = new ErrorResponse();
            AutoRateCompanyResponse rateCompanyResponse = new AutoRateCompanyResponse();


            rateCompanyResponse.Errors = new List<ErrorResponse>();
            autoResponse.Response = new List<AutoRateCompanyResponse>();
            rateCompanyResponse.AutoRateResults = new List<AutoRateResult>();

            try
            {
                XmlNode oNode = xmlResponse.DocumentElement;
                var oPaymentList = oNode.SelectSingleNode("/ACORD/InsuranceSvcRs/PersAutoPolicyQuoteInqRs").OuterXml;
                XmlSerializer serializer = new XmlSerializer(typeof(RespPersAutoPolicyQuoteInqRs));
                var objResponse = new RespPersAutoPolicyQuoteInqRs();

                using (TextReader reader1 = new StringReader(oPaymentList.ToString()))
                {
                    try
                    {
                        objResponse = (RespPersAutoPolicyQuoteInqRs)serializer.Deserialize(reader1);
                    }
                    catch (InvalidOperationException generatedExceptionName)
                    {
                    }
                }

                if (objResponse.MsgStatus != null && objResponse.MsgStatus.ExtendedStatus.Count > 0)
                {
                    foreach (var item in objResponse.MsgStatus.ExtendedStatus)
                    {
                        errorResponse.Message = Convert.ToString(item);
                        rateCompanyResponse.Errors.Add(errorResponse);
                    }

                    autoResponse.Response.Add(rateCompanyResponse);
                    autoResponse.RequestFile = requestXML;
                    autoResponse.ResponseFile = xmlResponse.OuterXml;
                    return autoResponse;
                }
                else
                {
                    double _premium = Convert.ToDouble(objResponse.PolicySummaryInfo.FullTermAmt.Amt);
                    List<AutoRateResult> _lstrateResults = new List<AutoRateResult>();
                    List<AutoResponseVehicle> _vehicles = new List<AutoResponseVehicle>();
                    List<AutoResponseCoverage> _coverages;

                    foreach (var item in objResponse.PersAutoLineBusiness.PersVeh)
                    {
                        AutoResponseVehicle autoResponseVehicle = new AutoResponseVehicle
                        {
                            VIN = item.VehIdentificationNumber,
                            Make = item.Manufacturer,
                            Model = item.Model.Text,
                            Year = Convert.ToInt32(item.ModelYear)
                        };
                        _coverages = new List<AutoResponseCoverage>();
                        foreach (var item_details in item.Coverage)
                        {
                            AutoResponseCoverage autoCoverage = new AutoResponseCoverage
                            {
                                CoverageCd = item_details.CoverageCd,
                                CoverageName = item_details.CoverageDesc,
                                option = item_details._option,
                                //autoCoverage.Deductible = item_details.Deductible.ToString();
                                Premium = item_details.CurrentTermAmt.Amt.ToString()
                            };
                            _coverages.Add(autoCoverage);
                        }

                        autoResponseVehicle.Coverages = _coverages;
                        _vehicles.Add(autoResponseVehicle);
                    }

                    foreach (var item in objResponse.PersPolicy.PaymentOption)
                    {
                        DateTime _installmentDueDate = item.InstallmentInfo.Where(w => w.InstallmentNumber == "1").Select(s => s.InstallmentDueDt).FirstOrDefault();
                        AutoRateResult autoresult = new AutoRateResult
                        {
                            AutoInstallmentInfos = new List<AutoInstallmentInfo>(),
                            PayPlanId = item.PayplanId,
                            PayPlanDescription = item.Description,
                            NumOfPayments = Convert.ToInt32(item.NumPayments),
                            DownPayment = Convert.ToInt32(item.NumPayments) > 0 ? Convert.ToDouble(item.InstallmentInfo[0].InstallmentAmt.Amt) : 0,
                            TotalPremium = _premium,
                            InstallmentFee = Convert.ToDouble(item.InstallmentFeeAmt),
                            InstallmentDueDt = _installmentDueDate.Date,

                        };

                        foreach (var item1 in item.InstallmentInfo)
                        {
                            AutoInstallmentInfo _installment = new AutoInstallmentInfo
                            {
                                Amount = Convert.ToDouble(item1.InstallmentAmt.Amt),
                                DueDate = Convert.ToDateTime(item1.InstallmentDueDt),
                                Number = Convert.ToInt32(item1.InstallmentNumber)
                            };
                            autoresult.AutoInstallmentInfos.Add(_installment);

                            if ((_installment.Number == 1))
                                autoresult.MontlyPayment = _installment.Amount;
                        }
                        _lstrateResults.Add(autoresult);
                    }

                    rateCompanyResponse.AutoRateResults.AddRange(_lstrateResults);
                    rateCompanyResponse.BridgeUrl = objResponse.PersAutoLineBusiness.CompanyURL;



                    rateCompanyResponse.Vehicles = _vehicles;

                    rateCompanyResponse.CompanyQuoteId = objResponse.PersPolicy.QuoteInfo.CompanysQuoteNumber;

                    List<DriverViolationDetails> _driverViolationDetails = new List<DriverViolationDetails>();
                    if (objResponse.PersPolicy.AccidentViolation != null && objResponse.PersPolicy.AccidentViolation.Count() > 0)
                    {
                        _driverViolationDetails = objResponse.PersPolicy.AccidentViolation.GroupBy(g => g.DriverRef).Select(s =>
                         new DriverViolationDetails
                         {
                             DriverOrderId = Convert.ToInt32(s.Key.Replace("Drv", "").Trim()),
                             ViolationPoints = s.Select(sp => sp.NumSurchargePoints).Sum()

                         }).ToList();
                    }
                    rateCompanyResponse.DriverViolationDetails = _driverViolationDetails;
                }

                ResPaymentOption paymentOption = new ResPaymentOption();

                autoResponse.RequestFile = requestXML;
                autoResponse.ResponseFile = xmlResponse.OuterXml;
                // SR22 Change Array Index 
                if (objResponse.PersAutoLineBusiness.Coverage.Any(a => a.CoverageCd == Enums.CoverageCode.SR22Fee.ToString()))
                {
                    rateCompanyResponse.SR22Fee = Convert.ToDouble(objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(f => f.CoverageCd == Enums.CoverageCode.SR22Fee.ToString()).CurrentTermAmt.Amt);
                }
                else if (objResponse.PersAutoLineBusiness.Coverage.Any(a => a.CoverageCd == Enums.CoverageCode.SR22AFee.ToString()))
                {
                    rateCompanyResponse.SR22Fee = Convert.ToDouble(objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(f => f.CoverageCd == Enums.CoverageCode.SR22AFee.ToString()).CurrentTermAmt.Amt);
                }
                if (objResponse.PersAutoLineBusiness.Coverage.Any(a => a.CoverageCd == Enums.CoverageCode.POLFE.ToString()))
                    if (objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(x => x.CoverageCd == Enums.CoverageCode.POLFE.ToString()).CreditOrSurcharge != null)
                        rateCompanyResponse.PolicyFee = Convert.ToDouble(objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(x => x.CoverageCd == Enums.CoverageCode.POLFE.ToString()).CreditOrSurcharge.NumericValue.FormatCurrencyAmt.Amt);

                if (objResponse.PersAutoLineBusiness.Coverage.Any(a => a.CoverageCd == Enums.CoverageCode.MVRFee.ToString()))
                    if (objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(x => x.CoverageCd == Enums.CoverageCode.MVRFee.ToString()).CurrentTermAmt != null)
                        rateCompanyResponse.MVRFee = Convert.ToDouble(objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(x => x.CoverageCd == Enums.CoverageCode.MVRFee.ToString()).CurrentTermAmt.Amt);

                if (objResponse.PersAutoLineBusiness.Coverage.Any(a => a.CoverageCd == Enums.CoverageCode.UMFee.ToString()))
                    if (objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(x => x.CoverageCd == Enums.CoverageCode.UMFee.ToString()).CurrentTermAmt != null)
                        rateCompanyResponse.UMFundFee = Convert.ToDouble(objResponse.PersAutoLineBusiness.Coverage.FirstOrDefault(x => x.CoverageCd == Enums.CoverageCode.UMFee.ToString()).CurrentTermAmt.Amt);

            }
            catch (Exception ex)
            {
                errorResponse.Message = xmlResponse.OuterXml;
                rateCompanyResponse.Errors.Add(errorResponse);

                autoResponse.RequestFile = requestXML;
                autoResponse.ResponseFile = xmlResponse.OuterXml;
            }

            autoResponse.Response.Add(rateCompanyResponse);
            return autoResponse;
        }

        private XmlDocument prepareRequestXML(ArrowheadReqModel _Request, long QuoteID)
        {
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                var xmlserializer = new XmlSerializer(typeof(ACORD));
                var stringWriter = new StringWriter();

                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, _Request.ACORD);
                    xmlDoc.InnerXml = stringWriter.ToString();
                }
            }
            // Temporary Code 
            catch (Exception ex)
            {
            }

            return xmlDoc;
        }

        private string getLicenseStatus(string Code)
        {
            string _licenseStatus = string.Empty;

            switch (Code)
            {
                case "REVOKED":
                    {
                        _licenseStatus = "InValid";
                        break;
                    }

                case "SUSPENDED":
                    {
                        _licenseStatus = "InValid";
                        break;
                    }

                case "LEARNERS":
                    {
                        _licenseStatus = "Valid";
                        break;
                    }

                case "EXPIRED":
                    {
                        _licenseStatus = "InValid";
                        break;
                    }
            }

            if (_licenseStatus == null)
                _licenseStatus = "Valid";
            return _licenseStatus;
        }

        private string getDriverRelationShip(string code)
        {
            string _relationship = string.Empty;

            switch (code)
            {
                case "INSURED":
                    {
                        _relationship = "Self";
                        break;
                    }

                case "SELF":
                    {
                        _relationship = "Self";
                        break;
                    }

                case "SPOUSE":
                    {
                        _relationship = "Spouse";
                        break;
                    }

                case "CHILD":
                    {
                        _relationship = "Child";
                        break;
                    }

                case "GRANDPARENT":
                    {
                        _relationship = "Grandparent";
                        break;
                    }

                case "RELATIVE":
                    {
                        _relationship = "Relative";
                        break;
                    }

                case "PARENT":
                    {
                        _relationship = "Parent";
                        break;
                    }

                case "OTHER":
                    {
                        _relationship = "Other";
                        break;
                    }
            }

            return _relationship;
        }

        private ArrowheadReqModel PrepareArrowheadRequest(AutoRequestModel model, ArrowheadCredential credential)
        {
            var _createdDate = model.AutoPolicy.CreatedDate;
            string _requestID = System.Guid.NewGuid().ToString();
            ArrowheadReqModel _request = new ArrowheadReqModel();
            _request.ACORD = new ACORD();
            _request.ACORD.xmlns = ConstantVariable.xmlns;
            SignonRq SignonRq = new SignonRq();

            try
            {
                SignonRq.SignonTransport = new SignonTransport();
                SignonRq.SignonTransport.CustId = new CustId();
                SignonRq.ClientApp = new ClientApp();
                SignonRq.SignonTransport.CustId.SPName = credential.ArrowheadSPName;
                SignonRq.ClientDt = CommonFun.FormateDate(_createdDate);
                SignonRq.ClientApp.Org = credential.ClientApp_Org;
                SignonRq.ClientApp.Name = credential.ClientApp_Name;
                SignonRq.ClientApp.Version = credential.ClientApp_Version;
                _request.ACORD.SignonRq = SignonRq;
                InsuranceSvcRq insuranceSvcRq = new InsuranceSvcRq();
                insuranceSvcRq.RqUID = _requestID;
                PersAutoPolicyQuoteInqRq persAutoPolicyQuoteInqRq = new PersAutoPolicyQuoteInqRq
                {
                    RqUID = _requestID,
                    TransactionRequestDt = CommonFun.FormateDate(_createdDate),
                    TransactionEffectiveDt = CommonFun.FormateDate(model.AutoPolicy.EffectiveDate)
                };
                Producer producer = new Producer();
                GeneralPartyInfo generalPartyInfo = new GeneralPartyInfo
                {
                    NameInfo = new NameInfo(),
                    Addr = new List<Addr>()
                };
                generalPartyInfo.NameInfo.CommlName = new CommlName();
                generalPartyInfo.Communications = new Communications();
                generalPartyInfo.Communications.PhoneInfo = new List<PhoneInfo>();
                generalPartyInfo.NameInfo.CommlName.CommercialName = credential.CommercialName;
                generalPartyInfo.NameInfo.PersonName = new PersonName();
                generalPartyInfo.NameInfo.PersonName.Surname = string.Empty;
                generalPartyInfo.NameInfo.PersonName.GivenName = string.Empty;
                Addr ProducerAddr = new Addr
                {
                    id = Enums.XMLAttribute.Gar1.ToString(),
                    AddrTypeCd = Enums.AddressType.GaragingAddress.ToString(),
                    Addr1 = credential.ProducerAddr1,
                    Addr2 = credential.ProducerAddr2,
                    City = credential.ProducerCity,
                    StateProvCd = credential.ProducerStateCD,
                    PostalCode = credential.ProducerPostalCode
                };
                ProducerAddr.Addr1 = credential.ProducerAddr1;
                generalPartyInfo.Addr.Add(ProducerAddr);
                PhoneInfo producerPhoneInfo = new PhoneInfo
                {
                    PhoneTypeCd = ConstantVariable.PhoneType,
                    PhoneNumber = credential.ProducerPhoneNumber
                };
                generalPartyInfo.Communications.PhoneInfo.Add(producerPhoneInfo);
                producer.ProducerInfo = new ProducerInfo();
                producer.ProducerInfo.ContractNumber = credential.ContractNumber;
                producer.ItemIdInfo = new ItemIdInfo();
                producer.ItemIdInfo.AgencyId = credential.ContractNumber;
                producer.GeneralPartyInfo = generalPartyInfo;
                persAutoPolicyQuoteInqRq.Producer = producer;
                InsuredOrPrincipal insuredOrPrincipal = new InsuredOrPrincipal();
                GeneralPartyInfo insgeneralPartyInfo = new GeneralPartyInfo();
                NameInfo insNameInfo = new NameInfo();
                insNameInfo.PersonName = new PersonName();
                insgeneralPartyInfo.Addr = new List<Addr>();
                insNameInfo.PersonName.GivenName = model.AutoPolicy.FirstName;
                insNameInfo.PersonName.Surname = model.AutoPolicy.LastName;
                insgeneralPartyInfo.NameInfo = insNameInfo;
                insgeneralPartyInfo.Addr.Add(AddMailingAddress(model, Enums.AddressType.MailingAddress.ToString()));
                insgeneralPartyInfo.Addr.Add(AddStreetAddress(model, Enums.AddressType.GaragingAddress.ToString()));
                insgeneralPartyInfo.Communications = new Communications();
                insgeneralPartyInfo.Communications.PhoneInfo = new List<PhoneInfo>();
                PhoneInfo insPhoneInfo = new PhoneInfo
                {
                    PhoneNumber = model.AutoPolicy.ContactNo,
                    PhoneTypeCd = ConstantVariable.PhoneType,
                    CommunicationUseCd = Enums.CommunicationUse.Home.ToString()
                };
                insgeneralPartyInfo.Communications.PhoneInfo.Add(insPhoneInfo);
                EmailInfo insEmailInfo = new EmailInfo
                {
                    EmailAddr = model.AutoPolicy.EmailAddress,
                    CommunicationUseCd = Enums.CommunicationUse.Day.ToString()
                };
                insgeneralPartyInfo.Communications.EmailInfo = insEmailInfo;
                string _strBirthDate = string.Empty;

                try
                {
                    if (model.AutoPolicy.BirthDate != null)
                    {
                        Convert.ToString(model.AutoPolicy.BirthDate);
                        _strBirthDate = CommonFun.FormateBirthDate(Convert.ToDateTime(model.AutoPolicy.BirthDate));
                    }
                }
                catch (Exception)
                {

                }

                InsuredOrPrincipalInfo insInsuredOrPrincipalInfo = new InsuredOrPrincipalInfo();
                insInsuredOrPrincipalInfo.InsuredOrPrincipalRoleCd = ConstantVariable.InsuredOrPrincipalRole;
                PersonInfo insPersonInfo = new PersonInfo
                {
                    BirthDt = _strBirthDate,
                    GenderCd = model.AutoPolicy.Gender
                };
                insInsuredOrPrincipalInfo.PersonInfo = insPersonInfo;
                insuredOrPrincipal.InsuredOrPrincipalInfo = insInsuredOrPrincipalInfo;
                insuredOrPrincipal.GeneralPartyInfo = insgeneralPartyInfo;
                persAutoPolicyQuoteInqRq.InsuredOrPrincipal = insuredOrPrincipal;
                PersPolicy persPolicy = new PersPolicy
                {
                    LOBCd = ConstantVariable.LOBCd,
                    ControllingStateProvCd = model.AutoPolicy.StateCd
                };
                ContractTerm perContractTerm = new ContractTerm
                {
                    EffectiveDt = CommonFun.FormateDate(model.AutoPolicy.EffectiveDate),
                    ExpirationDt = CommonFun.FormateDate(model.AutoPolicy.ExpirationDate),
                    DurationPeriod = new DurationPeriod()
                };
                perContractTerm.DurationPeriod.NumUnits = Convert.ToString(model.AutoPolicy.PolicyTerm);
                perContractTerm.DurationPeriod.UnitMeasurementCd = Enums.UnitMeasurement.MON.ToString();
                persPolicy.ContractTerm = perContractTerm;

                string isEnsured = Enums.IsEnsured.No.ToString();


                if (model.AutoPolicy.PriorInsurer != null)
                {
                    if (model.AutoPolicy.PriorInsurer.InsurerName != null && model.AutoPolicy.PriorInsurer.InsurerName != "")
                    {
                        persPolicy.OtherOrPriorPolicy = AddOtherOrPriorPolicy(model, credential);
                        if (Convert.ToBoolean(model.AutoPolicy.PriorInsurer.IsPreviouslyInsured))
                            isEnsured = Enums.IsEnsured.Yes.ToString();
                    }
                }

                QuoteInfo quoteInfo = new QuoteInfo();
                quoteInfo.CompanysQuoteNumber = "";
                quoteInfo.ITCAgentAcct = credential.ProducerSubCode;
                quoteInfo.CompanyId = credential.CompanyID;
                quoteInfo.QuotePreparedDt = CommonFun.FormateDate(model.AutoPolicy.CreatedDate);
                persPolicy.QuoteInfo = quoteInfo;
                QuestionAnswer qtQuestionAns = new QuestionAnswer();
                persPolicy.QuestionAnswer = new List<QuestionAnswer>();
                qtQuestionAns.QuestionCd = Enums.Question.Current_Insurance_Value.ToString();
                qtQuestionAns.YesNoCd = isEnsured;
                qtQuestionAns.Explanation = Enums.QuestionAnswer.NC.ToString();
                persPolicy.QuestionAnswer.Add(qtQuestionAns);
                PersApplicationInfo persApplicationInfo = new PersApplicationInfo();
                InsuredOrPrincipal perInsuredOrPrincipal = new InsuredOrPrincipal();
                GeneralPartyInfo perPartyInfo = new GeneralPartyInfo();
                NameInfo perNameInfo = new NameInfo();
                PersonName perPerNameInfo = new PersonName
                {
                    GivenName = model.AutoPolicy.FirstName,
                    Surname = model.AutoPolicy.LastName
                };
                perNameInfo.PersonName = perPerNameInfo;
                //TaxIdentity perTaxIdentity = new TaxIdentity
                //{
                //    TaxIdTypeCd = Enums.TaxIdType.SSN.ToString(),
                //    TaxId = model.AutoDrivers[0].License_Number
                //};
                //perNameInfo.TaxIdentity = perTaxIdentity;
                perPartyInfo.NameInfo = perNameInfo;
                perPartyInfo.Addr = new List<Addr>();
                perPartyInfo.Addr.Add(AddStreetAddress(model, null/* TODO Change to default(_) if this is not a reference type */));
                Communications perCommunications = new Communications();
                perCommunications.PhoneInfo = new List<PhoneInfo>();
                PhoneInfo perPhoneInfo = new PhoneInfo();
                perPhoneInfo.PhoneNumber = model.AutoPolicy.ContactNo;
                perPhoneInfo.PhoneTypeCd = ConstantVariable.PhoneType;
                perPhoneInfo.CommunicationUseCd = Enums.CommunicationUse.Home.ToString();
                perCommunications.PhoneInfo.Add(perPhoneInfo);
                EmailInfo perEmailInfo = new EmailInfo();
                perEmailInfo.EmailAddr = model.AutoPolicy.EmailAddress;
                perEmailInfo.CommunicationUseCd = Enums.CommunicationUse.Day.ToString();
                perCommunications.EmailInfo = perEmailInfo;
                perPartyInfo.Communications = perCommunications;
                InsuredOrPrincipalInfo perInsuredOrPrincipalInfo = new InsuredOrPrincipalInfo();
                perInsuredOrPrincipalInfo.InsuredOrPrincipalRoleCd = ConstantVariable.InsuredOrPrincipalRole;
                PersonInfo perPersonInfo = new PersonInfo();
                perPersonInfo.BirthDt = _strBirthDate;
                perPersonInfo.GenderCd = model.AutoPolicy.Gender;
                perInsuredOrPrincipalInfo.PersonInfo = perPersonInfo;
                perInsuredOrPrincipal.GeneralPartyInfo = perPartyInfo;
                perInsuredOrPrincipal.InsuredOrPrincipalInfo = perInsuredOrPrincipalInfo;
                persApplicationInfo.InsuredOrPrincipal = perInsuredOrPrincipal;
                persPolicy.PersApplicationInfo = persApplicationInfo;
                string ResidentOwner = string.Empty;

                if (model.AutoDrivers[0].HomeOwner)
                    persPolicy.PersApplicationInfo.ResidenceOwnedRentedCd = Enums.ResidenceType.OWNED.ToString();
                else
                    persPolicy.PersApplicationInfo.ResidenceOwnedRentedCd = Enums.ResidenceType.RENTD.ToString();

                if (model.AutoPolicy.FkHomeTypeId != null && model.AutoPolicy.FkHomeTypeId > 0)
                {
                    if (model.AutoPolicy.FkHomeTypeCode == "HOMEOWNER")
                        persPolicy.PersApplicationInfo.ResidenceTypeCd = "DW";
                    else if (model.AutoPolicy.FkHomeTypeCode == "MOBILEHOMEOWNER")
                        persPolicy.PersApplicationInfo.ResidenceTypeCd = "MH";
                    else if (model.AutoPolicy.FkHomeTypeCode == "APARTMENT")
                        persPolicy.PersApplicationInfo.ResidenceTypeCd = "APT";
                    else if (model.AutoPolicy.FkHomeTypeCode == "RENTAL")
                        persPolicy.PersApplicationInfo.ResidenceTypeCd = "APT";
                    else
                        persPolicy.PersApplicationInfo.ResidenceTypeCd = "";
                }
                //persPolicy.PersApplicationInfo.ResidenceTypeCd = "";
                persPolicy.PolicyInfo = new PolicyInfo();
                persPolicy.PolicyInfo.ITCAgentAcct = credential.ProducerSubCode;
                persAutoPolicyQuoteInqRq.PersPolicy = persPolicy;
                persAutoPolicyQuoteInqRq.Location = new List<Location>();
                Location mLocation = new Location();
                mLocation.Addr = AddMailingAddress(model, null/* TODO Change to default(_) if this is not a reference type */);
                persAutoPolicyQuoteInqRq.Location.Add(mLocation);
                Location gLocation = new Location();
                gLocation.Addr = AddStreetAddress(model, null/* TODO Change to default(_) if this is not a reference type */);
                persAutoPolicyQuoteInqRq.Location.Add(gLocation);
                persAutoPolicyQuoteInqRq.PersAutoLineBusiness = new PersAutoLineBusiness();
                persAutoPolicyQuoteInqRq.PersAutoLineBusiness.LOBCd = ConstantVariable.LOBCd;
                persAutoPolicyQuoteInqRq.PersAutoLineBusiness.PersDriver = new List<PersDriver>();
                persAutoPolicyQuoteInqRq.PersAutoLineBusiness.PersVeh = new List<PersVeh>();
                PerCoverage coverage = new PerCoverage();
                coverage.CoverageCd = ConstantVariable.AutoBusinessCoverage;
                coverage.CoverageDesc = ConstantVariable.AutoBusinessCoverage;
                coverage.CreditOrSurcharge = new CreditOrSurcharge();
                coverage.CreditOrSurcharge.CreditSurchargeCd = ConstantVariable.AutoBusinessCoverageCreditSurcharge;
                persAutoPolicyQuoteInqRq.PersAutoLineBusiness.Coverage = coverage;
                insuranceSvcRq.PersAutoPolicyQuoteInqRq = persAutoPolicyQuoteInqRq;
                _request.ACORD.InsuranceSvcRq = insuranceSvcRq;
            }
            catch (Exception ex)
            {
            }

            return _request;
        }

        private Addr AddMailingAddress(AutoRequestModel model, string addressType)
        {
            Addr MalilingAddr = new Addr();
            MalilingAddr.Addr1 = model.AutoPolicy.MailingStreet_Address1;
            MalilingAddr.Addr2 = model.AutoPolicy.MailingStreet_Address2;
            MalilingAddr.City = model.AutoPolicy.MailingCity;
            MalilingAddr.StateProvCd = model.AutoPolicy.MailingStateCd;
            MalilingAddr.PostalCode = model.AutoPolicy.MailingZipCode;
            MalilingAddr.Country = model.AutoPolicy.MailingCounty;
            if (!string.IsNullOrWhiteSpace(addressType))
                MalilingAddr.AddrTypeCd = addressType;
            return MalilingAddr;
        }

        private Addr AddStreetAddress(AutoRequestModel model, string addressType)
        {
            Addr streetAddr = new Addr();
            streetAddr.id = Enums.XMLAttribute.Gar1.ToString();
            streetAddr.Addr1 = model.AutoPolicy.Street_Address1;
            streetAddr.Addr2 = model.AutoPolicy.Street_Address2;
            streetAddr.City = model.AutoPolicy.City;
            streetAddr.StateProvCd = model.AutoPolicy.StateCd;
            streetAddr.PostalCode = model.AutoPolicy.ZipCode;
            streetAddr.Country = model.AutoPolicy.County;
            if (!string.IsNullOrWhiteSpace(addressType))
                streetAddr.AddrTypeCd = addressType;
            return streetAddr;
        }
    }
}
