﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Arrowhead.POS.Process
{
    public class BindPolicy
    {
        [XmlElement(ElementName = "InsuranceSvcRq")]
        public BinderInsuranceSvcRq InsuranceSvcRq { get; set; }
    }

    [XmlRoot(ElementName = "InsuranceSvcRq")]
    public class BinderInsuranceSvcRq
    {
        [XmlElement(ElementName = "PersAutoPolicyQuoteInqRq")]
        public BinderPersAutoPolicyQuoteInqRq PersAutoPolicyQuoteInqRq { get; set; }
    }

    [XmlRoot(ElementName = "PersAutoPolicyQuoteInqRq")]
    public class BinderPersAutoPolicyQuoteInqRq
    {
        [XmlElement(ElementName = "PersPolicy")]
        public BinderPersPolicy PersPolicy { get; set; }
    }

    [XmlRoot(ElementName = "PersPolicy")]
    public class BinderPersPolicy
    {
        [XmlElement(ElementName = "QuoteInfo")]
        public BinderQuoteInfo QuoteInfo { get; set; }
        [XmlElement(ElementName = "QuestionAnswer")]
        public List<BinderQuestionAnswer> QuestionAnswer { get; set; }
        [XmlElement(ElementName = "PaymentOption")]
        public BinderPaymentOption PaymentOption { get; set; }
        [XmlElement(ElementName = "ElectronicSignature")]
        public BinderElectronicSignature ElectronicSignature { get; set; }
    }

    [XmlRoot(ElementName = "QuoteInfo")]
    public class BinderQuoteInfo
    {
        [XmlElement(ElementName = "CompanysQuoteNumber")]
        public string CompanysQuoteNumber { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "QuotePreparedDt")]
        public string QuotePreparedDt { get; set; }
    }

    [XmlRoot(ElementName = "QuestionAnswer")]
    public class BinderQuestionAnswer
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "QuestionCd")]
        public string QuestionCd { get; set; }
        [XmlElement(ElementName = "QuestionDescription")]
        public string QuestionDescription { get; set; }
        [XmlElement(ElementName = "YesNoCd")]
        public string YesNoCd { get; set; }
        [XmlElement(ElementName = "ReasonCd")]
        public string ReasonCd { get; set; }
        [XmlElement(ElementName = "Explanation")]
        public string Explanation { get; set; }
        [XmlAttribute(AttributeName = "vehicleid")]
        public string Vehicleid { get; set; }
    }

    [XmlRoot(ElementName = "DownPaymentAmt")]
    public class BinderDownPaymentAmt
    {
        [XmlElement(ElementName = "Amt")]
        public string Amt { get; set; }
        [XmlElement(ElementName = "CurCd")]
        public string CurCd { get; set; }
    }

    [XmlRoot(ElementName = "PaymentOption")]
    public class BinderPaymentOption
    {
        [XmlElement(ElementName = "PayplanId")]
        public string PayplanId { get; set; }
        [XmlElement(ElementName = "MethodPaymentCd")]
        public string MethodPaymentCd { get; set; }
        [XmlElement(ElementName = "DownPaymentPct")]
        public string DownPaymentPct { get; set; }
        [XmlElement(ElementName = "NumPayments")]
        public string NumPayments { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "PaymentPlanCd")]
        public string PaymentPlanCd { get; set; }
        [XmlElement(ElementName = "DownPaymentAmt")]
        public BinderDownPaymentAmt DownPaymentAmt { get; set; }
        [XmlElement(ElementName = "ConfirmationCode")]
        public string ConfirmationCode { get; set; }

        [XmlElement(ElementName = "RoutingNumber")]
        public string RoutingNumber { get; set; }
        [XmlElement(ElementName = "AccountNumber")]
        public string AccountNumber { get; set; }
    }

    [XmlRoot(ElementName = "ElectronicSignature")]
    public class BinderElectronicSignature
    {
        [XmlElement(ElementName = "SignedDt")]
        public string SignedDt { get; set; }
        [XmlElement(ElementName = "SignedByCd")]
        public string SignedByCd { get; set; }
        [XmlElement(ElementName = "HashCode")]
        public string HashCode { get; set; }
    }



    #region "Response Node"

    [XmlRoot(ElementName = "InsuranceSvcRs")]
    public class BinderInsuranceSvcRs
    {
        [XmlElement(ElementName = "RqUID")]
        public string RqUID { get; set; }
        [XmlElement(ElementName = "PersAutoPolicyQuoteInqRs")]
        public BinderPersAutoPolicyQuoteInqRs PersAutoPolicyQuoteInqRs { get; set; }
    }

    [XmlRoot(ElementName = "PersAutoPolicyQuoteInqRs")]
    public class BinderPersAutoPolicyQuoteInqRs
    {
        [XmlElement(ElementName = "RqUID")]
        public string RqUID { get; set; }
        [XmlElement(ElementName = "TransactionResponseDt")]
        public string TransactionResponseDt { get; set; }
        [XmlElement(ElementName = "MsgStatus")]
        public BinderMsgStatus MsgStatus { get; set; }
        [XmlElement(ElementName = "PersPolicy")]
        public List<BinderResPersPolicy> PersPolicy { get; set; }
    }

    [XmlRoot(ElementName = "CodeList")]
    public class BinderCodeList
    {
        [XmlElement(ElementName = "WebsiteURL")]
        public string WebsiteURL { get; set; }
    }

    [XmlRoot(ElementName = "MsgStatus")]
    public class BinderMsgStatus
    {
        [XmlElement(ElementName = "MsgStatusCd")]
        public string MsgStatusCd { get; set; }
        [XmlElement(ElementName = "CodeList")]
        public BinderCodeList CodeList { get; set; }
        [XmlElement(ElementName = "MsgErrorCd")]
        public string MsgErrorCd { get; set; }
        [XmlElement(ElementName = "MsgStatusDesc")]
        public string MsgStatusDesc { get; set; }
    }

    [XmlRoot(ElementName = "QuoteInfo")]
    public class BinderResQuoteInfo
    {
        [XmlElement(ElementName = "CompanysPolicyNumber")]
        public string CompanysPolicyNumber { get; set; }
    }

    [XmlRoot(ElementName = "PersPolicy")]
    public class BinderResPersPolicy
    {
        [XmlElement(ElementName = "LOBCd")]
        public string LOBCd { get; set; }
        [XmlElement(ElementName = "QuoteInfo")]
        public BinderResQuoteInfo QuoteInfo { get; set; }
    }

    #endregion



}
