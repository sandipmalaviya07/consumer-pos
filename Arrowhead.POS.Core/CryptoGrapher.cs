﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Core
{
    public class CryptoGrapher
    {
        //----------------------MD5-String-Hashing-Function-----------------------------------------------
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            //get hash result after compute it
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        //----------------------String-Encryption-Function------------------------------------------------
        public static string EncryptText(string input, string secretKey)
        {
            // Get the bytes of the string
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(secretKey);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }

        //----------------------String-Decryption-Function------------------------------------------------
        public static string DecryptText(string input, string secretKey)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(secretKey);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = Encoding.UTF8.GetString(bytesDecrypted);

            return result;
        }

        //----------------------AES-Encryption-Function---------------------------------------------------
        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    dynamic key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        //----------------------AES-Decryption-Function---------------------------------------------------
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    dynamic key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        //----------------------RSA-Encryption-Function---------------------------------------------------
        public static string RSAEncryption(string PlainTextData, String RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] bytedEncryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    var bytesPlainTextData = System.Text.Encoding.Unicode.GetBytes(PlainTextData);
                    RSA.ImportParameters(ConvertStringToRSAKey(RSAKey));
                    bytedEncryptedData = RSA.Encrypt(bytesPlainTextData, DoOAEPPadding);
                }
                var cypherText = Convert.ToBase64String(bytedEncryptedData);
                return cypherText;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return e.Message;
            }
        }

        //----------------------RSA-Decryption-Function---------------------------------------------------
        public static string RSADecryption(string CypherText, string RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] bytesPlainTextData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    var bytesCypherText = Convert.FromBase64String(CypherText);
                    RSA.ImportParameters(ConvertStringToRSAKey(RSAKey));
                    bytesPlainTextData = RSA.Decrypt(bytesCypherText, DoOAEPPadding);
                }
                var plainTextData = System.Text.Encoding.Unicode.GetString(bytesPlainTextData);
                return plainTextData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return e.Message;
            }
        }

        //----------------------Convert-RSAKey-to-String--------------------------------------------------
        private static string ConvertRSAKeyToString(RSAParameters key)
        {
            //converting the RSA key into a string representation
            //we need some buffer
            var sw = new System.IO.StringWriter();
            //we need a serializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //serialize the key into the stream
            xs.Serialize(sw, key);
            //get the string from the stream
            string KeyString = sw.ToString();
            return KeyString;
        }

        //----------------------Convert-String-to-RSAKey--------------------------------------------------
        private static RSAParameters ConvertStringToRSAKey(string RSAStringKey)
        {
            //converting it back
            //get a stream from the string
            var sr = new System.IO.StringReader(RSAStringKey);
            //we need a deserializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //get the object back from the stream
            var RSAKey = (RSAParameters)xs.Deserialize(sr);
            return RSAKey;
        }
        public static string Generatehash512(string d)
        {
            var bytes = Encoding.UTF8.GetBytes(d);
            using (var hash = SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }
        public static string EncryptToken(string d)
        {
            byte[] ba = Encoding.Default.GetBytes(d);
            var hexString = BitConverter.ToString(ba);
            return hexString;
        }


        static readonly char[] padding = { '=' };

        public static string CBase64Encrypt(string stringToEncrypt)
        {
            byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(ConstantVariables.Base64Key);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray()).TrimEnd(padding).Replace('+', '-').Replace('/', '_'); ;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string CBase64Decrypt(string EncryptedText)
        {

            EncryptedText = EncryptedText.Replace('_', '/').Replace('-', '+');
            switch (EncryptedText.Length % 4)
            {
                case 2: EncryptedText += "=="; break;
                case 3: EncryptedText += "="; break;
            }
            byte[] inputByteArray = new byte[EncryptedText.Length + 1];
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };

            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(ConstantVariables.Base64Key);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(EncryptedText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

    }
}
