﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Arrowhead.POS.Core;
namespace Arrowhead.POS.Core.Paging
{
    public class PagerList<T>
    {
        public int PageRows { get; set; }
        public IEnumerable<T> PageList { get; set; }
        public int PageNumber { get; set; }
        public int TotalRows { get; set; }
        public int TotalPage { get; private set; }
        public string SortBy { get; private set; }
        public PagerList(IEnumerable<T> pageList, int pageNumber, int perPage, int totalRows, string sortBy = "")
        {
            PageRows = perPage;
            TotalRows = totalRows;
            PageList = pageList;
            PageNumber = pageNumber;
            SortBy = sortBy;
            TotalPage = ConvertTo.Rounds(ConvertTo.Decimal(totalRows / PageRows));
        }
    }

    public static class PagerQueryExtension
    {
        public static PagerList<T> ToPagerListOrderBy<T>(this IQueryable<T> query, int pageNumber, int perPage, string orderBy)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            var itemsToSkip = (pageNumber - 1) * perPage;
            var totalRows = query.Count();
            var pagerList = query.OrderBy(orderBy).Skip(itemsToSkip).Take(perPage).ToList();
            //var totalRows = pagerList.Count();
            return new PagerList<T>(pagerList, pageNumber, perPage, totalRows, orderBy);
        }
    }
}
