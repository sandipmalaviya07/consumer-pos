﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Fax.V1;
using Twilio.Rest.Lookups.V1;


namespace Arrowhead.POS.Core
{
    public static class TwilioUtility
    {
        public static MessageResource SendSMS(string twilioAccountSid, string twilioAuthToken, string toNumber, string fromNumber, string shortCode, string body, List<string> mediaUrls = null)
        {
            try
            {
                string phoneCarrier = CheckPhoneNumberCarrier(toNumber, twilioAccountSid, twilioAuthToken);
                if (phoneCarrier.ToLower().Contains(ConstantVariables.PhoneCarrier.ToString()))
                {
                    List<Uri> _mediaUrls = null;
                    if (mediaUrls != null && mediaUrls.Count > 0)
                    {
                        _mediaUrls = mediaUrls.Select(s => new Uri(s, UriKind.Absolute)).ToList();
                    }
                    TwilioClient.Init(twilioAccountSid, twilioAuthToken);
                    var message = MessageResource.Create(
                        to: new Twilio.Types.PhoneNumber("+1" + (toNumber).Trim()),
                        from: new Twilio.Types.PhoneNumber(fromNumber),
                        mediaUrl: _mediaUrls,
                        body: body);

                    return message;
                }
                else
                {
                    List<Uri> _mediaUrls = null;
                    if (mediaUrls != null && mediaUrls.Count > 0)
                    {
                        _mediaUrls = mediaUrls.Select(s => new Uri(s, UriKind.Absolute)).ToList();
                    }
                    TwilioClient.Init(twilioAccountSid, twilioAuthToken);
                    var message = MessageResource.Create(
                        to: new Twilio.Types.PhoneNumber("+1" + (toNumber).Trim()),
                        from: new Twilio.Types.PhoneNumber(shortCode),
                        mediaUrl: _mediaUrls,
                        body: body);

                    return message;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static FaxResource SendFax(string twilioAccountSid, string twilioAuthToken, string toNumber, string fromNumber, string mediaUrl)
        {
            try
            {
                string _toNumber = Regex.Replace(toNumber, @"[^\d]", "");

                TwilioClient.Init(twilioAccountSid, twilioAuthToken);
                var fax = FaxResource.Create(
                from: ((fromNumber).Trim()),
                to: ("+1" + (_toNumber).Trim()),
                mediaUrl: new Uri(mediaUrl)
            );

                return fax;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerPhoneNumber"></param>
        /// <returns></returns>
        public static string CheckPhoneNumberCarrier(string customerPhoneNumber, string twilioAccountSid, string twilioAuthToken)
        {
            string phoneName = string.Empty;
            try
            {

                if (!string.IsNullOrWhiteSpace(customerPhoneNumber))
                {


                    TwilioClient.Init(twilioAccountSid, twilioAuthToken);
                    var type = new List<string> { "carrier" };
                    var phoneNumber = PhoneNumberResource.Fetch(
                        type: type,
                        pathPhoneNumber: new Twilio.Types.PhoneNumber("+1" + (customerPhoneNumber).Trim())
                    );
                    string carrierType = phoneNumber.Carrier.Where(w => w.Key == "name").Select(s => s.Value).FirstOrDefault();

                    if (carrierType != null)
                    {
                        phoneName = carrierType;
                    }


                }


            }
            catch (Exception ex)
            {

            }

            return phoneName;
        }
    }
}
