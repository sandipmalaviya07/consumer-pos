﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Core
{
    public static class MailingUtility
    {
        public static bool SendEmail(string vFrom, string vTo, string vCC, string vBCc, string vSubject, string vBody, bool vIsBodyHtml, int vPriority, string vAttachmentName, string vReplyTo, string host, string userName, string password, int port, bool isUseDefaulCredential, bool isEnableSSL, bool isDefault, List<Attachment> fileBytes = null, string vAttachedFileName = "")
        {
            try
            {
                MailMessage mm = new MailMessage(vFrom, vTo);
                mm.Subject = vSubject;
                mm.Body = vBody;

                if (!string.IsNullOrWhiteSpace(vReplyTo))
                {
                    MailAddress replyTo = new MailAddress(vReplyTo);
                    mm.ReplyToList.Add(replyTo);
                }

                if (!string.IsNullOrWhiteSpace(vCC))
                {
                    MailAddress _CC = new MailAddress(vCC);
                    mm.CC.Add(_CC);
                }

                if (!string.IsNullOrWhiteSpace(vBCc)) { mm.Bcc.Add(vBCc); }

                if (vIsBodyHtml == true)
                { mm.IsBodyHtml = true; }
                else { mm.IsBodyHtml = false; }

                switch (vPriority)
                {
                    case 2:
                        mm.Priority = MailPriority.High;
                        break;
                    case 1:
                        mm.Priority = MailPriority.Normal;
                        break;
                    case 0:
                        mm.Priority = MailPriority.Low;
                        break;
                    default:
                        mm.Priority = MailPriority.Normal;
                        break;
                }

                if (!string.IsNullOrWhiteSpace(vAttachmentName))
                {

                    Attachment attach = new Attachment(vAttachmentName);
                    attach.Name = vAttachedFileName;
                    mm.Attachments.Add(attach);
                }

                if (fileBytes != null)
                {
                    foreach (var item in fileBytes)
                    {
                        mm.Attachments.Add(item);
                    }
                }

                System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(userName, password);
                SmtpClient smtp = new SmtpClient
                {
                    Host = host,
                    UseDefaultCredentials = isUseDefaulCredential,
                    Credentials = basicAuthenticationInfo,
                    Port = port,
                    EnableSsl = isEnableSSL
                };
                smtp.Send(mm);

                return true;
            }
            catch (SmtpException ex)
            {
                string vError = ex.Message;
                return false;
            }
        }
    }
}
