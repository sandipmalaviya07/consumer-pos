﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Arrowhead.POS.Core
{
    public class BlobFileUtility
    {
        private CloudBlobContainer blobContainer;
        public async Task<Uri> Save(string folderName, string fileName, byte[] fileContent)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConstantVariables.BlobStorageEndPoint);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(folderName);
                var createBlob = container.CreateIfNotExistsAsync().Result;

                if (createBlob)
                {
                    BlobContainerPermissions permissions = container.GetPermissionsAsync().Result;
                    permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                    await container.SetPermissionsAsync(permissions);
                }

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                await blockBlob.UploadFromByteArrayAsync(fileContent, 0, fileContent.Length);
                return blockBlob.Uri;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public void DeleteBlobFile(string folderName, string fileName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConstantVariables.BlobStorageEndPoint);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(folderName);
                CloudBlockBlob _blockBlob = container.GetBlockBlobReference(fileName);

                var IsExitsFolder = container.ExistsAsync().Result;
                if (!IsExitsFolder)
                {
                    return;
                }

                //delete blob from container    
                _blockBlob.DeleteIfExistsAsync();
            }
            catch (Exception ex) { }
        }


        public static byte[] GetFileContent(string folderName, string fileName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConstantVariables.BlobStorageEndPoint);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(folderName);
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName.Replace(folderName, ""));
                blockBlob.FetchAttributesAsync();
                long fileByteLength = blockBlob.Properties.Length;
                byte[] fileContent = new byte[fileByteLength];
                for (int i = 0; i < fileByteLength; i++)
                {
                    fileContent[i] = 0x20;
                }
                blockBlob.DownloadToByteArrayAsync(fileContent, 0);

                return fileContent;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string GetFileContentBase64(string folderName, string fileName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConstantVariables.BlobStorageEndPoint);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(folderName);
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName.Replace(folderName, ""));
                blockBlob.FetchAttributesAsync();
                Thread.Sleep(15000);
                long fileByteLength = blockBlob.Properties.Length;
                byte[] fileContent = new byte[fileByteLength];
                for (int i = 0; i < fileByteLength; i++)
                {
                    fileContent[i] = 0x20;
                }
                blockBlob.DownloadToByteArrayAsync(fileContent, 0);
                var azureBase64 = Convert.ToBase64String(fileContent);
                return azureBase64;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}

