﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Arrowhead.POS.Core;

namespace Arrowhead.POS.Core
{
    public class ConstantVariables
    {
        #region Encryption Key
        public const string Base64Key = "A0D1nX0Q";
        public const string AgencyEncryptionKey = "nCmBmaqLsM6SqRnGMpR4utJwSwXRvq2PFLN4j29R";

        public const string BindPolicyEncryptionKey = "GpV055sBySE3MpDH5CUxVDSfrO0670ET";
        #endregion

        public const string RaterServiceProviderCode = "ARROWHEAD";
        public const string ArrowheadCompanyCode = "ARROWHEAD";
        public const int UserID = 1;
        public const int PolicyTerm = 6;
        public const string FirstDriverRelationCode = "INSURED";
        public const string LicenseOccupationCode = "DRIVEROCCUPATION";
        public static string ConnectionString = WebConfigurationManager.AppSettings["ConnectionStringForSP"].ToString();
        //"data source=rateforce.database.windows.net;initial catalog=ArrowheadPOS;persist security info=True;user id=rateforce;password=Pblcsi8694_;MultipleActiveResultSets=True;App=EntityFramework&quot;";
        public const string ConstantAllCharsString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public const string BaseURl = "http://localhost:49968";
        public static string LiveBaseURl = WebConfigurationManager.AppSettings["LiveBaseURl"].ToString();
        public const string PGErrorNotifyEmail = "notify@rateforce.com"; 
        #region "Docusign"
        public const string EsignSubject = "Envelope with multiple documents";
        public const string Envdefstatus = "Sent";
        public const string Signer1Code = "Agent";
        public const string Signer2Code = "Customer";
        public const string AuthenticationMethod = "Email";
        public const string EmailSubject = "Auto Policy Paperwork";
        public const string DocusignSequence = "3";
        #endregion

        #region "Blob"
        public const string BlobStorageEndPoint = "DefaultEndpointsProtocol=https;AccountName=arrowheadpos;AccountKey=uzU4WWgJ2OQslOYtSzqbFjPSQHzF3uTIrer5bYQOdx9Fp7Fdr3UBPsr3i8oPfAdM46HUxhY/eBoxqfHEGOSM1g==;EndpointSuffix=core.windows.net";
        public const string RequestXMLContainer = "request-xml";
        public const string BinderRequestXMLContainer = "binder-requestxml";
        public const string BinderResponseXMLContainer = "binder-responsexml";
        public const string OfficeSignAppContainer = "quote-app";
        public const string IdCardAppContainer = "quote-idcards";
        public const string BillingAppContainer = "quote-billingpdf";
        public const string SR22AppContainer = "quote-sr22app";
        public const string ResponseXMLContainer = "response-xml";
        public const string NSDDocContainer = "nsd-xml";
        public const string ArrowheadBlobURL = "https://arrowheadpos.blob.core.windows.net/";
        public const string QuoteBridgeXMLContainer = "quote-bridgexml";
        #endregion

        #region "twilio"
        public const string PhoneCarrier = "verizon";
        #endregion

        #region "Devstripe"
        // Rateforce Dev Key
        //public const string DevPublishableKey = "pk_test_KsXJj3ibGgv1nV46SRvfhuEn";
        //public const string DevTestAPIKey = "sk_test_lcQXzItaTsNyZmJSiQ2j0mto";
        public const string DevStripeAccountId = "acct_1EWCiYA42rNDDVcM";

        //Samarpan Dev Key
        public const string DevPublishableKey = "pk_test_0QfURQBD68I3I9Is2IFcyIzS";
        public const string DevTestAPIKey = "sk_test_FXWSQgda0a5CucUlzbWEeYU1";
        #endregion

        #region "Prodstripe"
        public const string ProdPublishableKey = "pk_live_h5vFEdw9VjbEVvR6y0jYrayJ";
        public const string ProdTestAPIKey = "sk_live_yxan1nvoDnsy2abjXrvK1lBw";
        public const string ProdStripeAccountId = "acct_1EWCiYA42rNDDVcM";
        #endregion


        #region "AddressVerification"
        public const string AddressUserId = "377RATEF3309";
        #endregion

        #region "PolicyXML"
        public const string PhoneNumber = "9999999999";
        public const string EmailId = "test@gmail.com";
        #endregion


        #region NSD 
        public const string ArrowHeadNSDPassword = "rateforceAPI";
        public const string ArrowHeadNSDAgencyNumber = "70474";
        public const string CustomerNSDXMLFolder = "nsd-xml";
        #endregion

        #region Azure Cloud Storage Container Name
        public const string MMSFolderName = "mms";
        public const string BulkUploadFolderName = "bulkfileupload";
        public const string EnvelopeFolderName = "tempdata";
        public const string CustomerDocContainer = "qcdoc";
        public const string PrintedFileDocContainer = "printedfiles";
        public const string TempDocFolderName = "tempdoc";
        public const string SignatureFolderName = "signature";
        public const string DefaultAgencyFolderName = "velox";
        public const string TemplateFolderName = "template";
        public const string NotificationIcon = "notification-icon";
        #endregion


        #region "ArrowheadAgency"
        public const string AgencyPhoneNumber = "(678)-353-2400";
        public const string AgencyTollFreeNumber = "(800)-545-7742";
        public const string AgencyWebAddress = "www.arrowheadauto.com";
        public const string AgencyFaxNumber = "(678)-353-2400";
        #endregion



        #region "OfficeSMTPDetails"
        public const string EmailFrom = "policysupport@rateforce.net";
        public const string Host = "smtp.sendgrid.net";
        public const string UserName = "apikey";
        public const string Password = "SG.eW-e9frmQLWlC8aYanV0Iw.C9WI3Ls8ekwQmoQfn7qfqYe2e5zA7QV0QidqNy-QinY";
        public const int Priortiy = 1;
        public const int Port = 587;
        public const string EmailTo = "randy@rateforce.com";
        #endregion


        #region "SignApplication"
        public const string SignAppEmail = "misuspense@arrowheadauto.com";
        public const string SigningAppSubject = "Signing Application";
        #endregion

        #region "Docusign"
        public const string Text1 = "MONTHLY INSTALLMENT NO. 1 OF 5.";
        public const string Text2 = "MONTHLY INSTALLMENT NO. 1 OF 5.IF MAILED AFTER DUE DATE INCLUDE A LATE CHARGE OF $10.00";
        #endregion

        #region "AuditHistory"
        public static IDictionary<Enum, string> dict = new Dictionary<Enum, string>()
        {
             {Enums.AuditHistoryModule.AC, "Auto Coverage"},
             {Enums.AuditHistoryModule.AD, "Auto Driver"},
             {Enums.AuditHistoryModule.ADV, "Auto DriverViolation"},
             {Enums.AuditHistoryModule.AGB, "Agency Bank Details"},
             {Enums.AuditHistoryModule.AQ , "Auto Quote"},
             {Enums.AuditHistoryModule.AV  , "Address Verification"},
             {Enums.AuditHistoryModule.AVE , "Auto Vehicle"},
             {Enums.AuditHistoryModule.QA  , "QuestionnaireAnswer"},
             {Enums.AuditHistoryModule.QE   , "Quote Envelopes"},
             {Enums.AuditHistoryModule.QN   , "Quote Nsd"},
             {Enums.AuditHistoryModule.QP  , "Quote Payment"},
             {Enums.AuditHistoryModule.C  , "Customer"},
             {Enums.AuditHistoryModule.API  , "Auto PriorInsurance"},
             {Enums.AuditHistoryModule.AQD  , "Auto QuoteDetail"},
             {Enums.AuditHistoryModule.CA  , "Customer Address"},
             {Enums.AuditHistoryModule.T  , "Template"},
             {Enums.AuditHistoryModule.OS  , "Office SMTPDetails"},
             {Enums.AuditHistoryModule.COV  , "Coverages"},
             {Enums.AuditHistoryModule.L  , "Login"},
             {Enums.AuditHistoryModule.A  , "Agency"},
             {Enums.AuditHistoryModule.WRA  , "Web RaterAPICall"},
             {Enums.AuditHistoryModule.ARH  , "Auto RateHistory"},
             {Enums.AuditHistoryModule.LI  , "LienHolder"}
        };
        #endregion

        #region "On Board"
        public const string AgencyPassCode = "123456";
        public const string OnBoardNotificationArrowhead = " randy@rateforce.com";
        public const string OnBoardNotificationArrowheadGroup = "tdrake@arrowheadgrp.com";
        #endregion

    }
}
