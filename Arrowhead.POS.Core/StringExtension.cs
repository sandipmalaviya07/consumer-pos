﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Core
{
    public static class StringExtension
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        public static string ToPhoneNumber(this string no)
        {
            if (string.IsNullOrEmpty(no))
            {
                return no;
            }
            no = no.Trim();
            if (no.Contains("+1"))
            {
                no = no.Remove(0, 2);
            }
            no = System.Text.RegularExpressions.Regex.Replace(no, "[^0-9]+", "");
            if (no.Length == 11)
            {
                no = no.Substring(1); 
            }
            return no;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Truncate(this string s, int length)
        {
            string _str = s;
            if (!string.IsNullOrWhiteSpace(_str))
            {
                if (_str.Length > length)
                    //return _str.Substring(0, length) + "...";
                    return _str.Substring(0, length);
            }
            else
                _str = "--   no description   --";
            return _str;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string FirstCharToUpper(string input)
        {
            string returnString = string.Empty;
            if (!string.IsNullOrWhiteSpace(input))
            {
                string[] words = input.Split(' ');
                foreach (string w in words)
                {
                    if (!String.IsNullOrWhiteSpace(w))
                        if (!string.IsNullOrWhiteSpace(returnString))
                            returnString = returnString + " " + (w.First().ToString().ToUpper() + w.Substring(1).ToLower()).Trim();
                        else
                            returnString = (w.First().ToString().ToUpper() + w.Substring(1).ToLower()).Trim();
                }
            }
            return returnString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}
