﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Core
{
   public class SysFunctions
    {
        //Generate AuthToken
        public static string GenerateAuthToken(int charLength)
        {
            var random = new Random();
            var resultToken = new string(Enumerable.Repeat(ConstantVariables.ConstantAllCharsString.ToString(), charLength)
                                    .Select(token => token[random.Next(token.Length)]).ToArray());
            return resultToken.ToString();
        }
    }
}
