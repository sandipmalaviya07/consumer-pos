﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowhead.POS.Core
{
    public class Enums
    {
        public enum GeneralStatus
        {
            POLICYSTATUS,
            PAYMENTSTATUS,
            APPLICANTSOURCESTATUS,
            QUOTESTATUS,
            QUOTETYPE,
            ADDRESSTYPE,
            LICENSESTATUS,
            DRIVERRELATION,
            MARITALSTATUS,
            QUOTEORDERSTATUS,
            PAYMENTMETHOD,
            PAYMENTFOR,
            POLICYINSTALLMENTTYPES,
            ENVELOPESTATUS,
            CALLTYPE,
            ACCOUNTTYPE,
            DOCUMENTTYPE,
            FILETYPE,
            ENVELOPETYPE,
            OWNERTYPE,
            BIPRIORVALUE,
            PRIORINSURANCELAPSE,
            DRIVEROCCUPATION,
            SIGNMEDIUM,
            AGENCYSETUP,
            VERIFICATIONTYPE,
            TOKENIZE,
            MILITARY,
            WAREHOUSE,
            HOMETYPE,
            AUTHORIZENETPGSTATUS,
            USERPREFERENCETYPE
        }

        public enum PaymentMethod
        {
            CREDITCARD,
            ECHECK,
            SWEEP
        }

        public enum CallType
        {
            RATE,
            BINDPOLICY,
            MVR,
            PAYMENT,
            BRIDGEXML
        }

        public enum MaritalStatus
        {
            SINGLE,
            MARRIED
        }

        public enum FILETYPE
        {
            PDF,
            IMAGE,
            OTHER
        }


        public enum EntityCode
        {
            DOCUSIGN
        }


        public enum MessageTypeCode
        {
            EMAIL,
            TEXT
        }

        public enum CommunicationServicesProvider
        {
            TWILIO,
            RINGCENTRAL
        }


        public enum CommunicationProviderForm
        {
            Name,
            PhoneNumber,
            AccountSid,
            AuthToken,
            BaseURL,
            ClientID,
            ClientSecret,
            PassWord,
            UserName,
            ShortCode

        }

        public enum QuoteStatus
        {
            COMPLETED,
            VOID,
            NEW,
            FOLLOWUP
        }

        public enum AddressType
        {
            PERMANENT,
            BILLING
        }

        public enum DocusignRole
        {
            Customer,
            Agent
        }

        public enum MILITARY
        {
            NONE,
            AIRFORCE,
            ARMY,
            COASTGUARD,
            MARINES,
            NAVY
        }

        public enum WAREHOUSE
        {
            NONE,
            SAMSCLUB,
            COSTO
        }

        public enum EnvelopeProviderForm
        {
            ACCOUNTID,
            PASSWORD,
            INTEGRATORKEY,
            USERNAME,
            URL
        }

        public enum EnvelopeProviders
        {
            DOCUSIGN
        }


        public enum QuoteType
        {
            QUOTE,
            ENDORSEMENT,
            QUICKQUOTE,
            BRIDGEQUOTE
        }

        public enum Role
        {
            ADMINISTRATOR
        }

        public enum PolicyStatus
        {
            ONLINE,
            NEW,
            PROGRESS,
            ACTIVE,
            EXPIRED,
            CANCELLATED,
            REINSTATE,
            RENEW,
            VOID,
            BINDING,
            MVR
        }

        public enum PAYMENTSTATUS
        {
            INITIATED,
            SUCCESS,
            FAIL,
            VOID
        }

        public enum EnvelopeStatus
        {
            ESIGNCREATED,
            ESIGNCOMPLETED,
            WAITINGFORAGENT,
            WAITINGFORCLIENT,
        }
        public enum PAYMENTFOR
        {
            CARRIER,
            CREDITCARDFEES,
            NSD,
            OTHERFEES,
            AGENCYCOMMISSION,
            RFCOST
        }

        public enum PolicyInstallmentType
        {
            ENDORSEMENT,
            INSTALLMENT,
            NEWBUSINESS,
            RENEWAL,
            REWRITE,
            ROLLOVER,
            PREVIOUSDUE
        }

        public enum LicenseStatus
        {
            VALID,
            FOREIGN,
            INTERNATIONAL,
            PERMIT,
            REVOKED,
            SUSPENDED,
        }

        public enum DRIVERRELATION
        {
            SPOUSE,
            PARENT,
            RELATIVE,
            CHILD,
            OTHER,
            INSURED
        }

        public enum QUOTEORDERSTATUS
        {
            APPLICANT,
            VEHICLE,
            DRIVER,
            COVERAGE,
            QUESTIONNAIRE,
            POLICY,
            COMPLETE,
            BINDING
        }

        public enum Coverages
        {
            BI,
            UMBI,   //Uninsured Motorist Bodily Injury
            UMPD,   //Uninsured Motorist Property Damage
            MEDP,    //Medical Payments
            MEDPM,   // Additional Medical Payment Coverage
            COLL,
            COMP,
            PIP,    //Personal Injury Protection
            PPI,    //Property Protection Insurance
            PDE,
            PD,
            PIPDEDUCTIBLE,
            TL,
            UIMBI, //Underinsured Motorist Bodily Injury Per Person/Accident
            UNDUM, // Additional Underinsured Motorist Bodily Injury Per Person/Accident
            UIMPD,  //Underinsured Motorist Property Damage
            UNDPD, //Additional Underinsured Motorist Property Damage
            TORT,
            ADDPIP,
            UMPDD,  //Uninsured Motorist Property Damage Deductible  
            UM,     //Uninsured Motorist
            RENREM,
            LPD,
            LOU,
            LUSE,
            RREIM, // Additional LUSE Coverage
            PIPDEDOPT,
            COLLLEVEL,
            MCCA,
            SAF,
            SR22Fee,
            POLFE,  //Policy Fee
            MVRFee,
            UMFee
        }

        public enum ArrowheadCredential
        {
            ARROWHEADSPNAME,
            CLIENTAPPORG,
            CLIENTAPPNAME,
            CLIENTAPPVERSION,
            COMMERCIALNAME,
            PRODUCERSURNAME,
            PRODUCERNAME,
            COMPANYID,
            APIPASSWORD,
            ENDPOINTADDRESS,
            PRODUCERADDR1,
            PRODUCERADDR2,
            PRODUCERCITY,
            PRODUCERSTATECD,
            PRODUCERPHONENUMBER,
            PRODUCERPOSTALCODE,
            CONTRACTNUMBER,
            PRODUCERSUBCODE
        }

        public enum Gender
        {
            M,
            F
        }

        public enum IsDriverDefensive
        {
            N,
            Y
        }

        public enum SignMedium
        {
            SIGNINGOFFICE,
            CUSTOMERSIGN
        }

        public enum ExcludedDriver
        {
            E
        }

        public enum QuoteOrderStatus
        {
            APPLICANT,
            VEHICLE,
            DRIVER,
            COVERAGE,
            QUESTIONNAIRE,
            POLICY,
            COMPLETE
        }

        public enum AGENCYORDERSTATUS
        {
            BASICINFO,
            PINSET,
            NSDPACKAGE,
            NSDACK,
            NSDACH

        }

        public enum PaymentGateway
        {
            STRIPE,
            TRANZPAY,
            AUTHORIZENET
        }
        public enum ColLevel
        {
            None,
            Broad,
            Standard,
            Limited
        }
        public enum ColLevelValues
        {
            B,
            S,
            L
        }

        public enum EnvelopeType
        {
            PAYMENTRECEIPT,
            POLICY,
            NSD,
            OTHER,
            SR22FORM,
            SR22AFORM
        }


        public enum ProjectTerm
        {
            PROOFOFPRIOR,
            AGENCYPHONE,
            AGENCYWEBADDRESS,
            AGENCYLOCALPHONENUMBER,
            AGENCYTOLLFREENUMBER,
            AGENCYFAXNUMBER,
            DRIVERSTATUS,
            LATEDUE,
            INSTALLMENTLATEFEEDATE,
            SUPERADMINPASSWORD,
            SMSBODY,
            PHONECARRIER

        }

        public enum PhoneTypeCode
        {
            Phone,
            Fax
        }

        public enum CommunicationUseCd
        {
            Business,
            Home
        }

        public enum ApplicationSource
        {
            GOOGLE,
            YAHOO,
            YELLOWPAGES,
            EXISTINGCUSTOMER,
            FRIEND,
            OTHER
        }

        public enum TemplateType
        {
            SENDSIGNLINK,
            SENDPAPERWORK,
            SENDARROWHEADNOTIFICATION,
            SENDARROWHEADTRANSACTIONAMOUNTREPORT,
            SENDARROWHEADTRANSACTIONECHECKREPORT,
            SENDAGENCYNSDACTIVATIONLINK,
            SENDARROWHEADAGENCYINFOUPDATENOTIFICATION,
            AGENCYVOIDPOLICY,
            PRINTQUOTE,
            SENDAGENCYDAILYNSDDETAILS
        }

        public enum PolicyStatusFilter
        {
            ONLINE = 0,
            QUOTE = 5,
            PROCESS = 10,
            POLICY = 50
        }

        public enum DOCUMENTTYPE
        {
            LICENCEIMAGE,
            POLICY,
            NSD,
            PROOFOFPRIOR,
            HOMEOWNER,
            OTHER,
            BINDERFORM,
            IDCARD,
            PENDINGREGISTRATION,
            SIGNINGAPPLICATION,
            //DISMEMBERMENT,
            TOWINGANDRENTAL,
            HIPCONTRACT,
            ADANDDCONTRACT,
            //ADANDD, //Discloser
            AD,
            NSDDISCLOSURE,
            //HIPDISCLOUSURE,
            HIP,
            VEHICLEPICTURE,
            //ROADSIDEASSISTANCE,
            TOWBUSTERS,
            CANCELLATIONREQUEST,
            CHANGEREQUEST,
            GOODSTUDENT,
            VEHICLEREGISTRATION,

            TOWBUSTERSCONTRACT,
            TOWINGANDRENTALCONTRACT,
            ECHECKAUTHORIZATION,
            VOIDEDCHECK


        }

        public enum PolicyInfo
        {
            POLICYINFOTAB = 1
        }

        public enum NSDPlan
        {
            TOWBUSTERS
        }

        public enum AuditHistoryModule
        {
            AV,
            LI,
            AC,
            AD,
            AGB,
            AQ,
            AVE,
            QA,
            QE,
            QN,
            ADV,
            QP,
            C,
            AQD,
            API,
            CA,
            T,
            OS,
            COV,
            L,
            A,
            WRA,
            ARH,
            R
        }

        public enum AuditHistoryOperation
        {
            I, //I for Insert Operation.
            U,  //U for Update Operation.
            D,  //D for Delete Operation.
            S, //S for Insert && Update Operation.
            V //V for View Operation
        }


        public enum DriverOccupation
        {
            OTHER,
            ADMINISTRATIVE,
            ARTISAN,
            ATHLETE,
            CARPENTER,
            CELEBRITY,
            CLERGY,
            CLERICAL,
            CONSULTANT,
            CUSTODIANJANITOR,
            DRIVER,
            ELECTRICIAN,
            EMERGENCYSERVICES,

            GENERALCONTRACTOR,
            GENERALLABOR,
            HEALTHCAREPROVIDER,
            HOMEMAKER,
            MILITARY,
            NOTEMPLOYED,
            PAINTER,
            PROFESSIONAL,
            SALESMARKETINGAGENT,
            STUDENT,
            LABOR
        }

        public enum TransactionType
        {
            authOnlyTransaction,
            authCaptureTransaction,
            priorAuthCaptureTransaction,
            voidTransaction
        }

        public enum TransactionStatus
        {
            CAPTUREDPENDINGSETTLEMENT,
            SETTLEDSUCCESSFULLY,
            AUTHORIZEDPENDINGCAPTURED
        }

        public enum VerificationType
        {
            AMOUNTVERIFICATION,
            ECHECKAMOUNTVERIFICATION
        }

        public enum TokenizeType
        {
            NSDACTIVATIONLINK
        }

        public enum OwnerType
        {
            OWNED,
            FINANCED,
            LEASED
        }

        public enum PolicyXMLStep
        {
            APPLICANT,
            COVERAGE
        }

        public enum ExcludedLicense
        {
            EXCLUDEDLICENSESTATE
        }

        public enum AuthorizeNetPGStatus
        {
            AUTHORIZATIONCREATED,
            AUTHCAPTURECREATED,
            CAPTURECREATED,
            REFUNDCREATED,
            PRIORAUTHCAPTURECREATED,
            VOIDCREATED
        }

        public enum PriorInsuranceLapseType
        {
            FULLSIXMONTHSPRIORCOVNOLAPSE,
            LESSTHESIXMONTHSPRIORCOV,
            SIXMONTHSPRIORCOVANDLAPSEGREATERTHANONEDAY
        }

        public enum UserPreferenceType
        {
            LEFTPANEOPEN
        }

    }
}
